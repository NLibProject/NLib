#ifndef TTR_ACTION_PROTECT
#define TTR_ACTION_PROTECT

// ---------------------
// namespace TTR::Action
// ---------------------

#define REPERTOIRE_MUSIQUE		"DB/Music"
#define REPERTOIRE_NIVEAU		"DB/Niveau"
#define REPERTOIRE_NOTE_NIVEAU	"DB/Niveau/Note"
#define REPERTOIRE_DATABASE		"DB"

#define FICHIER_DATABASE		"Database"

// enum TTR::Action::NListeAction
#include "TTR_Action_NListeAction.h"

/* Action export */
NBOOL TTR_Action_Export( const char *data,
	NU32 taille,
	NU32 *curseur,
	const NClientServeur* );

/* Action catalogue */
NBOOL TTR_Action_Catalogue( const char *data,
	NU32 taille,
	NU32 *curseur,
	const NClientServeur* );

/* Action import */
NBOOL TTR_Action_Import( const char*,
	NU32 taille,
	NU32 *curseur,
	const NClientServeur* );

/* Action notation niveau */
NBOOL TTR_Action_Notation( const char*,
	NU32 taille,
	NU32 *curseur,
	const NClientServeur* );

#endif // !TTR_ACTION_PROTECT

