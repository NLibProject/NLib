#ifndef TTR_PROTECT
#define TTR_PROTECT

// -------------
// namespace TTR
// -------------

// namespace NLib
#include "../../../../NLib/include/NLib/NLib.h"

// namespace TTR::Erreur
#include "Erreur/TTR_Erreur.h"

// namespace TTR::Action
#include "Action/TTR_Action.h"

// namespace TTR::Catalogue
#include "Catalogue/TTR_Catalogue.h"

#endif // !TTR_PROTECT

