#ifndef TTR_CATALOGUE_NENTREECATALOGUE_PROTECT
#define TTR_CATALOGUE_NENTREECATALOGUE_PROTECT

// ---------------------------------------
// struct TTR::Catalogue::NEntreeCatalogue
// ---------------------------------------

#define EXTENSION_FICHIER_NOTE_NIVEAU ".not"

typedef struct NEntreeCatalogue
{
	// Titre musique
	char *titre;

	// Nom niveau
	char *nomNiveau;

	// Pseudo
	char *pseudo;

	// Moyenne note (/100)
	NU32 moyenneNote;

	// Note utilisateur
	NU32 noteUtilisateur;

	// Nombre de votes
	NU32 nombreVote;
} NEntreeCatalogue;

/* Construire */
__ALLOC NEntreeCatalogue *TTR_Catalogue_NEntreeCatalogue_Construire( const char *titre,
	const char *nomNiveau,
	const char *pseudo,
	const char *identifiantAppareil );

/* Detruire */
void TTR_Catalogue_NEntreeCatalogue_Detruire( NEntreeCatalogue** );

/* Obtenir titre */
const char *TTR_Catalogue_NEntreeCatalogue_ObtenirTitre( const NEntreeCatalogue* );

/* Obtenir nom niveau */
const char *TTR_Catalogue_NEntreeCatalogue_ObtenirNomNiveau( const NEntreeCatalogue* );

/* Obtenir pseudo */
const char *TTR_Catalogue_NEntreeCatalogue_ObtenirPseudo( const NEntreeCatalogue* );

/* Obtenir moyenne notes */
NU32 TTR_Catalogue_NEntreeCatalogue_ObtenirMoyenneNote( const NEntreeCatalogue* );

/* Obtenir note utilisateur */
NU32 TTR_Catalogue_NEntreeCatalogue_ObtenirNoteUtilisateur( const NEntreeCatalogue* );

/* Obtenir nombre votes */
NU32 TTR_Catalogue_NEntreeCatalogue_ObtenirNombreVote( const NEntreeCatalogue* );

#endif // !TTR_CATALOGUE_NENTREECATALOGUE_PROTECT

