#ifndef TTR_CATALOGUE_NFICHIERNOTECATALOGUE_PROTECT
#define TTR_CATALOGUE_NFICHIERNOTECATALOGUE_PROTECT

// --------------------------------------------
// struct TTR::Catalogue::NFichierNoteCatalogue
// --------------------------------------------

typedef struct NFichierNoteCatalogue
{
	// Nombre d'entrees
	NU32 nombreEntree;

	// Entrees
	NEntreeFichierNoteCatalogue **entree;

	// Nom niveau
	char *nomNiveau;
} NFichierNoteCatalogue;

/* Construire */
__ALLOC NFichierNoteCatalogue *TTR_Catalogue_NFichierNoteCatalogue_Construire( const char *niveau );
__ALLOC NFichierNoteCatalogue *TTR_Catalogue_NFichierNoteCatalogue_Construire2( const char *niveau );

/* Detruire */
void TTR_Catalogue_NFichierNoteCatalogue_Detruire( NFichierNoteCatalogue** );

/* Ajouter une entree */
NBOOL TTR_Catalogue_NFichierNoteCatalogue_AjouterEntree( NFichierNoteCatalogue*,
	float note,
	const char *identifiant );

/* Sauvegarder */
NBOOL TTR_Catalogue_NFichierNoteCatalogue_Sauvegarder( const NFichierNoteCatalogue* );

/* Est appareil deja present? */
NBOOL TTR_Catalogue_NFichierNoteCatalogue_EstAppareilPresent( const NFichierNoteCatalogue*,
	const char* );

/* Obtenir nombre entrees */
NU32 TTR_Catalogue_NFichierNoteCatalogue_ObtenirNombreEntree( const NFichierNoteCatalogue* );

/* Obtenir entree */
const NEntreeFichierNoteCatalogue *TTR_Catalogue_NFichierNoteCatalogue_ObtenirEntree( const NFichierNoteCatalogue*,
	NU32 );

/* Obtenir moyenne notes */
float TTR_Catalogue_NFichierNoteCatalogue_ObtenirMoyenneNote( const NFichierNoteCatalogue* );

/* Obtenir note appareil */
float TTR_Catalogue_NFichierNoteCatalogue_ObtenirNoteAppareil( const NFichierNoteCatalogue*,
	const char *appareil );

/* Obtenir nom niveau */
const char *TTR_Catalogue_NFichierNoteCatalogue_ObtenirNomNiveau( const NFichierNoteCatalogue* );

#endif // !TTR_CATALOGUE_NFICHIERNOTECATALOGUE_PROTECT

