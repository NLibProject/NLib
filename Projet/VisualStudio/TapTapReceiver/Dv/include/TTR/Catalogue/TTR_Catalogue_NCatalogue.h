#ifndef TTR_CATALOGUE_NCATALOGUE_PROTECT
#define TTR_CATALOGUE_NCATALOGUE_PROTECT

// ---------------------------------
// struct TTR::Catalogue::NCatalogue
// ---------------------------------

typedef struct NCatalogue
{
	// Entrees
	NEntreeCatalogue **entree;

	// Nombre entrees
	NU32 nombreEntree;
} NCatalogue;

/* Construire */
__ALLOC NCatalogue *TTR_Catalogue_NCatalogue_Construire( const char *identifiantAppareil );

/* Detruire */
void TTR_Catalogue_NCatalogue_Detruire( NCatalogue** );

/* Obtenir nombre entrees */
NU32 TTR_Catalogue_NCatalogue_ObtenirNombreEntree( const NCatalogue* );

/* Obtenir entree */
const NEntreeCatalogue *TTR_Catalogue_NCatalogue_ObtenirEntree( const NCatalogue*,
	NU32 );

#endif // !TTR_CATALOGUE_NCATALOGUE_PROTECT

