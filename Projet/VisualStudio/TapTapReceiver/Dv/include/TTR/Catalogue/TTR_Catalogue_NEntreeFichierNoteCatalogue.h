#ifndef TTR_CATALOGUE_NENTREEFICHIERNOTECATALOGUE_PROTECT
#define TTR_CATALOGUE_NENTREEFICHIERNOTECATALOGUE_PROTECT

// --------------------------------------------------
// struct TTR::Catalogue::NEntreeFichierNoteCatalogue
// --------------------------------------------------

typedef struct NEntreeFichierNoteCatalogue
{
	// Note
	float note;

	// Appareil
	char *appareil;
} NEntreeFichierNoteCatalogue;

/* Construire */
__ALLOC NEntreeFichierNoteCatalogue *TTR_Catalogue_NEntreeFichierNoteCatalogue_Construire( float note,
	const char* appareil );

/* Detruire */
void TTR_Catalogue_NEntreeFichierNoteCatalogue_Detruire( NEntreeFichierNoteCatalogue** );

/* Sauvegarder */
NBOOL TTR_Catalogue_NEntreeFichierNoteCatalogue_Sauvegarder( NEntreeFichierNoteCatalogue*,
	NFichierTexte* );

/* Definir note */
void TTR_Catalogue_NEntreeFichierNoteCatalogue_DefinirNote( NEntreeFichierNoteCatalogue*,
	float );

/* Obtenir note */
float TTR_Catalogue_NEntreeFichierNoteCatalogue_ObtenirNote( const NEntreeFichierNoteCatalogue* );

/* Obtenir appareil */
const char *TTR_Catalogue_NEntreeFichierNoteCatalogue_ObtenirAppareil( const NEntreeFichierNoteCatalogue* );

#endif // !TTR_CATALOGUE_NENTREEFICHIERNOTECATALOGUE_PROTECT

