#ifndef TTR_CATALOGUE_PROTECT
#define TTR_CATALOGUE_PROTECT

// ------------------------
// namespace TTR::Catalogue
// ------------------------

// struct TTR::Catalogue::NEntreeCatalogue
#include "TTR_Catalogue_NEntreeCatalogue.h"

// struct TTR::Catalogue::NEntreeFichierNoteCatalogue
#include "TTR_Catalogue_NEntreeFichierNoteCatalogue.h"

// struct TTR::Catalogue::NFichierNoteCatalogue
#include "TTR_Catalogue_NFichierNoteCatalogue.h"

// struct TTR::Catalogue::NCatalogue
#include "TTR_Catalogue_NCatalogue.h"

#endif // !TTR_CATALOGUE_PROTECT

