#include "../../../include/TTR/TTR.h"

// --------------------------------------------------
// struct TTR::Catalogue::NEntreeFichierNoteCatalogue
// --------------------------------------------------

/* Construire */
__ALLOC NEntreeFichierNoteCatalogue *TTR_Catalogue_NEntreeFichierNoteCatalogue_Construire( float note,
	const char* appareil )
{
	// Sortie
	__OUTPUT NEntreeFichierNoteCatalogue *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NEntreeFichierNoteCatalogue ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer appareil
	if( !( out->appareil = calloc( strlen( appareil ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier
	out->note = note;
	memcpy( out->appareil,
		appareil,
		strlen( appareil ) );

	// OK
	return out;
}

/* Detruire */
void TTR_Catalogue_NEntreeFichierNoteCatalogue_Detruire( NEntreeFichierNoteCatalogue **this )
{
	// Liberer
	NFREE( (*this)->appareil );
	NFREE( *this );
}

/* Sauvegarder */
NBOOL TTR_Catalogue_NEntreeFichierNoteCatalogue_Sauvegarder( NEntreeFichierNoteCatalogue *this,
	NFichierTexte *fichier )
{
	// Inscrire
	NLib_Fichier_NFichierTexte_Ecrire3( fichier,
		this->appareil );
	NLib_Fichier_NFichierTexte_Ecrire3( fichier,
		";" );
	NLib_Fichier_NFichierTexte_Ecrire4( fichier,
		this->note );
	NLib_Fichier_NFichierTexte_Ecrire3( fichier,
		";\n" );

	// OK
	return NTRUE;
}

/* Definir note */
void TTR_Catalogue_NEntreeFichierNoteCatalogue_DefinirNote( NEntreeFichierNoteCatalogue *this,
	float note )
{
	this->note = note;
}

/* Obtenir note */
float TTR_Catalogue_NEntreeFichierNoteCatalogue_ObtenirNote( const NEntreeFichierNoteCatalogue *this )
{
	return this->note;
}

/* Obtenir appareil */
const char *TTR_Catalogue_NEntreeFichierNoteCatalogue_ObtenirAppareil( const NEntreeFichierNoteCatalogue *this )
{
	return this->appareil;
}

