#include "../../../include/TTR/TTR.h"

// ---------------------------------
// struct TTR::Catalogue::NCatalogue
// ---------------------------------

/* Ajouter entree (privee) */
NBOOL TTR_Catalogue_NCatalogue_AjouterEntreeInterne( NCatalogue *this,
	__WILLBEOWNED NEntreeCatalogue *entree )
{
	// Entrees temporaires
	NEntreeCatalogue **tmp;

	// Aucune entree pour l'instant
	if( !this->nombreEntree )
	{
		// Allouer la memoire
		if( !( this->entree = calloc( 1,
			sizeof( NEntreeCatalogue* ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Quitter
			return NFALSE;
		}
	}
	// Une entree ou plus
	else
	{
		// Sauvegarder ancienne adresse
		tmp = this->entree;

		// Allouer la memoire
		if( !( this->entree = calloc( this->nombreEntree + 1,
			sizeof( NEntreeCatalogue* ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Restaurer
			this->entree = tmp;

			// Quitter
			return NFALSE;
		}

		// Copier
		memcpy( this->entree,
			tmp,
			sizeof( NEntreeCatalogue* ) * this->nombreEntree );

		// Liberer
		NFREE( tmp );
	}

	// Ajouter
	this->entree[ this->nombreEntree ] = entree;

	// Incrementer
	this->nombreEntree++;

	// OK
	return NTRUE;
}

__ALLOC char *TTR_Catalogue_NCatalogue_ExtrairePseudo( const char *niveau )
{
	// Sortie
	__OUTPUT char *sortie;

	// Fichier
	NFichierTexte *fichier;

	// Repertoire
	NRepertoire *rep;

	// Lien vers DB
	char lien[ 2048 ] = REPERTOIRE_DATABASE"/"FICHIER_DATABASE;

	// Verifier si la db existe
		// Creer rep
			if( !( rep = NLib_Module_Repertoire_NRepertoire_Construire( ) ) )
			{
				NOTIFIER_ERREUR( NLIB_ERREUR_REPERTOIRE );
				return NULL;
			}
		// Lister
			if( !NLib_Module_Repertoire_NRepertoire_Lister( rep,
				REPERTOIRE_DATABASE"/*",
				NATTRIBUT_REPERTOIRE_NORMAL ) )
			{
				NOTIFIER_ERREUR( NLIB_ERREUR_REPERTOIRE );
				NLib_Module_Repertoire_NRepertoire_Detruire( &rep );
				return NULL;
			}
		// Verifier l'existe
			if( !NLib_Module_Repertoire_NRepertoire_EstFichierExiste( rep,
				FICHIER_DATABASE ) )
			{
				NOTIFIER_ERREUR( NERREUR_FILE_NOT_FOUND );
				NLib_Module_Repertoire_NRepertoire_Detruire( &rep );
				return NULL;
			}
		// Detruire rep
			NLib_Module_Repertoire_NRepertoire_Detruire( &rep );

	// Ouvrir
	do
	{
		// Ouvrir
		fichier = NLib_Fichier_NFichierTexte_ConstruireLecture( lien );

		// Verifier
		if( !fichier )
			NLib_Temps_Attendre( 1000 );
	} while( fichier == NULL );

	// Chercher
		// Placer au niveau
			if( !NLib_Fichier_NFichierTexte_PositionnerProchaineChaine( fichier,
				niveau ) )
			{
				NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );
				NLib_Fichier_NFichierTexte_Detruire( &fichier );
				return NULL;
			}
		// Lire
			sortie = NLib_Fichier_NFichierTexte_Lire2( fichier,
				'|',
				NFALSE );

	// Fermer
	NLib_Fichier_NFichierTexte_Detruire( &fichier );

	// OK
	return sortie;
}

/* Construire */
__ALLOC NCatalogue *TTR_Catalogue_NCatalogue_Construire( const char *identifiantAppareil )
{
	// Repertoire
	NRepertoire *repertoire;

	// Sortie
	__OUTPUT NCatalogue *out;

	// Iterateur
	NU32 i;

	// Entree
	NEntreeCatalogue *entree;

	// Nom musique
	char *nomMusique;

	// Nom fichier
	const char *nomFichier;

	// Pseudo
	char *pseudo;

	// Fichier
	NFichierTexte *fichier;

	// Lien
	char lien[ 2048 ];

	// Construire repertoire
	if( !( repertoire = NLib_Module_Repertoire_NRepertoire_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Lister
	if( !NLib_Module_Repertoire_NRepertoire_Lister( repertoire,
		REPERTOIRE_NIVEAU"/*",
		NATTRIBUT_REPERTOIRE_NORMAL ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NLIB_ERREUR_REPERTOIRE );

		// Liberer
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NCatalogue ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Ajouter entrees
	for( i = 0; i < NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( repertoire ); i++ )
	{
		// Verifier
		if( NLib_Module_Repertoire_Element_NElementRepertoire_EstRepertoire( NLib_Module_Repertoire_NRepertoire_ObtenirFichier( repertoire,
			i ) ) )
			continue;

		// Obtenir nom fichier
		if( !( nomFichier = NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirNom( NLib_Module_Repertoire_NRepertoire_ObtenirFichier( repertoire,
			i ) ) ) )
			continue;

		// Obtenir lien
		sprintf( lien,
			"%s/%s",
			REPERTOIRE_NIVEAU,
			nomFichier );

		// Ouvrir fichier
		if( !( fichier = NLib_Fichier_NFichierTexte_ConstruireLecture( lien ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE );

			// Passer au fichier suivant
			continue;
		}

		// Lire le nom de la musique...
		if( !NLib_Fichier_NFichierTexte_PositionnerProchaineChaine( fichier,
			"Nom Musique:" ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE );
			printf( "[%s]\n", lien );

			// Fermer
			NLib_Fichier_NFichierTexte_Detruire( &fichier );

			// Passer au fichier suivant
			continue;
		}

		// Lire
		if( !( nomMusique = NLib_Fichier_NFichierTexte_Lire2( fichier,
			'"',
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE );

			// Fermer le fichier
			NLib_Fichier_NFichierTexte_Detruire( &fichier );

			// Fichier suivant
			continue;
		}

		// Fermer le fichier
		NLib_Fichier_NFichierTexte_Detruire( &fichier );

		// Recuperer pseudo
		if( !( pseudo = TTR_Catalogue_NCatalogue_ExtrairePseudo( nomFichier ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE );

			// Liberer
			NFREE( nomMusique );

			// Fichier suivant
			continue;
		}

		// Creer l'entree
		if( !( entree = TTR_Catalogue_NEntreeCatalogue_Construire( nomMusique,
			nomFichier,
			pseudo,
			identifiantAppareil ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			NFREE( nomMusique );
			NFREE( pseudo );

			// Fichier suivant
			continue;
		}

		// Liberer
		NFREE( pseudo );
		NFREE( nomMusique );

		// Ajouter entree
		TTR_Catalogue_NCatalogue_AjouterEntreeInterne( out,
			entree );
	}

	// Detruire repertoire
	NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

	// OK
	return out;
}

/* Detruire */
void TTR_Catalogue_NCatalogue_Detruire( NCatalogue **this )
{
	// Iterateur
	NU32 i = 0;

	// Liberer
	for( ; i < (*this)->nombreEntree; i++ )
		TTR_Catalogue_NEntreeCatalogue_Detruire( &(*this)->entree[ i ] );
	NFREE( (*this)->entree );
	NFREE( *this );
}

/* Obtenir nombre entrees */
NU32 TTR_Catalogue_NCatalogue_ObtenirNombreEntree( const NCatalogue *this )
{
	return this->nombreEntree;
}

/* Obtenir entree */
const NEntreeCatalogue *TTR_Catalogue_NCatalogue_ObtenirEntree( const NCatalogue *this,
	NU32 entree )
{
	return this->entree[ entree ];
}
