#include "../../../include/TTR/TTR.h"

// ------------------------------
// enum TTR::Action::NListeAction
// ------------------------------

/* Determiner action */
NListeAction TTR_Action_NListeAction_Determiner( const char *action )
{
	// Sortie
	__OUTPUT NListeAction sortie = (NListeAction)0;

	// Chercher
	for( ; sortie < NLISTE_ACTIONS; sortie++ )
		if( !_strcmpi( action,
			NListeActionTexte[ sortie ] ) )
			return sortie;

	// Introuvable
	return NERREUR;
}

