#ifndef CACHEPACKET_PROTECT
#define CACHEPACKET_PROTECT

// struct CachePacket (file)
typedef struct
{
	// Nombre de packet
	unsigned int m_nombrePacket;

	// Packet
	Packet **m_packet;

	// Mutex
	HANDLE m_mutex;
} CachePacket;

// Construire cache packet
__ALLOC CachePacket *CachePacket_Construire( void );

// Detruire cache packet
void CachePacket_Detruire( CachePacket** );

// Obtenir le nombre de packets
unsigned int CachePacket_ObtenirNombrePackets( const CachePacket* );

// Obtenir le packet a envoyer
__ALLOC Packet *CachePacket_ObtenirPacket( const CachePacket* );

// Est packet(s) dans le cache?
BOOL CachePacket_EstPacketsDansCache( const CachePacket *this );

// Ajouter un packet
BOOL CachePacket_AjouterPacket( CachePacket*,
	__WILLBEOWNED const Packet* );

// Supprimer le packet
BOOL CachePacket_SupprimerPacket( CachePacket* );


#endif // !CACHEPACKET_PROTECT

