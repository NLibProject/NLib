#define COMMANDESERVEUR_INTERNE
#include "Types.h"

// Parser une commande
CommandeServeur CommandeServeur_ParserCommande( const char *commande )
{
	// Sortie
	__OUTPUT CommandeServeur sortie = (CommandeServeur)0;

	// Parser
	for( ; sortie < COMMANDES_SERVEUR; sortie++ )
		// Comparer commandes
		if( !_strcmpi( commande,
			CommandeServeur_ObtenirCommande( sortie ) ) )
			// On a trouve la bonne
			break;

	// Donner le resultat
	return sortie;
}

// Obtenir une commande
const char *CommandeServeur_ObtenirCommande( CommandeServeur commande )
{
	return CommandeServeurTexte[ commande ];
}

// Obtenir une description de commande
const char *CommandeServeur_ObtenirDescriptionCommande( CommandeServeur commande )
{
	return DescriptionCommandeServeurTexte[ commande ];
}

