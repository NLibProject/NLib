#ifndef HTTPSPECIALTREATMENT_PROTECT
#define HTTPSPECIALTREATMENT_PROTECT

/* Genere les packets 404 par defaut */
__ALLOC char *HTTPSpecialTreatment_Generate404Msg( const char *fichierDemande );
__ALLOC Packet *HTTPSpecialTreatment_Generate404Header( const char *fichierDemande );
__ALLOC Packet *HTTPSpecialTreatment_Generate404Content( const char *fichierDemande );

/* Genere les packets de description */
__ALLOC char *HTTPSpecialTreatment_GenerateDescriptionMsg( void );
__ALLOC Packet *HTTPSpecialTreatment_GenerateDescriptionContent( void );
__ALLOC Packet *HTTPSpecialTreatment_GenerateDescriptionHeader( void );

#ifdef HTTPSPECIALTREATMENT_INTERNE

#define TAILLE_TEXTE_404				2048
const char Erreur404Texte[ TAILLE_TEXTE_404 ] =
{
	"<!DOCTYPE html>\n"
	"<html>\n"
	"<head>\n"
	"	<title>NRobot Erreur 404</title>\n"
	"	<style>\n"
	"		body { \n"
	"				background-color: #C9E4FF;\n"
	"				margin: 20px 20px;\n"
	"		}\n"
	"	</style>\n"
	"</head>\n"
	"\n"
	"<body>\n"
	"	<header>\n"
	"		\n"
	"	</header>\n"
	"	\n"
	"	<p>Robot n'a pas trouve le fichier [<em>%s</em>]</p>\n"
	"	<hr />\n"
	"	\n"
	"	<footer>\n"
	"		<p><a href=\"http://nproject.ddns.net/NServeur\">NServeur</a> - NProject (LS)</p>\n"
	"	</footer>\n"
	"</body>\n"
	"\n"
	"</html>"
};

#define TAILLE_TEXTE_DESCRIPTION		2048
const char DescriptionTexte[ TAILLE_TEXTE_DESCRIPTION ] =
{
	"<!DOCTYPE html>\n"
	"<html>\n"
	"<head>\n"
	"	<title>Pr&eacutesentation</title>\n"
	"	<style>\n"
	"		body { \n"
	"				background-color: #C9E4FF;\n"
	"				margin: 20px 20px;\n"
	"		}\n"
	"	</style>\n"
	"</head>\n"
	"\n"
	"<body>\n"
	"	<header>\n"
	"		<h1>NServeur par SOARES Lucas</h1>\n"
	"	</header>\n"
	"	\n"
	"	\n"
	"	<hr />\n"
	"	\n"
	"	<footer>\n"
	"		<p><a href=\"http://nproject.ddns.net/\">NProject</a> (LS)</p>\n"
	"	</footer>\n"
	"</body>\n"
	"\n"
	"</html>"
};
#endif // HTTPSPECIALTREATMENT_INTERNE

#endif // !HTTPSPECIALTREATMENT_PROTECT

