#ifndef SOCKETSELECTION_PROTECT
#define SOCKETSELECTION_PROTECT

// Selectionner socket ecriture
BOOL SocketSelection_EstEcritureDisponible( SOCKET socket );

// Selectionner socket lecture
BOOL SocketSelection_EstLectureDisponible( SOCKET socket );

#endif // !SOCKETSELECTION_PROTECT

