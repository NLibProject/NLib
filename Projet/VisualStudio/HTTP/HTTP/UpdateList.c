#include "Types.h"

#pragma warning( disable:4706 ) // assignation au sein d'une expression conditionnelle

#define FICHIER_SORTIE		"index.html"
#define COULEUR_SECTION		"00AA00"

// Repertoire
typedef struct
{
	// Nombre de fichiers
	unsigned int m_nombreFichiers;

	// Nom du repertoire
	char *m_nom;

	// Fichier
	char **m_fichiers;
} Repertoire;

// Resultat du traitement
typedef struct 
{
	// Nombre repertoires
	unsigned int m_nombreRepertoires;

	// Total fichiers
	unsigned int m_nombreTotalFichiers;

	// Repertoires
	Repertoire **m_repertoires;
} Resultat;

void libererRepertoire( Repertoire *r )
{
	// Iterateur
	unsigned int i = 0;

	// Liberer le nom du repertoire
	free( r->m_nom );

	// Liberer les fichiers
	for( ; i < r->m_nombreFichiers; i++ )
		free( r->m_fichiers[ i ] );

	// Liberer la base des fichiers
	free( r->m_fichiers );
}

void libererResultat( Resultat *r )
{
	// Iterateur
	unsigned int i = 0;

	// Liberer les repertoires
	for( ; i < r->m_nombreRepertoires; i++ )
	{
		// Liberer contenu
		libererRepertoire( r->m_repertoires[ i ] );

		// Liberer memoire
		free( r->m_repertoires[ i ] );
	}

	// Liberer base repertoire
	free( r->m_repertoires );
}

void ecrireEnteteHTML( FILE *fichier,
	Resultat *resultat )
{
	// Base
	fputs( "<!DOCTYPE html>\n", fichier );
	fputs( "<html lang=\"fr\">\n", fichier );
	fputs( "<head>\n", fichier );
	fputs( "<title>Liste des fichiers</title>\n", fichier );
	fputs( "<meta charset=\"UTF-8\" />\n", fichier );
	fputs( "<style>\n", fichier );
	fputs( "body { background-color: #FFFFFF; font-family: \"Arial\", Sans-serif; width: 80%; margin: auto auto; text-align: center; }\n", fichier );
	fputs( "h1 { font-style: italic; text-decoration: underline; border: solid; padding-top: 10px; padding-bottom: 10px; margin-bottom: 20px; background-color: #"COULEUR_SECTION"; }\n", fichier );
	fputs( "h2 { font-size: 20px; }\n", fichier );
	fputs( "ul { list-style-type: none; padding-left: 0px; }\n", fichier );
	fputs( "a { color: #00FFFF; }\n", fichier );
	fputs( "table { margin: auto; auto; margin-bottom: 10px; border: 2px solid; border-collapse: collapse; }\n", fichier );
	fputs( "tr, th, td { border: 2px solid; }\n", fichier );
	fputs( "section { border: solid; background-color: #"COULEUR_SECTION"; margin-top: 10px; margin-bottom: 10px; }\n", fichier );
	fputs( "footer { border: solid; margin-top: 20px; margin-bottom: 10px; background-color: #"COULEUR_SECTION"; }\n", fichier );
	fputs( "</style>\n", fichier );
	fputs( "</head>\n", fichier );

	// Entete
	fputs( "<body>\n", fichier );
	fputs( "<header>\n", fichier );
	fprintf( fichier,
		"<h1>Liste de%s %d fichier%s r&eacute;parti%s dans %d dossier%s)</h1>\n",
		( ( resultat->m_nombreTotalFichiers > 1 ) ? "s" : "" ),
		resultat->m_nombreTotalFichiers,
		( ( resultat->m_nombreTotalFichiers > 1 ) ? "s" : "" ),
		( ( resultat->m_nombreTotalFichiers > 1 ) ? "s" : "" ),
		resultat->m_nombreRepertoires,
		( ( resultat->m_nombreRepertoires > 1 ) ? "s" : "" ) );
	fputs( "</header>\n", fichier );
}

void ecrirePiedHTML( FILE *fichier )
{
	fputs( "<footer>\n", fichier );
	fputs( "<p>Par <em>SOARES Lucas</em>, <a href=\"http://nproject.ddns.net/NServeur\" title=\"Site du NProject\"><em>NProject</em></a> 2016, <a href=\"https://jigsaw.w3.org/css-validator\"><img style=\"border:0; width:88px; height:31px; vertical-align: middle;\" src=\"http://jigsaw.w3.org/css-validator/images/vcss-blue\" alt=\"CSS Valide !\" /></a></p>", fichier );
	fputs( "</footer>\n", fichier );
	fputs( "</body>\n", fichier );
	fputs( "</html>\n", fichier );
}

#define TAILLE_BUFFER_LIEN_FICHIER_HTML	4096

void corrigerLienFormatHTML( char *lien )
{
	// Buffer
	char buffer[ TAILLE_BUFFER_LIEN_FICHIER_HTML ] = { 0, };

	// Curseur buffer
	unsigned int curseurBuffer = 0;

	// Curseur lien
	unsigned int curseurLien;

	// Taille lien
	unsigned int tailleLien;

	// 4 octets
	unsigned char lowWeight,
		highWeight;

	// Est changement caractere?
	BOOL estChangementCaractere = FALSE;

	// Calculer la taille du lien
	tailleLien = strlen( lien );

	// Parcourir
	for( curseurLien = 0; curseurLien < tailleLien; curseurLien++ )
		if( Caractere_EstUnCaractereViableHTMLHref( lien[ curseurLien ] ) )
			buffer[ curseurBuffer++ ] = lien[ curseurLien ];
		else
		{
			// Calculer 4 octets
				// Poids lourd
					highWeight = ( (unsigned char)lien[ curseurLien ] ) >> 4;
				// Poids faible
					lowWeight = ( ( (unsigned char)lien[ curseurLien ] ) << 4 );
					lowWeight >>= 4;

			// Symbole '%'
			buffer[ curseurBuffer++ ] = '%';
			buffer[ curseurBuffer++ ] = Caractere_ConvertirHexadecimal( highWeight );
			buffer[ curseurBuffer++ ] = Caractere_ConvertirHexadecimal( lowWeight );

			// Il y a eu changement de caractere
			estChangementCaractere = TRUE;
		}

	// Copier si il y a eu un changement
	if( estChangementCaractere )
	{
		// Vider le lien
		memset( lien,
			0,
			TAILLE_BUFFER_LIEN_FICHIER_HTML );

		// Copier
		memcpy( lien,
			buffer,
			strlen( buffer ) );
	}
}

BOOL corrigerNomFichierFormatHTML( const char *src,
	char **dst )
{
	// Cha�ne temporaire
	char *temp;

	// Remplacement
	const char *remplacement;

	// Taille remplacement
	unsigned int tailleRemplacement;

	// Taille src
	unsigned int tailleSrc;

	// Curseur src
	unsigned int curseurSrc = 0;

	// Curseur dst
	unsigned int curseurDst = 0;

	// Curseur remplacement
	unsigned int curseurRemplacement;

	// Recuperer la taille
	tailleSrc = strlen( src );

	// Copier
	for( ; curseurSrc < tailleSrc; curseurSrc++ )
		// Le caractere doit etre remplace?
		if( CaractereHTML_EstCaractereDoitRemplacer( src[ curseurSrc ] ) )
		{
			// Obtenir le remplacement
			remplacement = CaractereHTML_ObtenirEquivalence( src[ curseurSrc ] );
			
			// Obtenir la taille du remplacement
			tailleRemplacement = strlen( remplacement );

			// S'il y a deja quelque chose
			if( *dst != NULL )
			{
				// Enregistrer ancienne adresse
				temp = *dst;

				// Allouer la memoire
				if( !( *dst = calloc( strlen( temp ) + tailleRemplacement + 1,
					sizeof( char ) ) ) )
				{
					// Notifier
					perror( "UpdateList_CorrigerNomFichierFormatHTML( )::Impossible d'allouer la memoire: " );

					// Liberer
					FREE( temp );

					// Quitter
					return FALSE;
				}

				// Copier
				memcpy( *dst,
					temp,
					strlen( temp ) );

				// Liberer
				FREE( temp );
			}
			// S'il n'y avait rien
			else
				// Allouer la memoire
				if( !( *dst = calloc( tailleRemplacement + 1,
					sizeof( char ) ) ) )
				{
					// Notifier
					perror( "UpdateList_CorrigerNomFichierFormatHTML( )::Impossible d'allouer la memoire: " );

					// Quitter
					return FALSE;
				}

			// Ajouter
			for( curseurRemplacement = 0; curseurRemplacement < tailleRemplacement; curseurRemplacement++ )
				(*dst)[ curseurDst++ ] = remplacement[ curseurRemplacement ];
		}
		// Copier
		else
		{
			// Si il y a deja quelque chose
			if( *dst != NULL )
			{
				// Enregistrer ancienne adresse
				temp = *dst;

				// Allouer la memoire
				if( !( *dst = calloc( strlen( temp ) + 2,
					sizeof( char ) ) ) )
				{
					// Notifier
					perror( "UpdateList_CorrigerNomFichierFormatHTML( )::Impossible d'allouer la memoire: " );

					// Liberer
					FREE( temp );

					// Quitter
					return FALSE;
				}

				// Copier
				memcpy( *dst,
					temp,
					strlen( temp ) );

				// Liberer
				FREE( temp );
			}
			// S'il n'y avait rien
			else
				// Allouer la memoire
				if( !( *dst = calloc( 2,
					sizeof( char ) ) ) )
				{
					// Notifier
					perror( "UpdateList_CorrigerNomFichierFormatHTML( )::Impossible d'allouer la memoire: " );

					// Quitter
					return FALSE;
				}

			// Copier
			(*dst)[ curseurDst++ ] = src[ curseurSrc ];
		}
	

	// OK
	return TRUE;
}

void ecrireFichierHTML( const char *repertoire,
	const char *nom,
	FILE *fichier )
{
	// Lien
	char lien[ TAILLE_BUFFER_LIEN_FICHIER_HTML ] = { 0, };

	// Nom modifie
	char *nomModifie = NULL;

	// Taille
	unsigned long taille;

	// Creer le lien
	sprintf( lien,
		"%s/%s",
		repertoire,
		nom );

	// Recuperer la taille
	taille = FileManipulation_GetSize2( lien );

	// Corriger le lien au format HTML
	corrigerLienFormatHTML( lien );

	// Corriger le nom du fichier
	if( !corrigerNomFichierFormatHTML( nom,
		&nomModifie ) )
		return;

	// Ecrire
	fprintf( fichier,
		"<td><a href=\"%s\" title=\"T&eacute;l&eacute;charger le fichier\">%s</a></td><td>%1.1f ko</td>\n",
		lien,
		nomModifie,
		(double)taille / 1000.0 );

	// Liberer
	FREE( nomModifie );
}

// Nombre de fichiers interdits
#define NOMBRE_FICHIERS_INTERDITS	3

// Liste des fichiers autorises
static const char listeFichiersInterdits[ NOMBRE_FICHIERS_INTERDITS ][ 48 ] =
{
	"index.html",
	"UpdateList.exe",
	"NServeur.exe"
};

int estFichierAutorise( const char *nom )
{
	// Iterateur
	int i = 0;

	// Chercher dans la liste
	for( ; i < NOMBRE_FICHIERS_INTERDITS; i++ )
		if( !_strcmpi( nom,
			listeFichiersInterdits[ i ] ) )
			return 0;

	// OK
	return 1;
}

// Nombre de dossiers interdits
#define NOMBRE_DOSSIERS_INTERDITS	1

// Liste des dossiers autorises
static const char listeDossiersInterdits[ NOMBRE_DOSSIERS_INTERDITS ][ 48 ] =
{
	"res"
};

int estDossierAutorise( const char *nom )
{
	// Iterateur
	int i = 0;

	// Chercher dans la liste
	for( ; i < NOMBRE_DOSSIERS_INTERDITS; i++ )
		if( !_strcmpi( nom,
			listeDossiersInterdits[ i ] ) )
			return 0;

	// OK
	return 1;
}

int ajouterFichierRepertoire( Repertoire *repertoire,
	const char *fichier )
{
	// Creer le nouveau fichier
	char *nouveauFichier = NULL;

	// Index d'ajout
	unsigned int indexAjout;

	// Nouveau contenant
	char **nouveauContenant;

	// Allouer la memoire
	if( !( nouveauFichier = calloc( strlen( fichier ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		perror( "Impossible d'allouer la memoire: " );

		// Quitter
		return 0;
	}

	// Copier nom fichier
	memcpy( nouveauFichier,
		fichier,
		strlen( fichier ) );

	// Si on a deja des fichiers, on conserve leurs adresses dans un tableau plus grand
	if( repertoire->m_nombreFichiers > 0 )
	{
		// Creer le nouveau contenant
		if( !( nouveauContenant = calloc( repertoire->m_nombreFichiers + 1,
			sizeof( char* ) ) ) )
		{
			// Notifier
			perror( "Impossible d'allouer la memoire: " );

			// Liberer
			free( nouveauFichier );

			// Quitter
			return 0;
		}

		// Copier les adresses
		for( indexAjout = 0; indexAjout < repertoire->m_nombreFichiers; indexAjout++ )
			nouveauContenant[ indexAjout ] = repertoire->m_fichiers[ indexAjout ];

		// Liberer
		free( repertoire->m_fichiers );

		// Remplacer
		repertoire->m_fichiers = nouveauContenant;
	}
	// Aucun fichier au depart, on alloue et on copie directement
	else
	{
		// Allouer la memoire
		if( !( repertoire->m_fichiers = calloc( 1,
			sizeof( char* ) ) ) )
		{
			// Notifier
			perror( "Impossible d'allouer la memoire: " );

			// Liberer
			free( nouveauFichier );

			// Quitter
			return 0;
		}

		// Enregistrer l'index d'ajout
		indexAjout = 0;
	}

	// Copier
	repertoire->m_fichiers[ indexAjout ] = nouveauFichier;

	// Incrementer nombre fichiers
	repertoire->m_nombreFichiers++;

	// OK
	return 1;
}

int ajouterRepertoireResultat( Resultat *resultat,
	Repertoire *repertoire )
{
	// Index d'ajout
	unsigned int indexAjout;

	// Nouveau contenant
	Repertoire **nouveauContenant;

	// Si on a deja des repertoires, il faut conserver leurs adresses dans un tableau plus grand
	if( resultat->m_nombreRepertoires > 0 )
	{
		// Allouer la memoire
		if( !( nouveauContenant = calloc( resultat->m_nombreRepertoires + 1,
			sizeof( Repertoire* ) ) ) )
		{
			// Notifier
			perror( "Impossible d'allouer la memoire: " );

			// Quitter
			return 1;
		}

		// Copier les adresses
		for( indexAjout = 0; indexAjout < resultat->m_nombreRepertoires; indexAjout++ )
			nouveauContenant[ indexAjout ] = resultat->m_repertoires[ indexAjout ];

		// Liberer ancien contenant
		free( resultat->m_repertoires );

		// Remplacer
		resultat->m_repertoires = nouveauContenant;
	}
	// Aucun repertoire au depart, on alloue et on ajoute
	else
	{
		// Allouer la memoire
		if( !( resultat->m_repertoires = calloc( 1,
			sizeof( Repertoire* ) ) ) )
		{
			// Notifier
			perror( "Impossible d'allouer la memoire: " );

			// Quitter
			return 0;
		}

		// Index d'ajout 0
		indexAjout = 0;
	}

	// Ajouter
	resultat->m_repertoires[ indexAjout ] = repertoire;

	// Incrementer le nombre de repertoires
	resultat->m_nombreRepertoires++;

	// OK
	return 1;
}

int compterFichiersResultat( const Resultat *resultat )
{
	// Iterateur
	unsigned int i = 0;

	// Sortie
	unsigned int sortie = 0;

	// Compter
	for( ; i < resultat->m_nombreRepertoires; i++ )
		sortie += resultat->m_repertoires[ i ]->m_nombreFichiers;

	// OK
	return sortie;
}

void updateNombreFichiersResultat( Resultat *resultat )
{
	// Compter/Mettre a jour
	resultat->m_nombreTotalFichiers = compterFichiersResultat( resultat );
}

int listerRepertoire( const char *repertoire,
	Resultat *resultat )
{
	// Handle de la recherche
	HANDLE handle;

	// Donnees fichier
	struct _WIN32_FIND_DATAA donnees;

	// Repertoire recursivite
	char *repertoireRecursivite = NULL;

	// Contenu repertoire
	Repertoire *contenuRepertoire;

	// Filtre recherche
	char *filtreRecherche;

	// Creer le filtre
	// Allouer la memoire
	if( !( filtreRecherche = calloc( strlen( repertoire )
		+ 2 // "/*"
		+ 1, // '\0'
		sizeof( char ) ) ) )
	{
		// Notifier
		perror( "Impossible d'allouer la memoire: " );

		// Quitter
		return 1;
	}
	// Creer
	// Base
	memcpy( filtreRecherche,
		repertoire,
		strlen( repertoire ) );
	// Filtre
	memcpy( filtreRecherche + strlen( repertoire ),
		"/*",
		2 );


	// Lancer la recherche
	if( ( handle = FindFirstFileA( filtreRecherche,
		&donnees ) ) == INVALID_HANDLE_VALUE )
	{
		// Notifier
		perror( "Impossible de lancer la recherche: " );

		// Liberer
		free( filtreRecherche );

		// Quitter
		return 0;
	}

	// Liberer filtre
	free( filtreRecherche );

	// Allouer la memoire pour le contenu du repertoire
	if( !( contenuRepertoire = calloc( 1,
		sizeof( Repertoire ) ) ) )
	{
		// Notifier
		perror( "Impossible d'allouer la memoire: " );

		// Fermer la recherche
		FindClose( handle );

		// Quitter
		return 0;
	}

	// Copier le nom du repertoire
	// Allouer
	if( !( contenuRepertoire->m_nom = calloc( strlen( repertoire ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		perror( "Impossible d'allouer la memoire: " );

		// Liberer la memoire
		free( contenuRepertoire );

		// Fermer la recherche
		FindClose( handle );

		// Quitter
		return 0;
	}
	// Copier
	memcpy( contenuRepertoire->m_nom,
		repertoire,
		strlen( repertoire ) );


#define NOTIFICATION_ELEMENT( TYPE, RETOUR_LIGNE ) \
	printf( TYPE"\t[%s/%s]%s", \
		repertoire, \
		donnees.cFileName, \
		RETOUR_LIGNE ? "\n" : "" )

	// Chercher
	do
	{
		// Recuperer le nom du fichier
		if( !( donnees.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
		{
			// Filtrer fichiers
			if( estFichierAutorise( donnees.cFileName )
				|| _strcmpi( repertoire,
					"." ) )
			{
				// Notifier
				NOTIFICATION_ELEMENT( "[INDEX]", 1 );

				// Ajouter
				if( !ajouterFichierRepertoire( contenuRepertoire,
					donnees.cFileName ) )
				{
					// Notifier
					printf( "Impossible d'ajouter un fichier au repertoire.\n" );

					// Liberer repertoire
					libererRepertoire( contenuRepertoire );

					// Liberer
					free( contenuRepertoire );

					// Fermer recherche
					FindClose( handle );

					// Quitter
					return 0;
				}
			}
			else
				NOTIFICATION_ELEMENT( "[IGNORE]", 1 );
		}
		else
		{
			// Notifier
			NOTIFICATION_ELEMENT( "[REP] ->", 0 );

			// Ignorer au besoin
			if( ( estDossierAutorise( donnees.cFileName )
				|| _strcmpi( repertoire,
					"." ) )
				&& strcmp( donnees.cFileName,
					"." )
				&& strcmp( donnees.cFileName,
					".." ) )
			{
				// Retour a la ligne
				puts( "" );

				// Creer le lien
				// Allouer la memoire
				if( !( repertoireRecursivite = calloc( strlen( repertoire )
					+ 1 // '/'
					+ strlen( donnees.cFileName ) // NOM REPERTOIRE
					+ 1, // '\0'
					sizeof( char ) ) ) )
				{
					// Notifier
					perror( "Impossible d'allouer la memoire: " );

					// Liberer le repertoire en cours
					libererRepertoire( contenuRepertoire );

					// Liberer
					free( contenuRepertoire );

					// Fermer la recherche
					FindClose( handle );

					// Quitter
					return 0;
				}
				// Copier
				// Repertoire courant
				memcpy( repertoireRecursivite,
					repertoire,
					strlen( repertoire ) );
				// '/'
				memset( repertoireRecursivite + strlen( repertoire ),
					'/',
					1 );
				// Repertoire a explorer
				memcpy( repertoireRecursivite + strlen( repertoire ) + 1,
					donnees.cFileName,
					strlen( donnees.cFileName ) );

				// Recursivite
				if( !listerRepertoire( repertoireRecursivite,
					resultat ) )
				{
					// Notifier
					printf( "[ATTENTION] Impossible de lister [%s].\n",
						repertoireRecursivite );

					// Liberer le repertoire en cours
					//libererRepertoire( contenuRepertoire );
					//
					// Liberer
					//free( contenuRepertoire );
					//
					// Liberer lien
					//free( repertoireRecursivite );
					//
					// Fermer recherche
					//FindClose( handle );
					//
					// Quitter
					//return 0;
				}

				// Liberer lien
				free( repertoireRecursivite );
			} // DOSSIER ET !AUTORISE
			else
				printf( " [IGN]\n" );
		} // REPERTOIRE
	} while( FindNextFileA( handle,
		&donnees ) );

	// Fermer la recherche
	FindClose( handle );

	// Ajouter le repertoire aux resultats
	if( !ajouterRepertoireResultat( resultat,
		contenuRepertoire ) )
	{
		// Notifier
		printf( "Impossible d'ajouter le repertoire au resultat." );

		// Liberer repertoire
		libererRepertoire( contenuRepertoire );

		// Quitter
		return 0;
	}

	// OK
	return 1;
}

void ecrireRepertoireHTML( FILE *fichier,
	Repertoire *repertoire )
{
	// Iterateur
	unsigned int i = 0;

	// Debut section
	fputs( "<section>\n", fichier );

	// Titre
	fprintf( fichier,
		"<h2>[%s] (<em>%d fichier%s</em>)</h2>\n",
		repertoire->m_nom,
		repertoire->m_nombreFichiers,
		( ( repertoire->m_nombreFichiers > 1 ) ? "s" : "" ) );

	// Ouvrir tableau
	if( repertoire->m_nombreFichiers > 0 )
		fprintf( fichier,
			"<table>\n<tr>\n<th>Fichier%s</th><th>Taille</th></tr>\n",
				( repertoire->m_nombreFichiers > 1 ) ? "s" : "" );
	// Notifier le vide
	else
		fputs( "<p style=\"text-align: center;\"><strong>VIDE</strong></p>", fichier );

	// Lister fichiers
	for( ; i < repertoire->m_nombreFichiers; i++ )
	{
		// Debut ligne
		fputs( "<tr>\n", fichier );

		// Ligne
		ecrireFichierHTML( repertoire->m_nom,
			repertoire->m_fichiers[ i ],
			fichier );

		// Fin ligne
		fputs( "</tr>\n", fichier );
	}

	// Fermer tableau
	if( repertoire->m_nombreFichiers > 0 )
		fputs( "</table>\n", fichier );

	// Fin section
	fputs( "</section>\n", fichier );
}

void ecrireContenuHTML( FILE *fichier,
	Resultat *resultat )
{
	// Iterateur
	signed int i = 0;

	// Verifier
	if( !resultat->m_nombreRepertoires )
		return;

	// Ecrire les repertoires a l'envers (combattons la recursivite)
	for( i = (signed int)( resultat->m_nombreRepertoires - 1 ); i >= 0 ; i-- )
		ecrireRepertoireHTML( fichier,
			resultat->m_repertoires[ i ] );
}

int UpdateList_Update( void )
{
	// HTML
	FILE *fichier;

	// Resultat du traitement
	Resultat *resultat = NULL;

	// Parametrer console
	system( "@echo off" );
	system( "title Listeur de ressources (Sortie dans index.html)" );
	system( "color 0F" );

	// Notifier
	printf( "Debut de l'indexation (REP: Repertoire/INDEX: Indexe/IGNORE: Ignore)\n\n" );

	// Allouer le resultat
	if( !( resultat = calloc( 1,
		sizeof( Resultat ) ) ) )
	{
		// Notifier
		perror( "Impossible d'allouer la memoire: " );

		// Pause pour lire
		system( "pause>nul" );

		// Quitter
		return EXIT_FAILURE;
	}

	// Lancer la recherche
	if( !listerRepertoire( ".",
		resultat ) )
	{
		// Notifier
		printf( "Impossible de lister le repertoire.\n" );

		// Liberer
		free( resultat );

		// Pause pour lire
		system( "pause>nul" );

		// Quitter
		return EXIT_FAILURE;
	}

	// Notifier update nombre fichiers
	printf( "\nUpdate nombre fichiers: " );

	// Mettre a jour le compte de fichier
	updateNombreFichiersResultat( resultat );

	// Notifier fin update
	printf( " %d fichier(s) dans %d dossier(s).\n",
		resultat->m_nombreTotalFichiers,
		resultat->m_nombreRepertoires );

	// Notifier fin indexation
	printf( "\nIndexation terminee.\n\n" );

	// Notifier debut ecriture index
	printf( "Ecriture de l'index...\t" );

	// Ouvrir le HTML
	if( !( fichier = fopen( FICHIER_SORTIE,
		"wb+" ) ) )
	{
		// Notifier
		perror( "[ERREUR]\n\nImpossible de creer le fichier de sortie ["FICHIER_SORTIE"]: " );

		// Liberer le resultat
		libererResultat( resultat );

		// Liberer la memoire
		free( resultat );

		// Pause pour lire
		system( "pause>nul" );

		// Quitter
		return EXIT_FAILURE;
	}

	// Ecrire l'entete du fichier
	ecrireEnteteHTML( fichier,
		resultat );

	// Ecrire contenu du fichier
	ecrireContenuHTML( fichier,
		resultat );

	// Ecrire le pied du fichier
	ecrirePiedHTML( fichier );

	// Fermer le fichier
	fclose( fichier );

	// Notifier fin d'ecriture de l'index
	printf( "[FAIT] (dans %s)\n",
		FICHIER_SORTIE );

	// Notifier nettoyage
	printf( "Nettoyage...\t\t" );

	// Liberer resultat
	// Contenu
	libererResultat( resultat );
	// Contenant
	free( resultat );

	// Notifier fin nettoyage
	printf( "[TERMINE]\n" );

	// Outil IPv4
	//system( "echo." );
	//system( "echo Votre IP est la suivante: " );
	//system( "echo." );
	//system( "ipconfig | findstr /R /C:\"Adresse IPv4\"" );
	//system( "echo." );

	// Fin du programme avec pause
	//system( "echo Appuyez sur une touche pour fermer le programme." );
	//system( "pause>nul" );

	// OK
	return EXIT_SUCCESS;
}

