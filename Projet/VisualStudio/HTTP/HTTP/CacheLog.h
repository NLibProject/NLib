#ifndef CACHELOG_PROTECT
#define CACHELOG_PROTECT

// struct CacheLog
typedef struct
{
	// Entrees a loguer
	EntreeCacheLog **m_entree;

	// Nombre d'entrees
	unsigned int m_nombreEntree;

	// Mutex
#ifdef IS_WINDOWS
	HANDLE m_mutex;
#endif // IS_WINDOWS
} CacheLog;

// Construire le cache
__ALLOC CacheLog *CacheLog_Construire( void );

// Detruire le cache
void CacheLog_Detruire( CacheLog** );

// Ajouter une entree au cache
BOOL CacheLog_AjouterEntree( CacheLog*,
	__WILLBEOWNED EntreeCacheLog *entree );

// Update le cache
void CacheLog_Update( CacheLog* );

#endif // !CACHELOG_PROTECT

