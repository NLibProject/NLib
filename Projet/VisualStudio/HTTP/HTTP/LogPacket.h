#ifndef LOGPACKET_PROTECT
#define LOGPACKET_PROTECT

#define REPERTOIRE_LOG_PACKET		"Packet"

#define NOM_FICHIER_LOG_PACKET_ERREUR		"__"

// Loguer packet
void LogPacket_LoguerPacket( const char *rawData,
	unsigned int taille,
	const char *ip );

#endif // !LOGPACKET_PROTECT

