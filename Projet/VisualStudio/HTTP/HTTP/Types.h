#ifndef TYPES_PROTECT
#define TYPES_PROTECT

// Base
#define _CRT_SECURE_NO_WARNINGS
#define _FILE_OFFSET_BITS 64 // (ftello/fseeko) !WINDOWS | (_fseeki64 _ftelli64) WINDOWS 
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <direct.h>

// Definition preprocesseur
#include "Preprocessor.h"

// Warnings en moins
#ifdef IS_WINDOWS
#pragma warning( disable:4706 ) // assignation au sein d'une expression conditionnelle
#endif

// Reseau
#ifdef IS_WINDOWS
// Winsock
#include <WinSock2.h>
#include <WS2tcpip.h>
#else // IS_WINDOWS

#endif // !IS_WINDOWS

// type BOOLEAN
#undef DEFINE_BOOL
#include "Boolean.h"

// Caractere
#include "Caractere.h"

// Donnees
#include "Data.h"

// File manipulation functions
#include "FileManipulation.h"

// String manipulation functions
#include "StringManipulation.h"

// Reseau
#include "Reseau.h"

// SocketSelection
#include "SocketSelection.h"

// struct Packet
#include "Packet.h"

// struct CachePacket
#include "CachePacket.h"

// enum TypeEntreeLog
#include "TypeEntreeLog.h"

// Log
#include "Log.h"

// Entree dans le cache
#include "EntreeCacheLog.h"

// Cache log
#include "CacheLog.h"

// struct ClientServeur
#include "ClientServeur.h"

// PacketIO
#include "PacketIO.h"

// Thread client serveur
#include "ThreadClientServeur.h"

// struct Serveur
#include "Serveur.h"

// Packet Entrant
#include "PacketEntrant.h"

// HTTPDef
#include "HTTPDef.h"

// HTTPProcess
#include "HTTPProcess.h"

// Caractere HTML
#include "CaractereHTML.h"

// Update list
#include "UpdateList.h"

// enum CommandeServeur
#include "CommandeServeur.h"

// Gestion commandes
#include "GestionCommandeServeur.h"

// Thread acceptation client serveur
#include "ThreadAcceptationClientServeur.h"

// enum HTTPSpecialTreatmentList
#include "HTTPSpecialTreatmentList.h"

// Traitement des cas particuliers
#include "HTTPSpecialTreatment.h"

// Enregistrer les packets bruts
#include "LogPacket.h"

#endif // !TYPES_PROTECT

