#include <time.h>
#include "Types.h"

int main( int argc,
	char *argv[ ] )
{
	// Serveur
	Serveur *serveur = NULL;

	// Referencer arguments
	REFERENCER( argc );
	REFERENCER( argv );

	// Configurer la console
	system( "color 0F" );

	// Initialiser rand
	srand( (unsigned int)time( NULL ) );

	// Initialiser le reseau
	if( !Reseau_Initialiser( ) )
	{
		// Notifier
		printf( "main( )::Impossible d'initialiser le reseau.\n" );

		// Quitter
		return EXIT_FAILURE;
	}

	// Creer le serveur
	if( !( serveur = Serveur_Construire( PORT_SERVEUR ) ) )
	{
		// Notifier
		printf( "main( )::Impossible de creer le serveur.\n" );

		// Fermer
		Reseau_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Afficher l'aide
	GestionCommandeServeur_AfficherAide( );

	// Boucle commandes
	do
	{
		// Gerer les commandes
		GestionCommandeServeur_Gerer( serveur );

		// Delais
		Sleep( 1 );
	} while( Serveur_EstEnCours( serveur ) );

	// Detruire le serveur
	Serveur_Detruire( &serveur );

	// Detruire le reseau
	Reseau_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

