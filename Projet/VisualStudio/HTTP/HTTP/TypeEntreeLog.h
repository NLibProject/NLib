#ifndef TYPEENTREELOG_PROTECT
#define TYPEENTREELOG_PROTECT

// enum TypeEntreeLog
typedef enum
{
	TYPE_ENTREE_LOG_DEMARRAGE,
	TYPE_ENTREE_LOG_CONNEXION,
	TYPE_ENTREE_LOG_DECONNEXION,
	TYPE_ENTREE_LOG_AGENT_CLIENT,
	TYPE_ENTREE_LOG_DEMANDE_FICHIER,
	TYPE_ENTREE_LOG_STATUT_ENVOI,
	TYPE_ENTREE_LOG_FIN_ENVOI,

	TYPES_ENTREE_LOG
} TypeEntreeLog;

#ifdef TYPEENTREELOG_INTERNE
static const char TypeEntreeLogTexte[ TYPES_ENTREE_LOG ][ 32 ] =
{
	"Demarrage ser",
	"Connexion cli",
	"Deconnexion c",
	"Agent client ",
	"Demande fich.",
	"Envoi fichier",
	"Fin envoi fic"
};
#endif // TYPEENTREELOG_INTERNE

// Obtenir type entree log
const char *TypeEntreeLog_ObtenirTypeEntree( TypeEntreeLog );

#endif // !TYPEENTREELOG_PROTECT

