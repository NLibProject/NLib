#include "Types.h"

// Determiner le type d'un fichier
HTTPTypeFlux HTTPProcess_ObtenirTypeFichier( const char *fichierDemande )
{
	// Extension fichier
	char *extension;

	// Sortie
	__OUTPUT HTTPTypeFlux sortie;

	// Lire l'extension
	if( !( extension = FileManipulation_GetExtension( fichierDemande ) ) )
		return HTTP_TYPE_FLUX_TELECHARGEMENT;

	// Parser
	sortie = HTTPDef_ParserExtensionFichier( extension );

	// Liberer
	FREE( extension );

	// OK
	return sortie;
}

// Notifier
void HTTPProcess_NotifierDemande( BOOL estFichierExiste,
	HTTPTypeFlux typeFlux,
	const char *nomFichier,
	unsigned int tailleFichier,
	const ClientServeur *client )
{
	// Temps
	char *temps;

	// Obtenir temps
	temps = Log_ObtenirTemps( 0 );

	// Notifier
	printf( "[%s][%s] (%d) demande \"%s\" qui %s",
		temps,
		ClientServeur_ObtenirAdresseIP( client ),
		ClientServeur_ObtenirIdentifiant( client ),
		nomFichier,
		estFichierExiste ? "existe " : "n'existe pas\n" );

	// Liberer
	FREE( temps );

	// Fichier existe, on detaille
	if( estFichierExiste )
		printf( "(%d octets, type=%s)\n",
			tailleFichier,
			HTTPDef_ObtenirTypeFlux( typeFlux ) );

	// Loguer
	CacheLog_AjouterEntree( client->m_callbackCacheLog,
		EntreeCacheLog_Construire( ClientServeur_ObtenirAdresseIP( client ),
			ClientServeur_ObtenirIdentifiant( client ),
			TYPE_ENTREE_LOG_DEMANDE_FICHIER,
			NULL,
			nomFichier,
			0,
			0,
			0,
			0 ) );
}

// Compose les informations de l'header
void HTTPProcess_ComposerInformationsHeaderPacketReponseGET( Packet *p,
	HTTPTypeFlux typeFlux,
	unsigned long tailleFichier,
	BOOL estErreur )
{
	// Buffer
	char buffer[ 2048 ] = { 0, };

	// Code reponse
	const char *codeReponse = estErreur ?
		" "NOT_FOUND_HTTP_TEXT"\n"
		: " "OK_HTTP_TEXT"\n";

	// Ajouter la version
	Packet_AjouterData( p,
		VERSION_HTTP,
		strlen( VERSION_HTTP ) );

	// Code de retour (verifie en amont, donc toujours 200)
	Packet_AjouterData( p,
		codeReponse,
		strlen( codeReponse ) );

	// Ajouter serveur
	// Mot clef
	Packet_AjouterData( p,
		HTTPDef_ObtenirAnswerKeyword( HTTP_ANSWER_KEYWORD_SERVER ),
		strlen( HTTPDef_ObtenirAnswerKeyword( HTTP_ANSWER_KEYWORD_SERVER ) ) );
	// Separateurs
	Packet_AjouterData( p,
		": ",
		strlen( ": " ) );
	// Nom serveur
	Packet_AjouterData( p,
		NOM_SERVEUR,
		strlen( NOM_SERVEUR ) );
	// Retour a la ligne
	Packet_AjouterData( p,
		"\n",
		strlen( "\n" ) );

	/* Ajouter accept ranges */
	// Mot clef
		Packet_AjouterData( p,
			HTTPDef_ObtenirAnswerKeyword( HTTP_ANSWER_KEYWORD_ACCEPT_RANGES ),
			strlen( HTTPDef_ObtenirAnswerKeyword( HTTP_ANSWER_KEYWORD_ACCEPT_RANGES ) ) );
	// Separateurs
		Packet_AjouterData( p,
			": ",
			strlen( ": " ) );
	// bytes
		Packet_AjouterData( p,
			"bytes\n",
			strlen( "bytes\n" ) );

	/* Ajouter le type */
	// Inscrire mot clef
		Packet_AjouterData( p,
			HTTPDef_ObtenirAnswerKeyword( HTTP_ANSWER_KEYWORD_CONTENT_TYPE ),
			strlen( HTTPDef_ObtenirAnswerKeyword( HTTP_ANSWER_KEYWORD_CONTENT_TYPE ) ) );
	// Separateurs
		Packet_AjouterData( p,
			": ",
			strlen( ": " ) );
	// Type
		Packet_AjouterData( p,
			HTTPDef_ObtenirTypeFlux( typeFlux ),
			strlen( HTTPDef_ObtenirTypeFlux( typeFlux ) ) );
	// Retour a la ligne
		Packet_AjouterData( p,
			"\n",
			strlen( "\n" ) );

	/* Ajouter la taille */
	// Bufferiser
		sprintf( buffer,
			"%lu",
			tailleFichier );
	// Inscrire mot clef
		Packet_AjouterData( p,
			HTTPDef_ObtenirAnswerKeyword( HTTP_ANSWER_KEYWORD_CONTENT_LENGTH ),
			strlen( HTTPDef_ObtenirAnswerKeyword( HTTP_ANSWER_KEYWORD_CONTENT_LENGTH ) ) );
	// Separateurs
		Packet_AjouterData( p,
			": ",
			strlen( ": " ) );
	// Taille
		Packet_AjouterData( p,
			buffer,
			strlen( buffer ) );
	// Retours a la ligne
		Packet_AjouterData( p,
			"\n\n",
			strlen( "\n\n" ) );
}

// Compose un packet de reponse pour GET reussi
__ALLOC Packet *HTTPProcess_ComposerHeaderPacketReponseGETStandardInterne( const char *fichierDemande,
	const ClientServeur *client )
{
	// Packet
	__OUTPUT Packet *p;

	// Type de flux
	HTTPTypeFlux typeFlux = HTTP_TYPE_FLUX_TELECHARGEMENT;

	// Taille fichier
	unsigned long tailleFichier = 0;

	// Construire le packet
	if( !( p = Packet_Construire( ) ) )
	{
		// Notifier
		perror( "HTTPProcess_ComposerHeaderPacketReponseGETStandardInterne( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}


	// Determiner le type de flux
	typeFlux = HTTPProcess_ObtenirTypeFichier( fichierDemande );

	// Determiner la taille du fichier
	tailleFichier = FileManipulation_GetSize2( fichierDemande );

	// Ajouter l'header au packet
	HTTPProcess_ComposerInformationsHeaderPacketReponseGET( p,
		typeFlux,
		tailleFichier,
		FALSE );

	// Notifier
	HTTPProcess_NotifierDemande( TRUE,
		typeFlux,
		fichierDemande,
		tailleFichier,
		client );		
		
	// OK
	return p;
}

// Corriger les caracteres
void HTTPProcess_CorrigerCaracteresUrlInterne( __OUTPUT char *fichierEnvoye )
{
	// Buffer
	char buffer[ MAX_PATH ] = { 0, };

	// Curseur entree
	unsigned int curseurEntree = 0;

	// Position %XX
	unsigned int curseurPourcentage = 0;

	// Curseur buffer
	unsigned int curseurBuffer = 0;

	// Curseur precedent
	unsigned int curseurPrecedent = 0;
	
	// Iterateur
	unsigned int i;

	// Renommage?
	BOOL estChangementCaractere = FALSE;

	// Taille entree
	unsigned int tailleEntree;

	// Caracteres
	char caractere[ 2 ];

	// Calculer taille en entree
	tailleEntree = strlen( fichierEnvoye );

	// Tant qu'on trouve un espace
	while( curseurEntree < tailleEntree )
	{
		// Chercher %
		if( StringManipulation_LookForCharacter( fichierEnvoye,
			'%',
			&curseurPourcentage ) )
		{
			// Recuperer les deux caracteres
			caractere[ 0 ] = StringManipulation_GetNextCharacter( fichierEnvoye,
				&curseurPourcentage,
				FALSE );
			caractere[ 1 ] = StringManipulation_GetNextCharacter( fichierEnvoye,
				&curseurPourcentage,
				FALSE );

			// Copier
			for( i = curseurPrecedent; i < curseurPourcentage - 3; i++ )
				buffer[ curseurBuffer++ ] = fichierEnvoye[ i ];

			// Ajouter espace
			buffer[ curseurBuffer++ ] = ( ( (char)Caractere_ConvertirDecimal( caractere[ 0 ] ) ) << 4 ) | (char)( Caractere_ConvertirDecimal( caractere[ 1 ] ) );

			// Enregistrer
			curseurPrecedent = curseurPourcentage;
			curseurEntree = curseurPourcentage;

			// Il y a eu suppression de caractere
			estChangementCaractere = TRUE;
		}
		// Pas d'espace
		else
			// Copier caractere
			for( curseurEntree = curseurPrecedent; curseurEntree < tailleEntree; curseurEntree++ )
				buffer[ curseurBuffer++ ] = fichierEnvoye[ curseurEntree ];		
	}

	// Si il y a eu un renommage
	if( estChangementCaractere )
	{
		// Notifier modification nom
		printf( "[SERVEUR] Renomme \"%s\" en \"%s\"\n",
			fichierEnvoye,
			buffer );

		// Vider l'entree
		memset( fichierEnvoye,
			0,
			MAX_PATH );

		// Copier dans l'entree
		memcpy( fichierEnvoye,
			buffer,
			strlen( buffer ) );
	}
}

// Traiter une requete GET
__ALLOC Packet **HTTPProcess_TraiterPacketGETInterne( const char *data,
	unsigned int taille,
	unsigned int *curseur,
	const ClientServeur *client )
{
	// Fichier demande
	char *fichierDemande;

	// Agent
	char *agent;

	// Buffer
	char *buffer;

	// Temps
	char *temps;

	// Fichier qui sera envoye au client
	char fichierEnvoye[ MAX_PATH + 1 ] = { 0, };

	// Traitement
	HTTPSpecialTreatmentList traitement;

	// Sortie
	__OUTPUT Packet **sortie;

	// Referencer
	REFERENCER( taille );

	// Lire le fichier demande
	if( !( fichierDemande = StringManipulation_ReadUntil( data,
		' ',
		curseur,
		FALSE ) ) )
	{
		// Notifier
		printf( "HTTPProcess_TraiterPacketInterne( )::Requete incorrecte.\n" );

		// Quitter
		return NULL;
	}

	// Verifier la taille
	if( strlen( fichierDemande ) >= MAX_PATH
		|| strlen( fichierDemande ) <= 0 )
	{
		// Notifier
		printf( "HTTPProcess_TraiterPacketGETInterne( )::Fichier incorrect.\n" );

		// Liberer
		FREE( fichierDemande );

		// Quitter
		return NULL;
	}

	// Si on veut le fichier par defaut (/), on donne FICHIER_PAR_DEFAUT_SERVEUR
	if( !strcmp( fichierDemande,
		"/" ) )
		memcpy( fichierEnvoye,
			FICHIER_PAR_DEFAUT_SERVEUR,
			strlen( FICHIER_PAR_DEFAUT_SERVEUR ) );
	else
	{
		// Supprimer le '/' du debut
		memcpy( fichierEnvoye,
			fichierDemande + 1,
			strlen( fichierDemande ) - 1 );

		// Corriger les espaces
		HTTPProcess_CorrigerCaracteresUrlInterne( fichierEnvoye );
	}

	// Allouer la memoire
	if( !( sortie = calloc( HTTP_PROCESS_PACKETS,
		sizeof( Packet* ) ) ) )
	{
		// Notifier
		perror( "HTTPProcess_TraiterPacketGETInterne( )::Impossible d'allouer la memoire: " );

		// Liberer
		FREE( fichierDemande );

		// Quitter
		return NULL;
	}

	// Chercher l'agent
	if( StringManipulation_LookForString( data,
		HTTPDef_ObtenirRequestKeyword( HTTP_REQUEST_KEYWORD_USER_AGENT ),
		curseur ) )
	{
		// Se placer aux ':'
		StringManipulation_LookForCharacter( data,
			':',
			curseur );

		// Recuperer l'agent
		if( ( agent = StringManipulation_ReadUntil( data,
			'\n',
			curseur,
			FALSE ) ) != NULL )
		{
			// Verifier la taille
			if( strlen( agent ) > 1 )
			{
				// Corriger si necessaire
				if( agent[ strlen( agent ) - 1 ] == '\r' )
					agent[ strlen( agent ) - 1 ] = '\0';

				// Obtenir temps
				temps = Log_ObtenirTemps( 0 );

				// Lire l'agent
				printf( "[%s][%s] (%d) agent = [%s]\n",
					temps,
					ClientServeur_ObtenirAdresseIP( client ),
					ClientServeur_ObtenirIdentifiant( client ),
					agent );

				// Liberer
				FREE( temps );

				// Loguer
				CacheLog_AjouterEntree( client->m_callbackCacheLog,
					EntreeCacheLog_Construire( ClientServeur_ObtenirAdresseIP( client ),
						client->m_identifiantUnique,
						TYPE_ENTREE_LOG_AGENT_CLIENT,
						agent,
						NULL,
						0,
						0,
						0,
						0 ) );
			}

			// Liberer
			FREE( agent );
		}
	}

	// Determiner le type d'action
	switch( ( traitement = HTTPSpecialTreatmentList_GetTreatment( fichierEnvoye ) ) )
	{
		case HTTP_SPECIAL_TREATMENT_FAVICON:
			// A VOIR SI BESOIN (pour l'instant osef)
		case HTTP_SPECIAL_TREATMENT_404_PAGE:
			// Composer l'header du message de reponse
			if( !( sortie[ HTTP_PROCESS_PACKET_HEADER ] = HTTPSpecialTreatment_Generate404Header( fichierEnvoye ) ) )
			{
				// Notifier
				printf( "HTTPProcess_TraiterPacketGETInterne( )::Impossible de creer l'header de reponse.\n" );

				// Liberer
				FREE( fichierDemande );
				FREE( sortie );

				// Quitter
				return NULL;
			}

			// Recuperer le contenu
			if( !( sortie[ HTTP_PROCESS_PACKET_CONTENT ] = HTTPSpecialTreatment_Generate404Content( fichierEnvoye ) ) )
			{
				// Notifier
				printf( "HTTPProcess_TraiterPacketGETInterne( )::Impossible de creer le contenu de la reponse.\n" );

				// Liberer
				FREE( fichierDemande );
				Packet_Detruire( &sortie[ HTTP_PROCESS_PACKET_HEADER ] );
				FREE( sortie );

				// Quitter
				return NULL;
			}
			break;

		case HTTP_SPECIAL_TREATMENT_GET_DESCRIPTION_PAGE:
			// Composer l'header du message de reponse
			if( !( sortie[ HTTP_PROCESS_PACKET_HEADER ] = HTTPSpecialTreatment_GenerateDescriptionHeader( ) ) )
			{
				// Notifier
				printf( "HTTPProcess_TraiterPacketGETInterne( )::Impossible de creer l'header de reponse.\n" );

				// Liberer
				FREE( fichierDemande );
				FREE( sortie );

				// Quitter
				return NULL;
			}

			// Recuperer le contenu
			if( !( sortie[ HTTP_PROCESS_PACKET_CONTENT ] = HTTPSpecialTreatment_GenerateDescriptionContent( ) ) )
			{
				// Notifier
				printf( "HTTPProcess_TraiterPacketGETInterne( )::Impossible de creer le contenu de la reponse.\n" );

				// Liberer
				FREE( fichierDemande );
				Packet_Detruire( &sortie[ HTTP_PROCESS_PACKET_HEADER ] );
				FREE( sortie );

				// Quitter
				return NULL;
			}
			break;

		case HTTP_SPECIAL_TREATMENT_GET_SUBDIR:
		case HTTP_SPECIAL_TREATMENT_ADD_HTML:
			switch( traitement )
			{
				case HTTP_SPECIAL_TREATMENT_ADD_HTML:
					// Allouer la memoire
					if( !( buffer = calloc( strlen( fichierEnvoye ) + strlen( HTTPDef_ObtenirExtensionTypeFichier( EXTENSION_PAGE_WEB_HTML ) ) + 1,
						sizeof( char ) ) ) )
					{
						// Notifier
						perror( "HTTPSpecialTreatmentList_GetTreatment( )::Impossible d'allouer la memoire: " );

						// Liberer
						FREE( fichierDemande );
						FREE( sortie );

						// Quitter
						return NULL;
					}

					// Copier
					memcpy( buffer,
						fichierEnvoye,
						strlen( fichierEnvoye ) );
					memcpy( buffer + strlen( fichierEnvoye ),
						".",
						sizeof( char ) );
					memcpy( buffer + 1 + strlen( fichierEnvoye ),
						HTTPDef_ObtenirExtensionTypeFichier( EXTENSION_PAGE_WEB_HTML ),
						strlen( HTTPDef_ObtenirExtensionTypeFichier( EXTENSION_PAGE_WEB_HTML ) ) );
					break;

				case HTTP_SPECIAL_TREATMENT_GET_SUBDIR:
					// Allouer la memoire
					if( !( buffer = calloc( strlen( fichierEnvoye ) + 1 /* / */ + strlen( FICHIER_PAR_DEFAUT_SERVEUR ) + 1,
						sizeof( char ) ) ) )
					{
						// Notifier
						perror( "HTTPSpecialTreatmentList_GetTreatment( )::Impossible d'allouer la memoire: " );

						// Liberer
						FREE( fichierDemande );
						FREE( sortie );

						// Quitter
						return NULL;
					}
					// Copier
					memcpy( buffer,
						fichierEnvoye,
						strlen( fichierEnvoye ) );
					memcpy( buffer + strlen( fichierEnvoye ),
						"/",
						sizeof( char ) );
					memcpy( buffer + strlen( fichierEnvoye ) + 1,
						FICHIER_PAR_DEFAUT_SERVEUR,
						strlen( FICHIER_PAR_DEFAUT_SERVEUR ) );
					break;

				default:
					FORGET( buffer );
					break;
			}

			// Composer l'header du message de reponse
			if( !( sortie[ HTTP_PROCESS_PACKET_HEADER ] = HTTPProcess_ComposerHeaderPacketReponseGETStandardInterne( buffer,
				client ) ) )
			{
				// Notifier
				printf( "HTTPProcess_TraiterPacketGETInterne( )::Impossible de creer l'header de reponse.\n" );

				// Liberer
				FREE( buffer );
				FREE( fichierDemande );
				FREE( sortie );

				// Quitter
				return NULL;
			}

			// Recuperer le contenu
			if( !( sortie[ HTTP_PROCESS_PACKET_CONTENT ] = Packet_Construire3( buffer ) ) )
			{
				// Notifier
				printf( "HTTPProcess_TraiterPacketGETInterne( )::Impossible de creer le contenu de la reponse.\n" );

				// Liberer
				FREE( buffer );
				FREE( fichierDemande );
				Packet_Detruire( &sortie[ HTTP_PROCESS_PACKET_HEADER ] );
				FREE( sortie );

				// Quitter
				return NULL;
			}

			// Liberer
			FREE( buffer );
			break;

		case HTTP_SPECIAL_TREATMENT_NO_SPECIAL:
		default:
			// Composer l'header du message de reponse
			if( !( sortie[ HTTP_PROCESS_PACKET_HEADER ] = HTTPProcess_ComposerHeaderPacketReponseGETStandardInterne( fichierEnvoye,
				client ) ) )
			{
				// Notifier
				printf( "HTTPProcess_TraiterPacketGETInterne( )::Impossible de creer l'header de reponse.\n" );

				// Liberer
				FREE( fichierDemande );
				FREE( sortie );

				// Quitter
				return NULL;
			}

			// Recuperer le contenu
			if( !( sortie[ HTTP_PROCESS_PACKET_CONTENT ] = Packet_Construire3( fichierEnvoye ) ) )
			{
				// Notifier
				printf( "HTTPProcess_TraiterPacketGETInterne( )::Impossible de creer le contenu de la reponse.\n" );

				// Liberer
				FREE( fichierDemande );
				Packet_Detruire( &sortie[ HTTP_PROCESS_PACKET_HEADER ] );
				FREE( sortie );

				// Quitter
				return NULL;
			}
			break;
	}

	// Liberer
	FREE( fichierDemande );

	// OK
	return sortie;
}

// Traiter un packet (privee)
__ALLOC Packet **HTTPProcess_TraiterPacketInterne( const char *data,
	unsigned int taille,
	const ClientServeur *client )
{
	// Curseur dans la chaine
	unsigned int curseur = 0;	

	// Type de requete lue
	char *typeRequete;

	// Type de requete parsee
	HTTPTypeRequete typeRequeteParsee;

	// Loguer
	LogPacket_LoguerPacket( data,
		taille,
		ClientServeur_ObtenirAdresseIP( client ) );

	// Lire la requete
		// GET
			// Lire
				if( !( typeRequete = StringManipulation_ReadUntil( data,
					' ',
					&curseur,
					FALSE ) ) )
				{
					// Notifier
					printf( "HTTPProcess_TraiterPacketInterne( )::Pas de requete.\n" );

					// Quitter
					return NULL;
				}
			// Parser
				typeRequeteParsee = HTTPDef_ParserTypeRequete( typeRequete );
			// Liberer
				FREE( typeRequete );
			// Verifier
				switch( typeRequeteParsee )
				{
					case HTTP_REQUETE_GET:
						return HTTPProcess_TraiterPacketGETInterne( data,
							taille,
							&curseur,
							client );

					default:
						// Notifier
						printf( "HTTPProcess_TraiterPacketInterne( )::Requete inconnue: %s.\n",
							typeRequete );

						// Quitter
						return NULL;
				}
			
}

// Traiter un packet [ HTTP_PROCESS_PACKETS ]
__ALLOC Packet **HTTPProcess_TraiterPacket( const Packet *packet,
	const ClientServeur *client )
{
	return HTTPProcess_TraiterPacketInterne( packet->m_data,
		packet->m_taille,
		client );
}

