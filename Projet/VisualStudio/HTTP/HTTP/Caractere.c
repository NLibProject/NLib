#include "Types.h"

// Est un chiffre?
BOOL Caractere_EstUnChiffre( char c )
{
	return ( c >= '0'
		&& c <= '9' );
}

// Est un chiffre hexadecimal?
BOOL Caractere_EstUnChiffreHexadecimal( char c )
{
	return ( ( c >= 'A'
				&& c <= 'F' )
			|| ( c >= 'a'
				&& c <= 'f' ) );
}

// Est une lettre?
BOOL Caractere_EstUneLettre( char c )
{
	return ( ( c >= 'A'
				&& c <= 'Z' )
			|| ( c >= 'a'
					&& c <= 'z' ) );
}

// Convertir un chiffre en decimal
unsigned int Caractere_ConvertirDecimal( char c )
{
	if( Caractere_EstUnChiffre( c ) )
		return c - '0';
	else
		if( Caractere_EstUnChiffreHexadecimal( c ) )
			switch( c )
			{
				case 'a':
				case 'A':
					return 0x0A;
				case 'b':
				case 'B':
					return 0x0B;
				case 'c':
				case 'C':
					return 0x0C;
				case 'd':
				case 'D':
					return 0x0D;
				case 'e':
				case 'E':
					return 0x0E;
				case 'f':
				case 'F':
					return 0x0F;

				default:
					return 0;
			}

	// Pas un chiffre
	return 0;
}

// Convertir un nombre en caractere entre 0 et F
char Caractere_ConvertirHexadecimal( unsigned char nb )
{
	// Verifier
	if( nb > 15 )
		return 'F';

	// Convertir
	if( nb < 10 )
		return '0' + (char)nb;
	else
		return 'A' + ( (char)nb - 10 );
}

// Est un caractere acceptable dans HTML
BOOL Caractere_EstUnCaractereViableHTMLHref( char c )
{
	// Caracteres toleres
	if( Caractere_EstUnChiffre( c )
		|| Caractere_EstUneLettre( c ) )
		return TRUE;
	else
		switch( c )
		{
			case '/':
			case '\\':
			case '.':
			case '_':
			case '-':
				return TRUE;

			default:
				return FALSE;
		}
}

