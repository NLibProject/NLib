#include "Types.h"

/* struct CachePacket */
// Construire cache packet
__ALLOC CachePacket *CachePacket_Construire( void )
{
	// Output
	__OUTPUT CachePacket *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( CachePacket ) ) ) )
	{
		// Notifier
		perror( "CachePacket_Construire( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Construire mutex
	if( !( out->m_mutex = CreateMutex( NULL,
		FALSE,
		NULL ) ) )
	{
		// Notifier
		perror( "CachePacket_Construire( )::Impossible de construire le mutex: " );

		// Liberer
		FREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

// Detruire cache packet
void CachePacket_Detruire( CachePacket **this )
{
	// Supprimer tous les packets
	while( CachePacket_ObtenirNombrePackets( *this ) > 0 )
		CachePacket_SupprimerPacket( *this );

	// Fermer mutex
	CloseHandle( (*this)->m_mutex );

	// Liberer la memoire
	FREE( (*this) );
}

// Obtenir le nombre de packets
unsigned int CachePacket_ObtenirNombrePackets( const CachePacket *this )
{
	// Resultat
	__OUTPUT unsigned int out;

	// Lock le mutex
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "CachePacket_ObtenirNombrePackets( )::Impossible de lock le mutex: " );

		// Quitter
		return 0;
	}

	// Copier le resultat
	out = this->m_nombrePacket;

	// Release le mutex
	ReleaseMutex( this->m_mutex );

	// OK
	return out;
}

// Obtenir le packet a envoyer
__ALLOC Packet *CachePacket_ObtenirPacket( const CachePacket *this )
{
	// Packet
	__OUTPUT Packet *packet;

	// Lock le mutex
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "CachePacket_ObtenirPacket( )::Impossible de lock le mutex: " );

		// Quitter
		return NULL;
	}

	// Verifier le contenu du tableau de packet
	if( this->m_nombrePacket == 0 )
	{
		// Notifier
		printf( "CachePacket_ObtenirPacket( )::Il n'y a aucun packet dans le cache.\n" );
		// Release mutex
		ReleaseMutex( this->m_mutex );

		// Quitter
		return NULL;
	}

	// Construire le packet
	if( !( packet = Packet_Construire2( this->m_packet[ 0 ] ) ) )
	{
		// Notifier
		printf( "CachePacket_ObtenirPacket( )::Impossible d'obtenir le packet.\n" );

		// Release le mutex
		ReleaseMutex( this->m_mutex );

		// Quitter
		return NULL;
	}

	// Copier statut
	packet->m_estFichier = this->m_packet[ 0 ]->m_estFichier;

	// Release le mutex
	ReleaseMutex( this->m_mutex );

	// OK
	return packet;
}

// Est packet(s) dans le cache?
BOOL CachePacket_EstPacketsDansCache( const CachePacket *this )
{
	return CachePacket_ObtenirNombrePackets( this ) > 0;
}

// Ajouter un packet
BOOL CachePacket_AjouterPacket( CachePacket *this,
	__WILLBEOWNED const Packet *packet )
{
	// Ancienne adresse
	Packet **ancienCache;

	// Index ajout
	unsigned int index;

	// Iterateur
	unsigned int i;

	// Lock le mutex
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "CachePacket_AjouterPacket( )::Impossible de lock le mutex: " );

		// Quitter
		return FALSE;
	}

	if( this->m_packet == NULL )
	{
		// Allouer le nouveau cache
		if( !( this->m_packet = calloc( 1,
			sizeof( Packet* ) ) ) )
		{
			// Notifier
			perror( "CachePacket_AjouterPacket( )::Impossible d'allouer la memoire: " );

			// Quitter
			return FALSE;
		}

		// Ajout a la case 0
		index = 0;
	}
	else
	{
		// Enregistrer ancien cache
		ancienCache = this->m_packet;

		// Allouer le nouveau cache
		if( !( this->m_packet = calloc( this->m_nombrePacket + 1,
			sizeof( Packet* ) ) ) )
		{
			// Notifier
			perror( "CachePacket_AjouterPacket( )::Impossible d'allouer la memoire: " );

			// Restaurer
			this->m_packet = ancienCache;

			// Release mutex
			ReleaseMutex( this->m_mutex );

			// Quitter
			return FALSE;
		}

		// Copier les packets
		for( i = 0; i < this->m_nombrePacket; i++ )
			this->m_packet[ i ] = ancienCache[ i ];

		// Ajout a la fin
		index = this->m_nombrePacket;
	}

	// Ajouter
	this->m_packet[ index ] = (Packet*)packet;

	// Incrementer le nombre de packets
	this->m_nombrePacket++;

	// Release le mutex
	ReleaseMutex( this->m_mutex );

	// OK
	return TRUE;
}

// Supprimer le packet
BOOL CachePacket_SupprimerPacket( CachePacket *this )
{
	// Ancien cache
	Packet **ancienCache;

	// Iterateur
	unsigned int i;

	// Lock mutex
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "CachePacket_SupprimerPacket( )::Impossible de lock le mutex: " );

		// Quitter
		return FALSE;
	}

	// Verifier le nombre de packet
	if( this->m_nombrePacket == 0 )
	{
		// Notifier
		printf( "CachePacket_SupprimerPacket( )::Il n'y a pas de packet dans le cache.\n" );

		// Release mutex
		ReleaseMutex( this->m_mutex );

		// Quitter
		return FALSE;
	}

	// Liberer la memoire
	Packet_Detruire( &this->m_packet[ 0 ] );

	// Si c'etait le dernier packet
	if( this->m_nombrePacket == 1 )
	{
		// Liberer le contenant
		FREE( this->m_packet );
	}
	else
	{
		// Enregistrer l'ancien cache
		ancienCache = this->m_packet;

		// Allouer la memoire
		if( !( this->m_packet = calloc( this->m_nombrePacket - 1,
			sizeof( Packet* ) ) ) )
		{
			// Notifier
			perror( "CachePacket_SupprimerPacket( )::Impossible d'allouer la memoire: " );

			// Restaurer
			this->m_packet = ancienCache;

			// Release mutex
			ReleaseMutex( this->m_mutex );

			// Quiter
			return FALSE;
		}

		// Copier
		for( i = 1; i < this->m_nombrePacket; i++ )
			this->m_packet[ i - 1 ] = ancienCache[ i ];
	}

	// Decrementer le nombre de packet
	this->m_nombrePacket--;

	// Release mutex
	ReleaseMutex( this->m_mutex );

	// OK
	return TRUE;
}
