#include "Types.h"

/* struct Serveur */

// Construire le serveur
__ALLOC Serveur *Serveur_Construire( unsigned short port )
{
	// Serveur
	__OUTPUT Serveur *out;

	// Creer le serveur
	if( !( out = calloc( 1,
		sizeof( Serveur ) ) ) )
	{
		// Notifier
		perror( "Serveur_Construire( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Creer le mutex
	if( !( out->m_mutex = CreateMutex( NULL,
		FALSE,
		NULL ) ) )
	{
		// Notifier
		perror( "Serveur_Construire( )::Impossible de creer le mutex: " );

		// Liberer
		FREE( out );

		// Quitter
		return NULL;
	}

	// Parametrer l'adressage
	out->m_adressageServeur.sin_family = AF_INET;
	out->m_adressageServeur.sin_port = htons( port );
	out->m_adressageServeur.sin_addr.S_un.S_addr = htonl( INADDR_ANY );

	// Creer la socket
	if( ( out->m_socket = socket( AF_INET,
		SOCK_STREAM,
		0 ) ) == INVALID_SOCKET )
	{
		// Notifier
		perror( "Serveur_Construire( )::Impossible de creer la socket: " );

		// Fermer mutex
		CloseHandle( out->m_mutex );

		// Liberer
		FREE( out );

		// Quitter
		return NULL;
	}

	// Bind la socket
	if( bind( out->m_socket,
		(const struct sockaddr*)&out->m_adressageServeur,
		sizeof( SOCKADDR_IN ) ) == SOCKET_ERROR )
	{
		// Notifier
		perror( "Serveur_Construire( )::Impossible de bind la socket: " );

		// Fermer la socket
		closesocket( out->m_socket );

		// Fermer mutex
		CloseHandle( out->m_mutex );

		// Liberer la memoire
		FREE( out );

		// Quitter
		return NULL;
	}

	// Ecouter les clients
	if( listen( out->m_socket,
		MAXIMUM_CLIENTS_SIMULTANES ) == SOCKET_ERROR )
	{
		// Notifier
		perror( "Serveur_Construire( )::Impossible d'ecouter sur la socket serveur: " );

		// Fermer la socket
		closesocket( out->m_socket );
		
		// Fermer mutex
		CloseHandle( out->m_mutex );

		// Liberer la memoire
		FREE( out );

		// Quitter
		return NULL;
	}

	// Demarrer le serveur
	out->m_estEnCours = TRUE;

	// Zero
	out->m_nombreClients = 0;

	// Creer le cache de log
	if( !( out->m_cacheLog = CacheLog_Construire( ) ) )
	{
		// Notifier
		printf( "Serveur_Construire( )::Impossible de construire le cache log.\n" );

		// Fermer la socket
		closesocket( out->m_socket );

		// Fermer mutex
		CloseHandle( out->m_mutex );

		// Liberer la memoire
		FREE( out );

		// Quitter
		return NULL;
	}

	// Creer le thread d'update
	if( !( out->m_threadUpdate = CreateThread( NULL,
		0,
		(LPTHREAD_START_ROUTINE)Serveur_ThreadUpdate,
		out,
		0,
		NULL ) ) )
	{
		// Notifier
		perror( "Serveur_Construire( )::Impossible de creer le thread de nettoyage: " );

		// Fermer la socket
		closesocket( out->m_socket );

		// Fermer mutex
		CloseHandle( out->m_mutex );

		// Detruire le cache
		CacheLog_Detruire( &out->m_cacheLog );

		// Liberer la memoire
		FREE( out );

		// Quitter
		return NULL;
	}

	// Creer le thread d'acceptation
	if( !( out->m_threadAcceptationClient = CreateThread( NULL,
		0,
		(LPTHREAD_START_ROUTINE)ThreadAcceptationClientServeur_Thread,
		out,
		0,
		NULL ) ) )
	{
		// Notifier
		perror( "Serveur_Construire( )::Impossible de creer le thread de nettoyage: " );

		// Arreter le serveur
		out->m_estEnCours = FALSE;

		// Fermer la socket
		closesocket( out->m_socket );

		// Fermer mutex
		CloseHandle( out->m_mutex );

		// Detruire le cache
		CacheLog_Detruire( &out->m_cacheLog );

		// Liberer la memoire
		FREE( out );

		// Quitter
		return NULL;
	}
		

	// Connexion autorisee
	Serveur_AutoriserConnexion( out );

	// Notifier
	printf( "[SERVEUR] Ecoute sur le port %d\n\n",
		port );

	// Loguer
	CacheLog_AjouterEntree( out->m_cacheLog,
		EntreeCacheLog_Construire( NULL,
			0,
			TYPE_ENTREE_LOG_DEMARRAGE,
			NULL,
			NULL,
			0,
			0,
			0,
			port ) );

	// OK
	return out;
}

// Detruire le serveur
void Serveur_Detruire( Serveur **serveur )
{
	// Interdire la connexion
	Serveur_InterdireConnexion( *serveur );

	// Tuer le thread d'acceptation clients
	TerminateThread( (*serveur)->m_threadAcceptationClient,
		EXIT_SUCCESS );

	// Attendre qu'il n'y ait plus de clients
	while( Serveur_ObtenirNombreClients( *serveur ) > 0 )
	{
		// Tuer tous les clients
		Serveur_TuerTousClients( *serveur );

		// Attendre
		Sleep( 1 );
	}

	// Arreter le serveur
	(*serveur)->m_estEnCours = FALSE;

	// Attendre la fin du thread de nettoyage
	while( WaitForSingleObject( (*serveur)->m_threadUpdate,
		INFINITE ) )
		Sleep( 1 );

	// Fermer la socket
	closesocket( (*serveur)->m_socket );

	// Tuer le cache log
	CacheLog_Detruire( &(*serveur)->m_cacheLog );

	// Fermer mutex
	CloseHandle( (*serveur)->m_mutex );

	// Liberer le conteneur
	FREE( *serveur );
}

// Est en cours
BOOL Serveur_EstEnCours( const Serveur *this )
{
	return this->m_estEnCours;
}

// Obtenir le nombre de clients
unsigned int Serveur_ObtenirNombreClients( const Serveur *this )
{
	// Sortie
	__OUTPUT unsigned int nombreClient;

	// Lock le mutex
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "Serveur_ObtenirNombreClients( )::Impossible de lock le mutex: " );

		// Quitter
		return 0;
	}

	// Obtenir nombre
	nombreClient = this->m_nombreClients;

	// Release mutex
	ReleaseMutex( this->m_mutex );

	// Quitter
	return nombreClient;
}

// Obtenir l'index du client avec cet identifiant (privee, mutex doit etre lock)
unsigned int Serveur_TrouverIndexDepuisIdentifiant( const Serveur *this,
	unsigned int identifiant )
{
	// Sortie
	__OUTPUT unsigned int out = 0;

	// Chercher
	for( ; out < this->m_nombreClients; out++ )
		// Si meme id
		if( ClientServeur_ObtenirIdentifiant( this->m_clients[ out ] ) == identifiant )
			return out;

	// Introuvable
	return ERREUR;
}

// Trouver un identifiant unique libre (privee, mutex doit etre lock)
unsigned int Serveur_TrouverIdentifiantInterne( const Serveur *this )
{
	// Identifiant
	__OUTPUT unsigned int identifiant;

	// Chercher un identifiant libre
	do
	{
		identifiant = (unsigned int)rand( )%LIMITE_IDENTIFIANT_CLIENT_SERVEUR;
	} while( Serveur_TrouverIndexDepuisIdentifiant( this,
		identifiant ) != ERREUR );

	// OK
	return identifiant;
}

// Ajout de client (private)
BOOL Serveur_AjouterClient( Serveur *this,
	const ClientServeur *nouveauClient )
{
	// Index
	unsigned int index;

	// Anciens clients
	ClientServeur **anciensClients;

	// Iterateur
	unsigned int i;

	// Verifier si la connexion est autorisee
	if( !this->m_estConnexionAutorisee )
	{
		// Notifier
		printf( "Serveur_AjouterClient( )::La connexion est interdite.\n" );

		// Quitter
		return FALSE;
	}

	// Lock le mutex
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "Serveur_AjouterClient( )::Impossible de lock le mutex: " );

		// Quitter
		return FALSE;
	}

	// Trouver un identifiant libre
	ClientServeur_DefinirIdentifiantUnique( (ClientServeur*)nouveauClient,
		Serveur_TrouverIdentifiantInterne( this ) );

	

	// Il n'y avait aucun client avant
	if( this->m_clients == NULL )
	{
		// Allouer le conteneur
		if( !( this->m_clients = calloc( 1,
			sizeof( ClientServeur* ) ) ) )
		{
			// Notifier
			perror( "Serveur_AjouterClient( )::Impossible d'allouer la memoire: " );

			// Pas OK
			return FALSE;
		}

		// Definir o� enregistrer le client
		index = 0;
	}
	else
	{
		// Enregistrer l'ancien conteneur
		anciensClients = this->m_clients;

		// Allouer le nouveau conteneur
		if( !( this->m_clients = calloc( this->m_nombreClients + 1,
			sizeof( ClientServeur* ) ) ) )
		{
			// Notifier
			perror( "Serveur_AjouterClient( )::Impossible d'allouer la memoire: " );

			// Restaurer
			this->m_clients = anciensClients;

			// Release mutex
			ReleaseMutex( this->m_mutex );

			// Quitter
			return FALSE;
		}

		// Copier les clients
		for( i = 0; i < this->m_nombreClients; i++ )
			this->m_clients[ i ] = anciensClients[ i ];

		// Liberer l'ancien conteneur
		FREE( anciensClients );

		// Definir o� enregistrer le client
		index = this->m_nombreClients;
	}

	// Enregistrer le client
	this->m_clients[ index ] = (ClientServeur*)nouveauClient;

	// Incrementer le nombre de clients
	this->m_nombreClients++;

	// Unlock le mutex
	ReleaseMutex( this->m_mutex );

	// OK
	return TRUE;
}

// Accepter un client
BOOL Serveur_AccepterClient( Serveur *this )
{
	// Client a ajouter
	ClientServeur *nouveauClient;

	// Temps
	char *temps;

	// Recuperer un nouveau client
	if( !( nouveauClient = ClientServeur_Construire( this->m_socket,
		this->m_cacheLog,
		&this->m_estConnexionAutorisee ) ) )
	{
		// Notifier
		printf( "Serveur_AccepterClient( )::Impossible d'accepter un client.\n" );

		// Pas OK
		return FALSE;
	}

	// Ajouter le client
	if( !Serveur_AjouterClient( this,
		nouveauClient ) )
	{
		// Notifier
		printf( "Serveur_AccepterClient( )::Impossible d'ajouter un client.\n" );

		// Liberer le client
		ClientServeur_Detruire( &nouveauClient );

		// Pas OK
		return FALSE;
	}

	// Obtenir temps
	temps = Log_ObtenirTemps( 0 );

	// Notifier
	printf( "[%s][%s] (%d) se connecte\n\n",
		temps,
		ClientServeur_ObtenirAdresseIP( nouveauClient ),
		ClientServeur_ObtenirIdentifiant( nouveauClient ) );

	// Liberer
	FREE( temps );

	// Loguer
	CacheLog_AjouterEntree( this->m_cacheLog,
		EntreeCacheLog_Construire( ClientServeur_ObtenirAdresseIP( nouveauClient ),
			ClientServeur_ObtenirIdentifiant( nouveauClient ),
			TYPE_ENTREE_LOG_CONNEXION,
			NULL,
			NULL,
			0,
			0,
			0,
			0 ) );

	// OK
	return TRUE;
}

// Obtenir l'index du client dans la liste (private, mutex doit etre lock)
unsigned int Serveur_ObtenirIndexClient( const Serveur *this,
	const ClientServeur *client )
{
	// Iterateur
	__OUTPUT unsigned int i = 0;

	// Chercher
	for( ; i < this->m_nombreClients; i++ )
		if( this->m_clients[ i ] == client )
			return i;

	// Introuvable
	return ERREUR;
}

// Supprimer un client (private, mutex doit etre lock)
BOOL Serveur_SupprimerClient( Serveur *this,
	unsigned int index )
{
	// Ancien tableau
	ClientServeur **ancienneAdresse = NULL;

	// Iterateurs
	unsigned int i, j;

	// Temps
	char *temps;

	// Verifier s'il reste des clients
	if( this->m_nombreClients <= 0 )
		return FALSE;

	// Obtenir temps
	temps = Log_ObtenirTemps( 0 );

	// Notifier
	printf( "[%s][%s] (%d) se deconnecte\n\n",
		temps,
		ClientServeur_ObtenirAdresseIP( this->m_clients[ index ] ),
		ClientServeur_ObtenirIdentifiant( this->m_clients[ index ] ) );

	// Liberer
	FREE( temps );

	// Loguer
	CacheLog_AjouterEntree( this->m_cacheLog,
		EntreeCacheLog_Construire( ClientServeur_ObtenirAdresseIP( this->m_clients[ index ] ),
			ClientServeur_ObtenirIdentifiant( this->m_clients[ index ] ),
			TYPE_ENTREE_LOG_DECONNEXION,
			NULL,
			NULL,
			0,
			0,
			0,
			0 ) );

	// Liberer la memoire
	ClientServeur_Detruire( &this->m_clients[ index ] );

	// S'il reste plus d'un client
	if( this->m_nombreClients > 1 )
	{
		// Sauvegarder l'ancienne adresse
		ancienneAdresse = this->m_clients;

		// Allouer une case de moins
		if( !( this->m_clients = calloc( this->m_nombreClients - 1,
			sizeof( ClientServeur* ) ) ) )
		{
			// Notifier
			perror( "Serveur_SupprimerClient( )::Impossible d'allouer la memoire: " );

			// Restaurer
			this->m_clients = ancienneAdresse;

			// Quitter
			return FALSE;
		}

		// Copier
		for( i = 0, j = 0; i < this->m_nombreClients; i++ )
			// Pas le client a supprimer?
			if( i != index )
			{
				// Copier adresse
				this->m_clients[ j ] = ancienneAdresse[ i ];
				
				// Incrementer client
				j++;
			}

		// Liberer
		FREE( ancienneAdresse );
	}
	// Il ne restait qu'un client
	else
		// Liberer
		FREE( this->m_clients );

	// Decrementer le nombre de clients
	this->m_nombreClients--;
	
	// OK
	return TRUE;
}

// Nettoyer les clients morts
unsigned int Serveur_NettoyerClients( Serveur *this )
{
	// Iterateur
	unsigned int i = 0;

	// Nombre de clients nettoyes
	__OUTPUT unsigned int nombreClientNettoye = 0;

	// Lock le mutex
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "Serveur_NettoyerClients( )::Impossible de lock le mutex: " );

		// Quitter
		return 0;
	}

	// Chercher les clients morts
	for( ; i < this->m_nombreClients; i++ )
		// Le client est mort?
		if( ClientServeur_EstMort( this->m_clients[ i ] ) )
		{
			// Supprimer le client
			Serveur_SupprimerClient( this,
				i );

			// Incrementer le nombre de clients nettoyes
			nombreClientNettoye++;

			// Recommencer la recherche pour ne rien rater
			i = 0;
		}

	// Release mutex
	ReleaseMutex( this->m_mutex );

	// OK
	return nombreClientNettoye;
}

void Serveur_TuerTousClients( Serveur *this )
{
	// Iterateur
	unsigned int i;

	// Lock le mutex
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "Serveur_TuerTousClients( )::Impossible de lock le mutex: " );

		// Quitter
		return;
	}

	// Tuer tous les clients
	for( i = 0; i < this->m_nombreClients; i++ )
		ClientServeur_Tuer( this->m_clients[ i ] );

	// Release mutex
	ReleaseMutex( this->m_mutex );
}

// Thread de nettoyage
void Serveur_ThreadUpdate( Serveur *this )
{
	// Nombre de clients nettoyes
	unsigned int clientsNettoyes;

	// Ticks pour update titre
	unsigned int ticksTitre = 0;

#define TAILLE_BUFFER_TITRE_CONSOLE		2048

	// Buffer pour le titre de la console
	char buffer[ TAILLE_BUFFER_TITRE_CONSOLE ];

	// Boucle d'update
	while( this->m_estEnCours )
	{
		// Vider buffer
		memset( buffer,
			0,
			TAILLE_BUFFER_TITRE_CONSOLE );

		// Definir le titre
		if( GetTickCount( ) - ticksTitre >= 1000 )
		{
			// Configurer
			sprintf( buffer,
				"title Serveur (%d client%s) [%s]",
				this->m_nombreClients,
				this->m_nombreClients > 1 ? "s" : "",
				this->m_estConnexionAutorisee ? "OUVERT" : "!!!FERME!!!" );

			// Definir
			system( buffer );
			
			// Enregistrer
			ticksTitre = GetTickCount( );
		}

		/* Nettoyage */
		clientsNettoyes = Serveur_NettoyerClients( this );

		// Si il y a eu du nettoyage
		if( clientsNettoyes > 0 )
			// Notifier
			printf( "[SERVEUR] %d client%s nettoye%s.\n",
				clientsNettoyes,
				( clientsNettoyes > 1 ) ? "s" : "",
				( clientsNettoyes > 1 ) ? "s" : "" );

		/* Mise a jour du log */
		CacheLog_Update( this->m_cacheLog );

		// Delais
		Sleep( 1 );
	}
}

// Changer l'etat d'autorisation de connexion (privee)
void Serveur_ChangerEtatConnexion( Serveur *this,
	BOOL etat )
{
	// Lock le mutex
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
		// Quitter
		return;

	// Changer l'etat
	this->m_estConnexionAutorisee = etat;

	// Release le mutex
	ReleaseMutex( this->m_mutex );
}

// Autoriser la connexion
void Serveur_AutoriserConnexion( Serveur *this )
{
	// Autoriser
	Serveur_ChangerEtatConnexion( this,
		TRUE );
}

// Interdire la connexion
void Serveur_InterdireConnexion( Serveur *this )
{
	// Interdire
	Serveur_ChangerEtatConnexion( this,
		FALSE );
}

// Arreter le serveur
void Serveur_Arreter( Serveur *this )
{
	// Arreter
	this->m_estEnCours = FALSE;
}

