#include "Types.h"

// ---------------
// struct CacheLog
// ---------------

// Construire le cache
__ALLOC CacheLog *CacheLog_Construire( void )
{
	// Sortie
	__OUTPUT CacheLog *this;

	// Allouer la memoire
	if( !( this = calloc( 1,
		sizeof( CacheLog ) ) ) )
	{
		// Notifier
		perror( "CacheLog_Construire( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Creer le mutex
	if( !( this->m_mutex = CreateMutex( NULL,
		0,
		NULL ) ) )
	{
		// Notifier
		perror( "CacheLog_Construire( )::Impossible de creer le mutex: " );

		// Liberer
		FREE( this );

		// Quitter
		return NULL;
	}

	// Zero
	this->m_entree = NULL;
	this->m_nombreEntree = 0;

	// OK
	return this;
}

// Supprimer la premiere entree cache (privee, mutex doit etre lock)
void CacheLog_SupprimerEntree( CacheLog *this )
{
	// Ancienne adresse
	EntreeCacheLog **temp = this->m_entree;

	// Verifier qu'il y ait au moins une entree
	if( !this->m_nombreEntree )
		return;

	// Detruire la premiere entree
	EntreeCacheLog_Detruire( &this->m_entree[ 0 ] );

	// Si le cache est ensuite vide
	if( this->m_nombreEntree <= 1 )
	{
		// Liberer
		FREE( this->m_entree );
	}
	// Il restera une entree ou plus
	else
	{
		// Allouer nouveau tableau
		if( !( this->m_entree = calloc( this->m_nombreEntree - 1,
			sizeof( EntreeCacheLog* ) ) ) )
		{
			// Notifier
			perror( "CacheLog_SupprimerEntree( )::Impossible d'allouer la memoire: " );

			// Restaurer
			this->m_entree = temp;

			// Quitter
			return;
		}

		// Copier anciennes entrees
		memcpy( this->m_entree,
			temp + 1,
			sizeof( EntreeCacheLog* ) * ( this->m_nombreEntree - 1 ) );

		// Liberer
		FREE( temp );
	}

	// Decrementer le nombre d'entree
	this->m_nombreEntree--;
}

// Vider le cache (privee)
void CacheLog_Vider( CacheLog *this )
{
	// Lock le mutex si demande
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "CacheLog_Vider( )::Impossible de lock le mutex: " );

		// Quitter
		return;
	}

	// Supprimer les entrees
	while( this->m_nombreEntree > 0 )
		CacheLog_SupprimerEntree( this );

	// Release le mutex si necessaire
	ReleaseMutex( this->m_mutex );
}

// Detruire le cache
void CacheLog_Detruire( CacheLog **this )
{
	// Vider le contenu
	CacheLog_Vider( *this );

	// Detruire le mutex
	CloseHandle( (*this)->m_mutex );

	// Liberer contenant
	FREE( (*this) );
}

// Ajouter une entree au cache
BOOL CacheLog_AjouterEntree( CacheLog *this,
	__WILLBEOWNED EntreeCacheLog *entree )
{
	// Tableau temporaire
	EntreeCacheLog **temp;

	// Verifier le parametre
	if( !entree )
	{
		// Notifier
		printf( "CacheLog_AjouterEntree( )::Parametre incorrect.\n" );

		// Quitter
		return FALSE;
	}

	// Lock le mutex
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "CacheLog_AjouterEntree( )::Impossible de lock le mutex: " );

		// Detruire l'entree
		EntreeCacheLog_Detruire( &entree );

		// Quitter
		return FALSE;
	}

	// Aucun cache
	if( this->m_entree == NULL )
	{
		// Allouer le conteneur
		if( !( this->m_entree = calloc( 1,
			sizeof( EntreeCacheLog* ) ) ) )
		{
			// Notifier
			perror( "CacheLog_AjouterEntree( )::Impossible d'allouer la memoire: " );

			// Liberer
			EntreeCacheLog_Detruire( &entree );

			// Quitter
			return FALSE;
		}
	}
	// Deja un cache
	else
	{
		// Copier l'ancienne adresse
		temp = this->m_entree;

		// Allouer le nouveau conteneur
		if( !( this->m_entree = calloc( this->m_nombreEntree + 1,
			sizeof( EntreeCacheLog* ) ) ) )
		{
			// Notifier
			perror( "CacheLog_AjouterEntree( )::Impossible d'allouer la memoire: " );

			// Liberer
			EntreeCacheLog_Detruire( &entree );

			// Restaurer
			this->m_entree = temp;

			// Quitter
			return FALSE;
		}

		// Copier les anciennes adresses
		memcpy( this->m_entree,
			temp,
			sizeof( EntreeCacheLog* ) * this->m_nombreEntree );

		// Liberer
		FREE( temp );
	}

	// Ajouter
	this->m_entree[ this->m_nombreEntree ] = entree;

	// Incrementer
	this->m_nombreEntree++;

	// Release le mutex
	ReleaseMutex( this->m_mutex );

	// OK
	return TRUE;
}

// Update le cache
void CacheLog_Update( CacheLog *this )
{
	// Lock le mutex
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "CacheLog_Update( )::Impossible de lock le mutex: " );

		// Quitter
		return;
	}

#define ENTREE	this->m_entree[ 0 ]

	// Loguer
	while( this->m_nombreEntree > 0 )
	{
		// Inscrire
		switch( this->m_entree[ 0 ]->m_type )
		{
			case TYPE_ENTREE_LOG_DEMARRAGE:
				Log_LoguerDemarrage( ENTREE->m_port );
				break;
			case TYPE_ENTREE_LOG_CONNEXION:
				Log_LoguerConnexion( ENTREE->m_ip,
					ENTREE->m_identifiantClient );
				break;
			case TYPE_ENTREE_LOG_DECONNEXION:
				Log_LoguerDeconnexion( ENTREE->m_ip,
					ENTREE->m_identifiantClient);
				break;
			case TYPE_ENTREE_LOG_AGENT_CLIENT:
				Log_LoguerAgentClient( ENTREE->m_ip,
					ENTREE->m_identifiantClient,
					ENTREE->m_agent );
				break;
			case TYPE_ENTREE_LOG_DEMANDE_FICHIER:
				Log_LoguerDemandeFichier( ENTREE->m_ip,
					ENTREE->m_identifiantClient,
					ENTREE->m_fichier );
				break;
			case TYPE_ENTREE_LOG_STATUT_ENVOI:
				Log_LoguerEnvoi( ENTREE->m_ip,
					ENTREE->m_identifiantClient,
					ENTREE->m_fichier,
					ENTREE->m_tailleEnvoyee,
					ENTREE->m_taille,
					ENTREE->m_vitesse );
				break;
			case TYPE_ENTREE_LOG_FIN_ENVOI:
				Log_LoguerFinEnvoi( ENTREE->m_ip,
					ENTREE->m_identifiantClient,
					ENTREE->m_fichier );
				break;

			default:
				break;
		}

		// Supprimer l'entree
		CacheLog_SupprimerEntree( this );
	}

	// Release le mutex
	ReleaseMutex( this->m_mutex );
}

