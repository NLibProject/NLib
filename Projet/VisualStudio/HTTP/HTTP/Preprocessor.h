#ifndef PREPROCESSOR_PROTECT
#define PREPROCESSOR_PROTECT

// Will alloc memory
#define __ALLOC

// Output
#define __OUTPUT

// Control will be gained by function once passed
#define __WILLBEOWNED

// Fonction utilisee en tant que callback
#define __CALLBACK

// Windows?
#if defined( WIN32 ) || defined( _WIN64 )
#define IS_WINDOWS
#endif // WIN32 || _WIN64

// Erreur standard
#define ERREUR		(unsigned int)0xFFFFFFFF

// Maccros
#define FORGET( m ) \
	m = NULL

#define FREE( m ) \
	{ \
		if( m != NULL ) \
		{ \
			free( m ); \
			FORGET( m ); \
		} \
	}

#define REFERENCER( a ) \
	a

#define NOUVELLE_LIGNE_CONSOLE( ) \
	puts( "" )

#endif // !PREPROCESSOR_PROTECT

