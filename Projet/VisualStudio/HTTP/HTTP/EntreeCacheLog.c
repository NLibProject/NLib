#include "Types.h"

// ---------------------
// struct EntreeCacheLog
// ---------------------

// Construire une entree
__ALLOC EntreeCacheLog *EntreeCacheLog_Construire( const char *ip,
	unsigned int identifiantClient,
	TypeEntreeLog type,
	const char *agent,
	const char *fichier,
	double vitesse,
	unsigned long taille,
	unsigned long tailleEnvoyee,
	unsigned int port )
{
	// Sortie
	__OUTPUT EntreeCacheLog *this;

	// Allouer la memoire
	if( !( this = calloc( 1,
		sizeof( EntreeCacheLog ) ) ) )
	{
		// Notifier
		perror( "EntreeCacheLog_Construire( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Copier les informations
		// Type
			this->m_type = type;
		// Identifiant
			this->m_identifiantClient = identifiantClient;
		// IP
			if( type != TYPE_ENTREE_LOG_DEMARRAGE )
			{
				// Allouer la memoire
					if( !( this->m_ip = calloc( strlen( ip ) + 1,
						sizeof( char ) ) ) )
					{
						// Notifier
						perror( "EntreeCacheLog_Construire( )::Impossible d'allouer la memoire: " );

						// Liberer
						FREE( this );

						// Quitter
						return NULL;
					}
				// Copier
					memcpy( this->m_ip,
						ip,
						strlen( ip ) );
			}
		// Suivant le type
			switch( this->m_type )
			{
				case TYPE_ENTREE_LOG_STATUT_ENVOI:
				case TYPE_ENTREE_LOG_DEMANDE_FICHIER:
				case TYPE_ENTREE_LOG_FIN_ENVOI:
					// Etat
					this->m_taille = taille;
					this->m_tailleEnvoyee = tailleEnvoyee;
					this->m_vitesse = vitesse;

					// Allouer nom fichier
					if( !( this->m_fichier = calloc( strlen( fichier ) + 1,
						sizeof( char ) ) ) )
					{
						// Notifier
						perror( "EntreeCacheLog_Construire( )::Impossible d'allouer la memoire: " );

						// Liberer
						FREE( this->m_ip );
						FREE( this );

						// Quitter
						return NULL;
					}

					// Copier
					memcpy( this->m_fichier,
						fichier,
						strlen( fichier ) );
					break;

				case TYPE_ENTREE_LOG_DEMARRAGE:
					this->m_port = port;
					break;

				case TYPE_ENTREE_LOG_AGENT_CLIENT:
					// Allouer agent
					if( !( this->m_agent = calloc( strlen( agent ) + 1,
						sizeof( char ) ) ) )
					{
						// Notifier
						perror( "EntreeCacheLog_Construire( )::Impossible d'allouer la memoire: " );

						// Liberer
						FREE( this->m_ip );
						FREE( this );

						// Quitter
						return NULL;
					}

					// Copier
					memcpy( this->m_agent,
						agent,
						strlen( agent ) );
					break;

				default:
				case TYPE_ENTREE_LOG_CONNEXION:
				case TYPE_ENTREE_LOG_DECONNEXION:
					break;
			}

	// OK
	return this;
}

// Detruire une entree
void EntreeCacheLog_Detruire( EntreeCacheLog **this )
{
	// Liberer
	FREE( (*this)->m_agent );
	FREE( (*this)->m_ip );
	FREE( (*this)->m_fichier );

	// Liberer conteneur
	FREE( (*this) )
}

// Obtenir le type
TypeEntreeLog EntreeCacheLog_ObtenirType( const EntreeCacheLog *this )
{
	return this->m_type;
}

// Obtenir l'ip
const char *EntreeCacheLog_ObtenirIP( const EntreeCacheLog *this )
{
	return this->m_ip;
}

// Obtenir l'agent
const char *EntreeCacheLog_ObtenirAgent( const EntreeCacheLog *this )
{
	return this->m_agent;
}

// Obtenir l'identifiant client
unsigned int EntreeCacheLog_ObtenirIdentifiantClient( const EntreeCacheLog *this )
{
	return this->m_identifiantClient;
}

// Obtenir le nom du fichier
const char *EntreeCacheLog_ObtenirNomFichier( const EntreeCacheLog *this )
{
	return this->m_fichier;
}

// Obtenir la vitesse
double EntreeCacheLog_ObtenirVitesse( const EntreeCacheLog *this )
{
	return this->m_vitesse;
}

// Obtenir la taille
unsigned long EntreeCacheLog_ObtenirTaille( const EntreeCacheLog *this )
{
	return this->m_taille;
}

// Obtenir la taille envoyee
unsigned long EntreeCacheLog_ObtenirTailleEnvoyee( const EntreeCacheLog *this )
{
	return this->m_tailleEnvoyee;
}

// Obtenir le port
unsigned int EntreeCacheLog_ObtenirPort( const EntreeCacheLog *this )
{
	return this->m_port;
}

