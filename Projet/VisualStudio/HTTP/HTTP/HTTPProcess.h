#ifndef HTTPPROCESS_PROTECT
#define HTTPPROCESS_PROTECT

#define NOM_SERVEUR		"NServer (http://nproject.ddns.net/)"

typedef enum
{
	HTTP_PROCESS_PACKET_HEADER,
	HTTP_PROCESS_PACKET_CONTENT,

	HTTP_PROCESS_PACKETS
} HTTPProcessPacket;

// Traiter un packet
__ALLOC Packet **HTTPProcess_TraiterPacket( const Packet *packet,
	const ClientServeur *client );

// Compose les informations header
void HTTPProcess_ComposerInformationsHeaderPacketReponseGET( Packet *p,
	HTTPTypeFlux typeFlux,
	unsigned long tailleFichier,
	BOOL estErreur );

#endif // !HTTPPROCESS_PROTECT

