#ifndef PACKET_PROTECT
#define PACKET_PROTECT

// struct Packet
typedef struct
{
	// Est un fichier?
	BOOL m_estFichier;

	// Taille
	unsigned int m_taille;

	// Donnees
	char *m_data;
} Packet;

// Construire le packet
__ALLOC Packet *Packet_Construire( void );
__ALLOC Packet *Packet_Construire2( const Packet *src );
__ALLOC Packet *Packet_Construire3( const char *lienFichier );

// Detruire le packet
void Packet_Detruire( Packet** );

// Obtenir la taille
unsigned int Packet_ObtenirTaille( const Packet* );

// Obtenir les donnees
const char *Packet_ObtenirData( const Packet* );

// Ajout donnees
BOOL Packet_AjouterData( Packet*,
	const char*,
	unsigned int );

// Est un fichier?
BOOL Packet_EstFichier( const Packet* );

#endif // !PACKET_PROTECT

