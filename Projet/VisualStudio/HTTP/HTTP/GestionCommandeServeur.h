#ifndef GESTIONCOMMANDESERVEUR_PROTECT
#define GESTIONCOMMANDESERVEUR_PROTECT

// Gerer les commandes serveur
void GestionCommandeServeur_Gerer( Serveur *serveur );

// Afficher l'aide
void GestionCommandeServeur_AfficherAide( void );

#endif // !GESTIONCOMMANDESERVEUR_PROTECT

