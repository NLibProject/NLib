#ifndef CARACTERE_PROTECT
#define CARACTERE_PROTECT

// Est un chiffre?
BOOL Caractere_EstUnChiffre( char );
BOOL Caractere_EstUnChiffreHexadecimal( char );

// Est une lettre?
BOOL Caractere_EstUneLettre( char );

// Est un caractere acceptable dans HTML
BOOL Caractere_EstUnCaractereViableHTMLHref( char );

// Convertir un chiffre en decimal
unsigned int Caractere_ConvertirDecimal( char );

// Convertir un nombre en caractere entre 0 et F
char Caractere_ConvertirHexadecimal( unsigned char nb );

#endif // !CARACTERE_PROTECT

