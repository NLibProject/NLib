#ifndef HTTPDEF_PROTECT
#define HTTPDEF_PROTECT

// Fichier par defaut si on essaye d'obtenir /
#define FICHIER_PAR_DEFAUT_SERVEUR		"index.html"

// Version HTTP
#define VERSION_HTTP					"HTTP/1.1"

// Answer text
#define OK_HTTP_TEXT					"200 OK"
#define NOT_FOUND_HTTP_TEXT				"404 Not Found"

// Mot clef requete
typedef enum
{
	HTTP_REQUETE_GET,

	HTTP_REQUETES
} HTTPTypeRequete;

typedef enum
{
	HTTP_REQUEST_KEYWORD_ACCEPT,
	HTTP_REQUEST_KEYWORD_ACCEPT_CHARSET,
	HTTP_REQUEST_KEYWORD_ACCEPT_ENCODING,
	HTTP_REQUEST_KEYWORD_ACCEPT_LANGUAGE,
	HTTP_REQUEST_KEYWORD_ACCEPT_DATETIME,
	HTTP_REQUEST_KEYWORD_AUTHORIZATION,
	HTTP_REQUEST_KEYWORD_CACHE_CONTROL,
	HTTP_REQUEST_KEYWORD_CONNECTION,
	HTTP_REQUEST_KEYWORD_PERMANENT,
	HTTP_REQUEST_KEYWORD_COOKIE,
	HTTP_REQUEST_KEYWORD_CONTENT_LENGTH,
	HTTP_REQUEST_KEYWORD_CONTENT_MD5,
	HTTP_REQUEST_KEYWORD_CONTENT_TYPE,
	HTTP_REQUEST_KEYWORD_DATE,
	HTTP_REQUEST_KEYWORD_EXPECT,
	HTTP_REQUEST_KEYWORD_FORWARDED,
	HTTP_REQUEST_KEYWORD_FROM,
	HTTP_REQUEST_KEYWORD_HOST,
	HTTP_REQUEST_KEYWORD_IF_MATCH,
	HTTP_REQUEST_KEYWORD_IF_MODIFIED_SINCE,
	HTTP_REQUEST_KEYWORD_IF_NONE_MATCH,
	HTTP_REQUEST_KEYWORD_IF_RANGE,
	HTTP_REQUEST_KEYWORD_IF_UNMODIFIED_SINCE,
	HTTP_REQUEST_KEYWORD_MAX_FORWARDS,
	HTTP_REQUEST_KEYWORD_ORIGIN,
	HTTP_REQUEST_KEYWORD_PRAGMA,
	HTTP_REQUEST_KEYWORD_PROXY_AUTHORIZATION,
	HTTP_REQUEST_KEYWORD_RANGE,
	HTTP_REQUEST_KEYWORD_REFERER,
	HTTP_REQUEST_KEYWORD_TE,
	HTTP_REQUEST_KEYWORD_USER_AGENT,
	HTTP_REQUEST_KEYWORD_UPGRADE,
	HTTP_REQUEST_KEYWORD_VIA,
	HTTP_REQUEST_KEYWORD_WARNING,

	HTTP_REQUEST_KEYWORDS
} HTTPRequestKeyWord;

typedef enum
{
	HTTP_ANSWER_KEYWORD_ACCESS_CONTROL_ALLOW_ORIGIN,
	HTTP_ANSWER_KEYWORD_ACCEPT_PATCH,
	HTTP_ANSWER_KEYWORD_ACCEPT_RANGES,
	HTTP_ANSWER_KEYWORD_AGE,
	HTTP_ANSWER_KEYWORD_ALLOW,
	HTTP_ANSWER_KEYWORD_ALT_SVC,
	HTTP_ANSWER_KEYWORD_CACHE_CONTROL,
	HTTP_ANSWER_KEYWORD_CONNECTION,
	HTTP_ANSWER_KEYWORD_CONTENT_DISPOSITION,
	HTTP_ANSWER_KEYWORD_CONTENT_ENCODING,
	HTTP_ANSWER_KEYWORD_CONTENT_LANGUAGE,
	HTTP_ANSWER_KEYWORD_CONTENT_LENGTH,
	HTTP_ANSWER_KEYWORD_CONTENT_LOCATION,
	HTTP_ANSWER_KEYWORD_CONTENT_MD5,
	HTTP_ANSWER_KEYWORD_CONTENT_RANGE,
	HTTP_ANSWER_KEYWORD_CONTENT_TYPE,
	HTTP_ANSWER_KEYWORD_DATE,
	HTTP_ANSWER_KEYWORD_ETAG,
	HTTP_ANSWER_KEYWORD_EXPIRES,
	HTTP_ANSWER_KEYWORD_LAST_MODIFIED,
	HTTP_ANSWER_KEYWORD_LINK,
	HTTP_ANSWER_KEYWORD_LOCATION,
	HTTP_ANSWER_KEYWORD_P3P,
	HTTP_ANSWER_KEYWORD_PRAGMA,
	HTTP_ANSWER_KEYWORD_PROXY_AUTHENTICATE,
	HTTP_ANSWER_KEYWORD_PUBLIC_KEY_PINS,
	HTTP_ANSWER_KEYWORD_REFRESH,
	HTTP_ANSWER_KEYWORD_RETRY_AFTER,
	HTTP_ANSWER_KEYWORD_SERVER,
	HTTP_ANSWER_KEYWORD_SET_COOKIE,
	HTTP_ANSWER_KEYWORD_STATUS,
	HTTP_ANSWER_KEYWORD_STRICT_TRANSPORT_SECURITY,
	HTTP_ANSWER_KEYWORD_TRAILER,
	HTTP_ANSWER_KEYWORD_TRANSFER_ENCODING,
	HTTP_ANSWER_KEYWORD_TSV,
	HTTP_ANSWER_KEYWORD_UPGRADE,
	HTTP_ANSWER_KEYWORD_VARY,
	HTTP_ANSWER_KEYWORD_PERMANENT,
	HTTP_ANSWER_KEYWORD_VIA,
	HTTP_ANSWER_KEYWORD_WARNING,
	HTTP_ANSWER_KEYWORD_WWW_AUTHENTICATE,
	HTTP_ANSWER_KEYWORD_X_FRAME_OPTIONS,

	NMOT_CLEF_REPONSE_HTTP_KEYWORDS
} HTTPAnswerKeyword;

typedef enum
{
	HTTP_TYPE_FLUX_HTML,
	HTTP_TYPE_FLUX_CSS,
	HTTP_TYPE_FLUX_TEXTE,
	HTTP_TYPE_FLUX_TELECHARGEMENT,
	HTTP_TYPE_FLUX_PDF,
	HTTP_TYPE_FLUX_IMAGE_PNG,
	HTTP_TYPE_FLUX_IMAGE_JPG,
	HTTP_TYPE_FLUX_IMAGE_GIF,

	HTTP_TYPES_FLUX
} HTTPTypeFlux;

typedef enum
{
	// Page web
	EXTENSION_PAGE_WEB_HTML,
	EXTENSION_PAGE_WEB_HTM,
	EXTENSION_PAGE_WEB_PHP,
	EXTENSION_PAGE_WEB_CSS,

	// Texte
	EXTENSION_TXT,
	EXTENSION_LOG,
	EXTENSION_C,
	EXTENSION_H,
	EXTENSION_CPP,
	EXTENSION_HPP,
	EXTENSION_JAVA,
	EXTENSION_CS,

	// PDF
	EXTENSION_PDF,

	// Images
	EXTENSION_PNG,
	EXTENSION_JPG,
	EXTENSION_JPEG,
	EXTENSION_GIF,

	EXTENSIONS_FICHIER
} ExtensionFichier;

// Parser un mot clef de type de requete
HTTPTypeRequete HTTPDef_ParserTypeRequete( const char * );

// Parser un mot clef de requete
HTTPRequestKeyWord HTTPDef_ParserRequestKeyword( const char* );

// Parser un mot clef de reponse
HTTPAnswerKeyword HTTPDef_ParserAnswerKeyword( const char* );

// Parser une extension pour fournir un type de flux
HTTPTypeFlux HTTPDef_ParserExtensionFichier( const char* );

// Obtenir un type de requete
const char *HTTPDef_ObtenirTypeRequete( HTTPTypeRequete );

// Obtenir un mot clef de requete
const char *HTTPDef_ObtenirRequestKeyword( HTTPRequestKeyWord );

// Obtenir un mot clef de reponse
const char *HTTPDef_ObtenirAnswerKeyword( HTTPAnswerKeyword );

// Obtenir un type de flux
const char *HTTPDef_ObtenirTypeFlux( HTTPTypeFlux );

// Obtenir extension
const char *HTTPDef_ObtenirExtensionTypeFichier( ExtensionFichier );

#ifdef HTTPDEF_INTERNE
const char HTTPTypeRequeteText[ HTTP_REQUETES ][ 32 ] =
{
	"GET"
};

const char HTTPRequestKeywordText[ HTTP_REQUEST_KEYWORDS ][ 32 ] =
{
	"Accept",
	"Accept-Charset",
	"Accept-Encoding",
	"Accept-Language",
	"Accept-Datetime",
	"Authorization",
	"Cache-Control",
	"Connection",
	"Permanent",
	"Cookie",
	"Content-Length",
	"Content-MD5",
	"Content-Type",
	"Date",
	"Expect",
	"Forwarded",
	"From",
	"Host",
	"If-Match",
	"If-Modified-Since",
	"If-None-Match",
	"If-Range",
	"If-Unmodified-Since",
	"Max-Forwards",
	"Origin",
	"Pragma",
	"Proxy-Authorization",
	"Range",
	"Referer",
	"TE",
	"User-Agent",
	"Upgrade",
	"Via",
	"Warning"
};

const char HTTPAnswerKeywordText[ NMOT_CLEF_REPONSE_HTTP_KEYWORDS ][ 32 ] =
{
	"Access-Control-Allow-Origin",
	"Accept-Patch",
	"Accept-Ranges",
	"Age",
	"Allow",
	"Alt-Svc",
	"Cache-Control",
	"Connection",
	"Content-Disposition",
	"Content-Encoding",
	"Content-Language",
	"Content-Length",
	"Content-Location",
	"Content-MD5",
	"Content-Range",
	"Content-Type",
	"Date",
	"ETag",
	"Expires",
	"Last-Modified",
	"Link",
	"Location",
	"P3P",
	"Pragma",
	"Proxy-Authenticate",
	"Public-Key-Pins",
	"Refresh",
	"Retry-After",
	"Server",
	"Set-Cookie",
	"Status",
	"Strict-Transport-Security",
	"Trailer",
	"Transfer-Encoding",
	"TSV",
	"Upgrade",
	"Vary",
	"Permanent",
	"Via",
	"Warning",
	"WWW-Authenticate",
	"X-Frame-Options"
};

const char HTTPTypeFluxTexte[ HTTP_TYPES_FLUX ][ 32 ] =
{
	"text/html",
	"text/css",
	"text/plain",
	"application/octet-stream",
	"application/pdf",
	"image/png",
	"image/jpeg",
	"image/gif"
};

// Extension par type
const char ExtensionsFichierTexte[ EXTENSIONS_FICHIER ][ 32 ] =
{
	// Page web
	"html",
	"htm",
	"php",
	"css",

	// Texte
	"txt",
	"log",
	"c",
	"h",
	"cpp",
	"hpp",
	"java",
	"cs",

	// PDF
	"pdf",

	// Images
	"png",
	"jpg",
	"jpeg",
	"gif"
};
#endif // HTTPDEF_INTERNE

#endif // !HTTPDEF_PROTECT

