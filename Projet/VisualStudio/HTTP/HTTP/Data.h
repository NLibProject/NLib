#ifndef DATA_PROTECT
#define DATA_PROTECT

// Augmenter taille
BOOL Data_AugmenterTaille( char **mem,
	unsigned int tailleMem,
	unsigned int delta );

// Ajout donnees
BOOL Data_AjouterData( char **mem,
	unsigned int tailleMem,
	const char *memAjout,
	unsigned int tailleMemAjout );

#endif // !DATA_PROTECT

