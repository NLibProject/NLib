#include "Types.h"

/* Thread client serveur */

// Thread emission
void ThreadClientServeur_ThreadEmission( ClientServeur *client )
{
	// Packet
	Packet *packet;

	// Thread
	do
	{
		// Delais
		Sleep( 1 );

		// Si le client est vivant
		//if( !ClientServeur_EstMort( client ) )
		//{
			// Si il y a quelque chose a ecrire dans le flux
			if( CachePacket_ObtenirNombrePackets( ClientServeur_ObtenirCachePacket( client ) ) > 0 )
				// Si on peut ecrire dans le flux
				if( SocketSelection_EstEcritureDisponible( ClientServeur_ObtenirSocket( client ) ) )
				{
					// Recuperer le packet
					if( !( packet = CachePacket_ObtenirPacket( ClientServeur_ObtenirCachePacket( client ) ) ) )
						continue;

					// Ecrire dans le flux
					if( !PacketIO_EnvoyerPacket( packet,
						client ) )
					{
						// Notifier
						printf( "ThreadClientServeur_ThreadEmission( )::Impossible d'envoyer un packet.\n" );

						// Tuer le client
						ClientServeur_Tuer( client );
					}

					// Liberer le packet
					Packet_Detruire( &packet );

					// Supprimer le packet du cache
					CachePacket_SupprimerPacket( ClientServeur_ObtenirCachePacket( client ) );
				}
		//}
	} while( ClientServeur_EstEnCours( client ) );
}

// Thread reception
void ThreadClientServeur_ThreadReception( ClientServeur *client )
{
	// Packet
	Packet *packet;

	// Thread
	do
	{
		// Delais
		Sleep( 1 );

		// Si le client est vivant
		if( !ClientServeur_EstMort( client ) )
		{
			// Si on peut lire dans le flux
			if( SocketSelection_EstLectureDisponible( ClientServeur_ObtenirSocket( client ) ) )
			{
				// Construire le packet
				if( !( packet = Packet_Construire( ) ) )
				{
					// Notifier
					printf( "ThreadClientServeur_ThreadReception( )::Impossible de construire le packet.\n" );

					// Continuer
					continue;
				}

				// Lire dans le flux
				if( !PacketIO_RecevoirPacket( packet,
					ClientServeur_ObtenirSocket( client ) ) )
				{
					// Notifier
					printf( "ThreadClientServeur_ThreadReception( )::Impossible de recevoir un packet.\n" );

					// Tuer le client
					ClientServeur_Tuer( client );
				}
				// Transmettre a la fonction de traitement
				else
					PacketEntrant_Lecture( packet,
						client );

				// Liberer le packet
				Packet_Detruire( &packet );
			}
		}
	} while( ClientServeur_EstEnCours( client ) );
}

