#include "Types.h"

/* PacketIO */
// Recevoir un packet (privee)
BOOL PacketIO_RecevoirPacketInterne( __OUTPUT char **data,
	__OUTPUT unsigned int *taille,
	SOCKET socket )
{
	// Buffer
	char buffer[ BUFFER_PACKET ] = { 0, };

	// Code de retour
	signed int code;

	// Taille lue
	unsigned int tailleLue = 0;

	// Lire le packet
	do
	{
		// Vider le buffer
		memset( buffer,
			0,
			BUFFER_PACKET );

		// Lire un buffer
		if( ( code = recv( socket,
			buffer,
			BUFFER_PACKET,
			0 ) ) == SOCKET_ERROR
			&& WSAGetLastError( ) != WSAETIMEDOUT )
		{
			// Notifier
			perror( "PacketIO_RecevoirPacketInterne( )::Impossible de recevoir le packet: ");

			// Quitter
			return FALSE;
		}

		// Si il y a des donnees a copier
		if( code > 0 )
			// Copier les donnees
			if( !Data_AjouterData( data,
				tailleLue,
				buffer,
				code ) )
			{
				// Notifier
				printf( "PacketIO_RecevoirPacketInterne( )::Impossible d'ajouter le buffer au total du packet.\n" );

				// Quitter
				return FALSE;
			}

		// Incrementer la taille lue
		tailleLue += code;
	} while( code > 0 );

	// Copier la taille lue
	*taille = tailleLue;

	// Verifier ce qui a ete lu
	if( *taille == 0 )
	{
		// Notifier
		printf( "PacketIO_RecevoirPacketInterne( )::Packet vide recu.\n" );

		// Liberer les donnees
		FREE( *data );

		// Quitter
		return FALSE;
	}

	// OK
	return TRUE;
}

// Envoyer un packet (privee)
BOOL PacketIO_EnvoyerPacketInterneData( char *data,
	unsigned int taille,
	SOCKET socket )
{
	// Taille envoyee
	unsigned int tailleEnvoyee = 0;

	// Taille a envoyer
	unsigned int tailleAEnvoyer;

	// Code retourne par send
	signed int code;

	// Buffer
	char buffer[ BUFFER_PACKET ];

	// Envoyer le packet
	do
	{
		// Vider le buffer
		memset( buffer,
			0,
			BUFFER_PACKET );

		// Calculer la taille a envoyer
		tailleAEnvoyer = ( taille - tailleEnvoyee >= BUFFER_PACKET )
			? BUFFER_PACKET
			: taille - tailleEnvoyee;

		// Copier les donnees dans le buffer
		memcpy( buffer,
			data + tailleEnvoyee,
			tailleAEnvoyer );

		// Envoyer un buffer
		if( ( code = send( socket,
			buffer,
			tailleAEnvoyer,
			0 ) ) == SOCKET_ERROR )
		{
			// Notifier
			perror( "PacketIO_EnvoyerPacketInterne( )::Impossible d'envoyer un buffer: " );

			// Quitter
			return FALSE;
		}

		// Incrementer la taille envoyee
		tailleEnvoyee += tailleAEnvoyer;
	} while( tailleEnvoyee < taille );

	// OK
	return TRUE;
}

BOOL PacketIO_EnvoyerPacketInterneFichier( const char *lien,
	const ClientServeur *client )
{
	// Fichier
	FILE *fichier;

	// Temps
	char *temps;

	// Taille du fichier
	unsigned long taille;

	// Taille envoyee
	unsigned long tailleEnvoyee = 0;

	// Taille a envoyer
	unsigned long tailleAEnvoyer;

	// Update vitesse
	unsigned long derniereUpdateVitesse;
	unsigned long derniereTailleVitesse = 0;
	unsigned long vitesse = 0;

	// Affichage console/Log vitesse
	unsigned dernierAffichageConsoleVitesse = 0;

	// Buffer
	char buffer[ BLOC_FICHIER ];

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"rb" ) ) )
	{
		// Notifier
		printf( "PacketIO_EnvoyerPacketInterneFichier( )::Impossible d'ouvrir [%s].\n",
			lien );

		// Quitter
		return FALSE;
	}

	// Obtenir la taille
	taille = FileManipulation_GetSize( fichier );

	// Initialiser le timer
	derniereUpdateVitesse = GetTickCount( );
	dernierAffichageConsoleVitesse = GetTickCount( );

	// Lire et envoyer
	while( tailleEnvoyee < taille )
	{
		// Vider le buffer
		memset( buffer,
			0,
			BLOC_FICHIER );

		// Calculer la taille a envoyer
		tailleAEnvoyer = ( taille - tailleEnvoyee ) >= BLOC_FICHIER ?
			BLOC_FICHIER
			: ( taille - tailleEnvoyee );

		// Recuperer dans le fichier
		if( tailleAEnvoyer != fread( buffer,
			sizeof( char ),
			tailleAEnvoyer,
			fichier ) )
		{
			// Notifier
			perror( "PacketIO_EnvoyerPacketInterneFichier( )::Impossible de lire dans le fichier.\n" );

			// Fermer le fichier
			fclose( fichier );

			// Quitter
			return FALSE;
		}

		// Envoyer
		if( !PacketIO_EnvoyerPacketInterneData( buffer,
			tailleAEnvoyer,
			ClientServeur_ObtenirSocket( client ) ) )
		{
			// Notifier
			printf( "PacketIO_EnvoyerPacketInterneFichier( )::Impossible d'envoyer les donnees.\n" );

			// Fermer le fichier
			fclose( fichier );

			// Quitter
			return FALSE;
		}

		// Incrementer la taille envoyee
		tailleEnvoyee += tailleAEnvoyer;

		// Update vitesse
		if( GetTickCount( ) - derniereUpdateVitesse >= FREQUENCE_UPDATE_VITESSE )
		{
			// Calculer
			vitesse = (unsigned long)( (double)( tailleEnvoyee - derniereTailleVitesse ) * ( (double)1000.0 / (double)( GetTickCount( ) - derniereUpdateVitesse ) ) );

			// Enregistrer nouvel etat
			derniereTailleVitesse = tailleEnvoyee;
			derniereUpdateVitesse = GetTickCount( );
		}

		// Notifier console
		if( GetTickCount( ) - dernierAffichageConsoleVitesse >= FREQUENCE_AFFICHAGE_CONSOLE )
		{
			// Loguer
			CacheLog_AjouterEntree( ClientServeur_ObtenirCacheLog( client ),
				EntreeCacheLog_Construire( ClientServeur_ObtenirAdresseIP( client ),
					ClientServeur_ObtenirIdentifiant( client ),
					TYPE_ENTREE_LOG_STATUT_ENVOI,
					NULL,
					lien,
					vitesse,
					taille,
					tailleEnvoyee,
					0 ) );

			// Obtenir temps
			temps = Log_ObtenirTemps( 0 );

			// Notifier
			printf( "[%s][%s] (%d) << [%s]\t| %lu / %lu octets\t| %1.0f ko/s\t|\n",
				temps,
				ClientServeur_ObtenirAdresseIP( client ),
				ClientServeur_ObtenirIdentifiant( client ),
				lien,
				tailleEnvoyee,
				taille,
				( (double)vitesse / 1000.0f ) );

			// Liberer
			FREE( temps );

			// Enregistrer dernier temps
			dernierAffichageConsoleVitesse = GetTickCount( );
		}
	}

	// Loguer fin
	CacheLog_AjouterEntree( ClientServeur_ObtenirCacheLog( client ),
		EntreeCacheLog_Construire( ClientServeur_ObtenirAdresseIP( client ),
			ClientServeur_ObtenirIdentifiant( client ),
			TYPE_ENTREE_LOG_FIN_ENVOI,
			NULL,
			lien,
			0,
			0,
			0,
			0 ) );

	// Obtenir temps
	temps = Log_ObtenirTemps( 0 );

	// Notifier
	printf( "[%s][%s] (%d) a fini de recevoir \"%s\"\n",
		temps,
		ClientServeur_ObtenirAdresseIP( client ),
		ClientServeur_ObtenirIdentifiant( client ),
		lien );

	// Liberer
	FREE( temps );

	// Fermer le fichier
	fclose( fichier );

	// OK
	return TRUE;
}

// Recevoir un packet
BOOL PacketIO_RecevoirPacket( __OUTPUT Packet *packet,
	SOCKET socket )
{
	// Donnees
	char *data = NULL;

	// Taille
	unsigned int taille = 0;

	// Recevoir
	if( !PacketIO_RecevoirPacketInterne( &data,
		&taille,
		socket ) )
	{
		// Notifier
		printf( "PacketIO_RecevoirPacket( )::Echec de la reception.\n" );

		// Quitter
		return FALSE;
	}

	// Traiter les donnees selon le type de reception
	if( Packet_EstFichier( packet ) )
		// Inscrire
		return FileManipulation_Ecrire( packet->m_data,
			data,
			taille,
			FALSE );
	else
	{
		// Copier les donnees
		packet->m_data = data;
		packet->m_taille = taille;
	}

	// OK
	return TRUE;
}

// Envoyer un packet
BOOL PacketIO_EnvoyerPacket( const Packet *packet,
	const ClientServeur *client )
{
	// Donnees dans un fichier
	if( Packet_EstFichier( packet ) )
	{
		// Envoyer les donnees
		return PacketIO_EnvoyerPacketInterneFichier( packet->m_data,
			client );
	}
	// Donnees dans le packet
	else
		// Envoyer les donnees
		return PacketIO_EnvoyerPacketInterneData( packet->m_data,
			packet->m_taille,
			ClientServeur_ObtenirSocket( client ) );
}

