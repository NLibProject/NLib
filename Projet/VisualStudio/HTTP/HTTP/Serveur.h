#ifndef SERVEUR_PROTECT
#define SERVEUR_PROTECT

// Port du serveur
#define PORT_SERVEUR				80

// Connexion simulatanees max
#define MAXIMUM_CLIENTS_SIMULTANES	30

// struct Serveur
typedef struct
{
	// Socket du serveur
	SOCKET m_socket;

	// Adressage serveur
	SOCKADDR_IN m_adressageServeur;

	// Clients serveur
	ClientServeur **m_clients;

	// Nombre de clients
	unsigned int m_nombreClients;

	// Thread d'update
	HANDLE m_threadUpdate;

	// Thread d'acceptation client
	HANDLE m_threadAcceptationClient;

	// Connexion autorisee?
	BOOL m_estConnexionAutorisee;

	// Cache log
	CacheLog *m_cacheLog;

	// Mutex
#ifdef IS_WINDOWS
	HANDLE m_mutex;
#endif // IS_WINDOWS

	// Est en cours
	BOOL m_estEnCours;
} Serveur;

// Construire le serveur
__ALLOC Serveur *Serveur_Construire( unsigned short port );

// Detruire le serveur
void Serveur_Detruire( Serveur** );

// Est en cours
BOOL Serveur_EstEnCours( const Serveur* );

// Obtenir le nombre de clients
unsigned int Serveur_ObtenirNombreClients( const Serveur* );

// Accepter un client
BOOL Serveur_AccepterClient( Serveur* );

// Nettoyer les clients morts
unsigned int Serveur_NettoyerClients( Serveur* );

// Tuer tous les clients
void Serveur_TuerTousClients( Serveur* );

// Thread de nettoyage
void Serveur_ThreadUpdate( Serveur* );

// Autoriser la connexion
void Serveur_AutoriserConnexion( Serveur* );

// Interdire la connexion
void Serveur_InterdireConnexion( Serveur* );

// Arreter le serveur
void Serveur_Arreter( Serveur* );

#endif // !SERVEUR_PROTECT

