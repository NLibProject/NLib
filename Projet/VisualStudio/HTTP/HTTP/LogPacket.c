#include "Types.h"

// Loguer packet
void LogPacket_LoguerPacket( const char *rawData,
	unsigned int taille,
	const char *ip )
{
	// Buffer
	char buffer[ 4096 ];

	// Base nom fichier
	char *baseNom;
	BOOL estDoitLibererBaseNom = TRUE;

	// Repertoire
	char *repertoire;
	BOOL estDoitLibererRepertoire = TRUE;

	// Indice
	unsigned int indice = 0;

	// Fichier
	FILE *fichier;

	// Creer le dossier
	_mkdir( REPERTOIRE_LOG_PACKET );

	// Obtenir date
	if( !( repertoire = Log_ObtenirTemps( 2 ) ) )
	{
		baseNom = NOM_FICHIER_LOG_PACKET_ERREUR;
		estDoitLibererRepertoire = FALSE;
	}

	// Creer le lien vers le repertoire a creer
	memcpy( buffer,
		REPERTOIRE_LOG_PACKET,
		strlen( REPERTOIRE_LOG_PACKET ) );
	buffer[ strlen( REPERTOIRE_LOG_PACKET ) ] = '/';
	memcpy( buffer + strlen( REPERTOIRE_LOG_PACKET ) + 1,
		repertoire,
		strlen( repertoire ) );

	// Creer le repertoire
	_mkdir( buffer );

	// Obtenir base nom
	if( !( baseNom = Log_ObtenirTemps( 3 ) ) )
	{
		baseNom = NOM_FICHIER_LOG_PACKET_ERREUR;
		estDoitLibererBaseNom = FALSE;
	}

	// Composer le nom
	do
	{
		// Construire
		if( !indice )
			sprintf( buffer,
				"%s/%s/%s-%s.raw",
				REPERTOIRE_LOG_PACKET,
				repertoire,
				baseNom,
				ip );
		else
			sprintf( buffer,
				"%s/%s/%s-%s_%d.raw",
				REPERTOIRE_LOG_PACKET,
				repertoire,
				baseNom,
				ip,
				indice );

		// Incrementer indice
		indice++;
	} while( FileManipulation_EstExiste( buffer ) );

	// Liberer
	if( estDoitLibererBaseNom )
		FREE( baseNom );
	if( estDoitLibererRepertoire )
		FREE( repertoire );

	// Ouvrir le fichier
	if( !( fichier = fopen( buffer,
		"wb+" ) ) )
	{
		perror( "LogPacket_LoguerPacket( )::Impossible d'ouvrir le fichier: " );
		return;
	}

	// Ecrire
	fwrite( rawData,
		sizeof( char ),
		taille,
		fichier );

	// Fermer le fichier
	fclose( fichier );
}

