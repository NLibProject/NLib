#include "Types.h"

// Thread de gestion des acceptation clients serveur
void ThreadAcceptationClientServeur_Thread( Serveur *serveur )
{
	// Boucle d'acceptation
	do
	{
		// Accepter client
		Serveur_AccepterClient( serveur );

		// Delais
		Sleep( 1 );
	} while( Serveur_EstEnCours( serveur ) );
}

