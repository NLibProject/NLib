#define RESEAU_INTERNE
#include "Types.h"

// Initialiser reseau
BOOL Reseau_Initialiser( void )
{
	// Verifier
	if( Reseau_EstInitialise( ) )
	{
		// Notifier
		printf( "Reseau_Initialiser( )::Le reseau est deja initialise.\n" );

		// Quitter
		return FALSE;
	}

#ifdef IS_WINDOWS
	if( WSAStartup( MAKEWORD( 2, 2 ),
		&m_contexteWSA ) != EXIT_SUCCESS )
	{
		// Notifier
		perror( "Reseau_Initialiser( )::WSAStartup( ): " );

		// Quitter
		return FALSE;
	}
#endif

	// OK
	return m_estInitialise = TRUE;
}

// Est reseau initialise?
BOOL Reseau_EstInitialise( void )
{
	return m_estInitialise;
}

// Detruire reseau
void Reseau_Detruire( void )
{
	// Verifier
	if( !Reseau_EstInitialise( ) )
	{
		// Notifier
		printf( "Reseau_Detruire( )::Le reseau n'est pas initialise.\n" );

		// Quitter
		return;
	}

	// Sous windows
#ifdef IS_WINDOWS
	WSACleanup( );
#endif // IS_WINDOWS
}

