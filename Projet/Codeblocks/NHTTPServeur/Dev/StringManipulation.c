#include "Types.h"

// Is String EOF
BOOL StringManipulation_IsEOF( const char *src,
	unsigned int cursor )
{
	return cursor >= strlen( src );
}

// Look for
BOOL StringManipulation_LookForCharacter( const char *src,
	char c,
	unsigned int *currentPosition )
{
	// Character read
	char cR;

	// Look for character
	do
	{
		// Read
		cR = src[ *currentPosition ];

		// Increase position
		(*currentPosition)++;
	} while( !StringManipulation_IsEOF( src,
		*currentPosition )
		&& cR != c );

	// OK
	return ( c == cR );
}

BOOL StringManipulation_LookForString( const char *src,
	const char *s,
	unsigned int *currentPosition )
{
	// Cursor
	unsigned int searchCursor = 0;

	// Length of string
	unsigned int length;

	// Get the string length
	length = strlen( s );

	// Look for string
	while( searchCursor < length
		&& !StringManipulation_IsEOF( src,
			*currentPosition ) )
		// Can't find character
		if( StringManipulation_GetNextCharacter( src,
			currentPosition,
			FALSE ) != s[ searchCursor ] )
			searchCursor = 0;
		// Character found
		else
			searchCursor++;

	// Return result
	return ( searchCursor >= length );
}

// Read
__ALLOC char *StringManipulation_ReadBetween( const char *src,
	char c1,
	char c2,
	unsigned int *currentPosition,
	BOOL isConservePosition )
{
	// Output
	__OUTPUT char *out = NULL;

	// Initial position
	unsigned int initialPosition = *currentPosition;

	// Temp array
	char *temp;

	// Character read
	char cR;

	// Cursor where to copy
	unsigned int cursor = 0;

	// If character isn't the escape character
	if( c1 != '\0' )
		// Look for first character
		if( !StringManipulation_LookForCharacter( src,
			c1,
			currentPosition ) )
			return NULL;

	// Read
	do
	{
		// If character different of the end character
		if( src[ *currentPosition ] != c2 )
		{
			// Not allocated now
			if( out == NULL )
			{
				// Allocate memory
				if( !( out = calloc( 1 /* + 1 character */ + 1 /* \0 */,
					sizeof( char ) ) ) )
				{
					// Notify
					perror( "Allocation failed: " );

					// Quit
					return NULL;
				}

				// Where to copy
				cursor = 0;
			}
			else
			{
				// Save address
				temp = out;

				// Allocate new size
				if( !( out = calloc( strlen( temp ) + 1 /* + 1 character */ + 1 /* \0 */,
					sizeof( char ) ) ) )
				{
					// Notify
					perror( "Allocation failed: " );

					// Free
					FREE( temp );

					// Quit
					return NULL;
				}

				// Copy old data
				memcpy( out,
					temp,
					strlen( temp ) );

				// Where to copy
				cursor = strlen( temp );

				// Free
				FREE( temp );
			}

			// Copy new character
			out[ cursor ] = src[ *currentPosition ];
		}

		// Character read
		cR = src[ *currentPosition ];

		// Increase cursor
		if( !StringManipulation_IsEOF( src,
			*currentPosition ) )
			(*currentPosition)++;
	} while( !StringManipulation_IsEOF( src,
			*currentPosition )
		&& cR != c2 );

	// If we want to conserve initial position
	if( isConservePosition )
		*currentPosition = initialPosition;

	// OK
	return out;
}

__ALLOC char *StringManipulation_ReadBetween2( const char *src,
	char c,
	unsigned int *currentPosition,
	BOOL isConservePosition )
{
	return StringManipulation_ReadBetween( src,
		c,
		c,
		currentPosition,
		isConservePosition );
}

__ALLOC char *StringManipulation_ReadUntil( const char *src,
	char c,
	unsigned int *currentPosition,
	BOOL isConservePosition )
{
	return StringManipulation_ReadBetween( src,
		'\0',
		c,
		currentPosition,
		isConservePosition );
}

// Get next character
char StringManipulation_GetNextCharacter( const char *src,
	unsigned int *currentPosition,
	BOOL isConservePosition )
{
	// Character read
	__OUTPUT char cR;

	// Initial position
	unsigned int initialPosition = *currentPosition;

	// Continue?
	BOOL isContinue = TRUE;

	// Look for non empty character
	do
	{
		// Read character
		cR = src[ *currentPosition ];

		// Analyse
		switch( cR )
		{
			case ' ':
			case '\n':
			case '\t':
			case '\r':
				break;

			default:
				isContinue = FALSE;
				break;
		}

		// Increase cursor
		(*currentPosition)++;
	} while( isContinue );

	// If we want to conserve position
	if( isConservePosition )
		*currentPosition = initialPosition;

	// OK
	return cR;
}

