#define TYPEENTREELOG_INTERNE
#include "Types.h"

// ------------------
// enum TypeEntreeLog
// ------------------

// Obtenir type entree log
const char *TypeEntreeLog_ObtenirTypeEntree( TypeEntreeLog type )
{
	return TypeEntreeLogTexte[ type ];
}

