#include "Types.h"

// Afficher l'aide (privee)
void GestionCommandeServeur_AfficherAide( void )
{
	// Iterateur
	CommandeServeur commande = (CommandeServeur)0;

	// Decrire
	printf( "Les commandes suivantes peuvent etre saisies soit via leur identifiant entre [], soit grace a leur nom:\n\n" );

	// Afficher
	for( ; commande < COMMANDES_SERVEUR; commande++ )
		printf( "[%d]\t\"%s\"\t[%s]\n",
			commande,
			CommandeServeur_ObtenirCommande( commande ),
			CommandeServeur_ObtenirDescriptionCommande( commande ) );

	// Nouvelle ligne
	NOUVELLE_LIGNE_CONSOLE( );
}

// Update list (privee)
void GestionCommandeServeur_UpdateList( void )
{
	UpdateList_Update( );
}

// Afficher les informations concernant le serveur (privee)
void GestionCommandeServeur_APropos( void )
{
	// About
	puts( "NServeurHTTP par SOARES Lucas (http://nproject.ddns.net/)\n(Merci a Minou pour son soutien)\n(Remerciement a Milou pour son remerciement)" );

	// Nouvelle ligne
	NOUVELLE_LIGNE_CONSOLE( );
}

// Obtenir l'adresse ip de la machine
void GestionCommandeServeur_GetIP( void )
{
	// Outil IPv4
#ifdef IS_WINDOWS
	system( "echo." );
	system( "echo Votre IP est la suivante: " );
	system( "echo." );
	system( "ipconfig | findstr /R /C:\"Adresse IPv4\"" );
	system( "echo." );
#else // IS_WINDOWS
	printf( "Adresses cartes:\n\n" );
	system( "ifconfig | grep \"inet addr\"" );
#endif // !IS_WINDOWS
}

void GestionCommandeServeur_FermerServeur( Serveur *serveur )
{
	// Fermer
	Serveur_InterdireConnexion( serveur );

	// Notifier
	printf( "[SERVEUR]\tLa connexion est desormais interdite\n" );

	// Nouvelle ligne
	NOUVELLE_LIGNE_CONSOLE( );
}

void GestionCommandeServeur_OuvrirServeur( Serveur *serveur )
{
	// Ouvrir
	Serveur_AutoriserConnexion( serveur );

	// Notifier
	printf( "[SERVEUR]\tLa connexion est desormais autorisee\n" );

	// Nouvelle ligne
	NOUVELLE_LIGNE_CONSOLE( );
}

// Lire la commande (privee)
CommandeServeur GestionCommandeServeur_LireCommandeInterne( void )
{
	// Buffer
	char buffer[ 2048 ] = { 0, };

	// Identifiant
	long identifiant;

	// Lire
	scanf( "%s",
		buffer );

	// Retour a la ligne
	NOUVELLE_LIGNE_CONSOLE( );

	// Commande numero
	if( Caractere_EstUnChiffre( buffer[ 0 ] ) )
	{
		// Identifiant
		identifiant = strtol( buffer,
			NULL,
			10 );

		// Verifier
		if( identifiant < 0
			|| identifiant >= COMMANDES_SERVEUR )
			return COMMANDES_SERVEUR;

		// Retourner l'id
		return (CommandeServeur)identifiant;
	}
	// Commande texte
	else
		return CommandeServeur_ParserCommande( buffer );
}

// Traiter la commande (privee)
void GestionCommandeServeur_TraiterCommandeInterne( CommandeServeur commande,
	Serveur *serveur )
{
	// Analyser la commande
	switch( commande )
	{
		case COMMANDE_SERVEUR_AIDE:
		case COMMANDE_SERVEUR_AIDE2:
			GestionCommandeServeur_AfficherAide( );
			break;

		case COMMANDE_SERVEUR_UPDATE_LIST:
			GestionCommandeServeur_UpdateList( );
			break;

		case COMMANDE_SERVEUR_GET_IP:
			GestionCommandeServeur_GetIP( );
			break;

		case COMMANDE_SERVEUR_FERMER_SERVEUR:
			GestionCommandeServeur_FermerServeur( serveur );
			break;
		case COMMANDE_SERVEUR_OUVRIR_SERVEUR:
			GestionCommandeServeur_OuvrirServeur( serveur );
			break;

		case COMMANDE_SERVEUR_A_PROPOS:
			GestionCommandeServeur_APropos( );
			break;

		case COMMANDE_SERVEUR_QUITTER:
		case COMMANDE_SERVEUR_QUITTER2:
			Serveur_Arreter( serveur );
			break;

		default:
			printf( "[SERVEUR]\tCommande inconnue, tapez aide pour plus d'informations sur les commandes\n" );
			break;
	}
}

// Gerer les commandes
void GestionCommandeServeur_Gerer( Serveur *serveur )
{
	// Commande
	CommandeServeur commande;

	// Lire commande
	commande = GestionCommandeServeur_LireCommandeInterne( );

	// Traiter la commande
	GestionCommandeServeur_TraiterCommandeInterne( commande,
		serveur );
}

