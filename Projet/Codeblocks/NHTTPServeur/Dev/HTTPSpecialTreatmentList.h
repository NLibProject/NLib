#ifndef HTTPSPECIALTREATMENTLIST_PROTECT
#define HTTPSPECIALTREATMENTLIST_PROTECT

// Le nom de l'ic�ne demande par le navigateur pour la miniature automatique
#define FAVORITE_ICON_DEFAULT_NAME		"favicon.ico"

// enum HTTPSpecialTreatmentList
typedef enum
{
	// Rien de particulier, tout se passe bien
	HTTP_SPECIAL_TREATMENT_NO_SPECIAL,

	// Le fichier n'a pas ete trouve
	HTTP_SPECIAL_TREATMENT_404_PAGE,

	// Tentative de cross directory
	HTTP_SPECIAL_TREATMENT_CROSS_DIRECTORY,

	// Le favicon.ico n'existe pas, on envoie celui par default
	HTTP_SPECIAL_TREATMENT_FAVICON,

	// Le fichier demande n'existe pas mais sa version en .html existe
	HTTP_SPECIAL_TREATMENT_ADD_HTML,

	// Obtenir index.html dans le subdir donnee
	HTTP_SPECIAL_TREATMENT_GET_SUBDIR,

	// Obtenir la page de description du serveur
	HTTP_SPECIAL_TREATMENT_GET_DESCRIPTION_PAGE,

	HTTP_SPECIAL_TREATMENTS
} HTTPSpecialTreatmentList;

/* Determiner le type de traitement selon les parametres actuels */
HTTPSpecialTreatmentList HTTPSpecialTreatmentList_GetTreatment( const char *fichier );

#endif // !HTTPSPECIALTREATMENTLIST_PROTECT

