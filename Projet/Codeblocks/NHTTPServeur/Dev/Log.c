#include "Types.h"

// ---
// Log
// ---

// Ouvrir le fichier de log (privee)
FILE *Log_OuvrirLogInterne( void )
{
	return fopen( NOM_FICHIER_LOG,
		"a" );
}

// Obtenir temps
__ALLOC char *Log_ObtenirTemps( unsigned int version )
{
	// Temps
	struct tm temps;

	// Timestamp
	time_t timestamp;

	// Buffer
	char *buffer;

	// Obtenir timestamp
	timestamp = time( NULL );

	// Convertir en tm
	temps = *gmtime( &timestamp );

	// Allouer
	if( !( buffer = calloc( 2048,
		sizeof( char ) ) ) )
		return NULL;

	// Obtenir temps
	switch( version )
	{
		case 0:
			strftime( buffer,
				2048,
				"%d/%m/%Y, %H:%M:%S",
				&temps );
			break;

		case 2:
			strftime( buffer,
				2048,
				"%d_%m_%Y",
				&temps );
			break;

		case 3:
			strftime( buffer,
				2048,
				"%H_%M_%S",
				&temps );
			break;

		default:
		case 1:
			strftime( buffer,
				2048,
				"D(%d_%m_%Y)H(%H_%M_%S)",
				&temps );
			break;
	}

	// OK
	return buffer;
}

// Loguer le demarrage
void Log_LoguerDemarrage( unsigned int port )
{
	// Log
	FILE *fichier;

	// Temps
	char *temps;

	// Ouvrir le log
	if( !( fichier = Log_OuvrirLogInterne( ) ) )
	{
		// Notifier
		perror( "Log_LoguerDemarrage( )::Impossible d'ouvrir le log: " );

		// Quitter
		return;
	}

	// Obtenir temps
	temps = Log_ObtenirTemps( 0 );

	// Ecrire
	fprintf( fichier,
		"[%s][%s]\t[SERVEUR]\tEcoute sur le port %d\n\n",
		temps,
		TypeEntreeLog_ObtenirTypeEntree( TYPE_ENTREE_LOG_DEMARRAGE ),
		port );

	// Liberer
	FREE( temps );

	// Fermer le log
	fclose( fichier );
}

// Loguer une connexion
void Log_LoguerConnexion( const char *ip,
	unsigned int identifiant )
{
	// Log
	FILE *fichier;

	// Temps
	char *temps;

	// Ouvrir le log
	if( !( fichier = Log_OuvrirLogInterne( ) ) )
	{
		// Notifier
		perror( "Log_LoguerConnexion( )::Impossible d'ouvrir le log: " );

		// Quitter
		return;
	}

	// Obtenir temps
	temps = Log_ObtenirTemps( 0 );

	// Ecrire
	fprintf( fichier,
		"[%s][%s]\t[%s]\t(%d)\tse connecte\n",
		temps,
		TypeEntreeLog_ObtenirTypeEntree( TYPE_ENTREE_LOG_CONNEXION ),
		ip,
		identifiant );

	// Liberer
	FREE( temps );

	// Fermer le log
	fclose( fichier );
}

// Loguer une deconnexion
void Log_LoguerDeconnexion( const char *ip,
	unsigned int identifiant )
{
	// Log
	FILE *fichier;

	// Temps
	char *temps;

	// Ouvrir le log
	if( !( fichier = Log_OuvrirLogInterne( ) ) )
	{
		// Notifier
		perror( "Log_LoguerDeconnexion( )::Impossible d'ouvrir le log: " );

		// Quitter
		return;
	}

	// Obtenir temps
	temps = Log_ObtenirTemps( 0 );

	// Ecrire
	fprintf( fichier,
		"[%s][%s]\t[%s]\t(%d)\tse deconnecte\n",
		temps,
		TypeEntreeLog_ObtenirTypeEntree( TYPE_ENTREE_LOG_DECONNEXION ),
		ip,
		identifiant );

	// Liberer
	FREE( temps );

	// Fermer le log
	fclose( fichier );
}

// Loguer agent client
void Log_LoguerAgentClient( const char *ip,
	unsigned int identifiant,
	const char *agent )
{
	// Log
	FILE *fichier;

	// Temps
	char *temps;

	// Ouvrir le log
	if( !( fichier = Log_OuvrirLogInterne( ) ) )
	{
		// Notifier
		perror( "Log_LoguerAgentClient( )::Impossible d'ouvrir le log: " );

		// Quitter
		return;
	}

	// Obtenir temps
	temps = Log_ObtenirTemps( 0 );

	// Ecrire
	fprintf( fichier,
		"[%s][%s]\t[%s]\t(%d)\tutilise \"%s\"\n",
		temps,
		TypeEntreeLog_ObtenirTypeEntree( TYPE_ENTREE_LOG_AGENT_CLIENT ),
		ip,
		identifiant,
		agent );

	// Liberer
	FREE( temps );

	// Fermer le log
	fclose( fichier );
}

// Loguer une demande de fichier
void Log_LoguerDemandeFichier( const char *ip,
	unsigned int identifiant,
	const char *lien )
{
	// Log
	FILE *fichier;

	// Temps
	char *temps;

	// Ouvrir le log
	if( !( fichier = Log_OuvrirLogInterne( ) ) )
	{
		// Notifier
		perror( "Log_LoguerDemandeFichier( )::Impossible d'ouvrir le log: " );

		// Quitter
		return;
	}

	// Obtenir temps
	temps = Log_ObtenirTemps( 0 );

	// Ecrire
	fprintf( fichier,
		"[%s][%s]\t[%s]\t(%d)\tdemande \"%s\"\n",
		temps,
		TypeEntreeLog_ObtenirTypeEntree( TYPE_ENTREE_LOG_DEMANDE_FICHIER ),
		ip,
		identifiant,
		lien );

	// Liberer
	FREE( temps );

	// Fermer le log
	fclose( fichier );
}

// Loguer une fin d'envoi
void Log_LoguerFinEnvoi( const char *ip,
	unsigned int identifiant,
	const char *lien )
{
	// Log
	FILE *fichier;

	// Temps
	char *temps;

	// Ouvrir le log
	if( !( fichier = Log_OuvrirLogInterne( ) ) )
	{
		// Notifier
		perror( "Log_LoguerFinEnvoi( )::Impossible d'ouvrir le log: " );

		// Quitter
		return;
	}

	// Obtenir temps
	temps = Log_ObtenirTemps( 0 );

	// Ecrire
	fprintf( fichier,
		"[%s][%s]\t[%s]\t(%d)\ta fini \"%s\"\n",
		temps,
		TypeEntreeLog_ObtenirTypeEntree( TYPE_ENTREE_LOG_FIN_ENVOI ),
		ip,
		identifiant,
		lien );

	// Liberer
	FREE( temps );

	// Fermer le log
	fclose( fichier );
}

// Loguer etat envoi
void Log_LoguerEnvoi( const char *ip,
	unsigned int identifiant,
	const char *lien,
	unsigned long tailleEnvoyee,
	unsigned long taille,
	double debit )
{
	// Log
	FILE *fichier;

	// Temps
	char *temps;

	// Ouvrir le log
	if( !( fichier = Log_OuvrirLogInterne( ) ) )
	{
		// Notifier
		perror( "Log_LoguerEnvoi( )::Impossible d'ouvrir le log: " );

		// Quitter
		return;
	}

	// Obtenir temps
	temps = Log_ObtenirTemps( 0 );

	// Ecrire
	fprintf( fichier,
		"[%s][%s]\t[%s]\t(%d)\t<< [%s]\t| %lu / %lu octets\t| %1.0f ko/s\t|\n",
		temps,
		TypeEntreeLog_ObtenirTypeEntree( TYPE_ENTREE_LOG_STATUT_ENVOI ),
		ip,
		identifiant,
		lien,
		tailleEnvoyee,
		taille,
		( (double)debit / 1000.0f ) );

	// Liberer
	FREE( temps );

	// Fermer le log
	fclose( fichier );
}

void Log_LoguerCrossDirectory( const char *ip,
	unsigned int identifiant )
{
	// Log
	FILE *fichier;

	// Temps
	char *temps;

	// Ouvrir le log
	if( !( fichier = Log_OuvrirLogInterne( ) ) )
	{
		// Notifier
		perror( "Log_LoguerEnvoi( )::Impossible d'ouvrir le log: " );

		// Quitter
		return;
	}

	// Obtenir temps
	temps = Log_ObtenirTemps( 0 );

	// Ecrire
	fprintf( fichier,
		"[%s][%s]\t[%s]\t(%d)\tCross directory!\n",
		temps,
		TypeEntreeLog_ObtenirTypeEntree( TYPE_ENTREE_LOG_CROSS_DIRECTORY ),
		ip,
		identifiant );

	// Liberer
	FREE( temps );

	// Fermer le log
	fclose( fichier );
}

/* Obtenir le nombre de ms depuis le lancement */
unsigned int Log_ObtenirTick( void )
{
#ifdef IS_WINDOWS
	return GetTickCount( );
#else // IS_WINDOWS
	// Stockage temps
	struct timespec now;

	// Recuperer le temps actuel
	if( clock_gettime( CLOCK_MONOTONIC,
		&now ) )
		// Impossible d'obtenir le temps
		return 0;

	// OK
	return now.tv_sec * 1000.0 + now.tv_nsec / 1000000.0;
#endif // !IS_WINDOWS
}

