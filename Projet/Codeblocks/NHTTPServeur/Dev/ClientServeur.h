#ifndef CLIENTSERVEUR_PROTECT
#define CLIENTSERVEUR_PROTECT

// Limite pour l'identifiant
#define LIMITE_IDENTIFIANT_CLIENT_SERVEUR		100000

// struct ClientServeur
typedef struct
{
	// Socket client
	SOCKET m_socket;

	// Contexte d'adressage
	SOCKADDR_IN m_contexteAdressage;

	// Identifiant unique
	unsigned int m_identifiantUnique;

	// Thread emission
#ifdef IS_WINDOWS
	HANDLE m_threadEmission;
#else // IS_WINDOWS
	pthread_t m_threadEmission;
#endif // !IS_WINDOWS

	// Thread reception
#ifdef IS_WINDOWS
	HANDLE m_threadReception;
#else // IS_WINDOWS
	pthread_t m_threadReception;
#endif // !IS_WINDOWS

	// Cache packets
	CachePacket *m_cachePacket;

	// Callback cache log
	CacheLog *m_callbackCacheLog;

	// IP du client
	char *m_ip;

	// Est mort? (Doit etre deco/supprime)
	BOOL m_estMort;

	// Est en cours?
	BOOL m_estEnCours;
} ClientServeur;

// Construire le client
__ALLOC ClientServeur *ClientServeur_Construire( SOCKET socketServeur,
	CacheLog *cacheLog,
	const BOOL *estConnexionAutorisee );

// Detruire le client
void ClientServeur_Detruire( ClientServeur** );

// Est mort?
BOOL ClientServeur_EstMort( const ClientServeur* );

// Est en cours?
BOOL ClientServeur_EstEnCours( const ClientServeur* );

// Est packet(s) dans le cache?
BOOL ClientServeur_EstPacketsDansCache( const ClientServeur* );

// Tuer le client
void ClientServeur_Tuer( ClientServeur* );

// Definir identifiant
void ClientServeur_DefinirIdentifiantUnique( ClientServeur*,
	unsigned int identifiant );

// Obtenir l'identifiant
unsigned int ClientServeur_ObtenirIdentifiant( const ClientServeur* );

// Obtenir SOCKET
SOCKET ClientServeur_ObtenirSocket( const ClientServeur* );

// Obtenir cache packet
CachePacket *ClientServeur_ObtenirCachePacket( const ClientServeur* );

// Obtenir cache log
CacheLog *ClientServeur_ObtenirCacheLog( const ClientServeur* );

// Ajouter un packet
BOOL ClientServeur_AjouterPacket( ClientServeur*,
	__WILLBEOWNED const Packet *packet );

// Obtenir adresse ip
const char *ClientServeur_ObtenirAdresseIP( const ClientServeur* );

#endif // !CLIENTSERVEUR_PROTECT

