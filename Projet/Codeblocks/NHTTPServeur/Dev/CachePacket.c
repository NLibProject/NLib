#include "Types.h"

/* struct CachePacket */
// Construire cache packet
__ALLOC CachePacket *CachePacket_Construire( void )
{
	// Output
	__OUTPUT CachePacket *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( CachePacket ) ) ) )
	{
		// Notifier
		perror( "CachePacket_Construire( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Construire mutex
#ifdef IS_WINDOWS
	if( !( out->m_mutex = CreateMutex( NULL,
		FALSE,
		NULL ) ) )
#else // IS_WINDOWS
	if( pthread_mutex_init( &out->m_mutex,
		NULL ) )
#endif // !IS_WINDOWS
	{
		// Notifier
		perror( "CachePacket_Construire( )::Impossible de construire le mutex: " );

		// Liberer
		FREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

// Detruire cache packet
void CachePacket_Detruire( CachePacket **this )
{
	// Supprimer tous les packets
	while( CachePacket_ObtenirNombrePackets( *this ) > 0 )
		CachePacket_SupprimerPacket( *this );

	// Fermer mutex
#ifdef IS_WINDOWS
	CloseHandle( (*this)->m_mutex );
#else // IS_WINDOWS
	pthread_mutex_destroy( &(*this)->m_mutex );
#endif // !IS_WINDOWS

	// Liberer la memoire
	FREE( (*this) );
}

// Obtenir le nombre de packets
unsigned int CachePacket_ObtenirNombrePackets( const CachePacket *this )
{
	// Resultat
	__OUTPUT unsigned int out;

	// Lock le mutex
#ifdef IS_WINDOWS
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "CachePacket_ObtenirNombrePackets( )::Impossible de lock le mutex: " );

		// Quitter
		return 0;
	}
#else // IS_WINDOWS
	pthread_mutex_lock( &((CachePacket*)this)->m_mutex );
#endif // !IS_WINDOWS

	// Copier le resultat
	out = this->m_nombrePacket;

	// Release le mutex
#ifdef IS_WINDOWS
	ReleaseMutex( this->m_mutex );
#else // IS_WINDOWS
	pthread_mutex_unlock( &((CachePacket*)this)->m_mutex );
#endif // !IS_WINDOWS

	// OK
	return out;
}

// Obtenir le packet a envoyer
__ALLOC Packet *CachePacket_ObtenirPacket( const CachePacket *this )
{
	// Packet
	__OUTPUT Packet *packet;

	// Lock le mutex
#ifdef IS_WINDOWS
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "CachePacket_ObtenirPacket( )::Impossible de lock le mutex: " );

		// Quitter
		return NULL;
	}
#else // IS_WINDOWS
	pthread_mutex_lock( &((CachePacket*)this)->m_mutex );
#endif // !IS_WINDOWS

	// Verifier le contenu du tableau de packet
	if( this->m_nombrePacket == 0 )
	{
		// Notifier
		printf( "CachePacket_ObtenirPacket( )::Il n'y a aucun packet dans le cache.\n" );

		// Release mutex
#ifdef IS_WINDOWS
		ReleaseMutex( this->m_mutex );
#else // IS_WINDOWS
		pthread_mutex_unlock( &((CachePacket*)this)->m_mutex );
#endif // !IS_WINDOWS

		// Quitter
		return NULL;
	}

	// Construire le packet
	if( !( packet = Packet_Construire2( this->m_packet[ 0 ] ) ) )
	{
		// Notifier
		printf( "CachePacket_ObtenirPacket( )::Impossible d'obtenir le packet.\n" );

		// Release le mutex
#ifdef IS_WINDOWS
		ReleaseMutex( this->m_mutex );
#else // IS_WINDOWS
		pthread_mutex_unlock( &((CachePacket*)this)->m_mutex );
#endif // !IS_WINDOWS

		// Quitter
		return NULL;
	}

	// Copier statut
	packet->m_estFichier = this->m_packet[ 0 ]->m_estFichier;

	// Release le mutex
#ifdef IS_WINDOWS
	ReleaseMutex( this->m_mutex );
#else // IS_WINDOWS
	pthread_mutex_unlock( &((CachePacket*)this)->m_mutex );
#endif // !IS_WINDOWS

	// OK
	return packet;
}

// Est packet(s) dans le cache?
BOOL CachePacket_EstPacketsDansCache( const CachePacket *this )
{
	return CachePacket_ObtenirNombrePackets( this ) > 0;
}

// Ajouter un packet
BOOL CachePacket_AjouterPacket( CachePacket *this,
	__WILLBEOWNED const Packet *packet )
{
	// Ancienne adresse
	Packet **ancienCache;

	// Index ajout
	unsigned int index;

	// Iterateur
	unsigned int i;

	// Lock le mutex
#ifdef IS_WINDOWS
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "CachePacket_AjouterPacket( )::Impossible de lock le mutex: " );

		// Quitter
		return FALSE;
	}
#else // IS_WINDOWS
	pthread_mutex_lock( &this->m_mutex );
#endif // !IS_WINDOWS

	if( this->m_packet == NULL )
	{
		// Allouer le nouveau cache
		if( !( this->m_packet = calloc( 1,
			sizeof( Packet* ) ) ) )
		{
			// Notifier
			perror( "CachePacket_AjouterPacket( )::Impossible d'allouer la memoire: " );

			// Release mutex
#ifdef IS_WINDOWS
			ReleaseMutex( this->m_mutex );
#else // IS_WINDOWS
			pthread_mutex_unlock( &this->m_mutex );
#endif // !IS_WINDOWS

			// Quitter
			return FALSE;
		}

		// Ajout a la case 0
		index = 0;
	}
	else
	{
		// Enregistrer ancien cache
		ancienCache = this->m_packet;

		// Allouer le nouveau cache
		if( !( this->m_packet = calloc( this->m_nombrePacket + 1,
			sizeof( Packet* ) ) ) )
		{
			// Notifier
			perror( "CachePacket_AjouterPacket( )::Impossible d'allouer la memoire: " );

			// Restaurer
			this->m_packet = ancienCache;

			// Release mutex
#ifdef IS_WINDOWS
			ReleaseMutex( this->m_mutex );
#else // IS_WINDOWS
			pthread_mutex_unlock( &this->m_mutex );
#endif // !IS_WINDOWS

			// Quitter
			return FALSE;
		}

		// Copier les packets
		for( i = 0; i < this->m_nombrePacket; i++ )
			this->m_packet[ i ] = ancienCache[ i ];

		// Ajout a la fin
		index = this->m_nombrePacket;
	}

	// Ajouter
	this->m_packet[ index ] = (Packet*)packet;

	// Incrementer le nombre de packets
	this->m_nombrePacket++;

	// Release le mutex
#ifdef IS_WINDOWS
	ReleaseMutex( this->m_mutex );
#else // IS_WINDOWS
	pthread_mutex_unlock( &this->m_mutex );
#endif // !IS_WINDOWS

	// OK
	return TRUE;
}

// Supprimer le packet
BOOL CachePacket_SupprimerPacket( CachePacket *this )
{
	// Ancien cache
	Packet **ancienCache;

	// Iterateur
	unsigned int i;

	// Lock mutex
#ifdef IS_WINDOWS
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		perror( "CachePacket_SupprimerPacket( )::Impossible de lock le mutex: " );

		// Quitter
		return FALSE;
	}
#else // IS_WINDOWS
	pthread_mutex_lock( &this->m_mutex );
#endif // !IS_WINDOWS

	// Verifier le nombre de packet
	if( this->m_nombrePacket == 0 )
	{
		// Notifier
		printf( "CachePacket_SupprimerPacket( )::Il n'y a pas de packet dans le cache.\n" );

		// Release mutex
#ifdef IS_WINDOWS
		ReleaseMutex( this->m_mutex );
#else // IS_WINDOWS
		pthread_mutex_unlock( &this->m_mutex );
#endif // !IS_WINDOWS

		// Quitter
		return FALSE;
	}

	// Liberer la memoire
	Packet_Detruire( &this->m_packet[ 0 ] );

	// Si c'etait le dernier packet
	if( this->m_nombrePacket == 1 )
	{
		// Liberer le contenant
		FREE( this->m_packet );
	}
	else
	{
		// Enregistrer l'ancien cache
		ancienCache = this->m_packet;

		// Allouer la memoire
		if( !( this->m_packet = calloc( this->m_nombrePacket - 1,
			sizeof( Packet* ) ) ) )
		{
			// Notifier
			perror( "CachePacket_SupprimerPacket( )::Impossible d'allouer la memoire: " );

			// Restaurer
			this->m_packet = ancienCache;

			// Release mutex
#ifdef IS_WINDOWS
			ReleaseMutex( this->m_mutex );
#else // IS_WINDOWS
			pthread_mutex_unlock( &this->m_mutex );
#endif // !IS_WINDOWS

			// Quiter
			return FALSE;
		}

		// Copier
		for( i = 1; i < this->m_nombrePacket; i++ )
			this->m_packet[ i - 1 ] = ancienCache[ i ];
	}

	// Decrementer le nombre de packet
	this->m_nombrePacket--;

	// Release mutex
#ifdef IS_WINDOWS
	ReleaseMutex( this->m_mutex );
#else // IS_WINDOWS
	pthread_mutex_unlock( &this->m_mutex );
#endif // !IS_WINDOWS

	// OK
	return TRUE;
}
