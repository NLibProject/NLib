#ifndef FILE_MANIPULATION_PROTECT
#define FILE_MANIPULATION_PROTECT

// Is EOF?
BOOL FileManipulation_IsEOF( FILE* );

// Get size
long FileManipulation_GetSize( FILE* );
long FileManipulation_GetSize2( const char* );

// Calculer CRC
int FileManipulation_GetCRC( FILE* );
int FileManipulation_GetCRC2( const char* );

// Le fichier existe?
BOOL FileManipulation_EstExiste( const char *lien );

// Le fichier est un repertoire?
BOOL FileManipulation_EstUnRepertoire( const char *lien );

// Look for
BOOL FileManipulation_LookForCharacter( FILE*,
	char );
BOOL FileManipulation_LookForString( FILE*,
	const char* );

// Read
__ALLOC char *FileManipulation_ReadBetween( FILE*,
	char c1,
	char c2 );

__ALLOC char *FileManipulation_ReadBetween2( FILE*,
	char c );

__ALLOC char *FileManipulation_ReadContent( FILE* );

// Ecrire
BOOL FileManipulation_Ecrire( const char *lien,
	const char *data,
	unsigned int taille,
	BOOL ecraserSiExiste );

// Obtenir extension
__ALLOC char *FileManipulation_GetExtension( const char* );

#endif // !FILE_MANIPULATION_PROTECT

