#include "Types.h"

// Selectionner socket ecriture
BOOL SocketSelection_EstEcritureDisponible( SOCKET socket )
{
	// Descripteur fichier
	fd_set fd;

	// Timeout counter
	struct timeval timeout = { 0, };

	// Zero file descriptor
	FD_ZERO( &fd );

	// Initialiser
	FD_SET( socket,
		&fd );

	// Select socket
	if( select( socket + 1,
		NULL,
		&fd,
		NULL,
		&timeout ) != SOCKET_ERROR )
		if( FD_ISSET( socket,
			&fd ) )
			return TRUE;

	// La selection a echoue
	return FALSE;
}

// Selectionner socket lecture
BOOL SocketSelection_EstLectureDisponible( SOCKET socket )
{
	// Descripteur fichier
	fd_set fd;

	// Timeout counter
	struct timeval timeout = { 0, };

	// Zero file descriptor
	FD_ZERO( &fd );

	// Initialiser
	FD_SET( socket,
		&fd );

	// Select socket
	if( select( socket + 1,
		&fd,
		NULL,
		NULL,
		&timeout ) != SOCKET_ERROR )
		if( FD_ISSET( socket,
			&fd ) )
			return TRUE;

	// La selection a echoue
	return FALSE;
}