#include "Types.h"

// -----------------------------------------
// FileManipulation: Use to manipulate files
// -----------------------------------------

// Get size
long FileManipulation_GetSize( FILE *f )
{
	// Initial position
	long initialPosition;

	// The size of the file
	__OUTPUT long size;

	// Get initial position
	initialPosition = ftell( f );

	// Go to the end of the file
	fseek( f,
		0,
		SEEK_END );

	// Get size
	size = ftell( f );

	// Restore position
	fseek( f,
		initialPosition,
		SEEK_SET );

	// Return
	return size;
}

long FileManipulation_GetSize2( const char *lien )
{
	// Fichier
	FILE *fichier;

	// Taille
	__OUTPUT long sortie;

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"r" ) ) )
	{
		// Notifier
		perror( "FileManipulation_GetSize2( )::Impossible d'ouvrir le fichier: " );

		// Quitter
		return 0;
	}

	// Lire la taille
	sortie = FileManipulation_GetSize( fichier );

	// Fermer le fichier
	fclose( fichier );

	// OK
	return sortie;
}

// Calculer CRC
int FileManipulation_GetCRC( FILE *fichier )
{
	// Sortie
	__OUTPUT int crc = 0;

	// Initial position
	long initialPosition;

	// Caractere lu
	char caractere;

	// Get initial position
	initialPosition = ftell( fichier );

	// Calculer
	while( !FileManipulation_IsEOF( fichier ) )
	{
		// Lire
		caractere = (char)fgetc( fichier );

		// Additionner
		crc += caractere;
	}

	// Restore position
	fseek( fichier,
		initialPosition,
		SEEK_SET );

	// Return
	return crc;
}

int FileManipulation_GetCRC2( const char *lien )
{
	// Fichier
	FILE *fichier;

	// Taille
	__OUTPUT int sortie;

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"rb" ) ) )
	{
		// Notifier
		perror( "FileManipulation_GetCRC2( )::Impossible d'ouvrir le fichier: " );

		// Quitter
		return 0;
	}

	// Lire la taille
	sortie = FileManipulation_GetCRC( fichier );

	// Fermer le fichier
	fclose( fichier );

	// OK
	return sortie;
}

// Is EOF?
BOOL FileManipulation_IsEOF( FILE *f )
{
	// Size
	long size;

	// Get size
	size = FileManipulation_GetSize( f );

	// Check
	return ( ftell( f ) >= size );
}

// Le fichier existe?
BOOL FileManipulation_EstExiste( const char *lien )
{
	// Fichier
	FILE *fichier;

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"r" ) ) )
		return FALSE;
	// Le fichier existe
	else
	{
		// Fermer le fichier
		fclose( fichier );

		// Quitter
		return TRUE;
	}
}

// Le fichier est un repertoire?
BOOL FileManipulation_EstUnRepertoire( const char *lien )
{
	// Details fichier
	struct stat s;

	// Dossier?
	if( stat( lien,
		&s) == 0 )
	{
		if( s.st_mode & S_IFDIR )
			return TRUE;
		else
			return FALSE;
	}

	// Erreur
	return FALSE;
}

// Look for
BOOL FileManipulation_LookForCharacter( FILE *f,
	char c )
{
	// Character read
	char cR;

	// Look for character
	do
	{
		// Read character
		cR = (char)fgetc( f );
	} while( !FileManipulation_IsEOF( f )
		&& c != cR );

	// OK
	return c == cR;
}

BOOL FileManipulation_LookForString( FILE *f,
	const char *s )
{
	// Cursor
	unsigned int cursor = 0;

	// Length of string
	unsigned int length;

	// Get the string length
	length = strlen( s );

	// Look for string
	while( cursor < length
		&& !FileManipulation_IsEOF( f ) )
	{
		// Can't find character
		if( !FileManipulation_LookForCharacter( f,
			s[ cursor ] ) )
			cursor = 0;
		// Character found
		else
			cursor++;
	}

	// Return result
	return ( cursor >= length );
}

// Read
__ALLOC char *FileManipulation_ReadBetween( FILE *f,
	char c1,
	char c2 )
{
	// Output
	__OUTPUT char *out = NULL;

	// Temp array
	char *temp;

	// Cursor where to copy
	unsigned int cursor = 0;

	// Character read
	char cR;

	// Look for first character
	if( !FileManipulation_LookForCharacter( f,
		c1 ) )
		return NULL;

	// Read
	do
	{
		// Read character
		cR = (char)fgetc( f );

		// If character different of the end character
		if( cR != c2 )
		{
			// Not allocated now
			if( out == NULL )
			{
				// Allocate memory
				if( !( out = calloc( 1 /* + 1 character */ + 1 /* \0 */,
					sizeof( char ) ) ) )
				{
					// Notify
					perror( "Allocation failed: " );

					// Quit
					return NULL;
				}

				// Where to copy
				cursor = 0;
			}
			else
			{
				// Save address
				temp = out;

				// Allocate new size
				if( !( out = calloc( strlen( temp ) + 2,
					sizeof( char ) ) ) )
				{
					// Notify
					perror( "Allocation failed: " );

					// Free
					FREE( temp );

					// Quit
					return NULL;
				}

				// Copy old data
				memcpy( out,
					temp,
					strlen( temp ) );

				// Where to copy
				cursor = strlen( temp );

				// Free
				FREE( temp );
			}

			// Copy new character
			out[ cursor ] = cR;
		}
	} while( !FileManipulation_IsEOF( f )
		&& cR != c2 );

	// OK
	return out;
}

__ALLOC char *FileManipulation_ReadBetween2( FILE *f,
	char c )
{
	return FileManipulation_ReadBetween( f,
		c,
		c );
}

// Read content
__ALLOC char *FileManipulation_ReadContent( FILE *f )
{
	// Size
	long fileSize;

	// Output
	__OUTPUT char *out;

	// Current position
	long currentPosition = 0;

	// Get file size
	fileSize = FileManipulation_GetSize( f );

	// Get current position
	currentPosition = ftell( f );

	// Go to the beginning of the file
	fseek( f,
		0,
		SEEK_SET );

	// Allocate memory
	if( !( out = calloc( fileSize + 1,
		sizeof( char ) ) ) )
	{
		// Notify
		perror( "Allocation failed: " );

		// Quit
		return NULL;
	}

	// Read
	fread( out,
		sizeof( char ),
		fileSize,
		f );

	// Restore position
	fseek( f,
		currentPosition,
		SEEK_SET );

	// OK
	return out;
}

// Ecrire
BOOL FileManipulation_Ecrire( const char *lien,
	const char *data,
	unsigned int taille,
	BOOL ecraserSiExiste )
{
	// Fichier
	FILE *fichier;

	// Verifier si le fichier existe au besoin
	if( !ecraserSiExiste )
		if( FileManipulation_EstExiste( lien ) )
		{
			// Notifier
			printf( "FileManipulation_Write( )::Le fichier existe deja.\n" );

			// Quitter
			return FALSE;
		}

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"wb+" ) ) )
	{
		// Notifier
		printf( "FileManipulation_Write( )::Impossible d'ouvrir [%s].\n",
			lien );

		// Quitter
		return FALSE;
	}

	// Ecrire
	fwrite( data,
		sizeof( char ),
		taille,
		fichier );

	// Fermer le fichier
	fclose( fichier );

	// OK
	return TRUE;
}

// Obtenir extension
__ALLOC char *FileManipulation_GetExtension( const char *nom )
{
	// Etape
	unsigned int etape = 0;

	// Curseur
	unsigned int curseur = 0;

	// Tableau temporaire
	char *temp;

	// Caractere
	char buffer;

	// Sortie
	__OUTPUT char *extension = NULL;

	// Parcourir
	while( curseur < strlen( nom ) )
	{
		// Lire le caractere
		buffer = nom[ curseur ];

		// Suivant l'etape
		switch( etape )
		{
			// On est au nom du fichier
			case 0:
				if( buffer == '.' )
				{
					if( curseur == 0 )
						return NULL;
					else
						etape++;
				}
				break;

			// On a trouve un point
			case 1:
				if( buffer != '.' )
				{
					// Allouer la memoire
					if( !( extension = calloc( 2,
						sizeof( char ) ) ) )
					{
						// Notifier
						perror( "FileManipulation_GetExtension( )::Impossible d'allouer la memoire: " );

						// Quitter
						return NULL;
					}

					// Copier le caractere
					extension[ 0 ] = buffer;

					// Etape suivante
					etape++;
				}
				break;

			// On est en train de lire l'extension
			case 2:
				// Enregistrer l'ancien etat
				temp = extension;

				// Allouer
				if( !( extension = calloc( strlen( temp ) + 2,
					sizeof( char ) ) ) )
				{
					// Notifier
					perror( "FileManipulation_GetExtension( )::Impossible d'allouer la memoire: " );

					// Liberer
					FREE( temp );

					// Quitter
					return NULL;
				}

				// Copier
				memcpy( extension,
					temp,
					strlen( temp ) );

				// Ajouter le caractere
				extension[ strlen( temp ) ] = buffer;

				// Liberer
				FREE( temp );
				break;

			// Etat inconnu
			default:
				// Liberer
				FREE( extension );

				// Quitter
				return NULL;
		}

		// Incrementer le curseur
		curseur++;
	}

	// OK
	return extension;
}

