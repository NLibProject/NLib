#ifndef THREADCLIENTSERVEUR_PROTECT
#define THREADCLIENTSERVEUR_PROTECT

// Thread emission
void ThreadClientServeur_ThreadEmission( ClientServeur *client );

// Thread reception
void ThreadClientServeur_ThreadReception( ClientServeur *client );

#endif // !THREADCLIENTSERVEUR_PROTECT

