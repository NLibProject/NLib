#ifndef BOOLEAN_PROTECT
#define BOOLEAN_PROTECT

#ifdef DEFINE_BOOL
// BOOL type
typedef unsigned int BOOL;

// Boolean constant
static const BOOL FALSE = 0;
static const BOOL TRUE = 1;
#endif // DEFINE_BOOL

#endif // !BOOLEAN_PROTECT

