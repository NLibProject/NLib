#ifndef ENTREECACHELOG_PROTECT
#define ENTREECACHELOG_PROTECT

// struct EntreeCacheLog
typedef struct EntreeCacheLog
{
	// Type
	TypeEntreeLog m_type;

	// IP
	char *m_ip;

	// Agent
	char *m_agent;

	// Identifiant client
	unsigned int m_identifiantClient;

	// Port
	unsigned int m_port;

	// Etat update
	char *m_fichier;
	double m_vitesse;
	unsigned long m_taille;
	unsigned long m_tailleEnvoyee;
} EntreeCacheLog;

// Construire une entree
__ALLOC EntreeCacheLog *EntreeCacheLog_Construire( const char *ip,
	unsigned int identifiantClient,
	TypeEntreeLog type,
	const char *agent,
	const char *fichier,
	double vitesse,
	unsigned long taille,
	unsigned long tailleEnvoyee,
	unsigned int port );

// Detruire une entree
void EntreeCacheLog_Detruire( EntreeCacheLog** );

// Obtenir le type
TypeEntreeLog EntreeCacheLog_ObtenirType( const EntreeCacheLog* );

// Obtenir l'ip
const char *EntreeCacheLog_ObtenirIP( const EntreeCacheLog* );

// Obtenir l'agent
const char *EntreeCacheLog_ObtenirAgent( const EntreeCacheLog* );

// Obtenir l'identifiant client
unsigned int EntreeCacheLog_ObtenirIdentifiantClient( const EntreeCacheLog* );

// Obtenir le nom du fichier
const char *EntreeCacheLog_ObtenirNomFichier( const EntreeCacheLog* );

// Obtenir la vitesse
double EntreeCacheLog_ObtenirVitesse( const EntreeCacheLog* );

// Obtenir la taille
unsigned long EntreeCacheLog_ObtenirTaille( const EntreeCacheLog* );

// Obtenir la taille envoyee
unsigned long EntreeCacheLog_ObtenirTailleEnvoyee( const EntreeCacheLog* );

// Obtenir le port
unsigned int EntreeCacheLog_ObtenirPort( const EntreeCacheLog* );

#endif // !ENTREECACHELOG_PROTECT

