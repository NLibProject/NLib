#include "Types.h"

// Thread de gestion des acceptation clients serveur
void ThreadAcceptationClientServeur_Thread( Serveur *serveur )
{
	// Boucle d'acceptation
	do
	{
		// Accepter client
		Serveur_AccepterClient( serveur );

		// Delais
#ifdef IS_WINDOWS
		Sleep( 1 );
#else // IS_WINDOWS
		usleep( 1 );
#endif // !IS_WINDOWS
	} while( Serveur_EstEnCours( serveur ) );
}

