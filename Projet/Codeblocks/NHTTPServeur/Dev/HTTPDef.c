#define HTTPDEF_INTERNE
#include "Types.h"

// ------------
// enum HTTPDef
// ------------

// Parser un mot clef de type de requete
HTTPTypeRequete HTTPDef_ParserTypeRequete( const char *type )
{
	// Sortie
	__OUTPUT HTTPTypeRequete out = (HTTPTypeRequete)0;

	// Chercher
	for( ; out < HTTP_REQUETES; out++ )
		if( !strcmp( HTTPDef_ObtenirTypeRequete( out ),
			type ) )
			return out;

	// Introuvable
	return out;
}

// Parser un mot clef de requete
HTTPRequestKeyWord HTTPDef_ParserRequestKeyword( const char *keyword )
{
	// Sortie
	__OUTPUT HTTPRequestKeyWord out = (HTTPRequestKeyWord)0;

	// Chercher
	for( ; out < HTTP_REQUEST_KEYWORDS; out++ )
		if( !strcmp( HTTPDef_ObtenirRequestKeyword( out ),
			keyword ) )
			return out;

	// Introuvable
	return out;
}

// Parser un mot clef de reponse
HTTPAnswerKeyword HTTPDef_ParserAnswerKeyword( const char *keyword )
{
	// Sortie
	__OUTPUT HTTPAnswerKeyword out = (HTTPAnswerKeyword)0;

	// Chercher
	for( ; out < NMOT_CLEF_REPONSE_HTTP_KEYWORDS; out++ )
		if( !strcmp( HTTPDef_ObtenirAnswerKeyword( out ),
			keyword ) )
			return out;

	// Introuvable
	return out;
}

// Parser un type d'extension (privee)
ExtensionFichier HTTPDef_ParserExtensionFichierInterne( const char *extension )
{
	// Sortie
	__OUTPUT ExtensionFichier out = (ExtensionFichier)0;

	// Chercher
	for( ; out < EXTENSIONS_FICHIER; out++ )
		if( !strcmp( extension,
			ExtensionsFichierTexte[ out ] ) )
			break;

	// Introuvable
	return out;
}

// Parser une extension pour fournir un type de flux
HTTPTypeFlux HTTPDef_ParserExtensionFichier( const char *extension )
{
	// Type extension
	ExtensionFichier typeExtension;

	// Obtenir le type
	typeExtension = HTTPDef_ParserExtensionFichierInterne( extension );

	// Analyser le type
	switch( typeExtension )
	{
		// Page web
		case EXTENSION_PAGE_WEB_HTML:
		case EXTENSION_PAGE_WEB_HTM:
		case EXTENSION_PAGE_WEB_PHP:
			return HTTP_TYPE_FLUX_HTML;

		case EXTENSION_PAGE_WEB_CSS:
			return HTTP_TYPE_FLUX_CSS;

		// Texte
		case EXTENSION_TXT:
		case EXTENSION_LOG:
		case EXTENSION_C:
		case EXTENSION_H:
		case EXTENSION_CPP:
		case EXTENSION_HPP:
		case EXTENSION_JAVA:
		case EXTENSION_CS:
			return HTTP_TYPE_FLUX_TEXTE;

		// PDF
		case EXTENSION_PDF:
			return HTTP_TYPE_FLUX_PDF;

		// Images
		case EXTENSION_PNG:
			return HTTP_TYPE_FLUX_IMAGE_PNG;
		case EXTENSION_JPG:
		case EXTENSION_JPEG:
			return HTTP_TYPE_FLUX_IMAGE_JPG;
		case EXTENSION_GIF:
			return HTTP_TYPE_FLUX_IMAGE_GIF;

		default:
			return HTTP_TYPE_FLUX_TELECHARGEMENT;
	}
}

// Obtenir un mot clef de requete
const char *HTTPDef_ObtenirRequestKeyword( HTTPRequestKeyWord keyword )
{
	return HTTPRequestKeywordText[ keyword ];
}

// Obtenir un mot clef de reponse
const char *HTTPDef_ObtenirAnswerKeyword( HTTPAnswerKeyword keyword )
{
	return HTTPAnswerKeywordText[ keyword ];
}

// Obtenir un type de requete
const char *HTTPDef_ObtenirTypeRequete( HTTPTypeRequete type )
{
	return HTTPTypeRequeteText[ type ];
}

// Obtenir un type de flux
const char *HTTPDef_ObtenirTypeFlux( HTTPTypeFlux type )
{
	return HTTPTypeFluxTexte[ type ];
}

// Obtenir extension
const char *HTTPDef_ObtenirExtensionTypeFichier( ExtensionFichier extension )
{
	return ExtensionsFichierTexte[ extension ];
}

