#define HTTPSPECIALTREATMENT_INTERNE
#include "Types.h"

/* Genere les packets 404 par defaut */
__ALLOC char *HTTPSpecialTreatment_Generate404Msg( const char *fichierDemande )
{
	// Buffer
	char buffer[ TAILLE_TEXTE_404 + MAX_PATH + 1 ];

	// Sortie
	__OUTPUT char *out;

	// Bufferiser
	snprintf( buffer,
		TAILLE_TEXTE_404 + MAX_PATH,
		Erreur404Texte,
		fichierDemande );

	// Copier
	if( !( out = calloc( strlen( buffer ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		perror( "HTTPSpecialTreatment_Generate404Msg( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out,
		buffer,
		strlen( buffer ) );

	// OK
	return out;
}

__ALLOC Packet *HTTPSpecialTreatment_Generate404Header( const char *fichierDemande )
{
	// Sortie
	__OUTPUT Packet *out;

	// Texte
	char *texte;

	// Allouer le packet
	if( !( out = Packet_Construire( ) ) )
	{
		// Notifier
		perror( "HTTPSpecialTreatment_Generate404Header( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Recuperer le texte
	if( !( texte = HTTPSpecialTreatment_Generate404Msg( fichierDemande ) ) )
	{
		// Notifier
		printf( "HTTPSpecialTreatment_Generate404Header( )::Impossible d'obtenir le texte.\n" );

		// Liberer le packet
		Packet_Detruire( &out );

		// Quitter
		return NULL;
	}

	// Ajouter l'header
	HTTPProcess_ComposerInformationsHeaderPacketReponseGET( out,
		HTTP_TYPE_FLUX_HTML,
		strlen( texte ),
		TRUE );

	// Liberer
	FREE( texte );

	// OK
	return out;
}

__ALLOC Packet *HTTPSpecialTreatment_Generate404Content( const char *fichierDemande )
{
	// Sortie
	__OUTPUT Packet *out;

	// Texte
	char *texte;

	// Allouer le packet
	if( !( out = Packet_Construire( ) ) )
	{
		// Notifier
		perror( "HTTPSpecialTreatment_Generate404Content( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Recuperer le texte
	if( !( texte = HTTPSpecialTreatment_Generate404Msg( fichierDemande ) ) )
	{
		// Notifier
		printf( "HTTPSpecialTreatment_Generate404Content( )::Impossible d'obtenir le texte.\n" );

		// Liberer le packet
		Packet_Detruire( &out );

		// Quitter
		return NULL;
	}

	// Ajouter le contenu
	Packet_AjouterData( out,
		texte,
		strlen( texte ) );

	// Liberer le texte
	FREE( texte );

	// OK
	return out;
}

/* Genere les packets description */
__ALLOC char *HTTPSpecialTreatment_GenerateDescriptionMsg( void )
{
	// Buffer
	char buffer[ TAILLE_TEXTE_DESCRIPTION + 1 ];

	// Sortie
	__OUTPUT char *out;

	// Bufferiser
	sprintf( buffer,
		DescriptionTexte );

	// Copier
	if( !( out = calloc( strlen( buffer ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		perror( "HTTPSpecialTreatment_GenerateDescriptionMsg( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out,
		buffer,
		strlen( buffer ) );

	// OK
	return out;

}
// Page de description
__ALLOC Packet *HTTPSpecialTreatment_GenerateDescriptionHeader( void )
{
	// Sortie
	__OUTPUT Packet *out;

	// Texte
	char *texte;

	// Allouer le packet
	if( !( out = Packet_Construire( ) ) )
	{
		// Notifier
		perror( "HTTPSpecialTreatment_GenerateDescriptionHeader( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Recuperer le texte
	if( !( texte = HTTPSpecialTreatment_GenerateDescriptionMsg( ) ) )
	{
		// Notifier
		printf( "HTTPSpecialTreatment_GenerateDescriptionHeader( )::Impossible d'obtenir le texte.\n" );

		// Liberer le packet
		Packet_Detruire( &out );

		// Quitter
		return NULL;
	}

	// Ajouter l'header
	HTTPProcess_ComposerInformationsHeaderPacketReponseGET( out,
		HTTP_TYPE_FLUX_HTML,
		strlen( texte ),
		TRUE );

	// Liberer
	FREE( texte );

	// OK
	return out;
}

__ALLOC Packet *HTTPSpecialTreatment_GenerateDescriptionContent( void )
{
	// Sortie
	__OUTPUT Packet *out;

	// Texte
	char *texte;

	// Allouer le packet
	if( !( out = Packet_Construire( ) ) )
	{
		// Notifier
		perror( "HTTPSpecialTreatment_GenerateDescriptionContent( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Recuperer le texte
	if( !( texte = HTTPSpecialTreatment_GenerateDescriptionMsg( ) ) )
	{
		// Notifier
		printf( "HTTPSpecialTreatment_GenerateDescriptionContent( )::Impossible d'obtenir le texte.\n" );

		// Liberer le packet
		Packet_Detruire( &out );

		// Quitter
		return NULL;
	}

	// Ajouter le contenu
	Packet_AjouterData( out,
		texte,
		strlen( texte ) );

	// Liberer le texte
	FREE( texte );

	// OK
	return out;
}

/**
 * Genere les packets relatifs a une tentative de hack
 */
__ALLOC Packet *HTTPSpecialTreatment_GenerateHackHeader( void )
{
	// Sortie
	__OUTPUT Packet *out;

	// Allouer le packet
	if( !( out = Packet_Construire( ) ) )
	{
		// Notifier
		perror( "HTTPSpecialTreatment_Generate404Header( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Ajouter l'header
	HTTPProcess_ComposerInformationsHeaderPacketReponseGET( out,
		HTTP_TYPE_FLUX_HTML,
		strlen( HackTexte ),
		FALSE );

	// OK
	return out;
}

__ALLOC Packet *HTTPSpecialTreatment_GenerateHackContent( void )
{
	// Sortie
	__OUTPUT Packet *out;

	// Allouer le packet
	if( !( out = Packet_Construire( ) ) )
	{
		// Notifier
		perror( "HTTPSpecialTreatment_GenerateDescriptionContent( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Ajouter le contenu
	Packet_AjouterData( out,
		HackTexte,
		strlen( HackTexte ) );

	// OK
	return out;
}

