#include <time.h>
#include "Types.h"

int main( int argc,
	char *argv[ ] )
{
	// Serveur
	Serveur *serveur = NULL;

	// Referencer arguments
	REFERENCER( argc );
	REFERENCER( argv );

	// Configurer la console
#ifdef IS_WINDOWS
	system( "color 0F" );
#endif // IS_WINDOWS

	// Initialiser rand
	srand( (unsigned int)time( NULL ) );

	// Initialiser le reseau
	if( !Reseau_Initialiser( ) )
	{
		// Notifier
		printf( "main( )::Impossible d'initialiser le reseau.\n" );

		// Quitter
		return EXIT_FAILURE;
	}

	// Creer le serveur
	if( !( serveur = Serveur_Construire( PORT_SERVEUR ) ) )
	{
		// Notifier
		printf( "main( )::Impossible de creer le serveur.\n" );

		// Fermer
		Reseau_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Afficher l'aide
	GestionCommandeServeur_AfficherAide( );

	// Boucle commandes
	do
	{
		// Gerer les commandes
		GestionCommandeServeur_Gerer( serveur );

		// Delais
#ifdef IS_WINDOWS
		Sleep( 1 );
#else // IS_WINDOWS
		usleep( 1 );
#endif // !IS_WINDOWS
	} while( Serveur_EstEnCours( serveur ) );

	// Detruire le serveur
	Serveur_Detruire( &serveur );

	// Detruire le reseau
	Reseau_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

