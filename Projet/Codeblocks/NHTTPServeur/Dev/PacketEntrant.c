#include "Types.h"

/* Packet entrant */
// Lecture packet
void PacketEntrant_Lecture( const Packet *packet,
	ClientServeur *client )
{
	// Packet de reponse
	Packet **reponse;

	// Traiter l'header HTTP
	if( !( reponse = HTTPProcess_TraiterPacket( packet,
		client ) ) )
	{
		// Notifier
		printf( "PacketEntrant_Lecture( )::Impossible de traiter le packet entrant.\n" );

		// Tuer le client
		ClientServeur_Tuer( client );

		// Quitter
		return;
	}

	// Envoyer la reponse
	ClientServeur_AjouterPacket( client,
		reponse[ HTTP_PROCESS_PACKET_HEADER ] );
	if( reponse[ HTTP_PROCESS_PACKET_CONTENT ] != NULL )
		ClientServeur_AjouterPacket( client,
			reponse[ HTTP_PROCESS_PACKET_CONTENT ] );


	// Liberer
	FREE( reponse );

	// Attendre que tous les packets soient partis
	while( ClientServeur_EstPacketsDansCache( client ) )
#ifdef IS_WINDOWS
		Sleep( 1 );
#else // IS_WINDOWS
		usleep( 1 );
#endif // !IS_WINDOWS

	// Tuer le client
	ClientServeur_Tuer( client );
}

