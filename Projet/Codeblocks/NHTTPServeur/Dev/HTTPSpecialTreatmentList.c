#include "Types.h"

// -----------------------------
// enum HTTPSpecialTreatmentList
// -----------------------------

/* Determiner le type de traitement selon les parametres actuels */
HTTPSpecialTreatmentList HTTPSpecialTreatmentList_GetTreatment( const char *fichier )
{
	// Buffer
	char *buffer;

	// Chemin
	char *chemin,
		cheminFichier[ MAX_PATH ];

	// Si il s'agit du fichier executable du serveur
	if( !strcmp( fichier,
		"NServeur.exe" )
		|| !strcmp( fichier,
			"NServeurHTTP" ) )
		// On interdit le telechargement
		return HTTP_SPECIAL_TREATMENT_404_PAGE;

	// Obtenir les chemins
	chemin = getcwd( NULL,
		0 );
	realpath( fichier,
		cheminFichier );

	// Anti cross directory
	if( strstr( cheminFichier,
		chemin ) == NULL )
	{
		// Liberer
#ifndef IS_WINDOWS
		FREE( chemin );
#endif // IS_WINDOWS

		// Quitter
		return HTTP_SPECIAL_TREATMENT_CROSS_DIRECTORY;
	}

	// Liberer
#ifndef IS_WINDOWS
	FREE( chemin );
#endif // IS_WINDOWS

	// Verifier l'existence
	if( FileManipulation_EstExiste( fichier )
		&& !FileManipulation_EstUnRepertoire( fichier ) )
		return HTTP_SPECIAL_TREATMENT_NO_SPECIAL;

	// Verifier si dans le dossier %fichier%, le fichier index.html existe
		// Allouer la memoire
			if( !( buffer = calloc( strlen( fichier ) + 1 /* / */ + strlen( FICHIER_PAR_DEFAUT_SERVEUR ) + 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				perror( "HTTPSpecialTreatmentList_GetTreatment( )::Impossible d'allouer la memoire: " );

				// Quitter
				return HTTP_SPECIAL_TREATMENT_404_PAGE;
			}
		// Copier
			memcpy( buffer,
				fichier,
				strlen( fichier ) );
			memcpy( buffer + strlen( fichier ),
				"/",
				sizeof( char ) );
			memcpy( buffer + strlen( fichier ) + 1,
				FICHIER_PAR_DEFAUT_SERVEUR,
				strlen( FICHIER_PAR_DEFAUT_SERVEUR ) );
		// Verifier
			if( FileManipulation_EstExiste( buffer ) )
			{
				// Liberer
				FREE( buffer );

				// OK
				return HTTP_SPECIAL_TREATMENT_GET_SUBDIR;
			}
		// Liberer
			FREE( buffer );

	// Verifier si en ajoutant .html le fichier existe
		// Allouer la memoire
			if( !( buffer = calloc( strlen( fichier ) + 1 /* . */ + strlen( HTTPDef_ObtenirExtensionTypeFichier( EXTENSION_PAGE_WEB_HTML ) ) + 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				perror( "HTTPSpecialTreatmentList_GetTreatment( )::Impossible d'allouer la memoire: " );

				// Quitter
				return HTTP_SPECIAL_TREATMENT_404_PAGE;
			}
		// Copier
			memcpy( buffer,
				fichier,
				strlen( fichier ) );
			memcpy( buffer + strlen( fichier ),
				".",
				sizeof( char ) );
			memcpy( buffer + strlen( fichier ) + 1,
				HTTPDef_ObtenirExtensionTypeFichier( EXTENSION_PAGE_WEB_HTML ),
				strlen( HTTPDef_ObtenirExtensionTypeFichier( EXTENSION_PAGE_WEB_HTML ) ) );
		// Verifier si le fichier existe
			if( FileManipulation_EstExiste( buffer ) )
			{
				// Liberer
				FREE( buffer );

				// Fichier.html existe
				return HTTP_SPECIAL_TREATMENT_ADD_HTML;
			}
		// Liberer
			FREE( buffer );

	// Si il s'agit du lien NServeur (affiche en cas de 404)
	if( !strcmp( fichier,
		"NServeur" ) )
		return HTTP_SPECIAL_TREATMENT_GET_DESCRIPTION_PAGE;

	// Si il s'agit de FAVORITE_ICON_DEFAULT_NAME
	if( !strcmp( fichier,
		FAVORITE_ICON_DEFAULT_NAME ) )
		return HTTP_SPECIAL_TREATMENT_FAVICON;
	else
		return HTTP_SPECIAL_TREATMENT_404_PAGE;
}

