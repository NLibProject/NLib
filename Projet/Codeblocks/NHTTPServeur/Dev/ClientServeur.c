#include "Types.h"

/* struct ClientServeur */
// Obtenir adresse ip
__ALLOC char *ClientServeur_ObtenirAdresseIPInterne( const ClientServeur *this )
{
	// Output
	__OUTPUT char *out;

	// Allocate memory
	if( !( out = calloc( INET_ADDRSTRLEN + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		perror( "ClientServeur_ObtenirAdresseIPInterne( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Obtenir adresse
	inet_ntop( AF_INET,
		(void*)&this->m_contexteAdressage.sin_addr,
		out,
		INET_ADDRSTRLEN * sizeof( char ) );

	// OK
	return out;
}

// Construire le client
__ALLOC ClientServeur *ClientServeur_Construire( SOCKET socketServeur,
	CacheLog *cacheLog,
	const BOOL *estConnexionAutorisee )
{
	// Sortie
	__OUTPUT ClientServeur *out;

	// Socket recv timeout
#ifdef IS_WINDOWS
	DWORD timeout = TIMEOUT_RECV;
#else // IS_WINDOWS
	struct timeval timeout;
#endif // !IS_WINDOWS

	// Taille du contexte d'adressage
	int tailleContexteAdressage = sizeof( SOCKADDR_IN );

	// Allouer le client
	if( !( out = calloc( 1,
		sizeof( ClientServeur ) ) ) )
	{
		// Notifier
		perror( "ClientServeur_Construire( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// Accepter la requete
	if( ( out->m_socket = accept( socketServeur,
		(struct sockaddr*)&out->m_contexteAdressage,
		(socklen_t*)&tailleContexteAdressage ) ) == INVALID_SOCKET )
	{
		// Notifier
		perror( "ClientServeur_Construire( )::Impossible d'accepter un client: " );

		// Liberer
		FREE( out );

		// Quitter
		return NULL;
	}

	// Verifier si la connexion est autorisee
	if( !(*estConnexionAutorisee) )
	{
		// Notifier
		printf( "[SERVEUR]\tTentative de connexion bloquee (Serveur ferme)\n" );

		// Fermer la socket
		closesocket( out->m_socket );

		// Liberer
		FREE( out );

		// Quitter
		return NULL;
	}

#ifndef IS_WINDOWS
	timeout.tv_sec = TIMEOUT_RECV / 1000;
	timeout.tv_usec = ( TIMEOUT_RECV % 1000 ) * 1000;
#endif // !IS_WINDOWS

	// Timeout
	setsockopt( out->m_socket,
		SOL_SOCKET,
		SO_RCVTIMEO,
		(char*)&timeout,
		sizeof( timeout ) );

	// Obtenir ip
	if( !( out->m_ip = ClientServeur_ObtenirAdresseIPInterne( out ) ) )
	{
		// Notifier
		printf( "ClientServeur_Construire( )::Impossible d'obtenir l'IP.\n" );

		// Fermer la socket
		closesocket( out->m_socket );

		// Liberer
		FREE( out );

		// Quitter
		return NULL;
	}

	// Creer le cache packet
	if( !( out->m_cachePacket = CachePacket_Construire( ) ) )
	{
		// Notifier
		printf( "ClientServeur_Construire( )::Impossible de construire le cache packets.\n" );

		// Fermer la socket
		closesocket( out->m_socket );

		// Liberer
		FREE( out->m_ip );
		FREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_callbackCacheLog = cacheLog;

	// Zero
	out->m_estMort = FALSE;
	out->m_estEnCours = TRUE;

	// Creer les threads
		// Emission
#ifdef IS_WINDOWS
			out->m_threadEmission = CreateThread( NULL,
				0,
				(LPTHREAD_START_ROUTINE)ThreadClientServeur_ThreadEmission,
				out,
				0,
				NULL );
#else // IS_WINDOWS
			pthread_create( &out->m_threadEmission,
				NULL,
				(void *( * )( void* ))ThreadClientServeur_ThreadEmission,
				out );
#endif // !IS_WINDOWS
		// Reception
#ifdef IS_WINDOWS
			out->m_threadReception = CreateThread( NULL,
				0,
				(LPTHREAD_START_ROUTINE)ThreadClientServeur_ThreadReception,
				out,
				0,
				NULL );
#else // IS_WINDOWS
			pthread_create( &out->m_threadReception,
				NULL,
				(void *( * )( void* ))ThreadClientServeur_ThreadReception,
				out );
#endif // !IS_WINDOWS

	// OK
	return out;
}

// Detruire le client
void ClientServeur_Detruire( ClientServeur **this )
{
	// Arreter le client
	(*this)->m_estEnCours = FALSE;

#ifdef IS_WINDOWS
	// Attendre que les threads se terminent
	while( WaitForSingleObject( (*this)->m_threadEmission,
			INFINITE ) != WAIT_OBJECT_0
		|| WaitForSingleObject( (*this)->m_threadReception,
			INFINITE ) != WAIT_OBJECT_0 )
		Sleep( 1 );

	// Fermer les threads
	CloseHandle( (*this)->m_threadEmission );
	CloseHandle( (*this)->m_threadReception );
#else // IS_WINDOWS
	// Attendre la fin du thread
	pthread_join( (*this)->m_threadEmission,
		NULL );
	pthread_join( (*this)->m_threadReception,
		NULL );
#endif // !IS_WINDOWS

	// Fermer la socket
	closesocket( (*this)->m_socket );

	// Liberer socket
	FREE( (*this)->m_ip );

	// Detruire le cache packet
	CachePacket_Detruire( &(*this)->m_cachePacket );

	// Liberer la memoire
	FREE( *this );
}

// Est mort?
BOOL ClientServeur_EstMort( const ClientServeur *this )
{
	return this->m_estMort;
}

// Est en cours?
BOOL ClientServeur_EstEnCours( const ClientServeur *this )
{
	return this->m_estEnCours;
}

// Est packet(s) dans le cache?
BOOL ClientServeur_EstPacketsDansCache( const ClientServeur *this )
{
	return CachePacket_EstPacketsDansCache( this->m_cachePacket );
}

// Tuer un client
void ClientServeur_Tuer( ClientServeur *this )
{
	this->m_estMort = TRUE;
}

// Definir identifiant
void ClientServeur_DefinirIdentifiantUnique( ClientServeur *this,
	unsigned int identifiant )
{
	this->m_identifiantUnique = identifiant;
}

// Obtenir l'identifiant
unsigned int ClientServeur_ObtenirIdentifiant( const ClientServeur *this )
{
	return this->m_identifiantUnique;
}

// Obtenir SOCKET
SOCKET ClientServeur_ObtenirSocket( const ClientServeur *this )
{
	return this->m_socket;
}

// Obtenir cache log
CacheLog *ClientServeur_ObtenirCacheLog( const ClientServeur *this )
{
	return this->m_callbackCacheLog;
}

// Obtenir cache packet
CachePacket *ClientServeur_ObtenirCachePacket( const ClientServeur *this )
{
	return this->m_cachePacket;
}

// Obtenir IP
const char *ClientServeur_ObtenirAdresseIP( const ClientServeur *this )
{
	return this->m_ip;
}

// Ajouter un packet
BOOL ClientServeur_AjouterPacket( ClientServeur *this,
	__WILLBEOWNED const Packet *packet )
{
	// Ajouter le packet au cache
	return CachePacket_AjouterPacket( this->m_cachePacket,
		packet );
}

