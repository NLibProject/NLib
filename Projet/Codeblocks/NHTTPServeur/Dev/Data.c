#include "Types.h"

// Augmenter taille
BOOL Data_AugmenterTaille( char **mem,
	unsigned int tailleMem,
	unsigned int delta )
{
	// Ancienne memoire
	char *ancienneMemoire = *mem;

	
	// Allouer nouvelle memoire
	if( !( *mem = calloc( tailleMem + delta,
		sizeof( char ) ) ) )
	{
		// Notifier
		perror( "Data_AugmenterTaille( )::Impossible d'allouer la memoire: " );

		// Restaurer
		*mem = ancienneMemoire;

		// Quitter
		return FALSE;
	}

	// Si il y avait des donnees avant
	if( ancienneMemoire != NULL )
	{
		// Copier ancienne donnees
		memcpy( *mem,
			ancienneMemoire,
			tailleMem );

		// Liberer ancienne donnees
		FREE( ancienneMemoire );
	}

	// OK
	return TRUE;
}

// Ajouter des donnees
BOOL Data_AjouterData( char **mem,
	unsigned int tailleMem,
	const char *memAjout,
	unsigned int tailleMemAjout )
{
	// Augmenter la taille disponible
	if( !Data_AugmenterTaille( mem,
		tailleMem,
		tailleMemAjout ) )
	{
		// Notifier
		printf( "Data_AjouterData( )::Impossible d'augmenter la taille.\n" );

		// Quitter
		return FALSE;
	}

	// Copier les nouvelles donnees
	memcpy( *mem + tailleMem,
		memAjout,
		tailleMemAjout );

	// OK
	return TRUE;
}

