#ifndef LOG_PROTECT
#define LOG_PROTECT

// Nom fichier log
#define NOM_FICHIER_LOG		"NHTTPServeur.log"

// Obtenir temps
__ALLOC char *Log_ObtenirTemps( unsigned int version );

// Loguer le demarrage
void Log_LoguerDemarrage( unsigned int port );

// Loguer une connexion
void Log_LoguerConnexion( const char *ip,
	unsigned int identifiant );

// Loguer une deconnexion
void Log_LoguerDeconnexion( const char *ip,
	unsigned int identifiant );

// Loguer agent client
void Log_LoguerAgentClient( const char *ip,
	unsigned int identifiant,
	const char *agent );

// Loguer une demande de fichier
void Log_LoguerDemandeFichier( const char *ip,
	unsigned int identifiant,
	const char *fichier );

// Loguer une fin d'envoi
void Log_LoguerFinEnvoi( const char *ip,
	unsigned int identifiant,
	const char *fichier );

// Loguer etat envoi
void Log_LoguerEnvoi( const char *ip,
	unsigned int identifiant,
	const char *lien,
	unsigned long tailleEnvoyee,
	unsigned long taille,
	double debit );

// Loguer cross directory
void Log_LoguerCrossDirectory( const char *ip,
	unsigned int identifiant );

/* Obtenir le nombre de ms depuis le lancement */
unsigned int Log_ObtenirTick( void );

#endif // !LOG_PROTECT

