#ifndef COMMANDESERVEUR_PROTECT
#define COMMANDESERVEUR_PROTECT

// enum CommandeServeur
typedef enum
{
	COMMANDE_SERVEUR_AIDE,
	COMMANDE_SERVEUR_AIDE2,

	COMMANDE_SERVEUR_UPDATE_LIST,

	COMMANDE_SERVEUR_GET_IP,

	COMMANDE_SERVEUR_FERMER_SERVEUR,
	COMMANDE_SERVEUR_OUVRIR_SERVEUR,

	COMMANDE_SERVEUR_A_PROPOS,

	COMMANDE_SERVEUR_QUITTER,
	COMMANDE_SERVEUR_QUITTER2,

	COMMANDES_SERVEUR
} CommandeServeur;

// Parser une commande
CommandeServeur CommandeServeur_ParserCommande( const char *commande );

// Obtenir une commande
const char *CommandeServeur_ObtenirCommande( CommandeServeur );

// Obtenir une description de commande
const char *CommandeServeur_ObtenirDescriptionCommande( CommandeServeur );

#ifdef COMMANDESERVEUR_INTERNE
const char CommandeServeurTexte[ COMMANDES_SERVEUR ][ 32 ] =
{
	"Aide",
	"Help",

	"List",

	"GetIP",

	"Close",
	"Open",

	"About",

	"Quit",
	"Exit"
};

const char DescriptionCommandeServeurTexte[ COMMANDES_SERVEUR ][ 96 ] =
{
	"Affiche l'aide",
	"Affiche l'aide",

	"Met a jour la liste des fichiers dans index.html (ecrase index.html)",

	"Affiche l'IP de la machine en local",

	"Ferme le serveur aux clients",
	"Ouvre le serveur aux clients",

	"Affiche les informations concernant le serveur",

	"Quitte le serveur",
	"Quitte le serveur"
};
#endif // COMMANDESERVEUR_INTERNE

#endif // !COMMANDESERVEUR_PROTECT

