#ifndef STRING_MANIPULATION_PROTECT
#define STRING_MANIPULATION_PROTECT

// Error
#define STRING_MANIPULATION_ERROR		(unsigned int)0xFFFFFFFF

// Is EOF?
BOOL StringManipulation_IsEOF( const char *src,
	unsigned int cursor );

// Look for
BOOL StringManipulation_LookForCharacter( const char *src,
	char,
	unsigned int *currentPosition );

BOOL StringManipulation_LookForString( const char *src,
	const char*,
	unsigned int *currentPosition );

// Read
__ALLOC char *StringManipulation_ReadBetween( const char *src,
	char c1,
	char c2,
	unsigned int *currentPosition,
	BOOL isConservePosition );

__ALLOC char *StringManipulation_ReadBetween2( const char *src,
	char c,
	unsigned int *currentPosition,
	BOOL isConservePosition );

__ALLOC char *StringManipulation_ReadUntil( const char *src,
	char c,
	unsigned int *currentPosition,
	BOOL isConservePosition );

// Get next character
char StringManipulation_GetNextCharacter( const char *src,
	unsigned int *currentPosition,
	BOOL isConservePosition );


#endif // !STRING_MANIPULATION_PROTECT

