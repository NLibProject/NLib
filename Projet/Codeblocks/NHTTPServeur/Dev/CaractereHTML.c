#define CARACTEREHTML_INTERNE
#include "Types.h"

// ------------------
// enum CaractereHTML
// ------------------

// Est caractere remplašable?
BOOL CaractereHTML_EstCaractereDoitRemplacer( char caractere )
{
	return CaractereHTML_ObtenirEquivalence( caractere ) != NULL;
}

// Obtenir equivalence HTML caractere
const char *CaractereHTML_ObtenirEquivalence( char caractere )
{
	// Iterateur
	CaractereHTML i;

	// Chercher
	for( i = (CaractereHTML)0; i < CARACTERES_HTML; i++ )
		if( CaractereHTMLCaractere[ i ] == caractere )
			return CaractereHTMLCorrespondancesTexte[ i ];

	// Introuvable
	return NULL;
}

