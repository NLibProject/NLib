#ifndef PACKETIO_PROTECT
#define PACKETIO_PROTECT

// Taille buffer packet (octets)
#define BUFFER_PACKET				32000

// Timeout recv (ms)
#define TIMEOUT_RECV				1000

// Bloc fichier envoye (octets)
#define BLOC_FICHIER				256000

// Frequence update vitesse transfert (ms)
#define FREQUENCE_UPDATE_VITESSE	150

// Frequence affichage console (ms)
#define FREQUENCE_AFFICHAGE_CONSOLE	1000

// Recevoir un packet
BOOL PacketIO_RecevoirPacket( __OUTPUT Packet *packet,
	SOCKET socket );

// Envoyer un packet
BOOL PacketIO_EnvoyerPacket( const Packet *packet,
	const ClientServeur *client );

#endif // !PACKETIO_PROTECT

