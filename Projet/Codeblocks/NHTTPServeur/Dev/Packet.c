#include "Types.h"

/* struct Packet */

// Construire le packet
__ALLOC Packet *Packet_Construire( void )
{
	// Output
	__OUTPUT Packet *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( Packet ) ) ) )
	{
		// Notifier
		perror( "Packet_Construire( )::Impossible d'allouer la memoire: " );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

__ALLOC Packet *Packet_Construire2( const Packet *src )
{
	// Output
	__OUTPUT Packet *out;

	// Construire le packet
	if( !( out = Packet_Construire( ) ) )
	{
		// Notifier
		printf( "Packet_Construire2( )::Impossible de construire le packet.\n" );

		// Quitter
		return NULL;
	}

	// Copier la taille
	out->m_taille = src->m_taille;

	// Allouer la memoire
	if( !( out->m_data = calloc( out->m_taille,
		sizeof( char ) ) ) )
	{
		// Notifier
		perror( "Packet_Construire2( )::Impossible d'allouer la memoire: " );

		// Liberer la memoire
		FREE( out );

		// Quitter
		return NULL;
	}

	// Copier les donnees
	memcpy( out->m_data,
		src->m_data,
		src->m_taille * sizeof( char ) );

	// OK
	return out;
}

__ALLOC Packet *Packet_Construire3( const char *lienFichier )
{
	// Sortie
	__OUTPUT Packet *out;

	// Construire le packet
	if( !( out = Packet_Construire( ) ) )
	{
		// Notifier
		printf( "Packet_Construire3( )::Impossible de construire le packet.\n" );

		// Quitter
		return NULL;
	}

	// Determiner la taille
	out->m_taille = strlen( lienFichier ) + 1;

	// Allouer la memoire
	if( !( out->m_data = calloc( out->m_taille,
		sizeof( char ) ) ) )
	{
		// Notifier
		perror( "Packet_Construire3( )::Impossible d'allouer la memoire: " );

		// Liberer
		FREE( out );

		// Quitter
		return NULL;
	}

	// Copier le lien
	memcpy( out->m_data,
		lienFichier,
		out->m_taille - 1 );

	// Il s'agit d'un fichier
	out->m_estFichier = TRUE;

	// OK
	return out;
}

// Detruire le packet
void Packet_Detruire( Packet **this )
{
	// Liberer les donnees
	FREE( (*this)->m_data );

	// Liberer la memoire
	FREE( (*this) );
}

// Obtenir la taille
unsigned int Packet_ObtenirTaille( const Packet *this )
{
	return this->m_taille;
}

// Obtenir les donnees
const char *Packet_ObtenirData( const Packet *this )
{
	return this->m_data;
}

// Est un fichier?
BOOL Packet_EstFichier( const Packet *this )
{
	return this->m_estFichier;
}

// Ajout donnees
BOOL Packet_AjouterData( Packet *this,
	const char *data,
	unsigned int taille )
{
	// Ajouter donnees
	if( !Data_AjouterData( &this->m_data,
		this->m_taille,
		data,
		taille ) )
	{
		// Notifier
		printf( "Packet_AjouterData( )::Impossible d'ajouter les donnees.\n" );

		// Quitter
		return FALSE;
	}

	// Augmenter la taille
	this->m_taille += taille;

	// OK
	return TRUE;
}
