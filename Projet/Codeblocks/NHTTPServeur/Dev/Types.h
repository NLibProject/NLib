#ifndef TYPES_PROTECT
#define TYPES_PROTECT

// Base
#define _CRT_SECURE_NO_WARNINGS
#define _FILE_OFFSET_BITS 64 // (ftello/fseeko) !WINDOWS | (_fseeki64 _ftelli64) WINDOWS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Definition preprocesseur
#include "Preprocessor.h"

// Reseau
#ifdef IS_WINDOWS
// Suppression warnings
#pragma warning( disable:4706 ) // assignation au sein d'une expression conditionnelle

// Repertoire
#include <direct.h>

// Winsock
#include <WinSock2.h>
#include <WS2tcpip.h>
#define FIN_ADRESSE		S_un.S_addr

#undef DEFINE_BOOL
#else // IS_WINDOWS
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <dirent.h>
#include <memory.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>

#include <pthread.h>
#define DEFINE_BOOL

// Types
typedef unsigned long int DWORD;

#define MAX_PATH	PATH_MAX

// SOCKET
typedef unsigned int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
#define INVALID_SOCKET		(SOCKET)-1
#define SOCKET_ERROR		(SOCKET)-1
#define closesocket( s ) \
	close( s )
#define FIN_ADRESSE		s_addr
#endif // !IS_WINDOWS

// type BOOLEAN

#include "Boolean.h"

// Caractere
#include "Caractere.h"

// Donnees
#include "Data.h"

// File manipulation functions
#include "FileManipulation.h"

// String manipulation functions
#include "StringManipulation.h"

// Reseau
#include "Reseau.h"

// SocketSelection
#include "SocketSelection.h"

// struct Packet
#include "Packet.h"

// struct CachePacket
#include "CachePacket.h"

// enum TypeEntreeLog
#include "TypeEntreeLog.h"

// Log
#include "Log.h"

// Entree dans le cache
#include "EntreeCacheLog.h"

// Cache log
#include "CacheLog.h"

// struct ClientServeur
#include "ClientServeur.h"

// PacketIO
#include "PacketIO.h"

// Thread client serveur
#include "ThreadClientServeur.h"

// struct Serveur
#include "Serveur.h"

// Packet Entrant
#include "PacketEntrant.h"

// HTTPDef
#include "HTTPDef.h"

// HTTPProcess
#include "HTTPProcess.h"

// Caractere HTML
#include "CaractereHTML.h"

// Update list
#include "UpdateList.h"

// enum CommandeServeur
#include "CommandeServeur.h"

// Gestion commandes
#include "GestionCommandeServeur.h"

// Thread acceptation client serveur
#include "ThreadAcceptationClientServeur.h"

// enum HTTPSpecialTreatmentList
#include "HTTPSpecialTreatmentList.h"

// Traitement des cas particuliers
#include "HTTPSpecialTreatment.h"

// Enregistrer les packets bruts
#include "LogPacket.h"

#endif // !TYPES_PROTECT

