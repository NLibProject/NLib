#ifndef RESEAU_PROTECT
#define RESEAU_PROTECT

// Initialiser reseau
BOOL Reseau_Initialiser( void );

// Est reseau initialise?
BOOL Reseau_EstInitialise( void );

// Detruire reseau
void Reseau_Detruire( void );

// Variables internes
#ifdef RESEAU_INTERNE
// Le reseau est initialise?
static BOOL m_estInitialise = 0;

// Contexte WSA pour windows uniquement
#ifdef IS_WINDOWS
WSADATA m_contexteWSA;
#endif // IS_WINDOWS
#endif // RESEAU_INTERNE

// Link
#ifdef IS_WINDOWS
#pragma comment( lib, "Ws2_32.lib" )
#endif // IS_WINDOWS


#endif // !RESEAU_PROTECT

