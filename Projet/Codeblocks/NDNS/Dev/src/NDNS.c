#include "../include/NDNS.h"

// --------------
// namespace NDNS
// --------------

NBOOL reception( NPacketUDP *packet,
	const NClientUDP *client )
{
	int i;
	int tmp = 0;
	printf( "\n------\nJe recois:" );
	for( i = 0; i < packet->m_packet->m_taille; i++ )
	{
		if( i % 16 == 0 )
			puts( "" );

		tmp = packet->m_packet->m_data[ i ];
		tmp = tmp & 0x000000FF;
		printf( "%.2x", tmp );

		printf( " " );
		printf( " " );
	}
	puts( "\n\n===\n\n" );
	for( i = 0; i < packet->m_packet->m_taille; i++ )
	{
		tmp = packet->m_packet->m_data[ i ];
		tmp = tmp & 0x000000FF;
		printf( "%c", tmp );
	}
	puts("\n-------\n" );
}

NS32 main( NS32 argc,
	char *argv[ ] )
{
	NClientUDP *client;

	// Initialiser NLib
	NLib_Initialiser( NULL );

	client = NLib_Module_Reseau_UDP_Client_NClientUDP_Construire( 16502,
		reception,
		NFALSE,
		1000 );
	getchar( );

	NLib_Module_Reseau_UDP_Client_NClientUDP_Detruire( &client );

	// Detruire NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

