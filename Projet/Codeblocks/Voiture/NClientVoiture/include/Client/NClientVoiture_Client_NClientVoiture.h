#ifndef NCLIENTVOITURE_CLIENT_NCLIENTVOITURE_PROTECT
#define NCLIENTVOITURE_CLIENT_NCLIENTVOITURE_PROTECT

// ---------------------------------------------
// struct NClientVoiture::Client::NClientVoiture
// ---------------------------------------------

typedef struct NClientVoiture
{
	// Mot de passe
	const char *m_motDePasse;

	// Nom capture
	char *m_nomCapture;

	// Client
	NClient *m_client;

	// Est authentifie?
	NBOOL m_estAuthentifie;

	// Resolution
	NUPoint m_resolution;

	// Nombre de frames par seconde
	NU32 m_frequence;

	// BPP
	NU32 m_bpp;

	// Mutex
	NMutex *m_mutex;

#ifndef NCLIENTVOITURE_DESACTIVER_UI
	// Texture surface
	NSurface *m_surface;

	// UI
	void *m_ui;
#endif // !NCLIENTVOITURE_DESACTIVER_UI

	// Thread de mise a jour
	NThread *m_thread;

	// Est en cours?
	NBOOL m_estEnCours;
} NClientVoiture;

/**
 * Construire le client
 *
 * @param ui
 *		L'ui si activee, NULL sinon
 * @param adresse
 *		L'adresse du serveur auquel se connecter
 * @param port
 *		Le port du serveur d'emission
 * @param motDePasse
 *		Le mot de passe d'acces au stream
 *
 * @return l'instance du client
 */
__ALLOC NClientVoiture *NClientVoiture_Client_NClientVoiture_Construire( void *ui,
	const char *adresse,
	NU32 port,
	const char *motDePasse );

/**
 * Detruire le client
 *
 * @param this
 *		Cette instance
 */
void NClientVoiture_Client_NClientVoiture_Detruire( NClientVoiture** );

#ifndef NCLIENTVOITURE_DESACTIVER_UI

/**
 * Obtenir la texture de l'image
 *
 * @param this
 *		Cette instance
 *
 * @return la texture de l'image reçue ou NULL si rien reçu
 */
__MUSTBEPROTECTED const NSurface *NClientVoiture_Client_NClientVoiture_ObtenirTexture( const NClientVoiture* );

#endif // !NCLIENTVOITURE_DESACTIVER_UI

/**
 * Proteger le client
 *
 * @param this
 *		Cette instance
 */
__WILLLOCK void NClientVoiture_Client_NClientVoiture_ActiverProtection( NClientVoiture* );

/**
 * Ne plus proteger le client
 *
 * @param this
 *		Cette instance
 */
__WILLUNLOCK void NClientVoiture_Client_NClientVoiture_DesactiverProtection( NClientVoiture* );

#endif // !NCLIENTVOITURE_CLIENT_NCLIENTVOITURE_PROTECT

