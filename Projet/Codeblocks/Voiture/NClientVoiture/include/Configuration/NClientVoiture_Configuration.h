#ifndef NCLIENTVOITURE_CONFIGURATION_PROTECT
#define NCLIENTVOITURE_CONFIGURATION_PROTECT

// ---------------------------------------
// namespace NClientVoiture::Configuration
// ---------------------------------------

// enum NClientVoiture::Configuration::NProprieteConfiguration
#include "NClientVoiture_Configuration_NProprieteConfiguration.h"

// struct NClientVoiture::Configuration::NConfiguration
#include "NClientVoiture_Configuration_NConfiguration.h"

#endif // !NCLIENTVOITURE_CONFIGURATION_PROTECT

