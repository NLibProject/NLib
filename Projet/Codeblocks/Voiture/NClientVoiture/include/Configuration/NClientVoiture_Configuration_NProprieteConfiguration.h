#ifndef NCLIENTVOITURE_CONFIGURATION_NPROPRIETECONFIGURATION_PROTECT
#define NCLIENTVOITURE_CONFIGURATION_NPROPRIETECONFIGURATION_PROTECT

// -----------------------------------------------------------
// enum NClientVoiture::Configuration::NProprieteConfiguration
// -----------------------------------------------------------

typedef enum NProprieteConfiguration
{
	NPROPRIETE_CONFIGURATION_PORT_ECOUTE_SERVEUR,
	NPROPRIETE_CONFIGURATION_PORT_EMISSION_SERVEUR,

	NPROPRIETE_CONFIGURATION_RESOLUTION_CAPTURE_X,
	NPROPRIETE_CONFIGURATION_RESOLUTION_CAPTURE_Y,
	NPROPRIETE_CONFIGURATION_NOM_CAPTURE,

	NPROPRIETE_CONFIGURATION_EST_UI_ACTIVE,
	NPROPRIETE_CONFIGURATION_EST_CAPTURE_ACTIVE,

	NPROPRIETE_CONFIGURATION_ADRESSE_SERVEUR,

	NPROPRIETE_CONFIGURATION_FREQUENCE_CAPTURE,

	NPROPRIETE_CONFIGURATION_MOT_DE_PASSE_SERVEUR,

	NPROPRIETES_CONFIGURATION
} NProprieteConfiguration;

/**
 * Obtenir ensemble clefs
 * (Seul le conteneur doit etre libere)
 *
 * @return l'ensemble des clefs
 */
__ALLOC char **NClientVoiture_Configuration_NProprieteConfiguration_ObtenirEnsembleClef( void );

#ifdef NCLIENTVOITURE_CONFIGURATION_NPROPRIETECONFIGURATION_INTERNE
static const char NProprieteConfigurationTexte[ NPROPRIETES_CONFIGURATION ][ 32 ] =
{
	"Port ecoute: ",
	"Port emission: ",

	"Resolution capture x: ",
	"Resolution capture y: ",
	"Nom capture: ",

	"UI active: ",
	"Capture active: ",

	"Adresse serveur: ",

	"Frequence capture: ",

	"Mot de passe: "
};
#endif // NCLIENTVOITURE_CONFIGURATION_NPROPRIETECONFIGURATION_INTERNE

#endif // !NCLIENTVOITURE_CONFIGURATION_NPROPRIETECONFIGURATION_PROTECT

