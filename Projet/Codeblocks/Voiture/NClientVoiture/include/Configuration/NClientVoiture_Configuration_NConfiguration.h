#ifndef NCLIENTVOITURE_CONFIGURATION_NCONFIGURATION_PROTECT
#define NCLIENTVOITURE_CONFIGURATION_NCONFIGURATION_PROTECT

// ----------------------------------------------------
// struct NClientVoiture::Configuration::NConfiguration
// ----------------------------------------------------

typedef struct NConfiguration
{
	// On doit afficher l'UI
	NBOOL m_estUIActive;

	// On capture avec les cameras?
	NBOOL m_estCaptureActive;

    // Resolution de capture
    NUPoint m_resolutionCapture;

	// Nom de la capture
	char *m_nomCapture;

	// Adresse du serveur
	char *m_adresseServeur;

	// Mot de passe serveur
	char *m_motDePasse;

    // Port d'ecoute du serveur
    NU32 m_portEcouteServeur;

    // Port d'emission du flux video du serveur
    NU32 m_portEmissionFluxVideoServeur;

    // Nombre d'images par seconde
    NU32 m_frequenceCapture;
} NConfiguration;

/**
 * Construire la configuration
 *
 * @param lien
 *		Le lien relatif du fichier de configuration
 *
 * @return l'instance de la configuration
 */
__ALLOC NConfiguration *NClientVoiture_Configuration_NConfiguration_Construire( const char *lien );

/**
 * Detruire la configuration
 *
 * @param this
 *		Cette instance
 */
void NClientVoiture_Configuration_NConfiguration_Detruire( NConfiguration** );

/**
 * Obtenir la resolution
 *
 * @param this
 *		Cette instance
 *
 * @return la resolution
 */
const NUPoint *NClientVoiture_Configuration_NConfiguration_ObtenirResolutionCapture( const NConfiguration* );

/**
 * Obtenir port ecoute serveur
 *
 * @param this
 *		Cette instance
 *
 * @return le port d'ecoute du serveur
 */
NU32 NClientVoiture_Configuration_NConfiguration_ObtenirPortEcouteServeur( const NConfiguration* );

/**
 * Obtenir port emission serveur
 *
 * @param this
 *		Cette instance
 *
 * @return le port d'emission du serveur
 */
NU32 NClientVoiture_Configuration_NConfiguration_ObtenirPortEmissionServeur( const NConfiguration* );

/**
 * Est UI active?
 *
 * @param this
 *		Cette instance
 *
 * @return si l'UI est active
 */
NBOOL NClientVoiture_Configuration_NConfiguration_EstUIActive( const NConfiguration* );

/**
 * Est capture active?
 *
 * @param this
 *		Cette instance
 *
 * @return si la capture est active
 */
NBOOL NClientVoiture_Configuration_NConfiguration_EstCaptureActive( const NConfiguration* );

/**
 * Obtenir l'adresse du serveur
 *
 * @param this
 *		Cette instance
 *
 * @return l'adresse du serveur
 */
const char *NClientVoiture_Configuration_NConfiguration_ObtenirAdresseServeur( const NConfiguration* );

/**
 * Obtenir la frequence de capture
 *
 * @param this
 *		Cette instance
 *
 * @return la frequence de capture
 */
NU32 NClientVoiture_Configuration_NConfiguration_ObtenirFrequenceCapture( const NConfiguration* );

/**
 * Obtenir le nom de la capture
 *
 * @param this
 *		Cette instance
 *
 * @return le nom de la capture
 */
const char *NClientVoiture_Configuration_NConfiguration_ObtenirNomCapture( const NConfiguration* );

/**
 * Obtenir mot de passe
 *
 * @param this
 *		Cette instance
 *
 * @return le mot de passe
 */
const char *NClientVoiture_Configuration_NConfiguration_ObtenirMotDePasse( const NConfiguration* );

#endif // !NCLIENTVOITURE_CONFIGURATION_NCONFIGURATION_PROTECT

