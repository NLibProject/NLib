#ifndef NCLIENTVOITURE_PROTECT
#define NCLIENTVOITURE_PROTECT

// ------------------------
// namespace NClientVoiture
// ------------------------

// Fichier de configuraton
#define NCLIENTVOITURE_FICHIER_CONFIGURATION		"Configuration.ini"

// namespace NVoiture
#include "../../NVoiture/include/NVoiture.h"

// namespace NClientVoiture::Configuration
#include "Configuration/NClientVoiture_Configuration.h"

// namespace NClientVoiture::Camera
#include "Camera/NClientVoiture_Camera.h"

// namespace NClientVoiture::Client
#include "Client/NClientVoiture_Client.h"

// namespace NClientVoiture::UI
#include "UI/NClientVoiture_UI.h"

#endif // !NCLIENTVOITURE_PROTECT

