#ifndef NCLIENTVOITURE_CAMERA_NCAMERAVOITURE_PROTECT
#define NCLIENTVOITURE_CAMERA_NCAMERAVOITURE_PROTECT

// ---------------------------------------------
// struct NClientVoiture::Camera::NCameraVoiture
// ---------------------------------------------

typedef struct NCameraVoiture
{
	// Camera
	NCamera *m_camera;

	// Nom de la capture
	char *m_nomCapture;

	// Client TCP
	NClient *m_client;

	// Thread de prise de vue
	NThread *m_thread;

	// Frequence de capture
	NU32 m_frequenceCapture;

	// Est en cours?
	NBOOL m_estEnCours;
} NCameraVoiture;

/**
 * Construire la camera
 *
 * @param device
 *		Le peripherique source
 * @param resolution
 *		La resolution de capture
 * @param adresseServeur
 *		L'adresse IP du serveur
 * @param portServeur
 *		Le port de connexion au serveur
 * @param frequenceCapture
 *		La frequence de capture des images
 * @param nomCapture
 *		Le nom de la capture
 *
 * @return l'instance de la camera
 */
__ALLOC NCameraVoiture *NClientVoiture_Camera_NCameraVoiture_Construire( const char *device,
	NUPoint resolution,
	const char *adresseServeur,
	NU32 portServeur,
	NU32 frequenceCapture,
	const char *nomCapture );

/**
 * Detruire l'instance
 *
 * @param this
 *		Cette instance
 */
void NClientVoiture_Camera_NCameraVoiture_Detruire( NCameraVoiture** );

#endif // !NCLIENTVOITURE_CAMERA_NCAMERAVOITURE_PROTECT

