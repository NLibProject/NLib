#ifndef NCLIENTVOITURE_CAMERA_NGESTIONCAMERA_PROTECT
#define NCLIENTVOITURE_CAMERA_NGESTIONCAMERA_PROTECT

// ---------------------------------------------
// struct NClientVoiture::Camera::NGestionCamera
// ---------------------------------------------

typedef struct NGestionCamera
{
	// Nombre de camera
	NU32 m_nombre;

	// Camera
	NCameraVoiture **m_camera;
} NGestionCamera;

/**
 * Construire le gestionnaire
 *
 * @param resolutionX
 *		La resolution de capture horizontale
 * @param resolutionY
 *		La resolution de capture verticale
 * @param adresseServeur
 *		L'adresse IP du serveur
 * @param portServeur
 *		Le port du serveur
 * @param frequenceCapture
 *		Le nombre d'images par secondes
 * @param nomCapture
 *		Le nom donnees a cette capture
 *
 * @return l'instance du gestionnaire
 */
__ALLOC NGestionCamera *NClientVoiture_Camera_NGestionCamera_Construire( NU32 resolutionX,
	NU32 resolutionY,
	const char *adresseServeur,
	NU32 portServeur,
	NU32 frequenceCapture,
	const char *nomCapture );

/**
 * Detruire l'instance du gestionnaire
 *
 * @param this
 *		Cette instance
 */
void NClientVoiture_Camera_NGestionCamera_Detruire( NGestionCamera** );

/**
 * Obtenir le nombre de cameras
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de cameras
 */
NU32 NClientVoiture_Camera_NGestionCamera_ObtenirNombreCamera( const NGestionCamera* );

#endif // !NCLIENTVOITURE_CAMERA_NGESTIONCAMERA_PROTECT

