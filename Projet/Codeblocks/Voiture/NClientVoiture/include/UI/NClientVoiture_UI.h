#ifndef NCLIENTVOITURE_UI_PROTECT
#define NCLIENTVOITURE_UI_PROTECT

// ----------------------------
// namespace NClientVoiture::UI
// ----------------------------

#ifndef NCLIENTVOITURE_DESACTIVER_UI

// Framerate souhaite
#define NCLIENTVOITURE_UI_FRAMERATE			30

// struct NClientVoiture::UI::NUIClient
#include "NClientVoiture_UI_NUIClient.h"

#endif // !NCLIENTVOITURE_DESACTIVER_UI

#endif // !NCLIENTVOITURE_UI_PROTECT

