#ifndef NCLIENTVOITURE_UI_NUICLIENT_PROTECT
#define NCLIENTVOITURE_UI_NUICLIENT_PROTECT

// ------------------------------------
// struct NClientVoiture::UI::NUIClient
// ------------------------------------

#ifndef NCLIENTVOITURE_DESACTIVER_UI

typedef struct NUIClient
{
    // Fenetre
    NFenetre *m_fenetre;

    // Framerate
    NFramerate *m_framerate;

    // Est en cours?
    NBOOL m_estEnCours;

    // Client voiture
    NClientVoiture *m_client;

	// Configuration
	const NConfiguration *m_configuration;
} NUIClient;

/**
 * Construire l'UI
 *
 * @param configuration
 *		La configuration
 *
 * @return l'instance de l'UI
 */
__ALLOC NUIClient *NClientVoiture_UI_NUIClient_Construire( const NConfiguration* );

/**
 * Detruire l'UI
 *
 * @param this
 *		Cette instance
 */
void NClientVoiture_UI_NUIClient_Detruire( NUIClient** );

/**
 * Lancer l'UI
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NClientVoiture_UI_NUIClient_Lancer( NUIClient* );

/**
 * Obtenir fenetre
 *
 * @param this
 *		Cette instance
 *
 * @return la fenetre
 */
const NFenetre *NClientVoiture_UI_NUIClient_ObtenirFenetre( const NUIClient* );

#endif // !NCLIENTVOITURE_DESACTIVER_UI

#endif // !NCLIENTVOITURE_UI_NUICLIENT_PROTECT

