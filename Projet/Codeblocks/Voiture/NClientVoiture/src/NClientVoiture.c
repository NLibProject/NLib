#include "../include/NClientVoiture.h"

// ------------------------
// namespace NClientVoiture
// ------------------------

/**
 * Fonction principale
 *
 * @param argc
 *		Le nombre d'arguments
 * @param argv
 *		Les arguments
 *
 * @return EXIT_SUCESS si l'operation s'est bien passee
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Gestionnaire des cameras
	NGestionCamera *camera;

	// Configuration
	NConfiguration *configuration;

#ifndef NCLIENTVOITURE_DESACTIVER_UI
	// UI
	NUIClient *ui;
#endif // !NCLIENTVOITURE_DESACTIVER_UI

	// Initialiser NLib
	NLib_Initialiser( NULL );

	// Charger la configuration
	if( !( configuration = NClientVoiture_Configuration_NConfiguration_Construire( NCLIENTVOITURE_FICHIER_CONFIGURATION ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
    }

    // Capture active?
    if( NClientVoiture_Configuration_NConfiguration_EstCaptureActive( configuration ) )
		// Construire le gestionnaire de cameras
		if( !( camera = NClientVoiture_Camera_NGestionCamera_Construire( NClientVoiture_Configuration_NConfiguration_ObtenirResolutionCapture( configuration )->x,
			NClientVoiture_Configuration_NConfiguration_ObtenirResolutionCapture( configuration )->y,
			NClientVoiture_Configuration_NConfiguration_ObtenirAdresseServeur( configuration ),
			NClientVoiture_Configuration_NConfiguration_ObtenirPortEcouteServeur( configuration ),
			NClientVoiture_Configuration_NConfiguration_ObtenirFrequenceCapture( configuration ),
			NClientVoiture_Configuration_NConfiguration_ObtenirNomCapture( configuration ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire la configuration
			NClientVoiture_Configuration_NConfiguration_Detruire( &configuration );

			// Detruire NLib
			NLib_Detruire( );

			// Quitter
			return EXIT_FAILURE;
		}

#ifndef NCLIENTVOITURE_DESACTIVER_UI
	// Construire l'UI
	if( !( ui = NClientVoiture_UI_NUIClient_Construire( configuration ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire le gestionnaire
		NClientVoiture_Camera_NGestionCamera_Detruire( &camera );

		// Detruire configuration
		NClientVoiture_Configuration_NConfiguration_Detruire( &configuration );

		// Detruire NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Lancer l'UI
	NClientVoiture_UI_NUIClient_Lancer( ui );

	// Detruire l'UI
	NClientVoiture_UI_NUIClient_Detruire( &ui );
#else // !NCLIENTVOITURE_DESACTIVER_UI
	getchar( );
#endif // NCLIENTVOITURE_DESACTIVER_UI

	// Capture active?
	if( NClientVoiture_Configuration_NConfiguration_EstCaptureActive( configuration ) )
		// Detruire le gestionnaire de cameras
		NClientVoiture_Camera_NGestionCamera_Detruire( &camera );

	// Detruire la configuration
    NClientVoiture_Configuration_NConfiguration_Detruire( &configuration );

	// Detruire NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

