#include "../../include/NClientVoiture.h"

// ---------------------------------------------
// struct NClientVoiture::Client::NClientVoiture
// ---------------------------------------------

/**
 * Authentifier le client
 *
 * @param this
 *		Cette instance
 * @param trame
 *		La trame reçue
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NClientVoiture_Client_NClientVoiture_Authentifier( NClientVoiture *this,
	const NTrame *trame )
{
	// Lecteur de donnees
	NLecteurMemoire *lecteur;

	// Taille nom
	NU32 tailleNom;

	// Verifier etat/flux
	if( this->m_estAuthentifie )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Construire le lecteur
	if( !( lecteur = NLib_Memoire_NLecteurMemoire_Construire( NVoiture_Trame_NTrame_ObtenirData( trame ),
		NVoiture_Trame_NTrame_ObtenirTailleData( trame ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	/** Lire donnees **/
	// Frequence
	this->m_frequence = NLib_Memoire_NLecteurMemoire_Lire2( lecteur );

	// Taille nom
	tailleNom = NLib_Memoire_NLecteurMemoire_Lire2( lecteur );

	// Nom
	if( !( this->m_nomCapture = NLib_Memoire_NLecteurMemoire_LireCopieValeur( lecteur,
		tailleNom,
		NTRUE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Resolution X
	this->m_resolution.x = NLib_Memoire_NLecteurMemoire_Lire2( lecteur );

	// Resolution Y
	this->m_resolution.y = NLib_Memoire_NLecteurMemoire_Lire2( lecteur );

	// BPP
	this->m_bpp = NLib_Memoire_NLecteurMemoire_Lire2( lecteur );

	// Detruire le lecteur
	NLib_Memoire_NLecteurMemoire_Detruire( &lecteur );

	// OK
	return this->m_estAuthentifie = NTRUE;
}

/**
 * Recevoir une image
 *
 * @param this
 *		Cette instance
 * @param data
 *		Les donnees reçues
 * @param tailleData
 *		La taille des donnees reçues
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NClientVoiture_Client_NClientVoiture_RecevoirImage( NClientVoiture *this,
	const char *data,
	NU32 tailleData )
{
#ifndef NCLIENTVOITURE_DESACTIVER_UI
	// Surface
	SDL_Surface *surface;

	// Texture
	NSurface *texture;

	// Ancienne texture
	NSurface *ancienneTexture;
#endif // !NCLIENTVOITURE_DESACTIVER_UI

	// Verifier flux
	if( *((NU32*)( data )) != this->m_resolution.x
		|| *((NU32*)( data + sizeof( NU32 ) )) != this->m_resolution.y
		|| *((NU32*)( data + sizeof( NU32 ) * 2 )) != this->m_bpp )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

#ifndef NCLIENTVOITURE_DESACTIVER_UI
	// Creer la surface
	if( !( surface = SDL_CreateRGBSurfaceFrom( (void*)( data + sizeof( NU32 ) * 3 ),
		this->m_resolution.x,
		this->m_resolution.y,
		this->m_bpp * 8,
		this->m_resolution.x * this->m_bpp,
		0x00000000,
		0x00000000,
		0X00000000,
		0x00000000 ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Quitter
		return NFALSE;
	}

	// Creer texture
	if( !( texture = NLib_Module_SDL_Surface_NSurface_Construire3( NClientVoiture_UI_NUIClient_ObtenirFenetre( this->m_ui ),
		surface ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire surface
		NLIB_NETTOYER_SURFACE( surface );

		// Quitter
		return NFALSE;
	}

	// Proteger
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Enregistrer ancienne texture a liberer
	ancienneTexture = this->m_surface;

	// Enregistrer nouvelle texture
	this->m_surface = texture;

	// Ne plus proteger
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// Detruire surface
	NLIB_NETTOYER_SURFACE( surface );

	// Detruire la texture
	NLib_Module_SDL_Surface_NSurface_Detruire( &ancienneTexture );
#endif // !NCLIENTVOITURE_DESACTIVER_UI

	// OK
	return NTRUE;
}

/**
 * Callback reception packet
 *
 * @param packet
 *		Le packet reçu
 * @param client
 *		Le client
 *
 * @return si l'operation a reussi
 */
__CALLBACK NBOOL NClientVoiture_Client_NClientVoiture_CallbackReceptionPacket( const NPacket *packet,
	const NClient *client )
{
	// Trame
	NTrame *trame;

	// Cette instance
	NClientVoiture *this;

	// Obtenir instance
	this = NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client );

	// Construire la trame
	if( !( trame = NVoiture_Trame_NTrame_Construire2( NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Analyser trame
	switch( NVoiture_Trame_NTrame_ObtenirType( trame ) )
	{
		case NTYPE_TRAME_PARAMETRAGE_FLUX_VIDEO:
            if( !NClientVoiture_Client_NClientVoiture_Authentifier( this,
                trame ) )
            {
				// Notifier
				NOTIFIER_ERREUR( NERREUR_LOGIN_FAILED );

				// Detruire trame
				NVoiture_Trame_NTrame_Detruire( &trame );

				// Quitter
				return NFALSE;
            }
			break;

		case NTYPE_TRAME_TRANSMISSION_IMAGE:
			// Recevoir l'image
			NClientVoiture_Client_NClientVoiture_RecevoirImage( this,
				NVoiture_Trame_NTrame_ObtenirData( trame ),
				NVoiture_Trame_NTrame_ObtenirTailleData( trame ) );
			break;

		default:
			break;
    }

	// Detruire la trame
	NVoiture_Trame_NTrame_Detruire( &trame );

	// OK
	return NTRUE;
}

/**
 * Callback deconnexion
 *
 * @param client
 *		Le client qui se deconnecte
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NClientVoiture_Client_NClientVoiture_CallbackDeconnexion( const NClient *client )
{
	// Cette instance
	NClientVoiture *this;

	// Obtenir instance
	this = NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client );

	// On est plus authentifie
	this->m_estAuthentifie = NFALSE;

	// Liberer
	NFREE( this->m_nomCapture );

	// OK
	return NTRUE;
}

/**
 * Envoyer packet authentification
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NClientVoiture_Client_NClientVoiture_EnvoyerPacketAuthentification( NClientVoiture *this )
{
	// Packet
	NPacket *packet;

	// Buffer
	NU32 buffer;

	// Construire le packet
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Ajouter type
	buffer = NTYPE_TRAME_AUTHENTIFICATION_CLIENT;
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&buffer,
		sizeof( NU32 ) );

	// Ajouter taille mot de passe
	buffer = strlen( this->m_motDePasse );
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&buffer,
		sizeof( NU32 ) );

	// Ajouter mot de passe
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		this->m_motDePasse,
		strlen( this->m_motDePasse ) );

	// OK
	return NLib_Module_Reseau_Client_NClient_AjouterPacket( this->m_client,
		packet );
}

/**
 * Thread update
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
__THREAD NBOOL NClientVoiture_Client_NClientVoiture_ThreadUpdate( NClientVoiture *this )
{
	// Attendre la connexion du client emission au serveur
	NLib_Temps_Attendre( 1000 );

	// Thread
	do
	{
        // On est deconnecte?
        if( !NLib_Module_Reseau_Client_NClient_EstConnecte( this->m_client ) )
        {
			// Tenter de se connecter
			if( NLib_Module_Reseau_Client_NClient_Connecter( this->m_client ) )
				// Envoyer packet authentification
				NClientVoiture_Client_NClientVoiture_EnvoyerPacketAuthentification( this );
		}
		else
			if( NLib_Module_Reseau_Client_NClient_EstErreur( this->m_client ) )
				NLib_Module_Reseau_Client_NClient_Deconnecter( this->m_client );

        // Attendre
        NLib_Temps_Attendre( 16 );
	} while( this->m_estEnCours );

	// OK
	return NTRUE;
}

/**
 * Construire le client
 *
 * @param ui
 *		L'ui si activee, NULL sinon
 * @param adresse
 *		L'adresse du serveur auquel se connecter
 * @param port
 *		Le port du serveur d'emission
 * @param motDePasse
 *		Le mot de passe d'acces au stream
 *
 * @return l'instance du client
 */
__ALLOC NClientVoiture *NClientVoiture_Client_NClientVoiture_Construire( void *ui,
	const char *adresse,
	NU32 port,
	const char *motDePasse )
{
	// Sortie
	__OUTPUT NClientVoiture *out;

	// Allouer la sortie
	if( !( out = calloc( 1,
		sizeof( NClientVoiture ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire mutex
	if( !( out->m_mutex = NLib_Mutex_NMutex_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire client
	if( !( out->m_client = NLib_Module_Reseau_Client_NClient_Construire( adresse,
		port,
		NClientVoiture_Client_NClientVoiture_CallbackReceptionPacket,
		NClientVoiture_Client_NClientVoiture_CallbackDeconnexion,
		out,
		50,
		NTYPE_CLIENT_TCP,
		NMETHODE_TRANSMISSION_PACKETIO_RAW ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_motDePasse = motDePasse;
	out->m_ui = ui;

	// Lancer thread
	out->m_estEnCours = NTRUE;

	// Creer thread
	if( !( out->m_thread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NClientVoiture_Client_NClientVoiture_ThreadUpdate,
		out,
		&out->m_estEnCours ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire client
		NLib_Module_Reseau_Client_NClient_Detruire( &out->m_client );

		// Detruire mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire le client
 *
 * @param this
 *		Cette instance
 */
void NClientVoiture_Client_NClientVoiture_Detruire( NClientVoiture **this )
{
	// Liberer nom
	NFREE( (*this)->m_nomCapture );

	// Detruire thread
	NLib_Thread_NThread_Detruire( &(*this)->m_thread );

	// Detruire client
	NLib_Module_Reseau_Client_NClient_Detruire( &(*this)->m_client );

	// Detruire mutex
	NLib_Mutex_NMutex_Detruire( &(*this)->m_mutex );

#ifndef NCLIENTVOITURE_DESACTIVER_UI
	// Detruire texture
	NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surface );
#endif // !NCLIENTVOITURE_DESACTIVER_UI

	// Liberer
	NFREE( *this );
}

#ifndef NCLIENTVOITURE_DESACTIVER_UI
/**
 * Obtenir la texture de l'image
 *
 * @param this
 *		Cette instance
 *
 * @return la texture de l'image reçue ou NULL si rien reçu
 */
__MUSTBEPROTECTED const NSurface *NClientVoiture_Client_NClientVoiture_ObtenirTexture( const NClientVoiture *this )
{
	return this->m_surface;
}
#endif // !NCLIENTVOITURE_DESACTIVER_UI

/**
 * Proteger le client
 *
 * @param this
 *		Cette instance
 */
__WILLLOCK void NClientVoiture_Client_NClientVoiture_ActiverProtection( NClientVoiture *this )
{
	NLib_Mutex_NMutex_Lock( this->m_mutex );
}

/**
 * Ne plus proteger le client
 *
 * @param this
 *		Cette instance
 */
__WILLUNLOCK void NClientVoiture_Client_NClientVoiture_DesactiverProtection( NClientVoiture *this )
{
	NLib_Mutex_NMutex_Unlock( this->m_mutex );
}


