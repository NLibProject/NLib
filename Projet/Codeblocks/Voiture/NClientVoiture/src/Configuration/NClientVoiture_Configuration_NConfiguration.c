#include "../../include/NClientVoiture.h"

// ----------------------------------------------------
// struct NClientVoiture::Configuration::NConfiguration
// ----------------------------------------------------

/**
 * Construire la configuration
 *
 * @param lien
 *		Le lien relatif du fichier de configuration
 *
 * @return l'instance de la configuration
 */
__ALLOC NConfiguration *NClientVoiture_Configuration_NConfiguration_Construire( const char *lien )
{
	// Sortie
	__OUTPUT NConfiguration *out;

	// Ensemble des clefs
	char **clefs;

	// Fichier de configuration
	NFichierClef *fichier;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NConfiguration ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Obtenir les clefs
	if( !( clefs = NClientVoiture_Configuration_NProprieteConfiguration_ObtenirEnsembleClef( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_EXPORT_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Charger le fichier
	if( !( fichier = NLib_Fichier_Clef_NFichierClef_Construire( lien,
		(const char**)clefs,
		NPROPRIETES_CONFIGURATION ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( clefs );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Liberer clefs
	NFREE( clefs );

	// Lire proprietes
		// Port ecoute
			out->m_portEcouteServeur = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_PORT_ECOUTE_SERVEUR );
		// Port emission
			out->m_portEmissionFluxVideoServeur = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_PORT_EMISSION_SERVEUR );
		// Resolution
			NDEFINIR_POSITION( out->m_resolutionCapture,
				NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
					NPROPRIETE_CONFIGURATION_RESOLUTION_CAPTURE_X ),
				NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
					NPROPRIETE_CONFIGURATION_RESOLUTION_CAPTURE_Y ) );
		// Nom capture
			out->m_nomCapture = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( fichier,
				NPROPRIETE_CONFIGURATION_NOM_CAPTURE );
		// Est UI active?
			out->m_estUIActive = NLib_Chaine_Comparer( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( fichier,
					NPROPRIETE_CONFIGURATION_EST_UI_ACTIVE ),
				"oui",
				NFALSE,
				0 );
		// Est capture active?
			out->m_estCaptureActive = NLib_Chaine_Comparer( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( fichier,
					NPROPRIETE_CONFIGURATION_EST_CAPTURE_ACTIVE ),
				"oui",
				NFALSE,
				0 );
		// Adresse serveur
			out->m_adresseServeur = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( fichier,
				NPROPRIETE_CONFIGURATION_ADRESSE_SERVEUR );
		// Frequence de capture
			out->m_frequenceCapture = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_FREQUENCE_CAPTURE );
		// Mot de passe
			out->m_motDePasse = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( fichier,
				NPROPRIETE_CONFIGURATION_MOT_DE_PASSE_SERVEUR );

	// Detruire fichier
	NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

	// OK
	return out;
}

/**
 * Detruire la configuration
 *
 * @param this
 *		Cette instance
 */
void NClientVoiture_Configuration_NConfiguration_Detruire( NConfiguration **this )
{
	// Liberer
	NFREE( (*this)->m_motDePasse );
	NFREE( (*this)->m_nomCapture );
	NFREE( (*this)->m_adresseServeur );
	NFREE( *this );
}

/**
 * Obtenir la resolution
 *
 * @param this
 *		Cette instance
 *
 * @return la resolution
 */
const NUPoint *NClientVoiture_Configuration_NConfiguration_ObtenirResolutionCapture( const NConfiguration *this )
{
	return &this->m_resolutionCapture;
}

/**
 * Obtenir port ecoute serveur
 *
 * @param this
 *		Cette instance
 *
 * @return le port d'ecoute du serveur
 */
NU32 NClientVoiture_Configuration_NConfiguration_ObtenirPortEcouteServeur( const NConfiguration *this )
{
	return this->m_portEcouteServeur;
}

/**
 * Obtenir port emission serveur
 *
 * @param this
 *		Cette instance
 *
 * @return le port d'emission du serveur
 */
NU32 NClientVoiture_Configuration_NConfiguration_ObtenirPortEmissionServeur( const NConfiguration *this )
{
	return this->m_portEmissionFluxVideoServeur;
}

/**
 * Est UI active?
 *
 * @param this
 *		Cette instance
 *
 * @return si l'UI est active
 */
NBOOL NClientVoiture_Configuration_NConfiguration_EstUIActive( const NConfiguration *this )
{
	return this->m_estUIActive;
}

/**
 * Est capture active?
 *
 * @param this
 *		Cette instance
 *
 * @return si la capture est active
 */
NBOOL NClientVoiture_Configuration_NConfiguration_EstCaptureActive( const NConfiguration *this )
{
	return this->m_estCaptureActive;
}

/**
 * Obtenir l'adresse du serveur
 *
 * @param this
 *		Cette instance
 *
 * @return l'adresse du serveur
 */
const char *NClientVoiture_Configuration_NConfiguration_ObtenirAdresseServeur( const NConfiguration *this )
{
	return this->m_adresseServeur;
}

/**
 * Obtenir la frequence de capture
 *
 * @param this
 *		Cette instance
 *
 * @return la frequence de capture
 */
NU32 NClientVoiture_Configuration_NConfiguration_ObtenirFrequenceCapture( const NConfiguration *this )
{
	return this->m_frequenceCapture;
}

/**
 * Obtenir le nom de la capture
 *
 * @param this
 *		Cette instance
 *
 * @return le nom de la capture
 */
const char *NClientVoiture_Configuration_NConfiguration_ObtenirNomCapture( const NConfiguration *this )
{
	return this->m_nomCapture;
}

/**
 * Obtenir mot de passe
 *
 * @param this
 *		Cette instance
 *
 * @return le mot de passe
 */
const char *NClientVoiture_Configuration_NConfiguration_ObtenirMotDePasse( const NConfiguration *this )
{
	return this->m_motDePasse;
}


