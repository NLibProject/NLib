#include "../../include/NClientVoiture.h"

// ---------------------------------------------
// struct NClientVoiture::Camera::NCameraVoiture
// ---------------------------------------------

NBOOL NClientVoiture_Camera_NCameraVoiture_CallbackReceptionPacket( const NPacket *packet,
	const NClient *client )
{
	// Cette instance
	NCameraVoiture *this;

	// Obtenir l'instance
	this = (NCameraVoiture*)NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client );

	// OK
	return NTRUE;
}

NBOOL NClientVoiture_Camera_NCameraVoiture_CallbackDeconnexion( const NClient *client )
{
	// Cette instance
	NCameraVoiture *this;

	// Obtenir l'instance
	this = (NCameraVoiture*)NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client );

	// OK
	return NTRUE;
}

/**
 * Envoyer le packet d'authentification
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NClientVoiture_Camera_NCameraVoiture_EnvoyerAuthentification( NCameraVoiture *this )
{
    // Packet
    NPacket *packet;

	// Valeur temporaire
	NU32 tmp;

    // Construire le packet
    if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
    {
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
    }

    /** Ajouter donnees **/
    // Type
    tmp = NTYPE_TRAME_PARAMETRAGE_FLUX_VIDEO;
    NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&tmp,
		sizeof( NU32 ) );

	// Frequence
    tmp = this->m_frequenceCapture;
    NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&tmp,
		sizeof( NU32 ) );

	// Taille nom capture
	tmp = strlen( this->m_nomCapture );
    NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&tmp,
		sizeof( NU32 ) );

	// Nom capture
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		this->m_nomCapture,
		strlen( this->m_nomCapture ) );

	// Resolution X
	tmp = NLib_Module_Webcam_NCamera_ObtenirResolution( this->m_camera )->x;
    NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&tmp,
		sizeof( NU32 ) );

	// Resolution Y
	tmp = NLib_Module_Webcam_NCamera_ObtenirResolution( this->m_camera )->y;
    NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&tmp,
		sizeof( NU32 ) );

	// BPP
    tmp = NLib_Module_Webcam_NCamera_ObtenirNombreOctetParPixel( this->m_camera );
    NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&tmp,
		sizeof( NU32 ) );

	// Ajouter le packet
	return NLib_Module_Reseau_Client_NClient_AjouterPacket( this->m_client,
		packet );
}

/**
 * Envoyer image au serveur
 *
 * @param this
 *		Cette instance
 * @param image
 *		L'image a envoyer au serveur
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NClientVoiture_Camera_NCameraVoiture_EnvoyerImage( NCameraVoiture *this,
	const NImageCamera *image )
{
	// Packet
	NPacket *packet;

	// Valeur temporaire
	NU32 tmp;

	// Construire packet
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	/** Ajouter donnees **/
	// Type
	tmp = NTYPE_TRAME_TRANSMISSION_IMAGE;
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&tmp,
		sizeof( NU32 ) );

	// Resolution X
	tmp = NLib_Module_Webcam_NImageCamera_ObtenirDimension( image )->x;
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&tmp,
		sizeof( NU32 ) );

	// Resolution Y
	tmp = NLib_Module_Webcam_NImageCamera_ObtenirDimension( image )->y;
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&tmp,
		sizeof( NU32 ) );

	// BPP
	tmp = NLib_Module_Webcam_NImageCamera_ObtenirOctetParPixel( image );
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&tmp,
		sizeof( NU32 ) );

	// Image
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		NLib_Module_Webcam_NImageCamera_ObtenirData( image ),
		NLib_Module_Webcam_NImageCamera_ObtenirTaille( image ) );

	// Envoyer le packet
	return NLib_Module_Reseau_Client_NClient_AjouterPacket( this->m_client,
		packet );
}

/**
 * Thread de capture d'images
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NClientVoiture_Camera_NCameraVoiture_ThreadCapture( NCameraVoiture *this )
{
	// Derniere capture
	NU32 derniereCapture = 0;

	// Image
	NImageCamera *image;

	// Boucle de capture
	do
	{
		// Attendre
		NLib_Temps_Attendre( 1 );

		// On doit capturer?
		if( NLib_Temps_ObtenirTick( ) - derniereCapture >= (NU32)( ( 1.0f / (float)this->m_frequenceCapture ) * 1000.0f ) )
		{
			// Enregistrer
			derniereCapture = NLib_Temps_ObtenirTick( );

			// On est connecte au serveur?
			if( NLib_Module_Reseau_Client_NClient_EstConnecte( this->m_client ) )
			{
				// Il y a une erreur
				if( NLib_Module_Reseau_Client_NClient_EstErreur( this->m_client ) )
					// Deconnecter
					NLib_Module_Reseau_Client_NClient_Deconnecter( this->m_client );
				// Aucune erreur
				else
				{
					// Capturer
					if( ( image = NLib_Module_Webcam_NCamera_Capturer( this->m_camera ) ) != NULL )
					{
						// Envoyer packet
						NClientVoiture_Camera_NCameraVoiture_EnvoyerImage( this,
							image );

						// Liberer
						NLib_Module_Webcam_NImageCamera_Detruire( &image );
					}
				}
			}
			// On doit se connecter au serveur?
			else
				// Tenter de se connecter
                if( NLib_Module_Reseau_Client_NClient_Connecter( this->m_client ) )
					// Envoyer le packet d'authentification
					NClientVoiture_Camera_NCameraVoiture_EnvoyerAuthentification( this );
		}
	} while( this->m_estEnCours );

	// OK
	return NTRUE;
}

/**
 * Construire la camera
 *
 * @param device
 *		Le peripherique source
 * @param resolution
 *		La resolution de capture
 * @param adresseServeur
 *		L'adresse IP du serveur
 * @param portServeur
 *		Le port de connexion au serveur
 * @param frequenceCapture
 *		La frequence de capture des images
 * @param nomCapture
 *		Le nom de la capture
 *
 * @return l'instance de la camera
 */
__ALLOC NCameraVoiture *NClientVoiture_Camera_NCameraVoiture_Construire( const char *device,
	NUPoint resolution,
	const char *adresseServeur,
	NU32 portServeur,
	NU32 frequenceCapture,
	const char *nomCapture )
{
	// Sortie
	__OUTPUT NCameraVoiture *out;

	// Verifier la frequence
	if( frequenceCapture <= 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NCameraVoiture ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer le nom de la capture
	if( !( out->m_nomCapture = calloc( strlen( nomCapture ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire le client
	if( !( out->m_client = NLib_Module_Reseau_Client_NClient_Construire( adresseServeur,
		portServeur,
		NClientVoiture_Camera_NCameraVoiture_CallbackReceptionPacket,
		NClientVoiture_Camera_NCameraVoiture_CallbackDeconnexion,
		out,
		0,
		NTYPE_CLIENT_TCP,
		NMETHODE_TRANSMISSION_PACKETIO_NPROJECT ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out->m_nomCapture );
		NFREE( out );

		// Quitter
		return NULL;
	}

    // Construire la camera
    if( !( out->m_camera = NLib_Module_Webcam_NCamera_Construire( device,
		resolution.x,
		resolution.y ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire client
		NLib_Module_Reseau_Client_NClient_Detruire( &out->m_client );

		// Liberer
		NFREE( out->m_nomCapture );
		NFREE( out );

		// Quitter
		return NULL;
    }

    // Enregistrer
    out->m_frequenceCapture = frequenceCapture;
    memcpy( out->m_nomCapture,
		nomCapture,
		strlen( nomCapture ) );

	// Lancer le thread
	out->m_estEnCours = NTRUE;

    // Construire le thread
    if( !( out->m_thread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NClientVoiture_Camera_NCameraVoiture_ThreadCapture,
		out,
		&out->m_estEnCours ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire camera
		NLib_Module_Webcam_NCamera_Detruire( &out->m_camera );

		// Detruire client
		NLib_Module_Reseau_Client_NClient_Detruire( &out->m_client );

		// Liberer
		NFREE( out->m_nomCapture );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire l'instance
 *
 * @param this
 *		Cette instance
 */
void NClientVoiture_Camera_NCameraVoiture_Detruire( NCameraVoiture **this )
{
	// Detruire thread
	NLib_Thread_NThread_Detruire( &(*this)->m_thread );

	// Detruire camera
	NLib_Module_Webcam_NCamera_Detruire( &(*this)->m_camera );

	// Detruire client
	NLib_Module_Reseau_Client_NClient_Detruire( &(*this)->m_client );

	// Liberer
	NFREE( (*this)->m_nomCapture );
	NFREE( (*this) );
}

