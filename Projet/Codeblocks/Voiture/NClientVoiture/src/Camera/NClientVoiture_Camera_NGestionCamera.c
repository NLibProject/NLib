#include "../../include/NClientVoiture.h"

// ---------------------------------------------
// struct NClientVoiture::Camera::NGestionCamera
// ---------------------------------------------

/**
 * Construire le gestionnaire
 *
 * @param resolutionX
 *		La resolution de capture horizontale
 * @param resolutionY
 *		La resolution de capture verticale
 * @param adresseServeur
 *		L'adresse IP du serveur
 * @param portServeur
 *		Le port du serveur
 * @param frequenceCapture
 *		Le nombre d'images par secondes
 * @param nomCapture
 *		Le nom donnees a cette capture
 *
 * @return l'instance du gestionnaire
 */
__ALLOC NGestionCamera *NClientVoiture_Camera_NGestionCamera_Construire( NU32 resolutionX,
	NU32 resolutionY,
	const char *adresseServeur,
	NU32 portServeur,
	NU32 frequenceCapture,
	const char *nomCapture )
{
	// Sortie
	__OUTPUT NGestionCamera *out;

	// Recherche de camera connectees
	NRechercheCamera *recherche;

	// Iterateur
	NU32 i;

	// Resolution
	NUPoint resolution;

	// Copier la resolution
	NDEFINIR_POSITION( resolution,
		resolutionX,
		resolutionY );

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NGestionCamera ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Recuperer la liste des cameras
	if( !( recherche = NVoiture_Camera_NRechercheCamera_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Obtenir le nombre de cameras
	if( ( out->m_nombre = NVoiture_Camera_NRechercheCamera_ObtenirNombre( recherche ) ) > 0 )
	{
		// Allouer le conteneur des cameras
		if( !( out->m_camera = calloc( out->m_nombre,
			sizeof( NCameraVoiture* ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Detruire recherche
			NVoiture_Camera_NRechercheCamera_Detruire( &recherche );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Construire les cameras
		for( i = 0; i < out->m_nombre; i++ )
			// Construire la camera
			if( !( out->m_camera[ i ] = NClientVoiture_Camera_NCameraVoiture_Construire( NVoiture_Camera_NPeripheriqueCamera_ObtenirLienComplet( NVoiture_Camera_NRechercheCamera_ObtenirPeripherique( recherche,
					i ) ),
				resolution,
				adresseServeur,
				portServeur,
				frequenceCapture,
				nomCapture ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Detruire cameras
				for( i--; (NS32)i >= 0; i-- )
					NClientVoiture_Camera_NCameraVoiture_Detruire( &out->m_camera[ i ] );

				// Detruire recherche
				NVoiture_Camera_NRechercheCamera_Detruire( &recherche );

				// Liberer
				NFREE( out->m_camera );
				NFREE( out );

				// Quitter
				return NULL;
			}
	}

	// Detruire recherche
	NVoiture_Camera_NRechercheCamera_Detruire( &recherche );

	// OK
	return out;
}

/**
 * Detruire l'instance du gestionnaire
 *
 * @param this
 *		Cette instance
 */
void NClientVoiture_Camera_NGestionCamera_Detruire( NGestionCamera **this )
{
	// Iterateur
	NU32 i = 0;

	// Detruire cameras
	for( ; i < (*this)->m_nombre; i++ )
		NClientVoiture_Camera_NCameraVoiture_Detruire( &(*this)->m_camera[ i ] );

	// Liberer
	NFREE( (*this)->m_camera );
	NFREE( (*this) );
}

/**
 * Obtenir le nombre de cameras
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de cameras
 */
NU32 NClientVoiture_Camera_NGestionCamera_ObtenirNombreCamera( const NGestionCamera *this )
{
	return this->m_nombre;
}

