#include "../../include/NClientVoiture.h"

// ------------------------------------
// struct NClientVoiture::UI::NUIClient
// ------------------------------------

#ifndef NCLIENTVOITURE_DESACTIVER_UI

/**
 * Construire l'UI
 *
 * @param configuration
 *		La configuration
 *
 * @return l'instance de l'UI
 */
__ALLOC NUIClient *NClientVoiture_UI_NUIClient_Construire( const NConfiguration *configuration )
{
	// Sortie
	__OUTPUT NUIClient *out;

	// Resolution actuelle
	NUPoint resolution;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NUIClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Si l'UI est affichee
	if( NClientVoiture_Configuration_NConfiguration_EstUIActive( configuration ) )
	{
		// Construire le client
		if( !( out->m_client = NClientVoiture_Client_NClientVoiture_Construire( out,
			NClientVoiture_Configuration_NConfiguration_ObtenirAdresseServeur( configuration ),
			NClientVoiture_Configuration_NConfiguration_ObtenirPortEmissionServeur( configuration ),
			NClientVoiture_Configuration_NConfiguration_ObtenirMotDePasse( configuration ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Obtenir la resolution
		resolution = NLib_Module_SDL_ObtenirResolutionActuelle( );

		// Construire le framerate
		if( !( out->m_framerate = NLib_Temps_NFramerate_Construire( 30 ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Detruire client
			NClientVoiture_Client_NClientVoiture_Detruire( &out->m_client );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Construire la fenetre
		if( !( out->m_fenetre = NLib_Module_SDL_NFenetre_Construire( "UI",
			resolution,
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire framerate
			NLib_Temps_NFramerate_Detruire( &out->m_framerate );

			// Detruire client
			NClientVoiture_Client_NClientVoiture_Detruire( &out->m_client );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}
	}

	// Enregistrer
	out->m_configuration = configuration;

	// OK
	return out;
}

/**
 * Detruire l'UI
 *
 * @param this
 *		Cette instance
 */
void NClientVoiture_UI_NUIClient_Detruire( NUIClient **this )
{
	// Si l'UI est affichee
	if( NClientVoiture_Configuration_NConfiguration_EstUIActive( (*this)->m_configuration ) )
	{
		// Detruire le framerate
		NLib_Temps_NFramerate_Detruire( &(*this)->m_framerate );

		// Detruire la fenetre
		NLib_Module_SDL_NFenetre_Detruire( &(*this)->m_fenetre );

		// Detruire client
		NClientVoiture_Client_NClientVoiture_Detruire( &(*this)->m_client );
	}

	// Liberer
	NFREE( (*this) );
}

/**
 * Lancer l'UI
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NClientVoiture_UI_NUIClient_Lancer( NUIClient *this )
{
	// Evenement
	SDL_Event e;

	// Surface
	NSurface *surface;

	// Lancer
	this->m_estEnCours = NTRUE;

	// Boucle UI
	if( NClientVoiture_Configuration_NConfiguration_EstUIActive( this->m_configuration ) )
		do
		{
			// Debuter fonction
			NLib_Temps_NFramerate_DebuterFonction( this->m_framerate );

			// Nettoyer la fenetre
			NLib_Module_SDL_NFenetre_Nettoyer( this->m_fenetre );

			// Recuperer evenement
			SDL_PollEvent( &e );

			// Analyser l'evenement
			switch( e.type )
			{
				case SDL_QUIT:
					this->m_estEnCours = NFALSE;
					break;

				case SDL_KEYDOWN:
					switch( e.key.keysym.sym )
					{
						case SDLK_ESCAPE:
							this->m_estEnCours = NFALSE;
							break;

						default:
							break;

					}
					break;

				default:
					break;
			}

			// Proteger
			NClientVoiture_Client_NClientVoiture_ActiverProtection( this->m_client );

			// Obtenir surface
			surface = (NSurface*)NClientVoiture_Client_NClientVoiture_ObtenirTexture( this->m_client );

			// Afficher
			if( surface != NULL )
				NLib_Module_SDL_Surface_NSurface_Afficher( surface );

			// Ne plus proteger
			NClientVoiture_Client_NClientVoiture_DesactiverProtection( this->m_client );

			// Actualiser la fenetre
			NLib_Module_SDL_NFenetre_Update( this->m_fenetre );

			// Terminer fonction
			NLib_Temps_NFramerate_TerminerFonction( this->m_framerate );
		} while( this->m_estEnCours );
	else
		getchar( );

	// OK
	return NTRUE;
}

/**
 * Obtenir fenetre
 *
 * @param this
 *		Cette instance
 *
 * @return la fenetre
 */
const NFenetre *NClientVoiture_UI_NUIClient_ObtenirFenetre( const NUIClient *this )
{
	return this->m_fenetre;
}

#endif // !NCLIENTVOITURE_DESACTIVER_UI
