#include "../../include/NServeurVoiture.h"

// ------------------------------------------------
// struct NServeurVoiture::Serveur::NServeurVoiture
// ------------------------------------------------

/**
 * Obtenir client reception
 *
 * @param this
 *		Cette instance
 * @param client
 *		Le client au format NClientServeur
 *
 * @return le client ou NULL si introuvable
 */
__MUSTBEPROTECTED const NClientReception *NServeurVoiture_Serveur_NServeurVoiture_ObtenirClientReception( const NServeurVoiture *this,
	const NClientServeur *client )
{
    // Iterateur
    NU32 i = 0;

    // Sortie
    __OUTPUT const NClientReception *out;

    // Chercher
    for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_clientReception ); i++ )
		// Verifier
		if( NServeurVoiture_Serveur_NClientReception_ObtenirClient( out = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clientReception,
			i ) ) == client )
			return out;

	// Introuvable
	return NULL;
}

/**
 * Obtenir client emission
 *
 * @param this
 *		Cette instance
 * @param client
 *		Le client au format NClientServeur
 *
 * @return le client ou NULL si introuvable
 */
__MUSTBEPROTECTED const NClientEmission *NServeurVoiture_Serveur_NServeurVoiture_ObtenirClientEmission( const NServeurVoiture *this,
	const NClientServeur *client )
{
    // Iterateur
    NU32 i = 0;

    // Sortie
    __OUTPUT const NClientEmission *out;

    // Chercher
    for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_clientEmission ); i++ )
		// Verifier
		if( NServeurVoiture_Serveur_NClientEmission_ObtenirClient( out = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clientEmission,
			i ) ) == client )
			return out;

	// Introuvable
	return NULL;
}

/**
 * Callback de reception serveur emission
 *
 * @param client
 *		Le client
 * @param packet
 *		Le packet reçu
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NServeurVoiture_Serveur_NServeurVoiture_CallbackReceptionServeurEmission( const NClientServeur *client,
	const NPacket *packet )
{
	// Cette instance
	NServeurVoiture *this;

	// Client emission
	NClientEmission *clientEmission;

	// Obtenir l'instance
	this = (NServeurVoiture*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client );

	// Proteger les clients
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Recuperer le client
	if( !( clientEmission = (NClientEmission*)NServeurVoiture_Serveur_NServeurVoiture_ObtenirClientEmission( this,
		client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Desactiver protection
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NFALSE;
	}

	// Determiner type de client
	switch( NServeurVoiture_Serveur_NClientEmission_ObtenirTypeClient( clientEmission ) )
	{
		default:
		case NTYPE_CLIENT_EMISSION_INCONNU:
		case NTYPE_CLIENT_EMISSION_HTTP:
			if( !NServeurVoiture_Serveur_NClientEmission_RecevoirAuthentification( clientEmission,
				this->m_motDePasseStream,
				NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
				NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ),
				this ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_LOGIN_FAILED );

				// Desactiver protection
				NLib_Mutex_NMutex_Unlock( this->m_mutex );

				// Quitter
				return NFALSE;
			}
			break;

		case NTYPE_CLIENT_EMISSION_NPROJECT:
			if( !NServeurVoiture_Serveur_NClientEmission_RecevoirCommandeNProject( clientEmission,
				NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
				NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_LOGIN_FAILED );

				// Desactiver protection
				NLib_Mutex_NMutex_Unlock( this->m_mutex );

				// Quitter
				return NFALSE;
			}
			break;
	}

	// Desactiver protection
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

/**
 * Le callback de connexion d'un client serveur emission
 *
 * @param client
 *		Le client qui se connecte
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NServeurVoiture_Serveur_NServeurVoiture_CallbackConnexionServeurEmission( const NClientServeur *client )
{
	// Client emission
	NClientEmission *clientEmission;

	// Cette instance
	NServeurVoiture *this;

	// Obtenir l'instance
	this = (NServeurVoiture*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client );

	// Construire le client
	if( !( clientEmission = NServeurVoiture_Serveur_NClientEmission_Construire( client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Lock la liste
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Ajouter le client
	NLib_Memoire_NListe_Ajouter( this->m_clientEmission,
		clientEmission );

	// Unlock la liste
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

/**
 * Callback deconnexion serveur emission
 *
 * @param client
 *		Le client s'etant deconnecte
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NServeurVoiture_Serveur_NServeurVoiture_CallbackDeconnexionServeurEmission( const NClientServeur *client )
{
	// Iterateur
	NU32 i = 0;

	// Cette instance
	NServeurVoiture *this;

	// Obtenir l'instance
	this = (NServeurVoiture*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client );

	// Proteger
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Chercher
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_clientEmission ); i++ )
		if( NServeurVoiture_Serveur_NClientEmission_ObtenirClient( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clientEmission,
			i ) ) == client )
		{
			// Supprimer
			NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_clientEmission,
				i );

			// Quitter
			break;
		}

	// Ne plus proteger
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

/**
 * Callback de reception serveur reception
 *
 * @param client
 *		Le client
 * @param packet
 *		Le packet reçu
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NServeurVoiture_Serveur_NServeurVoiture_CallbackReceptionServeurReception( const NClientServeur *client,
	const NPacket *packet )
{
	// Cette instance
	NServeurVoiture *this;

	// Client reception
	NClientReception *clientReception;

	// Trame
	NTrame *trame;

	// Obtenir l'instance
	this = (NServeurVoiture*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client );

	// Construire la trame
	if( !( trame = NVoiture_Trame_NTrame_Construire2( NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Proteger les clients
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Obtenir le client reception
	if( !( clientReception = (NClientReception*)NServeurVoiture_Serveur_NServeurVoiture_ObtenirClientReception( this,
		client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Retirer protection client
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Detruire trame
		NVoiture_Trame_NTrame_Detruire( &trame );

		// Quitter
		return NFALSE;
	}

	// Traiter la trame
	switch( NVoiture_Trame_NTrame_ObtenirType( trame ) )
	{
		case NTYPE_TRAME_PARAMETRAGE_FLUX_VIDEO:
			// Authentifier le client
			if( !NServeurVoiture_Serveur_NClientReception_Authentifier( clientReception,
				trame ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_LOGIN_FAILED );

				// Retirer protection client
				NLib_Mutex_NMutex_Unlock( this->m_mutex );

				// Detruire trame
				NVoiture_Trame_NTrame_Detruire( &trame );

				// Quitter
				return NFALSE;
			}
			break;

		case NTYPE_TRAME_TRANSMISSION_IMAGE:
			// Recevoir image
			if( !NServeurVoiture_Serveur_NClientReception_RecevoirImage( clientReception,
				trame ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_LOGIN_FAILED );

				// Retirer protection client
				NLib_Mutex_NMutex_Unlock( this->m_mutex );

				// Detruire trame
				NVoiture_Trame_NTrame_Detruire( &trame );

				// Quitter
				return NFALSE;
			}
			break;

		default:
			break;
	}

	// Desativer protection
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// Detruire la trame
	NVoiture_Trame_NTrame_Detruire( &trame );

	// OK
	return NTRUE;
}

/**
 * Le callback de connexion d'un client serveur reception
 *
 * @param client
 *		Le client qui se connecte
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NServeurVoiture_Serveur_NServeurVoiture_CallbackConnexionServeurReception( const NClientServeur *client )
{
	// Client reception
	NClientReception *clientReception;

	// Cette instance
	NServeurVoiture *this;

	// Obtenir l'instance
	this = (NServeurVoiture*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client );

	// Construire le client
	if( !( clientReception = NServeurVoiture_Serveur_NClientReception_Construire( client,
		this->m_clientEmission,
		this->m_dernierIdentifiantClientReception++,
		this->m_tailleMaximaleFichierFlux,
		this->m_nombreMaximumFichierParFlux,
		this->m_emplacementSauvegarde,
		this->m_nombreFrameSkippe ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Lock la liste
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Ajouter le client
	NLib_Memoire_NListe_Ajouter( this->m_clientReception,
		clientReception );

	// Unlock la liste
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

/**
 * Callback deconnexion serveur reception
 *
 * @param client
 *		Le client s'etant deconnecte
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NServeurVoiture_Serveur_NServeurVoiture_CallbackDeconnexionServeurReception( const NClientServeur *client )
{
	// Iterateur
	NU32 i = 0;

	// Cette instance
	NServeurVoiture *this;

	// Obtenir l'instance
	this = (NServeurVoiture*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client );

	// Lock la liste
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Chercher
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_clientReception ); i++ )
		if( NServeurVoiture_Serveur_NClientReception_ObtenirClient( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clientReception,
			i ) ) == client )
		{
			// Supprimer
			NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_clientReception,
				i );

			// Quitter
			break;
		}

	// Unlock la liste
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

/**
 * Thread de mise a jour
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
__THREAD NBOOL NServeurVoiture_Serveur_NServeurVoiture_ThreadUpdate( NServeurVoiture *this )
{
	// Update
	do
	{


		// Attendre
		NLib_Temps_Attendre( this->m_delaiEntreUpdate );
	} while( this->m_estEnCours );

	// OK
	return NTRUE;
}

/**
 * Construire le serveur
 *
 * @param portEcoute
 *		Le port d'ecoute des entrees video
 * @param portEmission
 *		Le port d'emission des sources vers le clients
 * @param delaiEntreUpdate
 *		Le delai entre chaque mise a jour du serveur
 * @param tailleMaximaleFichier
 *		La taille maximale d'un fichier de flux
 * @param nombreMaximumFichier
 *		Le nombre maximum de fichiers conserves pour un flux
 * @param emplacementSauvegarde
 *		L'emplacement de sauvegarde des fichiers perimes (NULL pour ne pas les conserver)
 * @param motDePasseStream
 *		Le mot de passe pour acceder au stream
 * @param nombreDeFrameIgnoreEnregistrement
 *		Le nombre de frame skippees lors de l'enregistrement
 *
 * @return l'instance du serveur
 */
__ALLOC NServeurVoiture *NServeurVoiture_Serveur_NServeurVoiture_Construire( NU32 portEcoute,
	NU32 portEmission,
	NU32 delaiEntreUpdate,
	NU32 tailleMaximaleFichier,
	NU32 nombreMaximumFichier,
	const char *emplacementSauvegarde,
	const char *motDePasseStream,
	NU32 nombreDeFrameIgnoreEnregistrement )
{
	// Sortie
	__OUTPUT NServeurVoiture *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NServeurVoiture ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_delaiEntreUpdate = delaiEntreUpdate;
	out->m_tailleMaximaleFichierFlux = tailleMaximaleFichier;
	out->m_nombreMaximumFichierParFlux = nombreMaximumFichier;
	out->m_nombreFrameSkippe = nombreDeFrameIgnoreEnregistrement;
	out->m_emplacementSauvegarde = NLib_Chaine_Comparer( emplacementSauvegarde,
		NSERVEURVOITURE_EMPLACEMENT_SAUVEGARDE_DESACTIVE,
		NFALSE,
		0 ) ?
			NULL
			: emplacementSauvegarde;
	out->m_motDePasseStream = motDePasseStream;

	// Construire mutex
	if( !( out->m_mutex = NLib_Mutex_NMutex_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

    // Construire les listes de clients
		// Emission
			if( !( out->m_clientEmission = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NServeurVoiture_Serveur_NClientEmission_Detruire ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Detruire mutex
				NLib_Mutex_NMutex_Detruire( &out->m_mutex );

				// Liberer
				NFREE( out );

				// Quitter
				return NULL;
			}
		// Reception
			if( !( out->m_clientReception = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NServeurVoiture_Serveur_NClientReception_Detruire ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Detruire liste clients
				NLib_Memoire_NListe_Detruire( &out->m_clientEmission );

				// Detruire mutex
				NLib_Mutex_NMutex_Detruire( &out->m_mutex );

				// Liberer
				NFREE( out );

				// Quitter
				return NULL;
			}

    // Construire serveur emission
    if( !( out->m_serveurEmission = NLib_Module_Reseau_Serveur_NServeur_Construire( portEmission,
		NServeurVoiture_Serveur_NServeurVoiture_CallbackReceptionServeurEmission,
		NServeurVoiture_Serveur_NServeurVoiture_CallbackConnexionServeurEmission,
		NServeurVoiture_Serveur_NServeurVoiture_CallbackDeconnexionServeurEmission,
		out,
		NSERVEURVOITURE_TIMEOUT_SERVEUR_EMISSION,
		NTYPE_SERVEUR_TCP,
		NMETHODE_TRANSMISSION_PACKETIO_RAW ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire listes des clients
		NLib_Memoire_NListe_Detruire( &out->m_clientEmission );
		NLib_Memoire_NListe_Detruire( &out->m_clientReception );

		// Detruire mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire serveur reception
    if( !( out->m_serveurReception = NLib_Module_Reseau_Serveur_NServeur_Construire( portEcoute,
		NServeurVoiture_Serveur_NServeurVoiture_CallbackReceptionServeurReception,
		NServeurVoiture_Serveur_NServeurVoiture_CallbackConnexionServeurReception,
		NServeurVoiture_Serveur_NServeurVoiture_CallbackDeconnexionServeurReception,
		out,
		0,
		NTYPE_SERVEUR_TCP,
		NMETHODE_TRANSMISSION_PACKETIO_NPROJECT ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire serveur emission
		NLib_Module_Reseau_Serveur_NServeur_Detruire( &out->m_serveurEmission );

		// Detruire listes des clients
		NLib_Memoire_NListe_Detruire( &out->m_clientEmission );
		NLib_Memoire_NListe_Detruire( &out->m_clientReception );

		// Detruire mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Lancer le thread
	out->m_estEnCours = NTRUE;

	// Construire le thread d'update
	if( !( out->m_threadUpdate = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NServeurVoiture_Serveur_NServeurVoiture_ThreadUpdate,
		out,
		&out->m_estEnCours ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire serveur reception
		NLib_Module_Reseau_Serveur_NServeur_Detruire( &out->m_serveurReception );

		// Detruire serveur emission
		NLib_Module_Reseau_Serveur_NServeur_Detruire( &out->m_serveurEmission );

		// Detruire listes des clients
		NLib_Memoire_NListe_Detruire( &out->m_clientEmission );
		NLib_Memoire_NListe_Detruire( &out->m_clientReception );

		// Detruire mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire le serveur
 *
 * @param this
 *		Cette instance
 */
void NServeurVoiture_Serveur_NServeurVoiture_Detruire( NServeurVoiture **this )
{
	// Detruire thread update
	NLib_Thread_NThread_Detruire( &(*this)->m_threadUpdate );

	// Detruire serveurs
	NLib_Module_Reseau_Serveur_NServeur_Detruire( &(*this)->m_serveurReception );
	NLib_Module_Reseau_Serveur_NServeur_Detruire( &(*this)->m_serveurEmission );

	// Detruire listes des clients
	NLib_Memoire_NListe_Detruire( &(*this)->m_clientEmission );
	NLib_Memoire_NListe_Detruire( &(*this)->m_clientReception );

	// Detruire mutex
	NLib_Mutex_NMutex_Detruire( &(*this)->m_mutex );

	// Liberer
	NFREE( (*this) );
}

/**
 * Corriger flux
 *
 * @param this
 *		Cette instance
 * @param flux
 *		Le flux a corriger
 *
 * @return si l'operation s'est bien passee
 */
__MUSTBEPROTECTED NBOOL NServeurVoiture_Serveur_NServeurVoiture_CorrigerFlux( NServeurVoiture *this,
	__OUTPUT NU32 *flux )
{
	// Iterateur
	NU32 i = 0;

	// Chercher le flux
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_clientReception ); i++ )
		// Ce flux est correct?
		if( NServeurVoiture_Serveur_NClientReception_ObtenirIdentifiant( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clientReception,
			i ) ) == *flux )
			// C'est bon!
			return NTRUE;

	// Corriger
	*flux = NServeurVoiture_Serveur_NClientReception_ObtenirIdentifiant( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clientReception,
		0 ) );

	// OK
	return NTRUE;
}

/**
 * Obtenir client reception
 *
 * @param this
 *		Cette instance
 * @param identifiant
 *		L'identifiant du client
 *
 * @return le client reception
 */
__MUSTBEPROTECTED const NClientReception *NServeurVoiture_Serveur_NServeurVoiture_ObtenirClientReceptionDepuisIdentifiant( const NServeurVoiture *this,
	NU32 identifiant )
{
	// Iterateur
	NU32 i = 0;

	// Chercher
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_clientReception ); i++ )
		if( NServeurVoiture_Serveur_NClientReception_ObtenirIdentifiant( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clientReception,
			i ) ) == identifiant )
			return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clientReception,
				i );

	// Introuvable
	return NULL;
}

/**
 * Obtenir client reception
 *
 * @param this
 *		Cette instance
 * @param index
 *		L'index du client a obtenir
 *
 * @return le client
 */
__MUSTBEPROTECTED const NClientReception *NServeurVoiture_Serveur_NServeurVoiture_ObtenirClientReceptionDepuisIndex( const NServeurVoiture *this,
	NU32 index )
{
	// Verifier
	if( index >= NLib_Memoire_NListe_ObtenirNombre( this->m_clientReception ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// OK
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clientReception,
		index );
}

/**
 * Obtenir le nombre de client reception
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de clients en reception
 */
__MUSTBEPROTECTED NU32 NServeurVoiture_Serveur_NServeurVoiture_ObtenirNombreClientReception( const NServeurVoiture *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_clientReception );
}

