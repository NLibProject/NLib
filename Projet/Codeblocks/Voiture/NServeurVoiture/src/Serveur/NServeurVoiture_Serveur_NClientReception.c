#include "../../include/NServeurVoiture.h"

// -------------------------------------------------
// struct NServeurVoiture::Serveur::NClientReception
// -------------------------------------------------

/**
 * Construire le client
 *
 * @param client
 *		Le client
 * @param listeClientEmission
 *		La liste des clients en emission
 * @param identifiant
 *		L'identifiant unique du client
 * @param tailleMaximaleFichier
 *		La taille maximale d'un fichier de flux
 * @param nombreMaximumFichier
 *		Le nombre maximum de fichier de flux
 * @param emplacementSauvegarde
 *		L'emplacement de sauvegarde du fichier perime
 * @param nombreFrameSkip
 *		Le nombre de frames ignorees a l'enregistrement
 *
 * @return l'instance du client
 */
__ALLOC NClientReception *NServeurVoiture_Serveur_NClientReception_Construire( const NClientServeur *client,
	NListe *listeClientEmission,
	NU32 identifiant,
	NU32 tailleMaximaleFichier,
	NU32 nombreMaximumFichier,
	const char *emplacementSauvegarde,
	NU32 nombreFrameSkip )
{
    // Sortie
    __OUTPUT NClientReception *out;

    // Allouer la memoire
    if( !( out = calloc( 1,
		sizeof( NClientReception ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_client = (NClientServeur*)client;
	out->m_identifiant = identifiant;
	out->m_tailleMaximaleFichierFlux = tailleMaximaleFichier;
	out->m_nombreMaximumFichierParFlux = nombreMaximumFichier;
	out->m_emplacementSauvegarde = emplacementSauvegarde;
	out->m_listeClientEmission = listeClientEmission;
	out->m_nombreFrameASkip = nombreFrameSkip;

	// OK
	return out;
}

/**
 * Detruire le client
 *
 * @param this
 * 		Cette instance
 */
void NServeurVoiture_Serveur_NClientReception_Detruire( NClientReception **this )
{
	// Detruire le flux
	if( (*this)->m_estAuthentifie )
		NLib_Module_MPEGWriter_NMPEGWriter_Detruire( &(*this)->m_writerMPEG );

	// Liberer
	NFREE( (*this)->m_nomCapture );
	NFREE( *this );
}

/**
 * Authentifier le client
 *
 * @param this
 *		Cette instance
 * @param trame
 *		La trame reçue
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NServeurVoiture_Serveur_NClientReception_Authentifier( NClientReception *this,
	const NTrame *trame )
{
	// Lecteur de donnees
	NLecteurMemoire *lecteur;

	// Taille nom
	NU32 tailleNom;

	// Buffer
	char message[ 2048 ];

	// Verifier etat/flux
	if( this->m_estAuthentifie )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Construire le lecteur
	if( !( lecteur = NLib_Memoire_NLecteurMemoire_Construire( NVoiture_Trame_NTrame_ObtenirData( trame ),
		NVoiture_Trame_NTrame_ObtenirTailleData( trame ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	/** Lire donnees **/
	// Frequence
	this->m_frequence = NLib_Memoire_NLecteurMemoire_Lire2( lecteur );

	// Taille nom
	tailleNom = NLib_Memoire_NLecteurMemoire_Lire2( lecteur );

	// Nom
	if( !( this->m_nomCapture = NLib_Memoire_NLecteurMemoire_LireCopieValeur( lecteur,
		tailleNom,
		NTRUE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Resolution X
	this->m_resolution.x = NLib_Memoire_NLecteurMemoire_Lire2( lecteur );

	// Resolution Y
	this->m_resolution.y = NLib_Memoire_NLecteurMemoire_Lire2( lecteur );

	// BPP
	this->m_bpp = NLib_Memoire_NLecteurMemoire_Lire2( lecteur );

	// Detruire le lecteur
	NLib_Memoire_NLecteurMemoire_Detruire( &lecteur );

	// Trouver un nom du fichier libre
	this->m_writerMPEG = NVoiture_Camera_CreerWriterMPEG( this->m_nomCapture,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( this->m_client ),
		this->m_identifiant,
		this->m_tailleMaximaleFichierFlux,
		this->m_nombreMaximumFichierParFlux,
		this->m_emplacementSauvegarde,
		&this->m_indexFlux,
		&this->m_dernierFichierSupprime );

	// Prepare message
	snprintf( message,
		2048,
		"[%s]\tAuthentifie avec %dx%d[%dBPP] a %dfps (\"%s\")",
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( this->m_client ),
		this->m_resolution.x,
		this->m_resolution.y,
		this->m_bpp,
		this->m_frequence,
		this->m_nomCapture );

	// Notifier
	NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
		message,
		0 );

	// OK
	return this->m_estAuthentifie = NTRUE;
}

/**
 * Diffuser la frame MPEG
 *
 * @param this
 *		Cette instance
 * @param frame
 *		La frame MPEG a diffuser
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NServeurVoiture_Serveur_NClientReception_DiffuserTrameMPEG( NClientReception *this,
	const NFrameMPEG *frame )
{
	// Iterateur
	NU32 i = 0;

	// Client emission
	NClientEmission *clientEmission;

	// Packet
	NPacket *packet;

	// Verifier
	if( NLib_Memoire_NListe_ObtenirNombre( this->m_listeClientEmission ) <= 0 )
		return NTRUE;

	// Creer le packet
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire3( NLib_Module_MPEGWriter_NFrameMPEG_ObtenirData( frame ),
		NLib_Module_MPEGWriter_NFrameMPEG_ObtenirTaille( frame ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Parcourir les clients
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_listeClientEmission ); i++ )
	{
		// Client emission
		clientEmission = (NClientEmission*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_listeClientEmission,
			i );

		// Verifier qu'il s'agisse du bon type de client
		switch( NServeurVoiture_Serveur_NClientEmission_ObtenirTypeClient( clientEmission ) )
		{
			// Type http
			case NTYPE_CLIENT_EMISSION_HTTP:
				break;

			// Autre client reçoit frame brute
			default:
				continue;
		}

		// Envoyer si le flux est celui demande?
		if( NServeurVoiture_Serveur_NClientEmission_EstAuthentifie( clientEmission )
			&& this->m_identifiant == NServeurVoiture_Serveur_NClientEmission_ObtenirFlux( clientEmission ) )
			NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacketCopie( (NClientServeur*)NServeurVoiture_Serveur_NClientEmission_ObtenirClient( clientEmission ),
				packet );
	}

	// Detruire le packet
	NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

	// OK
	return NTRUE;
}

/**
 * Diffuser la frame brute
 *
 * @param this
 *		Cette instance
 * @param trame
 *		La trame brute
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NServeurVoiture_Serveur_NClientReception_DiffuserTrameBrute( NClientReception *this,
	const NTrame *trame )
{
	// Iterateur
	NU32 i = 0;

	// Client emission
	NClientEmission *clientEmission;

	// Packet
	NPacket *packet;

	// Donnees trame
	char *data;

	// Verifier
	if( NLib_Memoire_NListe_ObtenirNombre( this->m_listeClientEmission ) <= 0 )
		return NTRUE;

	// Creer le packet
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Exporter trame
	if( !( data = NVoiture_Trame_NTrame_Exporter( trame ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_EXPORT_FAILED );

		// Detruire packet
		NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

		// Quitter
		return NFALSE;
	}

	// Ajouter trame
	if( !NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		data,
		NVoiture_Trame_NTrame_ObtenirTailleExport( trame ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Liberer
		NFREE( data );

		// Detruire packet
		NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

		// Quitter
		return NFALSE;
    }

    // Liberer
    NFREE( data );

	// Parcourir les clients
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_listeClientEmission ); i++ )
	{
		// Client emission
		clientEmission = (NClientEmission*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_listeClientEmission,
			i );

		// Verifier qu'il s'agisse du bon type de client
		switch( NServeurVoiture_Serveur_NClientEmission_ObtenirTypeClient( clientEmission ) )
		{
			// Type nproject
			case NTYPE_CLIENT_EMISSION_NPROJECT:
				break;

			// Autre client reçoit frame brute
			default:
				continue;
		}

		// Envoyer si le flux est celui demande?
		if( NServeurVoiture_Serveur_NClientEmission_EstAuthentifie( clientEmission )
			&& this->m_identifiant == NServeurVoiture_Serveur_NClientEmission_ObtenirFlux( clientEmission ) )
			NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacketCopie( (NClientServeur*)NServeurVoiture_Serveur_NClientEmission_ObtenirClient( clientEmission ),
				packet );
	}

	// Detruire le packet
	NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

	// OK
	return NTRUE;
}

/**
 * Recevoir une image
 *
 * @param this
 *		Cette instance
 * @param trame
 *		La trame reçue
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NServeurVoiture_Serveur_NClientReception_RecevoirImage( NClientReception *this,
	const NTrame *trame )
{
	// Resolution
	NUPoint resolution;

	// BPP
    NU32 bpp;

    // Message
    char message[ 2048 ];

    // Frame
    NFrameMPEG *frame;

    // Verifier la taille
    if( NVoiture_Trame_NTrame_ObtenirTailleData( trame ) < sizeof( NU32 ) * 3 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

    // Lire les donnees
    resolution.x = *( (NU32*)( NVoiture_Trame_NTrame_ObtenirData( trame ) ) );
	resolution.y = *( (NU32*)( NVoiture_Trame_NTrame_ObtenirData( trame ) + sizeof( NU32 ) ) );
	bpp = *( (NU32*)( NVoiture_Trame_NTrame_ObtenirData( trame ) + sizeof( NU32 ) * 2 ) );

	// Verifier
	if( bpp != 3
		|| NVoiture_Trame_NTrame_ObtenirTailleData( trame ) < ( resolution.x * resolution.y * bpp ) + sizeof( NU32 ) * 3 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INCORRECT_SIZE );

		// Quitter
		return NFALSE;
	}

	// Index correct pour enregistrer?
	if( this->m_indexFrame++ >= this->m_nombreFrameASkip )
	{
		// Verifier la taille du fichier
		if( NLib_Module_MPEGWriter_ObtenirTaille( this->m_writerMPEG ) >= this->m_tailleMaximaleFichierFlux )
		{
			// Creer le message
			snprintf( message,
				2048,
				"Fichier plein (%1.2fmo), changement du flux pour [%s:%s]...",
				(float)this->m_tailleMaximaleFichierFlux / 1000000.0f,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( this->m_client ),
				this->m_nomCapture );

			// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
				message,
				0 );

			// Detruire l'ancien flux
			NLib_Module_MPEGWriter_NMPEGWriter_Detruire( &this->m_writerMPEG );

			// Creer le nouveau flux
			this->m_writerMPEG = NVoiture_Camera_CreerWriterMPEG( this->m_nomCapture,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( this->m_client ),
				this->m_identifiant,
				this->m_tailleMaximaleFichierFlux,
                this->m_nombreMaximumFichierParFlux,
                this->m_emplacementSauvegarde,
                &this->m_indexFlux,
                &this->m_dernierFichierSupprime );
		}

		// Inscrire la frame
		NLib_Module_MPEGWriter_NMPEGWriter_Inscrire( this->m_writerMPEG,
			(NU8*)( NVoiture_Trame_NTrame_ObtenirData( trame ) + sizeof( NU32 ) * 3 ),
			resolution.x,
			resolution.y,
			this->m_frequence );

		// Lire la frame inscrite
		if( !( frame = NLib_Module_MPEGWriter_ObtenirDerniereFrame( this->m_writerMPEG ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_EXPORT_FAILED );

			// Quitter
			return NFALSE;
		}

		// Diffuser la trame MPEG
		NServeurVoiture_Serveur_NClientReception_DiffuserTrameMPEG( this,
			frame );

		// Detruire la frame
		NLib_Module_MPEGWriter_NFrameMPEG_Detruire( &frame );

		// Remettre a zero
		this->m_indexFrame = 0;
	}

	// Diffuser la trame brute
	NServeurVoiture_Serveur_NClientReception_DiffuserTrameBrute( this,
		trame );

	// OK
	return NTRUE;
}

/**
 * Obtenir client
 *
 * @param this
 *		Cette instance
 *
 * @return le client
 */
const NClientServeur *NServeurVoiture_Serveur_NClientReception_ObtenirClient( const NClientReception *this )
{
	return this->m_client;
}

/**
 * Obtenir l'identifiant
 *
 * @param this
 *		Cette instance
 *
 * @return l'identifiant
 */
NU32 NServeurVoiture_Serveur_NClientReception_ObtenirIdentifiant( const NClientReception *this )
{
	return this->m_identifiant;
}

/**
 * Obtenir frequence
 *
 * @param this
 *		Cette instance
 *
 * @return la frequence
 */
NU32 NServeurVoiture_Serveur_NClientReception_ObtenirFrequence( const NClientReception *this )
{
	return this->m_frequence;
}

/**
 * Obtenir le nom de la capture
 *
 * @param this
 *		Cette instance
 *
 * @return le nom de la capture
 */
const char *NServeurVoiture_Serveur_NClientReception_ObtenirNomCapture( const NClientReception *this )
{
	return this->m_nomCapture;
}

/**
 * Obtenir profondeur
 *
 * @param this
 *		Cette instance
 *
 * @return la profondeur de l'image
 */
NU32 NServeurVoiture_Serveur_NClientReception_ObtenirProfondeur( const NClientReception *this )
{
	return this->m_bpp;
}

/**
 * Obtenir la resolution
 *
 * @param this
 *		Cette instance
 *
 * @return la resolution
 */
const NUPoint *NServeurVoiture_Serveur_NClientReception_ObtenirResolution( const NClientReception *this )
{
	return &this->m_resolution;
}
