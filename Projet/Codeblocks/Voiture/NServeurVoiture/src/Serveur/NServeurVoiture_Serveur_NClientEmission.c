#include "../../include/NServeurVoiture.h"

// ------------------------------------------------
// struct NServeurVoiture::Serveur::NClientEmission
// ------------------------------------------------

/**
 * Construire le client
 *
 * @param client
 *		Le client
 *
 * @return l'instance du client
 */
__ALLOC NClientEmission *NServeurVoiture_Serveur_NClientEmission_Construire( const NClientServeur *client )
{
    // Sortie
    __OUTPUT NClientEmission *out;

    // Allouer la memoire
    if( !( out = calloc( 1,
		sizeof( NClientEmission ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_client = (NClientServeur*)client;

	// OK
	return out;
}

/**
 * Detruire le client
 *
 * @param this
 * 		Cette instance
 */
void NServeurVoiture_Serveur_NClientEmission_Detruire( NClientEmission **this )
{
	NFREE( *this );
}

/**
 * Obtenir client
 *
 * @param this
 *		Cette instance
 *
 * @return le client
 */
const NClientServeur *NServeurVoiture_Serveur_NClientEmission_ObtenirClient( const NClientEmission *this )
{
	return this->m_client;
}

/**
 * Envoyer packet authentification HTTP
 *
 * @param this
 *		Cette instance
 * @param estAuthentificationReussie
 *		L'authentification a reussi?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NServeurVoiture_Serveur_NClientEmission_EnvoyerPacketAuthentificationHTTP( NClientEmission *this,
	NBOOL estAuthentificationReussie )
{
	// Packet
	NPacket *packet;

#define NSERVEURVOITURE_SERVEUR_NSERVEURVOITURE_PACKET_HTTP_VALIDATION_STREAM \
	"HTTP/1.1 200 OK\r\n" \
	"\n" \
	"Content-Type: video/mpeg\r\n" \
	"Connection: keep-alive\r\n"

#define NSERVEURVOITURE_SERVEUR_NSERVEURVOITURE_PACKET_HTTP_REFUS_STREAM \
	"HTTP/1.1 401 Unauthorized\r\n" \
	"\n"

	// Creer packet
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire3( ( estAuthentificationReussie ?
		NSERVEURVOITURE_SERVEUR_NSERVEURVOITURE_PACKET_HTTP_VALIDATION_STREAM
		: NSERVEURVOITURE_SERVEUR_NSERVEURVOITURE_PACKET_HTTP_REFUS_STREAM ),
		strlen( ( estAuthentificationReussie ?
			NSERVEURVOITURE_SERVEUR_NSERVEURVOITURE_PACKET_HTTP_VALIDATION_STREAM
			: NSERVEURVOITURE_SERVEUR_NSERVEURVOITURE_PACKET_HTTP_REFUS_STREAM ) ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Ajouter packet
	NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( this->m_client,
		packet );

	// Le flux est en erreur?
	if( !estAuthentificationReussie )
		// Attendre que le packet soit parti
		while( NLib_Module_Reseau_Serveur_NClientServeur_EstPacketsDansCache( this->m_client ) )
			NLib_Temps_Attendre( 1 );

	// OK
	return this->m_estAuthentifie = estAuthentificationReussie;
}

/**
 * Envoyer authentification NProject
 *
 * @param this
 *		Cette instance
 * @param serveurVoiture
 *		Le serveur de la voiture
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NServeurVoiture_Serveur_NClientEmission_EnvoyerPacketAuthentificationNProject( NClientEmission *this,
	NServeurVoiture *serveurVoiture )
{
	// Client reception
	const NClientReception *clientReception;

	// Packet
	NPacket *packet;

	// Buffer
	NU32 buffer;

	// Recuperer client reception
	if( !( clientReception = NServeurVoiture_Serveur_NServeurVoiture_ObtenirClientReceptionDepuisIdentifiant( serveurVoiture,
		this->m_identifiantFlux ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Construire packet
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Ajouter type
	buffer = NTYPE_TRAME_PARAMETRAGE_FLUX_VIDEO;
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&buffer,
		sizeof( NU32 ) );

	// Ajouter frequence
	buffer = NServeurVoiture_Serveur_NClientReception_ObtenirFrequence( clientReception );
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&buffer,
		sizeof( NU32 ) );

	// Ajouter taille nom capture
	buffer = strlen( NServeurVoiture_Serveur_NClientReception_ObtenirNomCapture( clientReception ) );
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&buffer,
		sizeof( NU32 ) );

	// Ajouter le nom de la capture
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		NServeurVoiture_Serveur_NClientReception_ObtenirNomCapture( clientReception ),
		strlen( NServeurVoiture_Serveur_NClientReception_ObtenirNomCapture( clientReception ) ) );

	// Ajouter resolution x
	buffer = NServeurVoiture_Serveur_NClientReception_ObtenirResolution( clientReception )->x;
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&buffer,
		sizeof( NU32 ) );

	// Ajouter resolution y
	buffer = NServeurVoiture_Serveur_NClientReception_ObtenirResolution( clientReception )->y;
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&buffer,
		sizeof( NU32 ) );

	// Ajouter bpp
	buffer = NServeurVoiture_Serveur_NClientReception_ObtenirProfondeur( clientReception );
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&buffer,
		sizeof( NU32 ) );

	// Envoyer le packet
	NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( this->m_client,
		packet );

	// Attendre que le cache soit vide
	while( NLib_Module_Reseau_Serveur_NClientServeur_EstPacketsDansCache( this->m_client ) )
		// Attendre
		NLib_Temps_Attendre( 1 );

	// OK
	return NTRUE;
}

/**
 * Authentifier
 *
 * @param this
 *		Cette instance
 * @param motDePasse
 *		Le mot de passe pour acceder au flux
 * @param data
 *		Les donnees reçues
 * @param tailleData
 *		La taille des donnees reçues
 * @param serveurVoiture
 *		Le serveur voiture
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NServeurVoiture_Serveur_NClientEmission_RecevoirAuthentification( NClientEmission *this,
	const char *motDePasse,
	const char *data,
	NU32 tailleData,
	void *serveurVoiture )
{
	// Curseur
	NU32 curseur = 0;

	// Chaîne
	char *chaine;

	// Taille mot de passe
	NU32 tailleMotDePasseLu;

	// Mot de passe lu
	char *motDePasseLu;

	// Lecteur memoire
	NLecteurMemoire *lecteur;

	// Index
	char *index;

	// Flux HTTP?
	if( tailleData >= 3 )
	{
		// Creer chaine avec \0
		if( !( chaine = calloc( tailleData + 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Envoyer erreur
			NServeurVoiture_Serveur_NClientEmission_EnvoyerPacketAuthentificationHTTP( this,
				NFALSE );

			// Quitter
			return NFALSE;
		}

		// Copier
		memcpy( chaine,
			data,
			tailleData );

		// Packet HTTP?
        if( !memcmp( chaine,
			"GET",
			3 ) )
		{
			// Positionner
			curseur = 3;

			// Verifier mot de passe
				// Positionner
					if( !NLib_Chaine_PlacerALaChaine( chaine,
						"password=",
						&curseur ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

						// Envoyer erreur
						NServeurVoiture_Serveur_NClientEmission_EnvoyerPacketAuthentificationHTTP( this,
							NFALSE );

						// Liberer
						NFREE( chaine );

						// Quitter
						return NFALSE;
					}
				// Lire
					if( !( motDePasseLu = NLib_Chaine_LireJusqua( chaine,
						'&',
						&curseur,
						NFALSE ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

						// Envoyer erreur
						NServeurVoiture_Serveur_NClientEmission_EnvoyerPacketAuthentificationHTTP( this,
							NFALSE );

						// Liberer
						NFREE( chaine );

						// Quitter
						return NFALSE;
					}
				// Comparer
					if( !NLib_Chaine_Comparer( motDePasseLu,
						motDePasse,
						NTRUE,
						0 ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

						// Liberer
						NFREE( motDePasseLu );

						// Envoyer erreur
						NServeurVoiture_Serveur_NClientEmission_EnvoyerPacketAuthentificationHTTP( this,
							NFALSE );

						// Liberer
						NFREE( chaine );

						// Quitter
						return NFALSE;
					}
				// Liberer mot de passe
					NFREE( motDePasseLu );

			// Lire index flux
				// Positionner
                    if( !NLib_Chaine_PlacerALaChaine( chaine,
						"index=",
						&curseur ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

						// Envoyer erreur
						NServeurVoiture_Serveur_NClientEmission_EnvoyerPacketAuthentificationHTTP( this,
							NFALSE );

						// Liberer
						NFREE( chaine );

						// Quitter
						return NFALSE;
					}
				// Lire
					if( !( index = NLib_Chaine_LireJusqua( chaine,
						'\n',
						&curseur,
						NFALSE ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

						// Envoyer erreur
						NServeurVoiture_Serveur_NClientEmission_EnvoyerPacketAuthentificationHTTP( this,
							NFALSE );

						// Liberer
						NFREE( chaine );

						// Quitter
						return NFALSE;
					}
				// Supprimer carriage-return
					NLib_Chaine_Remplacer( index,
						'\r',
						'\0' );
				// Enregistrer
					this->m_identifiantFlux = strtol( index,
                        NULL,
                        10 );
                // Liberer
					NFREE( index );

			// Corriger le flux
			NServeurVoiture_Serveur_NServeurVoiture_CorrigerFlux( serveurVoiture,
				&this->m_identifiantFlux );

			// Enregistrer le type
			this->m_type = NTYPE_CLIENT_EMISSION_HTTP;

			// Envoyer resultat positif
			NServeurVoiture_Serveur_NClientEmission_EnvoyerPacketAuthentificationHTTP( this,
				NTRUE );

			// Le client demande un reset?
			if( strstr( chaine,
				"reset" ) != NULL )
				// Redemarrer le raspberry
				system( "sudo shutdown -r now" );

			// OK
			return NTRUE;
		}

		// Liberer
		NFREE( chaine );
	}

	// Corriger le flux
	NServeurVoiture_Serveur_NServeurVoiture_CorrigerFlux( serveurVoiture,
		&this->m_identifiantFlux );

	// Flux NProject
		// Creer lecteur memoire
			if( !( lecteur = NLib_Memoire_NLecteurMemoire_Construire( data,
				tailleData ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Quitter
				return NFALSE;
			}
		// Verifier le type de packet
			if( NLib_Memoire_NLecteurMemoire_Lire2( lecteur ) != NTYPE_TRAME_AUTHENTIFICATION_CLIENT )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

				// Detruire lecteur
				NLib_Memoire_NLecteurMemoire_Detruire( &lecteur );

				// Quitter
				return NFALSE;
			}
		// Lire taille mot de passe
			tailleMotDePasseLu = NLib_Memoire_NLecteurMemoire_Lire2( lecteur );
		// Lire mot de passe
			if( !( motDePasseLu = (char*)NLib_Memoire_NLecteurMemoire_Lire( lecteur,
				tailleMotDePasseLu ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

				// Detruire lecteur
				NLib_Memoire_NLecteurMemoire_Detruire( &lecteur );

				// Quitter
				return NFALSE;
			}
		// Detruire lecteur memoire
			NLib_Memoire_NLecteurMemoire_Detruire( &lecteur );
		// Verifier mot de passe
			if( !NLib_Chaine_Comparer( motDePasse,
				motDePasseLu,
				NTRUE,
				0 ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

				// Quitter
				return NFALSE;
			}

	// Enregistrer le type
	this->m_type = NTYPE_CLIENT_EMISSION_NPROJECT;

	// Envoyer packet d'authentification NProject
	NServeurVoiture_Serveur_NClientEmission_EnvoyerPacketAuthentificationNProject( this,
		serveurVoiture );

	// OK
	return this->m_estAuthentifie = NTRUE;
}

/**
 * Recevoir packet de commande si client NProject
 *
 * @param this
 *		Cette instance
 * @param data
 *		Les donnees
 * @param tailleData
 *		La taille des donnees
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NServeurVoiture_Serveur_NClientEmission_RecevoirCommandeNProject( NClientEmission *this,
	const char *data,
	NU32 tailleData )
{
    // OK
    return NTRUE;
}

/**
 * Changer de flux
 *
 * @param this
 *		Cette instance
 * @param flux
 *		Le flux
 */
void NServeurVoiture_Serveur_NClientEmission_ChangerFlux( NClientEmission *this,
	NU32 flux )
{
	// Enregistrer
	this->m_identifiantFlux = flux;
}

/**
 * Obtenir flux
 *
 * @param this
 *		Cette instance
 *
 * @return le flux
 */
NU32 NServeurVoiture_Serveur_NClientEmission_ObtenirFlux( const NClientEmission *this )
{
	return this->m_identifiantFlux;
}

/**
 * Est authentifie?
 *
 * @param this
 *		Cette instance
 *
 * @return si le client est authentife
 */
NBOOL NServeurVoiture_Serveur_NClientEmission_EstAuthentifie( const NClientEmission *this )
{
	return this->m_estAuthentifie;
}


/**
 * Obtenir type flux
 *
 * @param this
 *		Cette instance
 *
 * @return le type de flux
 */
NTypeClientEmission NServeurVoiture_Serveur_NClientEmission_ObtenirTypeClient( const NClientEmission *this )
{
	return this->m_type;
}


