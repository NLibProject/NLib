#include "../../include/NServeurVoiture.h"

// -----------------------------------------------------
// struct NServeurVoiture::Configuration::NConfiguration
// -----------------------------------------------------

/**
 * Construire la configuration
 *
 * @param lien
 *		Le lien relatif vers le fichier de configuration
 *
 * @return l'instance de la configuration
 */
__ALLOC NConfiguration *NServeurVoiture_Configuration_NConfiguration_Construire( const char *lien )
{
	// Sortie
	__OUTPUT NConfiguration *out;

	// Fichier
	NFichierClef *fichier;

	// Clefs
	char **clefs;

	// Obtenir l'ensemble de clefs
	if( !( clefs = NServeurVoiture_Configuration_NProprieteConfiguration_ObtenirEnsembleClef( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_EXPORT_FAILED );

		// Quitter
		return NULL;
	}

	// Ouvrir le fichier
	if( !( fichier = NLib_Fichier_Clef_NFichierClef_Construire( lien,
		(const char**)clefs,
		NPROPRIETES_CONFIGURATION ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( clefs );

		// Quitter
		return NULL;
	}

	// Liberer
	NFREE( clefs );

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NConfiguration ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire le fichier
		NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Obtenir proprietes
		// Port emission
			out->m_portEmission = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_PORT_EMISSION );
		// Port reception
			out->m_portReception = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_PORT_RECEPTION );
		// Delai entre update
			out->m_delaiEntreUpdate = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_DELAI_ENTRE_UPDATE_SERVEUR );
		// Taille maximale fichier
			out->m_tailleMaximaleFichier = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_TAILLE_MAXIMALE_FICHIER_CAPTURE );
		// Nombre maximum de fichiers conserves
			out->m_nombreFichierMaximum = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_NOMBRE_MAXIMUM_FICHIER_CONSERVE );
		// Emplacement de sauvegarde
			out->m_emplacementSauvegarde = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( fichier,
				NPROPRIETE_CONFIGURATION_EMPLACEMENT_SAUVEGARDE_FICHIER_PERIME );
		// Mot de passe stream
			out->m_motDePasseStream = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( fichier,
				NPROPRIETE_CONFIGURATION_MOT_DE_PASSE_STREAM_HTTP );
		// Espacement entre images
			out->m_espacementImageEnregistrement = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_ESPACE_IMAGE_ENREGISTREMENT );

	// Fermer le fichier
	NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

	// OK
	return out;
}

/**
 * Detruire la configuration
 *
 * @param this
 *		Cette instance
 */
void NServeurVoiture_Configuration_NConfiguration_Detruire( NConfiguration **this )
{
	// Liberer
	NFREE( (*this)->m_motDePasseStream );
	NFREE( (*this)->m_emplacementSauvegarde );
	NFREE( *this );
}

/**
 * Obtenir le port d'emission
 *
 * @param this
 *		Cette instance
 *
 * @return le port d'emission
 */
NU32 NServeurVoiture_Configuration_NConfiguration_ObtenirPortEmission( const NConfiguration *this )
{
	return this->m_portEmission;
}

/**
 * Obtenir le port de reception
 *
 * @param this
 *		Cette instance
 *
 * @return le port de reception
 */
NU32 NServeurVoiture_Configuration_NConfiguration_ObtenirPortReception( const NConfiguration *this )
{
	return this->m_portReception;
}

/**
 * Obtenir le delai entre les mises a jour du serveur
 *
 * @param this
 *		Cette instance
 *
 * @return le delai entre les mises a jour
 */
NU32 NServeurVoiture_Configuration_NConfiguration_ObtenirDelaiEntreUpdate( const NConfiguration *this )
{
	return this->m_delaiEntreUpdate;
}

/**
 * Obtenir la taille maximale des fichiers (octets)
 *
 * @param this
 *		Cette instance
 *
 * @return la taille maximale des fichiers
 */
NU32 NServeurVoiture_Configuration_NConfiguration_ObtenirTailleMaximaleFichier( const NConfiguration *this )
{
	return this->m_tailleMaximaleFichier;
}

/**
 * Obtenir le nombre maximal de fichiers conserves
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de fichiers conserves au maximum par flux
 */
NU32 NServeurVoiture_Configuration_NConfiguration_ObtenirNombreFichierParFluxMaximum( const NConfiguration *this )
{
    return this->m_nombreFichierMaximum;
}

/**
 * Obtenir l'emplacement de sauvegarde des fichiers perimes
 *
 * @param this
 *		Cette instance
 *
 * @return le lien de sauvegarde
 */
const char *NServeurVoiture_Configuration_NConfiguration_ObtenirEmplacementSauvegardeFichierPerime( const NConfiguration *this )
{
	return this->m_emplacementSauvegarde;
}

/**
 * Obtenir mot de passe stream
 *
 * @param this
 *		Cette instance
 *
 * @return le mot de passe du stream http
 */
const char *NServeurVoiture_Configuration_NConfiguration_ObtenirMotDePasseStream( const NConfiguration *this )
{
	return this->m_motDePasseStream;
}

/**
 * Obtenir espacement entre image
 *
 * @param this
 *		Cette instance
 *
 * @return l'espacement entre chaque image
 */
NU32 NServeurVoiture_Configuration_NConfiguration_ObtenirEspacementEntreImage( const NConfiguration *this )
{
	return this->m_espacementImageEnregistrement;
}

