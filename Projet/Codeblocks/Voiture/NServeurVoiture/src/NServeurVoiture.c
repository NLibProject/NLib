#include "../include/NServeurVoiture.h"

// -------------------------
// namespace NServeurVoiture
// -------------------------

/**
 * Point d'entree du serveur
 *
 * @param argc
 *		Le nombre d'arguments
 * @param argv
 *		La liste d'arguments
 *
 * @return EXIT_SUCCESS si tout s'est bien passe
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Configuration
	NConfiguration *configuration;

	// Serveur
	NServeurVoiture *serveur;

	// Est en cours?
	NBOOL estEnCours = NTRUE;

	// Initialiser NLib
	NLib_Initialiser( NULL );

	// Charger la configuration
	if( !( configuration = NServeurVoiture_Configuration_NConfiguration_Construire( NSERVEURVOITURE_FICHIER_CONFIGURATION ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Construire le serveur
	if( !( serveur = NServeurVoiture_Serveur_NServeurVoiture_Construire( NServeurVoiture_Configuration_NConfiguration_ObtenirPortReception( configuration ),
		NServeurVoiture_Configuration_NConfiguration_ObtenirPortEmission( configuration ),
		NServeurVoiture_Configuration_NConfiguration_ObtenirDelaiEntreUpdate( configuration ),
		NServeurVoiture_Configuration_NConfiguration_ObtenirTailleMaximaleFichier( configuration ),
		NServeurVoiture_Configuration_NConfiguration_ObtenirNombreFichierParFluxMaximum( configuration ),
		NServeurVoiture_Configuration_NConfiguration_ObtenirEmplacementSauvegardeFichierPerime( configuration ),
		NServeurVoiture_Configuration_NConfiguration_ObtenirMotDePasseStream( configuration ),
		NServeurVoiture_Configuration_NConfiguration_ObtenirEspacementEntreImage( configuration ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire configuration
		NServeurVoiture_Configuration_NConfiguration_Detruire( &configuration );

		// Quitter NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Lister les commandes
	NServeurVoiture_Commande_NCommande_Lister( );

	// Boucle principale
	do
	{
		// Lire commande
		switch( NServeurVoiture_Commande_NCommande_Lire( ) )
		{
			case NCOMMANDE_QUITTER:
				estEnCours = NFALSE;
				break;

			case NCOMMANDE_AIDE:
				NServeurVoiture_Commande_NCommande_Lister( );
				break;

			case NCOMMANDE_AUCUNE:
			default:
				break;
		}
	} while( estEnCours );

	// Detruire le serveur
	NServeurVoiture_Serveur_NServeurVoiture_Detruire( &serveur );

	// Detruire la configuration
	NServeurVoiture_Configuration_NConfiguration_Detruire( &configuration );

	// Fermer NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

