#define NSERVEURVOITURE_COMMANDE_NCOMMANDE_INTERNE
#include "../../include/NServeurVoiture.h"

// -----------------------------------------
// enum NServeurVoiture::Commande::NCommande
// -----------------------------------------

/**
 * Parser une commande
 *
 * @param commande
 *		La commande a parser
 *
 * @return la commande
 */
NCommande NServeurVoiture_Commande_NCommande_Parser( const char *commande )
{
	// Sortie
	__OUTPUT NCommande i = (NCommande)0;

	// Parser nombre
	if( NLib_Chaine_EstUnNombre( commande ) )
		return ( i = ( (NCommande)strtol( commande,
			NULL,
			10 ) + 1 ) ) >= NCOMMANDES ?
				NCOMMANDE_AUCUNE
				: i;

	// Chercher
	for( ; i < NCOMMANDES; i++ )
		// Verifier
		if( NLib_Chaine_Comparer( NCommandeTexte[ i ],
			commande,
			NFALSE,
			0 ) )
			return i;

	// Introuvable
	return NCOMMANDE_AUCUNE;
}

/**
 * Lire une commande
 *
 * @return la commande lue
 */
NCommande NServeurVoiture_Commande_NCommande_Lire( void )
{
	// Buffer
	char buffer[ 2048 ];

	// Lire
	scanf( "%s",
		buffer );

	// Parser
	return NServeurVoiture_Commande_NCommande_Parser( buffer );
}

/**
 * Lister les commandes
 */
void NServeurVoiture_Commande_NCommande_Lister( void )
{
	// Iterateur
	NU32 i = 1;

	// Lister
	for( ; i < NCOMMANDES; i++ )
		printf( "[%d]: \"%s\"\n",
			i - 1,
			NCommandeTexte[ i ] );
}

