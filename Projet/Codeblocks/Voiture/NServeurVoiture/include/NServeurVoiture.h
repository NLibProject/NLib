#ifndef NSERVEURVOITURE_PROTECT
#define NSERVEURVOITURE_PROTECT

// -------------------------
// namespace NServeurVoiture
// -------------------------

// Fichier de configuration
#define NSERVEURVOITURE_FICHIER_CONFIGURATION				"Configuration.ini"

// Emplacement de sauvegarde desactive
#define NSERVEURVOITURE_EMPLACEMENT_SAUVEGARDE_DESACTIVE	"NULL"

// Timeout serveur emission
#define NSERVEURVOITURE_TIMEOUT_SERVEUR_EMISSION			100

// namespace NVoiture
#include "../../NVoiture/include/NVoiture.h"

// namespace NServeurVoiture::Configuration
#include "Configuration/NServeurVoiture_Configuration.h"

// namespace NServeurVoiture::Serveur
#include "Serveur/NServeurVoiture_Serveur.h"

// namespace NServeurVoiture::Commande
#include "Commande/NServeurVoiture_Commande.h"

#endif // !NSERVEURVOITURE_PROTECT


