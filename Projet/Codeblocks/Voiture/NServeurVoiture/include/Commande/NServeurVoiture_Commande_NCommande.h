#ifndef NSERVEURVOITURE_COMMANDE_NCOMMANDE_PROTECT
#define NSERVEURVOITURE_COMMANDE_NCOMMANDE_PROTECT

// -----------------------------------------
// enum NServeurVoiture::Commande::NCommande
// -----------------------------------------

typedef enum NCommande
{
	NCOMMANDE_AUCUNE,

	NCOMMANDE_QUITTER,

	NCOMMANDE_AIDE,

	NCOMMANDES
} NCommande;

/**
 * Parser une commande
 *
 * @param commande
 *		La commande a parser
 *
 * @return la commande
 */
NCommande NServeurVoiture_Commande_NCommande_Parser( const char *commande );

/**
 * Lire une commande
 *
 * @return la commande lue
 */
NCommande NServeurVoiture_Commande_NCommande_Lire( void );

/**
 * Lister les commandes
 */
void NServeurVoiture_Commande_NCommande_Lister( void );

#ifdef NSERVEURVOITURE_COMMANDE_NCOMMANDE_INTERNE
static const char NCommandeTexte[ NCOMMANDES ][ 32 ] =
{
	"",

	"Exit",

	"Help"
};
#endif // NSERVEURVOITURE_COMMANDE_NCOMMANDE_INTERNE

#endif // !NSERVEURVOITURE_COMMANDE_NCOMMANDE_PROTECT

