#ifndef NSERVEURVOITURE_CONFIGURATION_NCONFIGURATION_PROTECT
#define NSERVEURVOITURE_CONFIGURATION_NCONFIGURATION_PROTECT

// -----------------------------------------------------
// struct NServeurVoiture::Configuration::NConfiguration
// -----------------------------------------------------

typedef struct NConfiguration
{
	// Port emission
	NU32 m_portEmission;

	// Port reception
	NU32 m_portReception;

	// Delai entre update
	NU32 m_delaiEntreUpdate;

	// Taille maximale fichier
	NU32 m_tailleMaximaleFichier;

	// Nombre maximum de fichiers conserves
	NU32 m_nombreFichierMaximum;

	// Emplacement de sauvegarde
	char *m_emplacementSauvegarde;

	// Mot de passe stream
	char *m_motDePasseStream;

	// Espacement image enregistrement
	NU32 m_espacementImageEnregistrement;
} NConfiguration;

/**
 * Construire la configuration
 *
 * @param lien
 *		Le lien relatif vers le fichier de configuration
 *
 * @return l'instance de la configuration
 */
__ALLOC NConfiguration *NServeurVoiture_Configuration_NConfiguration_Construire( const char *lien );

/**
 * Detruire la configuration
 *
 * @param this
 *		Cette instance
 */
void NServeurVoiture_Configuration_NConfiguration_Detruire( NConfiguration** );

/**
 * Obtenir le port d'emission
 *
 * @param this
 *		Cette instance
 *
 * @return le port d'emission
 */
NU32 NServeurVoiture_Configuration_NConfiguration_ObtenirPortEmission( const NConfiguration* );

/**
 * Obtenir le port de reception
 *
 * @param this
 *		Cette instance
 *
 * @return le port de reception
 */
NU32 NServeurVoiture_Configuration_NConfiguration_ObtenirPortReception( const NConfiguration* );

/**
 * Obtenir le delai entre les mises a jour du serveur
 *
 * @param this
 *		Cette instance
 *
 * @return le delai entre les mises a jour
 */
NU32 NServeurVoiture_Configuration_NConfiguration_ObtenirDelaiEntreUpdate( const NConfiguration* );

/**
 * Obtenir la taille maximale des fichiers (octets)
 *
 * @param this
 *		Cette instance
 *
 * @return la taille maximale des fichiers
 */
NU32 NServeurVoiture_Configuration_NConfiguration_ObtenirTailleMaximaleFichier( const NConfiguration* );

/**
 * Obtenir le nombre maximal de fichiers conserves
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de fichiers conserves au maximum par flux
 */
NU32 NServeurVoiture_Configuration_NConfiguration_ObtenirNombreFichierParFluxMaximum( const NConfiguration* );

/**
 * Obtenir l'emplacement de sauvegarde des fichiers perimes
 *
 * @param this
 *		Cette instance
 *
 * @return le lien de sauvegarde
 */
const char *NServeurVoiture_Configuration_NConfiguration_ObtenirEmplacementSauvegardeFichierPerime( const NConfiguration* );

/**
 * Obtenir mot de passe stream
 *
 * @param this
 *		Cette instance
 *
 * @return le mot de passe du stream http
 */
const char *NServeurVoiture_Configuration_NConfiguration_ObtenirMotDePasseStream( const NConfiguration* );

/**
 * Obtenir espacement entre image
 *
 * @param this
 *		Cette instance
 *
 * @return l'espacement entre chaque image
 */
NU32 NServeurVoiture_Configuration_NConfiguration_ObtenirEspacementEntreImage( const NConfiguration* );

#endif // !NSERVEURVOITURE_CONFIGURATION_NCONFIGURATION_PROTECT

