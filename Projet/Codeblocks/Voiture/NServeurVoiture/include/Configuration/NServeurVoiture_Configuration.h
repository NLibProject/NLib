#ifndef NSERVEURVOITURE_CONFIGURATION_PROTECT
#define NSERVEURVOITURE_CONFIGURATION_PROTECT

// ----------------------------------------
// namespace NServeurVoiture::Configuration
// ----------------------------------------

// enum NServeurVoiture::Configuration::NProprieteConfiguration
#include "NServeurVoiture_Configuration_NProprieteConfiguration.h"

// struct NServeurVoiture::Configuration::NConfiguration
#include "NServeurVoiture_Configuration_NConfiguration.h"

#endif // !NSERVEURVOITURE_CONFIGURATION_PROTECT

