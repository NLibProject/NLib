#ifndef NSERVEURVOITURE_CONFIGURATION_NPROPRIETECONFIGURATION_PROTECT
#define NSERVEURVOITURE_CONFIGURATION_NPROPRIETECONFIGURATION_PROTECT

// ------------------------------------------------------------
// enum NServeurVoiture::Configuration::NProprieteConfiguration
// ------------------------------------------------------------

typedef enum NProprieteConfiguration
{
	NPROPRIETE_CONFIGURATION_PORT_RECEPTION,
	NPROPRIETE_CONFIGURATION_PORT_EMISSION,

	NPROPRIETE_CONFIGURATION_TAILLE_MAXIMALE_FICHIER_CAPTURE,
	NPROPRIETE_CONFIGURATION_NOMBRE_MAXIMUM_FICHIER_CONSERVE,

	NPROPRIETE_CONFIGURATION_DELAI_ENTRE_UPDATE_SERVEUR,

	NPROPRIETE_CONFIGURATION_EMPLACEMENT_SAUVEGARDE_FICHIER_PERIME,

	NPROPRIETE_CONFIGURATION_MOT_DE_PASSE_STREAM_HTTP,

	NPROPRIETE_CONFIGURATION_ESPACE_IMAGE_ENREGISTREMENT,

	NPROPRIETES_CONFIGURATION
} NProprieteConfiguration;

/**
 * Obtenir l'ensemble des clefs
 * (Seul le conteneur doit etre libere)
 *
 * @return l'ensemble des clefs
 */
__ALLOC char **NServeurVoiture_Configuration_NProprieteConfiguration_ObtenirEnsembleClef( void );

#ifdef NSERVEURVOITURE_CONFIGURATION_NPROPRIETECONFIGURATION_INTERNE
static const char NProprieteConfigurationTexte[ NPROPRIETES_CONFIGURATION ][ 64 ] =
{
	"Port ecoute: ",
	"Port emission: ",

	"Taille maximale (octet): ",
	"Nombre maximum fichier/flux: ",

	"Delai entre update serveur: ",

	"Emplacement sauvegarde: ",

	"Mot de passe stream: ",

	"Espacement image enregistrement: "
};
#endif // NSERVEURVOITURE_CONFIGURATION_NPROPRIETECONFIGURATION_INTERNE

#endif // !NSERVEURVOITURE_CONFIGURATION_NPROPRIETECONFIGURATION_PROTECT

