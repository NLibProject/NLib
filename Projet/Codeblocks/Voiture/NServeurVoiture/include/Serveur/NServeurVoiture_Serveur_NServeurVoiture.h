#ifndef NSERVEURVOITURE_SERVEUR_NSERVEURVOITURE_PROTECT
#define NSERVEURVOITURE_SERVEUR_NSERVEURVOITURE_PROTECT

// ------------------------------------------------
// struct NServeurVoiture::Serveur::NServeurVoiture
// ------------------------------------------------

typedef struct NServeurVoiture
{
	// Emplacement de sauvegarde
	const char *m_emplacementSauvegarde;

	// Mot de passe stream http
	const char *m_motDePasseStream;

	// Delai entre update
	NU32 m_delaiEntreUpdate;

	// Taille maximale fichier
	NU32 m_tailleMaximaleFichierFlux;

	// Nombre maximum de fichier par flux
    NU32 m_nombreMaximumFichierParFlux;

	// Serveur reception flux video
    NServeur *m_serveurReception;

    // Dernier identifiant client reception
    NU32 m_dernierIdentifiantClientReception;

	// Serveur emission
	NServeur *m_serveurEmission;

	// Liste des clients reception (NClientReception)
	NListe *m_clientReception;

	// Liste des clients emission (NClientEmission)
	NListe *m_clientEmission;

	// Thread de mise a jour
	NThread *m_threadUpdate;

	// Est en cours?
	NBOOL m_estEnCours;

	// Nombre de frame skippees a l'enregistrement
	NU32 m_nombreFrameSkippe;

	// Mutex
	NMutex *m_mutex;
} NServeurVoiture;

/**
 * Construire le serveur
 *
 * @param portEcoute
 *		Le port d'ecoute des entrees video
 * @param portEmission
 *		Le port d'emission des sources vers le clients
 * @param delaiEntreUpdate
 *		Le delai entre chaque mise a jour du serveur
 * @param tailleMaximaleFichier
 *		La taille maximale d'un fichier de flux
 * @param nombreMaximumFichier
 *		Le nombre maximum de fichiers conserves pour un flux
 * @param emplacementSauvegarde
 *		L'emplacement de sauvegarde des fichiers perimes (NULL pour ne pas les conserver)
 * @param motDePasseStream
 *		Le mot de passe pour acceder au stream
 * @param nombreDeFrameIgnoreEnregistrement
 *		Le nombre de frame skippees lors de l'enregistrement
 *
 * @return l'instance du serveur
 */
__ALLOC NServeurVoiture *NServeurVoiture_Serveur_NServeurVoiture_Construire( NU32 portEcoute,
	NU32 portEmission,
	NU32 delaiEntreUpdate,
	NU32 tailleMaximaleFichier,
	NU32 nombreMaximumFichier,
	const char *emplacementSauvegarde,
	const char *motDePasseStream,
	NU32 nombreDeFrameIgnoreEnregistrement );

/**
 * Detruire le serveur
 *
 * @param this
 *		Cette instance
 */
void NServeurVoiture_Serveur_NServeurVoiture_Detruire( NServeurVoiture** );

/**
 * Corriger flux
 *
 * @param this
 *		Cette instance
 * @param flux
 *		Le flux a corriger
 *
 * @return si l'operation s'est bien passee
 */
__MUSTBEPROTECTED NBOOL NServeurVoiture_Serveur_NServeurVoiture_CorrigerFlux( NServeurVoiture*,
	__OUTPUT NU32 *flux );

/**
 * Obtenir client reception
 *
 * @param this
 *		Cette instance
 * @param identifiant
 *		L'identifiant du client
 *
 * @return le client reception
 */
__MUSTBEPROTECTED const NClientReception *NServeurVoiture_Serveur_NServeurVoiture_ObtenirClientReceptionDepuisIdentifiant( const NServeurVoiture*,
	NU32 identifiant );

/**
 * Obtenir client reception
 *
 * @param this
 *		Cette instance
 * @param index
 *		L'index du client a obtenir
 *
 * @return le client
 */
__MUSTBEPROTECTED const NClientReception *NServeurVoiture_Serveur_NServeurVoiture_ObtenirClientReceptionDepuisIndex( const NServeurVoiture*,
	NU32 index );

/**
 * Obtenir le nombre de client reception
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de clients en reception
 */
__MUSTBEPROTECTED NU32 NServeurVoiture_Serveur_NServeurVoiture_ObtenirNombreClientReception( const NServeurVoiture* );

#endif // !NSERVEURVOITURE_SERVEUR_NSERVEURVOITURE_PROTECT

