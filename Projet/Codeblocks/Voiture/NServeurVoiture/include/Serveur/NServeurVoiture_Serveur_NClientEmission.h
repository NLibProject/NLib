#ifndef NSERVEURVOITURE_SERVEUR_NCLIENTEMISSION_PROTECT
#define NSERVEURVOITURE_SERVEUR_NCLIENTEMISSION_PROTECT

// ------------------------------------------------
// struct NServeurVoiture::Serveur::NClientEmission
// ------------------------------------------------

typedef struct NClientEmission
{
	// Client serveur
	NClientServeur *m_client;

	// Type de client
	NTypeClientEmission m_type;

	// Identifiant du flux emis pour ce client
	NU32 m_identifiantFlux;

	// Est authentifie?
	NBOOL m_estAuthentifie;
} NClientEmission;

/**
 * Construire le client
 *
 * @param client
 *		Le client
 *
 * @return l'instance du client
 */
__ALLOC NClientEmission *NServeurVoiture_Serveur_NClientEmission_Construire( const NClientServeur *client );

/**
 * Detruire le client
 *
 * @param this
 * 		Cette instance
 */
void NServeurVoiture_Serveur_NClientEmission_Detruire( NClientEmission** );

/**
 * Obtenir client
 *
 * @param this
 *		Cette instance
 *
 * @return le client
 */
const NClientServeur *NServeurVoiture_Serveur_NClientEmission_ObtenirClient( const NClientEmission* );

/**
 * Authentifier
 *
 * @param this
 *		Cette instance
 * @param motDePasse
 *		Le mot de passe pour acceder au flux
 * @param data
 *		Les donnees reçues
 * @param tailleData
 *		La taille des donnees reçues
 * @param serveurVoiture
 *		Le serveur voiture
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NServeurVoiture_Serveur_NClientEmission_RecevoirAuthentification( NClientEmission*,
	const char *motDePasse,
	const char *data,
	NU32 tailleData,
	void *serveurVoiture );

/**
 * Recevoir packet de commande si client NProject
 *
 * @param this
 *		Cette instance
 * @param data
 *		Les donnees
 * @param tailleData
 *		La taille des donnees
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NServeurVoiture_Serveur_NClientEmission_RecevoirCommandeNProject( NClientEmission*,
	const char *data,
	NU32 tailleData );

/**
 * Changer de flux
 *
 * @param this
 *		Cette instance
 * @param flux
 *		Le flux
 */
void NServeurVoiture_Serveur_NClientEmission_ChangerFlux( NClientEmission*,
	NU32 flux );

/**
 * Obtenir flux
 *
 * @param this
 *		Cette instance
 *
 * @return le flux
 */
NU32 NServeurVoiture_Serveur_NClientEmission_ObtenirFlux( const NClientEmission* );

/**
 * Est authentifie?
 *
 * @param this
 *		Cette instance
 *
 * @return si le client est authentife
 */
NBOOL NServeurVoiture_Serveur_NClientEmission_EstAuthentifie( const NClientEmission* );

/**
 * Obtenir type flux
 *
 * @param this
 *		Cette instance
 *
 * @return le type de flux
 */
NTypeClientEmission NServeurVoiture_Serveur_NClientEmission_ObtenirTypeClient( const NClientEmission* );

#endif // !NSERVEURVOITURE_SERVEUR_NCLIENTEMISSION_PROTECT

