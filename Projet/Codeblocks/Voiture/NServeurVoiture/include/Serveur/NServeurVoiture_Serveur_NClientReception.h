#ifndef NSERVEURVOITURE_SERVEUR_NCLIENTRECEPTION_PROTECT
#define NSERVEURVOITURE_SERVEUR_NCLIENTRECEPTION_PROTECT

// -------------------------------------------------
// struct NServeurVoiture::Serveur::NClientReception
// -------------------------------------------------

typedef struct NClientReception
{
	// Client
	NClientServeur *m_client;

	// Flux fichier video
	NMPEGWriter *m_writerMPEG;

	// Liste des clients en emission
	NListe *m_listeClientEmission;

	// Identifiant
	NU32 m_identifiant;

	// Est authentifie?
	NBOOL m_estAuthentifie;

	// Resolution
	NUPoint m_resolution;

	// Byte Per Pixel
	NU32 m_bpp;

	// Frequence
	NU32 m_frequence;

	// Nom de la capture
	char *m_nomCapture;

	// Taille maximale fichier
	NU32 m_tailleMaximaleFichierFlux;

	// Nombre maximum de fichier par flux
    NU32 m_nombreMaximumFichierParFlux;

    // Index du flux actuel
    NU32 m_indexFlux;

    // Lien de sauvegarde
    const char *m_emplacementSauvegarde;

    // Dernier fichier supprime
    NU32 m_dernierFichierSupprime;

    // Nombre de frames a skipper
    NU32 m_nombreFrameASkip;

    // Index de la frame
    NU32 m_indexFrame;
} NClientReception;

/**
 * Construire le client
 *
 * @param client
 *		Le client
 * @param listeClientEmission
 *		La liste des clients en emission
 * @param identifiant
 *		L'identifiant unique du client
 * @param tailleMaximaleFichier
 *		La taille maximale d'un fichier de flux
 * @param nombreMaximumFichier
 *		Le nombre maximum de fichier de flux
 * @param emplacementSauvegarde
 *		L'emplacement de sauvegarde du fichier perime
 * @param nombreFrameSkip
 *		Le nombre de frames ignorees a l'enregistrement
 *
 * @return l'instance du client
 */
__ALLOC NClientReception *NServeurVoiture_Serveur_NClientReception_Construire( const NClientServeur *client,
	NListe *listeClientEmission,
	NU32 identifiant,
	NU32 tailleMaximaleFichier,
	NU32 nombreMaximumFichier,
	const char *emplacementSauvegarde,
	NU32 nombreFrameSkip );

/**
 * Detruire le client
 *
 * @param this
 * 		Cette instance
 */
void NServeurVoiture_Serveur_NClientReception_Detruire( NClientReception** );

/**
 * Creer le lien du flux pour le index'ieme fichier
 *
 * @param this
 *		Cette instance
 * @param out
 *		La sortie
 * @param tailleOut
 *		La taille de la sortie
 * @param index
 *		L'index du flux
 */
void NServeurVoiture_Serveur_NClientReception_CreerLienFlux( const NClientReception*,
	__OUTPUT char *out,
	NU32 tailleOut,
	NU32 index );

/**
 * Authentifier le client
 *
 * @param this
 *		Cette instance
 * @param trame
 *		La trame reçue
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NServeurVoiture_Serveur_NClientReception_Authentifier( NClientReception*,
	const NTrame *trame );

/**
 * Recevoir une image
 *
 * @param this
 *		Cette instance
 * @param trame
 *		La trame reçue
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NServeurVoiture_Serveur_NClientReception_RecevoirImage( NClientReception*,
	const NTrame *trame );

/**
 * Obtenir client
 *
 * @param this
 *		Cette instance
 *
 * @return le client
 */
const NClientServeur *NServeurVoiture_Serveur_NClientReception_ObtenirClient( const NClientReception* );

/**
 * Obtenir l'identifiant
 *
 * @param this
 *		Cette instance
 *
 * @return l'identifiant
 */
NU32 NServeurVoiture_Serveur_NClientReception_ObtenirIdentifiant( const NClientReception* );

/**
 * Obtenir frequence
 *
 * @param this
 *		Cette instance
 *
 * @return la frequence
 */
NU32 NServeurVoiture_Serveur_NClientReception_ObtenirFrequence( const NClientReception* );

/**
 * Obtenir le nom de la capture
 *
 * @param this
 *		Cette instance
 *
 * @return le nom de la capture
 */
const char *NServeurVoiture_Serveur_NClientReception_ObtenirNomCapture( const NClientReception* );

/**
 * Obtenir profondeur
 *
 * @param this
 *		Cette instance
 *
 * @return la profondeur de l'image
 */
NU32 NServeurVoiture_Serveur_NClientReception_ObtenirProfondeur( const NClientReception* );

/**
 * Obtenir la resolution
 *
 * @param this
 *		Cette instance
 *
 * @return la resolution
 */
const NUPoint *NServeurVoiture_Serveur_NClientReception_ObtenirResolution( const NClientReception* );

#endif // !NSERVEURVOITURE_SERVEUR_NCLIENTRECEPTION_PROTECT

