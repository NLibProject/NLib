#ifndef NSERVEURVOITURE_SERVEUR_PROTECT
#define NSERVEURVOITURE_SERVEUR_PROTECT

// ----------------------------------
// namespace NServeurVoiture::Serveur
// ----------------------------------

// enum NServeurVoiture::Serveur::NTypeClientEmission
#include "NServeurVoiture_Serveur_NTypeClientEmission.h"

// struct NServeurVoiture::Serveur::NClientEmission
#include "NServeurVoiture_Serveur_NClientEmission.h"

// struct NServeurVoiture::Serveur::NClientReception
#include "NServeurVoiture_Serveur_NClientReception.h"

// struct NServeurVoiture::Serveur::NServeurVoiture
#include "NServeurVoiture_Serveur_NServeurVoiture.h"

#endif // !NSERVEURVOITURE_SERVEUR_PROTECT

