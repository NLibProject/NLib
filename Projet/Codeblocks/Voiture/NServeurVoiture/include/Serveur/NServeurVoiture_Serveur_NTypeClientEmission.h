#ifndef NSERVEURVOITURE_SERVEUR_NTYPECLIENTEMISSION_PROTECT
#define NSERVEURVOITURE_SERVEUR_NTYPECLIENTEMISSION_PROTECT

// --------------------------------------------------
// enum NServeurVoiture::Serveur::NTypeClientEmission
// --------------------------------------------------

typedef enum NTypeClientEmission
{
	NTYPE_CLIENT_EMISSION_INCONNU,

	NTYPE_CLIENT_EMISSION_HTTP,
	NTYPE_CLIENT_EMISSION_NPROJECT,

	NTYPES_CLIENT_EMISSION
} NTypeClientEmission;

#endif // !NSERVEURVOITURE_SERVEUR_NTYPECLIENTEMISSION_PROTECT

