#ifndef NVOITURE_TRAME_NTYPETRAME_PROTECT
#define NVOITURE_TRAME_NTYPETRAME_PROTECT

// --------------------------------
// enum NVoiture::Trame::NTypeTrame
// --------------------------------

typedef enum NTypeTrame
{
	// Parametrage du flux video
	// |---------------------------------------------------------|
	// |Frequence|Taille nom capture|Nom Capture|Res X|Res Y|BPP |
	// |---------------------------------------------------------|
	// |NU32     |NU32              |TailleNom  |NU32 |NU32 |NU32|
	// |---------------------------------------------------------|
	NTYPE_TRAME_PARAMETRAGE_FLUX_VIDEO,

	// Transmission d'image
	// |------------------------------------|
	// |Res X|Res Y|BPP |Data			    |
	// |------------------------------------|
	// |NU32 |NU32 |NU32|Res X * Res Y * BPP|
	// |------------------------------------|
	NTYPE_TRAME_TRANSMISSION_IMAGE,

	// Authentification client
	// |----------------|
	// |TailleMDP|MDP   |
	// |----------------|
	// |NU32	 |TaiMDP|
	// |----------------|
	NTYPE_TRAME_AUTHENTIFICATION_CLIENT,

	NTYPES_TRAME
} NTypeTrame;

#endif // !NVOITURE_TRAME_NTYPETRAME_PROTECT

