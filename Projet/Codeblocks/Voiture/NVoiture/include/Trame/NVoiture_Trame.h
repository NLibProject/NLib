#ifndef NVOITURE_TRAME_PROTECT
#define NVOITURE_TRAME_PROTECT

// -------------------------
// namespace NVoiture::Trame
// -------------------------

// enum NVoiture::Trame::NTypeTrame
#include "NVoiture_Trame_NTypeTrame.h"

// struct NVoiture::Trame::NTrame
#include "NVoiture_Trame_NTrame.h"

#endif // !NVOITURE_TRAME_PROTECT

