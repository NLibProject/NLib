#ifndef NVOITURE_TRAME_NTRAME_PROTECT
#define NVOITURE_TRAME_NTRAME_PROTECT

// ------------------------------
// struct NVoiture::Trame::NTrame
// ------------------------------

typedef struct NTrame
{
	// Type de trame
	NTypeTrame m_type;

	// Donnees
	char *m_data;

	// Taille des donnees
	NU32 m_tailleData;
} NTrame;

/**
 * Construire la trame
 *
 * @param type
 *		Le type de trame
 *
 * @return l'instance de la trame
 */
__ALLOC NTrame *NVoiture_Trame_NTrame_Construire( NTypeTrame );

/**
 * Construire la trame
 *
 * @param data
 *		Les donnees reçues
 * @param tailleData
 *		La taille des donnees reçues
 *
 * @return l'instance de la trame
 */
__ALLOC NTrame *NVoiture_Trame_NTrame_Construire2( const char *data,
	NU32 tailleData );

/**
 * Detruire la trame
 *
 * @param this
 *		Cette instance
 */
void NVoiture_Trame_NTrame_Detruire( NTrame** );

/**
 * Obtenir le type de trame
 *
 * @param this
 *		Cette instance
 *
 * @return le type de trame
 */
NTypeTrame NVoiture_Trame_NTrame_ObtenitType( const NTrame* );

/**
 * Obtenir les donnees
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees
 */
const char *NVoiture_Trame_NTrame_ObtenirData( const NTrame* );

/**
 * Obtenir la taille des donnees
 *
 * @param this
 *		Cette instance
 *
 * @return la taille des donnees
 */
NU32 NVoiture_Trame_NTrame_ObtenirTailleData( const NTrame* );

/**
 * Obtenir type
 *
 * @param this
 *		Cette instance
 *
 * @return le type de trame
 */
NTypeTrame NVoiture_Trame_NTrame_ObtenirType( const NTrame* );

/**
 * Ajouter des donnees
 *
 * @param this
 *		Cette instance
 * @param data
 *		Les donnees a ajouter
 * @param tailleData
 *		La taille des donnees a ajouter
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NVoiture_Trame_NTrame_AjouterData( NTrame*,
	const char *data,
	NU32 tailleData );

/**
 * Exporter
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees exportees
 */
__ALLOC char *NVoiture_Trame_NTrame_Exporter( const NTrame* );

/**
 * Obtenir taille export
 *
 * @param this
 *		Cette instance
 *
 * @return la taille des donnees exportees
 */
NU32 NVoiture_Trame_NTrame_ObtenirTailleExport( const NTrame* );

#endif // !NVOITURE_TRAME_NTRAME_PROTECT

