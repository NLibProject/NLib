#ifndef NVOITURE_PROTECT
#define NVOITURE_PROTECT

// ------------------
// namespace NVoiture
// ------------------

// Port d'ecoute des donnees de camera
#define NVOITURE_PORT_ECOUTE_CAMERA_SERVEUR		16550

// Port d'emission des donnees de camera du serveur
#define NVOITURE_PORT_EMISSION_CAMERA_SERVEUR	16551

// namespace NLib
#include "../../../../../NLib/include/NLib/NLib.h"

// namespace NVoiture::Trame
#include "Trame/NVoiture_Trame.h"

// namespace NVoiture::Camera
#include "Camera/NVoiture_Camera.h"

#endif // !NVOITURE_PROTECT

