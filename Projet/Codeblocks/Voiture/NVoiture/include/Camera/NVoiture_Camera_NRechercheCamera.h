#ifndef NVOITURE_CAMERA_NRECHERCHECAMERA_PROTECT
#define NVOITURE_CAMERA_NRECHERCHECAMERA_PROTECT

// -----------------------------------------
// struct NVoiture::Camera::NRechercheCamera
// -----------------------------------------

typedef struct NRechercheCamera
{
    // Liste de peripheriques trouves
    NListe *m_peripherique;
} NRechercheCamera;

/**
 * Construire l'instance de la recherche
 *
 * @return l'instance de la recherche
 */
__ALLOC NRechercheCamera *NVoiture_Camera_NRechercheCamera_Construire( void );

/**
 * Detruire l'instance
 *
 * @param this
 *		Cette instance
 */
void NVoiture_Camera_NRechercheCamera_Detruire( NRechercheCamera** );

/**
 * Obtenir le nombre de peripheriques
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de peripheriques
 */
NU32 NVoiture_Camera_NRechercheCamera_ObtenirNombre( const NRechercheCamera* );

/**
 * Obtenir un peripherique
 *
 * @param this
 *		Cette instance
 * @param index
 *		L'index du peripherique
 *
 * @return le peripherique
 */
const NPeripheriqueCamera *NVoiture_Camera_NRechercheCamera_ObtenirPeripherique( const NRechercheCamera*,
	NU32 index );

#endif // !NVOITURE_CAMERA_NRECHERCHECAMERA_PROTECT

