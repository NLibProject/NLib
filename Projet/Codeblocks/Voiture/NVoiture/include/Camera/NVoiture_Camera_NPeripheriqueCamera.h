#ifndef NVOITURE_CAMERA_NPERIPHERIQUECAMERA_PROTECT
#define NVOITURE_CAMERA_NPERIPHERIQUECAMERA_PROTECT

// --------------------------------------------
// struct NVoiture::Camera::NPeripheriqueCamera
// --------------------------------------------

typedef struct NPeripheriqueCamera
{
    /**
     * Repertoire
     */
	char *m_repertoire;

	/**
	 * Nom device
	 */
	char *m_device;

	/**
	 * Lien complet
	 */
	char *m_lienComplet;
} NPeripheriqueCamera;

/**
 * Construire
 *
 * @param repertoire
 *		Le repertoire
 * @param device
 *		Le nom du peripherique
 *
 * @return l'instance du peripherique
 */
__ALLOC NPeripheriqueCamera *NVoiture_Camera_NPeripheriqueCamera_Construire( const char *repertoire,
	const char *device );

/**
 * Detruire l'instance
 *
 * @param this
 *		Cette instance
 */
void NVoiture_Camera_NPeripheriqueCamera_Detruire( NPeripheriqueCamera** );

/**
 * Obtenir le repertoire
 *
 * @param this
 *		Cette instance
 *
 * @return le repertoire
 */
const char *NVoiture_Camera_NPeripheriqueCamera_ObtenirRepertoire( const NPeripheriqueCamera* );

/**
 * Obtenir le nom du peripherique
 *
 * @param this
 *		Cette instance
 *
 * @return le nom du peripherique
 */
const char *NVoiture_Camera_NPeripheriqueCamera_ObtenirDevice( const NPeripheriqueCamera* );

/**
 * Obtenir le lien complet
 *
 * @param this
 *		Cette instance
 *
 * @return le lien complet
 */
const char *NVoiture_Camera_NPeripheriqueCamera_ObtenirLienComplet( const NPeripheriqueCamera* );

#endif // !NVOITURE_CAMERA_NPERIPHERIQUECAMERA_PROTECT

