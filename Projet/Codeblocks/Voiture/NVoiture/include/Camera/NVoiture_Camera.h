#ifndef NVOITURE_CAMERA_PROTECT
#define NVOITURE_CAMERA_PROTECT

// --------------------------
// namespace NVoiture::Camera
// --------------------------

// enum NVoiture::Camera::NListeRepertoireCamera
#include "NVoiture_Camera_NListeRepertoireCamera.h"

// struct NVoiture::Camera::NPeripheriqueCamera
#include "NVoiture_Camera_NPeripheriqueCamera.h"

// struct NVoiture::Camera::NRechercheCamera
#include "NVoiture_Camera_NRechercheCamera.h"

/**
 * Creer le lien du flux pour le index'ieme fichier
 *
 * @param this
 *		Cette instance
 * @param out
 *		La sortie
 * @param tailleOut
 *		La taille de la sortie
 * @param index
 *		L'index du flux
 */
void NVoiture_Camera_CreerLienFlux( const char *nom,
	const char *ip,
	NU32 identifiant,
	__OUTPUT char *out,
	NU32 tailleOut,
	NU32 index );

/**
 * Creer le lien du flux pour le index'ieme fichier generique
 *
 * @param this
 *		Cette instance
 * @param out
 *		La sortie
 * @param tailleOut
 *		La taille de la sortie
 */
void NVoiture_Camera_CreerLienFluxGenerique( const char *nom,
	const char *ip,
	NU32 identifiant,
	__OUTPUT char *out,
	NU32 tailleOut );

/**
 * Creer le writer mpeg pour enregistrer le flux video
 *
 * @param nom
 *		Le nom du flux
 * @param ip
 *		L'ip du flux
 * @param identifiant
 *		L'identifiant du flux
 * @param tailleMaximaleFlux
 *		La taille maximale du fichier de stockage du flux
 * @param nombreMaximumFlux
 *		Le nombre maximum de flux conserves
 * @param emplacementSauvegarde
 *		L'emplacement de sauvegarde au lieu de supprimer (si != NULL)
 * @param indexFlux
 *		L'index de flux actuel
 * @param dernierFichierSupprime
 *		L'index du dernier fichier a avoir ete supprime
 *
 * @return le nouveau writer mpeg
 */
__ALLOC NMPEGWriter *NVoiture_Camera_CreerWriterMPEG( const char *nom,
	const char *ip,
	NU32 identifiant,
	NU32 tailleMaximaleFlux,
	NU32 nombreMaximumFlux,
	const char *emplacementSauvegarde,
	__OUTPUT NU32 *indexFlux,
	__OUTPUT NU32 *dernierFichierSupprime );

#endif // !NVOITURE_CAMERA_PROTECT

