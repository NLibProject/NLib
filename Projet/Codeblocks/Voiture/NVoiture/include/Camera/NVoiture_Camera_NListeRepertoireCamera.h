#ifndef NVOITURE_CAMERA_NLISTEREPERTOIRECAMERA_PROTECT
#define NVOITURE_CAMERA_NLISTEREPERTOIRECAMERA_PROTECT

// ---------------------------------------------
// enum NVoiture::Camera::NListeRepertoireCamera
// ---------------------------------------------

typedef enum NListeRepertoireCamera
{
	NLISTE_REPERTOIRE_CAMERA_DEV,

	NLISTE_REPERTOIRES_CAMERA
} NListeRepertoireCamera;

/**
 * Obtenir le nom du repertoire
 *
 * @param repertoire
 *		Le repertoire dont on cherche le nom
 *
 * @return le nom du repertoire
 */
const char *NVoiture_Camera_NListeRepertoireCamera_ObtenirNom( NListeRepertoireCamera );

#ifdef NVOITURE_CAMERA_NLISTEREPERTOIRECAMERA_INTERNE
static const char NListeRepertoireCameraTexte[ NLISTE_REPERTOIRES_CAMERA ][ 32 ] =
{
	"/dev"
};
#endif // NVOITURE_CAMERA_NLISTEREPERTOIRECAMERA_INTERNE

#endif // !NVOITURE_CAMERA_NLISTEREPERTOIRECAMERA_PROTECT

