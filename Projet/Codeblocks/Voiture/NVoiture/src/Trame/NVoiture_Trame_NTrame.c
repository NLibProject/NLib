#include "../../include/NVoiture.h"

// ------------------------------
// struct NVoiture::Trame::NTrame
// ------------------------------

/**
 * Construire la trame
 *
 * @param type
 *		Le type de trame
 *
 * @return l'instance de la trame
 */
__ALLOC NTrame *NVoiture_Trame_NTrame_Construire( NTypeTrame type )
{
	// Sortie
	__OUTPUT NTrame *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NTrame ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_type = type;

	// OK
	return out;
}

/**
 * Construire la trame
 *
 * @param data
 *		Les donnees reçues
 * @param tailleData
 *		La taille des donnees reçues
 *
 * @return l'instance de la trame
 */
__ALLOC NTrame *NVoiture_Trame_NTrame_Construire2( const char *data,
	NU32 tailleData )
{
	// Sortie
	__OUTPUT NTrame *out;

	// Verifier parametre
	if( tailleData < sizeof( NU32 ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NTrame ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Lire le type
	out->m_type = *( (NU32*)( data ) );

	// Lire les donnees
	if( ( out->m_tailleData = tailleData - sizeof( NU32 ) ) > 0 )
	{
		// Allouer les donnees
		if( !( out->m_data = calloc( out->m_tailleData,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Copier
		memcpy( out->m_data,
			data + sizeof( NU32 ),
			out->m_tailleData );
	}

	// OK
	return out;
}

/**
 * Detruire la trame
 *
 * @param this
 *		Cette instance
 */
void NVoiture_Trame_NTrame_Detruire( NTrame **this )
{
	NFREE( (*this)->m_data );
	NFREE( (*this) );
}

/**
 * Obtenir le type de trame
 *
 * @param this
 *		Cette instance
 *
 * @return le type de trame
 */
NTypeTrame NVoiture_Trame_NTrame_ObtenitType( const NTrame *this )
{
	return this->m_type;
}

/**
 * Obtenir les donnees
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees
 */
const char *NVoiture_Trame_NTrame_ObtenirData( const NTrame *this )
{
	return this->m_data;
}

/**
 * Obtenir la taille des donnees
 *
 * @param this
 *		Cette instance
 *
 * @return la taille des donnees
 */
NU32 NVoiture_Trame_NTrame_ObtenirTailleData( const NTrame *this )
{
	return this->m_tailleData;
}

/**
 * Obtenir type
 *
 * @param this
 *		Cette instance
 *
 * @return le type de trame
 */
NTypeTrame NVoiture_Trame_NTrame_ObtenirType( const NTrame *this )
{
	return this->m_type;
}

/**
 * Ajouter des donnees
 *
 * @param this
 *		Cette instance
 * @param data
 *		Les donnees a ajouter
 * @param tailleData
 *		La taille des donnees a ajouter
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NVoiture_Trame_NTrame_AjouterData( NTrame *this,
	const char *data,
	NU32 tailleData )
{
	// Ajouter data
	if( !NLib_Memoire_AjouterData( &this->m_data,
		this->m_tailleData,
		data,
		tailleData ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Incrementer la taille
	this->m_tailleData += tailleData;

	// OK
	return NTRUE;
}

/**
 * Exporter
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees exportees
 */
__ALLOC char *NVoiture_Trame_NTrame_Exporter( const NTrame *this )
{
	// Sortie
	__OUTPUT char *out;

	// Allouer la memoire
	if( !( out = calloc( NVoiture_Trame_NTrame_ObtenirTailleExport( this ),
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
		// Type
			*( (NU32*)( out ) ) = this->m_type;
		// Donnees
			memcpy( out + sizeof( NU32 ),
				this->m_data,
				this->m_tailleData );

	// OK
	return out;
}

/**
 * Obtenir taille export
 *
 * @param this
 *		Cette instance
 *
 * @return la taille des donnees exportees
 */
NU32 NVoiture_Trame_NTrame_ObtenirTailleExport( const NTrame *this )
{
	return sizeof( NU32 )
		+ this->m_tailleData;
}

