#include "../../include/NVoiture.h"

// --------------------------
// namespace NVoiture::Camera
// --------------------------

/**
 * Creer le lien du flux pour le index'ieme fichier
 *
 * @param this
 *		Cette instance
 * @param out
 *		La sortie
 * @param tailleOut
 *		La taille de la sortie
 * @param index
 *		L'index du flux
 */
void NVoiture_Camera_CreerLienFlux( const char *nom,
	const char *ip,
	NU32 identifiant,
	__OUTPUT char *out,
	NU32 tailleOut,
	NU32 index )
{
	// Creer le lien
	snprintf( out,
		tailleOut,
		"%s_%s_%d_%d.mpg",
		ip,
		nom,
		identifiant,
		index );
}

/**
 * Creer le lien du flux pour le index'ieme fichier generique
 *
 * @param this
 *		Cette instance
 * @param out
 *		La sortie
 * @param tailleOut
 *		La taille de la sortie
 */
void NVoiture_Camera_CreerLienFluxGenerique( const char *nom,
	const char *ip,
	NU32 identifiant,
	__OUTPUT char *out,
	NU32 tailleOut )
{
	// Creer le lien
	snprintf( out,
		tailleOut,
		"%s_%s_%d_%%d",
		ip,
		nom,
		identifiant );
}

/**
 * Trouver fichier a supprimer
 *
 * @param this
 *		Cette instance
 * @param out
 *		Le lien vers le fichier a supprimer
 * @param tailleOut
 *		La taille du lien en sortie
 *
 * @return si il y a un fichier a supprimer
 */
NBOOL NVoiture_Camera_TrouverFichierASupprimer( const char *nom,
	const char *ip,
	NU32 identifiant,
	NU32 *dernierFichierSupprime,
	NU32 nombreMaximumFichierFlux,
	__OUTPUT char *out,
	NU32 tailleOut )
{
    // Premier index
    NU32 index;

    // Nombre de fichiers parcourus
    NU32 nombre = 0;

    // Definir l'index de depart
    if( *dernierFichierSupprime >= nombreMaximumFichierFlux )
		index = 0;
	else
		index = *dernierFichierSupprime;

	// Verifier que le stockage des videos est rempli
	do
	{
		// Creer le lien
		NVoiture_Camera_CreerLienFlux( nom,
			ip,
			identifiant,
			out,
			tailleOut,
			index );

		// Verifier que le fichier existe
        if( NLib_Fichier_Operation_EstExiste( out ) )
        {
			// Enregistrer
			*dernierFichierSupprime = index + 1;

			// Supprimer le fichier
			return NTRUE;
		}

		// Incrementer index
		if( index < nombreMaximumFichierFlux - 1 )
			index++;
		else
			index = 0;
	} while( nombre++ < nombreMaximumFichierFlux + 1 );

    // Impossible de trouver un fichier a supprimer
    return NFALSE;
}

/**
 * Archiver un fichier
 *
 * @param this
 *		Cette instance
 * @param fichier
 *		Le lien du fichier a archiver
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NVoiture_Camera_ArchiverFichier( const char *nom,
	const char *ip,
	NU32 identifiant,
	const char *emplacementSauvegarde,
	const char *fichier )
{
	// Lien copie
	char lienCopie[ 2048 ];

	// Nom standard
	char nomSortie[ 512 ];

	// Index
	NU32 index = 0;

    // Chercher un lien libre dans le repertoire de destination
    do
    {
		// Creer le nom standard
		NVoiture_Camera_CreerLienFlux( nom,
			ip,
			identifiant,
			nomSortie,
			512,
			index++ );

		// Creer le lien final
		snprintf( lienCopie,
			2048,
			"%s/%s",
			emplacementSauvegarde,
			nomSortie );
    } while( NLib_Fichier_Operation_EstExiste( lienCopie ) );

    // Copier le fichier
    NLib_Fichier_Operation_CopierFichier( fichier,
		lienCopie );

    // OK
    return NTRUE;
}

/**
 * Creer le writer mpeg pour enregistrer le flux video
 *
 * @param nom
 *		Le nom du flux
 * @param ip
 *		L'ip du flux
 * @param identifiant
 *		L'identifiant du flux
 * @param tailleMaximaleFlux
 *		La taille maximale du fichier de stockage du flux
 * @param nombreMaximumFlux
 *		Le nombre maximum de flux conserves
 * @param emplacementSauvegarde
 *		L'emplacement de sauvegarde au lieu de supprimer (si != NULL)
 * @param indexFlux
 *		L'index de flux actuel
 * @param dernierFichierSupprime
 *		L'index du dernier fichier a avoir ete supprime
 *
 * @return le nouveau writer mpeg
 */
__ALLOC NMPEGWriter *NVoiture_Camera_CreerWriterMPEG( const char *nom,
	const char *ip,
	NU32 identifiant,
	NU32 tailleMaximaleFlux,
	NU32 nombreMaximumFlux,
	const char *emplacementSauvegarde,
	__OUTPUT NU32 *indexFlux,
	__OUTPUT NU32 *dernierFichierSupprime )
{
	// Existe?
	NBOOL estExiste;

	// Nom fichier
    char nomFichierSortie[ 512 ];

    // Message
    char message[ 2048 ];

    // Continuer?
    NBOOL estContinuer = NTRUE;

    // Nombre d'iterations
    NU32 nombreIteration = 0;

	// Writer
	__OUTPUT NMPEGWriter *writerMPEG;

	// Creer le lien
	do
	{
		// Incrementer nombre iteration
		if( nombreIteration++ >= nombreMaximumFlux + 1 )
		{
			// Supprimer fichier superflux
			if( NVoiture_Camera_TrouverFichierASupprimer( nom,
				ip,
				identifiant,
				dernierFichierSupprime,
				nombreMaximumFlux,
				nomFichierSortie,
				512 ) )
			{
				// On a un emplacement de sauvegarde
				if( emplacementSauvegarde != NULL )
					// Copier le fichier
					NVoiture_Camera_ArchiverFichier( nom,
						ip,
						identifiant,
						emplacementSauvegarde,
						nomFichierSortie );

				// Supprimer le fichier
				NLib_Fichier_Operation_SupprimerFichier( nomFichierSortie );
			}

			// Nombre iterations a zero
			nombreIteration = 1;
		}

		// Verifier
		if( *indexFlux >= nombreMaximumFlux )
			// Remettre a zero
			(*indexFlux) = 0;

		// Creer le lien
		NVoiture_Camera_CreerLienFlux( nom,
			ip,
			identifiant,
			nomFichierSortie,
			512,
			(*indexFlux)++ );

		// Verifier
		if( ( estExiste = NLib_Fichier_Operation_EstExiste( nomFichierSortie ) )
			&& NLib_Fichier_Operation_ObtenirTaille2( nomFichierSortie ) >= tailleMaximaleFlux )
			continue;

		// Tenter de creer le writer
		if( !( writerMPEG = NLib_Module_MPEGWriter_NMPEGWriter_Construire( nomFichierSortie,
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_AVERTISSEMENT( NERREUR_CONSTRUCTOR_FAILED );

			// Quitter
			continue;
		}

		// On quitte
		estContinuer = NFALSE;
	} while( estContinuer );

	// Creer le message
	snprintf( message,
		2048,
		"[%s]\tEcriture du flux \"%s\" dans le fichier \"%s\"",
		ip,
		nom,
		nomFichierSortie );

	// Notifier
	NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
		message,
		0 );

	// OK
	return writerMPEG;
}

