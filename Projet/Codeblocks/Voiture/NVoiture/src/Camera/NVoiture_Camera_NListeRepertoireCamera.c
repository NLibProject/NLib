#define NVOITURE_CAMERA_NLISTEREPERTOIRECAMERA_INTERNE
#include "../../include/NVoiture.h"

// ---------------------------------------------
// enum NVoiture::Camera::NListeRepertoireCamera
// ---------------------------------------------

/**
 * Obtenir le nom du repertoire
 *
 * @param repertoire
 *		Le repertoire dont on cherche le nom
 *
 * @return le nom du repertoire
 */
const char *NVoiture_Camera_NListeRepertoireCamera_ObtenirNom( NListeRepertoireCamera repertoire )
{
	return NListeRepertoireCameraTexte[ repertoire ];
}

