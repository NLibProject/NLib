#include "../../include/NVoiture.h"

// -----------------------------------------
// struct NVoiture::Camera::NRechercheCamera
// -----------------------------------------

/**
 * Construire l'instance de la recherche
 *
 * @return l'instance de la recherche
 */
__ALLOC NRechercheCamera *NVoiture_Camera_NRechercheCamera_Construire( void )
{
	// Sortie
	__OUTPUT NRechercheCamera *out;

    // Repertoire
    NRepertoire *repertoire;

    // Peripherique
    NPeripheriqueCamera *peripherique;

    // Fichier repertoire
   const NElementRepertoire *fichierRepertoire;

    // Iterateurs
    NU32 i = 0,
		j;

    // Allouer la sortie
    if( !( out = calloc( 1,
		sizeof( NRechercheCamera ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire le repertoire
	if( !( repertoire = NLib_Module_Repertoire_NRepertoire_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire la liste des peripheriques
	if( !( out->m_peripherique = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NVoiture_Camera_NPeripheriqueCamera_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire repertoire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

    // Chercher
    for( ; i < NLISTE_REPERTOIRES_CAMERA; i++ )
	{
		// Changer de repertoire
		if( !NLib_Module_Repertoire_Changer( NVoiture_Camera_NListeRepertoireCamera_ObtenirNom( (NListeRepertoireCamera)i ) ) )
		{
			// Notifier
			NOTIFIER_AVERTISSEMENT( NERREUR_DIRECTORY );

			// Ignorer
			continue;
		}

		// Lister repertoire
		NLib_Module_Repertoire_NRepertoire_Lister( repertoire,
			"video*",
			NATTRIBUT_REPERTOIRE_DEVICE );

		// Ajouter les peripheriques
		for( j = 0; j < NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( repertoire ); j++ )
		{
			// Obtenir le fichier repertoire
			if( !( fichierRepertoire = NLib_Module_Repertoire_NRepertoire_ObtenirFichier( repertoire,
				j ) ) )
				continue;

			// Construire le peripherique
			if( !( peripherique = NVoiture_Camera_NPeripheriqueCamera_Construire( NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirCheminAbsolu( fichierRepertoire ),
				NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirNom( fichierRepertoire ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Restaurer le repertoire
				NLib_Module_Repertoire_RestaurerInitial( );

				// Detruire liste
				NLib_Memoire_NListe_Detruire( &out->m_peripherique );

				// Detruire repertoire
				NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

                // Liberer
                NFREE( out );

                // Quitter
                return NULL;
			}

			// Ajouter a la liste
			if( !NLib_Memoire_NListe_Ajouter( out->m_peripherique,
				peripherique ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Restaurer le repertoire
				NLib_Module_Repertoire_RestaurerInitial( );

				// Detruire liste
				NLib_Memoire_NListe_Detruire( &out->m_peripherique );

				// Detruire repertoire
				NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

                // Liberer
                NFREE( out );

                // Quitter
                return NULL;
			}
		}
	}

	// Restaurer le repertoire
	NLib_Module_Repertoire_RestaurerInitial( );

	// Detruire la liste
	NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

	// OK
	return out;
}

/**
 * Detruire l'instance
 *
 * @param this
 *		Cette instance
 */
void NVoiture_Camera_NRechercheCamera_Detruire( NRechercheCamera **this )
{
	// Detruire la liste
	NLib_Memoire_NListe_Detruire( &(*this)->m_peripherique );

	// Liberer
	NFREE( *this );
}

/**
 * Obtenir le nombre de peripheriques
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de peripheriques
 */
NU32 NVoiture_Camera_NRechercheCamera_ObtenirNombre( const NRechercheCamera *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_peripherique );
}

/**
 * Obtenir un peripherique
 *
 * @param this
 *		Cette instance
 * @param index
 *		L'index du peripherique
 *
 * @return le peripherique
 */
const NPeripheriqueCamera *NVoiture_Camera_NRechercheCamera_ObtenirPeripherique( const NRechercheCamera *this,
	NU32 index )
{
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_peripherique,
		index );
}

