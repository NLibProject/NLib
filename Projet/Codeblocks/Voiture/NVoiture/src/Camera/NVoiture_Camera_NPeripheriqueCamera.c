#include "../../include/NVoiture.h"

// --------------------------------------------
// struct NVoiture::Camera::NPeripheriqueCamera
// --------------------------------------------

/**
 * Construire
 *
 * @param repertoire
 *		Le repertoire
 * @param device
 *		Le nom du peripherique
 *
 * @return l'instance du peripherique
 */
__ALLOC NPeripheriqueCamera *NVoiture_Camera_NPeripheriqueCamera_Construire( const char *repertoire,
	const char *device )
{
	// Sortie
	__OUTPUT NPeripheriqueCamera *out;

	// Taille des chaines
	NU32 tailleDevice,
		tailleRepertoire;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NPeripheriqueCamera ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Calculer les tailles
	tailleDevice = strlen( device );
	tailleRepertoire = strlen( repertoire );

	// Allouer les details
	if( !( out->m_device = calloc( tailleDevice + 1,
		sizeof( char ) ) )
		|| !( out->m_repertoire = calloc( tailleRepertoire + 1,
			sizeof( char ) ) )
		|| !( out->m_lienComplet = calloc( tailleDevice + tailleRepertoire + 2,
			sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out->m_device );
		NFREE( out->m_repertoire );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out->m_device,
		device,
		tailleDevice );
	memcpy( out->m_repertoire,
		repertoire,
		tailleRepertoire );
	snprintf( out->m_lienComplet,
		tailleDevice + tailleRepertoire + 2,
		"%s/%s",
		repertoire,
		device );

	// OK
	return out;
}

/**
 * Detruire l'instance
 *
 * @param this
 *		Cette instance
 */
void NVoiture_Camera_NPeripheriqueCamera_Detruire( NPeripheriqueCamera **this )
{
	NFREE( (*this)->m_device );
	NFREE( (*this)->m_repertoire );
	NFREE( (*this)->m_lienComplet );
	NFREE( *this );
}

/**
 * Obtenir le repertoire
 *
 * @param this
 *		Cette instance
 *
 * @return le repertoire
 */
const char *NVoiture_Camera_NPeripheriqueCamera_ObtenirRepertoire( const NPeripheriqueCamera *this )
{
	return this->m_repertoire;
}

/**
 * Obtenir le nom du peripherique
 *
 * @param this
 *		Cette instance
 *
 * @return le nom du peripherique
 */
const char *NVoiture_Camera_NPeripheriqueCamera_ObtenirDevice( const NPeripheriqueCamera *this )
{
	return this->m_device;
}

/**
 * Obtenir le lien complet
 *
 * @param this
 *		Cette instance
 *
 * @return le lien complet
 */
const char *NVoiture_Camera_NPeripheriqueCamera_ObtenirLienComplet( const NPeripheriqueCamera *this )
{
	return this->m_lienComplet;
}

