#include "../include/NExtracteurDashcam.h"

// ----------------------------
// namespace NExtracteurDashcam
// ----------------------------

/**
 * Point d'entree
 *
 * @param argc
 *		Le nombre d'argument
 * @param argv
 *		Les arguments
 *
 * @return EXIT_SUCCESS si tout s'est bien passe
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Verifier l'appel
	if( argc < 8 )
	{
		// Notifier
		puts( "./NExtracteurDashcam NOM_CAPTURE IDENTIFIANT NOMBRE_MAX_FICHIERS QUALITE_CRF_MP4 FPS CONSERVE_FICHIER_INTERMEDIAIRE \"REPERTOIRE_SORTIE\"\n" );

		// Quitter
		return EXIT_FAILURE;
	}

	// Initialiser NLib
	NLib_Initialiser( NULL );

	// Lancer l'extraction
	NDashcam_Extracteur_Extraire( argv[ 1 ],
		strtol( argv[ 2 ],
			NULL,
			10 ),
		strtol( argv[ 3 ],
			NULL,
			10 ),
		strtol( argv[ 4 ],
			NULL,
			10 ),
		strtol( argv[ 5 ],
			NULL,
			10 ),
		strtol( argv[ 6 ],
			NULL,
			10 ),
		argv[ 7 ] );

	// Quitter NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

