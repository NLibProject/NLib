#ifndef NDASHCAM_NBOUTONDASHCAM_PROTECT
#define NDASHCAM_NBOUTONDASHCAM_PROTECT

// -----------------------------
// enum NDashcam::NBoutonDashcam
// -----------------------------

typedef enum NBoutonDashcam
{
	NBOUTON_DASHCAM_MASQUER_FENETRE,

	NBOUTON_DASHCAM_SOURCE_SUIVANTE,
	NBOUTON_DASHCAM_SOURCE_PRECEDENTE,

	NBOUTON_DASHCAM_MASQUER_MONTRER_UI,

	NBOUTONS_DASHCAM
} NBoutonDashcam;

// Taille des boutons source suivante/precedente
#define NDASHCAM_TAILLE_BOUTON_SOURCE_X					50
#define NDASHCAM_TAILLE_BOUTON_SOURCE_Y					50

// Taille du bouton de masquage fenetre
#define NDASHCAM_TAILLE_BOUTON_MASQUAGE_FENETRE_X		100
#define NDASHCAM_TAILLE_BOUTON_MASQUAGE_FENETRE_Y		50

// Marge bouton
#define NDASHCAM_MARGE_BOUTON							20

// Epaisseur des contours
#define NDASHCAM_EPAISSEUR_BORDURE_BOUTON				0

// Police texte
#define NDASHCAM_POLICE_BOUTON							"dejavu.ttf"

// Taille police texte
#define NDASHCAM_TAILLE_POLICE_BOUTON					20

// Couleur police
#define NDASHCAM_COULEUR_POLICE_BOUTON					(NCouleur){ 0xFF, 0xFF, 0xFF }
/**
 * Obtenir texte bouton
 *
 * @param bouton
 *		Le bouton dont on veut le texte
 *
 * @return le texte du bouton
 */
const char *NDashcam_NBoutonDashcam_ObtenirTexte( NBoutonDashcam );

#ifdef NDASHCAM_NBOUTONDASHCAM_INTERNE
static const char NBoutonDashcamTexte[ NBOUTONS_DASHCAM ][ 32 ] =
{
    "Masquer",

    ">",
    "<",

    "UI"
};
#endif // NDASHCAM_NBOUTONDASHCAM_INTERNE

#endif // !NDASHCAM_NBOUTONDASHCAM_PROTECT

