#ifndef NDASHCAM_FORMAT_PROTECT
#define NDASHCAM_FORMAT_PROTECT

// --------------------------
// namespace NDashcam::Format
// --------------------------

/**
 * Header fichier
 *
 * DASHCAM_HEADER_FICHIER
 * RESOLUTION X
 * RESOLUTION Y
 * BPP
 */

// Header fichier
#define DASHCAM_HEADER_FICHIER		"NHDRDASHCAMFRAME"

// Taille header fichier
#define DASHCAM_TAILLE_HEA_FICHIER	16

/**
 * Format d'une frame
 *
 * DASHCAM_HEADER_FRAME
 * CHECKSUM (taille+frame)
 * TAILLE FRAME COMPRESSEE au format lz4
 * FRAME COMPRESSEE (lz4)
 */

// Header frame
#define DASHCAM_HEADER_FRAME	"NPRO"

// Taille header frame
#define DASHCAM_TAILLE_HEADER	4

#endif // !NDASHCAM_FORMAT_PROTECT

