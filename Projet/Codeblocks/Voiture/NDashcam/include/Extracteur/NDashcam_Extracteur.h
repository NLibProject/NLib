#ifndef NDASHCAM_EXTRACTEUR_PROTECT
#define NDASHCAM_EXTRACTEUR_PROTECT

// ------------------------------
// namespace NDashcam::Extracteur
// ------------------------------

/**
 * Extraire frame depuis les fichiers de la dashcam
 *
 * @param nomCapture
 *		Le nom de la capture
 * @param identifiant
 *		L'identifiant de la capture
 * @param nombreMaxFichier
 *		Le nombre maximum de fichiers
 * @param qualiteExport
 *		Le CRF (allant de 0 pour lossless a 51 pour avoir de la grosse merde)
 * @param fpsExport
 *		Le nombre d'image par seconde rendues dans le fichier de sortie
 * @param estConserveIntermediaire
 *		On conserve les frames PNG intermediaires?
 * @param repertoireSortie
 *		Le repertoire de sortie du traitement
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NDashcam_Extracteur_Extraire( const char *nomCapture,
	NU32 identifiant,
	NU32 nombreMaximumFichier,
	NU32 qualiteExport,
	NU32 fpsExport,
	NBOOL estConserveIntermediaire,
	const char *repertoireSortie );

#endif // !NDASHCAM_EXTRACTEUR_PROTECT

