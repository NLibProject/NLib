#ifndef NDASHCAM_NCAMERADASHCAM_PROTECT
#define NDASHCAM_NCAMERADASHCAM_PROTECT

// -------------------------------
// struct NDashcam::NCameraDashcam
// -------------------------------

typedef struct NCameraDashcam
{
	// Camera
	NCamera *m_camera;

	// Fenetre
	const NFenetre *m_fenetre;

	// Configuration
	const NConfiguration *m_configuration;

	// Doit generer une surface SDL
	NBOOL m_estDoitGenererSurface;

	// Surface SDL
	NSurface *m_surface;

	// Writer MPEG
	NMPEGWriter *m_writer;

    // Identifiant
    NU32 m_identifiant;

	// Thread
	NThread *m_thread;

	// Est en cours?
	NBOOL m_estEnCours;

	// Peripherique
	const NPeripheriqueCamera *m_peripherique;

	// Mutex
	NMutex *m_mutex;

	// Donnees inter-lancement
	NInterLancement *m_interLancement;

	// Buffer
	char *m_buffer;

	// Taille buffer
	NU32 m_tailleBuffer;

	// Framerate
	NFramerate *m_framerate;

	// Dernier temps d'acquisition
	NU32 m_dernierTempsAcquisition;

	// Index frame
	NU32 m_indexFrame;
} NCameraDashcam;

/**
 * Construire la camera
 *
 * @param configuration
 *		La configuration
 * @param fenetre
 *		La fenetre de l'interface
 * @param peripherique
 *		La description du peripherique
 * @param identifiant
 *		L'identifiant de la camera
 *
 * @return si l'operation s'est bien passee
 */
__ALLOC NCameraDashcam *NDashcam_NCameraDashcam_Construire( const NConfiguration *configuration,
	const NFenetre *fenetre,
	const NPeripheriqueCamera *peripherique,
	NU32 identifiant );

/**
 * Detruire l'instance
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NCameraDashcam_Detruire( NCameraDashcam** );

/**
 * Activer protection
 *
 * @param this
 *		Cette instance
 */
__WILLLOCK void NDashcam_NCameraDashcam_ActiverProtection( NCameraDashcam* );

/**
 * Desactiver protection
 *
 * @param this
 *		Cette instance
 */
__WILLUNLOCK void NDashcam_NCameraDashcam_DesactiverProtection( NCameraDashcam* );

/**
 * Obtenir surface
 *
 * @param this
 *		Cette instance
 *
 * @return la surface
 */
__MUSTBEPROTECTED const NSurface *NDashcam_NCameraDashcam_ObtenirSurface( const NCameraDashcam* );

/**
 * Obtenir peripherique
 *
 * @param this
 *		Cette instance
 *
 * @return le detail du peripherique
 */
const NPeripheriqueCamera *NDashcam_NCameraDashcam_ObtenirPeripherique( const NCameraDashcam* );

/**
 * Obtenir identifiant
 *
 * @param this
 *		Cette instance
 *
 * @return l'identifiant de la camera
 */
NU32 NDashcam_NCameraDashcam_ObtenirIdentifiant( const NCameraDashcam* );

/**
 * Changer etat generation surface
 *
 * @param this
 *		Cette instance
 * @param estActif
 *		La generation doit etre active?
 */
void NDashcam_NCameraDashcam_DefinirGenerationSurface( NCameraDashcam*,
	NBOOL estActif );

/**
 * Obtenir dernier temps d'acquisition
 *
 * @param this
 *		Cette instance
 *
 * @return le dernier temps d'acquisition
 */
NU32 NDashcam_NCameraDashcam_ObtenirDernierTempsAcquisition( const NCameraDashcam* );

#endif // !NDASHCAM_NCAMERADASHCAM_PROTECT

