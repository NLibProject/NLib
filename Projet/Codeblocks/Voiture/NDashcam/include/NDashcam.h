#ifndef NDASHCAM_PROTECT
#define NDASHCAM_PROTECT

// ------------------
// namespace NDashcam
// ------------------

// Nom du fichier de configuration
#define NDASHCAM_NOM_FICHIER_CONFIGURATION		"NConfiguration.ini"

// namespace NLib
#include "../../../../../NLib/include/NLib/NLib.h"

// lz4 librairie
#include <lz4/lz4.h>

// namespace NVoiture
#include "../../NVoiture/include/NVoiture.h"

// namespace NDashcam::Configuration
#include "Configuration/NDashcam_Configuration.h"

// namespace NDashcam::Format
#include "Format/NDashcam_Format.h"

// namespace NDashcam::Extracteur
#include "Extracteur/NDashcam_Extracteur.h"

// struct NDashcam::NInterLancement
#include "NDashcam_NInterLancement.h"

// struct NDashcam::NCameraDashcam
#include "NDashcam_NCameraDashcam.h"

// enum NDashcam::NBoutonDashcam
#include "NDashcam_NBoutonDashcam.h"

// struct NDashcam::NDashcam
#include "NDashcam_NDashcam.h"

// namespace NDashcam::Commande
#include "Commande/NDashcam_Commande.h"

#endif // !NDASHCAM_PROTECT

