#ifndef NDASHCAM_COMMANDE_NCOMMANDE_PROTECT
#define NDASHCAM_COMMANDE_NCOMMANDE_PROTECT

// ----------------------------------
// enum NDashcam::Commande::NCommande
// ----------------------------------

typedef enum NCommande
{
	NCOMMANDE_QUITTER,

	NCOMMANDE_RESTAURER_FENETRE,

	NCOMMANDES
} NCommande;

/**
 * Parser commande
 *
 * @param commande
 *		La commande a parser
 *
 * @return la commande parsee
 */
NCommande NDashcam_Commande_NCommande_Parser( const char *commande );

/**
 * Obtenir commande
 *
 * @param commande
 *		La commande a obtenir
 *
 * @return la commande texte
 */
const char *NDashcam_Commande_NCommande_ObtenirCommande( NCommande );

/**
 * Obtenir description commande
 *
 * @param commande
 *		La commande concernee
 *
 * @return la description de la commande
 */
const char *NDashcam_Commande_NCommande_ObtenirDescriptionCommande( NCommande );

#ifdef NDASHCAM_COMMANDE_NCOMMANDE_INTERNE
static const char NCommandeTexte[ NCOMMANDES ][ 32 ] =
{
	"Exit",

	"Restaurer"
};

static const char NDescriptionCommandeTexte[ NCOMMANDES ][ 64 ] =
{
	"Fermer le programme",

	"Restaurer la fenetre SDL"
};
#endif // NDASHCAM_COMMANDE_NCOMMANDE_INTERNE

#endif // !NDASHCAM_COMMANDE_NCOMMANDE_PROTECT

