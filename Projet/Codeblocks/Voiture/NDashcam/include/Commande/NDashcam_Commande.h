#ifndef NDASHCAM_COMMANDE_PROTECT
#define NDASHCAM_COMMANDE_PROTECT

// ----------------------------
// namespace NDashcam::Commande
// ----------------------------

// enum NDashcam::Commande::NCommande
#include "NDashcam_Commande_NCommande.h"

/**
 * Thread de commandes
 *
 * @param dashcam
 *		L'instance de la dashcam
 *
 * @return si l'operation s'est bien passee
 */
__THREAD NBOOL NDashcam_Commande_Thread( NDashcam *dashcam );

#endif // !NDASHCAM_COMMANDE_PROTECT

