#ifndef NDASHCAM_NDASHCAM_PROTECT
#define NDASHCAM_NDASHCAM_PROTECT

// -------------------------
// struct NDashcam::NDashcam
// -------------------------

typedef struct NDashcam
{
    // Est en cours?
    NBOOL m_estEnCours;

    // Framerate
    NFramerate *m_framerate;

    // Fenetre
    NFenetre *m_fenetre;

    // Configuration
    const NConfiguration *m_configuration;

    // Liste des cameras
	NRechercheCamera *m_listeCamera;

	// Cameras
	NCameraDashcam **m_camera;

	// Camera visible
	NU32 m_indexCameraActive;

	// La fenetre est affichee?
	NBOOL m_estFenetreVisible;

	// Boutons
	NBouton *m_bouton[ NBOUTONS_DASHCAM ];

	// Surfaces boutons
	NSurface *m_surfaceBouton[ NBOUTONS_DASHCAM ];

	// Position souris
	NSPoint m_positionSouris;

	// Thread commande
	NThread *m_threadCommande;
} NDashcam;

/**
 * Construire
 *
 * @param configuration
 *		La configuration de la dashcam
 *
 * @return l'instance de la dashcam
 */
__ALLOC NDashcam *NDashcam_NDashcam_Construire( const NConfiguration *configuration );

/**
 * Detruire la dashcam
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_Detruire( NDashcam** );

/**
 * Lancer
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NDashcam_NDashcam_Lancer( NDashcam* );

/**
 * Masquer la fenetre
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_MasquerFenetre( NDashcam* );

/**
 * Restaurer la fenetre
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_RestaurerFenetre( NDashcam* );

/**
 * La dashcam est en cours?
 *
 * @param this
 *		Cette instance
 *
 * @return si la dashcam est en cours
 */
NBOOL NDashcam_NDashcam_EstEnCours( NDashcam* );

/**
 * Arreter la dashcam
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_Arreter( NDashcam* );

#endif // !NDASHCAM_NDASHCAM_PROTECT

