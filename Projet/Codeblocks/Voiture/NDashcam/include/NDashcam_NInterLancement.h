#ifndef NDASHCAM_NINTERLANCEMENT_PROTECT
#define NDASHCAM_NINTERLANCEMENT_PROTECT

// --------------------------------
// struct NDashcam::NInterLancement
// --------------------------------

typedef struct NInterLancement
{
	// Fichier de sauvegarde
	char *m_fichierSauvegarde;

    // Index
    NU32 m_index;
} NInterLancement;

/**
 * Construire
 *
 * @param nomFichier
 *		Le fichier intermediare
 *
 * @return l'instance
 */
__ALLOC NInterLancement *NDashcam_NInterLancement_Construire( const char *nomFichier );

/**
 * Detruire
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NInterLancement_Detruire( NInterLancement** );

/**
 * Sauvegarder nouvelles valeurs
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NDashcam_NInterLancement_Sauvegarder( NInterLancement* );

/**
 * Obtenir index
 *
 * @param this
 *		Cette instance
 *
 * @return l'index
 */
NU32 NDashcam_NInterLancement_ObtenirIndex( const NInterLancement* );

/**
 * Definir index
 *
 * @param this
 *		Cette instance
 * @param index
 *		L'index a definir
 */
 void NDashcam_NInterLancement_DefinirIndex( NInterLancement*,
	NU32 index );

#endif // !NDASHCAM_NINTERLANCEMENT_PROTECT

