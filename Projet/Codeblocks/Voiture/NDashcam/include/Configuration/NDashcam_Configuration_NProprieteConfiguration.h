#ifndef NDASHCAM_CONFIGURATION_NPROPRIETECONFIGURATION_PROTECT
#define NDASHCAM_CONFIGURATION_NPROPRIETECONFIGURATION_PROTECT

// -----------------------------------------------------
// enum NDashcam::Configuration::NProprieteConfiguration
// -----------------------------------------------------

typedef enum NProprieteConfiguration
{
	NPROPRIETE_CONFIGURATION_RESOLUTION_CAPTURE_X,
	NPROPRIETE_CONFIGURATION_RESOLUTION_CAPTURE_Y,

	NPROPRIETE_CONFIGURATION_NOM_CAPTURE,

	NPROPRIETE_CONFIGURATION_FREQUENCE_CAPTURE,
	NPROPRIETE_CONFIGURATION_NOMBRE_FRAME_IGNOREE,
	NPROPRIETE_CONFIGURATION_FPS,

	NPROPRIETE_CONFIGURATION_NOMBRE_FICHIER_CAPTURE,
	NPROPRIETE_CONFIGURATION_TAILLE_FICHIER_CAPTURE,

	NPROPRIETE_CONFIGURATION_EST_PLEIN_ECRAN,

	NPROPRIETES_CONFIGURATION
} NProprieteConfiguration;

/**
 * Obtenir ensemble clefs
 * (Seul le conteneur doit etre libere)
 *
 * @return l'ensemble des clefs
 */
__ALLOC char **NDashcam_Configuration_NProprieteConfiguration_ObtenirEnsembleClef( void );

#ifdef NDASHCAM_CONFIGURATION_NPROPRIETECONFIGURATION_INTERNE
static const char NProprieteConfigurationTexte[ NPROPRIETES_CONFIGURATION ][ 32 ] =
{
	"Resolution capture x: ",
	"Resolution capture y: ",

	"Nom capture: ",

	"Frequence capture: ",
	"Nombre frame skip: ",
	"FPS: ",

	"Nombre fichier capture: ",
	"Taille fichier capture: ",

	"Est plein ecran: "
};
#endif // NDASHCAM_CONFIGURATION_NPROPRIETECONFIGURATION_INTERNE

#endif // !NDASHCAM_CONFIGURATION_NPROPRIETECONFIGURATION_PROTECT

