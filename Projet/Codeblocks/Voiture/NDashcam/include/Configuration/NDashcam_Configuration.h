#ifndef NDASHCAM_CONFIGURATION_PROTECT
#define NDASHCAM_CONFIGURATION_PROTECT

// ---------------------------------
// namespace NDashcam::Configuration
// ---------------------------------

// enum NDashcam::Configuration::NProprieteConfiguration
#include "NDashcam_Configuration_NProprieteConfiguration.h"

// struct NDashcam::Configuration::NConfiguration
#include "NDashcam_Configuration_NConfiguration.h"

#endif // !NDASHCAM_CONFIGURATION_PROTECT

