#ifndef NDASHCAM_CONFIGURATION_NCONFIGURATION_PROTECT
#define NDASHCAM_CONFIGURATION_NCONFIGURATION_PROTECT

// ----------------------------------------------
// struct NDashcam::Configuration::NConfiguration
// ----------------------------------------------

typedef struct NConfiguration
{
    // Resolution de capture
    NUPoint m_resolutionCapture;

	// Nom de la capture
	char *m_nomCapture;

    // Nombre d'images par seconde
    NU32 m_frequenceCapture;

    // Nombre de frames skippees
    NU32 m_nombreFrameSkip;

    // FPS
    NU32 m_fps;

    // Nombre de fichiers de sauvegarde
    NU32 m_nombreFichierSauvegarde;

    // Taille des fichiers de sauvegarde
    NU32 m_tailleFichierSauvegarde;

    // Est plein ecran?
    NBOOL m_estPleinEcran;
} NConfiguration;

/**
 * Construire la configuration
 *
 * @param lien
 *		Le lien relatif du fichier de configuration
 *
 * @return l'instance de la configuration
 */
__ALLOC NConfiguration *NDashcam_Configuration_NConfiguration_Construire( const char *lien );

/**
 * Detruire la configuration
 *
 * @param this
 *		Cette instance
 */
void NDashcam_Configuration_NConfiguration_Detruire( NConfiguration** );

/**
 * Obtenir la resolution
 *
 * @param this
 *		Cette instance
 *
 * @return la resolution
 */
const NUPoint *NDashcam_Configuration_NConfiguration_ObtenirResolutionCapture( const NConfiguration* );

/**
 * Obtenir le nom de la capture
 *
 * @param this
 *		Cette instance
 *
 * @return le nom de la capture
 */
const char *NDashcam_Configuration_NConfiguration_ObtenirNomCapture( const NConfiguration* );

/**
 * Obtenir la frequence de capture
 *
 * @param this
 *		Cette instance
 *
 * @return la frequence de capture
 */
NU32 NDashcam_Configuration_NConfiguration_ObtenirFrequenceCapture( const NConfiguration* );

/**
 * Obtenir le nombre de frames a skipper
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de frames a skipper
 */
NU32 NDashcam_Configuration_NConfiguration_ObtenirNombreFrameSkip( const NConfiguration* );

/**
 * Obtenir le nombre d'images par seconde
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre d'images par seconde
 */
NU32 NDashcam_Configuration_NConfiguration_ObtenirFPS( const NConfiguration* );

/**
 * Obtenir nombre de fichier de sauvegarde
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de fichiers de sauvegarde
 */
NU32 NDashcam_Configuration_NConfiguration_ObtenirNombreFichierSauvegarde( const NConfiguration* );

/**
 * Obtenir taille fichier de sauvegarde
 *
 * @param this
 *		Cette instance
 *
 * @return la taille des fichiers de sauvegarde
 */
NU32 NDashcam_Configuration_NConfiguration_ObtenirTailleFichierSauvegarde( const NConfiguration* );

/**
 * Est plein ecran?
 *
 * @param this
 *		Cette instance
 *
 * @return si est plein ecran
 */
NBOOL NDashcam_Configuration_NConfiguration_EstPleinEcran( const NConfiguration* );

#endif // !NDASHCAM_CONFIGURATION_NCONFIGURATION_PROTECT

