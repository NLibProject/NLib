#include "../../include/NDashcam.h"

// ------------------------------
// namespace NDashcam::Extracteur
// ------------------------------

/**
 * Extraire les frames
 *
 * @param fichier
 *		Le fichier depuis lequel extraire les frames
 * @param index
 *		L'identifiant de la frame actuelle
 * @param nomCapture
 *		Le nom de la capture
 * @param identifiant
 *		L'identifiant de la capture
 * @param numeroFichier
 *		Le numero du fichie en cours de traitement
 * @param resolution
 *		La resolution des frames de ce fichier
 * @param repertoireSortie
 *		Le repertoire de sortie
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NDashcam_Extracteur_ExtraireFrameFichier( FILE *fichier,
	NU32 *index,
	const char *nomCapture,
	NU32 identifiant,
	NU32 numeroFichier,
	__OUTPUT NUPoint *resolution,
	const char *repertoireSortie )
{
	// Header fichier
	char headerFichier[ DASHCAM_TAILLE_HEA_FICHIER + 1 ];

	// Buffer
	char *buffer,
		*bufferDecompresse;

	// Position
	NU32 position;

	// BPP
	NU32 bpp;

	// Taille maximale d'une frame decompressee
	NU32 tailleMaximaleFrame;

	// Checksum
	NU32 checksum;

	// Taille frame compressee
	NU32 tailleFrameCompresse;

	// Message
	char message[ 2048 ];

	// Lien fichier sortie png
	char lienSortie[ 512 ];

	// Vider receptacle header
	memset( headerFichier,
		0,
		DASHCAM_TAILLE_HEA_FICHIER + 1 );

	// Lire header
    fread( headerFichier,
		sizeof( char ),
		DASHCAM_TAILLE_HEA_FICHIER,
		fichier );

	// Verifier header
	if( !NLib_Chaine_Comparer( DASHCAM_HEADER_FICHIER,
		headerFichier,
		NTRUE,
		0 ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Lire resolution
	fread( &resolution->x,
		sizeof( NU32 ),
		1,
		fichier );
	fread( &resolution->y,
		sizeof( NU32 ),
		1,
		fichier );

	// Lire BPP
	fread( &bpp,
		sizeof( NU32 ),
		1,
		fichier );

	// Calculer la taille maximale
	tailleMaximaleFrame = resolution->x * resolution->y * bpp;

	// Allouer la memoire
	if( !( buffer = calloc( tailleMaximaleFrame,
		sizeof( char ) ) )
		|| !( bufferDecompresse = calloc( tailleMaximaleFrame,
			sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( bufferDecompresse );

		// Quitter
		return NFALSE;
	}

	// Lire les frames
	do
	{
		// Chercher le prochain header
		if( !NLib_Fichier_Operation_PlacerChaine( fichier,
			DASHCAM_HEADER_FRAME,
			NFALSE ) )
			// Quitter
			break;

		// Enregistrer la position
		position = ftell( fichier );

		// Lire checksum
		fread( &checksum,
			sizeof( NU32 ),
			1,
			fichier );

		// Lire taille
		fread( &tailleFrameCompresse,
			sizeof( NU32 ),
			1,
			fichier );

		// Lire frame
        fread( buffer,
			sizeof( char ),
			tailleFrameCompresse,
			fichier );

		// Verifier checksum
		if( checksum != ( NLib_Memoire_CalculerCRC( buffer,
				tailleFrameCompresse )
			+ ( *( (NU8*)&tailleFrameCompresse ) + *( ( (NU8*)&tailleFrameCompresse ) + 1 ) + *( ( (NU8*)&tailleFrameCompresse ) + 2 ) + *( ( (NU8*)&tailleFrameCompresse ) + 3 ) ) ) )
		{
			// Creer message
			snprintf( message,
				2048,
				"[EXPORT DE %s.%d] Frame corrompue a [%d]octets",
				nomCapture,
				identifiant,
				position );

			// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
				message,
				NERREUR_CRC );

			// Replacer a la derniere position connue
			fseek( fichier,
				position,
				SEEK_SET );

			// Continuer
			continue;
		}

		// Decompresser la frame
		if( LZ4_decompress_safe( buffer,
			bufferDecompresse,
			tailleFrameCompresse,
			tailleMaximaleFrame ) < tailleMaximaleFrame )
		{
			// Creer message
			snprintf( message,
				2048,
				"[EXPORT DE %s.%d] Decompression echoue a [%d]octets",
				nomCapture,
				identifiant,
				position );

			// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
				message,
				NERREUR_PROTOCOL );

			// Replacer a la derniere position connue
			fseek( fichier,
				position,
				SEEK_SET );

			// Continuer
			continue;
		}

		// Creer le lien de sortie
		snprintf( lienSortie,
			512,
			"%s/%s_%d_%d.png",
			repertoireSortie,
			nomCapture,
			identifiant,
			(*index)++ );

		// Creer message
		snprintf( message,
			2048,
			"[%s.%d (%d), extraction de la frame %d (%d octets)",
			nomCapture,
			identifiant,
			numeroFichier,
			*index - 1,
			tailleFrameCompresse );

		// Notifier
		NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
			message,
			0 );

		// Inscrire la frame
		NLib_Module_PNGWriter_Inscrire( bufferDecompresse,
			resolution->x,
			resolution->y,
			bpp,
			lienSortie );

		// Delai
		NLib_Temps_Attendre( 1 );
	} while( NTRUE );

	// Liberer
	NFREE( bufferDecompresse );
	NFREE( buffer );

	// OK
	return NTRUE;
}

/**
 * Extraire frame depuis les fichiers de la dashcam
 *
 * @param nomCapture
 *		Le nom de la capture
 * @param identifiant
 *		L'identifiant de la capture
 * @param nombreMaxFichier
 *		Le nombre maximum de fichiers
 * @param qualiteExport
 *		Le CRF (allant de 0 pour lossless a 51 pour avoir de la grosse merde)
 * @param fpsExport
 *		Le nombre d'image par seconde rendues dans le fichier de sortie
 * @param estConserveIntermediaire
 *		On conserve les frames PNG intermediaires?
 * @param repertoireSortie
 *		Le repertoire de sortie du traitement
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NDashcam_Extracteur_Extraire( const char *nomCapture,
	NU32 identifiant,
	NU32 nombreMaximumFichier,
	NU32 qualiteExport,
	NU32 fpsExport,
	NBOOL estConserveIntermediaire,
	const char *repertoireSortie )
{
	// Nom fichier
	char nomFichier[ 512 ];

	// Ligne commande generation fichier video
    char ligneCommande[ 2048 ];

	// Iterateur
	NU32 i = 0;

	// Index
	NU32 index = 0;

	// Resolution
	NUPoint resolution = { 0, };

	// Resolution lue
	NUPoint resolutionLue;

	// Fichier
	FILE *fichier;

	// Export possible?
	NBOOL estExportPossible = NTRUE;

	// Message
	char message[ 2048 ];

	// Creer le repertoire de sortie
	NLib_Module_Repertoire_CreerRepertoire( repertoireSortie );

	// Parcourir les fichiers
	for( ; i < nombreMaximumFichier; i++ )
	{
		// Creer le nom
		NVoiture_Camera_CreerLienFlux( nomCapture,
			"localhost",
			identifiant,
			nomFichier,
			512,
			i );

		// Ouvrir le fichier
		if( !( fichier = fopen( nomFichier,
			"rb" ) ) )
			// Ignorer le fichier absent
			continue;

		// Extraire les frames
		NDashcam_Extracteur_ExtraireFrameFichier( fichier,
			&index,
			nomCapture,
			identifiant,
			i,
			&resolutionLue,
			repertoireSortie );

		// Verifier resolution
		if( !resolution.x
			&& !resolution.y )
			// Enregistrer
			resolution = resolutionLue;
		else if( resolution.x != resolutionLue.x
			|| resolution.y != resolutionLue.y )
		{
			// Creer message
			snprintf( message,
				2048,
				"La resolution varie pour le fichier [%s] (%dx%d != %dx%d) ==> L'export en MP4 est annule",
				nomFichier,
				resolutionLue.x,
				resolutionLue.y,
				resolution.x,
				resolution.y );

			// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
				message,
				0 );

			// Annuler l'export
			estExportPossible = NFALSE;
		}

		// Fermer le fichier
		fclose( fichier );
	}

	// On peut exporter
	if( estExportPossible )
	{
		// Creer ligne de commande export mp4
		snprintf( ligneCommande,
			2048,
			"ffmpeg -r %d -f image2 -s %dx%d -start_number 0 -i %s/%s_%d_%%d.png -vframes %d -vcodec libx264 -crf %d -pix_fmt yuv420p %s/%s_%d.mp4",
			fpsExport,
			resolution.x,
			resolution.y,
			repertoireSortie,
			nomCapture,
			identifiant,
			index,
			qualiteExport,
			repertoireSortie,
			nomCapture,
			identifiant );

		// Notifier
		NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
			"Execution de la ligne de commande:",
			0 );

		// Afficher export
		puts( ligneCommande );
		puts( "\n" );

		// Executer
		system( ligneCommande );

		// On supprime les frames intermediaires?
		if( !estConserveIntermediaire )
			for( i = 0; i < index; i++ )
			{
				// Creer nom
                snprintf( nomFichier,
					512,
					"%s/%s_%d_%d.png",
					repertoireSortie,
					nomCapture,
					identifiant,
					i );

				// Supprimer le fichier
				remove( nomFichier );
			}
	}

	// OK
	return NTRUE;
}

