#include "../include/NDashcam.h"

// -------------------------
// struct NDashcam::NDashcam
// -------------------------

/**
 * Creer surfaces
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NDashcam_NDashcam_CreerSurface( NDashcam *this )
{
	// Iterateur
	NBoutonDashcam i;

	// Police
	NPolice *police;

	// Creer la police
	if( !( police = NLib_Module_SDL_TTF_NPolice_Construire2( NDASHCAM_POLICE_BOUTON,
		NDASHCAM_TAILLE_POLICE_BOUTON,
		NDASHCAM_COULEUR_POLICE_BOUTON ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Creer surfaces
	for( i = 0; i < NBOUTONS_DASHCAM; i++ )
	{
		// Creer surface
		if( !( this->m_surfaceBouton[ i ] = NLib_Module_SDL_TTF_NPolice_CreerTexte( police,
			this->m_fenetre,
			NDashcam_NBoutonDashcam_ObtenirTexte( i ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire
			for( i--; (NS32)i >= 0; i-- )
				// Detruire
				NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceBouton[ i ] );

			// Detruire police
			NLib_Module_SDL_TTF_NPolice_Detruire( &police );

			// Quitter
			return NFALSE;
		}

		// Positionner la surface
		if( this->m_bouton[ i ] != NULL )
			NLib_Module_SDL_Surface_NSurface_DefinirPosition( this->m_surfaceBouton[ i ],
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_bouton[ i ] ).x + ( ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_bouton[ i ] ).x / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceBouton[ i ] )->x / 2 ) ),
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_bouton[ i ] ).y + ( ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_bouton[ i ] ).y / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceBouton[ i ] )->y / 2 ) ) );
	}

	// Detruire la police
	NLib_Module_SDL_TTF_NPolice_Detruire( &police );

	// OK
	return NTRUE;
}

/**
 * Construire
 *
 * @param configuration
 *		La configuration de la dashcam
 *
 * @return l'instance de la dashcam
 */
__ALLOC NDashcam *NDashcam_NDashcam_Construire( const NConfiguration *configuration )
{
	// Sortie
	__OUTPUT NDashcam *out;

	// Resolution actuelle
	NUPoint resolutionActuelle;

	// Position
	NSPoint position;

	// Taille
	NUPoint taille;

	// Couleur contour
	NCouleur couleurContour;

	// Couleur fond
	NCouleur couleurFond;

	// Iterateur
	NU32 i;

	// Doit construire ?
	NBOOL estDoitConstruire;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NDashcam ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_configuration = configuration;

	// Obtenir la resolution actuelle
	resolutionActuelle = NLib_Module_SDL_ObtenirResolutionActuelle( );

    // Construire la fenetre
    if( !( out->m_fenetre = NLib_Module_SDL_NFenetre_Construire( "NDashcam",
		resolutionActuelle,
		NDashcam_Configuration_NConfiguration_EstPleinEcran( configuration ) ) ) )
	{
        // Notifier
        NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

        // Liberer
        NFREE( out );

        // Quitter
        return NULL;
	}

	// La fenetre est visible
	out->m_estFenetreVisible = NTRUE;

	// Construire le framerate
	if( !( out->m_framerate = NLib_Temps_NFramerate_Construire( NDashcam_Configuration_NConfiguration_ObtenirFPS( configuration ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire la fenetre
		NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Rechercher les cameras
	if( !( out->m_listeCamera = NVoiture_Camera_NRechercheCamera_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire framerate
		NLib_Temps_NFramerate_Detruire( &out->m_framerate );

		// Detruire la fenetre
		NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Verifier le nombre de cameras
	if( NVoiture_Camera_NRechercheCamera_ObtenirNombre( out->m_listeCamera ) <= 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_NOT_FOUND );

		// Detruire liste cameras
        NVoiture_Camera_NRechercheCamera_Detruire( &out->m_listeCamera );

		// Detruire framerate
		NLib_Temps_NFramerate_Detruire( &out->m_framerate );

		// Detruire la fenetre
		NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out->m_camera = calloc( NVoiture_Camera_NRechercheCamera_ObtenirNombre( out->m_listeCamera ),
		sizeof( NCameraDashcam* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire liste cameras
        NVoiture_Camera_NRechercheCamera_Detruire( &out->m_listeCamera );

		// Detruire framerate
		NLib_Temps_NFramerate_Detruire( &out->m_framerate );

		// Detruire la fenetre
		NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire les cameras
	for( i = 0; i < NVoiture_Camera_NRechercheCamera_ObtenirNombre( out->m_listeCamera ); i++ )
		// Construire camera
        if( !( out->m_camera[ i ] = NDashcam_NCameraDashcam_Construire( configuration,
			out->m_fenetre,
			NVoiture_Camera_NRechercheCamera_ObtenirPeripherique( out->m_listeCamera,
				i ),
			i + 1 ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire cameras
            for( i--; (NS32)i >= 0; i-- )
				NDashcam_NCameraDashcam_Detruire( &out->m_camera[ i ] );

			// Detruire liste cameras
			NVoiture_Camera_NRechercheCamera_Detruire( &out->m_listeCamera );

			// Detruire framerate
			NLib_Temps_NFramerate_Detruire( &out->m_framerate );

			// Detruire la fenetre
			NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

			// Liberer
			NFREE( out->m_camera );
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Definir couleur contour cadre
	NDEFINIR_COULEUR_A( couleurContour,
		0x00,
		0x00,
		0x00,
		0x00 );

	// Construire les cadres
	for( i = 0; i < NBOUTONS_DASHCAM; i++ )
	{
		// On ne doit pas construire
		estDoitConstruire = NFALSE;

		// Parametrage
		switch( i )
		{
			case NBOUTON_DASHCAM_MASQUER_FENETRE:
				// Definir couleur fond
				NDEFINIR_COULEUR_A( couleurFond,
					0xFF,
					0xFF,
					0xFF,
					0x7F );

				// Definir taille
				NDEFINIR_POSITION( taille,
					NDASHCAM_TAILLE_BOUTON_MASQUAGE_FENETRE_X,
					NDASHCAM_TAILLE_BOUTON_MASQUAGE_FENETRE_Y );

				// Definir position
				NDEFINIR_POSITION( position,
					( NLib_Module_SDL_NFenetre_ObtenirResolution( out->m_fenetre )->x / 2 ) - ( taille.x / 2 ),
					NDASHCAM_MARGE_BOUTON );
				break;

			case NBOUTON_DASHCAM_MASQUER_MONTRER_UI:
				// Definir couleur fond
				NDEFINIR_COULEUR_A( couleurFond,
					0xFF,
					0xFF,
					0xFF,
					0x7F );

				// Definir taille
				NDEFINIR_POSITION( taille,
					NDASHCAM_TAILLE_BOUTON_MASQUAGE_FENETRE_X,
					NDASHCAM_TAILLE_BOUTON_MASQUAGE_FENETRE_Y );

				// Definir position
				NDEFINIR_POSITION( position,
					( NLib_Module_SDL_NFenetre_ObtenirResolution( out->m_fenetre )->x / 2 ) - ( taille.x / 2 ),
					NLib_Module_SDL_NFenetre_ObtenirResolution( out->m_fenetre )->y - taille.y - NDASHCAM_MARGE_BOUTON );
				break;

			case NBOUTON_DASHCAM_SOURCE_PRECEDENTE:
			case NBOUTON_DASHCAM_SOURCE_SUIVANTE:
				// Definir couleur fond
				NDEFINIR_COULEUR_A( couleurFond,
					0xFF,
					0xFF,
					0xFF,
					0x7F );

				// Definir taille
				NDEFINIR_POSITION( taille,
					NDASHCAM_TAILLE_BOUTON_SOURCE_X,
					NDASHCAM_TAILLE_BOUTON_SOURCE_Y );

				// Definir position
				switch( i )
				{
					case NBOUTON_DASHCAM_SOURCE_PRECEDENTE:
						// Definir position
						NDEFINIR_POSITION( position,
							NDASHCAM_MARGE_BOUTON,
							NDASHCAM_MARGE_BOUTON );
						break;
					case NBOUTON_DASHCAM_SOURCE_SUIVANTE:
						// Definir position
						NDEFINIR_POSITION( position,
							NLib_Module_SDL_NFenetre_ObtenirResolution( out->m_fenetre )->x - NDASHCAM_TAILLE_BOUTON_SOURCE_X - NDASHCAM_MARGE_BOUTON,
							NDASHCAM_MARGE_BOUTON );
						break;
				}
				break;
		}

		switch( i )
		{
			case NBOUTON_DASHCAM_MASQUER_MONTRER_UI:
            case NBOUTON_DASHCAM_MASQUER_FENETRE:
				// On doit construire
				estDoitConstruire = NTRUE;
				break;

			case NBOUTON_DASHCAM_SOURCE_PRECEDENTE:
			case NBOUTON_DASHCAM_SOURCE_SUIVANTE:
				if( NVoiture_Camera_NRechercheCamera_ObtenirNombre( out->m_listeCamera ) > 1 )
					// On doit construire
					estDoitConstruire = NTRUE;
				break;

			default:
				break;
		}

		// Construire le bouton
		if( estDoitConstruire )
			if( !( out->m_bouton[ i ] = NLib_Module_SDL_Bouton_NBouton_Construire( position,
				taille,
				couleurContour,
				couleurFond,
				out->m_fenetre,
				NDASHCAM_EPAISSEUR_BORDURE_BOUTON ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Detruire cadres
				for( i--; (NS32)i >= 0; i-- )
					if( out->m_bouton[ i ] != NULL )
						NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_bouton[ i ] );

				// Detruire cameras
				for( i = 0; i < NVoiture_Camera_NRechercheCamera_ObtenirNombre( out->m_listeCamera ); i++ )
					NDashcam_NCameraDashcam_Detruire( &out->m_camera[ i ] );

				// Detruire liste cameras
				NVoiture_Camera_NRechercheCamera_Detruire( &out->m_listeCamera );

				// Detruire framerate
				NLib_Temps_NFramerate_Detruire( &out->m_framerate );

				// Detruire la fenetre
				NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

				// Liberer
				NFREE( out->m_camera );
				NFREE( out );

				// Quitter
				return NULL;
			}
	}

	// Creer surfaces boutons
	if( !NDashcam_NDashcam_CreerSurface( out ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Detruire cadres
		for( i = 0; i < NBOUTONS_DASHCAM; i++ )
			if( out->m_bouton[ i ] != NULL )
				NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_bouton[ i ] );

		// Detruire cameras
		for( i = 0; i < NVoiture_Camera_NRechercheCamera_ObtenirNombre( out->m_listeCamera ); i++ )
			NDashcam_NCameraDashcam_Detruire( &out->m_camera[ i ] );

		// Detruire liste cameras
		NVoiture_Camera_NRechercheCamera_Detruire( &out->m_listeCamera );

		// Detruire framerate
		NLib_Temps_NFramerate_Detruire( &out->m_framerate );

		// Detruire la fenetre
		NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

		// Liberer
		NFREE( out->m_camera );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Lancer le thread de commande
	if( !( out->m_threadCommande = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NDashcam_Commande_Thread,
		out,
		NULL ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire cadres/surfaces
		for( i = 0; i < NBOUTONS_DASHCAM; i++ )
		{
			// Cadre
			if( out->m_bouton[ i ] != NULL )
				NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_bouton[ i ] );

			// Surface
			NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfaceBouton[ i ] );
		}

		// Detruire cameras
		for( i = 0; i < NVoiture_Camera_NRechercheCamera_ObtenirNombre( out->m_listeCamera ); i++ )
			NDashcam_NCameraDashcam_Detruire( &out->m_camera[ i ] );

		// Detruire liste cameras
		NVoiture_Camera_NRechercheCamera_Detruire( &out->m_listeCamera );

		// Detruire framerate
		NLib_Temps_NFramerate_Detruire( &out->m_framerate );

		// Detruire la fenetre
		NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

		// Liberer
		NFREE( out->m_camera );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire la dashcam
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_Detruire( NDashcam **this )
{
	// Iterateur
	NU32 i = 0;

	// Detruire thread commande
	NLib_Thread_NThread_Detruire( &(*this)->m_threadCommande );

	// Detruire les cadres/surfaces
	for( ; i < NBOUTONS_DASHCAM; i++ )
	{
		// Bouton existe?
		if( (*this)->m_bouton[ i ] != NULL )
			// Detruire bouton
			NLib_Module_SDL_Bouton_NBouton_Detruire( &(*this)->m_bouton[ i ] );

		// Surface
		NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfaceBouton[ i ] );
	}

	// Detruire les cameras
	for( i = 0; i < NVoiture_Camera_NRechercheCamera_ObtenirNombre( (*this)->m_listeCamera ); i++ )
		// Detruire camera
		NDashcam_NCameraDashcam_Detruire( &(*this)->m_camera[ i ] );

	// Detruire la liste des cameras
	NVoiture_Camera_NRechercheCamera_Detruire( &(*this)->m_listeCamera );

	// Detruire le framerate
	NLib_Temps_NFramerate_Detruire( &(*this)->m_framerate );

	// Detruire la fenetre
	NLib_Module_SDL_NFenetre_Detruire( &(*this)->m_fenetre );

	// Liberer
	NFREE( (*this)->m_camera );
	NFREE( *this );
}

/**
 * Desactiver l'affichage de toutes les cameras
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_DesactiverEnsembleCamera( NDashcam *this )
{
	// Iterateur
	NU32 i = 0;

	// Desactiver la creation de surface pour toutes les cameras
	for( ; i < NVoiture_Camera_NRechercheCamera_ObtenirNombre( this->m_listeCamera ); i++ )
		// Desactiver camera
		NDashcam_NCameraDashcam_DefinirGenerationSurface( this->m_camera[ i ],
			NFALSE );

}

/**
 * Activer une camera
 *
 * @param this
 *		Cette instance
 * @param camera
 *		L'index de la camera a activer
 */
void NDashcam_NDashcam_ActiverCamera( NDashcam *this,
	NU32 camera )
{
	// Desactiver toutes les cameras
	NDashcam_NDashcam_DesactiverEnsembleCamera( this );

	// Activer la camera
	NDashcam_NCameraDashcam_DefinirGenerationSurface( this->m_camera[ camera ],
		NTRUE );

	// Enregistrer
	this->m_indexCameraActive = camera;
}

/**
 * Activer camera suivante
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_ActiverCameraSuivante( NDashcam *this )
{
	NDashcam_NDashcam_ActiverCamera( this,
		this->m_indexCameraActive >= NVoiture_Camera_NRechercheCamera_ObtenirNombre( this->m_listeCamera ) - 1 ?
			0
			: this->m_indexCameraActive + 1 );
}

/**
 * Activer camera precedente
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_ActiverCameraPrecedente( NDashcam *this )
{
    NDashcam_NDashcam_ActiverCamera( this,
		this->m_indexCameraActive <= 0 ?
			NVoiture_Camera_NRechercheCamera_ObtenirNombre( this->m_listeCamera ) - 1
			: this->m_indexCameraActive - 1 );
}

/**
 * Afficher camera
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_AfficherCamera( NDashcam *this )
{
	// Surface
	NSurface *surface;

	// Proteger camera
	NDashcam_NCameraDashcam_ActiverProtection( this->m_camera[ this->m_indexCameraActive ] );

	// Verifier qu'il y ait une surface
	if( ( surface = (NSurface*)NDashcam_NCameraDashcam_ObtenirSurface( this->m_camera[ this->m_indexCameraActive ] ) ) != NULL )
	{
		// Definir la taille
		NLib_Module_SDL_Surface_NSurface_DefinirTaille( surface,
			NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x,
			NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y );

		// Afficher la surface
		NLib_Module_SDL_Surface_NSurface_Afficher( surface );
	}

	// Ne plus proteger camera
	NDashcam_NCameraDashcam_DesactiverProtection( this->m_camera[ this->m_indexCameraActive ] );
}

/**
 * Afficher UI
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_AfficherUI( NDashcam *this )
{
	// Iterateur
	NU32 i = 0;

	// Afficher cadres
	for( ; i < NBOUTONS_DASHCAM; i++ )
		// Cadre existe?
		if( this->m_bouton[ i ] != NULL
			&& NLib_Module_SDL_Bouton_NBouton_EstVisible( this->m_bouton[ i ] ) )
		{
			// Afficher bouton
			NLib_Module_SDL_Bouton_NBouton_Dessiner( this->m_bouton[ i ] );

			// Afficher texte
			NLib_Module_SDL_Surface_NSurface_Afficher( this->m_surfaceBouton[ i ] );
		}
}

/**
 * Switcher la visibilite de l'UI
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_SwitcherUI( NDashcam *this )
{
	// Iterateur
	NBoutonDashcam i = (NBoutonDashcam)0;

	// Switcher
	for( ; i < NBOUTONS_DASHCAM; i++ )
		// Bouton existe?
		if( this->m_bouton[ i ] != NULL )
			// Inverser etat
			NLib_Module_SDL_Bouton_NBouton_DefinirEstVisible( this->m_bouton[ i ],
				!NLib_Module_SDL_Bouton_NBouton_EstVisible( this->m_bouton[ i ] ) );
}

/**
 * Gerer les evenements
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_UpdateFenetre( NDashcam *this )
{
	// Evenement
	SDL_Event e;

	// Iterateur
	NU32 i;

	// Quitter si fenetre masquee
	if( !this->m_estFenetreVisible )
		return;

	// Nettoyer la fenetre
	NLib_Module_SDL_NFenetre_Nettoyer( this->m_fenetre );

	// Recuperer evenement
	SDL_PollEvent( &e );

	// Analyser evenement
	switch( e.type )
	{
		case SDL_QUIT:
			this->m_estEnCours = NFALSE;
			break;

		case SDL_KEYDOWN:
			switch( e.key.keysym.sym )
			{
				case SDLK_LEFT:
					NDashcam_NDashcam_ActiverCameraPrecedente( this );
					break;

				case SDLK_RIGHT:
					NDashcam_NDashcam_ActiverCameraSuivante( this );
					break;

				case SDLK_ESCAPE:
					this->m_estEnCours = NFALSE;
					break;

				case SDLK_h:
					NDashcam_NDashcam_SwitcherUI( this );
					break;

				case SDLK_m:
					NDashcam_NDashcam_MasquerFenetre( this );
					break;

				default:
					break;
			}
			break;

		case SDL_MOUSEMOTION:
			// Enregistrer
			NDEFINIR_POSITION( this->m_positionSouris,
				e.motion.x,
				e.motion.y );
			break;

		default:
			break;
	}

	// Mettre a jour les boutons
	for( i = 0; i < NBOUTONS_DASHCAM; i++ )
		// Bouton existe?
		if( this->m_bouton[ i ] != NULL )
		{
			// Update
			NLib_Module_SDL_Bouton_NBouton_Update( this->m_bouton[ i ],
				&this->m_positionSouris,
				&e );

			// Traiter etat
			switch( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_bouton[ i ] ) )
			{
				case NETAT_BOUTON_PRESSE:
					switch( i )
					{
						case NBOUTON_DASHCAM_SOURCE_PRECEDENTE:
							NDashcam_NDashcam_ActiverCameraPrecedente( this );
							break;
						case NBOUTON_DASHCAM_SOURCE_SUIVANTE:
							NDashcam_NDashcam_ActiverCameraSuivante( this );
							break;

						case NBOUTON_DASHCAM_MASQUER_FENETRE:
							NDashcam_NDashcam_MasquerFenetre( this );
							break;

						case NBOUTON_DASHCAM_MASQUER_MONTRER_UI:
							NDashcam_NDashcam_SwitcherUI( this );
							break;

						default:
							break;
					}

					// Reset etat
                    NLib_Module_SDL_Bouton_NBouton_RemettreEtatAZero( this->m_bouton[ i ] );

					break;

				default:
					break;
			}
		}

	// Afficher camera
	NDashcam_NDashcam_AfficherCamera( this );

	// Afficher UI
	NDashcam_NDashcam_AfficherUI( this );

	// Actualiser la fenetre
	NLib_Module_SDL_NFenetre_Update( this->m_fenetre );
}

/**
 * Lancer
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NDashcam_NDashcam_Lancer( NDashcam *this )
{
	// Lancer
	this->m_estEnCours = NTRUE;

	// Activer 0
	NDashcam_NDashcam_ActiverCamera( this,
		0 );

	// Boucle principale
	do
	{
		// Debuter fonction
		NLib_Temps_NFramerate_DebuterFonction( this->m_framerate );

		// Gerer les evenements
		NDashcam_NDashcam_UpdateFenetre( this );

		// Terminer la fonction
		NLib_Temps_NFramerate_TerminerFonction( this->m_framerate );
	} while( this->m_estEnCours );

	// OK
	return NTRUE;
}

/**
 * Masquer la fenetre
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_MasquerFenetre( NDashcam *this )
{
	// On ne voit plus la fenetre
    this->m_estFenetreVisible = NFALSE;

	// Masquer fenetre
    NLib_Module_SDL_NFenetre_CacheFenetre( this->m_fenetre );

    // Desactiver toutes les cameras
    NDashcam_NDashcam_DesactiverEnsembleCamera( this );
}

/**
 * Restaurer la fenetre
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_RestaurerFenetre( NDashcam *this )
{
	// Activer camera
	NDashcam_NDashcam_ActiverCamera( this,
		this->m_indexCameraActive );

    // Restaurer fenetre
    NLib_Module_SDL_NFenetre_AfficherFenetre( this->m_fenetre );

    // On voit la fenetre
    this->m_estFenetreVisible = NTRUE;
}

/**
 * La dashcam est en cours?
 *
 * @param this
 *		Cette instance
 *
 * @return si la dashcam est en cours
 */
NBOOL NDashcam_NDashcam_EstEnCours( NDashcam *this )
{
	return this->m_estEnCours;
}

/**
 * Arreter la dashcam
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NDashcam_Arreter( NDashcam *this )
{
	this->m_estEnCours = NFALSE;
}

