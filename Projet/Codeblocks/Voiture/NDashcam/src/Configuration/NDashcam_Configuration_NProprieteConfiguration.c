#define NDASHCAM_CONFIGURATION_NPROPRIETECONFIGURATION_INTERNE
#include "../../include/NDashcam.h"

// -----------------------------------------------------
// enum NDashcam::Configuration::NProprieteConfiguration
// -----------------------------------------------------

/**
 * Obtenir ensemble clefs
 * (Seul le conteneur doit etre libere)
 *
 * @return l'ensemble des clefs
 */
__ALLOC char **NDashcam_Configuration_NProprieteConfiguration_ObtenirEnsembleClef( void )
{
	// Sortie
	__OUTPUT char **out;

	// Iterateur
	NU32 i;

	// Allouer la memoire
	if( !( out = calloc( NPROPRIETES_CONFIGURATION,
		sizeof( char* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	for( i = 0; i < NPROPRIETES_CONFIGURATION; i++ )
		out[ i ] = (char*)NProprieteConfigurationTexte[ i ];

	// OK
	return out;
}

