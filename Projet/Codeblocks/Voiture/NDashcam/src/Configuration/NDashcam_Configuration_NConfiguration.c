#include "../../include/NDashcam.h"

// ----------------------------------------------
// struct NDashcam::Configuration::NConfiguration
// ----------------------------------------------

/**
 * Construire la configuration
 *
 * @param lien
 *		Le lien relatif du fichier de configuration
 *
 * @return l'instance de la configuration
 */
__ALLOC NConfiguration *NDashcam_Configuration_NConfiguration_Construire( const char *lien )
{
	// Sortie
	__OUTPUT NConfiguration *out;

	// Ensemble des clefs
	char **clefs;

	// Fichier de configuration
	NFichierClef *fichier;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NConfiguration ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Obtenir les clefs
	if( !( clefs = NDashcam_Configuration_NProprieteConfiguration_ObtenirEnsembleClef( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_EXPORT_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Charger le fichier
	if( !( fichier = NLib_Fichier_Clef_NFichierClef_Construire( lien,
		(const char**)clefs,
		NPROPRIETES_CONFIGURATION ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( clefs );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Liberer clefs
	NFREE( clefs );

	// Lire proprietes
		// Resolution
			NDEFINIR_POSITION( out->m_resolutionCapture,
				NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
					NPROPRIETE_CONFIGURATION_RESOLUTION_CAPTURE_X ),
				NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
					NPROPRIETE_CONFIGURATION_RESOLUTION_CAPTURE_Y ) );
		// Nom capture
			out->m_nomCapture = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( fichier,
				NPROPRIETE_CONFIGURATION_NOM_CAPTURE );
		// Frequence de capture
			out->m_frequenceCapture = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_FREQUENCE_CAPTURE );
		// Nombre de frame skip
			out->m_nombreFrameSkip = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_NOMBRE_FRAME_IGNOREE );
		// FPS
			out->m_fps = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_FPS );
		// Nombre de fichiers de sauvegarde
			out->m_nombreFichierSauvegarde = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_NOMBRE_FICHIER_CAPTURE );
		// Taille des fichiers de sauvegarde
			out->m_tailleFichierSauvegarde = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
				NPROPRIETE_CONFIGURATION_TAILLE_FICHIER_CAPTURE );
		// Est plein ecran?
			out->m_estPleinEcran = NLib_Chaine_Comparer( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( fichier,
					NPROPRIETE_CONFIGURATION_EST_PLEIN_ECRAN ),
				"Oui",
				NFALSE,
				0 );

	// Detruire fichier
	NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

	// OK
	return out;
}

/**
 * Detruire la configuration
 *
 * @param this
 *		Cette instance
 */
void NDashcam_Configuration_NConfiguration_Detruire( NConfiguration **this )
{
	// Liberer
	NFREE( (*this)->m_nomCapture );
	NFREE( *this );
}

/**
 * Obtenir la resolution
 *
 * @param this
 *		Cette instance
 *
 * @return la resolution
 */
const NUPoint *NDashcam_Configuration_NConfiguration_ObtenirResolutionCapture( const NConfiguration *this )
{
	return &this->m_resolutionCapture;
}

/**
 * Obtenir le nom de la capture
 *
 * @param this
 *		Cette instance
 *
 * @return le nom de la capture
 */
const char *NDashcam_Configuration_NConfiguration_ObtenirNomCapture( const NConfiguration *this )
{
	return this->m_nomCapture;
}

/**
 * Obtenir la frequence de capture
 *
 * @param this
 *		Cette instance
 *
 * @return la frequence de capture
 */
NU32 NDashcam_Configuration_NConfiguration_ObtenirFrequenceCapture( const NConfiguration *this )
{
	return this->m_frequenceCapture;
}

/**
 * Obtenir le nombre de frames a skipper
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de frames a skipper
 */
NU32 NDashcam_Configuration_NConfiguration_ObtenirNombreFrameSkip( const NConfiguration *this )
{
	return this->m_nombreFrameSkip;
}

/**
 * Obtenir le nombre d'images par seconde
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre d'images par seconde
 */
NU32 NDashcam_Configuration_NConfiguration_ObtenirFPS( const NConfiguration *this )
{
	return this->m_fps;
}

/**
 * Obtenir nombre de fichier de sauvegarde
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de fichiers de sauvegarde
 */
NU32 NDashcam_Configuration_NConfiguration_ObtenirNombreFichierSauvegarde( const NConfiguration *this )
{
	return this->m_nombreFichierSauvegarde;
}

/**
 * Obtenir taille fichier de sauvegarde
 *
 * @param this
 *		Cette instance
 *
 * @return la taille des fichiers de sauvegarde
 */
NU32 NDashcam_Configuration_NConfiguration_ObtenirTailleFichierSauvegarde( const NConfiguration *this )
{
	return this->m_tailleFichierSauvegarde;
}

/**
 * Est plein ecran?
 *
 * @param this
 *		Cette instance
 *
 * @return si plein ecran
 */
NBOOL NDashcam_Configuration_NConfiguration_EstPleinEcran( const NConfiguration *this )
{
	return this->m_estPleinEcran;
}



