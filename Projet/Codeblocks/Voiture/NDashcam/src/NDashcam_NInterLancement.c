#include "../include/NDashcam.h"

// --------------------------------
// struct NDashcam::NInterLancement
// --------------------------------

/**
 * Construire
 *
 * @param nomFichier
 *		Le fichier intermediare
 *
 * @return l'instance
 */
__ALLOC NInterLancement *NDashcam_NInterLancement_Construire( const char *nomFichier )
{
	// Sortie
	__OUTPUT NInterLancement *out = NULL;

	// Fichier
	NFichierTexte *fichier;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NInterLancement ) ) )
		|| !( out->m_fichierSauvegarde = calloc( strlen( nomFichier ) + 1,
			sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	memcpy( out->m_fichierSauvegarde,
		nomFichier,
		strlen( nomFichier ) );

	// Tenter d'ouvrir le fichier
	if( ( fichier = NLib_Fichier_NFichierTexte_ConstruireLecture( nomFichier ) ) )
	{
		// Lire valeurs
		out->m_index = NLib_Fichier_NFichierTexte_LireNombre( fichier,
			NFALSE,
			NFALSE );

		// Fermer le fichier
		NLib_Fichier_NFichierTexte_Detruire( &fichier );
	}

	// OK
	return out;
}

/**
 * Detruire
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NInterLancement_Detruire( NInterLancement **this )
{
	NFREE( (*this)->m_fichierSauvegarde );
	NFREE( (*this) );
}

/**
 * Sauvegarder nouvelles valeurs
 *
 * @param this
 *		Cette instance
 * @param dernierFichierSupprime
 *		Le dernier fichier supprime
 * @param index
 *		L'index
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NDashcam_NInterLancement_Sauvegarder( NInterLancement *this )
{
	// Fichier
	NFichierTexte *fichier;

	// Ouvrir le fichier
	if( !( fichier = NLib_Fichier_NFichierTexte_ConstruireEcriture( this->m_fichierSauvegarde,
		NTRUE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Ecrire
	NLib_Fichier_NFichierTexte_Ecrire( fichier,
		this->m_index );
	NLib_Fichier_NFichierTexte_Ecrire6( fichier,
		';' );

	// Fermer le fichier
	NLib_Fichier_NFichierTexte_Detruire( &fichier );

	// OK
	return NTRUE;
}

/**
 * Obtenir index
 *
 * @param this
 *		Cette instance
 *
 * @return l'index
 */
NU32 NDashcam_NInterLancement_ObtenirIndex( const NInterLancement *this )
{
	return this->m_index;
}

/**
 * Definir index
 *
 * @param this
 *		Cette instance
 * @param index
 *		L'index a definir
 */
 void NDashcam_NInterLancement_DefinirIndex( NInterLancement *this,
	NU32 index )
{
	this->m_index = index;
}

