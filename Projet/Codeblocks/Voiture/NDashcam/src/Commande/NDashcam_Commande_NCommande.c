#define NDASHCAM_COMMANDE_NCOMMANDE_INTERNE
#include "../../include/NDashcam.h"

/**
 * Parser commande
 *
 * @param commande
 *		La commande a parser
 *
 * @return la commande parsee
 */
NCommande NDashcam_Commande_NCommande_Parser( const char *commande )
{
	// Sortie
	__OUTPUT NCommande out = (NCommande)0;

	// Nombre?
	if( NLib_Chaine_EstUnNombre( commande ) )
		return ( out = (NCommande)strtol( commande,
			NULL,
			10 ) ) >= NCOMMANDES ?
				(NCommande)NERREUR
				: out;

	// Chercher
	for( ; out < NCOMMANDES; out++ )
		// Verifier
		if( NLib_Chaine_Comparer( NCommandeTexte[ out ],
			commande,
			NFALSE,
			0 ) )
			// OK
			return out;

	// Introuvable
	return (NCommande)NERREUR;
}

/**
 * Obtenir commande
 *
 * @param commande
 *		La commande a obtenir
 *
 * @return la commande texte
 */
const char *NDashcam_Commande_NCommande_ObtenirCommande( NCommande commande )
{
	return NCommandeTexte[ commande ];
}

/**
 * Obtenir description commande
 *
 * @param commande
 *		La commande concernee
 *
 * @return la description de la commande
 */
const char *NDashcam_Commande_NCommande_ObtenirDescriptionCommande( NCommande commande )
{
	return NDescriptionCommandeTexte[ commande ];
}

