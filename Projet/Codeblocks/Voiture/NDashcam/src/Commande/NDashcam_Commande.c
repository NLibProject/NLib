#include "../../include/NDashcam.h"

// ----------------------------
// namespace NDashcam::Commande
// ----------------------------

/**
 * Lister les commandes
 */
void NDashcam_Commande_Lister( void )
{
	// Iterateur
	NCommande i = (NCommande)0;

	// Lister
	for( ; i < NCOMMANDES; i++ )
		printf( "[%d]: \"%s\" (%s)\n",
			i,
			NDashcam_Commande_NCommande_ObtenirCommande( i ),
			NDashcam_Commande_NCommande_ObtenirDescriptionCommande( i ) );
}

/**
 * Thread de commandes
 *
 * @param dashcam
 *		L'instance de la dashcam
 *
 * @return si l'operation s'est bien passee
 */
__THREAD NBOOL NDashcam_Commande_Thread( NDashcam *dashcam )
{
	// Buffer
	char buffer[ 2048 ];

	// Boucle commande
	do
	{
		// Afficher liste commandes
        NDashcam_Commande_Lister( );

		// Lire
		scanf( "%s",
			buffer );

		// Parser
		switch( NDashcam_Commande_NCommande_Parser( buffer ) )
		{
			case NCOMMANDE_QUITTER:
                NDashcam_NDashcam_Arreter( dashcam );
                break;

            case NCOMMANDE_RESTAURER_FENETRE:
				// Masquer
				NDashcam_NDashcam_MasquerFenetre( dashcam );

				// Afficher
				NDashcam_NDashcam_RestaurerFenetre( dashcam );
				break;

			default:
				puts( "Commande inconnue.\n" );
				break;
		}
	} while( NDashcam_NDashcam_EstEnCours( dashcam ) );

	// OK
	return NTRUE;
}
