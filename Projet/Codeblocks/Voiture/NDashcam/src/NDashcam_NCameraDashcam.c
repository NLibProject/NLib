#include "../include/NDashcam.h"

// -------------------------------
// struct NDashcam::NCameraDashcam
// -------------------------------

/**
 * Creer le lien du flux
 *
 * @param this
 *		Cette instance
 * @param out
 *		La sortie
 * @param tailleOut
 *		La taille de la sortie
 */
void NDashcam_NCameraDashcam_CreerLienWriter( NCameraDashcam *this,
	__OUTPUT char *out,
	NU32 tailleOut )
{
	NVoiture_Camera_CreerLienFlux( NDashcam_Configuration_NConfiguration_ObtenirNomCapture( this->m_configuration ),
		"localhost",
		this->m_identifiant,
		out,
		tailleOut,
		NDashcam_NInterLancement_ObtenirIndex( this->m_interLancement ) );
}

/**
 * Creer le writer mpeg
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NCameraDashcam_CreerWriterMPEG( NCameraDashcam *this )
{
	// Buffer
	NU32 buffer;

	// Lien
	char lien[ 512 ];

	// Message
	char message[ 2048 ];

	// Detruire l'ancien writer
	if( this->m_writer != NULL )
		NLib_Module_MPEGWriter_NMPEGWriter_Detruire( &this->m_writer );

	// Creer le lien du flux actuel
	NDashcam_NCameraDashcam_CreerLienWriter( this,
		lien,
		512 );

	// Creer le writer
	this->m_writer = NLib_Module_MPEGWriter_NMPEGWriter_Construire( lien,
		NFALSE );

	// Se placer a la fin du fichier
	fseek( NLib_Module_MPEGWriter_NMPEGWriter_ObtenirFichier( this->m_writer ),
		0,
		SEEK_END );

	// Fichier plein?
	if( NLib_Module_MPEGWriter_NMPEGWriter_ObtenirTaille( this->m_writer ) >= NDashcam_Configuration_NConfiguration_ObtenirTailleFichierSauvegarde( this->m_configuration ) )
	{
		// Fermer writer
		NLib_Module_MPEGWriter_NMPEGWriter_Detruire( &this->m_writer );

		// Incrementer curseur
		NDashcam_NInterLancement_DefinirIndex( this->m_interLancement,
			NDashcam_NInterLancement_ObtenirIndex( this->m_interLancement ) + 1 >= NDashcam_Configuration_NConfiguration_ObtenirNombreFichierSauvegarde( this->m_configuration ) ?
				0
				: NDashcam_NInterLancement_ObtenirIndex( this->m_interLancement ) + 1 );

		// Creer lien writer
		NDashcam_NCameraDashcam_CreerLienWriter( this,
			lien,
			512 );

		// Construire le writer
		this->m_writer = NLib_Module_MPEGWriter_NMPEGWriter_Construire( lien,
			NTRUE );
	}

	// Enregistrer
	NDashcam_NInterLancement_Sauvegarder( this->m_interLancement );

	// Creer le message
	snprintf( message,
		2048,
		"[%s]\tEcriture du flux \"%s\" dans le fichier \"%s\"",
		"localhost",
		NDashcam_Configuration_NConfiguration_ObtenirNomCapture( this->m_configuration ),
		lien );

	// Notifier
	NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
		message,
		0 );

	// Se placer a la fin du fichier
	fseek( NLib_Module_MPEGWriter_NMPEGWriter_ObtenirFichier( this->m_writer ),
		0,
		SEEK_END );

	// Si le fichier est malforme
	if( NLib_Module_MPEGWriter_NMPEGWriter_ObtenirTaille( this->m_writer ) < DASHCAM_TAILLE_HEA_FICHIER + sizeof( NU32 ) * 3 )
	{
		// Le fichier n'est pas vide?
		if( NLib_Module_MPEGWriter_NMPEGWriter_ObtenirTaille( this->m_writer ) != 0 )
			// Vider le fichier
			NLib_Module_MPEGWriter_NMPEGWriter_ViderContenu( this->m_writer );

		// Inscrire l'header
		fwrite( DASHCAM_HEADER_FICHIER,
			sizeof( char ),
			DASHCAM_TAILLE_HEA_FICHIER,
			NLib_Module_MPEGWriter_NMPEGWriter_ObtenirFichier( this->m_writer ) );

		// Inscrire resolution x
		buffer = NLib_Module_Webcam_NCamera_ObtenirResolution( this->m_camera )->x;
		fwrite( &buffer,
			sizeof( NU32 ),
			1,
			NLib_Module_MPEGWriter_NMPEGWriter_ObtenirFichier( this->m_writer ) );

		// Inscrire resolution y
		buffer = NLib_Module_Webcam_NCamera_ObtenirResolution( this->m_camera )->y;
		fwrite( &buffer,
			sizeof( NU32 ),
			1,
			NLib_Module_MPEGWriter_NMPEGWriter_ObtenirFichier( this->m_writer ) );

		// Inscrire bpp
		buffer = NLib_Module_Webcam_NCamera_ObtenirNombreOctetParPixel( this->m_camera );
		fwrite( &buffer,
			sizeof( NU32 ),
			1,
			NLib_Module_MPEGWriter_NMPEGWriter_ObtenirFichier( this->m_writer ) );
	}
}

/**
 * Inscrire une frame
 *
 * @param this
 *		Cette instance
 * @param frame
 *		La frame a inscrire
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NDashcam_NCameraDashcam_InscrireFrame( NCameraDashcam *this,
	NImageCamera *frame )
{
	// Fichier
	FILE *fichier;

	// Taille
	NU32 taille;

	// Buffer
	NU32 buffer;

	// Se placer a la fin du fichier
	fseek( NLib_Module_MPEGWriter_NMPEGWriter_ObtenirFichier( this->m_writer ),
		0,
		SEEK_END );

	// Verifier le writer
	if( NLib_Module_MPEGWriter_NMPEGWriter_ObtenirTaille( this->m_writer ) >= NDashcam_Configuration_NConfiguration_ObtenirTailleFichierSauvegarde( this->m_configuration ) )
		// Creer nouveau writer
		NDashcam_NCameraDashcam_CreerWriterMPEG( this );

	// Obtenir fichier
	fichier = NLib_Module_MPEGWriter_NMPEGWriter_ObtenirFichier( this->m_writer );

	// Compresser image
	taille = LZ4_compress_default( NLib_Module_Webcam_NImageCamera_ObtenirData( frame ),
		this->m_buffer,
		NLib_Module_Webcam_NImageCamera_ObtenirTaille( frame ),
		this->m_tailleBuffer );

	// Inscrire header
	fwrite( DASHCAM_HEADER_FRAME,
		sizeof( char ),
		DASHCAM_TAILLE_HEADER,
		fichier );

	// Calculer checksum
	buffer = NLib_Memoire_CalculerCRC( this->m_buffer,
			taille )
		+ *( ( (NU8*)&taille ) ) + *( ( (NU8*)&taille ) + 1 ) + *( ( (NU8*)&taille ) + 2 ) + *( ( (NU8*)&taille ) + 3 );

	// Inscrire checksum
	fwrite( &buffer,
		sizeof( NU32 ),
		1,
		fichier );

	// Inscrire taille
	fwrite( &taille,
		sizeof( NU32 ),
		1,
		fichier );

	// Inscrire frame
	fwrite( this->m_buffer,
		sizeof( char ),
		taille,
		fichier );

	// OK
	return NTRUE;
}

/**
 * Generer surface
 *
 * @param this
 *		Cette instance
 * @param frame
 *		La frame a transformer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NDashcam_NCameraDashcam_GenererSurface( NCameraDashcam *this,
	NImageCamera *frame )
{
	// Surface
	SDL_Surface *surface;

	// Ancienne surface
	NSurface *ancienneSurface;

	// Nouvelle surface
	NSurface *nouvelleSurface;

	// Iterateur
	NU32 i;

	// Data
	char *data;
	NU32 tailleData;

    // Verifier
    if( !this->m_estDoitGenererSurface )
        // Quitter
		return NFALSE;

	// Obtenir donnees
	data = (char*)NLib_Module_Webcam_NImageCamera_ObtenirData( frame );
	tailleData = NLib_Module_Webcam_NImageCamera_ObtenirTaille( frame );

	// Inverser pixels
	for( i = 0; i < tailleData; i += 3 )
	{
		*( data + i ) ^= *( data + i + 2 );
		*( data + i + 2 ) ^= *( data + i );
		*( data + i ) ^= *( data + i + 2 );
	}


	// Creer surface
	if( !( surface = SDL_CreateRGBSurfaceFrom( data,
		NLib_Module_Webcam_NCamera_ObtenirResolution( this->m_camera )->x,
		NLib_Module_Webcam_NCamera_ObtenirResolution( this->m_camera )->y,
		24,
		NLib_Module_Webcam_NCamera_ObtenirResolution( this->m_camera )->x * 3,
		0,
		0,
		0,
		0 ) ) )
		return NFALSE;

	// Creer texture
	if( !( nouvelleSurface = NLib_Module_SDL_Surface_NSurface_Construire3( this->m_fenetre,
		surface ) ) )
	{
		// Detruire surface
		NLIB_NETTOYER_SURFACE( surface );

		// Quitter
		return NFALSE;
	}

	// Lock le mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Sauvegarder ancienne surface
	ancienneSurface = this->m_surface;

	// Enregistrer nouvelle surface
	this->m_surface = nouvelleSurface;

	// Unlock le mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// Detruire ancienne surface
	NLib_Module_SDL_Surface_NSurface_Detruire( &ancienneSurface );

	// OK
	return NTRUE;
}

/**
 * Thread camera
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
__THREAD NBOOL NDashcam_NCameraDashcam_Thread( NCameraDashcam *this )
{
	// Image camera
	NImageCamera *image;

	// Debut de l'acquisition
	NU32 debutAcquisition;

	// Thread camera
	do
	{
		// Debut
		NLib_Temps_NFramerate_DebuterFonction( this->m_framerate );

		// Enregistrer le temps de debut de l'acquisition
		debutAcquisition = NLib_Temps_ObtenirTick( );

		// Lire l'image
		image = NLib_Module_Webcam_NCamera_Capturer( this->m_camera );

		// On skip la frame ou on la conserve?
		if( ++this->m_indexFrame >= NDashcam_Configuration_NConfiguration_ObtenirNombreFrameSkip( this->m_configuration ) )
		{
			// Inscrire frame
			NDashcam_NCameraDashcam_InscrireFrame( this,
				image );

			// Remettre le compteur a zero
			this->m_indexFrame = 0;
		}

		// Generer la surface
		NDashcam_NCameraDashcam_GenererSurface( this,
			image );

		// Detruire l'image
		NLib_Module_Webcam_NImageCamera_Detruire( &image );

		// Enregistrer temps d'acquisition
		this->m_dernierTempsAcquisition = NLib_Temps_ObtenirTick( ) - debutAcquisition;

		// Fin
		NLib_Temps_NFramerate_TerminerFonction( this->m_framerate );
	} while( this->m_estEnCours );

	// OK
	return NTRUE;
}

/**
 * Construire la camera
 *
 * @param configuration
 *		La configuration
 * @param fenetre
 *		La fenetre de l'interface
 * @param peripherique
 *		La description du peripherique
 * @param identifiant
 *		L'identifiant de la camera
 *
 * @return si l'operation s'est bien passee
 */
__ALLOC NCameraDashcam *NDashcam_NCameraDashcam_Construire( const NConfiguration *configuration,
	const NFenetre *fenetre,
	const NPeripheriqueCamera *peripherique,
	NU32 identifiant )
{
	// Sortie
	__OUTPUT NCameraDashcam *out;

	// Lien
	char lien[ 512 ];

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NCameraDashcam ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_peripherique = peripherique;
	out->m_configuration = configuration;
	out->m_identifiant = identifiant;
	out->m_fenetre = fenetre;

	// Creer la camera
	if( !( out->m_camera = NLib_Module_Webcam_NCamera_Construire( NVoiture_Camera_NPeripheriqueCamera_ObtenirLienComplet( peripherique ),
		NDashcam_Configuration_NConfiguration_ObtenirResolutionCapture( configuration )->x,
		NDashcam_Configuration_NConfiguration_ObtenirResolutionCapture( configuration )->y ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire mutex
	if( !( out->m_mutex = NLib_Mutex_NMutex_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire camera
		NLib_Module_Webcam_NCamera_Detruire( &out->m_camera );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire le framerate
	if( !( out->m_framerate = NLib_Temps_NFramerate_Construire( NDashcam_Configuration_NConfiguration_ObtenirFrequenceCapture( configuration ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Detruire camera
		NLib_Module_Webcam_NCamera_Detruire( &out->m_camera );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Calculer la taille du buffer
	out->m_tailleBuffer = LZ4_compressBound( NLib_Module_Webcam_NCamera_ObtenirResolution( out->m_camera )->x
		* NLib_Module_Webcam_NCamera_ObtenirResolution( out->m_camera )->y
		* NLib_Module_Webcam_NCamera_ObtenirNombreOctetParPixel( out->m_camera ) );

	// Allouer le buffer
	if( !( out->m_buffer = calloc( out->m_tailleBuffer,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire framerate
		NLib_Temps_NFramerate_Detruire( &out->m_framerate );

		// Detruire mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Detruire camera
		NLib_Module_Webcam_NCamera_Detruire( &out->m_camera );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire lien
	snprintf( lien,
		512,
		".inter%d",
		identifiant );

	// Construire l'inter lancement
	if( !( out->m_interLancement = NDashcam_NInterLancement_Construire( lien ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire framerate
		NLib_Temps_NFramerate_Detruire( &out->m_framerate );

		// Detruire mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Detruire camera
		NLib_Module_Webcam_NCamera_Detruire( &out->m_camera );

		// Liberer
		NFREE( out->m_buffer );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Lancer la camera
	out->m_estEnCours = NTRUE;

	// Lancer le thread
	if( !( out->m_thread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NDashcam_NCameraDashcam_Thread,
		out,
		&out->m_estEnCours ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire framerate
		NLib_Temps_NFramerate_Detruire( &out->m_framerate );

		// Detruire mutex
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );

		// Detruire camera
		NLib_Module_Webcam_NCamera_Detruire( &out->m_camera );

		// Liberer
		NFREE( out->m_buffer );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer le writer
	NDashcam_NCameraDashcam_CreerWriterMPEG( out );

	// OK
	return out;
}

/**
 * Detruire l'instance
 *
 * @param this
 *		Cette instance
 */
void NDashcam_NCameraDashcam_Detruire( NCameraDashcam **this )
{
	// Detruire inter
	NDashcam_NInterLancement_Detruire( &(*this)->m_interLancement );

	// Detruire thread
	NLib_Thread_NThread_Detruire( &(*this)->m_thread );

	// Detruire framerate
	NLib_Temps_NFramerate_Detruire( &(*this)->m_framerate );

	// Detruire mutex
	NLib_Mutex_NMutex_Detruire( &(*this)->m_mutex );

	// Detruire camera
	NLib_Module_Webcam_NCamera_Detruire( &(*this)->m_camera );

	// Detruire surface
	NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surface );

	// Liberer
	NFREE( (*this)->m_buffer );
	NFREE( *this );
}

/**
 * Activer protection
 *
 * @param this
 *		Cette instance
 */
__WILLLOCK void NDashcam_NCameraDashcam_ActiverProtection( NCameraDashcam *this )
{
	NLib_Mutex_NMutex_Lock( this->m_mutex );
}

/**
 * Desactiver protection
 *
 * @param this
 *		Cette instance
 */
__WILLUNLOCK void NDashcam_NCameraDashcam_DesactiverProtection( NCameraDashcam *this )
{
	NLib_Mutex_NMutex_Unlock( this->m_mutex );
}

/**
 * Obtenir surface
 *
 * @param this
 *		Cette instance
 *
 * @return la surface
 */
__MUSTBEPROTECTED const NSurface *NDashcam_NCameraDashcam_ObtenirSurface( const NCameraDashcam *this )
{
	return this->m_surface;
}

/**
 * Obtenir peripherique
 *
 * @param this
 *		Cette instance
 *
 * @return le detail du peripherique
 */
const NPeripheriqueCamera *NDashcam_NCameraDashcam_ObtenirPeripherique( const NCameraDashcam *this )
{
	return this->m_peripherique;
}

/**
 * Obtenir identifiant
 *
 * @param this
 *		Cette instance
 *
 * @return l'identifiant de la camera
 */
NU32 NDashcam_NCameraDashcam_ObtenirIdentifiant( const NCameraDashcam *this )
{
	return this->m_identifiant;
}

/**
 * Changer etat generation surface
 *
 * @param this
 *		Cette instance
 * @param estActif
 *		La generation doit etre active?
 */
void NDashcam_NCameraDashcam_DefinirGenerationSurface( NCameraDashcam *this,
	NBOOL estActif )
{
	this->m_estDoitGenererSurface = estActif;
}

/**
 * Obtenir dernier temps d'acquisition
 *
 * @param this
 *		Cette instance
 *
 * @return le dernier temps d'acquisition
 */
NU32 NDashcam_NCameraDashcam_ObtenirDernierTempsAcquisition( const NCameraDashcam *this )
{
	return this->m_dernierTempsAcquisition;
}

