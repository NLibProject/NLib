#include "../include/NDashcam.h"

// ------------------
// namespace NDashcam
// ------------------

NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Configuration
	NConfiguration *configuration;

	// Dashcam
	NDashcam *dashcam;

	// Initialiser NLib
	NLib_Initialiser( NULL );

	// Construire la configuration
	if( !( configuration = NDashcam_Configuration_NConfiguration_Construire( NDASHCAM_NOM_FICHIER_CONFIGURATION ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Construire la dashcam
	if( !( dashcam = NDashcam_NDashcam_Construire( configuration ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire configuration
		NDashcam_Configuration_NConfiguration_Detruire( &configuration );

		// Detruire NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Lancer la dashcam
    NDashcam_NDashcam_Lancer( dashcam );

	// Detruire la dashcam
	NDashcam_NDashcam_Detruire( &dashcam );

	// Detruire la configuration
	NDashcam_Configuration_NConfiguration_Detruire( &configuration );

	// Detruire NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

