#define NDASHCAM_NBOUTONDASHCAM_INTERNE
#include "../include/NDashcam.h"

// -----------------------------
// enum NDashcam::NBoutonDashcam
// -----------------------------

/**
 * Obtenir texte bouton
 *
 * @param bouton
 *		Le bouton dont on veut le texte
 *
 * @return le texte du bouton
 */
const char *NDashcam_NBoutonDashcam_ObtenirTexte( NBoutonDashcam bouton )
{
	return NBoutonDashcamTexte[ bouton ];
}

