#include "../../../NLib/include/NLib/NLib.h"

NBOOL reception( const NClientServeur *client, const NPacket *packet )
{
	printf( "Serveur: " );
	NLib_Memoire_Afficher( packet->m_data,
		packet->m_taille );
		printf(" \n" );
	return NTRUE;
}
static NClientServeur *clientServeur;
NBOOL connexion( const NClientServeur *client )
{
	printf( "Nouveau client !\n" );
	clientServeur = client;
	return NTRUE;
}


NBOOL deconnexion( const NClientServeur *client )
{

	return NTRUE;

}

NBOOL reception2( const NPacket *packet, void *client )
{
	printf( "Client: " );
	NLib_Memoire_Afficher( packet->m_data,
		packet->m_taille );
		printf(" \n" );
	return NTRUE;

}

NBOOL deconnexion2( void *client )
{
	return NTRUE;

}

NBOOL receptionUDP( NPacketUDP *packet,
	const NClientUDP *client )
{
	// Packet reçu
	printf( "Je reçois (UDP1): " );
	NLib_Memoire_Afficher( packet->m_packet->m_data,
		packet->m_packet->m_taille );
	printf( "\n" );
	return NTRUE;
}

NBOOL receptionUDP2( NPacketUDP *packet,
	const NClientUDP *client )
{
	// Packet reçu
	printf( "Je reçois (UDP2): " );
	NLib_Memoire_Afficher( packet->m_packet->m_data,
		packet->m_packet->m_taille );
	printf( "\n" );
	return NTRUE;
}

/**
 * Fonction principal, point d'entree
 *
 * @param argc
 *		Le nombre d'arguments
 * @param argv
 *		La liste d'arguments
 *
 * @return EXIT_SUCCESS si tout s'est bien passe
 */
int main( int argc,
	char *argv[ ] )
{
    // Iterateur
	int i;

	// Camera
	NCamera *camera;

	// Image
	NImageCamera *image;

	NClientUDP *udp,
		*udp2;
	NDetailIP *ip;


	NBOOL estContinuer = NTRUE;

	/*NServeur *serveur;
	NClient *client;
	NDetailIP *ip;
	NPacket *packet;

	packet = NLib_Module_Reseau_Packet_NPacket_Construire3( "Bonjour!", 8 );*/

	// Initialiser NLib
	NLib_Initialiser( NULL );
	/*serveur = NLib_Module_Reseau_Serveur_NServeur_Construire( 16500, reception, connexion, deconnexion, serveur, 100 );
	ip = NLib_Module_Reseau_IP_NDetailIP_Construire( NPROTOCOLE_IP_IPV4, "127.0.0.1" );
	client = NLib_Module_Reseau_Client_NClient_Construire( ip,
		16500,
		reception2,
		deconnexion2,
		client,
		100 );
	NLib_Module_Reseau_Client_NClient_Connecter( client );
	NLib_Temps_Attendre( 500 );
	NLib_Module_Reseau_Client_NClient_AjouterPacket( client,
		packet );
	NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( clientServeur,
		NLib_Module_Reseau_Packet_NPacket_Construire3( "Bonjour client!", strlen( "Bonjour client!" ) ) );
	NLib_Temps_Attendre( 2000 );
	printf( "Tuer... Client\n" );
	NLib_Module_Reseau_Client_NClient_Detruire( &client );
	printf( "OK.\nServeur... \n" );
	NLib_Module_Reseau_Serveur_NServeur_Detruire( &serveur );
	printf( "OK.\n" );
	NLib_Module_Reseau_IP_NDetailIP_Detruire( &ip );
*/

    udp = NLib_Module_Reseau_UDP_Client_NClientUDP_Construire( 16500,
		receptionUDP );
	udp2 = NLib_Module_Reseau_UDP_Client_NClientUDP_Construire( 16501,
		receptionUDP2 );

	do
	{
		switch( getchar( ) )
		{
			case 'a':
				ip = NLib_Module_Reseau_IP_NDetailIP_Construire( NPROTOCOLE_IP_IPV4, "127.0.0.1" );
				NLib_Module_Reseau_UDP_Client_NClientUDP_AjouterPacket( udp,
					NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire( "Bonjour je suis UDP1!",
						strlen( "Bonjour je suis UDP1!" ),
						ip,
						16501 ) );
				break;
			case 'b':
				ip = NLib_Module_Reseau_IP_NDetailIP_Construire( NPROTOCOLE_IP_IPV4, "127.0.0.1" );
				NLib_Module_Reseau_UDP_Client_NClientUDP_AjouterPacket( udp2,
					NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire( "Bonjour je suis UDP2!",
						strlen( "Bonjour je suis UDP2!" ),
						ip,
						16500 ) );
				break;

			default:
				break;

			case 'q':
				estContinuer = NFALSE;
				break;
		}
	} while( estContinuer );

	NLib_Module_Reseau_UDP_Client_NClientUDP_Detruire( &udp );
	NLib_Module_Reseau_UDP_Client_NClientUDP_Detruire( &udp2 );

	// Construire la camera
	if( !( camera = NLib_Module_Webcam_NCamera_Construire( "/dev/video0",
		1280,
		960 ) ) )
	{
		// Notifier
		printf( "Impossible de construire la camera...\n" );

		// Quitter
		return EXIT_FAILURE;
	}

	do
	{
		// Capturer une image
		image = NLib_Module_Webcam_NCamera_Capturer( camera );

		NLib_Module_Webcam_NImageCamera_Enregistrer( image,
			"test.pcm" );

		NLib_Module_Webcam_NImageCamera_Detruire( &image );

		usleep( 1000 );
	} while( 1 );

	// Detruire la camera
	NLib_Module_Webcam_NCamera_Detruire( &camera );

    // Detruire NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

