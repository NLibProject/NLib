#include "../../include/TTR/TTR.h"

NBOOL TTR_CallbackReception( const NClientServeur *client,
	const NPacket *packet )
{
	// Donnees
	char *data;

	// Curseur
	NU32 curseur = 0;

	// Buffer
	char *buffer;

	// Referencer
	NREFERENCER( client );

	// Securiser donnees
		// Allouer
			if( !( data = calloc( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) + 1,
				sizeof( char ) ) ) )
			{
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );
				return NFALSE;
			}
		// Copier
			memcpy( data,
				NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
				NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) );

	// Lire l'action a effectuer
	if( !( buffer = NLib_Chaine_LireJusqua( data,
		'\n',
		&curseur,
		NFALSE ) ) )
	{
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		NFREE( data );
		return NFALSE;
	}

	// Determiner l'action a effectuer
	switch( TTR_Action_NListeAction_Determiner( buffer ) )
	{
		case NLISTE_ACTION_EXPORT:
			// Action export
			printf( "BUFFER = %s\n", buffer );
			TTR_Action_Export( data,
				NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ),
				&curseur,
				client );
			break;

		case NLISTE_ACTION_CATALOGUE:
			// Donne catalogue
			TTR_Action_Catalogue( data,
				NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ),
				&curseur,
				client );
			break;

		case NLISTE_ACTION_IMPORT:
			// Action import
			TTR_Action_Import( data,
				NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ),
				&curseur,
				client );
			break;

		case NLISTE_ACTION_NOTE:
			// Action notation
			TTR_Action_Notation( data,
				NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ),
				&curseur,
				client );
			break;

		default:
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
			NFREE( data );
			NFREE( buffer );
			return NFALSE;
	}

	// Liberer
	NFREE( data );
	NFREE( buffer );

	// OK
	return NTRUE;
}

NBOOL TTR_CallbackConnexion( const NClientServeur *client )
{
	// Referencer
	NREFERENCER( client );

	// OK
	return NTRUE;
}

NBOOL TTR_CallbackDeconnexion( const NClientServeur *client )
{
	// Referencer
	NREFERENCER( client );

	// OK
	return NTRUE;
}

NS32 main( void )
{
	// Serveur
	NServeur *serveur;

	// Continuer?
	NBOOL estContinuer = NTRUE;
	char commande[ 256 ];

	// Initialiser NLib
	if( !NLib_Initialiser( TTR_Erreur_CallbackErreur ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Quitter
		return EXIT_FAILURE;
	}

	// Creer serveur
	if( !( serveur = NLib_Module_Reseau_Serveur_NServeur_Construire( 16501,
		TTR_CallbackReception,
		TTR_CallbackConnexion,
		TTR_CallbackDeconnexion,
		NULL,
		0,
		NTYPE_SERVEUR_TCP ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_LISTEN );

		// Detruire
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	printf( "[TapTapReceiver]\n\n" );
	do
	{
		scanf( "%s",
			commande );
		if( NLib_Chaine_Comparer( commande,
			"stop",
			NFALSE,
			0 )
			|| NLib_Chaine_Comparer( commande,
				"exit",
				NFALSE,
				0 ) )
			estContinuer = NFALSE;
	} while( estContinuer );

	// Detruire serveur
	NLib_Module_Reseau_Serveur_NServeur_Detruire( &serveur );

	// Detruire NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

