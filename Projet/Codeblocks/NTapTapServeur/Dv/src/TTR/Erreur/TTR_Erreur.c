#include "../../../include/TTR/TTR.h"

// ---------------------
// namespace TTR::Erreur
// ---------------------

/* Callback erreur */
void TTR_Erreur_CallbackErreur( const NErreur *erreur )
{
	// Afficher message?
	NBOOL estAfficher = NTRUE;

	// On n'affichera pas les avertissements
#ifndef NLIB_ERREUR_NOTIFICATION_AFFICHER_AVERTISSEMENTS
	if( NLib_Erreur_NErreur_ObtenirNiveau( erreur ) <= NNIVEAU_ERREUR_AVERTISSEMENT )
		return;
#endif // NLIB_ERREUR_NOTIFICATION_AFFICHER_AVERTISSEMENTS

	// Doit afficher?
	switch( NLib_Erreur_NErreur_ObtenirCode( erreur ) )
	{
		case NERREUR_SOCKET_RECV:
		case NERREUR_SOCKET_SEND:
			estAfficher = NFALSE;
			break;

		default:
			break;
	}

	// Afficher l'erreur
	if( estAfficher )
	{
		// Erreur
		printf( "[%s]: %s( )::(%s)\n",
			NLib_Erreur_NNiveauErreur_Traduire( NLib_Erreur_NErreur_ObtenirNiveau( erreur ) ),
			NLib_Erreur_NErreur_ObtenirMessage( erreur ),
			NLib_Erreur_NCodeErreur_Traduire( NLib_Erreur_NErreur_ObtenirCode( erreur ) ) );
	
		// Origine
		printf( "\"%s\", ligne %d, code %d.\n\n",
			NLib_Erreur_NErreur_ObtenirFichier( erreur ),
			NLib_Erreur_NErreur_ObtenirLigne( erreur ),
			NLib_Erreur_NErreur_ObtenirCodeUtilisateur( erreur ) );
	}
}

