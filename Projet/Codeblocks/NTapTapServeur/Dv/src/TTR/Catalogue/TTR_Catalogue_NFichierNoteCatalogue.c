#include "../../../include/TTR/TTR.h"

// --------------------------------------------
// struct TTR::Catalogue::NFichierNoteCatalogue
// --------------------------------------------

/* Construire */
__ALLOC NFichierNoteCatalogue *TTR_Catalogue_NFichierNoteCatalogue_Construire( const char *niveau )
{
	// Sortie
	__OUTPUT NFichierNoteCatalogue *out = NULL;
	
	// Lien
	char lien[ 2048 ];

	// Fichier
	NFichierTexte *fichier;

	// Ligne
	char *ligne;
	NU32 curseur;

	// Identifiant
	char *identifiant;
	
	// Note
	char *note;
	
	// Iterateur
	NU32 i;

	// Construire le lien
	snprintf( lien,
		2048,
		"%s/%s",
		REPERTOIRE_NOTE_NIVEAU,
		niveau );

	// Ouvrir le fichier
	if( !( fichier = NLib_Fichier_NFichierTexte_ConstruireLecture( lien ) ) )
		// Quitter
		return NFALSE;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NFichierNoteCatalogue ) ) )
		|| !( out->nomNiveau = calloc( strlen( niveau ) + 1,
			sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Fermer fichier
		NLib_Fichier_NFichierTexte_Detruire( &fichier );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out->nomNiveau,
		niveau,
		strlen( niveau ) );

	// Lire contenu
	while( !NLib_Fichier_NFichierTexte_EstEOF( fichier ) )
	{
		// Lire ligne
		if( !( ligne = NLib_Fichier_NFichierTexte_LireLigne( fichier,
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

			// Liberer
			for( i = 0; i < out->nombreEntree; i++ )
				TTR_Catalogue_NEntreeFichierNoteCatalogue_Detruire( &out->entree[ i ] );
			NFREE( out->nomNiveau );
			NFREE( out->entree );
			NFREE( out );

			// Fermer le fichier
			NLib_Fichier_NFichierTexte_Detruire( &fichier );

			// Quitter
			return NFALSE;
		}

		// Curseur a zero
		curseur = 0;

		// Extraire identifiant
		if( !( identifiant = NLib_Chaine_LireJusqua( ligne,
			';',
			&curseur,
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

			// Liberer
			for( i = 0; i < out->nombreEntree; i++ )
				TTR_Catalogue_NEntreeFichierNoteCatalogue_Detruire( &out->entree[ i ] );
			NFREE( out->entree );
			NFREE( out->nomNiveau );
			NFREE( out );
			NFREE( ligne );

			// Fermer le fichier
			NLib_Fichier_NFichierTexte_Detruire( &fichier );

			// Quitter
			return NFALSE;
		}

		// Extraire note
		if( !( note = NLib_Chaine_LireJusqua( ligne,
			';',
			&curseur,
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );
			
			// Liberer
			for( i = 0; i < out->nombreEntree; i++ )
				TTR_Catalogue_NEntreeFichierNoteCatalogue_Detruire( &out->entree[ i ] );
			NFREE( out->entree );
			NFREE( out->nomNiveau );
			NFREE( out );
			NFREE( identifiant );
			NFREE( ligne );

			// Fermer le fichier
			NLib_Fichier_NFichierTexte_Detruire( &fichier );

			// Quitter
			return NFALSE;
		}

		if( !TTR_Catalogue_NFichierNoteCatalogue_AjouterEntree( out,
			strtof( note,
				NULL ),
			identifiant ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );
			
			// Liberer
			for( i = 0; i < out->nombreEntree; i++ )
				TTR_Catalogue_NEntreeFichierNoteCatalogue_Detruire( &out->entree[ i ] );
			NFREE( out->entree );
			NFREE( out->nomNiveau );
			NFREE( out );
			NFREE( identifiant );
			NFREE( ligne );
			NFREE( note );

			// Fermer le fichier
			NLib_Fichier_NFichierTexte_Detruire( &fichier );

			// Quitter
			return NFALSE;
		}
				
		// Liberer
		NFREE( note );
		NFREE( identifiant );
		NFREE( ligne );
	}

	// Fermer fichier
	NLib_Fichier_NFichierTexte_Detruire( &fichier );

	// OK
	return out;
}

__ALLOC NFichierNoteCatalogue *TTR_Catalogue_NFichierNoteCatalogue_Construire2( const char *niveau )
{
	// Sortie
	__OUTPUT NFichierNoteCatalogue *out = NULL;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NFichierNoteCatalogue ) ) )
		|| !( out->nomNiveau = calloc( strlen( niveau ) + 1,
			sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out->nomNiveau,
		niveau,
		strlen( niveau ) );

	// OK
	return out;
}

/* Detruire */
void TTR_Catalogue_NFichierNoteCatalogue_Detruire( NFichierNoteCatalogue **this )
{
	// Iterateur
	NU32 i = 0;

	// Liberer entrees
	for( ; i < (*this)->nombreEntree; i++ )
		TTR_Catalogue_NEntreeFichierNoteCatalogue_Detruire( &(*this)->entree[ i ] );
	NFREE( (*this)->entree );
	NFREE( (*this)->nomNiveau );

	// Liberer
	NFREE( *this );
}

/* Obtenir index appareil */
NU32 TTR_Catalogue_NFichierNoteCatalogue_ObtenirIndexAppareil( const NFichierNoteCatalogue *this,
	const char *appareil )
{
	// Iterateur
	__OUTPUT NU32 i = 0;

	// Chercher
	for( ; i < this->nombreEntree; i++ )
		// Verifier si il s'agit du meme appareil
		if( !strcmp( TTR_Catalogue_NEntreeFichierNoteCatalogue_ObtenirAppareil( this->entree[ i ] ),
			appareil ) )
			// Appareil present
			return i;

	// Introuvable
	return NERREUR;
}

/* Ajouter une entree */
NBOOL TTR_Catalogue_NFichierNoteCatalogue_AjouterEntree( NFichierNoteCatalogue *this,
	float note,
	const char *identifiant )
{
	// Entree
	NEntreeFichierNoteCatalogue *entree;

	// Buffer
	NEntreeFichierNoteCatalogue **tmp;

	// Index appareil
	NU32 indexAppareil;

	// Verifier
	if( ( indexAppareil = TTR_Catalogue_NFichierNoteCatalogue_ObtenirIndexAppareil( this,
		identifiant ) ) != NERREUR )
	{
		// Modifier l'entree
		TTR_Catalogue_NEntreeFichierNoteCatalogue_DefinirNote( this->entree[ indexAppareil ],
			note );

		// Quitter
		return NTRUE;
	}

	// Construire entree
	if( !( entree = TTR_Catalogue_NEntreeFichierNoteCatalogue_Construire( note,
		identifiant ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Allouer tableau entrees
	if( this->entree != NULL )
	{
		// Allouer la memoire
		if( !( tmp = calloc( this->nombreEntree + 1,
			sizeof( NEntreeFichierNoteCatalogue* ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			TTR_Catalogue_NEntreeFichierNoteCatalogue_Detruire( &entree );

			// Quitter
			return NFALSE;
		}

		// Copier
		memcpy( tmp,
			this->entree,
			sizeof( NEntreeFichierNoteCatalogue* ) * this->nombreEntree );

		// Liberer
		NFREE( this->entree );

		// Enregistrer
		this->entree = tmp;
	}
	else
		if( !( this->entree = calloc( this->nombreEntree + 1,
			sizeof( NEntreeFichierNoteCatalogue* ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			TTR_Catalogue_NEntreeFichierNoteCatalogue_Detruire( &entree );

			// Quitter
			return NFALSE;
		}

	// Ajouter entree
	this->entree[ this->nombreEntree ] = entree;
	this->nombreEntree++;

	// OK
	return NTRUE;
}

/* Sauvegarder */
NBOOL TTR_Catalogue_NFichierNoteCatalogue_Sauvegarder( const NFichierNoteCatalogue *this )
{
	// Fichier
	NFichierTexte *fichier;

	// Lien
	char lien[ 2048 ];

	// Iterateur
	NU32 i;

	// Creer le lien
	snprintf( lien,
		2048,
		"%s/%s",
		REPERTOIRE_NOTE_NIVEAU,
		this->nomNiveau );

	// Creer repertoire
	_mkdir( REPERTOIRE_NOTE_NIVEAU );

	// Ouvrir le fichier
	if( !( fichier = NLib_Fichier_NFichierTexte_ConstruireEcriture( lien,
		NTRUE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return NFALSE;
	}

	// Inscrire entrees
	for( i = 0; i < this->nombreEntree; i++ )
		// Sauvegarder
		if( !TTR_Catalogue_NEntreeFichierNoteCatalogue_Sauvegarder( this->entree[ i ],
			fichier ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_CANT_WRITE );

			// Fermer le fichier
			NLib_Fichier_NFichierTexte_Detruire( &fichier );

			// Quitter
			return NFALSE;
		}

	// Fermer le fichier
	NLib_Fichier_NFichierTexte_Detruire( &fichier );

	// OK
	return NTRUE;
}

/* Est appareil deja present? */
NBOOL TTR_Catalogue_NFichierNoteCatalogue_EstAppareilPresent( const NFichierNoteCatalogue *this,
	const char *appareil )
{
	return TTR_Catalogue_NFichierNoteCatalogue_ObtenirIndexAppareil( this,
		appareil ) != NERREUR;
}

/* Obtenir nombre entrees */
NU32 TTR_Catalogue_NFichierNoteCatalogue_ObtenirNombreEntree( const NFichierNoteCatalogue *this )
{
	return this->nombreEntree;
}

/* Obtenir entree */
const NEntreeFichierNoteCatalogue *TTR_Catalogue_NFichierNoteCatalogue_ObtenirEntree( const NFichierNoteCatalogue *this,
	NU32 entree )
{
	return this->entree[ entree ];
}

/* Obtenir moyenne notes */
float TTR_Catalogue_NFichierNoteCatalogue_ObtenirMoyenneNote( const NFichierNoteCatalogue *this )
{
	// Sortie
	__OUTPUT float totalNote = 0;

	// Iterateur
	NU32 i = 0;

	// Verifier
	if( !this->nombreEntree )
		return 0.0f;

	// Calculer
	for( ; i < this->nombreEntree; i++ )
		totalNote += TTR_Catalogue_NEntreeFichierNoteCatalogue_ObtenirNote( this->entree[ i ] );

	// Resultat
	return totalNote / (float)this->nombreEntree;
}

/* Obtenir note appareil */
float TTR_Catalogue_NFichierNoteCatalogue_ObtenirNoteAppareil( const NFichierNoteCatalogue *this,
	const char *appareil )
{
	// Iterateur
	NU32 i = 0;

	// Chercher
	for( ; i < this->nombreEntree; i++ )
		if( !strcmp( TTR_Catalogue_NEntreeFichierNoteCatalogue_ObtenirAppareil( this->entree[ i ] ),
			appareil ) )
			return TTR_Catalogue_NEntreeFichierNoteCatalogue_ObtenirNote( this->entree[ i ] );

	// Introuvable
	return 0.0f;
}

/* Obtenir nom niveau */
const char *TTR_Catalogue_NFichierNoteCatalogue_ObtenirNomNiveau( const NFichierNoteCatalogue *this )
{
	return this->nomNiveau;
}

