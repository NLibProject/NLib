#include "../../../include/TTR/TTR.h"

// ---------------------------------------
// struct TTR::Catalogue::NEntreeCatalogue
// ---------------------------------------

/* Recuperer notes dans fichier niveau.not */
NBOOL TTR_Catalogue_NEntreeCatalogue_RecupererNoteInterne( NEntreeCatalogue *this,
	const char *identifiantAppareil )
{
	// Fichier note
	NFichierNoteCatalogue *fichier;

	// Construire
	if( !( fichier = TTR_Catalogue_NFichierNoteCatalogue_Construire( this->nomNiveau ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_FILE_NOT_FOUND );

		// Quitter
		return NFALSE;
	}

	// Enregistrer
	this->moyenneNote = (NU32)( TTR_Catalogue_NFichierNoteCatalogue_ObtenirMoyenneNote( fichier ) * 100.0f );
	this->noteUtilisateur = (NU32)( TTR_Catalogue_NFichierNoteCatalogue_ObtenirNoteAppareil( fichier,
		identifiantAppareil ) * 100.0f );
	this->nombreVote = TTR_Catalogue_NFichierNoteCatalogue_ObtenirNombreEntree( fichier );

	// Detruire
	TTR_Catalogue_NFichierNoteCatalogue_Detruire( &fichier );

	// OK
	return NTRUE;
}

/* Construire */
__ALLOC NEntreeCatalogue *TTR_Catalogue_NEntreeCatalogue_Construire( const char *titre,
	const char *nomNiveau,
	const char *pseudo,
	const char *identifiantAppareil )
{
	// Sortie
	__OUTPUT NEntreeCatalogue *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NEntreeCatalogue ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer
	if( !( out->titre = calloc( strlen( titre ) + 1,
			sizeof( char ) ) )
		|| !( out->nomNiveau = calloc( strlen( nomNiveau ) + 1,
			sizeof( char ) ) )
		|| !( out->pseudo = calloc( strlen( pseudo ) + 1,
			sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out->nomNiveau );
		NFREE( out->titre );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out->nomNiveau,
		nomNiveau,
		strlen( nomNiveau ) );
	memcpy( out->titre,
		titre,
		strlen( titre ) );
	memcpy( out->pseudo,
		pseudo,
		strlen( pseudo ) );

	// Recuperer note
	if( !TTR_Catalogue_NEntreeCatalogue_RecupererNoteInterne( out,
		identifiantAppareil ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_FILE_NOT_FOUND );

		// Mettre a zero
		out->moyenneNote = 0;
		out->noteUtilisateur = 0;
	}

	// OK
	return out;
}

/* Detruire */
void TTR_Catalogue_NEntreeCatalogue_Detruire( NEntreeCatalogue **this )
{
	NFREE( (*this)->titre );
	NFREE( (*this)->nomNiveau );
	NFREE( (*this)->pseudo );
	NFREE( (*this) );
}

/* Obtenir titre */
const char *TTR_Catalogue_NEntreeCatalogue_ObtenirTitre( const NEntreeCatalogue *this )
{
	return this->titre;
}

/* Obtenir nom niveau */
const char *TTR_Catalogue_NEntreeCatalogue_ObtenirNomNiveau( const NEntreeCatalogue *this )
{
	return this->nomNiveau;
}

/* Obtenir pseudo */
const char *TTR_Catalogue_NEntreeCatalogue_ObtenirPseudo( const NEntreeCatalogue *this )
{
	return this->pseudo;
}

/* Obtenir moyenne notes */
NU32 TTR_Catalogue_NEntreeCatalogue_ObtenirMoyenneNote( const NEntreeCatalogue *this )
{
	return this->moyenneNote;
}

/* Obtenir note utilisateur */
NU32 TTR_Catalogue_NEntreeCatalogue_ObtenirNoteUtilisateur( const NEntreeCatalogue *this )
{
	return this->noteUtilisateur;
}

/* Obtenir nombre votes */
NU32 TTR_Catalogue_NEntreeCatalogue_ObtenirNombreVote( const NEntreeCatalogue *this )
{
	return this->nombreVote;
}

