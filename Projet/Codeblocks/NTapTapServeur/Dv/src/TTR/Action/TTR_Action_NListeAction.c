#include "../../../include/TTR/TTR.h"

// ------------------------------
// enum TTR::Action::NListeAction
// ------------------------------

/* Determiner action */
NListeAction TTR_Action_NListeAction_Determiner( const char *action )
{
	// Sortie
	__OUTPUT NListeAction sortie = (NListeAction)0;

	// Chercher
	for( ; sortie < NLISTE_ACTIONS; sortie++ )
		if( NLib_Chaine_Comparer( action,
			NListeActionTexte[ sortie ],
			NFALSE,
			0 ) )
			return sortie;

	// Introuvable
	return NERREUR;
}

