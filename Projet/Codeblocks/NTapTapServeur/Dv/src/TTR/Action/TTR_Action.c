#include "../../../include/TTR/TTR.h"

// ---------------------
// namespace TTR::Action
// ---------------------

/* Action export */
enum ClefActionExport
{
	CLEF_ACTION_EXPORT_APPAREIL,
	CLEF_ACTION_EXPORT_NIVEAU,
	CLEF_ACTION_EXPORT_PSEUDO,
	CLEF_ACTION_EXPORT_TAILLE_NIVEAU,
	CLEF_ACTION_EXPORT_TAILLE_MUSIQUE,
	CLEF_ACTION_EXPORT_CONTENU_NIVEAU,
	CLEF_ACTION_EXPORT_CONTENU_MUSIQUE,

	CLEFS_ACTION_EXPORT
};

static const char ClefActionExportTexte[ CLEFS_ACTION_EXPORT ][ 32 ] =
{
	"Appareil: ",
	"Niveau: ",
	"Pseudo: ",
	"Taille niveau: ",
	"Taille musique: ",
	"Contenu niveau: ",
	"Contenu musique: "
};

/* mkdir recursif */
static void __mkdir( const char *dir )
{
	char tmp[ MAX_PATH ];
	char *p = NULL;
	size_t len;

	snprintf( tmp,
		sizeof( tmp ),
		"%s",
		dir );

	len = strlen( tmp );
	if( tmp[ len - 1 ] == '/' )
		tmp[ len - 1 ] = 0;
	for( p = tmp + 1; *p; p++ )
		if( *p == '/' )
		{
			*p = 0;
			_mkdir( tmp );
			*p = '/';
		}

	_mkdir( tmp );
}

/* Enregistrer dans la base de donnees */
NBOOL TTR_Action_Export_EnregistrerDB( const char *niveau,
	const char *pseudo,
	const char *appareil,
	const char *ip )
{
	// Fichier
	FILE *fichier;

	// Lien
	char lien[ 2048 ] = REPERTOIRE_DATABASE"/"FICHIER_DATABASE;

	// Tenter d'ouvrir le fichier
	do
	{
		// Ouvrir
		fichier = fopen( lien,
			"a" );

		// Attendre si l'ouverture echoue
		if( !fichier )
			NLib_Temps_Attendre( 1000 );
	} while( fichier == NULL );

	// Inscrire
	fprintf( fichier,
		"|%s|%s|%s|%s|\n",
		niveau,
		pseudo,
		appareil,
		ip );

	// Fermer le fichier
	fclose( fichier );

	// OK
	return NTRUE;
}

/* Sauvegarder */
NBOOL TTR_Action_EstChaineViableExport( const char *chaine )
{
	// Iterateur
	NU32 i = 0;

	// Taille chaine
	NU32 tailleChaine;

	// Calculer taille
	tailleChaine = strlen( chaine );

	// Verifier
	for( ; i < tailleChaine; i++ )
		if( ( chaine[ i ] >= '0'
				&& chaine[ i ] <= '9' )
			|| ( chaine[ i ] >= 'A'
					&& chaine[ i ] <= 'Z' )
			|| ( chaine[ i ] >= 'a'
					&& chaine[ i ] <= 'z' ) )
			continue;
		else if( chaine[ i ] >= 1
				&& chaine[ i ] <= 31 )
			return NFALSE;
		else
			switch( chaine[ i ] )
			{
				case '<':
				case '>':
				case ':':
				case '"':
				case '\\':
				case '|':
				case '?':
				case '*':
					return NFALSE;

				default:
					continue;
			}

	// OK
	return NTRUE;
}

NBOOL TTR_Action_Export_Sauvegarder( const char *appareil,
	const char *niveau,
	const char *pseudo,
	const char *contenuNiveau,
	const char *musique,
	NU32 tailleNiveau,
	NU32 tailleMusique,
	const NClientServeur *client )
{
	// Curseur
	NU32 curseur = 0;

	// Lien musique
	char *lienMusique;
	char *lienRepertoireMusique;
	char *lienFinalMusique;

	// Lien niveau
	char lienNiveau[ 2048 ];

	// Fichier
	FILE *fichier;

	// Extraire informations du niveau
	if( !NLib_Chaine_PlacerALaChaine( contenuNiveau,
			"Lien Musique:",
			&curseur )
		|| !( lienMusique = NLib_Chaine_LireEntre2( contenuNiveau,
			'"',
			&curseur,
			NFALSE ) ) )
	{
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		return NFALSE;
	}

	// Verifier
	if( !TTR_Action_EstChaineViableExport( lienMusique ) )
	{
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		NFREE( lienMusique );
		return NFALSE;
	}

	// Couper derniere partie
		// Chercher '/'
			curseur = strlen( lienMusique );
			while( curseur > 0
				&& lienMusique[ curseur ] != '/' )
				curseur--;
		// Couper
			if( !( lienRepertoireMusique = calloc( strlen( REPERTOIRE_MUSIQUE ) + 1 + strlen( lienMusique ) + 1,
				sizeof( char ) ) ) )
			{
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );
				NFREE( lienMusique );
				return NFALSE;
			}
			sprintf( lienRepertoireMusique,
				"%s/%s",
				REPERTOIRE_MUSIQUE,
				lienMusique );
			lienRepertoireMusique[ strlen( REPERTOIRE_MUSIQUE ) + 1 + curseur ] = '\0';

			// Verifier
			if( !TTR_Action_EstChaineViableExport( lienRepertoireMusique ) )
			{
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
				NFREE( lienMusique );
				NFREE( lienRepertoireMusique );
				return NFALSE;
			}

			__mkdir( lienRepertoireMusique );
			NFREE( lienRepertoireMusique );
		// Notifier
			printf( "L'appareil \"%s\" envoie \"%s\" (niveau \"%s\") depuis \"%s\" sous le pseudo [%s].\n",
				appareil,
				lienMusique + curseur,
				niveau,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ),
				pseudo );


	// Copier
	if( !( lienFinalMusique = calloc( strlen( REPERTOIRE_MUSIQUE ) + strlen( lienMusique ) + 1,
		sizeof( char ) ) ) )
	{
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );
		NFREE( lienMusique );
		return NFALSE;
	}

	// Creer lien
	sprintf( lienFinalMusique,
		"%s/%s",
		REPERTOIRE_MUSIQUE,
		lienMusique );
	NFREE( lienMusique );

	// Ecrire musique
	if( !( fichier = fopen( lienFinalMusique,
		"wb+" ) ) )
	{
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );
		NFREE( lienFinalMusique );
		return NFALSE;
	}
	NFREE( lienFinalMusique );
	fwrite( musique,
		sizeof( char ),
		tailleMusique,
		fichier );
	fclose( fichier );

	// Creer lien niveau
	snprintf( lienNiveau,
		2048,
		"%s/%s",
		REPERTOIRE_NIVEAU,
		niveau );

	// Verifier si le niveau n'existe pas deja
	if( NLib_Fichier_Operation_EstExiste( lienNiveau ) )
		return NFALSE;

	// Ouvrir
	if( !( fichier = fopen( lienNiveau,
			"wb+" ) ) )
	{
		NOTIFIER_ERREUR( NERREUR_FILE_ALREADY_EXISTS );
		return NFALSE;
	}

	// Inscrire contenu
	fwrite( contenuNiveau,
		sizeof( char ),
		tailleNiveau,
		fichier);

	// Fermer
	fclose( fichier );

	// Ajouter dans la base de donnees
	return TTR_Action_Export_EnregistrerDB( niveau,
		pseudo,
		appareil,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ) );
}

/* Verifier packet */
NBOOL TTR_Action_Export_VerifierPacket( const char *data,
	NU32 taille,
	NU32 *curseur )
{
	// Iterateur
	NU32 i = 0;

	// Curseur
	NU32 copieCurseur = *curseur;

	// Referencer
	NREFERENCER( taille );

	// Verifier
	for( ; i < CLEFS_ACTION_EXPORT; i++ )
		if( !NLib_Chaine_PlacerALaChaine( data,
			ClefActionExportTexte[ i ],
			&copieCurseur ) )
			return NFALSE;

	// OK
	return NTRUE;
}

NBOOL TTR_Action_Export( const char *data,
	NU32 taille,
	NU32 *curseur,
	const NClientServeur *client )
{
	// Appareil
	char *appareil;

	// Niveau
	char *niveau;

	// Contenu niveau
	char *contenuNiveau;

	// Musique
	char *musique;

	// Pseudo
	char *pseudo;

	// Taille niveau
	char *tailleNiveauTexte;
	NU32 tailleNiveau;

	// Taille musique
	char *tailleMusiqueTexte;
	NU32 tailleMusique;

	// Verifier packet
	if( !TTR_Action_Export_VerifierPacket( data,
		taille,
		curseur ) )
	{
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		return NFALSE;
	}

	// Lire
		// Appareil
			if( !NLib_Chaine_PlacerALaChaine( data,
				ClefActionExportTexte[ CLEF_ACTION_EXPORT_APPAREIL ],
				curseur )
				|| !( appareil = NLib_Chaine_LireJusqua( data,
				'\n',
				curseur,
				NFALSE ) ) )
			{
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
				return NFALSE;
			}
		// Niveau
			if( !NLib_Chaine_PlacerALaChaine( data,
				ClefActionExportTexte[ CLEF_ACTION_EXPORT_NIVEAU ],
				curseur )
				|| !( niveau = NLib_Chaine_LireJusqua( data,
					'\n',
					curseur,
					NFALSE ) ) )
			{
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
				NFREE( appareil );
				return NFALSE;
			}
		// Pseudo
			if( !NLib_Chaine_PlacerALaChaine( data,
				ClefActionExportTexte[ CLEF_ACTION_EXPORT_PSEUDO ],
				curseur )
				|| !( pseudo = NLib_Chaine_LireJusqua( data,
					'\n',
					curseur,
					NFALSE ) ) )
			{
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
				NFREE( niveau );
				NFREE( appareil );
				return NFALSE;
			}
		// Verifier
			if( strlen( pseudo ) == 0
				|| !TTR_Action_EstChaineViableExport( appareil )
				|| !TTR_Action_EstChaineViableExport( niveau )
				|| !TTR_Action_EstChaineViableExport( pseudo ) )
			{
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
				NFREE( pseudo );
				NFREE( niveau );
				NFREE( appareil );
				return NFALSE;
			}

		// Taille niveau
			if( !NLib_Chaine_PlacerALaChaine( data,
				ClefActionExportTexte[ CLEF_ACTION_EXPORT_TAILLE_NIVEAU ],
				curseur )
				|| !( tailleNiveauTexte = NLib_Chaine_LireJusqua( data,
					'\n',
					curseur,
					NFALSE ) ) )
			{
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
				NFREE( pseudo );
				NFREE( appareil );
				NFREE( niveau );
				return NFALSE;
			}
			tailleNiveau = strtol( tailleNiveauTexte,
				NULL,
				10 );
			NFREE( tailleNiveauTexte );
		// Taille musique
			if( !NLib_Chaine_PlacerALaChaine( data,
				ClefActionExportTexte[ CLEF_ACTION_EXPORT_TAILLE_MUSIQUE ],
				curseur )
				|| !( tailleMusiqueTexte = NLib_Chaine_LireJusqua( data,
					'\n',
					curseur,
					NFALSE ) ) )
			{
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
				NFREE( pseudo );
				NFREE( appareil );
				NFREE( niveau );
				return NFALSE;
			}
			tailleMusique = strtol( tailleMusiqueTexte,
				NULL,
				10 );
			NFREE( tailleMusiqueTexte );
		// Contenu niveau
			if( !NLib_Chaine_PlacerALaChaine( data,
				ClefActionExportTexte[ CLEF_ACTION_EXPORT_CONTENU_NIVEAU ],
				curseur ) )
			{
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
				NFREE( pseudo );
				NFREE( appareil );
				NFREE( niveau );
				return NFALSE;
			}
			if( *curseur + tailleNiveau >= taille )
			{
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
				NFREE( pseudo );
				NFREE( appareil );
				NFREE( niveau );
				return NFALSE;
			}
			if( !( contenuNiveau = calloc( tailleNiveau + 1,
				sizeof( char ) ) ) )
			{
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );
				NFREE( pseudo );
				NFREE( appareil );
				NFREE( niveau );
				return NFALSE;
			}

			memcpy( contenuNiveau,
				data + *curseur,
				tailleNiveau );
		// Contenu musique
			if( !NLib_Chaine_PlacerALaChaine( data,
				ClefActionExportTexte[ CLEF_ACTION_EXPORT_CONTENU_MUSIQUE ],
				curseur ) )
			{
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
				NFREE( pseudo );
				NFREE( contenuNiveau );
				NFREE( appareil );
				NFREE( niveau );
				return NFALSE;
			}
			if( *curseur + tailleMusique > taille )
			{
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
				NFREE( pseudo );
				NFREE( contenuNiveau );
				NFREE( appareil );
				NFREE( niveau );
				return NFALSE;
			}
			if( !( musique = calloc( tailleMusique + 1,
				sizeof( char ) ) ) )
			{
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );
				NFREE( pseudo );
				NFREE( contenuNiveau );
				NFREE( appareil );
				NFREE( niveau );
				return NFALSE;
			}
			memcpy( musique,
				data + *curseur,
				tailleMusique );

	// Sauvegarder niveau
	TTR_Action_Export_Sauvegarder( appareil,
		niveau,
		pseudo,
		contenuNiveau,
		musique,
		tailleNiveau,
		tailleMusique,
		client );

	// Liberer
	NFREE( musique );
	NFREE( contenuNiveau );
	NFREE( pseudo );
	NFREE( appareil );
	NFREE( niveau );

	// OK
	return NTRUE;
}

/* Action catalogue */
enum ClefActionCatalogue
{
	CLEF_ACTION_CATALOGUE_IDENTIFIANT_APPAREIL,

	CLEFS_ACTION_CATALOGUE
};

static const char ClefActionCatalogue[ CLEFS_ACTION_CATALOGUE ][ 32 ] =
{
	"Appareil: "
};

NBOOL TTR_Action_Catalogue( const char *data,
	NU32 taille,
	NU32 *curseur,
	const NClientServeur *client )
{
	// Catalogue
	NCatalogue *catalogue;

	// Packet
	NPacket *packet;

	// Entree catalogue
	const NEntreeCatalogue *entree;

	// Identifiant appareil
	char *identifiantAppareil;

	// Buffer
	char buffer[ 2048 ];

	// Iterateur
	NU32 i = 0;

	// Referencer
	NREFERENCER( taille );

	// Extraire l'identifiant de l'appareil
	if( !NLib_Chaine_PlacerALaChaine( data,
		ClefActionCatalogue[ CLEF_ACTION_CATALOGUE_IDENTIFIANT_APPAREIL ],
		curseur )
		|| !( identifiantAppareil = NLib_Chaine_LireJusqua( data,
			'\n',
			curseur,
			NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Construire le catalogue
	if( !( catalogue = TTR_Catalogue_NCatalogue_Construire( identifiantAppareil ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( identifiantAppareil );

		// Quitter
		return NFALSE;
	}

	// Creer packet
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		TTR_Catalogue_NCatalogue_Detruire( &catalogue );
		NFREE( identifiantAppareil );

		// Quitter
		return NFALSE;
	}

	// Liberer
	NFREE( identifiantAppareil );

	// Ajouter donnees
	for( ; i < TTR_Catalogue_NCatalogue_ObtenirNombreEntree( catalogue ); i++ )
	{
		// Recuperer entree
		entree = TTR_Catalogue_NCatalogue_ObtenirEntree( catalogue,
			i );

		// Ajouter nom niveau
		NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
			TTR_Catalogue_NEntreeCatalogue_ObtenirNomNiveau( entree ),
			strlen( TTR_Catalogue_NEntreeCatalogue_ObtenirNomNiveau( entree ) ) );

		// Separateur
		NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
			"||",
			strlen( "||" ) );

		// Ajouter nom musique
		NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
			TTR_Catalogue_NEntreeCatalogue_ObtenirTitre( entree ),
			strlen( TTR_Catalogue_NEntreeCatalogue_ObtenirTitre( entree ) ) );

		// Separateur
		NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
			"||",
			strlen( "||" ) );

		// Ajouter pseudo
		NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
			TTR_Catalogue_NEntreeCatalogue_ObtenirPseudo( entree ),
			strlen( TTR_Catalogue_NEntreeCatalogue_ObtenirPseudo( entree ) ) );

		// Separateur
		NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
			"||",
			strlen( "||" ) );

		// Ajouter moyenne notes
		snprintf( buffer,
			2048,
			"%d",
			TTR_Catalogue_NEntreeCatalogue_ObtenirMoyenneNote( entree ) );
		NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
			buffer,
			strlen( buffer ) );

		// Separateur
		NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
			"||",
			strlen( "||" ) );

		// Ajouter note utilisateur
		snprintf( buffer,
			2048,
			"%d",
			TTR_Catalogue_NEntreeCatalogue_ObtenirNoteUtilisateur( entree ) );
		NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
			buffer,
			strlen( buffer ) );

		// Separateur
		NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
			"||",
			strlen( "||" ) );

		// Ajouter nombre votes
		snprintf( buffer,
			2048,
			"%d",
			TTR_Catalogue_NEntreeCatalogue_ObtenirNombreVote( entree ) );
		NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
			buffer,
			strlen( buffer ) );

		// Retour a la ligne
		NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
			"\n",
			strlen( "\n" ) );
	}

	// Detruire le catalogue
	TTR_Catalogue_NCatalogue_Detruire( &catalogue );

	// Notifier
	printf( "J'envoie le catalogue a \"%s\".\n",
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ) );

	// Envoyer le packet
	return NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( (NClientServeur*)client,
		packet );
}

/* Action import */
enum ClefActionImport
{
	CLEF_ACTION_IMPORT_NOM_NIVEAU,

	CLEFS_ACTION_IMPORT
};

static const char ClefActionImportTexte[ CLEFS_ACTION_IMPORT ][ 32 ] =
{
	"Niveau: "
};

NBOOL TTR_Action_Import( const char *data,
	NU32 taille,
	NU32 *curseur,
	const NClientServeur *client )
{
	// Buffer
	char buffer[ 2048 ];

	// Nom niveau
	char *nomNiveau;

	// Packet
	NPacket *packet;

	// Fichier
	NFichierTexte *fichierTexte;
	NFichierBinaire *fichierNiveau,
		*fichierMusique;

	// Contenu fichiers
	char *contenuFichier;

	// Lien vers niveau
	char lienNiveau[ 2048 ];

	// Lien vers la musique
	char *lienBrutMusique;
	char lienMusique[ 2048 ];

	// Referencer
	NREFERENCER( taille );

	// Extraire nom niveau
	if( !NLib_Chaine_PlacerALaChaine( data,
			ClefActionImportTexte[ CLEF_ACTION_IMPORT_NOM_NIVEAU ],
			curseur )
		|| !( nomNiveau = NLib_Chaine_LireJusqua( data,
			'\n',
			curseur,
			NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Notifier
	printf( "Le client [%s] demande le niveau \"%s\".\n",
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ),
		nomNiveau );

	// Chercher le nom de la musique
		// Creer lien vers niveau
			snprintf( lienNiveau,
				2048,
				"%s/%s",
				REPERTOIRE_NIVEAU,
				nomNiveau );
		// Ouvrir fichier
			if( !( fichierTexte = NLib_Fichier_NFichierTexte_ConstruireLecture( lienNiveau ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_NOT_FOUND );

				// Liberer
				NFREE( nomNiveau );

				// Quitter
				return NFALSE;
			}
		// Lire lien musique
			if( !NLib_Fichier_NFichierTexte_PositionnerProchaineChaine( fichierTexte,
				"Lien Musique:" )
				|| !( lienBrutMusique = NLib_Fichier_NFichierTexte_Lire2( fichierTexte,
					'"',
					NFALSE ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

				// Liberer
				NFREE( nomNiveau );
				NLib_Fichier_NFichierTexte_Detruire( &fichierTexte );

				// Quitter
				return NFALSE;
			}
		// Fermer fichier
			NLib_Fichier_NFichierTexte_Detruire( &fichierTexte );
		// Construire lien vers musique
			snprintf( lienMusique,
				2048,
				"%s/%s",
				REPERTOIRE_MUSIQUE,
				lienBrutMusique );
		// Liberer
			NFREE( lienBrutMusique );

	// Verifier si la musique existe
	if( !NLib_Fichier_Operation_EstExiste( lienMusique ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_NOT_FOUND );

		// Liberer
		NFREE( nomNiveau );

		// Quitter
		return NFALSE;
	}

	// Ouvrir niveau
	if( !( fichierNiveau = NLib_Fichier_NFichierBinaire_ConstruireLecture( lienNiveau ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Liberer
		NFREE( nomNiveau );

		// Quitter
		return NFALSE;
	}

	// Ouvrir musique
	if( !( fichierMusique = NLib_Fichier_NFichierBinaire_ConstruireLecture( lienMusique ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Liberer
		NLib_Fichier_NFichierBinaire_Detruire( &fichierNiveau );
		NFREE( nomNiveau );

		// Quitter
		return NFALSE;
	}

	// Creer packet
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Fermer fichiers
		NLib_Fichier_NFichierBinaire_Detruire( &fichierNiveau );
		NLib_Fichier_NFichierBinaire_Detruire( &fichierMusique );

		// Liberer
		NFREE( nomNiveau );

		// Quitter
		return NFALSE;
	}

	// Remplir packet
		// Nom niveau
			NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				"Niveau: ",
				strlen( "Niveau: " ) );
			NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				nomNiveau,
				strlen( nomNiveau ) );
			NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				"\n",
				strlen( "\n" ) );
			NFREE( nomNiveau );
		// Taille niveau
			sprintf( buffer,
				"%d",
				NLib_Fichier_NFichierBinaire_ObtenirTaille( fichierNiveau ) );
			NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				"Taille niveau: ",
				strlen( "Taille niveau: " ) );
			NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				buffer,
				strlen( buffer ) );
			NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				"\n",
				strlen( "\n" ) );
		// Taille musique
			sprintf( buffer,
				"%d",
				NLib_Fichier_NFichierBinaire_ObtenirTaille( fichierMusique ) );
			NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				"Taille musique: ",
				strlen( "Taille musique: " ) );
			NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				buffer,
				strlen( buffer ) );
			NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				"\n",
				strlen( "\n" ) );
		// Contenu
			NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				"Contenu: ",
				strlen( "Contenu: " ) );

			// Niveau
				// Recuperer contenu
					if( !( contenuFichier = NLib_Fichier_NFichierBinaire_Lire( fichierNiveau,
						NLib_Fichier_NFichierBinaire_ObtenirTaille( fichierNiveau ) ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

						// Fermer fichiers
						NLib_Fichier_NFichierBinaire_Detruire( &fichierNiveau );
						NLib_Fichier_NFichierBinaire_Detruire( &fichierMusique );

						// Quitter
						return NFALSE;
					}
				// Ajouter
					NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
						contenuFichier,
						NLib_Fichier_NFichierBinaire_ObtenirTaille( fichierNiveau ) );
				// Liberer
					NFREE( contenuFichier );
			// Musique
				// Recuperer contenu
					if( !( contenuFichier = NLib_Fichier_NFichierBinaire_Lire( fichierMusique,
						NLib_Fichier_NFichierBinaire_ObtenirTaille( fichierMusique ) ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

						// Fermer fichiers
						NLib_Fichier_NFichierBinaire_Detruire( &fichierNiveau );
						NLib_Fichier_NFichierBinaire_Detruire( &fichierMusique );

						// Quitter
						return NFALSE;
					}
				// Ajouter
					NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
						contenuFichier,
						NLib_Fichier_NFichierBinaire_ObtenirTaille( fichierMusique ) );
				// Liberer
					NFREE( contenuFichier );

	// Fermer fichiers
	NLib_Fichier_NFichierBinaire_Detruire( &fichierNiveau );
	NLib_Fichier_NFichierBinaire_Detruire( &fichierMusique );

	// Ajouter packet
	if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( (NClientServeur*)client,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Attendre la fin de l'envoi
	do
	{
		// 1 seconde de pause
		NLib_Temps_Attendre( 1000 );
	} while( NLib_Module_Reseau_Serveur_NClientServeur_EstPacketsDansCache( client ) );

	// OK
	return NTRUE;
}

/* Action notation niveau */
NBOOL TTR_Action_Notation( const char *data,
	NU32 taille,
	NU32 *curseur,
	const NClientServeur *client )
{
	// Identifiant
	char *identifiant = NULL;

	// Nom niveau
	char *nomNiveau = NULL;

	// Note
	char *note = NULL;

	// Fichier
	NFichierNoteCatalogue *fichier;

	// Referencer
	NREFERENCER( taille );
	NREFERENCER( client );

	// Extraire informations
	if( !( identifiant = NLib_Chaine_LireJusqua( data,
		';',
		curseur,
		NFALSE ) )
		|| !( nomNiveau = NLib_Chaine_LireJusqua( data,
			';',
			curseur,
			NFALSE ) )
		|| !( note = NLib_Chaine_LireJusqua( data,
			';',
			curseur,
			NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Liberer
		NFREE( identifiant );
		NFREE( nomNiveau );

		// Quitter
		return NFALSE;
	}

	// Creer repertoire note
	_mkdir( REPERTOIRE_NOTE_NIVEAU );

	// Ouvrir le fichier
	if( ( fichier = TTR_Catalogue_NFichierNoteCatalogue_Construire( nomNiveau ) ) != NULL )
	{
		// Ajouter l'entree
		if( !TTR_Catalogue_NFichierNoteCatalogue_AjouterEntree( fichier,
			strtof( note,
				NULL ) / 100.0f,
			identifiant ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			NFREE( identifiant );
			NFREE( nomNiveau );
			TTR_Catalogue_NFichierNoteCatalogue_Detruire( &fichier );

			// Quitter
			return NFALSE;
		}

		// Sauvegarder
		TTR_Catalogue_NFichierNoteCatalogue_Sauvegarder( fichier );

		// Fermer le fichier
		TTR_Catalogue_NFichierNoteCatalogue_Detruire( &fichier );
	}
	else
	{
		// Creer le fichier
		if( !( fichier = TTR_Catalogue_NFichierNoteCatalogue_Construire2( nomNiveau ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			NFREE( identifiant );
			NFREE( nomNiveau );

			// Quitter
			return NFALSE;
		}

		// Ajouter entree
		TTR_Catalogue_NFichierNoteCatalogue_AjouterEntree( fichier,
			strtof( note,
				NULL ) / 100.0f,
			identifiant );

		// Sauvegarder
		TTR_Catalogue_NFichierNoteCatalogue_Sauvegarder( fichier );

		// Fermer le fichier
		TTR_Catalogue_NFichierNoteCatalogue_Detruire( &fichier );
	}

	// Liberer
	NFREE( identifiant );
	NFREE( nomNiveau );

	// OK
	return NTRUE;
}

