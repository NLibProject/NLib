#ifndef TTR_ACTION_NLISTEACTION
#define TTR_ACTION_NLISTEACTION

// ------------------------------
// enum TTR::Action::NListeAction
// ------------------------------

typedef enum NListeAction
{
	NLISTE_ACTION_EXPORT,
	NLISTE_ACTION_CATALOGUE,
	NLISTE_ACTION_IMPORT,
	NLISTE_ACTION_NOTE,

	NLISTE_ACTIONS
} NListeAction;

// Actions possibles forme textuelle
static const char NListeActionTexte[ NLISTE_ACTIONS ][ 32 ] =
{
	"Bonjour, export",
	"Bonjour, catalogue",
	"Bonjour, import",
	"Bonjour, note"
};

/* Determiner l'action */
NListeAction TTR_Action_NListeAction_Determiner( const char* );

#endif // !TTR_ACTION_NLISTEACTION

