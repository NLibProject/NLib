#ifndef TTR_PROTECT
#define TTR_PROTECT

// -------------
// namespace TTR
// -------------

// namespace NLib
#include "../../../../../../NLib/include/NLib/NLib.h"

// Max path
#ifndef IS_WINDOWS
#include <linux/limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#define MAX_PATH		PATH_MAX
#define _mkdir( d ) \
	mkdir( d, \
		S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH )
#endif // !IS_WINDOWS

// namespace TTR::Erreur
#include "Erreur/TTR_Erreur.h"

// namespace TTR::Action
#include "Action/TTR_Action.h"

// namespace TTR::Catalogue
#include "Catalogue/TTR_Catalogue.h"

#endif // !TTR_PROTECT

