#define COMMUN_INTERNE
#include "../include/Commun.h"

// -------------------------
// enum Commun::TypeCommande
// -------------------------

/**
 * Parser commande
 * 
 * @param commande
 * 		La commande a parser
 * 
 * @return la commande
 */
TypeCommande Commun_TypeCommande_Parser( const char *commande )
{
	// Sortie
	__OUTPUT TypeCommande out;
	
	// C'est un nombre?
	if( NLib_Chaine_EstUnNombre( commande ) )
		// Convertir
		out = strtol( commande,
			NULL,
			10 );
	// C'est du texte
	else
		// Chercher
		for( out = (TypeCommande)0; out < TYPES_COMMANDES; out++ )
			if( NLib_Chaine_Comparer( commande,
				TypeCommandeTexte[ out ],
				NFALSE,
				0 ) )
				return out;
				
	// OK?
	return out >= TYPES_COMMANDES ?
		TYPES_COMMANDES
		: out;
}

/**
 * Obtenir texte commande
 * 
 * @param commande
 * 		La commande
 * 
 * @return la commande texte
 */
const char *Commun_TypeCommande_ObtenirTexteCommande( TypeCommande commande )
{
	return TypeCommandeTexte[ commande ];
}

// ----------------
// namespace Commun
// ----------------

/**
 * Executer commande shell
 * 
 * @param lecteur
 * 		Le lecteur
 * 
 * @return la sortie texte de la commande
 */
__ALLOC char *Commun_ExecuterCommandeShell( NLecteurMemoire *lecteur )
{
	// Flux
	FILE *pipe;

	// Commande
	NU32 tailleCommande;
	char *commande;

	// Sortie
	char *sortie = NULL;
	NU32 tailleSortie = 0;
	char *nouvelleAdresse;
	
	// Caractere lu
	char caractere;

	// Lire taille commande
	if( !( tailleCommande = NLib_Memoire_NLecteurMemoire_Lire2( lecteur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Quitter
		return NULL;
	}
	
	// Lire commande
	if( !( commande = NLib_Memoire_NLecteurMemoire_LireCopieValeur( lecteur,
		tailleCommande,
		NTRUE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Quitter
		return NULL;
	}

	// Executer la commande
	if( !( pipe = popen( commande,
		"r" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Liberer
		NFREE( commande );
		
		// Quitter
		return NULL;
	}

	// Liberer
	NFREE( commande );
	
	// Lire la sortie
	while( ( caractere = fgetc( pipe ) ) != EOF )
	{
		// Allouer nouvel espace
		if( !( nouvelleAdresse = calloc( tailleSortie + 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			NFREE( sortie );

			// Fermer pipe
			pclose( pipe );

			// Quitter
			return NULL;
		}

		// Faire evoluer etat
		if( tailleSortie > 0 )
		{
			// Copier ancienne chaine
			memcpy( nouvelleAdresse,
				sortie,
				tailleSortie );

			// Liberer
			NFREE( sortie );
		}
		nouvelleAdresse[ tailleSortie++ ] = caractere;

		// Enregistrer
		sortie = nouvelleAdresse;
	}

	// Fermer le pipe
	pclose( pipe );
	
	// Creer le packet final
	if( !( nouvelleAdresse = calloc( tailleSortie + sizeof( NU32 ),
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );
		
		// Liberer
		NFREE( sortie );
		
		// Quitter
		return NULL;
	}
	
	// Ajouter taille
	if( tailleSortie > 0 )
	{
		*( (NU32*)nouvelleAdresse ) = tailleSortie - 1;
		memcpy( nouvelleAdresse + sizeof( NU32 ),
			sortie,
			tailleSortie - 1 );
	}
	
	// Liberer
	NFREE( sortie );
	
	// OK
	return nouvelleAdresse;
}

/**
 * Creer le packet contenant le fichier
 * 
 * @param packet
 * 		Le packet
 * @param source
 * 		Le fichier a obtenir
 * @param destination
 * 		Le fichier de sortie
 * 
 * @return si l'operation s'est bien passee
 */
NBOOL Commun_CreerPacketEnvoiFichier( __OUTPUT NPacket *packet,
	const char *source,
	const char *destination )
{
	// Fichier
	FILE *fichier;
	
	// Donnee
	char *data;
	
	// Taille
	NU32 taille;
	
	// Taille fichier
	NU32 tailleFichier;
	
	// Ouvrir fichier
	if( !( fichier = fopen( source,
		"rb" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );
		
		// Quitter
		return NFALSE;
	}
	
	// Obtenir les donnees
	tailleFichier = NLib_Fichier_Operation_ObtenirTaille( fichier );
	if( !( data = NLib_Fichier_Operation_LireContenu( fichier ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );
		
		// Fermer le fichier
		fclose( fichier );
		
		// Quitter
		return NFALSE;
	}
	
	// Inscrire taille fichier
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&tailleFichier,
		sizeof( NU32 ) );
		
	// Inscrire destination
	taille = strlen( destination );
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		(char*)&taille,
		sizeof( NU32 ) );
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		destination,
		taille );
		
	// Inscrire fichier
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
		data,
		tailleFichier );
		
	// Liberer
	NFREE( data );
	
	// Fermer le fichier
	fclose( fichier );
	
	// OK
	return NTRUE;
}

/**
 * Envoyer un fichier
 * 
 * @param lecteur
 * 		Le lecteur dans les donnees reçues
 * 
 * @return le packet en sortie
 */
__ALLOC char *Commun_EnvoyerFichier( NLecteurMemoire *lecteur )
{
	// Source
	char *source;
	NU32 tailleSource;
	
	// Destination
	char *destination;
	NU32 tailleDestination;
	
	// Packet
	NPacket *packet;
	
	// Sortie
	__OUTPUT char *out;
	
	// Lire taille source
	if( ( tailleSource = NLib_Memoire_NLecteurMemoire_Lire2( lecteur ) ) == NERREUR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Quitter
		return NULL;
	}
	
	// Lire source
	if( !( source = NLib_Memoire_NLecteurMemoire_LireCopieValeur( lecteur,
		tailleSource,
		NTRUE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Quitter
		return NULL;
	}
	
	// Lire taille destination
	if( ( tailleDestination = NLib_Memoire_NLecteurMemoire_Lire2( lecteur ) ) == NERREUR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Liberer
		NFREE( source );
		
		// Quitter
		return NULL;
	}
	
	// Lire destination
	if( !( destination = NLib_Memoire_NLecteurMemoire_LireCopieValeur( lecteur,
		tailleDestination,
		NTRUE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Liberer
		NFREE( source );
		
		// Quitter
		return NULL;
	}

	// Creer le packet vide
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );
		
		// Liberer
		NFREE( destination );
		NFREE( source );
		
		// Quitter
		return NULL;
	}
	
	// Creer le packet
	if( !Commun_CreerPacketEnvoiFichier( packet,
		source,
		destination ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Detruire packet
		NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );
		
		// Liberer
		NFREE( destination );
		NFREE( source );
		
		// Quitter
		return NULL;
	}
	
	// Liberer
	NFREE( destination );
	NFREE( source );
	
	// Allouer sortie
	if( !( out = calloc( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) + sizeof( NU32 ),
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Detruire packet
		NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );
		
		// Quitter
		return NULL;
	}
	
	// Copier
	*( (NU32*)out ) = NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet );
	memcpy( out + sizeof( NU32 ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) );
	
	// Detruire packet
	NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );
	
	// OK
	return out;
}

/**
 * Recevoir un fichier
 * 
 * @param lecteur
 * 		Le lecteur memoire
 * 
 * @return la sortie texte de l'operation
 */
__ALLOC char *Commun_RecevoirFichier( NLecteurMemoire *lecteur )
{
	// Fichier
	NU32 tailleFichier;
	const char *dataFichier;
	FILE *fichier;
	
	// Sortie
	char *sortie;
	
	// Destination
	NU32 tailleDestination;
	char *destination;

	// Lire source
	if( !( tailleFichier = NLib_Memoire_NLecteurMemoire_Lire2( lecteur ) )
		// Lire taille destination
		|| !( tailleDestination = NLib_Memoire_NLecteurMemoire_Lire2( lecteur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Quitter
		return NULL;
	}
	
	// Lire destination
	if( !( destination = NLib_Memoire_NLecteurMemoire_LireCopieValeur( lecteur,
		tailleDestination,
		NTRUE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Quitter
		return NULL;
	}
	
	// Ouvrir fichier sortie
	if( !( fichier = fopen( destination,
		"wb+" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );
		
		// Liberer
		NFREE( destination );
		
		// Quitter
		return NULL;
	}
	
	// Obtenir data fichier
	if( !( dataFichier = NLib_Memoire_NLecteurMemoire_Lire( lecteur,
		tailleFichier ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Fermer fichier
		fclose( fichier );
		
		// Liberer
		NFREE( destination );
		
		// Quitter
		return NULL;
	}
	
	// Inscrire
	if( fwrite( dataFichier,
		sizeof( char ),
		tailleFichier,
		fichier ) != tailleFichier )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Fermer fichier
		fclose( fichier );
		
		// Liberer
		NFREE( destination );
		
		// Quitter
		return NULL;
	}
		
	// Fermer fichier
	fclose( fichier );
	
	// Allouer sortie
	if( ( sortie = calloc( sizeof( NU32 ) + tailleDestination + 4,
		sizeof( char ) ) ) != NULL )
	{
		*( (NU32*)sortie ) = tailleDestination + 3;
		snprintf( sortie + sizeof( NU32 ),
			tailleDestination + 4,
			"%s OK",
			destination );
	}
	
	// Liberer
	NFREE( destination );

	// OK
	return sortie;
}
