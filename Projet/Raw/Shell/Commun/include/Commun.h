#ifndef COMMUN_PROTECT
#define COMMUN_PROTECT

// namespace NLib
#include "../../../../../NLib/include/NLib/NLib.h"

// -------------------------
// enum Commun::TypeCommande
// -------------------------

typedef enum TypeCommande
{
	TYPE_COMMANDE_SHELL,
	TYPE_COMMANDE_GET_FILE,
	TYPE_COMMANDE_SEND_FILE,
	TYPE_COMMANDE_HELP,
	TYPE_COMMANDE_QUIT,

	TYPES_COMMANDES
} TypeCommande;

#ifdef COMMUN_INTERNE
static const char TypeCommandeTexte[ TYPES_COMMANDES ][ 32 ] =
{
	"Shell",
	"GetFile",
	"SendFile",
	"Help",
	"Exit"
};
#endif // COMMUN_INTERNE

/**
 * Parser commande
 * 
 * @param commande
 * 		La commande a parser
 * 
 * @return la commande
 */
TypeCommande Commun_TypeCommande_Parser( const char *commande );

/**
 * Obtenir texte commande
 * 
 * @param commande
 * 		La commande
 * 
 * @return la commande texte
 */
const char *Commun_TypeCommande_ObtenirTexteCommande( TypeCommande commande );

// -------------------
// enum Commun::Packet
// -------------------

typedef enum Packet
{
	/**
	 * Commande shell
	 *
	 * v---------------v---------------v
	 * |Taille commande|NU32           |
	 * |---------------|---------------|
	 * |Commande       |Taille Commande|
	 * v---------------v---------------v
	 */
	PACKET_SHELL,

	/**
	 * Serveur <=== Client (Requete)
	 *
	 * v------------------v------------------v
	 * |Taille source     |NU32              |
 	 * |------------------|------------------|
	 * |Source            |Taille source     |
	 * |------------------|------------------|
	 * |Taille destination|NU32              |
	 * |------------------|------------------|
	 * |Destination       |Taille destination|
	 * v------------------v------------------v
	 */
	PACKET_GET_FILE,

	/**
 	 * Serveur ===> Client OU Serveur <=== Client (Reponse a PACKET_GET_FILE)
	 *
	 * v------------------v------------------v
 	 * |Taille Fichier    |NU32              |
	 * |------------------|------------------|
 	 * |Taille Destination|NU32              |
	 * |------------------|------------------|
 	 * |Destination       |Taille destination|
	 * |------------------|------------------|
 	 * |Fichier           |Taille fichier    |
	 * v------------------v------------------v
	 */
	PACKET_SEND_FILE,

	/**
	 * Retour texte d'une commande
	 *
	 * v------------v------------v
	 * |Taille Texte|NU32        |
	 * |------------|------------|
	 * |Retour Cmd. |Taille Texte|
	 * v------------v------------v
	 */
	PACKET_RETOUR_TEXTE,

	PACKETS,

	// Aucun packet
	PACKET_AUCUN = (NU32)NERREUR
} Packet;

nassert( sizeof( Packet ) == sizeof( NU32 ),
	"Taille packet incorrecte." );

// ----------------
// namespace Commun
// ----------------

/**
 * Executer commande shell
 * 
 * @param lecteur
 * 		Le lecteur
 * 
 * @return la sortie texte de la commande
 */
__ALLOC char *Commun_ExecuterCommandeShell( NLecteurMemoire *lecteur );

/**
 * Creer le packet contenant le fichier
 * 
 * @param packet
 * 		Le packet
 * @param source
 * 		Le fichier a obtenir
 * @param destination
 * 		Le fichier de sortie
 * 
 * @return si l'operation s'est bien passee
 */
NBOOL Commun_CreerPacketEnvoiFichier( __OUTPUT NPacket *packet,
	const char *source,
	const char *destination );

/**
 * Envoyer un fichier
 * 
 * @param lecteur
 * 		Le lecteur dans les donnees reçues
 * 
 * @return le packet en sortie
 */
__ALLOC char *Commun_EnvoyerFichier( NLecteurMemoire *lecteur );

/**
 * Recevoir un fichier
 * 
 * @param lecteur
 * 		Le lecteur memoire
 * 
 * @return la sortie texte de l'operation
 */
__ALLOC char *Commun_RecevoirFichier( NLecteurMemoire *lecteur );

#endif // !COMMUN_PROTECT

