#include "../include/Client.h"

// -----------------
// struct EtatClient
// -----------------

typedef struct EtatClient
{
	// Est en cours?
	NU32 estEnCours;

	// Temps de la derniere tentative de connexion
	NU32 tempsDerniereTentativeConnexion;
} EtatClient;

/**
 * Construire client
 *
 * @return l'instance du client
 */
__ALLOC EtatClient *EtatClient_Construire( void )
{
	// Sortie
	__OUTPUT EtatClient *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( EtatClient ) ) ) )
	{
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire client
 *
 * @param this
 *		Cette instance
 */
void EtatClient_Detruire( EtatClient **this )
{
	NFREE( (*this) );
}

// ----------------
// namespace Client
// ----------------

#define TEMPS_ENTRE_TENTATIVE_CONNEXION		5000

/**
 * Callback reception packet
 *
 * @param packet
 *		Le packet reçu
 * @param client
 *		L'instance du client
 *
 * @return si l'operation s'est bien passee
 */
NBOOL CallbackReception( const NPacket *packet,
	const NClient *client )
{
	// Sortie
	char *sortie = NULL;

	// Lecteur
	NLecteurMemoire *lecteur;

	// Packet
	NPacket *packetAEnvoyer;
	
	// Type de packet
	Packet typePacket;
	
	// Construire le lecteur
	if( !( lecteur = NLib_Memoire_NLecteurMemoire_Construire( NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );
		
		// Quitter
		return NFALSE;
	}
	
	// Analyser
	switch( NLib_Memoire_NLecteurMemoire_Lire2( lecteur ) )
	{
		// Commande de shell
		case PACKET_SHELL:
			sortie = Commun_ExecuterCommandeShell( lecteur );
			typePacket = PACKET_RETOUR_TEXTE;
			break;
			
		// On doit recevoir un fichier
		case PACKET_SEND_FILE:
			sortie = Commun_RecevoirFichier( lecteur );
			typePacket = PACKET_RETOUR_TEXTE;
			break;
			
		// On doit envoyer un fichier
		case PACKET_GET_FILE:
			sortie = Commun_EnvoyerFichier( lecteur );
			typePacket = PACKET_SEND_FILE;
			break;
			
		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
			
			// Detruire lecteur
			NLib_Memoire_NLecteurMemoire_Detruire( &lecteur );
			
			// Quitter
			return NFALSE;
	}
	
	// Il y a une sortie?
	if( sortie != NULL )
	{
		// Creer packet
		if( !( packetAEnvoyer = NLib_Module_Reseau_Packet_NPacket_Construire3( (char*)&typePacket,
			sizeof( Packet ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			NFREE( sortie );
			
			// Detruire lecteur
			NLib_Memoire_NLecteurMemoire_Detruire( &lecteur );

			// Quitter
			return NFALSE;
		}
		
		// Ajouter donnees
		if( !NLib_Module_Reseau_Packet_NPacket_AjouterData( packetAEnvoyer,
			sortie,
			*( (NU32*)sortie ) + sizeof( NU32 ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Detruire packet
			NLib_Module_Reseau_Packet_NPacket_Detruire( &packetAEnvoyer );

			// Liberer
			NFREE( sortie );

			// Detruire lecteur
			NLib_Memoire_NLecteurMemoire_Detruire( &lecteur );
			
			// Quitter
			return NFALSE;
		}

		// Liberer
		NFREE( sortie );

		// Ajouter le packet
		if( !NLib_Module_Reseau_Client_NClient_AjouterPacket( (NClient*)client,
			packetAEnvoyer ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );
			
			// Detruire lecteur
			NLib_Memoire_NLecteurMemoire_Detruire( &lecteur );
			
			// Quitter
			return NFALSE;
		}
	}
	
	// Detruire lecteur
	NLib_Memoire_NLecteurMemoire_Detruire( &lecteur );

	// OK
	return NTRUE;
}

/**
 * Callback deconnexion
 *
 * @param client
 *		L'instance du client qui se deconnecte
 *
 * @return si l'operation s'est bien passee
 */
NBOOL CallbackDeconnexion( const NClient *client )
{
	return NTRUE;
}

/**
 * Point d'entree
 *
 * @param argc
 *		Le nombre d'arguments
 * @param argv
 *		Le vecteur d'arguments
 *
 * @return EXIT_SUCCESS si tout s'est bien passe
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Client
	NClient *client;

	// Etat client
	EtatClient *etatClient;

	// Verifier
	if( argc <= 2 )
	{
		puts( "NClient ADRESSE PORT" );
		return EXIT_FAILURE;
	}

	// Initialiser NLib
	NLib_Initialiser( NULL );

	// Creer l'etat du client
	if( !( etatClient = EtatClient_Construire( ) ) )
	{
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );
		NLib_Detruire( );
		return EXIT_FAILURE;
	}

	// Construire client
	if( !( client = NLib_Module_Reseau_Client_NClient_Construire( argv[ 1 ],
		strtol( argv[ 2 ],
			NULL,
			10 ),
		CallbackReception,
		CallbackDeconnexion,
		etatClient,
		0,
		NTYPE_CLIENT_TCP,
		NMETHODE_TRANSMISSION_PACKETIO_NPROJECT ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		EtatClient_Detruire( &etatClient );

		// Fermer NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Boucle principale
	etatClient->estEnCours = NTRUE;
	do
	{
		// On est connecte?
		if( !NLib_Module_Reseau_Client_NClient_EstConnecte( client ) )
		{
			if( NLib_Temps_ObtenirTick( ) - etatClient->tempsDerniereTentativeConnexion >= TEMPS_ENTRE_TENTATIVE_CONNEXION )
			{
				printf( "Connexion a %s:%s... ",
					argv[ 1 ],
					argv[ 2 ] );
				if( NLib_Module_Reseau_Client_NClient_Connecter( client ) )
					puts( "OK" );
				else
					puts( "ECHEC" );
				etatClient->tempsDerniereTentativeConnexion = NLib_Temps_ObtenirTick( );
			}
		}
		else
			if( NLib_Module_Reseau_Client_NClient_EstErreur( client ) )
			{
				NLib_Module_Reseau_Client_NClient_Deconnecter( client );
				printf( "Deconnexion de %s:%s...\n",
					argv[ 1 ],
					argv[ 2 ] );
			}

		// Attendre
		NLib_Temps_Attendre( 1 );
	} while( etatClient->estEnCours );

	// Detruire le client
	NLib_Module_Reseau_Client_NClient_Detruire( &client );

	// Detruire etat client
	EtatClient_Detruire( &etatClient );

	// Quitter NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}


