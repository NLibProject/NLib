////////////////////////////////////////////////////////////
//
// SFML - Simple and Fast Multimedia Library
// Copyright (C) 2007-2015 Laurent Gomila (laurent@sfml-dev.org)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_SYSTEM_HPP
#define SFML_SYSTEM_HPP

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////

#include <SFMLx64/Config.hpp>
#include <SFMLx64/System/Clock.hpp>
#include <SFMLx64/System/Err.hpp>
#include <SFMLx64/System/FileInputStream.hpp>
#include <SFMLx64/System/InputStream.hpp>
#include <SFMLx64/System/Lock.hpp>
#include <SFMLx64/System/MemoryInputStream.hpp>
#include <SFMLx64/System/Mutex.hpp>
#include <SFMLx64/System/NonCopyable.hpp>
#include <SFMLx64/System/Sleep.hpp>
#include <SFMLx64/System/String.hpp>
#include <SFMLx64/System/Thread.hpp>
#include <SFMLx64/System/ThreadLocal.hpp>
#include <SFMLx64/System/ThreadLocalPtr.hpp>
#include <SFMLx64/System/Time.hpp>
#include <SFMLx64/System/Utf.hpp>
#include <SFMLx64/System/Vector2.hpp>
#include <SFMLx64/System/Vector3.hpp>

#endif // SFML_SYSTEM_HPP

////////////////////////////////////////////////////////////
/// \defgroup system System module
///
/// Base module of SFML, defining various utilities. It provides
/// vector classes, Unicode strings and conversion functions,
/// threads and mutexes, timing classes.
///
////////////////////////////////////////////////////////////
