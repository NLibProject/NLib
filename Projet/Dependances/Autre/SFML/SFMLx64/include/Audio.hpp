////////////////////////////////////////////////////////////
//
// SFML - Simple and Fast Multimedia Library
// Copyright (C) 2007-2015 Laurent Gomila (laurent@sfml-dev.org)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_AUDIO_HPP
#define SFML_AUDIO_HPP

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////

#include <SFMLx64/System.hpp>
#include <SFMLx64/Audio/InputSoundFile.hpp>
#include <SFMLx64/Audio/Listener.hpp>
#include <SFMLx64/Audio/Music.hpp>
#include <SFMLx64/Audio/OutputSoundFile.hpp>
#include <SFMLx64/Audio/Sound.hpp>
#include <SFMLx64/Audio/SoundBuffer.hpp>
#include <SFMLx64/Audio/SoundBufferRecorder.hpp>
#include <SFMLx64/Audio/SoundFileFactory.hpp>
#include <SFMLx64/Audio/SoundFileReader.hpp>
#include <SFMLx64/Audio/SoundFileWriter.hpp>
#include <SFMLx64/Audio/SoundRecorder.hpp>
#include <SFMLx64/Audio/SoundSource.hpp>
#include <SFMLx64/Audio/SoundStream.hpp>


#endif // SFML_AUDIO_HPP

////////////////////////////////////////////////////////////
/// \defgroup audio Audio module
///
/// Sounds, streaming (musics or custom sources), recording,
/// spatialization.
///
////////////////////////////////////////////////////////////
