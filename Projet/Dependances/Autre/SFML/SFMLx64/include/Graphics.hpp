////////////////////////////////////////////////////////////
//
// SFML - Simple and Fast Multimedia Library
// Copyright (C) 2007-2015 Laurent Gomila (laurent@sfml-dev.org)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_GRAPHICS_HPP
#define SFML_GRAPHICS_HPP

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////

#include <SFMLx64/Window.hpp>
#include <SFMLx64/Graphics/BlendMode.hpp>
#include <SFMLx64/Graphics/CircleShape.hpp>
#include <SFMLx64/Graphics/Color.hpp>
#include <SFMLx64/Graphics/ConvexShape.hpp>
#include <SFMLx64/Graphics/Drawable.hpp>
#include <SFMLx64/Graphics/Font.hpp>
#include <SFMLx64/Graphics/Glyph.hpp>
#include <SFMLx64/Graphics/Image.hpp>
#include <SFMLx64/Graphics/PrimitiveType.hpp>
#include <SFMLx64/Graphics/Rect.hpp>
#include <SFMLx64/Graphics/RectangleShape.hpp>
#include <SFMLx64/Graphics/RenderStates.hpp>
#include <SFMLx64/Graphics/RenderTarget.hpp>
#include <SFMLx64/Graphics/RenderTexture.hpp>
#include <SFMLx64/Graphics/RenderWindow.hpp>
#include <SFMLx64/Graphics/Shader.hpp>
#include <SFMLx64/Graphics/Shape.hpp>
#include <SFMLx64/Graphics/Sprite.hpp>
#include <SFMLx64/Graphics/Text.hpp>
#include <SFMLx64/Graphics/Texture.hpp>
#include <SFMLx64/Graphics/Transform.hpp>
#include <SFMLx64/Graphics/Transformable.hpp>
#include <SFMLx64/Graphics/Vertex.hpp>
#include <SFMLx64/Graphics/VertexArray.hpp>
#include <SFMLx64/Graphics/View.hpp>


#endif // SFML_GRAPHICS_HPP

////////////////////////////////////////////////////////////
/// \defgroup graphics Graphics module
///
/// 2D graphics module: sprites, text, shapes, ...
///
////////////////////////////////////////////////////////////
