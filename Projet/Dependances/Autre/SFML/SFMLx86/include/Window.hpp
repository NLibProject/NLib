////////////////////////////////////////////////////////////
//
// SFML - Simple and Fast Multimedia Library
// Copyright (C) 2007-2015 Laurent Gomila (laurent@sfml-dev.org)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_SFML_WINDOW_HPP
#define SFML_SFML_WINDOW_HPP

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////

#include <SFMLx86/System.hpp>
#include <SFMLx86/Window/Context.hpp>
#include <SFMLx86/Window/ContextSettings.hpp>
#include <SFMLx86/Window/Event.hpp>
#include <SFMLx86/Window/Joystick.hpp>
#include <SFMLx86/Window/Keyboard.hpp>
#include <SFMLx86/Window/Mouse.hpp>
#include <SFMLx86/Window/Sensor.hpp>
#include <SFMLx86/Window/Touch.hpp>
#include <SFMLx86/Window/VideoMode.hpp>
#include <SFMLx86/Window/Window.hpp>
#include <SFMLx86/Window/WindowHandle.hpp>
#include <SFMLx86/Window/WindowStyle.hpp>



#endif // SFML_SFML_WINDOW_HPP

////////////////////////////////////////////////////////////
/// \defgroup window Window module
///
/// Provides OpenGL-based windows, and abstractions for
/// events and input handling.
///
////////////////////////////////////////////////////////////
