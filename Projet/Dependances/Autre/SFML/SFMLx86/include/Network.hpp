////////////////////////////////////////////////////////////
//
// SFML - Simple and Fast Multimedia Library
// Copyright (C) 2007-2015 Laurent Gomila (laurent@sfml-dev.org)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_NETWORK_HPP
#define SFML_NETWORK_HPP

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////

#include <SFMLx86/System.hpp>
#include <SFMLx86/Network/Ftp.hpp>
#include <SFMLx86/Network/Http.hpp>
#include <SFMLx86/Network/IpAddress.hpp>
#include <SFMLx86/Network/Packet.hpp>
#include <SFMLx86/Network/Socket.hpp>
#include <SFMLx86/Network/SocketHandle.hpp>
#include <SFMLx86/Network/SocketSelector.hpp>
#include <SFMLx86/Network/TcpListener.hpp>
#include <SFMLx86/Network/TcpSocket.hpp>
#include <SFMLx86/Network/UdpSocket.hpp>


#endif // SFML_NETWORK_HPP

////////////////////////////////////////////////////////////
/// \defgroup network Network module
///
/// Socket-based communication, utilities and higher-level
/// network protocols (HTTP, FTP).
///
////////////////////////////////////////////////////////////
