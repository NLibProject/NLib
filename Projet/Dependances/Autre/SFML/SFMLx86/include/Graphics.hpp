////////////////////////////////////////////////////////////
//
// SFML - Simple and Fast Multimedia Library
// Copyright (C) 2007-2015 Laurent Gomila (laurent@sfml-dev.org)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef SFML_GRAPHICS_HPP
#define SFML_GRAPHICS_HPP

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////

#include <SFMLx86/Window.hpp>
#include <SFMLx86/Graphics/BlendMode.hpp>
#include <SFMLx86/Graphics/CircleShape.hpp>
#include <SFMLx86/Graphics/Color.hpp>
#include <SFMLx86/Graphics/ConvexShape.hpp>
#include <SFMLx86/Graphics/Drawable.hpp>
#include <SFMLx86/Graphics/Font.hpp>
#include <SFMLx86/Graphics/Glyph.hpp>
#include <SFMLx86/Graphics/Image.hpp>
#include <SFMLx86/Graphics/PrimitiveType.hpp>
#include <SFMLx86/Graphics/Rect.hpp>
#include <SFMLx86/Graphics/RectangleShape.hpp>
#include <SFMLx86/Graphics/RenderStates.hpp>
#include <SFMLx86/Graphics/RenderTarget.hpp>
#include <SFMLx86/Graphics/RenderTexture.hpp>
#include <SFMLx86/Graphics/RenderWindow.hpp>
#include <SFMLx86/Graphics/Shader.hpp>
#include <SFMLx86/Graphics/Shape.hpp>
#include <SFMLx86/Graphics/Sprite.hpp>
#include <SFMLx86/Graphics/Text.hpp>
#include <SFMLx86/Graphics/Texture.hpp>
#include <SFMLx86/Graphics/Transform.hpp>
#include <SFMLx86/Graphics/Transformable.hpp>
#include <SFMLx86/Graphics/Vertex.hpp>
#include <SFMLx86/Graphics/VertexArray.hpp>
#include <SFMLx86/Graphics/View.hpp>


#endif // SFML_GRAPHICS_HPP

////////////////////////////////////////////////////////////
/// \defgroup graphics Graphics module
///
/// 2D graphics module: sprites, text, shapes, ...
///
////////////////////////////////////////////////////////////
