Contribution guide
==================

Rules
-----

Start your commit with a short header

Three operations permitted:

    - ADD: Add a file/functionality
    - REMOVE: Remove a file/functionnality
    - EDIT: A file/fonctionnality

Example
-------

    Test commit

    ADD Super functionnality

        => Details about functionnality

    REMOVE Something

    EDIT Something else
