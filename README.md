NLib
====

Description
-----------

NLib is a C written library which can deal with many interaction types.
With it you have some modules such as: networking, SDL, FModex, Directory
exploration, PNG gesture, MPEG gesture, Webcam acquision, and serial ports.

It also take care of low level stuff with thread and mutex parts.

NLib is OS-independant.

Debugging
---------

Memory leaks:

GDB:

    1) gdb PROGRAM/PATH

    2) handle SIG32 nostop

    5) continue

VALGRIND: 

    3) valgrind --vgdb=yes --vgdb-error=0 --leak-check=full --show-leak-kinds=all PROGRAM/PATH

    4) Copy the line with remote target in gdb

Modules
-------

    - NLIB_MODULE_FMODEX
    - NLIB_MODULE_MPEGWRITER
    - NLIB_MODULE_OPENSSL
    - NLIB_MODULE_PNGWRITER
    - NLIB_MODULE_REPERTOIRE
    - NLIB_MODULE_RESEAU
    - NLIB_MODULE_SDL
        libSDL2
    - NLIB_MODULE_SDL_IMAGE
        libSDL2_image
    - NLIB_MODULE_SDL_TTF
        libSDL2_ttf
    - NLIB_MODULE_SERIAL
    - NLIB_MODULE_SSH
        libssh2, libssl, libcrypto
    - NLIB_MODULE_WEBCAM

	Always link libpthread

Maintainer
----------

SOARES Lucas (lucas.soares@orange.com)

https://gitlab.com/NLucasSoares/NLib.git
