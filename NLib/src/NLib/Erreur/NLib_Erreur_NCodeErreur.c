#define NLIB_ERREUR_NCODEERREUR_INTERNE
#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ------------------------------
// enum NLib::Erreur::NCodeErreur
// ------------------------------

/* Obtention texte erreurs */
const char *NLib_Erreur_NCodeErreur_Traduire( NCodeErreur code )
{
	return ( code >= NERREURS ) ?
		NULL
		: NCodeErreurTexte[ code ];
}

/* Savoir si un code utilise le code utilisateur */
NBOOL NLib_Erreur_EstUtilisateurCodeUtilisateur( NCodeErreur code )
{
	// Analyse code
	switch( code )
	{
		case NERREUR_USER:
		case NERREUR_DEBOGAGE:
		case NERREUR_SOCKET_RECV:
		case NERREUR_SOCKET_SEND:
			return NTRUE;

		default:
			return NFALSE;
	}
}

