#define NLIB_ERREUR_NOTIFICATION_INTERNE
#include "../../../../include/NLib/NLib.h"

/**
 *	@author SOARES Lucas
 */

// ------------------------------------
// namespace NLib::Erreur::Notification
// ------------------------------------

/* Notifier */
void NLib_Erreur_Notification_Notifier( __WILLBEOWNED NErreur *erreur )
{
	// Envoyer au callback concerne
	if( erreur )
		m_fluxActif( erreur );
	// Erreur NULL
	else
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

	// Detruire erreur
	NLib_Erreur_NErreur_Detruire( &erreur );
}

/* Flux standard */
void NLib_Erreur_Notification_FluxStandard( const NErreur *erreur )
{
	// Afficher message?
	NBOOL estAfficher = NTRUE;

	// Date
	char *date;

	// On n'affichera pas les avertissements
#ifndef NLIB_ERREUR_NOTIFICATION_AFFICHER_AVERTISSEMENTS
	if( NLib_Erreur_NErreur_ObtenirNiveau( erreur ) <= NNIVEAU_ERREUR_AVERTISSEMENT )
		estAfficher = NFALSE;
#endif // NLIB_ERREUR_NOTIFICATION_AFFICHER_AVERTISSEMENTS

	// Doit afficher?
	switch( NLib_Erreur_NErreur_ObtenirCode( erreur ) )
	{
		case NERREUR_SOCKET_RECV:
		case NERREUR_SOCKET_SEND:
			//estAfficher = ( NLib_Erreur_NErreur_ObtenirCodeUtilisateur( erreur ) != 0 ); // (<=> errno != 0?)
			break;

		case NERREUR_USER:
			estAfficher = NTRUE;
			break;

		default:
			break;
	}

	// Afficher l'erreur
	if( estAfficher )
		switch( NLib_Erreur_NErreur_ObtenirNiveau( erreur ) )
		{
			case NNIVEAU_ERREUR_ERREUR:
				// Obtenir date
				date = NLib_Temps_ObtenirDateFormate2( NLib_Erreur_NErreur_ObtenirTimestamp( erreur ) );

				// Erreur
				printf( "[%s][%s]: %s( )::(%s)\n",
					date,
					NLib_Erreur_NNiveauErreur_Traduire( NLib_Erreur_NErreur_ObtenirNiveau( erreur ) ),
					NLib_Erreur_NErreur_ObtenirMessage( erreur ),
					NLib_Erreur_NCodeErreur_Traduire( NLib_Erreur_NErreur_ObtenirCode( erreur ) ) );

				// Liberer
				NFREE( date );

				// Origine
				printf( "\"%s\", ligne %d, code %d.\n\n",
					NLib_Erreur_NErreur_ObtenirFichier( erreur ),
					NLib_Erreur_NErreur_ObtenirLigne( erreur ),
					NLib_Erreur_NErreur_ObtenirCodeUtilisateur( erreur ) );
				break;

			case NNIVEAU_ERREUR_AVERTISSEMENT:
				puts( NLib_Erreur_NErreur_ObtenirMessage( erreur ) );
				break;

			default:
				break;
		}
}

/* Modifier le flux d'erreur */
NBOOL NLib_Erreur_Notification_ModifierCallbackFluxErreur( __CALLBACK void ( ___cdecl *callback )( const NErreur* ) )
{
	// Verifier
		// Parametre
			if( !callback )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

				// Quitter
				return NFALSE;
			}

	// Enregistrer le flux
	m_fluxActif = callback;

	// OK
	return NTRUE;
}

/* Restaurer flux d'erreur */
void NLib_Erreur_Notification_RestaurerCallbackFluxErreurInitial( void )
{
	m_fluxActif = NLib_Erreur_Notification_FluxStandard;
}

