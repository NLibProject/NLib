#include "../../../include/NLib/NLib.h"
#include "../../../include/NLib/Erreur/NLib_Erreur.h"

/*
	@author SOARES Lucas
*/

// ----------------------------
// struct NLib::Erreur::NErreur
// ----------------------------

/**
 * Copier texte erreur (privee)
 *
 * @param this
 * 		Cette instance
 * @param message
 * 		Le message d'erreur
 * @param fichier
 * 		Le fichier ou a eu lieu l'erreur
 *
 * @return si l'operation a reussi
 */
__PRIVATE NBOOL NLib_Erreur_NErreur_CopierTexteInterne( NErreur *this,
	const char *message,
	const char *fichier )
{
	// Tailles chaines
	NU32 tailleMessage,
		tailleFichier;

	// Allouer la memoire
	if( !( this->m_message = (char*)calloc( ( tailleMessage = (NU32)strlen( message ) ) + 1,
			sizeof( char ) ) )
		|| !( this->m_fichier = (char*)calloc( ( tailleFichier = (NU32)strlen( fichier ) ) + 1,
			sizeof( char ) ) ) )
	{
		// Liberer
		NFREE( this->m_message );

		// Quitter
		return NFALSE;
	}

	// Copier donnees
		// Message
			memcpy( this->m_message,
				message,
				tailleMessage );
		// Fichier
			memcpy( this->m_fichier,
				fichier,
				tailleFichier );

	// OK
	return NTRUE;
}

/**
 * Construire erreur
 *
 * @param codeErreur
 * 		Le code d'erreur
 * @param message
 * 		Le message d'erreur
 * @param codeUtilisateur
 * 		Le code utilisateur
 * @param fichier
 * 		Le fichier ou a eu lieu l'erreur
 * @param ligne
 * 		La ligne ou a eu lieu l'erreur
 * @param niveau
 * 		Le niveau de gravite de l'erreur
 *
 * @return l'instance
 */
__ALLOC NErreur *NLib_Erreur_NErreur_Construire( NCodeErreur codeErreur,
	const char *message,
	NU32 codeUtilisateur,
	const char *fichier,
	NU32 ligne,
	NNiveauErreur niveau )
{
	// Sortie
	__OUTPUT NErreur *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NErreur ) ) ) )
	{
		// Notifier
		printf( "%s( )::Impossible d'allouer la memoire.\n",
			__FUNCTION__ );

		// Quitter
		return NULL;
	}

	// Copier
	out->m_code = codeErreur;
	out->m_ligne = ligne;
	out->m_codeUtilisateur = codeUtilisateur;
	out->m_niveau = niveau;
	out->m_timestamp = NLib_Temps_ObtenirTimestamp( );

	// Copier texte
	NLib_Erreur_NErreur_CopierTexteInterne( out,
		message,
		fichier );

	// OK
	return out;
}

/**
 * Detruire erreur
 *
 * @param this
 * 		Cette instance
 */
void NLib_Erreur_NErreur_Detruire( NErreur **this )
{
	// Liberer
	NFREE( (*this)->m_message );
	NFREE( (*this)->m_fichier );

	// Detruire
	NFREE( *this );
}

/**
 * Obtenir code erreur
 *
 * @param this
 * 		Cette instance
 *
 * @return le code d'erreur
 */
NCodeErreur NLib_Erreur_NErreur_ObtenirCode( const NErreur *this )
{
	return this->m_code;
}

/**
 * Obtenir code erreur utilisateur
 *
 * @param this
 * 		Cette instance
 *
 * @return le code d'erreur utilisateur
 */
NU32 NLib_Erreur_NErreur_ObtenirCodeUtilisateur( const NErreur *this )
{
	return this->m_codeUtilisateur;
}

/**
 * Obtenir message
 *
 * @param this
 * 		Cette instance
 *
 * @return le message d'erreur
 */
const char *NLib_Erreur_NErreur_ObtenirMessage( const NErreur *this )
{
	return this->m_message;
}

/**
 * Obtenir ligne
 *
 * @param this
 * 		Cette instance
 *
 * @return la ligne de l'erreur
 */
NU32 NLib_Erreur_NErreur_ObtenirLigne( const NErreur *this )
{
	return this->m_ligne;
}

/**
 * Obtenir fichier
 *
 * @param this
 * 		Cette instance
 *
 * @return le fichier
 */
const char *NLib_Erreur_NErreur_ObtenirFichier( const NErreur *this )
{
	return this->m_fichier;
}

/**
 * Obtenir niveau erreur
 *
 * @param this
 * 		Cette instance
 *
 * @return le niveau d'erreur
 */
NNiveauErreur NLib_Erreur_NErreur_ObtenirNiveau( const NErreur *this )
{
	return this->m_niveau;
}

/**
 * Obtenir timestamp
 *
 * @param this
 * 		Cette instance
 *
 * @return le timestamp de l'erreur
 */
NU64 NLib_Erreur_NErreur_ObtenirTimestamp( const NErreur *this )
{
	return this->m_timestamp;
}
