#define NLIB_INTERNE
#include "../../include/NLib/NLib.h"

/**
 * @file
 * @brief Base de NLib
 * 
 * @author SOARES Lucas
 */

// --------------
// namespace NLib
// --------------

/* Initialiser NLib */
NBOOL NLib_Initialiser( void ( *callbackNotificationErreur )( const NErreur* ) )
{
#ifdef _DEBUG
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif // _DEBUG

	// Couleur console
#ifdef IS_WINDOWS
	system( "color 0F" );
#endif // IS_WINDOWS

	// Donner graine pour NLib_Temps_ObtenirNombreAleatoire
	srand( (NU32)NLib_Temps_ObtenirTimestamp( ) );

	// Modifier le callback par defaut au besoin
	if( callbackNotificationErreur != NULL )
		NLib_Erreur_Notification_ModifierCallbackFluxErreur( callbackNotificationErreur );

	// Initialiser les modules
	if( !NLib_Module_Initialiser( ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Quitter
		return NFALSE;
	}

	// OK
	return ( m_estInitialise = NTRUE );
}

/* Detruire NLib */
void NLib_Detruire( void )
{
	// Est initialise?
	if( !m_estInitialise )
		return;

	// Notifier fermeture
#ifdef IS_WINDOWS
	system( "title Fermeture..." );
#endif // IS_WINDOWS

	// Detruire les modules
	NLib_Module_Detruire( );

	// Plus initialise
	m_estInitialise = NFALSE;
}

