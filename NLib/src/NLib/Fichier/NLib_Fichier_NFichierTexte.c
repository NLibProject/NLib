#include "../../../include/NLib/NLib.h"
#include "../../../include/NLib/Fichier/NLib_Fichier_NFichierTexte.h"

/*
	@author SOARES Lucas
*/

// -----------------------------------
// struct NLib::Fichier::NFichierTexte
// -----------------------------------

/**
 * Construire fichier (prive)
 *
 * @param lien
 * 		Le lien du fichier a ouvrir
 * @param mode
 * 		Le mode d'ouverture souhaite
 *
 * @return l'instance du fichier
 */
__PRIVATE __ALLOC NFichierTexte *NLib_Fichier_NFichierTexte_ConstruireInterne( const char *lien,
	const char *mode )
{
	// Sortie
	__OUTPUT NFichierTexte *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NFichierTexte ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Ouvrir le fichier
	if( !( out->m_fichier = fopen( lien,
		mode ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_FILE_CANNOT_BE_OPENED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Determiner si il s'agit de lecture/ecriture
	switch( mode[ 0 ] )
	{
		case 'w':
			out->m_estLecture = NFALSE;
			break;

		case 'r':
			out->m_estLecture = NTRUE;
			break;

		default:
			break;
	}

	// Determiner la taille
	if( out->m_estLecture )
		out->m_taille = (NU32)NLib_Fichier_Operation_ObtenirTaille( out->m_fichier );

	// OK
	return out;
}

/**
 * Construire fichier texte en ecriture
 *
 * @param lien
 * 		Le lien du fichier
 * @param estEcraserContenu
 * 		Vider le fichier a l'ouverture?
 *
 * @return l'instance du fichier
 */
__ALLOC NFichierTexte *NLib_Fichier_NFichierTexte_ConstruireEcriture( const char *lien,
	NBOOL estEcraserContenu )
{
	// Construire
	return NLib_Fichier_NFichierTexte_ConstruireInterne( lien,
		estEcraserContenu ?
			"w+"
			: "a" );
}

/**
 * Construire fichier texte en lecture
 *
 * @param lien
 * 		Le lien du fichier
 *
 * @return l'instance du fichier
 */
__ALLOC NFichierTexte *NLib_Fichier_NFichierTexte_ConstruireLecture( const char *lien )
{
	return NLib_Fichier_NFichierTexte_ConstruireInterne( lien,
		"r" );
}

/**
 * Detruire le fichier
 *
 * @param this
 * 		Cette instance
 */
void NLib_Fichier_NFichierTexte_Detruire( NFichierTexte **this )
{
	// Fermer le fichier
	fclose( (*this)->m_fichier );

	// Liberer
	NFREE( *this );
}

/**
 * On est a la fin du fichier?
 *
 * @param this
 * 		Cette instance
 *
 * @return si EOF
 */
NBOOL NLib_Fichier_NFichierTexte_EstEOF( const NFichierTexte *this )
{
	return NLib_Fichier_Operation_EstEOF2( this->m_fichier,
		this->m_taille );
}

/**
 * Obtenir la taille du fichier
 *
 * @param this
 * 		Cette instance
 *
 * @return la taille du fichier
 */
NU32 NLib_Fichier_NFichierTexte_ObtenirTaille( const NFichierTexte *this )
{
	return this->m_estLecture ?
		this->m_taille
		: NFICHIER_CODE_ERREUR;
}

/**
 * Le fichier est ouvert en lecture?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le fichier est ouvert en lecture
 */
NBOOL NLib_Fichier_NFichierTexte_EstLecture( const NFichierTexte *this )
{
	return this->m_estLecture;
}

/**
 * Le fichier est ouvert en ecriture?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le fichier est ouvert en ecriture
 */
NBOOL NLib_Fichier_NFichierTexte_EstEcriture( const NFichierTexte *this )
{
	return (NBOOL)!this->m_estLecture;
}

/**
 * Lire caractere
 *
 * @param this
 * 		Cette instance
 * @param estConservePosition
 * 		Conserver la position?
 *
 * @return le caractere lu
 */
char NLib_Fichier_NFichierTexte_LireCaractere( NFichierTexte *this,
	NBOOL estConservePosition )
{
	// Position initiale
	NS64 positionInitiale;

	// Sortie
	__OUTPUT char sortie;

	// Verifier
	if( NLib_Fichier_NFichierTexte_EstEOF( this )
		|| !this->m_estLecture )
		return '\0';

	// Enregistrer position initiale
	positionInitiale = ftell( this->m_fichier );

	// Lire
	sortie = (char)fgetc( this->m_fichier );

	// Restaurer si necessaire
	if( estConservePosition )
		fseek( this->m_fichier,
			positionInitiale,
			SEEK_SET );

	// OK
	return sortie;
}

/**
 * Lire un nombre dans le fichier
 *
 * @param this
 * 		Cette instance
 * @param estSigne
 * 		Le nombre a lire est signe?
 * @param estConservePosition
 * 		Conserver la position?
 *
 * @return le nombre lu ou NFICHIER_CODE_ERREUR en cas d'erreur
 */
NU32 NLib_Fichier_NFichierTexte_LireNombre( NFichierTexte *this,
	NBOOL estSigne,
	NBOOL estConservePosition )
{
	return estSigne ?
		NLib_Fichier_Operation_LireNombreSigne( this->m_fichier,
			estConservePosition )
		: NLib_Fichier_Operation_LireNombreNonSigne( this->m_fichier,
			estConservePosition );
}

/**
 * Lire un mot
 *
 * @param this
 * 		Cette instance
 * @param estConservePosition
 * 		Conserver la position?
 *
 * @return le mot lu
 */
__ALLOC char *NLib_Fichier_NFichierTexte_LireMot( NFichierTexte *this,
	NBOOL estConservePosition )
{
	// Position initiale
	NU32 positionInitiale;

	// Sortie
	__OUTPUT char *sortie;

	// Enregistrer position initiale
	positionInitiale = (NU32)ftell( this->m_fichier );

	// Lire
	sortie = NLib_Fichier_Operation_LireEntre( this->m_fichier,
		'\0',
		'\0',
		NTRUE );

	// Restaurer si necessaire
	if( estConservePosition )
		// Restaurer
		fseek( this->m_fichier,
			positionInitiale,
			SEEK_SET );

	// OK
	return sortie;
}

/**
 * Lire une ligne
 *
 * @param this
 * 		Cette instance
 * @param estConservePosition
 * 		Conserver la position?
 *
 * @return la ligne lue
 */
__ALLOC char *NLib_Fichier_NFichierTexte_LireLigne( NFichierTexte *this,
	NBOOL estConservePosition )
{
	// Position initiale
	NU32 positionInitiale;

	// Sortie
	__OUTPUT char *sortie;

	// Enregistrer position initiale
	positionInitiale = (NU32)ftell( this->m_fichier );

	// Lire
	sortie = NLib_Fichier_Operation_LireEntre( this->m_fichier,
		'\0',
		'\n',
		NTRUE );

	// Restaurer si necessaire
	if( estConservePosition )
		// Restaurer
		fseek( this->m_fichier,
			positionInitiale,
			SEEK_SET );

	// OK
	return sortie;
}

/**
 * Chercher et lire prochaine ligne sans commentaire
 *
 * @param this
 * 		Cette instance
 * @param caractereCommentaire
 * 		Le caractere identifiant un commentaire
 * @param estConserverPosition
 * 		Conserver la position?
 *
 * @return la chaine lue ou NULL
 */
__ALLOC char *NLib_Fichier_NFichierTexte_LireProchaineLigneSansCommentaire( NFichierTexte *this,
	char caractereCommentaire,
	NBOOL estConserverPosition )
{
	// Sortie
	__OUTPUT char *sortie = NULL;

	// Curseur
	NU32 curseur = 0;

	// Position initiale
	long positionInitiale;

	// Enregistrer position
	positionInitiale = ftell( this->m_fichier );

	do
	{
		// Liberer
		NFREE( sortie );

		// Zero
		curseur = 0;

		// Lire
		if( !( sortie = NLib_Fichier_NFichierTexte_LireLigne( this,
			NFALSE ) ) )
		{
			// Restaurer position si necessaire
			if( estConserverPosition )
				fseek( this->m_fichier,
					positionInitiale,
					SEEK_SET );

			// Quitter
			return NULL;
		}
	} while( NLib_Chaine_EstVide( sortie )
		|| NLib_Chaine_ObtenirProchainCaractere2( sortie,
			&curseur,
			NFALSE,
			NFALSE ) == caractereCommentaire );

	// Restaurer position si necessaire
	if( estConserverPosition )
		fseek( this->m_fichier,
			positionInitiale,
			SEEK_SET );

	// OK
	return sortie;
}

/**
 * Lire un mot encadre de caracteres
 *
 * @param this
 * 		Cette instance
 * @param caractereGauche
 * 		Le delimiteur gauche (\0 pour ignorer)
 * @param caractereDroit
 * 		Le delimiteur droit
 * @param estConserverPosition
 * 		On conserve la position apres la lecture?
 * @param estForcerRelectureCaractereFin
 * 		On force la lecture du premier caractere lu jusqu'a trouver un caractere different de %caractereDroit%? (Defaut: NTRUE)
 *
 * @return ce qui a ete lu ou NULL
 */
__ALLOC char *NLib_Fichier_NFichierTexte_Lire( NFichierTexte *this,
	char caractereGauche,
	char caractereDroit,
	NBOOL estConserverPosition,
	NBOOL estForcerRelectureCaractereFin )
{
	// Sortie
	__OUTPUT char *sortie;

	// Position initiale
	NU32 positionInitiale;

	// Enregistrer position initiale
	positionInitiale = (NU32)ftell( this->m_fichier );

	// Lire
	sortie = NLib_Fichier_Operation_LireEntre( this->m_fichier,
		caractereGauche,
		caractereDroit,
		estForcerRelectureCaractereFin );

	// Restaurer la position
	if( estConserverPosition )
		// Restaurer
		fseek( this->m_fichier,
			positionInitiale,
			SEEK_SET );

	// OK
	return sortie;
}

/**
 * Lire une chaine entre deux delimiteurs
 *
 * @param this
 * 		Cette instance
 * @param caractereEncadrement
 * 		Le caractere encadrant
 * @param estConserverPosition
 * 		Conserver la position?
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Fichier_NFichierTexte_Lire2( NFichierTexte *this,
	char caractereEncadrement,
	NBOOL estConserverPosition )
{
	return NLib_Fichier_NFichierTexte_Lire( this,
		caractereEncadrement,
		caractereEncadrement,
		estConserverPosition,
		NTRUE );
}

/**
 * Ecrire entier non signe dans fichier
 *
 * @param this
 * 		Cette instance
 * @param nombre
 * 		Le nombre a inscrire
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_Ecrire( NFichierTexte *this,
	NU32 nombre )
{
	return (NBOOL)( fprintf( this->m_fichier,
		"%u",
		nombre ) > 0 );
}

/**
 * Ecrire entier signe
 *
 * @param this
 * 		Cette instance
 * @param nombre
 * 		Le nombre a inscrire
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_Ecrire2( NFichierTexte *this,
	NS32 nombre )
{
	return (NBOOL)( fprintf( this->m_fichier,
		"%d",
		nombre ) > 0 );
}

/**
 * Ecrire chaine
 *
 * @param this
 * 		Cette instance
 * @param chaine
 * 		La chaine a inscrire
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_Ecrire3( NFichierTexte *this,
	const char *chaine )
{
	return (NBOOL)( fputs( chaine,
		this->m_fichier ) >= 0 );
}

/**
 * Ecrire nombre reel
 *
 * @param this
 * 		Cette instance
 * @param nombre
 * 		Le nombre a inscrire
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_Ecrire4( NFichierTexte *this,
	float nombre )
{
	return NLib_Fichier_NFichierTexte_Ecrire5( this,
		(double)nombre );
}

/**
 * Ecrire nombre reel
 *
 * @param this
 * 		Cette instance
 * @param nombre
 * 		Le nombre a inscrire
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_Ecrire5( NFichierTexte *this,
	double nombre )
{
	return (NBOOL)( fprintf( this->m_fichier,
		"%f",
		nombre ) > 0 );
}

/**
 * Ecrire caractere
 *
 * @param this
 * 		Cette instance
 * @param caractere
 * 		Le caractere a inscrire
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_Ecrire6( NFichierTexte *this,
	char caractere )
{
	return (NBOOL)( fprintf( this->m_fichier,
		"%c",
		caractere ) > 0 );
}

/**
 * Positionner curseur apres le caractere
 *
 * @param this
 * 		Cette instance
 * @param caractere
 * 		Le caractere apres lequel se placer
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_PositionnerProchainCaractere( NFichierTexte *this,
	char caractere )
{
	return NLib_Fichier_Operation_PlacerCaractere( this->m_fichier,
		caractere );
}

/**
 * Positionner le curseur apres la chaine
 *
 * @param this
 * 		Cette instance
 * @param chaine
 * 		La chaine
 * @param estDoitRetournerDebut
 * 		La recherche part du debut?
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_PositionnerProchaineChaine( NFichierTexte *this,
	const char *chaine,
	NBOOL estDoitRetournerDebut )
{
	return NLib_Fichier_Operation_PlacerChaine( this->m_fichier,
		chaine,
		estDoitRetournerDebut );
}

/**
 * Obtenir fichier
 *
 * @param this
 * 		Cette instance
 *
 * @return le fichier
 */
FILE *NLib_Fichier_NFichierTexte_ObtenirFichier( NFichierTexte *this )
{
	return this->m_fichier;
}

