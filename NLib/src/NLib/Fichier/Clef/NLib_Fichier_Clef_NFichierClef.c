#include "../../../../include/NLib/NLib.h"
#include "../../../../include/NLib/Fichier/Clef/NLib_Fichier_Clef.h"

// ----------------------------------------
// struct NLib::Fichier::Clef::NFichierClef
// ----------------------------------------

/**
 * Construire un ensemble de valeurs associes aux clefs, valeurs recuperees dans le fichier
 *
 * @param lien
 * 		Le lien vers le fichier
 * @param clefs
 * 		La liste des clefs
 * @param nombreClefs
 * 		Le nombre de clefs
 * @param estCorrigerValeur
 * 		Est ce qu'on supprime les espaces au debut de la valeur, et les retour chariot/nouvelle ligne a la fin?
 *
 * @return l'instance du fichier clefs
 */
__ALLOC NFichierClef *NLib_Fichier_Clef_NFichierClef_Construire( const char *lien,
	const char **clefs,
	NU32 nombreClefs,
	NBOOL estCorrigerValeur )
{
	// Fichier texte
	NFichierTexte *fichier;

	// Iterateurs
	NU32 i,
		j;

	// Sortie
	__OUTPUT NFichierClef *out;

	// Construire le fichier
	if( !( fichier = NLib_Fichier_NFichierTexte_ConstruireLecture( lien ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NFichierClef ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Fermer le fichier
		NLib_Fichier_NFichierTexte_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Allouer valeurs
	if( !( out->m_valeurs = calloc( nombreClefs,
		sizeof( char* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Fermer le fichier
		NLib_Fichier_NFichierTexte_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Enregistrer le nombre de clefs
	out->m_nombreClefs = nombreClefs;

	// Charger
	for( i = 0; i < nombreClefs; i++ )
	{
		// Positionner a la clef
		if( !NLib_Fichier_NFichierTexte_PositionnerProchaineChaine( fichier,
			clefs[ i ],
			NTRUE ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

			// Liberer
			for( j = 0; j < i; j++ )
				NFREE( out->m_valeurs[ j ] );
			NFREE( out->m_valeurs );
			NFREE( out );

			// Fermer le fichier
			NLib_Fichier_NFichierTexte_Detruire( &fichier );

			// Quitter
			return NULL;
		}

		// Lire
		if( ( out->m_valeurs[ i ] = NLib_Fichier_NFichierTexte_Lire( fichier,
			'\0',
			'\n',
			NFALSE,
			NFALSE ) ) == NULL )
		{
			// Creer une valeur vide (aucune valeur associee a cette clef)
			if( !( out->m_valeurs[ i ] = calloc( 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

				// Liberer
				for( j = 0; j < i; j++ )
				NFREE( out->m_valeurs[ j ] );
				NFREE( out );

				// Fermer le fichier
				NLib_Fichier_NFichierTexte_Detruire( &fichier );

				// Quitter
				return NULL;
			}
		}
		else
			if( estCorrigerValeur )
			{
				// Supprimer espaces debut
				NLib_Chaine_SupprimerCaractereDebut( out->m_valeurs[ i ],
					' ' );

				// Supprimer nouvelle ligne
				NLib_Chaine_SupprimerCaractere( out->m_valeurs[ i ],
					'\n' );
				NLib_Chaine_SupprimerCaractere( out->m_valeurs[ i ],
					'\r' );
			}
	}

	// Fermer le fichier
	NLib_Fichier_NFichierTexte_Detruire( &fichier );
	
	// OK
	return out;
}

/**
 * Detruire le fichier
 *
 * @param this
 * 		Cette instance
 */
void NLib_Fichier_Clef_NFichierClef_Detruire( NFichierClef **this )
{
	// Iterateur
	NU32 i = 0;

	// Liberer valeurs
	for( ; i < (*this)->m_nombreClefs; i++ )
		NFREE( (*this)->m_valeurs[ i ] );
	NFREE( (*this)->m_valeurs );

	// Liberer
	NFREE( (*this) );
}


/**
 * Obtenir valeur texte
 *
 * @param this
 * 		Cette instance
 * @param clef
 * 		La clef
 *
 * @return la valeur texte
 */
const char *NLib_Fichier_Clef_NFichierClef_ObtenirValeur( const NFichierClef *this,
	NU32 clef )
{
	// Verifier
	if( clef >= this->m_nombreClefs )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	return this->m_valeurs[ clef ];
}

/**
 * Obtenir valeur entiere non signee
 *
 * @param this
 * 		Cette instance
 * @param clef
 * 		La clef
 *
 * @return la valeur entiere non signee ou NERREUR si une erreur a eu lieu
 */
NU32 NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( const NFichierClef *this,
	NU32 clef )
{
	// Verifier
	if( clef >= this->m_nombreClefs )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NERREUR;
	}

	return (NU32)strtol( this->m_valeurs[ clef ],
		NULL,
		10 );
}

/**
 * Obtenir valeur entiere signee
 *
 * @param this
 * 		Cette instance
 * @param clef
 * 		La clef
 *
 * @return la valeur entiere signee
 */
NS32 NLib_Fichier_Clef_NFichierClef_ObtenirValeur3( const NFichierClef *this,
	NU32 clef )
{
	// Verifier
	if( clef >= this->m_nombreClefs )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return 0;
	}

	return (NS32)strtol( this->m_valeurs[ clef ],
		NULL,
		10 );
}

/**
 * Obtenir copie valeur texte
 *
 * @param this
 * 		Cette instance
 * @param clef
 * 		La clef
 *
 * @return la copie de la valeur texte
 */
__ALLOC char *NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( const NFichierClef *this,
	NU32 clef )
{
	// Sortie
	__OUTPUT char *out;

	// Verifier
	if( clef >= this->m_nombreClefs )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( strlen( this->m_valeurs[ clef ] ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out,
		this->m_valeurs[ clef ],
		strlen( this->m_valeurs[ clef ] ) );

	// OK
	return out;
}

