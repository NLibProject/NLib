#include "../../../include/NLib/NLib.h"
#include "../../../include/NLib/Fichier/NLib_Fichier_NFichierBinaire.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------
// struct NLib::Fichier::NFichierBinaire
// -------------------------------------

/* Construire (privee) */
__ALLOC NFichierBinaire *NLib_Fichier_NFichierBinaire_ConstruireInterne( const char *lien,
	const char *mode )
{
	// Sortie
	__OUTPUT NFichierBinaire *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NFichierBinaire ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Ouvrir le fichier
	if( !( out->m_fichier = fopen( lien,
		mode ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Determiner si il s'agit de lecture/ecriture
	switch( mode[ 0 ] )
	{
		case 'w':
			out->m_estLecture = NFALSE;
			break;

		case 'r':
			out->m_estLecture = NTRUE;
			break;

		default:
			break;
	}

	// Determiner la taille
	if( out->m_estLecture )
		out->m_taille = NLib_Fichier_Operation_ObtenirTaille( out->m_fichier );

	// OK
	return out;
}

/* Construire */
__ALLOC NFichierBinaire *NLib_Fichier_NFichierBinaire_ConstruireEcriture( const char *lien,
	NBOOL estEcraserContenu )
{
	// Mode
	char mode[ 48 ];
	
	// Creer mode
	strcpy( mode,
		estEcraserContenu ?
			"wb+"
			: "wb" );

	// Construire
	return NLib_Fichier_NFichierBinaire_ConstruireInterne( lien,
		mode );
}

__ALLOC NFichierBinaire *NLib_Fichier_NFichierBinaire_ConstruireLecture( const char *lien )
{
	return NLib_Fichier_NFichierBinaire_ConstruireInterne( lien,
		"rb" );
}

/* Detruire */
void NLib_Fichier_NFichierBinaire_Detruire( NFichierBinaire **this )
{
	// Fermer le fichier
	fclose( (*this)->m_fichier );

	// Liberer
	NFREE( *this );
}

/* Est EOF? */
NBOOL NLib_Fichier_NFichierBinaire_EstEOF( const NFichierBinaire *this )
{
	return NLib_Fichier_Operation_EstEOF2( this->m_fichier,
		this->m_taille );
}

/* Obtenir taille */
NU64 NLib_Fichier_NFichierBinaire_ObtenirTaille( const NFichierBinaire *this )
{
	return this->m_estLecture ?
		this->m_taille
		: NFICHIER_CODE_ERREUR;
}

/* Est lecture? */
NBOOL NLib_Fichier_NFichierBinaire_EstLecture( const NFichierBinaire *this )
{
	return this->m_estLecture;
}

/* Est ecriture? */
NBOOL NLib_Fichier_NFichierBinaire_EstEcriture( const NFichierBinaire *this )
{
	return (NBOOL)!this->m_estLecture;
}

/* Lire */
__ALLOC char *NLib_Fichier_NFichierBinaire_Lire( NFichierBinaire *this,
	NU32 taille )
{
	// Sortie
	__OUTPUT char *sortie;

	// Allouer la memoire
	if( !( sortie = calloc( taille,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Lire
	if( !NLib_Fichier_NFichierBinaire_Lire2( this,
		sortie,
		taille ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

		// Liberer
		NFREE( sortie );

		// Quitter
		return NULL;
	}

	// OK
	return sortie;
}

NBOOL NLib_Fichier_NFichierBinaire_Lire2( NFichierBinaire *this,
	__OUTPUT char *sortie,
	NU32 taille )
{
	// Lire
	return (NBOOL)( fread( sortie,
		sizeof( char ),
		taille,
		this->m_fichier ) == taille );
}

/* Ecrire */
NBOOL NLib_Fichier_NFichierBinaire_Ecrire( NFichierBinaire *this,
	const char *buffer,
	NU32 tailleBuffer )
{
	return (NBOOL)( fwrite( buffer,
		sizeof( char ),
		tailleBuffer,
		this->m_fichier ) == tailleBuffer );
}

/**
 * Retourner au debut du fichier
 *
 * @param this
 * 		Cette instance
 */
void NLib_Fichier_NFichierBinaire_DefinirPositionDebut( NFichierBinaire *this )
{
	fseek( this->m_fichier,
		0,
		SEEK_SET );
}

/**
 * Obtenir fichier FILE*
 *
 * @param this
 * 		Cette instance
 *
 * @return le fichier
 */
FILE *NLib_Fichier_NFichierBinaire_ObtenirFichier( NFichierBinaire *this )
{
	return this->m_fichier;
}

/**
 * Lire tout le contenu
 *
 * @param this
 * 		Cette instance
 *
 * @return le contenu du fichier
 */
__ALLOC char *NLib_Fichier_NFichierBinaire_LireTout( NFichierBinaire *this )
{
	// Sortie
	__OUTPUT char *sortie;

	// Position initiale
	NU64 positionInitiale;

	// Allouer la memoire
	if( !( sortie = calloc( this->m_taille + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer position initiale
	positionInitiale = (NU64)ftell( this->m_fichier );

	// Retourner debut fichier
	fseek( this->m_fichier,
		0,
		SEEK_SET );

	// Lire
	if( fread( sortie,
		sizeof( char ),
		this->m_taille,
		this->m_fichier ) != this->m_taille * sizeof( char ) )
		NOTIFIER_AVERTISSEMENT( NERREUR_FILE );

	// Restaurer position
	fseek( this->m_fichier,
		(long)positionInitiale,
		SEEK_SET );

	// OK
	return sortie;
}

