#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------
// namespace NLib::Fichier::Operation
// ----------------------------------

/**
 * Obtenir la taille d'un fichier
 *
 * @param f
 * 		Le fichier
 *
 * @return la taille du fichier
 */
NU64 NLib_Fichier_Operation_ObtenirTaille( FILE *f )
{
	// Position initiale
	long positionInitiale;

	// Taille du fichier
	__OUTPUT NU64 taille;

	// Obtenir position initiale
	positionInitiale = ftell( f );

	// Aller a la fin du fichier
	fseek( f,
		0,
		SEEK_END );

	// Obtenir taille
	taille = (NU64)ftell( f );

	// Restaurer position
	fseek( f,
		positionInitiale,
		SEEK_SET );

	// OK
	return taille;
}

/**
 * Obtenir la taille du fichier
 *
 * @param lien
 * 		Le lien vers le fichier
 *
 * @return la taille du fichier
 */
NU64 NLib_Fichier_Operation_ObtenirTaille2( const char *lien )
{
	// Fichier
	FILE *fichier;

	// Taille
	__OUTPUT NU64 sortie;

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"r" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return 0;
	}

	// Lire la taille
	sortie = NLib_Fichier_Operation_ObtenirTaille( fichier );

	// Fermer le fichier
	fclose( fichier );

	// OK
	return sortie;
}

/**
 * Calculer le checksum d'un fichier
 *
 * @param fichier
 * 		Le fichier dont on veut le checksum
 *
 * @return le checksum du fichier
 */
NS32 NLib_Fichier_Operation_CalculerChecksum( FILE *fichier )
{
	// Sortie
	__OUTPUT NS32 crc = 0;

	// Position initiale
	long positionInitiale;

	// Caractere lu
	char caractere;

	// Obtenir position initiale
	positionInitiale = ftell( fichier );

	// Calculer
	while( !NLib_Fichier_Operation_EstEOF( fichier ) )
	{
		// Lire
		caractere = (char)fgetc( fichier );

		// Additionner
		crc += caractere;
	}

	// Restaurer position
	fseek( fichier,
		positionInitiale,
		SEEK_SET );

	// OK
	return crc;
}

/**
 * Calculer le checksum d'un fichier
 *
 * @param lien
 * 		Le lien du fichier
 *
 * @return le checksum du fichier
 */
NS32 NLib_Fichier_Operation_CalculerChecksum2( const char *lien )
{
	// Fichier
	FILE *fichier;

	// Taille
	__OUTPUT NS32 sortie;

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"rb" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return 0;
	}

	// Lire la taille
	sortie = NLib_Fichier_Operation_CalculerChecksum( fichier );

	// Fermer le fichier
	fclose( fichier );

	// OK
	return sortie;
}

/**
 * Le fichier est EOF?
 *
 * @param f
 * 		Le fichier
 *
 * @return Est EOF?
 */
NBOOL NLib_Fichier_Operation_EstEOF( FILE *f )
{
	// La taille
	NU64 taille;

	// Obtenir la taille
	taille = NLib_Fichier_Operation_ObtenirTaille( f );

	// Verifier
	return NLib_Fichier_Operation_EstEOF2( f,
		taille );
}

/**
 * Le fichier est EOF?
 *
 * @param f
 * 		Le fichier
 * @param taille
 * 		La taille du fichier
 *
 * @return Est EOF?
 */
NBOOL NLib_Fichier_Operation_EstEOF2( FILE *f,
	NU64 taille )
{
	// Verifier
	return (NBOOL)( ftell( f ) >= taille );
}

/**
 * Le fichier existe?
 *
 * @param lien
 * 		Le lien vers le fichier
 *
 * @return si le fichier existe
 */
NBOOL NLib_Fichier_Operation_EstExiste( const char *lien )
{
	// Fichier
	FILE *fichier;

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"r" ) ) )
		return NFALSE;
		// Le fichier existe
	else
	{
		// Fermer le fichier
		fclose( fichier );

		// Quitter
		return NTRUE;
	}
}

/**
 * Placer le curseur a la prochaine occurence du caractere
 *
 * @param f
 * 		Le fichier
 * @param c
 * 		Le caractere auquel se placer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Fichier_Operation_PlacerCaractere( FILE *f,
	char c )
{
	// Caractere lu
	char cL;

	// Chercher le caractere
	do
	{
		// Lire caractere
		cL = (char)fgetc( f );
	} while( !NLib_Fichier_Operation_EstEOF( f )
			 && c != cL );

	// OK
	return (NBOOL)( c == cL );
}

/**
 * Se placer a la chaine
 *
 * @param f
 * 		Le fichier
 * @param s
 * 		La chaine a laquelle se placer
 * @param estDoitRetournerAuDebut
 * 		On retourne au debut avant de se placer a cette chaine?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Fichier_Operation_PlacerChaine( FILE *f,
	const char *s,
	NBOOL estDoitRetournerAuDebut )
{
	// Curseur
	NU32 curseur = 0;

	// Longueur chaine
	long longueur;

	// Caractere lu
	char cL;

	// Obtenir la taille de la chaine a rechercher
	longueur = strlen( s );

	// Replacer au debut du fichier
	if( estDoitRetournerAuDebut )
		fseek( f,
			0,
			SEEK_SET );

	// Chercher la chaine
	while( curseur < longueur
		   && !NLib_Fichier_Operation_EstEOF( f ) )
	{
		// Obtenir caractere
		cL = (char)fgetc( f );

		// Caractere trouve
		if( cL == s[ curseur ] )
			curseur++;
			// Impossible de trouver le caractere
		else
			curseur = 0;
	}

	// OK?
	return (NBOOL)( curseur >= longueur );
}

/**
 * Lire chaine entre deux delimiteurs
 *
 * @param f
 * 		Le fichier
 * @param c1
 * 		Le caractere a gauche
 * @param c2
 * 		Le caractere a droite
 * @param estForcerRelectureCaractereFin
 * 		Est ce que si le premier caractere lu a droite est un %c2% au tout debut, on doit lire le caractere suivant? (Defaut: NTRUE)
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Fichier_Operation_LireEntre( FILE *f,
	char c1,
	char c2,
	NBOOL estForcerRelectureCaractereFin )
{
	// Sortie
	__OUTPUT char *out = NULL;

	// Buffer
	char buffer[ 2 ];

	// Caractere lu
	char cL;

	// Trouver le 1er caractere
	if( c1 != '\0' )
		if( !NLib_Fichier_Operation_PlacerCaractere( f,
			c1 ) )
			return NULL;

	// Lire
	do
	{
		// Lire caractere
		do
		{
			cL = (char)fgetc( f );
		} while( estForcerRelectureCaractereFin ? (NBOOL)( cL == c2 // forcer la lecture si le premier caractere est le caractere de fin
				&& out == NULL )
			: NFALSE );

		// Si caractere correct
		if( cL != (char)~0 )
		{
			// Si le caractere lu n'est pas le caractere de fin
			if( ( ( c2 == '\0' ) ?
				  !NLib_Caractere_EstUnSeparateur( cL )
				  : ( cL != c2 ) ) )
			{
				// Construire ce qu'il y a a ajouter
				buffer[ 0 ] = cL;
				buffer[ 1 ] = '\0';

				// Ajouter donnee
				NLib_Memoire_AjouterData( &out,
					(NU32)( out != NULL ?
						strlen( out )
						: 0 ),
					buffer,
					2 );
			}
		}
	} while( !NLib_Fichier_Operation_EstEOF( f )
			 && ( ( c2 == '\0' )
				  ? !NLib_Caractere_EstUnSeparateur( cL )
				  : ( cL != c2 ) ) );

	// On a une chaine?
	if( out != NULL )
		// Ajouter \0
			NLib_Memoire_AjouterData( &out,
				(NU32)strlen( out ),
				"",
				1 );

	// OK
	return out;
}

/**
 * Lire entre deux delimiteurs identiques
 *
 * @param f
 * 		Le fichier
 * @param c
 * 		Le delimiteur gauche/droit
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Fichier_Operation_LireEntre2( FILE *f,
	char c )
{
	return NLib_Fichier_Operation_LireEntre( f,
		c,
		c,
		NTRUE );
}

/**
 * Lire l'integralite du fichier
 *
 * @param f
 * 		Le fichier
 *
 * @return l'integralite du fichier dans une chaine
 */
__ALLOC char *NLib_Fichier_Operation_LireContenu( FILE *f )
{
	// Sortie
	__OUTPUT char *out;

	// Taille du fichier
	NU64 taille;

	// Position actuelle
	long positionActuelle = 0;

	// Obtenir taille fichier
	taille = NLib_Fichier_Operation_ObtenirTaille( f );

	// Obtenir position actuelle
	positionActuelle = ftell( f );

	// Aller au debut du fichier
	fseek( f,
		0,
		SEEK_SET );

	// Allouer la memoire
	if( !( out = calloc( (size_t)( taille + 1 ),
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Lire
	if( fread( out,
		sizeof( char ),
		(size_t)taille,
		f ) != taille )
		NOTIFIER_AVERTISSEMENT( NERREUR_FILE );

	// Restaurer la position
	fseek( f,
		positionActuelle,
		SEEK_SET );

	// OK
	return out;
}

/**
 * Lire nombre (prive)
 *
 * @param f
 * 		Le fichier
 * @param estConserverPosition
 * 		Restaurer la position apres lecture?
 * @param estSigne
 * 		Le nombre lu peut il etre signe?
 *
 * @return le nombre lu
 */
NU32 NLib_Fichier_Operation_LireNombreInterne( FILE *f,
	NBOOL estConserverPosition,
	NBOOL estSigne )
{
	// Position initiale
	long positionInitiale;

	// Taille du fichier
	NU64 taille;

	// Caractere lu
	char caractere;

	// Curseur
	NU32 curseur = 0;

#define TAILLE_BUFFER        4096

	// Buffer
	char buffer[ TAILLE_BUFFER ] = { 0, };

	// Obtenir la taille
	taille = NLib_Fichier_Operation_ObtenirTaille( f );

	// Enregistrer position initiale
	positionInitiale = ftell( f );

	// Chercher chiffre ou '-' si signe
	do
	{
		// Lire
		caractere = (char)fgetc( f );
	} while( ( !NLib_Caractere_EstUnChiffre( caractere )
			   && ( estSigne ?
					(NBOOL)( caractere != '-' )
					: NTRUE ) )
			 && !NLib_Fichier_Operation_EstEOF2( f,
		taille ) );

	// Aucun chiffre
	if( NLib_Fichier_Operation_EstEOF2( f,
		taille ) )
	{
		// Restaurer si necessaire
		if( estConserverPosition )
			fseek( f,
				positionInitiale,
				SEEK_SET );

		// Quitter
		return NFICHIER_CODE_ERREUR;
	}

	// Reculer d'une case
	fseek( f,
		-1,
		SEEK_CUR );

	// Vider le buffer
	memset( buffer,
		0,
		TAILLE_BUFFER );

	// Lire nombre
	do
	{
		// Lire
		caractere = (char)fgetc( f );

		// Verifier
		if( NLib_Caractere_EstUnChiffre( caractere )
			|| ( estSigne ?
				 (NBOOL)( caractere == '-' && !curseur )
				 : NFALSE ) )
			// Ajouter au buffer
			buffer[ curseur++ ] = caractere;
	} while( ( NLib_Caractere_EstUnChiffre( caractere )
		   || ( estSigne ?
				(NBOOL)( caractere == '-' && curseur == 1 )
				: NFALSE ) )
		 && strlen( buffer ) < TAILLE_BUFFER
		 && !NLib_Fichier_Operation_EstEOF2( f,
			taille ) );

#undef TAILLE_BUFFER

	// Restaurer si necessaire
	if( estConserverPosition )
		fseek( f,
			positionInitiale,
			SEEK_SET );

	// OK
	return (NU32)strtol( buffer,
		NULL,
		10 );
}

/**
 * Lire nombre non signe
 *
 * @param f
 * 		Le fichier
 * @param estConserverPosition
 * 		Restaurer la position apres lecture?
 *
 * @return le nombre lu
 */
NU32 NLib_Fichier_Operation_LireNombreNonSigne( FILE *f,
	NBOOL estConserverPosition )
{
	return NLib_Fichier_Operation_LireNombreInterne( f,
		estConserverPosition,
		NFALSE );
}

/**
 * Lire nombre signe
 *
 * @param f
 * 		Le fichier
 * @param estConserverPosition
 * 		Restaurer la position apres lecture?
 *
 * @return le nombre lu
 */
NU32 NLib_Fichier_Operation_LireNombreSigne( FILE *f,
	NBOOL estConserverPosition )
{
	return NLib_Fichier_Operation_LireNombreInterne( f,
		estConserverPosition,
		NTRUE );
}

/**
 * Obtenir prochain caractere
 *
 * @param f
 * 		Le fichier
 * @param estConserverSeparateur
 * 		On considere les separateurs comme des caracteres
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture?
 *
 * @return le caractere lu
 */
char NLib_Fichier_Operation_ObtenirProchainCaractere( FILE *f,
	NBOOL estConserverSeparateur,
	NBOOL estConserverPosition )
{
	// Position initiale
	long positionInitiale;

	// Sortie
	__OUTPUT char out = '\0';

	// Enregistrer position initiale
	positionInitiale = ftell( f );

	// Lire
	if( estConserverSeparateur )
		out = (char)fgetc( f );
	else
		while( !NLib_Fichier_Operation_EstEOF( f )
			   && NLib_Caractere_EstUnSeparateur( out = (char)fgetc( f ) ) );

	// Restaurer position initiale
	if( estConserverPosition )
		// Positionner
		fseek( f,
			positionInitiale,
			SEEK_SET );

	// OK
	return out;
}

/**
 * Ecrire data dans fichier en ecrasant le contenu
 *
 * @param lien
 * 		Le lien du fichier dans lequel ecrire
 * @param data
 * 		Les donnees a inscrire
 * @param taille
 * 		La taille des donnees a inscrire
 * @param estEcraserSiExiste
 * 		On ecrase le fichier si il existe?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Fichier_Operation_Ecrire( const char *lien,
	const char *data,
	NU64 taille,
	NBOOL ecraserSiExiste )
{
	// Fichier
	FILE *fichier;

	// Verifier si le fichier existe au besoin
	if( !ecraserSiExiste )
		if( NLib_Fichier_Operation_EstExiste( lien ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_ALREADY_EXISTS );

			// Quitter
			return NFALSE;
		}

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"wb+" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return NFALSE;
	}

	// Ecrire
	fwrite( data,
		sizeof( char ),
		(size_t)taille,
		fichier );

	// Fermer le fichier
	fclose( fichier );

	// OK
	return NTRUE;
}

/**
 * Determiner l'extension d'un fichier
 *
 * @param nom
 * 		Le nom du fichier
 *
 * @return l'extension du fichier
 */
__ALLOC char *NLib_Fichier_Operation_ObtenirExtension( const char *nom )
{
	// Etape
	NU32 etape = 0;

	// Curseur
	NU32 curseur = 0;

	// Tableau temporaire
	char *temp;

	// Caractere
	char buffer;

	// Sortie
	__OUTPUT char *extension = NULL;

	// Parcourir
	while( curseur < strlen( nom ) )
	{
		// Lire le caractere
		buffer = nom[ curseur ];

		// Suivant l'etape
		switch( etape )
		{
			// On est au nom du fichier
			case 0:
				if( buffer == '.' )
				{
					if( curseur == 0 )
						return NULL;
					else
						etape++;
				}
				break;

				// On a trouve un point
			case 1:
				if( buffer != '.' )
				{
					// Allouer la memoire
					if( !( extension = calloc( 2,
						sizeof( char ) ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

						// Quitter
						return NULL;
					}

					// Copier le caractere
					extension[ 0 ] = buffer;

					// Etape suivante
					etape++;
				}
				break;

				// On est en train de lire l'extension
			case 2:
				// Enregistrer l'ancien etat
				temp = extension;

				// Allouer
				if( !( extension = calloc( strlen( temp ) + 2,
					sizeof( char ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Liberer
					NFREE( temp );

					// Quitter
					return NULL;
				}

				// Copier
				memcpy( extension,
					temp,
					strlen( temp ) );

				// Ajouter le caractere
				extension[ strlen( temp ) ] = buffer;

				// Liberer
				NFREE( temp );
				break;

				// Etat inconnu
			default:
				// Liberer
			NFREE( extension );

				// Quitter
				return NULL;
		}

		// Incrementer le curseur
		curseur++;
	}

	// OK
	return extension;
}

/**
 * Supprimer un fichier
 *
 * @param fichier
 *		Le lien du fichier a supprimer
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_Operation_SupprimerFichier( const char *fichier )
{
#ifdef IS_WINDOWS
	return !remove( fichier );
#else // IS_WINDOWS
	return (NBOOL)!unlink( fichier );
#endif // !IS_WINDOWS
}

/**
 * Copier fichier (ecrase la destination)
 *
 * @param src
 *		Le fichier source
 * @param dst
 *		Le fichier destination
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Fichier_Operation_CopierFichier( const char *src,
	const char *dst )
{
	// Fichiers
	FILE *fichierSource,
		*fichierDestination;

	// Buffer
#define NTAILLE_BUFFER_COPIE        128
	char buffer[ NTAILLE_BUFFER_COPIE ];

	// Taille lue
	size_t tailleLue,
		tailleLueTotal = 0;

	// Taille source
	NS64 tailleSource;

	// Ouvrir source
	if( !( fichierSource = fopen( src,
		"rb" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return NFALSE;
	}

	// Calculer la taille
	tailleSource = NLib_Fichier_Operation_ObtenirTaille( fichierSource );

	// Ouvrir destination
	if( !( fichierDestination = fopen( dst,
		"wb+" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OVERRIDE );

		// Fermer fichier
		fclose( fichierSource );

		// Quitter
		return NFALSE;
	}

	// Copier
	while( tailleLueTotal < tailleSource )
	{
		// Lire
		tailleLue = fread( buffer,
			sizeof( char ),
			NTAILLE_BUFFER_COPIE,
			fichierSource );

		// Ecrire
		fwrite( buffer,
			sizeof( char ),
			tailleLue,
			fichierDestination );

		// Ajouter la taille lue
		tailleLueTotal += tailleLue;
	}
#undef NTAILLE_BUFFER_COPIE

	// Fermer les fichiers
	fclose( fichierSource );
	fclose( fichierDestination );

	// OK
	return NTRUE;
}

/**
 * Calculer hash md5
 *
 * @param fichier
 * 		Le fichier
 * @param hash
 * 		Le hash en sortie (16)
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Fichier_Operation_CalculerHashMD5( FILE *fichier,
	__OUTPUT unsigned char hash[ 16 ] )
{
#define TAILLE_BUFFER_FICHIER_MD5		128

	// Taille fichier
	NU64 tailleFichier;

	// MD5 state
	NMD5_CTX statusMD5;

	// Buffer
	char buffer[ TAILLE_BUFFER_FICHIER_MD5 + 1 ];

	// Taille a lire
	NU32 tailleALire;

	// Taille lue
	NU64 tailleLue = 0;

	// Obtenir taille fichier
	tailleFichier = NLib_Fichier_Operation_ObtenirTaille( fichier );

	// Initialiser hash
	NLib_Math_MD5_Init( &statusMD5 );

	// Hasher
	while( !NLib_Fichier_Operation_EstEOF2( fichier,
		tailleFichier ) )
	{
		// Calculer taille a lire
		tailleALire = (NU32)( tailleFichier - tailleLue >= TAILLE_BUFFER_FICHIER_MD5 ?
			TAILLE_BUFFER_FICHIER_MD5
			: ( tailleFichier - tailleLue ) );

		// Lire
		if( fread( buffer,
			sizeof( char ),
			tailleALire,
			fichier ) != tailleALire )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

			// Quitter
			return NFALSE;
		}

		// Update MD5
		NLib_Math_MD5_Update( &statusMD5,
			buffer,
			tailleALire );

		// Incrementer taille lue
		tailleLue += tailleALire;
	}

	// Finaliser hash
	NLib_Math_MD5_Final( hash,
		&statusMD5 );

#undef TAILLE_BUFFER_FICHIER_MD5

	// OK
	return NTRUE;
}

/**
 * Calculer hash md5
 *
 * @param lienFichier
 * 		Le chemin vers le fichier
 * @param hash
 * 		Le hash en sortie (16)
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Fichier_Operation_CalculerHashMD5_2( const char *lienFichier,
	__OUTPUT unsigned char hash[ 16 ] )
{
	// Fichier
	FILE *fichier;

	// Sortie
	__OUTPUT NBOOL resultat;

	// Ouvrir fichier
	if( !( fichier = fopen( lienFichier,
		"rb" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return NFALSE;
	}

	// Calculer hash
	resultat = NLib_Fichier_Operation_CalculerHashMD5( fichier,
		hash );

	// Fermer fichier
	fclose( fichier );

	// OK?
	return resultat;
}
