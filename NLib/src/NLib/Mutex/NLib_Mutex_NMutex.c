#include "../../../include/NLib/NLib.h"

/* Construire */
__ALLOC NMutex *NLib_Mutex_NMutex_Construire( void )
{
	// Sortie
	__OUTPUT NMutex *out;

	// Allouer
	if( !( out = calloc( 1,
		sizeof( NMutex ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire mutex
#ifdef IS_WINDOWS
	if( !( out->m_mutex = CreateMutex( NULL,
		NFALSE,
		NULL ) ) )
#else // IS_WINDOWS
    if( pthread_mutex_init( &out->m_mutex,
		NULL ) )
#endif // !IS_WINDOWS
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MUTEX );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}
	
	// OK
	return out;
}

/* Detruire */
void NLib_Mutex_NMutex_Detruire( NMutex **this )
{
	// Fermer
#ifdef IS_WINDOWS
	CloseHandle( (*this)->m_mutex );
#else // IS_WINDOWS
	pthread_mutex_destroy( &(*this)->m_mutex );
#endif // !IS_WINDOWS

	NFREE( *this );
}

/* Lock */
#ifdef NLIB_DEBUG_MUTEX
NBOOL NLib_Mutex_NMutex_LockDebug( NMutex *this,
	const char *fichier,
	NU32 ligne )
#else // NLIB_DEBUG_MUTEX
NBOOL NLib_Mutex_NMutex_Lock( NMutex *this )
#endif // !NLIB_DEBUG_MUTEX
{
#ifdef NLIB_DEBUG_MUTEX
	printf( "Lock %s, %d\n",
		fichier,
		ligne );
#endif // NLIB_DEBUG_MUTEX

	// Lock
#ifdef IS_WINDOWS
	if( WaitForSingleObject( this->m_mutex,
		INFINITE ) != WAIT_OBJECT_0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MUTEX );

		// Quitter
		return NFALSE;
	}
#else // IS_WINDOWS
	pthread_mutex_lock( &this->m_mutex );
#endif // !IS_WINDOWS

	// OK
	return NTRUE;
}

/* Unlock */
#ifdef NLIB_DEBUG_MUTEX
void NLib_Mutex_NMutex_UnlockDebug( NMutex *this,
	const char *fichier,
	NU32 ligne )
#else // NLIB_DEBUG_MUTEX
void NLib_Mutex_NMutex_Unlock( NMutex *this )
#endif // !NLIB_DEBUG_MUTEX
{
#ifdef NLIB_DEBUG_MUTEX
	printf( "Unlock %s, %d\n",
		fichier,
		ligne );
#endif // NLIB_DEBUG_MUTEX

#ifdef IS_WINDOWS
	ReleaseMutex( this->m_mutex );
#else // IS_WINDOWS
	pthread_mutex_unlock( &this->m_mutex );
#endif // !IS_WINDOWS
}

