#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// --------------------
// namespace NLib::Math
// --------------------

/* Absolue */
NS32 NLib_Math_Abs( NS32 x )
{
	return ( x < 0 ) ?
		-x
		: x;
}

/* Minimum */
NS32 NLib_Math_Minimum( NS32 x,
	NS32 y )
{
	return x >= y ?
		y
		: x;
}

NU32 NLib_Math_Minimum2( NU32 x,
	NU32 y )
{
	return x >= y ?
		y
		: x;
}

/* Maximum */
NS32 NLib_Math_Maximum( NS32 x,
	NS32 y )
{
	return x >= y ?
		x
		: y;
}

NU32 NLib_Math_Maximum2( NU32 x,
	NU32 y )
{
	return x >= y ?
		x
		: y;
}

/* Obtenir nombre digit */
NU32 NLib_Math_ObtenirNombreDigit( NU32 x )
{
	// Sortie
	__OUTPUT NU32 sortie = 0;

	// Calculer
	do sortie++; while( ( x /= 10 ) > 0 );

	// OK
	return sortie;
}

/**
 * Calculer puissance
 *
 * @param nombre
 * 		Le nombre
 * @param puissance
 * 		La puissance
 *
 * @return nombre^puissance
 */
NU32 NLib_Math_CalculerPuissance( NU32 nombre,
	NU32 puissance )
{
	// Sortie
	__OUTPUT NU32 sortie = 1;

	// Iterateur
	NU32 i = 0;

	// Calculer
	for( ; i < puissance; i++ )
		sortie *= nombre;

	// OK
	return sortie;
}

