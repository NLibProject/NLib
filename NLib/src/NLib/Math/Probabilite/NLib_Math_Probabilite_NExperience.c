#include "../../../../include/NLib/NLib.h"

// -------------------------------------------
// struct NLib::Math::Probabilite::NExperience
// -------------------------------------------

/* Construire (proba entre 0 et 100, avec total <= 100) */
__ALLOC NExperience *NLib_Math_Probabilite_NExperience_Construire( const NU32 *probabilite,
	NU32 nombreProbabilite )
{
	// Sortie
	__OUTPUT NExperience *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NExperience ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer probabilite
	if( !( out->m_probabilite = calloc( nombreProbabilite,
		sizeof( NU32 ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out->m_probabilite,
		probabilite,
		sizeof( NU32 ) * nombreProbabilite );

	// Enregistrer
	out->m_nombreEvenement = nombreProbabilite;

	// OK
	return out;
}

/* Detruire */
void NLib_Math_Probabilite_NExperience_Detruire( NExperience **this )
{
	// Liberer probabilites
	NFREE( (*this)->m_probabilite );

	// Liberer
	NFREE( *this );
}

/* Faire une experience (retourne l'identifiant de l'evenement) */
NU32 NLib_Math_Probabilite_NExperience_Lancer( const NExperience *this )
{
	// Iterateur
	NU32 i = 0;

	// Valeur courante
	NU32 valeurCourante = 0;

	// Valeur aleatoire
	NU32 alea;

	// Tirer une valeur
	alea = NLib_Temps_ObtenirNombreAleatoire( )%101;

	// Determiner l'evenement
	for( ; i < this->m_nombreEvenement; i++ )
	{
		// Ajouter valeur probabilite
		valeurCourante += this->m_probabilite[ i ];

		// Verifier
		if( alea <= valeurCourante )
			return i;
	}

	// Experience sans resultat compris dans les bornes
	return NERREUR;
}

