#include "../../../include/NLib/NLib.h"

/**
 *	Gestion des chaines de caracteres
 *
 *	@author SOARES Lucas
 */

// ----------------------
// namespace NLib::Chaine
// ----------------------

/**
 * Est a la fin du fichier?
 *
 * @param source
 * 		La chaine source
 * @param curseur
 * 		Le curseur dans la chaine
 *
 * @return si EOF
 */
NBOOL NLib_Chaine_EstEOF( const char *source,
	NU32 curseur )
{
	return (NBOOL)( curseur >= strlen( source ) );
}

/**
 * Est a la fin du fichier?
 *
 * @param curseur
 * 		Le curseur dans le fichier
 * @param taille
 * 		La taille du fichier
 *
 * @return si EOF
 */
NBOOL NLib_Chaine_EstEOF2( NU32 curseur,
	NU32 taille )
{
	return (NBOOL)( curseur >= taille );
}

/**
 * Est un nombre entier?
 *
 * @param chaine
 *		La chaine a analyser
 * @param base
 * 		La base consideree (10 ou 16?)
 *
 * @return si c'est un nombre entier
 */
NBOOL NLib_Chaine_EstUnNombreEntier( const char *chaine,
	NU32 base )
{
	// Iterateur
	NU32 i = 0;

	// Taille chaine
	size_t taille;

	// Calculer la taille
	if( ( taille = strlen( chaine ) ) <= 0 )
		// Ce n'est pas un nombre
		return NFALSE;

	// Verifier
	for( ; i < taille; i++ )
		switch( base )
		{
			case 10:
				if( !NLib_Caractere_EstUnChiffre( chaine[ i ] ) )
					return NFALSE;
				break;
			case 16:
				if( !NLib_Caractere_EstUnChiffreHexadecimal( chaine[ i ] )
					&& !NLib_Caractere_EstUnChiffre( chaine[ i ] ) )
					return NFALSE;
				break;

			default:
				// Notifier
				NOTIFIER_ERREUR( NERREUR_UNIMPLEMENTED );

				// Quit
				return NFALSE;
		}

	// OK
	return NTRUE;
}

/**
 * Est un nombre reel?
 *
 * @param chaine
 *		La chaine a analyser
 *
 * @return si c'est un nombre reel
 */
NBOOL NLib_Chaine_EstUnNombreReel( const char *chaine )
{
	// Iterateur
	NU32 i = 0;

	// Nombre de '.' trouve(s)
	NU32 nombrePoint = 0;

	// Taille chaine
	size_t taille;

	// Calculer la taille
	taille = strlen( chaine );

	// Verifier
	for( ; i < taille; i++ )
		// C'est un chiffre?
		if( !NLib_Caractere_EstUnChiffre( chaine[ i ] ) )
		{
			// C'est un marqueur de nombre reel
			if( chaine[ i ] == '.' )
			{
				// Il a trop de points?
				if( ++nombrePoint >= 2 )
					// Ce n'est pas un nombre reel
					return NFALSE;
			}
			else
				return NFALSE;
		}

	// OK
	return NTRUE;
}

/**
 * Placer le curseur au caractere
 *
 * @param source
 * 		La chaine a considerer
 * @param caractere
 * 		Le caractere ou se placer
 * @param positionActuelle
 * 		La position actuelle du curseur
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Chaine_PlacerAuCaractere( const char *source,
	char caractere,
	NU32 *positionActuelle )
{
	// Caractere lu
	char caractereLu;

	// Chercher le caractere
	do
	{
		// Lire
		caractereLu = source[ *positionActuelle ];

		// Avancer le curseur
		(*positionActuelle)++;
	} while( !NLib_Chaine_EstEOF( source,
		*positionActuelle )
		&& caractereLu != caractere );

	// OK
	return (NBOOL)( caractere == caractereLu );
}

/**
 * Se placer a la fin de la chaine
 *
 * @param source
 * 		La chaine a considerer
 * @param chaineRecherche
 * 		La chaine a laquelle se placer
 * @param positionActuelle
 * 		La position actuelle dans la chaine source
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Chaine_PlacerALaChaine( const char *source,
	const char *chaineRecherche,
	NU32 *positionActuelle )
{
	// Curseur de recherche
	NU32 curseurRecherche = 0;

	// Longueur de la chaine
	size_t longueur;

	// Obtenir la longueur
	if( ( longueur = strlen( chaineRecherche ) ) <= 0 )
		// Il n'y a rien a faire
		return NTRUE;

	// Chercher la chaine
	while( curseurRecherche < longueur
		&& !NLib_Chaine_EstEOF( source,
			*positionActuelle ) )
		// Caractere en cours trouve
		if( NLib_Chaine_ObtenirProchainCaractere2( source,
			positionActuelle,
			NFALSE,
			NTRUE ) == chaineRecherche[ curseurRecherche ] )
			curseurRecherche++;
		else
			// Impossible de trouver le caractere
			curseurRecherche = 0;

	// OK?
	return (NBOOL)( curseurRecherche >= longueur );
}

/**
 * Lire une chaine entre deux separateurs
 *
 * @param source
 * 		La chaine source
 * @param caractereDelimiteurGauche
 * 		Le delimitateur gauche
 * @param caractereDelimiteurDroit
 * 		Le delimitateur droit
 * @param nombreCaractereArret
 * 		Le nombre de caractere dans %caractereDelimiteurDroit%
 * @param positionActuelle
 * 		La position dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position a la fin de la lecture
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Chaine_LireEntre( const char *source,
	char caractereDelimiteurGauche,
	const char *caractereDelimiteurDroit,
	NU32 nombreCaractereArret,
	NU32 *positionActuelle,
	NBOOL estConserverPosition )
{
	// Sortie
	__OUTPUT char *out = NULL;

	// Doit arreter?
	NBOOL estDoitArreter;

	// Iterateur
	NU32 i;

	// Position initiale
	NU32 positionInitiale = *positionActuelle;

	// Caractere a ajouter
	char caractereAAjouter[ 2 ] = { 0, 0 };

	// Caractere d'echappement?
	if( caractereDelimiteurGauche != '\0'
		// Chercher le premier caractere
		&& !NLib_Chaine_PlacerAuCaractere( source,
			caractereDelimiteurGauche,
			positionActuelle ) )
		{
			// Restaurer position
			if( estConserverPosition )
				*positionActuelle = positionInitiale;

			// Quitter
			return NULL;
		}

	// Lire
	do
	{
		// Verifier caractere d'arret
		estDoitArreter = NFALSE;
		for( i = 0; i < nombreCaractereArret; i++ )
			if( source[ *positionActuelle ] == caractereDelimiteurDroit[ i ] )
				estDoitArreter = NTRUE;

		// Si le caractere n'est pas le caractere de fin
		if( !estDoitArreter )
		{
			// Definir caractere a ajouter
			caractereAAjouter[ 0 ] = source[ *positionActuelle ];

			// Ajouter
			NLib_Memoire_AjouterData( &out,
				(NU32)( out == NULL ?
					0
					: strlen( out ) ),
				caractereAAjouter,
				2 );
		}

		// Incrementer curseur
		if( !NLib_Chaine_EstEOF( source,
			*positionActuelle ) )
			(*positionActuelle)++;
	} while( !NLib_Chaine_EstEOF( source,
			*positionActuelle )
		&& !estDoitArreter );

	// Restaurer la position initiale si demande
	if( estConserverPosition )
		*positionActuelle = positionInitiale;

	// OK
	return out;
}

/**
 * Lire entre deux caractere
 *
 * @param source
 * 		La chaine source
 * @param caractere
 * 		Le delimitateur gauche/droit
 * @param positionActuelle
 * 		La position actuelle
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Chaine_LireEntre2( const char *source,
	char caractere,
	NU32 *positionActuelle,
	NBOOL estConserverPosition )
{
	return NLib_Chaine_LireEntre( source,
		caractere,
		&caractere,
		1,
		positionActuelle,
		estConserverPosition );
}

/**
 * Lire a partir de la position actuelle jusqu'au caractere
 *
 * @param source
 * 		La chaine source
 * @param caractere
 * 		Le caractere ou s'arreter
 * @param positionActuelle
 * 		La position actuelle dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Chaine_LireJusqua( const char *source,
	char caractere,
	NU32 *positionActuelle,
	NBOOL estConserverPosition )
{
	return NLib_Chaine_LireEntre( source,
		'\0',
		&caractere,
		1,
		positionActuelle,
		estConserverPosition );
}

/**
 * Lire a partir de la position actuelle jusqu'aux caracteres presents dans c
 *
 * @param source
 * 		La chaine source
 * @param listeCaractere
 * 		Le tableau de caracteres ou s'arreter
 * @param nombreCaractereArret
 * 		Le nombre de caractere dans c
 * @param positionActuelle
 * 		La position actuelle dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Chaine_LireJusqua2( const char *source,
	const char *listeCaractere,
	NU32 nombreCaractereArret,
	NU32 *positionActuelle,
	NBOOL estConserverPosition )
{
	return NLib_Chaine_LireEntre( source,
		'\0',
		listeCaractere,
		nombreCaractereArret,
		positionActuelle,
		estConserverPosition );
}

/**
 * Placer curseur avant prochaine lettre (en ignorant tous les separateurs espace/\t/\n...
 *
 * @param source
 * 		La chaine source
 * @param curseur
 * 		Le curseur
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Chaine_PlacerCurseurProchainCaractereNonSeparateur( const char *source,
	NU32 *curseur )
{
	// Chercher le premier caractere non separateur
	while( source[ *curseur ] != '\0'
		&& NLib_Caractere_EstUnSeparateur( source[ *curseur ] ) )
		// Increment
		(*curseur)++;

	// OK?
	return (NBOOL)( source[ *curseur ] != '\0' );
}

/**
 * Obtenir le prochain caractere
 *
 * @param source
 * 		La chaine source
 * @param positionActuelle
 * 		La position actuelle dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture?
 * @param estInclureSeparateur
 * 		Doit-on considerer /\t/\n/' '/...
 *
 * @return le prochain caractere
 */
char NLib_Chaine_ObtenirProchainCaractere2( const char *source,
	NU32 *positionActuelle,
	NBOOL estConserverPosition,
	NBOOL estInclureSeparateur )
{
	// Caractere lu
	__OUTPUT char caractereLu;

	// On inclus les separateurs?
	if( estInclureSeparateur )
	{
		// Lire caractere
		caractereLu = source[ *positionActuelle ];

		// Restaurer la position si necessaire
		if( !estConserverPosition
			&& caractereLu != '\0' )
				// Incrementer curseur
				(*positionActuelle)++;

		// OK
		return caractereLu;
	}

	// Non inclusion des separateurs
	return NLib_Chaine_ObtenirProchainCaractere( source,
		positionActuelle,
		estConserverPosition );
}

/**
 * Obtenir le prochain caractere
 *
 * @param source
 * 		La chaine source
 * @param positionActuelle
 * 		La position actuelle dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position apres lecture?
 *
 * @return le prochain caractere
 */
char NLib_Chaine_ObtenirProchainCaractere( const char *source,
	NU32 *positionActuelle,
	NBOOL estConserverPosition )
{
	// Caractere lu
	__OUTPUT char caractereLu;

	// Position initiale
	NU32 positionInitiale = *positionActuelle;

	// Taille de la chaine
	size_t tailleChaine;

	// Continuer?
	NBOOL estContinuer = NTRUE;

	// Calculer taille chaine
	tailleChaine = strlen( source );

	// Chercher des caracteres non separateurs
	do
	{
		// Lire caractere
		caractereLu = source[ *positionActuelle ];

		// Analyser
		switch( caractereLu )
		{
			case ' ':
			case '\n':
			case '\t':
			case '\r':
			case '\0':
				break;

			default:
				estContinuer = NFALSE;
				break;
		}

		// Incrementer curseur
		(*positionActuelle)++;
	} while( estContinuer
		&& *positionActuelle < tailleChaine );

	// Restaurer la position si necessaire
	if( estConserverPosition )
		*positionActuelle = positionInitiale;
	else
		// Verifier que le curseur ne soit pas incorrect
		if( *positionActuelle >= tailleChaine )
			// Restaurer une position correcte
			*positionActuelle = (NU32)( tailleChaine );

	// OK
	return caractereLu;
}

/**
 * Lire un nombre (privee)
 *
 * @param chaine
 * 		La chaine
 * @param curseur
 * 		Le curseur dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position apres lecture
 * @param estSigne
 * 		Le nombre a lire peut etre signe?
 *
 * @return le nombre lu
 */
NU32 NLib_Chaine_LireNombreInterne( const char *chaine,
	NU32 *curseur,
	NBOOL estConserverPosition,
	NBOOL estSigne )
{
	// Position initiale
	NU32 positionInitiale;

	// Taille du fichier
	size_t taille;

	// Caractere lu
	char caractere;

	// Curseur
	NU32 curseurBuffer = 0;

#define TAILLE_BUFFER		4096

	// Buffer
	char buffer[ TAILLE_BUFFER ] = { 0, };

	// Obtenir la taille
	taille = strlen( chaine );

	// Enregistrer position initiale
	positionInitiale = *curseur;

	// Chercher chiffre ou '-' si signe
	do
	{
		// Lire
		caractere = chaine[ *curseur ];

		// Avancer
		(*curseur)++;
	} while( ( !NLib_Caractere_EstUnChiffre( caractere )
			&& ( estSigne ? (NBOOL)( caractere != '-' ) : NTRUE ) )
		&& !NLib_Chaine_EstEOF2( *curseur,
			(NU32)taille ) );

	// Quitter en traduisant
	if( NLib_Chaine_EstEOF2( *curseur,
		(NU32)taille ) )
	{
		// Restaurer si necessaire
		if( estConserverPosition )
			*curseur = positionInitiale;

		// Verifier si il s'agit d'un caractere chiffre
		if( NLib_Caractere_EstUnChiffre( caractere ) )
			// Il s'agit d'un chiffre
			return NLib_Caractere_ConvertirDecimal( caractere );

		// Ce n'est pas un chiffre
		return NERREUR;
	}

	// Reculer d'une case
	if( *curseur > 0 )
		(*curseur)--;

	// Vider le buffer
	memset( buffer,
		0,
		TAILLE_BUFFER );

	// Lire nombre
	do
	{
		// Lire
		caractere = chaine[ *curseur ];

		// Avancer
		(*curseur)++;

		// Verifier
		if( NLib_Caractere_EstUnChiffre( caractere )
			|| ( estSigne ?
				(NBOOL)( caractere == '-'
					&& !curseurBuffer )
				: NFALSE ) )
			// Ajouter au buffer
			buffer[ curseurBuffer++ ] = caractere;
	} while( ( NLib_Caractere_EstUnChiffre( caractere )
			|| ( estSigne ? (NBOOL)( caractere == '-' && curseurBuffer == 1 ) : NFALSE ) )
		&& strlen( buffer ) < TAILLE_BUFFER
		&& !NLib_Chaine_EstEOF2( *curseur,
		(NU32)taille ) );

#undef TAILLE_BUFFER

	// Restaurer si necessaire
	if( estConserverPosition )
		*curseur = positionInitiale;

	// OK
	return (NU32)strtol( buffer,
		NULL,
		10 );
}

/**
 * Lire un nombre non signe
 *
 * @param chaine
 * 		La chaine source
 * @param curseur
 * 		Le curseur dans la chaine
 * @param estConserverPosition
 * 		Restaurer position apres lecture?
 *
 * @return le nombre lu
 */
NU32 NLib_Chaine_LireNombreNonSigne( const char *chaine,
	NU32 *curseur,
	NBOOL estConserverPosition )
{
	return NLib_Chaine_LireNombreInterne( chaine,
		curseur,
		estConserverPosition,
		NFALSE );
}

/**
 * Lire un nombre signe
 *
 * @param chaine
 * 		La chaine source
 * @param curseur
 * 		Le curseur dans la chaine
 * @param estConserverPosition
 * 		Restaurer position apres lecture?
 *
 * @return le nombre lu
 */
NU32 NLib_Chaine_LireNombreSigne( const char *chaine,
	NU32 *curseur,
	NBOOL estConserverPosition )
{
	return NLib_Chaine_LireNombreInterne( chaine,
		curseur,
		estConserverPosition,
		NTRUE );
}

/**
 * Est ce que la chaine est vide? (Que des separateurs/taille nulle?)
 *
 * @param source
 * 		La chaine source
 *
 * @return si la chaine est vide
 */
NBOOL NLib_Chaine_EstVide( const char *source )
{
	// Verifier taille
	if( !strlen( source ) )
		return NTRUE;

	// Verifier contenu
	while( NLib_Caractere_EstUnSeparateur( *source )
		&& *source != '\0' )
		source++;

	// La chaine est vide
	return (NBOOL)( *source == '\0' );
}

/**
 * Comparaison chaine
 *
 * @param chaine1
 *		La premiere chaine
 * @param chaine2
 *		La deuxieme chaine
 * @param estCasseAConsiderer
 *		Considere-t-on la casse?
 * @param taille
 *		La taille a considerer (si nulle, les tailles sont comparees)
 *
 * @return si les chaines sont identiques
 */
NBOOL NLib_Chaine_Comparer( const char *chaine1,
	const char *chaine2,
	NBOOL estCasseAConsiderer,
	NU32 taille )
{
	// Tailles des chaines
	NU32 tailleS1,
		tailleS2;

	// Taille a considerer
	NU32 tailleAConsiderer;

	// Iterateur
	NU32 i;

	// Calculer les tailles reelles
	tailleS1 = (NU32)strlen( chaine1 );
	tailleS2 = (NU32)strlen( chaine2 );

	// Taille a considerer nulle?
	if( taille <= 0 )
	{
		// Verifier que les tailles soient egales
		if( tailleS1 != tailleS2 )
			// Chaine differente
			return NFALSE;

		// Enregistrer taille
		tailleAConsiderer = tailleS1;
	}
	// Verifier que les chaines aient bien une taille superieure ou egale a celle choisie
	else
	{
		// Taille inferieure?
		if( tailleS1 < taille
			|| tailleS2 < taille )
			// Quitter
			return NFALSE;

		// Enregistrer taille
		tailleAConsiderer = taille;
	}


	// Casse a prendre en compte?
	if( estCasseAConsiderer )
	{
		// Parcourir
		for( i = 0; i < tailleAConsiderer; i++ )
			// Caractere different
			if( chaine1[ i ] != chaine2[ i ] )
				// Chaine differente
				return NFALSE;
	}
	// Casse ignoree
	else
		// Parcourir
		for( i = 0; i < tailleAConsiderer; i++ )
			// Comparer les minuscules
			if( NLib_Caractere_ConvertirMinuscule( chaine1[ i ] ) != NLib_Caractere_ConvertirMinuscule( chaine2[ i ] ) )
				// Chaines differentes
				return NFALSE;

	// Chaines identiques
	return NTRUE;
}

/**
 * La chaine contient le caractere?
 *
 * @param chaine
 *		La chaine
 * @param caractere
 *		Le caractere
 *
 * @return si la chaine contient le caractere
 */
NBOOL NLib_Chaine_EstChaineContient( const char *chaine,
	char caractere )
{
	// Iterateur
	NU32 i = 0;

	// Taille chaine
	NU32 taille;

	// Calculer taille
	taille = (NU32)strlen( chaine );

	// Chercher
	for( ; i < taille; i++ )
		if( chaine[ i ] == caractere )
			return NTRUE;

	// Introuvable
	return NFALSE;
}

/**
 * La chaine contient la chaine?
 *
 * @param chaine
 * 		La chaine dans laquelle chercher
 * @param motif
 * 		Le motif a rechercher dans la chaine
 * @param estConsidereCasse
 * 		Considere-t-on la casse de la chaine lors de la recherche?
 *
 * @return si la chaine contient le motif
 */
NBOOL NLib_Chaine_EstChaineContient2( const char *chaine,
	const char *motif,
	NBOOL estConsidereCasse )
{
	// Copie chaines
	char *copieChaine,
		*copieMotif;

	// Sortie
	__OUTPUT NBOOL resultat;

	// On considere la casse
	if( estConsidereCasse )
		// Verifier si le motif apparait tel quel
		resultat = (NBOOL)( strstr( chaine,
			motif ) != NULL );
	// On ne considere pas la casse
	else
	{
		// Dupliquer
		if( !( copieChaine = NLib_Chaine_Dupliquer( chaine ) )
			|| !( copieMotif = NLib_Chaine_Dupliquer( motif ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			NFREE( copieChaine );

			// Quitter
			return NFALSE;
		}

		// Mettre en minuscule
		NLib_Chaine_MettreEnMinuscule( copieChaine );
		NLib_Chaine_MettreEnMinuscule( copieMotif );

		// Comparer
		resultat = (NBOOL)( strstr( copieChaine,
			copieMotif ) != NULL );

		// Liberer
		NFREE( copieChaine );
		NFREE( copieMotif );
	}

	// OK
	return resultat;
}

/**
 * Est chaine continent lettres?
 *
 * @param chaine
 * 		La chaine a analyser
 *
 * @return si la chaine contient des lettres
 */
NBOOL NLib_Chaine_EstChaineContientLettre( const char *chaine )
{
	// Iterateur
	NU32 i = 0;

	// Taille chaine
	NU32 tailleChaine;

	// Calculer taille chaine
	tailleChaine = (NU32)strlen( chaine );

	// Verifier
	for( ; i < tailleChaine; i++ )
		if( NLib_Caractere_EstUneLettre( chaine[ i ] ) )
			// Lettre presente
			return NTRUE;

	// Aucune lettre
	return NFALSE;
}

/**
 * Remplacer un caractere par un autre
 *
 * @param chaine
 *		La chaine
 * @param source
 *		Source
 * @param destination
 *		Destination
 */
void NLib_Chaine_Remplacer( char *chaine,
	char source,
	char destination )
{
	// Iterateur
	NU32 i = 0;

	// Taille
	size_t taille;

	// Calculer taille
	taille = strlen( chaine );

	// Remplacer
	for( ; i < taille; i++ )
		// Verifier
		if( chaine[ i ] == source )
			chaine[ i ] = destination;
}

#ifndef IS_WINDOWS
/**
 * Comparer une chaine avec une regex (Et retourne si l'expression correspond)
 *
 * @param expression
 *		L'expression a analyser
 * @param regex
 * 		La regex a laquelle comparer l'expression
 *
 * @return si l'expression correspond a la regex
 */
NBOOL NLib_Chaine_EstCorrespondRegex( const char *expression,
	const char *regex )
{
	// Expression reguliere
	regex_t traductionRegex;

	// Sortie
	__OUTPUT NBOOL sortie;

	// Creer la traduction de l'expression
	if( regcomp( &traductionRegex,
		regex,
		(NS32)( REG_ICASE | REG_EXTENDED | REG_NOSUB ) ) != 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_REGEX );

		// Quitter
		return NFALSE;
	}

	// Comparer
	sortie = (NBOOL)regexec( &traductionRegex,
		expression,
		0,
		NULL,
		0 );

	// Liberer la traduction
	regfree( &traductionRegex );

	// OK?
	return (NBOOL)!sortie;
}
#endif // !IS_WINDOWS

/**
 * Compter le nombre d'occurences d'un caractere
 *
 * @param chaine
 *		La chaine
 * @param caractere
 *		Le caractere
 *
 * @return le nombre d'occurence(s) de c dans chaine
 */
NU32 NLib_Chaine_CompterNombreOccurence( const char *chaine,
	char caractere )
{
	// Iterateur
	NU32 i = 0;

	// Taille chaine
	size_t taille;

	// Sortie
	__OUTPUT NU32 nombreOccurence = 0;

	// Calculer la taille
	taille = strlen( chaine );

	// Compter le nombre d'occurences
	for( ; i < taille; i++ )
		// Occurence?
		if( chaine[ i ] == caractere )
			// Incrementer
			nombreOccurence++;

	// OK
	return nombreOccurence;
}

/**
 * Compter le nombre de caracteres au debut de la chaine
 *
 * @param chaine
 * 		La chaine
 * @param caractere
 *		Le caractere a compter
 *
 * @return le nombre de caracteres
 */
NU32 NLib_Chaine_CompterNombreCaractereDebut( const char *chaine,
	char caractere )
{
	// Compteur
	NU32 compteur = 0;

	// Compter
	while( chaine[ compteur ] == caractere )
		compteur++;

	// OK
	return compteur;
}

/**
 * Dupliquer une chaine
 *
 * @param source
 * 		La chaine a dupliquer
 *
 * @return la chaine dupliquee
 */
__ALLOC char *NLib_Chaine_Dupliquer( const char *source )
{
	// Sortie
	__OUTPUT char *out;

	// Taille
	size_t taille;

	// Allouer la memoire
	if( !( out = calloc( ( taille = strlen( source ) ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Il y a quelque chose a copier?
	if( taille > 0 )
		// Copier
		memcpy( out,
			source,
			taille );

	// OK
	return out;
}

/**
 * Dupliquer une chaine de facon securisee (en fixant la taille utile)
 *
 * @param source
 * 		La chaine a dupliquer
 * @param taille
 * 		La taille de la chaine a dupliquer (sans \0)
 *
 * @return la copie de la chaine
 */
__ALLOC char *NLib_Chaine_DupliquerSecurite( const char *source,
	NU32 taille )
{
	// Sortie
	__OUTPUT char *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		taille + 1 ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out,
		source,
		taille );

	// OK
	return out;
}

/**
 * Lire une chaine depuis stdin
 *
 * @param tailleMaximale
 * 		La taille maximale a lire (0 = s'arrete a \n)
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Chaine_LireStdin( NU32 tailleMaximale )
{
	// Sortie
	__OUTPUT char *out = NULL;

	// Curseur
	NU32 curseur = 0;

	// Caractere lu
	char caractere;

	// Lire
	do
	{
		// Lire caractere
		if( ( caractere = (char)getchar( ) ) == '\n' )
			break;

		// Ajouter a la chaine
		if( !NLib_Memoire_AjouterData( &out,
			curseur,
			&caractere,
			sizeof( char ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Incrementer curseur
		curseur++;
	} while( tailleMaximale > 0 ?
		 (NBOOL)( curseur < tailleMaximale )
		 : NTRUE );

	// OK
	return out;
}

/**
 * Lire une chaine depuis stdin dans un buffer
 *
 * @param buffer
 * 		Le buffer
 * @param tailleBuffer
 * 		La taille du buffer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Chaine_LireStdin2( char *buffer,
	NU32 tailleBuffer )
{
	// Curseur
	NU32 curseur = 0;

	// Caractere lu
	char caractere;

	// Vider buffer
	memset( buffer,
		0,
		tailleBuffer );

	// Lire
	while( curseur < tailleBuffer
		&& ( caractere = (char)getchar( ) ) != '\n' )
		// Ajouter
		buffer[ curseur++ ] = caractere;

	// OK
	return NTRUE;
}

/**
 * Supprimer caractere
 *
 * @param source
 * 		La chaine
 * @param caractere
 * 		Le caractere a supprimer
 *
 * @return le nombre de caractere(s) supprime(s)
 */
NU32 NLib_Chaine_SupprimerCaractere( char *source,
	char caractere )
{
	// Iterateurs
	NU32 i = 0,
		j;

	// Taille chaine
	size_t taille;

	// Nombre occurences
	NU32 nombreOccurence = 0;

	// Calculer taille
	taille = strlen( source );

	// Supprimer
	for( ; i < taille; i++ )
		// Caractere egal?
		if( source[ i ] == caractere )
		{
			// Incrementer le nombre d'occurences
			nombreOccurence++;

			// Remplacer
			for( j = i; j < taille - 1; j++ )
				// Decaler
				source[ j ] = source[ j + 1 ];
		}

	// Fermer nouvelle chaine
	source[ taille - nombreOccurence ] = '\0';

	// OK
	return nombreOccurence;
}

/**
 * Supprimer caractere au debut
 *
 * @param source
 * 		La chaine
 * @param caractere
 * 		Le caractere a supprimer
 *
 * @return le nombre de caractere initiaux supprimes
 */
NU32 NLib_Chaine_SupprimerCaractereDebut( char *source,
	char caractere )
{
	// Iterateur
	NU32 i;

	// Taille
	NU32 taille;

	// Taille initiale
	NU32 tailleInitiale;

	// Calculer la taille
	tailleInitiale = taille = (NU32)strlen( source );

	// Chercher
	while( source[ 0 ] == caractere
		&& taille > 0 )
	{
		// Decaler
		for( i = 0; i < taille - 1; i++ )
			source[ i ] = source[ i + 1 ];

		// Incrementer
		taille--;
	}

	// Fermer
	source[ taille ] = '\0';

	// OK
	return tailleInitiale - taille;
}

/**
 * Mettre la chaine en minuscule
 *
 * @param source
 * 		La chaine
 */
void NLib_Chaine_MettreEnMinuscule( char *source )
{
	// Iterateur
	NU32 i = 0;

	// Taille
	NU32 taille;

	// Obtenir taille chaine
	taille = (NU32)strlen( source );

	// Remplacer
	for( ; i < taille; i++ )
		source[ i ] = NLib_Caractere_ConvertirMinuscule( source[ i ] );
}

/**
 * Mettre la chaine en majuscule
 *
 * @param source
 * 		La chaine
 */
void NLib_Chaine_MettreEnMajuscule( char *source )
{
	// Iterateur
	NU32 i = 0;

	// Taille
	NU32 taille;

	// Obtenir taille chaine
	taille = (NU32)strlen( source );

	// Remplacer
	for( ; i < taille; i++ )
		source[ i ] = NLib_Caractere_ConvertirMajuscule( source[ i ] );
}

/**
 * Convertir en base64
 *
 * @param texte
 * 		Le texte
 * @param taille
 * 		La taille du texte
 *
 * @return le texte en base64
 */
__ALLOC char *NLib_Chaine_ConvertirBase64( const char *texte,
	NU32 taille )
{
	// Conversion classique
	return NLib_Chaine_ConvertirBase64_2( texte,
		taille,
		NFALSE );
}

/**
 * Convertir en base64
 *
 * @param texte
 * 		Le texte
 * @param taille
 * 		La taille du texte
 * @param estEncoderPourURL
 * 		On veut encoder pour mettre dans une URL? (Et donc remplacer + par - et / par _)
 *
 * @return le texte en base64
 */
__ALLOC char *NLib_Chaine_ConvertirBase64_2( const char *texte,
	NU32 taille,
	NBOOL estEncoderPourURL )
{
	// Curseur sortie
	NU32 curseurSortie = 0;

	// Sortie
	__OUTPUT char *sortie;

	// Valeur
	NU32 valeur = 0;

#define NOMBRE_CARACTERE_A_LIRE		3

	// Taille sortie
	NU32 tailleSortie;

	// Nombre de caracteres lus
	NU32 nombreCaractereLu;

	// Iterateurs
	NU32 i,
		j;

	// Calculer la taille de la sortie
	if( !taille
		|| ( tailleSortie = ( (NU32)( 4.0 * ( (double)taille / 3.0 ) ) ) + 3 ) <= 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la sortie
	if( !( sortie = calloc( tailleSortie + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Parcourir
	for( i = 0; i < taille; i += NOMBRE_CARACTERE_A_LIRE )
	{
		// Lire caracteres
		valeur = 0;
		nombreCaractereLu = 0;
		for( j = 0; j < NOMBRE_CARACTERE_A_LIRE; j++ )
			if( i + j < taille )
			{
				// Lire
				valeur |= ( ( ( (NU8)texte[ i + j ] ) << ( ( NOMBRE_CARACTERE_A_LIRE - j - 1 ) * 8 ) ) );

				// Incrementer nombre caracteres lus
				nombreCaractereLu++;
			}


		// Ajouter
		for( j = 0; j < NOMBRE_CARACTERE_A_LIRE + 1; j++ )
			if( curseurSortie < tailleSortie )
			{
				if( j < nombreCaractereLu + 1 )
					sortie[ curseurSortie++ ] = NLib_Caractere_ConvertirValeurVersBase64( (NU8)( ( valeur >> ( 6 * ( NOMBRE_CARACTERE_A_LIRE - j ) ) ) & 0x0000003F ),
						estEncoderPourURL );
				else
					sortie[ curseurSortie++ ] = '=';
			}
	}

#undef NOMBRE_CARACTERE_A_LIRE

	// OK
	return sortie;
}

/**
 * Decoder base64
 *
 * @param texte
 * 		Le message en base64
 * @param taille
 * 		La taille du message
 *
 * @return le message decode
 */
__ALLOC char *NLib_Chaine_DecoderBase64( const char *texte,
	NU32 taille )
{
	// Taille sortie
	NU32 tailleSortie;

	// Sortie
	__OUTPUT char *sortie;

#define NOMBRE_CARACTERE_A_LIRE		4

	// Iterateurs
	NU32 i,
		j;

	// Valeur
	NU32 valeur;

	// Curseur sortie
	NU32 curseurSortie = 0;

	// Calculer la taille de la sortie
	if( ( tailleSortie = (NU32)( 3.0 * ( (double)taille / 4.0 ) ) + 6 ) <= 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( sortie = calloc( tailleSortie + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Decoder
	for( i = 0; i < taille; i += NOMBRE_CARACTERE_A_LIRE )
	{
		// Lire
		valeur = 0;
		for( j = 0; j < NOMBRE_CARACTERE_A_LIRE; j++ )
			if( i + j < taille )
				valeur |= ( ( ( NLib_Caractere_ConvertirValeurDepuisBase64( texte[ i + j ] ) ) << ( ( NOMBRE_CARACTERE_A_LIRE - j - 1 ) * 6 ) ) );

		// Ajouter
		for( j = 1; (NS32)j < NOMBRE_CARACTERE_A_LIRE; j++ )
			sortie[ curseurSortie++ ] = (char)( ( valeur >> ( 8 * ( ( NOMBRE_CARACTERE_A_LIRE - 1 ) - j ) ) ) & 0x000000FF );
	}

#undef NOMBRE_CARACTERE_A_LIRE

	// OK
	return sortie;
}

/**
 * Lire ligne
 *
 * @param texte
 * 		Le texte
 * @param curseur
 * 		Le curseur dans le texte
 * @param estConserverPosition
 * 		Doit-on conserver la position?
 *
 * @return la ligne lue
 */
__ALLOC char *NLib_Chaine_LireLigne( const char *texte,
	NU32 *curseur,
	NBOOL estConserverPosition )
{
	// Sortie
	__OUTPUT char *ligne;

	// Lire
	if( !( ligne = NLib_Chaine_LireJusqua( texte,
		'\n',
		curseur,
		estConserverPosition ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_COPY );

		// Quitter
		return NULL;
	}

	// Eliminer '\r'
	NLib_Chaine_SupprimerCaractere( ligne,
		'\r' );

	// OK
	return ligne;
}

/**
 * Lire prochaine ligne sans commentaire
 *
 * @param texte
 * 		Le texte
 * @param curseur
 * 		Le curseur dans le texte
 * @param estConserverPosition
 * 		Doit-on conserver la position?
 * @param caractereCommentaire
 * 		Le caractere utilise pour signaler un commentaire
 *
 * @return la ligne lue
 */
__ALLOC char *NLib_Chaine_LireProchaineLigneSansCommentaire( const char *texte,
	NU32 *curseur,
	NBOOL estConserverPosition,
	char caractereCommentaire )
{
	// Sortie
	__OUTPUT char *sortie;

	// Curseur
	NU32 curseurCopie = *curseur;

	// Lire
	while( !NLib_Chaine_EstEOF( texte,
			*curseur )
		&& ( sortie = NLib_Chaine_LireLigne( texte,
			&curseurCopie,
			NFALSE ) ) != NULL )
	{
		// Chaine correcte?
		if( strlen( sortie ) > 0
			&& sortie[ 0 ] != caractereCommentaire )
		{
			// On veut sauvegarder la position?
			if( !estConserverPosition )
				// Enregistrer
				*curseur = curseurCopie;

			// OK
			return sortie;
		}

		// Liberer
		NFREE( sortie );
	}

	// On veut sauvegarder la position?
	if( !estConserverPosition )
		// Enregistrer
		*curseur = curseurCopie;

	// Plus rien
	return NULL;
}

/**
 * Decouper suivant un caractere (c1.c2.c3 avec . donne [0]="c1", [1]="c2", [2]="c3")
 *
 * @param texte
 * 		Le texte a decouper
 * @param caractere
 * 		Le caractere suivant lequel decouper
 *
 * @return la liste des mots
 */
__ALLOC NListe *NLib_Chaine_Decouper( const char *texte,
	char caractere )
{
	// Sortie
	__OUTPUT NListe *sortie;

	// Mot
	char *mot;

	// Curseur
	NU32 curseur = 0;

	// Construire liste
	if( !( sortie = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NLib_Memoire_Liberer ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Lire
	while( ( mot = NLib_Chaine_LireJusqua( texte,
		caractere,
		&curseur,
		NFALSE ) ) != NULL )
	{
		// Verifier taille
		if( strlen( mot ) <= 0 )
		{
			// Liberer
			NFREE( mot );

			// Quitter
			break;
		}

		// Ajouter mot
		if( !NLib_Memoire_NListe_Ajouter( sortie,
			mot ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_COPY );

			// Detruire
			NLib_Memoire_NListe_Detruire( &sortie );

			// Quitter
			return NULL;
		}
	}

	// OK
	return sortie;
}

/**
 * La chaine commence par ca?
 *
 * @param texte
 * 		Le texte
 * @param debut
 * 		Le texte par lequel doit commencer la chaine
 *
 * @return si le texte commence par %debut%
 */
NBOOL NLib_Chaine_EstChaineCommencePar( const char *texte,
	const char *debut )
{
	// Iterateur
	NU32 i = 0;

	// Taille debut
	NU32 tailleDebut;

	// Verifier
	if( strlen( texte ) < ( tailleDebut = (NU32)strlen( debut ) ) )
		// Trop court
		return NFALSE;

	// Verifier
	for( ; i < tailleDebut; i++ )
		// Verifier
		if( debut[ i ] != texte[ i ] )
			// Lettre differente
			return NFALSE;

	// OK
	return NTRUE;
}

