#include "../../../include/NLib/NLib.h"

// ----------------------------
// struct NLib::Thread::NThread
// ----------------------------

/**
 * Arreter un thread (privee)
 *
 * @param this
 *		Cette instance
 */
void NLib_Thread_NThread_ArreterInterne( NThread *this )
{
	// Si on possede une condition d'arret
	if( this->m_estContinuer != NULL )
		// On arrete l'execution
		*this->m_estContinuer = NFALSE;
	// Forcer l'arret
	else
#ifdef IS_WINDOWS
		TerminateThread( this->m_handle,
			EXIT_SUCCESS );
#else // IS_WINDOWS
		pthread_cancel( this->m_handle );
#endif // !IS_WINDOWS

	// Attendre la fin du thread
#ifdef IS_WINDOWS
	while( WaitForSingleObject( this->m_handle,
		INFINITE ) )
		NLib_Temps_Attendre( 1 );
#else // IS_WINDOWS
	pthread_join( this->m_handle,
		NULL );
#endif // !IS_WINDOWS
}

/**
 * Construit et lance un thread
 *
 * @param fonction
 *		La fonction du thread
 * @param param
 *		Le parametre a passer au thread
 *
 * @return l'instance du thread
 */
__ALLOC NThread *NLib_Thread_NThread_Construire( NBOOL ( *fonction )( void* ),
	void *param,
	NBOOL *conditionArret )
{
	// Sortie
	__OUTPUT NThread *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NThread ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Sauvegarder
	out->m_parametre = param;

	// Creer thread
#ifdef IS_WINDOWS
	if( !( out->m_handle = CreateThread( NULL,
		0,
		(LPTHREAD_START_ROUTINE)fonction,
		param,
		0,
		NULL ) ) )
#else // IS_WINDOWS
	if( pthread_create( &out->m_handle,
		NULL,
		(void *( * )( void* ))fonction,
		param ) != 0 )
#endif // !IS_WINDOWS
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_THREAD );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_estContinuer = conditionArret;

	// OK
	return out;
}

/**
 * Detruire un thread
 *
 * @param this
 *		Cette instance
 */
void NLib_Thread_NThread_Detruire( NThread **this )
{
	// Arreter le thread
	NLib_Thread_NThread_ArreterInterne( (*this) );

	// Detruire le thread
#ifdef IS_WINDOWS
	CloseHandle( (*this)->m_handle );
#else // IS_WINDOWS
	//pthread_detach( (*this)->m_handle );
#endif // !IS_WINDOWS

	// Liberer
	NFREE( *this );
}

/**
 * Obtenir parametre thread
 *
 * @param this
 * 		Cette instance
 *
 * @return le parametre
 */
void *NLib_Thread_NThread_ObtenirParametre( const NThread *this )
{
	return this->m_parametre;
}

