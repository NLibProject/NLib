#include "../../../include/NLib/NLib.h"

// ------------------------------
// struct NLib::Temps::NFramerate
// ------------------------------

/**
 * Construire le framerate
 *
 * @param framerateSouhaite
 *		Le framerate souhaite
 *
 * @return le framerate
 */
__ALLOC NFramerate *NLib_Temps_NFramerate_Construire( NU32 framerateSouhaite )
{
	// Sortie
	__OUTPUT NFramerate *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NFramerate ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_framerateSouhaite = framerateSouhaite;

	// Calculer le temps d'attente
	out->m_tempsAttenteEntreDeuxFrame = (NU32)( ( 1.0f / (float)framerateSouhaite ) * 1000.0f );

	// OK
	return out;
}

/**
 * Detruire le framerate
 *
 * @param this
 *		Cette instance
 */
void NLib_Temps_NFramerate_Detruire( NFramerate **this )
{
	NFREE( *this );
}

/**
 * Obtenir le framerate actuel
 *
 * @param this
 *		Cette instance
 *
 * @return le framerate actuel
 */
NU32 NLib_Temps_NFramerate_ObtenirFramerateActuel( const NFramerate *this )
{
	return (NU32)this->m_framerateActuel;
}

/**
 * Debuter fonction
 *
 * @param this
 *		Cette instance
 */
void NLib_Temps_NFramerate_DebuterFonction( NFramerate *this )
{
	// Temps de debut
	this->m_tempsDebut = NLib_Temps_ObtenirTick( );
}

/**
 * Terminer fonction
 *
 * @param this
 *		Cette instance
 */
void NLib_Temps_NFramerate_TerminerFonction( NFramerate *this )
{
	// Delta
	NU32 delta;

	// Calculer delta
	delta = NLib_Temps_ObtenirTick( ) - this->m_tempsDebut;

	// Attendre le temps necessaire
	if( this->m_tempsAttenteEntreDeuxFrame > delta )
		NLib_Temps_Attendre( this->m_tempsAttenteEntreDeuxFrame - delta );

	// Ajouter le temps
	this->m_totalTempsEntreSuiteIteration += ( NLib_Temps_ObtenirTick( ) - this->m_tempsDebut );

	// Verifier
	if( ++this->m_nombreIteration >= NLIB_TEMPS_NFRAMERATE_NOMBRE_ITERATION_AVANT_CALCUL )
	{
		// Calculer
		if( (float)this->m_totalTempsEntreSuiteIteration > 0.0f )
			this->m_framerateActuel = ( (float)NLIB_TEMPS_NFRAMERATE_NOMBRE_ITERATION_AVANT_CALCUL / (float)this->m_totalTempsEntreSuiteIteration ) * 1000.0f;

		// Reset
		this->m_nombreIteration = 0;
		this->m_totalTempsEntreSuiteIteration = 0;
	}
}

