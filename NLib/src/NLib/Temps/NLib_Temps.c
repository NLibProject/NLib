#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ---------------------
// namespace NLib::Temps
// ---------------------

/* Obtenir le nombre de ms depuis le lancement */
NU32 NLib_Temps_ObtenirTick( void )
{
#ifdef IS_WINDOWS
	return GetTickCount( );
#else // IS_WINDOWS
	// Stockage temps
	struct timespec now;

	// Recuperer le temps actuel
	if( clock_gettime( CLOCK_MONOTONIC,
		&now ) )
		// Impossible d'obtenir le temps
		return 0;

	// OK
	return (NU32)( now.tv_sec * 1000.0 + now.tv_nsec / 1000000.0 );
#endif // !IS_WINDOWS
}

NU64 NLib_Temps_ObtenirTick64( void )
{
#ifdef IS_WINDOWS
	return GetTickCount64( );
#else // IS_WINDOWS
	return NLib_Temps_ObtenirTick( );
#endif // !IS_WINDOWS
}

/* Attendre (ms) */
void NLib_Temps_Attendre( NU32 t )
{
#ifdef IS_WINDOWS
	Sleep( t );
#else // IS_WINDOWS
	usleep( t * 1000 );
#endif // !IS_WINDOWS
}

/* Obtenir nombre aleatoire */
NU32 NLib_Temps_ObtenirNombreAleatoire( void )
{
#ifdef IS_WINDOWS
	// Sortie
	__OUTPUT NU32 sortie;

	// Obtenir
	rand_s( &sortie );

	// OK
	return sortie;
#else // IS_WINDOWS
	return (NU32)rand( );
#endif // !IS_WINDOWS
}

/**
 * Obtenir timestamp
 *
 * @return le timestamp en secondes
 */
NU64 NLib_Temps_ObtenirTimestamp( void )
{
	return (NU64)time( NULL );
}

/**
 * Obtenir date formattee JJ/MM/AAAA, hh:mm:ss
 *
 * @return la date du jour
 */
__ALLOC char *NLib_Temps_ObtenirDateFormate( void )
{
	return NLib_Temps_ObtenirDateFormate2( NLib_Temps_ObtenirTimestamp( ) );
}

/**
 * Obtenir date formattee JJ/MM/AAAA, hh:mm:ss
 *
 * @param timestamp
 * 		Le timestamp a partir duquel calculer la date
 *
 * @return la date du jour
 */
__ALLOC char *NLib_Temps_ObtenirDateFormate2( NU64 timestamp )
{
	// Sortie
	__OUTPUT char *out;

	// Representation temps
	struct tm *representation;

	// Allouer la memoire
	if( !( out = calloc( 21,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Obtenir representation
	representation = localtime( (time_t*)&timestamp );

	// Formatter temps
	snprintf( out,
		21,
		"%02d/%02d/%d, %02d:%02d:%02d\n",
		representation->tm_mday,
		representation->tm_mon + 1,
		representation->tm_year + 1900,
		representation->tm_hour,
		representation->tm_min,
		representation->tm_sec );

	// OK
	return out;
}


/**
 * Obtenir heure formatee hh:mm:ss
 *
 * @param nombreSeconde
 * 		Le timestamp depuis lequel calculer
 *
 * @return la chaine formatee correspondant au temps
 */
__ALLOC char *NLib_Temps_ObtenirHeureFormate( NU64 nombreSeconde )
{
	// Heure
	NU64 heure;

	// Minute
	NU64 minute;

	// Buffer
	char buffer[ 2048 ];

	// Sortie
	__OUTPUT char *sortie;

	// Calculer
	heure = nombreSeconde / 3600;
	minute = ( nombreSeconde / 60 ) % 60;

	// Construire temps
	snprintf( buffer,
		2048,
		"%02lld:%02lld:%02lld",
		heure,
		minute,
		nombreSeconde % 60 );

	// Copier
	if( !( sortie = NLib_Chaine_Dupliquer( buffer ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NULL;
	}

	// OK
	return sortie;
}

