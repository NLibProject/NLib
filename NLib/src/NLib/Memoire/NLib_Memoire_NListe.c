#include "../../../include/NLib/NLib.h"

// ----------------------------
// struct NLib::Memoire::NListe
// ----------------------------

/**
 * Construire la liste
 *
 * @param callbackSuppression
 *		Callback de suppression de l'element
 *
 * @return l'instance de la liste
 */
__ALLOC NListe *NLib_Memoire_NListe_Construire( void ( ___cdecl *callbackSuppression )( void* ) )
{
    // Sortie
    __OUTPUT NListe *out;

    // Allouer la memoire
    if( !( out = calloc( 1,
		sizeof( NListe ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

    // Construire le mutex
    if( !( out->m_mutex = NLib_Mutex_NMutex_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer le callback
	out->m_callbackSuppression = callbackSuppression;

	// OK
	return out;
}

/**
 * Detruire la liste
 *
 * @param this
 *		Cette instance
 */
void NLib_Memoire_NListe_Detruire( NListe **this )
{
	// Supprimer tous les elements
	while( (*this)->m_nombre > 0 )
        // Supprimer element
        NLib_Memoire_NListe_SupprimerDepuisIndex( *this,
			0 );

	// Detruire le mutex
	NLib_Mutex_NMutex_Detruire( &(*this)->m_mutex );

	// Liberer
	NFREE( *this );
}

/**
 * Ajouter un element
 *
 * @param this
 * 		Cette instance
 * @param element
 *		L'element a ajouter
 *
 * @return si l'operation s'est bien passee
 */
__MUSTBEPROTECTED NBOOL NLib_Memoire_NListe_Ajouter( NListe *this,
	__WILLBEOWNED void *element )
{
	// Nouvelle adresse
	char **nouvelleAdresse;

	// Allouer le nouveau conteneur
	if( !( nouvelleAdresse = calloc( this->m_nombre + 1,
		sizeof( void* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire element
		if( this->m_callbackSuppression != NULL )
			this->m_callbackSuppression( &element );

		// Quitter
		return NFALSE;
	}

	// Copier
	if( this->m_nombre > 0 )
		memcpy( nouvelleAdresse,
			this->m_element,
			this->m_nombre * sizeof( void* ) );

	// Liberer
	NFREE( this->m_element );

	// Ajouter
	nouvelleAdresse[ this->m_nombre ] = element;

	// Enregistrer
	this->m_element = nouvelleAdresse;

	// Incrementer
	this->m_nombre++;

	// OK
	return NTRUE;
}

/**
 * Supprimer un element depuis son adresse
 *
 * @param this
 *		Cette instance
 * @param element
 *		L'adresse de l'element a supprimer
 *
 * @return si l'operation s'est bien passee
 */
__MUSTBEPROTECTED NBOOL NLib_Memoire_NListe_SupprimerDepuisAdresse( NListe *this,
	void *element )
{
	// Index
	NU32 index;

	// Obtenir l'index
	if( ( index = NLib_Memoire_NListe_ObtenirIndexDepuisAdresse( this,
		element ) ) == NERREUR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NLib_Memoire_NListe_SupprimerDepuisIndex( this,
		index );
}

/**
 * Supprimer un element depuis son index
 *
 * @param this
 *		Cette instance
 * @param element
 *		L'index de l'element a supprimer
 *
 * @return si l'operation s'est bien passee
 */
__MUSTBEPROTECTED NBOOL NLib_Memoire_NListe_SupprimerDepuisIndex( NListe *this,
	NU32 element )
{
	return NLib_Memoire_NListe_SupprimerDepuisIndex2( this,
		element,
		NTRUE );
}

/**
 * Supprimer un element depuis son index
 *
 * @param this
 * 		Cette instance
 * @param element
 * 		L'index de l'element a supprimer
 * @param estDoitLibererElement
 * 		On liberer l'element qu'on supprime?
 *
 * @return si l'operation a reussi
 */
__MUSTBEPROTECTED NBOOL NLib_Memoire_NListe_SupprimerDepuisIndex2( NListe *this,
	NU32 element,
	NBOOL estDoitLibererElement )
{
	// Nouvelle adresse
	char **nouvelleAdresse = NULL;

	// Element a supprimer
	void *elementASupprimer;

	// Verifier
	if( this->m_nombre <= 0 )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Enregistrer l'element a supprimer
	elementASupprimer = this->m_element[ element ];

	// Il reste des elements apres la suppression?
	if( this->m_nombre > 1 )
	{
		// Allouer le nouvel espace
		if( !( nouvelleAdresse = calloc( this->m_nombre - 1,
			sizeof( void* ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Quitter
			return NFALSE;
		}

		// Copier les anciens elements
			// Gauche
				if( element > 0 )
					memcpy( nouvelleAdresse,
						this->m_element,
						element * sizeof( void* ) );
		// Droite
				if( (NS32)( this->m_nombre - element - 1 ) > 0 )
					memcpy( (char*)nouvelleAdresse + ( element * sizeof( void* ) ),
						(char*)this->m_element + ( element + 1 ) * sizeof( void* ),
						( this->m_nombre - element - 1 ) * sizeof( void* ) );
	}

	// Liberer le conteneur
	NFREE( this->m_element );

	// Enregistrer
	this->m_element = nouvelleAdresse;

	// Decrementer
	this->m_nombre--;

	// On dispose d'une methode de liberation?
	if( this->m_callbackSuppression != NULL
		// On doit liberer?
		&& estDoitLibererElement )
		// Liberer
		this->m_callbackSuppression( &elementASupprimer );

	// OK
	return NTRUE;
}

/**
 * Inverser deux evenements
 *
 * @param this
 *	Cette instance
 * @param e1
 *	L'index de l'element 1
 * @param e2
 *	L'index de l'element 2
 *
 * @return si l'operation s'est bien passee
 */
__MUSTBEPROTECTED NBOOL NLib_Memoire_NListe_Inverser( NListe *this,
	NU32 e1,
	NU32 e2 )
{
	// Swaper
	char *swap;

	// Verifier
	if( e1 >= this->m_nombre
		|| e2 >= this->m_nombre )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Inverser
	swap = this->m_element[ e1 ];
	this->m_element[ e1 ] = this->m_element[ e2 ];
	this->m_element[ e2 ] = swap;

	// OK
	return NTRUE;
}

/**
 * Obtenir index depuis adresse
 *
 * @param this
 *		Cette instance
 * @param element
 *		L'adresse de l'element dont on cherche l'index
 *
 * @return l'index de l'element
 */
__MUSTBEPROTECTED NU32 NLib_Memoire_NListe_ObtenirIndexDepuisAdresse( const NListe *this,
	void *element )
{
	// Index
	__OUTPUT NU32 index = 0;

	// Chercher
	for( ; index < this->m_nombre; index++ )
		// Verifier
		if( this->m_element[ index ] == element )
			return index;

	// Introuvable
	return NERREUR;
}

/**
 * Obtenir element depuis index
 *
 * @param this
 * 		Cette instance
 * @param element
 *		L'index de l'element a obtenir
 *
 * @return l'element
 */
__MUSTBEPROTECTED const void *NLib_Memoire_NListe_ObtenirElementDepuisIndex( const NListe *this,
	NU32 element )
{
	return this->m_element[ element ];
}

/**
 * Obtenir le nombre d'elements
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre d'elements
 */
__MUSTBEPROTECTED NU32 NLib_Memoire_NListe_ObtenirNombre( const NListe *this )
{
	return this->m_nombre;
}

/**
 * Proteger
 *
 * @param this
 *		Cette instance
 */
__WILLLOCK void NLib_Memoire_NListe_ActiverProtection( NListe *this )
{
	NLib_Mutex_NMutex_Lock( this->m_mutex );
}

/**
 * Ne plus proteger
 *
 * @param this
 *		Cette instance
 */
__WILLUNLOCK void NLib_Memoire_NListe_DesactiverProtection( NListe *this )
{
	NLib_Mutex_NMutex_Unlock( this->m_mutex );
}

