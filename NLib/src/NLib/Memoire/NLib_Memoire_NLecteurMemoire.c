#include "../../../include/NLib/NLib.h"

// -------------------------------------
// struct NLib::Memoire::NLecteurMemoire
// -------------------------------------

/**
 * Construire le lecteur
 *
 * @param data
 *		Les donnees
 * @param tailleData
 *		La taille des donnees
 *
 * @return l'instance du lecteur
 */
__ALLOC NLecteurMemoire *NLib_Memoire_NLecteurMemoire_Construire( const char *data,
	NU32 tailleData )
{
	// Sortie
	__OUTPUT NLecteurMemoire *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NLecteurMemoire ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_data = data;
	out->m_tailleData = tailleData;

	// OK
	return out;
}

/**
 * Detruire le lecteur
 *
 * @param this
 *		Cette instance
 */
void NLib_Memoire_NLecteurMemoire_Detruire( NLecteurMemoire **this )
{
	// Liberer
	NFREE( *this );
}

/**
 * Verifier que le curseur soit correct
 *
 * @param this
 *		Cette instance
 * @param curseur
 *		Le curseur
 *
 * @return si la position du curseur est correcte
 */
NBOOL NLib_Memoire_NLecteurMemoire_EstCurseurCorrect( const NLecteurMemoire *this,
	NU32 curseur )
{
	return curseur > this->m_tailleData;
}

/**
 * Positionner curseur
 *
 * @param this
 *		Cette instance
 * @param curseur
 *		La nouvelle position
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Memoire_NLecteurMemoire_DefinirCurseur( NLecteurMemoire *this,
	NU32 curseur )
{
	// Verifier parametre
	if( !NLib_Memoire_NLecteurMemoire_EstCurseurCorrect( this,
		curseur ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Enregistrer
	this->m_curseur = curseur;

	// OK
	return NTRUE;
}

/**
 * Obtenir la position du curseur
 *
 * @param this
 *		Cette instance
 *
 * @return la position du curseur
 */
NU32 NLib_Memoire_NLecteurMemoire_ObtenirCurseur( const NLecteurMemoire *this )
{
	return this->m_curseur;
}

/**
 * Obtenir donnees
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees
 */
const char *NLib_Memoire_NLecteurMemoire_ObtenirData( const NLecteurMemoire *this )
{
	return this->m_data;
}

/**
 * Obtenir taille donnees
 *
 * @param this
 *		Cette instance
 *
 * @return la taille des donnees
 */
NU32 NLib_Memoire_NLecteurMemoire_ObtenirTailleData( const NLecteurMemoire *this )
{
	return this->m_tailleData;
}

/**
 * Lire une chaine de caractere
 *
 * @param this
 *		Cette instance
 * @param taille
 *		La taille a lire
 *
 * @return la chaine de caractere
 */
const char *NLib_Memoire_NLecteurMemoire_Lire( NLecteurMemoire *this,
	NU32 taille )
{
    // Sortie
    __OUTPUT const char *out;

    // Verifier
    if( NLib_Memoire_NLecteurMemoire_EstCurseurCorrect( this,
		this->m_curseur + taille ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

    // Lire
    out = this->m_data + this->m_curseur;

    // Avancer le curseur
    this->m_curseur += taille;

    // OK
    return out;
}

/**
 * Lire un entier non signe
 *
 * @param this
 *		Cette instance
 *
 * @return un entier non signe
 */
NU32 NLib_Memoire_NLecteurMemoire_Lire2( NLecteurMemoire *this )
{
	// Sortie
    __OUTPUT NU32 out;

    // Verifier
    if( NLib_Memoire_NLecteurMemoire_EstCurseurCorrect( this,
		this->m_curseur + sizeof( NU32 ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return 0;
	}

	// Recuperer
	out = *( (NU32*)( this->m_data + this->m_curseur ) );

	// Avancer curseur
	this->m_curseur += sizeof( NU32 );

	// OK
	return out;
}

/**
 * Lire un entier signe
 *
 * @param this
 *		Cette instance
 *
 * @return un entier signe
 */
NS32 NLib_Memoire_NLecteurMemoire_Lire3( NLecteurMemoire *this )
{
		// Sortie
    __OUTPUT NS32 out;

	// Verifier
    if( NLib_Memoire_NLecteurMemoire_EstCurseurCorrect( this,
		this->m_curseur + sizeof( NS32 ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return 0;
	}

	// Recuperer
	out = *( (NS32*)( this->m_data + this->m_curseur ) );

	// Avancer curseur
	this->m_curseur += sizeof( NS32 );

	// OK
	return out;
}

/**
 * Lire une copie
 *
 * @param this
 *		Cette instance
 * @param taille
 *		La taille a lire
 * @param estChaine
 *		Il s'agit d'une chaine?
 *
 * @return une copie de ce qui est lu
 */
__ALLOC char *NLib_Memoire_NLecteurMemoire_LireCopieValeur( NLecteurMemoire *this,
	NU32 taille,
	NBOOL estChaine )
{
	// Sortie
	__OUTPUT char *out;

	// Verifier
    if( NLib_Memoire_NLecteurMemoire_EstCurseurCorrect( this,
		this->m_curseur + taille ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( taille + ( estChaine ?
			1
			: 0 ),
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out,
		this->m_data + this->m_curseur,
		taille );

	// Avancer curseur
	this->m_curseur += taille;

	// OK
	return out;
}

