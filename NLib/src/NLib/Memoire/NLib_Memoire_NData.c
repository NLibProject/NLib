#include "../../../include/NLib/NLib.h"

// ---------------------------
// struct NLib::Memoire::NData
// ---------------------------

/**
 * Construire la data
 *
 * @return une donnee vide
 */
__ALLOC NData *NLib_Memoire_NData_Construire( void )
{
	// Sortie
	__OUTPUT NData *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NData ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire la data
 *
 * @param this
 * 		Cette instance
 */
void NLib_Memoire_NData_Detruire( NData **this )
{
	// Liberer
	NFREE( (*this)->m_data );
	NFREE( (*this) );
}

/**
 * Ajouter data
 *
 * @param this
 * 		Cette instance
 * @param data
 * 		La donnee a ajouter
 * @param tailleData
 * 		La taille de la donnee
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Memoire_NData_AjouterData( NData *this,
	const char *data,
	NU32 tailleData )
{
	// Ajouter donnees
	if( !NLib_Memoire_AjouterData( &this->m_data,
		this->m_tailleData,
		data,
		tailleData ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Incrementer taille
	this->m_tailleData += tailleData;

	// OK
	return NTRUE;
}

/**
 * Obtenir les donnees
 *
 * @param this
 * 		Cette instance
 *
 * @return les donnees
 */
const char *NLib_Memoire_NData_ObtenirData( const NData *this )
{
	return this->m_data;
}

/**
 * Obtenir la taille des donnees
 *
 * @param this
 * 		Cette instance
 *
 * @return la taille des donnees
 */
NU32 NLib_Memoire_NData_ObtenirTailleData( const NData *this )
{
	return this->m_tailleData;
}

