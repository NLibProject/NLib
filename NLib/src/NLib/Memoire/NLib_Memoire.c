#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -----------------------
// namespace NLib::Memoire
// -----------------------

/**
 * Reallouer de la memoire
 *
 * @param src
 * 		La memoire a reallouer
 * @param tailleSrc
 * 		La taille de la memoire initiale
 * @param tailleFinale
 * 		La taille finale souhaitee
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Memoire_ReallouerMemoire( char **src,
	NU32 tailleSrc,
	NU32 tailleFinale )
{
	// Nouvelle adresse
	char *nouvelleAdresse;

	// Allouer le nouvel espace memoire
	if( !( nouvelleAdresse = calloc( tailleFinale,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Il y avait des donnees avant?
	if( tailleSrc > 0 )
		// Copier ancienne donnees
		memcpy( nouvelleAdresse,
			*src,
			tailleSrc < tailleFinale ?
				tailleSrc
				: tailleFinale );

	// Liberer
	NFREE( *src );

	// Enregistrer
	*src = nouvelleAdresse;

	// OK
	return NTRUE;
}

/* Ajouter des donnees */
NBOOL NLib_Memoire_AjouterData( char **mem,
	NU32 tailleMem,
	const char *memAjout,
	NU32 tailleMemAjout )
{
	// Augmenter la taille disponible
	if( !NLib_Memoire_ReallouerMemoire( mem,
		tailleMem,
		tailleMem + tailleMemAjout ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MEMORY );

		// Quitter
		return NFALSE;
	}

	// Copier les nouvelles donnees
	memcpy( *mem + tailleMem,
		memAjout,
		tailleMemAjout );

	// OK
	return NTRUE;
}

/**
 * Ajout donnees texte
 *
 * @param mem
 * 		L'adresse du pointeur
 * @param tailleMem
 * 		La taille actuelle de l'espace memoire
 * @param texte
 * 		Le texte a ajouter
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Memoire_AjouterData2( char **mem,
	NU32 tailleMem,
	const char *texte )
{
	return NLib_Memoire_AjouterData( mem,
		tailleMem,
		texte,
		(NU32)strlen( texte ) + 1 );
}

/* Calculer CRC32 */
NU32 NLib_Memoire_CalculerCRC( const char *data,
	NU32 taille )
{
	// Sortie
	__OUTPUT NU32 out = 0;

	// Iterateur
	NU32 i = 0;

	// Calculer
	for( ; i < taille; i++ )
		out += (NU32)*(NU8*)( data + i );

	// OK
	return out;
}

/* Swap */
NBOOL NLib_Memoire_Swap( void *d1,
	void *d2,
	NU32 taille )
{
	// Temp
	char *temp;

	// Allouer la memoire
	if( !( temp = calloc( taille,
		sizeof( char ) ) ) )
		return NFALSE;

	// Swap
		// temp<-d1;
			memcpy( temp,
				d1,
				taille );
		// d1<-d2
			memcpy( d1,
				d2,
				taille );
		// d2<-temp
			memcpy( d2,
				temp,
				taille );

	// Liberer
	NFREE( temp );

	// OK
	return NTRUE;
}

void NLib_Memoire_SwapEntier( NU32 *d1,
	NU32 *d2 )
{
	// Temp
	NU32 d3;

	// Swap
	d3 = *d1;
	*d2 = *d1;
	*d1 = d3;
}

/**
 * Afficher la memoire
 *
 * @param mem
 *		La memoire a afficher
 * @param taille
 *		La taille a afficher
 */
void NLib_Memoire_Afficher( const void *mem,
	NU32 taille )
{
	// Iterateur
	NU32 i = 0;

	// Afficher
	for( ; i < taille; i++ )
		printf( "%c",
			((char*)mem)[ i ] );

	// '\n'
	puts( "" );
}

/**
 * Afficher en hexa
 *
 * @param data
 *		Les donnees
 * @param tailleData
 *		La taille des donnees
 */
void NLib_Memoire_Afficher2( const char *data,
	NU32 tailleData )
{
	// Iterateur
	NU32 i = 0;

	// Afficher
	for( ; i < tailleData; i++ )
	{
		// Retour ligne
		if( !( i % 8 )
			&& i != 0 )
			puts( "" );

		// Afficher
		printf( "%.2x ",
			data[ i ] );
	}
}

/**
 * Liberer un simple element e une dimension (utile pour le callback de
 * NListe)
 *
 * @param e
 * 		L'element a Liberer
 */
void NLib_Memoire_Liberer( char **e )
{
	NFREE( *e );
}

/**
 * Afficher entier en binaire
 *
 * @param entier
 * 		L'entier a afficher
 * @param taille
 * 		La taille de l'entier
 * @param tailleGroupe
 * 		Le nombre d'octets par groupe d'octets (4 par exemple)
 */
void NLib_Memoire_AfficherBinaire( void ( *callbackAffichage )( const char * ),
	NU32 entier,
	NU32 taille,
	NU32 tailleGroupe )
{
	// Iterateur
	NS32 i;

	// Buffer
	char buffer[ 2048 ];

#define AFFICHER_MESSAGE( m ) \
	if( callbackAffichage != NULL ) \
		callbackAffichage( m );

	// Afficher
	for( i = (NS32)( taille - 1 ); i >= 0; i-- )
	{
		// Composer message
		snprintf( buffer,
			2048,
			"%d",
			( entier & (NU32)NLib_Math_CalculerPuissance( 2,
				(NU32)i ) ) != 0 );

		// Afficher message
		AFFICHER_MESSAGE( buffer );

		// Separer
		if( ( i % tailleGroupe ) == 0 )
			AFFICHER_MESSAGE( " " );
	}

#undef AFFICHER_MESSAGE

	// Vider flux en attente
	fflush( stdout );
}

/**
 * Compter le nombre de bits à %bitACompter%
 *
 * @param entier
 * 		L'entier a afficher
 * @param taille
 * 		La taille de l'entier
 * @param bitACompter
 * 		0 ou 1 suivant ce qu'on veut compter
 *
 * @return le nombre de bit à %bitACompter%
 */
NU32 NLib_Memoire_CompterNombreBit( NU32 entier,
	NU32 taille,
	NU32 bitACompter )
{
	// Iterateur
	NS32 i;

	// Sortie
	__OUTPUT NU32 sortie = 0;

	// Compter
	for( i = (NS32)( taille - 1 ); i >= 0; i-- )
		// Ajouter
		sortie += ( entier & (NU32)NLib_Math_CalculerPuissance( 2,
			(NU32)i ) ) != 0 ?
				( bitACompter == 1 ?
					1
					: 0 )
						: ( bitACompter == 0 ?
							1
							: 0 );

	// OK
	return sortie;
}

/**
 * Inverser un tableau de NU8
 *
 * @param tableau
 * 		Le tableau de NU8
 * @param taille
 * 		La taille du tableau
 */
void NLib_Memoire_Inverser( NU8 *tableau,
	NU32 taille )
{
	// Iterateur
	NU32 i = 0;

	// Swap
	NU8 swap;

	// Inverser
	for( ; i < taille / 2; i++ )
	{
		swap = tableau[ ( taille - i - 1 ) ];
		tableau[ ( taille - i - 1 ) ] = tableau[ i ];
		tableau[ i ] = swap;
	}
}