#include "../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_PNGWRITER

// ---------------------------------
// namespace NLib::Module::PNGWriter
// ---------------------------------

/**
 * Ecrire tableau de pixels vers PNG
 *
 * @param pixels
 *		Le tableau brut en entree
 * @param w
 *		La taille taille horizontale
 * @param h
 *		La taille verticale
 * @param bpp
 *		Nombre de byte par pixel
 * @param path
 *		Le chemin de sauvegarde
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_PNGWriter_Inscrire( const char *pixels,
	NU32 w,
	NU32 h,
	NU32 bpp,
	const char *path )
{
	// Fichier sortie
    FILE * fp;

    // Handle png
    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;

    // Iterateur
    NU32 x,
		y;

	// Pixels png
    png_byte ** row_pointers = NULL;
    png_byte *row;

    // Pixel
    const NU8 *pixel;

    // Ouvrir fichier sortie
    if( !( fp = fopen( path,
		"wb+" ) ) )
    {
        // Notifier
        NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

        // Quitter
        return NFALSE;
    }

    // Initialiser contexte png
    if( !( png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING,
		NULL,
		NULL,
		NULL ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PNG );

		// Fermer fichier
		fclose( fp );

		// Quitter
		return NFALSE;
	}

	// Creer informations sur l'image
    if( !( info_ptr = png_create_info_struct( png_ptr ) ) )
    {
        // Notifier
		NOTIFIER_ERREUR( NERREUR_PNG );

		// Detruire contexte
		png_destroy_write_struct( &png_ptr,
			&info_ptr );

		// Fermer fichier
		fclose( fp );

		// Quitter
		return NFALSE;
    }

    // Parametrer gestion des erreurs
    if( setjmp( png_jmpbuf( png_ptr ) ) )
    {
        // Notifier
		NOTIFIER_ERREUR( NERREUR_PNG );

		// Detruire contexte
		png_destroy_write_struct( &png_ptr,
			&info_ptr );

		// Fermer fichier
		fclose( fp );

		// Quitter
		return NFALSE;
    }

    // Definir attributs image
    png_set_IHDR( png_ptr,
	  info_ptr,
	  w,
	  h,
	  8,
	  PNG_COLOR_TYPE_RGB,
	  PNG_INTERLACE_NONE,
	  PNG_COMPRESSION_TYPE_DEFAULT,
	  PNG_FILTER_TYPE_DEFAULT );

    // Allouer le conteneur des lignes
    row_pointers = png_malloc( png_ptr,
		h * sizeof( png_byte* ) );

	// Creer les lignes
    for( y = 0; y < h; y++ )
    {
		// Ligne actuelle
        row = png_malloc( png_ptr,
			sizeof( NU8 ) * w * bpp );

		// Enregistrer
        row_pointers[ y ] = row;

        // Remplir ligne
        for( x = 0; x < w; x++ )
        {
			// Obtenir pixel
            pixel = (const NU8*)( pixels + ( ( w * y ) + x ) * 3 );

			// Enregistrer RGB
            *row++ = *pixel;
            *row++ = *( pixel + 1 );
            *row++ = *( pixel + 2 );
        }
    }

    /* Write the image data to "fp". */
    png_init_io( png_ptr,
		fp );
    png_set_rows( png_ptr,
		info_ptr,
		row_pointers );
    png_write_png( png_ptr,
		info_ptr,
		PNG_TRANSFORM_IDENTITY,
		NULL );

	// Liberer
    for( y = 0; y < h; y++ )
        png_free( png_ptr,
			row_pointers[ y ] );
    png_free( png_ptr,
		row_pointers );

	// Detruire contexte
	png_destroy_write_struct( &png_ptr,
			&info_ptr );

	// Fermer fichier
	fclose( fp );

    // OK
    return NTRUE;
}

#endif // NLIB_MODULE_PNGWRITER

