#define NLIB_MODULE_FMODEX_INTERNE
#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ------------------------------
// namespace NLib::Module::FModex
// ------------------------------

#ifdef NLIB_MODULE_FMODEX
/* Initialiser */
NBOOL NLib_Module_FModex_Initialiser( void )
{
	// Creer le contexte
	if( FMOD_System_Create( &m_contexte ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Quitter
		return NFALSE;
	}

	// Initialiser le contexte
	if( FMOD_System_Init( m_contexte,
		NLIB_MODULE_FMOD_MAX_CANAUX,
		FMOD_INIT_NORMAL,
		NULL ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Liberer le contexte
		FMOD_System_Release( m_contexte );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Detruire */
void NLib_Module_FModex_Detruire( void )
{
	// Detruire le contexte
	FMOD_System_Close( m_contexte );

	// Liberer
	FMOD_System_Release( m_contexte );
}

/* Obtenir le contexte */
FMOD_SYSTEM *NLib_Module_FModex_ObtenirContexte( void )
{
	return m_contexte;
}

/* Stopper les sons termines */
void NLib_Module_FModex_Update( void )
{
	// Mettre a jour
	FMOD_System_Update( m_contexte );
}

#endif // NLIB_MODULE_FMODEX

