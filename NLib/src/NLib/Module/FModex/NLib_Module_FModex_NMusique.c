#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------
// struct NLib::Module::FModex::NMusique
// -------------------------------------

#ifdef NLIB_MODULE_FMODEX
/**
 * Construire musique
 *
 * @param lien
 * 		Le lien vers la musique
 * @param volume
 * 		Le volume initial (0-100)
 * @param estRepete
 * 		La musique se repete?
 *
 * @return
 */
__ALLOC NMusique *NLib_Module_FModex_NMusique_Construire( const char *lien,
	NU32 volume,
	NBOOL estRepete )
{
	// Sortie
	__OUTPUT NMusique *out;

	// Code fmod
	FMOD_RESULT code;

#ifndef IS_WINDOWS
	FMOD_CREATESOUNDEXINFO informationSupplementaire;
#endif // !IS_WINDOWS

	// Verifier parametre
	if( volume > 100 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NMusique ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Recuperer le contexte
	out->m_contexte = NLib_Module_FModex_ObtenirContexte( );

#ifndef IS_WINDOWS
	// Vider
	memset( &informationSupplementaire,
		0,
		sizeof( FMOD_CREATESOUNDEXINFO ) );

	// Definir
	informationSupplementaire.cbsize = sizeof( FMOD_CREATESOUNDEXINFO );
	informationSupplementaire.dlsname = NLIB_MODULE_FMOD_DLS_FILE_UNIX;
#endif // !IS_WINDOWS

	// Creer la musique
	if( ( code = FMOD_System_CreateSound( (FMOD_SYSTEM*)out->m_contexte,
		lien,
		(FMOD_MODE)( FMOD_DEFAULT | ( estRepete ? FMOD_LOOP_NORMAL : 0 ) ),
#ifdef IS_WINDOWS
		NULL,
#else // IS_WINDOWS
		&informationSupplementaire,
#endif // !IS_WINDOWS
		&out->m_musique ) ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR_UTILISATEUR( NERREUR_FMODEX,
			__FUNCTION__,
			code );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Obtenir duree
	if( FMOD_Sound_GetLength( out->m_musique,
		&out->m_duree,
		FMOD_TIMEUNIT_MS ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Detruire le son
		FMOD_Sound_Release( out->m_musique );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Activer les repetitions
	if( estRepete )
		if( FMOD_Sound_SetLoopCount( out->m_musique,
			NLIB_MODULE_FMODEX_REPETITION_INFINIE ) != FMOD_OK )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FMODEX );

			// Detruire le son
			FMOD_Sound_Release( out->m_musique );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Zero
	out->m_estLecture = NFALSE;
	out->m_positionPause = 0;

	// Enregistrer
	out->m_volume = volume;

	// OK
	return out;
}

/**
 * Detruire musique
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_FModex_NMusique_Detruire( NMusique **this )
{
	// Arreter la lecture
	if( (*this)->m_estLecture )
		FMOD_Channel_Stop( (*this)->m_canal );

	// Detruire le son
	FMOD_Sound_Release( (*this)->m_musique );

	// Liberer
	NFREE( (*this) );
}

/**
 * Reprendre la lecture
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NMusique_Lire( NMusique *this )
{
	// Verifier etat
	if( this->m_estLecture )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Update FMod
	NLib_Module_FModex_Update( );

	// Lire
	if( FMOD_System_PlaySound( (FMOD_SYSTEM*)this->m_contexte,
		this->m_musique,
		NULL,
		NTRUE,
		&this->m_canal ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Quitter
		return NFALSE;
	}

	// Definir le volume
	if( FMOD_Channel_SetVolume( this->m_canal,
		(float)this->m_volume / 100.0f ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Tuer le canal
		FMOD_Channel_Stop( this->m_canal );

		// Quitter
		return NFALSE;
	}

	// Definir la position de reprise
	if( FMOD_Channel_SetPosition( this->m_canal,
		this->m_positionPause,
		FMOD_TIMEUNIT_MS ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Restaurer le temps 0
		this->m_positionPause = 0;

		// Tuer le canal
		FMOD_Channel_Stop( this->m_canal );

		// Quitter
		return NFALSE;
	}

	// Lancer la musique
	if( FMOD_Channel_SetPaused( this->m_canal,
		NFALSE ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Tuer le canal
		FMOD_Channel_Stop( this->m_canal );

		// Quitter
		return NFALSE;
	}

	// On est en lecture
	return this->m_estLecture = NTRUE;
}

/**
 * Mettre en pause
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NMusique_Pause( NMusique *this )
{
	// Verifier etat
	if( !this->m_estLecture )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Recuperer le temps actuel
	if( FMOD_Channel_GetPosition( this->m_canal,
		&this->m_positionPause,
		FMOD_TIMEUNIT_MS ) != FMOD_OK )
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_FMODEX );

	// Stopper la lecture
	if( FMOD_Channel_Stop( this->m_canal ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Quitter
		return NFALSE;
	}

	// On est en pause
	return (NBOOL)!( this->m_estLecture = NFALSE );
}

/**
 * Stopper la lecture
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NMusique_Arreter( NMusique *this )
{
	// Verifier
	if( !this->m_estLecture )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Zero
	this->m_positionPause = 0;

	// Stopper la lecture
	if( FMOD_Channel_Stop( this->m_canal ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Quitter
		return NFALSE;
	}

	// OK
	return (NBOOL)!( this->m_estLecture = NFALSE );
}

/**
 * Lecture en cours?
 *
 * @param this
 * 		Cette instance
 *
 * @return si la musique est en cours de lecture
 */
NBOOL NLib_Module_FModex_NMusique_EstLecture( const NMusique *this )
{
	return this->m_estLecture;
}

/**
 * Pause en cours?
 *
 * @param this
 * 		Cette instance
 *
 * @return si la musique est en cours de pause
 */
NBOOL NLib_Module_FModex_NMusique_EstPause( const NMusique *this )
{
	return (NBOOL)!this->m_estLecture;
}

/**
 * Definir le volume
 *
 * @param this
 * 		Cette instance
 * @param volume
 * 		Le volume a definir (0-100)
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NMusique_DefinirVolume( NMusique *this,
	NU32 volume )
{
	// Verifier parametre
	if( volume > 100 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;

	}
	// Enregistrer
	this->m_volume = volume;

	// Definir sur le canal si lecture en cours
	if( this->m_estLecture )
		return (NBOOL)( FMOD_Channel_SetVolume( this->m_canal,
			(float)volume / 100.0f ) == FMOD_OK );

	// OK
	return NTRUE;
}

/**
 * Definir position (ms)
 *
 * @param this
 * 		Cette instance
 * @param position
 * 		La position en ms
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NMusique_DefinirPosition( NMusique *this,
	NU32 position )
{
	// Paused
	if( !this->m_estLecture )
		this->m_positionPause = position;
	else
		return (NBOOL)( FMOD_Channel_SetPosition( this->m_canal,
			position,
			FMOD_TIMEUNIT_MS ) == FMOD_OK );

	// OK
	return NTRUE;
}

/**
 * Obtenir position (ms)
 *
 * @param this
 * 		Cette instance
 *
 * @return la position (ms)
 */
NU32 NLib_Module_FModex_NMusique_ObtenirPosition( NMusique *this )
{
	// Position
	__OUTPUT NU32 position = 0;

	// Lecture en cours?
	if( this->m_estLecture )
		// Obtenir position
		FMOD_Channel_GetPosition( this->m_canal,
			&position,
			FMOD_TIMEUNIT_MS );

	// OK
	return position;
}

/**
 * Obtenir volume (0 - 100)
 *
 * @param this
 * 		Cette instance
 *
 * @return le volume
 */
NU32 NLib_Module_FModex_NMusique_ObtenirVolume( const NMusique *this )
{
	return this->m_volume;
}

/**
 * Obtenir duree musique
 *
 * @param this
 * 		Cette instance
 *
 * @return la duree de la musique
 */
NU32 NLib_Module_FModex_NMusique_ObtenirDuree( const NMusique *this )
{
	return this->m_duree;
}
#endif // NLIB_MODULE_FMODEX

