#include "../../../../include/NLib/NLib.h"
#include "../../../../include/NLib/Module/FModex/NLib_Module_FModex_NSon.h"

/*
	@author SOARES Lucas
*/

// ---------------------------------
// struct NLib::Module::FModex::NSon
// ---------------------------------

#ifdef NLIB_MODULE_FMODEX
/**
 * Construire le son
 *
 * @param lien
 * 		Le lien vers le fichier audio
 * @param volume
 * 		Le volume (1 - 100)
 *
 * @return l'instance
 */
__ALLOC NSon *NLib_Module_FModex_NSon_Construire( const char *lien,
	NU32 volume )
{
	// Sortie
	__OUTPUT NSon *out;

	// Verifier parametre
	if( volume == 0
		|| volume > 100 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NSon ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Recuperer le contexte
	out->m_contexte = NLib_Module_FModex_ObtenirContexte( );

	// Creer le son
	if( FMOD_System_CreateSound( (FMOD_SYSTEM*)out->m_contexte,
		lien,
		FMOD_2D | FMOD_CREATESAMPLE,
		NULL,
		&out->m_son ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_volume = volume;

	// OK
	return out;
}

/**
 * Detruire l'instance
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_FModex_NSon_Detruire( NSon **this )
{
	// Detruire le son
	FMOD_Sound_Release( (*this)->m_son );

	// Liberer
	NFREE( (*this) );
}

/**
 * Lancer la lecture
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NSon_Lire( NSon *this )
{
	// Update FMod
	NLib_Module_FModex_Update( );

	// Lire
	if( FMOD_System_PlaySound( (FMOD_SYSTEM*)this->m_contexte,
		this->m_son,
		NULL,
		NTRUE,
		&this->m_canal ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Quitter
		return NFALSE;
	}

	// Definir le volume
	if( FMOD_Channel_SetVolume( this->m_canal,
		(float)this->m_volume / 100.0f ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Tuer le canal
		FMOD_Channel_Stop( this->m_canal );

		// Quitter
		return NFALSE;
	}

	// Lancer le son
	if( FMOD_Channel_SetPaused( this->m_canal,
		NFALSE ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Tuer le canal
		FMOD_Channel_Stop( this->m_canal );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Definir le volume
 *
 * @param this
 * 		Cette instance
 * @param volume
 * 		Le volume a definir (1 - 100)
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NSon_DefinirVolume( NSon *this,
	NU32 volume )
{
	// Verifier parametre
	if( volume == 0
		|| volume > 100 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;

	}
	// Enregistrer
	this->m_volume = volume;

	// OK
	return NTRUE;
}

/**
 * Est en lecture?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le son est en lecture
 */
NBOOL NLib_Module_FModex_NSon_EstLecture( NSon *this )
{
	// Est en lecture?
	__OUTPUT FMOD_BOOL estLecture = (FMOD_BOOL)0;

	// Obtenir canal
	if( this->m_canal != NULL )
		// En cours de lecture?
		FMOD_Channel_IsPlaying( this->m_canal,
			&estLecture );

	// OK
	return (NBOOL)estLecture;
}

/**
 * Obtenir position (ms)
 *
 * @param this
 * 		Cette instance
 *
 * @return la position dans le son (ms)
 */
NU32 NLib_Module_FModex_NSon_ObtenirPosition( NSon *this )
{
	// Position
	__OUTPUT NU32 position = 0;

	// Canal present?
	if( this->m_canal )
		// Obtenir position
		FMOD_Channel_GetPosition( this->m_canal,
			&position,
			FMOD_TIMEUNIT_MS );

	// OK
	return position;
}
#endif // NLIB_MODULE_FMODEX

