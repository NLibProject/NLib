#include "../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_WEBCAM

// ------------------------------
// namespace NLib::Module::Webcam
// ------------------------------

/**
 * Effectue une requete IOCTL
 *
 * @param handle
 * 		Handle v4l2
 * @param request
 * 		Le contrôle a effectuer
 * @param arg
 * 		L'argument a transmettre
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Webcam_xioctl( int handle,
	int request,
	void *arg )
{
	// Code retour de l'ioctl
	int codeRetour;

	// Effectuer l'ioctl
	do
	{
		// Executer
		codeRetour = v4l2_ioctl( handle,
			request,
			arg );
	} while( codeRetour == -1
		&& ( ( errno == EINTR )
			|| ( errno == EAGAIN ) ) );

	// Impossible d'effectuer l'ioctl
	if( codeRetour == -1 )
	{
		// Notifier
		NOTIFIER_ERREUR_UTILISATEUR( NERREUR_V4L2,
			__FUNCTION__,
			errno );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

#endif // NLIB_MODULE_WEBCAM


