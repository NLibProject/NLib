#include "../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_WEBCAM

// ------------------------------------
// struct NLib::Module::Webcam::NCamera
// ------------------------------------

/**
 * Construire la camera
 *
 * @param dev
 * 		Le device a utiliser
 * @param w
 * 		La resolution horizontale
 * @param h
 * 		La resolution verticale
 *
 * @return l'instance de la camera
 */
 __ALLOC NCamera *NLib_Module_Webcam_NCamera_Construire( const char *dev,
	NU32 w,
	NU32 h )
 {
	 // Format de capture
	struct v4l2_format formatCapture = { 0, };

	// Proprietes buffer
	struct v4l2_buffer infoBuffer;

	// Request buffer
	struct v4l2_requestbuffers requeteBuffer = { 0, };

	// Type de buffer
	enum v4l2_buf_type type;

	// Iterateur
	unsigned int i,
		idBuffer;

	// Instance camera
	__OUTPUT NCamera *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NCamera ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Ouvrir le flux de webcam
	if( ( out->m_descripteurWebcam = v4l2_open( dev,
		O_RDWR | O_NONBLOCK,
		0 ) ) < 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_V4L2 );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Definir le format de capture
	formatCapture.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	formatCapture.fmt.pix.width = w;
	formatCapture.fmt.pix.height = h;
	formatCapture.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
	formatCapture.fmt.pix.field = V4L2_FIELD_INTERLACED;

	// Enregistrer le format
	if( !NLib_Module_Webcam_xioctl( out->m_descripteurWebcam,
		VIDIOC_S_FMT,
		&formatCapture ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_V4L2 );

		// Fermer le descripteur
		v4l2_close( out->m_descripteurWebcam );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer la taille du format
	out->m_tailleFormat = 3;

	// Verifier que la requete de format ait ete acceptee
	if( formatCapture.fmt.pix.pixelformat != V4L2_PIX_FMT_RGB24 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_V4L2 );

		// Fermer le descripteur
		v4l2_close( out->m_descripteurWebcam );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer la taille de capture
	out->m_tailleCapture.x = formatCapture.fmt.pix.width;
	out->m_tailleCapture.y = formatCapture.fmt.pix.height;

	// Creer la requete pour le type de lecture
	requeteBuffer.count = NLIB_MODULE_WEBCAM_NOMBRE_BUFFER;
	requeteBuffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	requeteBuffer.memory = V4L2_MEMORY_MMAP;

	// Envoyer la requete
	if( !NLib_Module_Webcam_xioctl( out->m_descripteurWebcam,
		VIDIOC_REQBUFS,
		&requeteBuffer ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_V4L2 );

		// Fermer le descripteur
		v4l2_close( out->m_descripteurWebcam );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer le nombre de buffer(s)
	out->m_nombreBuffer = requeteBuffer.count;

	// Allouer les buffers
	if( !( out->m_buffer = calloc( requeteBuffer.count,
		sizeof( NImageCamera ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Fermer le descripteur
		v4l2_close( out->m_descripteurWebcam );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Allouer les buffers
	for( idBuffer = 0; idBuffer < requeteBuffer.count; idBuffer++ )
	{
		// Vider le buffer
		memset( &infoBuffer,
			0,
			sizeof( struct v4l2_buffer ) );

		// Parametrer la requete de buffer
		infoBuffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		infoBuffer.memory = V4L2_MEMORY_MMAP;
		infoBuffer.index = idBuffer;

		// Envoyer la requete
		if( !NLib_Module_Webcam_xioctl( out->m_descripteurWebcam,
			VIDIOC_QUERYBUF,
			&infoBuffer ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_V4L2 );

			// Liberer
			for( i = 0; i < idBuffer; i++ )
				v4l2_munmap( out->m_buffer[ i ].m_data,
					out->m_buffer[ i ].m_taille );

			// Fermer le descripteur
			v4l2_close( out->m_descripteurWebcam );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Enregistrer la taille d'un buffer
		out->m_buffer[ idBuffer ].m_taille = infoBuffer.length;

		// Allouer la memoire
		out->m_buffer[ idBuffer ].m_data = v4l2_mmap( NULL,
			infoBuffer.length,
			PROT_READ | PROT_WRITE,
			MAP_SHARED,
			out->m_descripteurWebcam,
			infoBuffer.m.offset );

		// L'allocation a echouee
		if( out->m_buffer[ idBuffer ].m_data == MAP_FAILED )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_V4L2 );

			// Liberer
			for( i = 0; i < idBuffer; i++ )
				v4l2_munmap( out->m_buffer[ i ].m_data,
					out->m_buffer[ i ].m_taille );

			// Fermer le descripteur
			v4l2_close( out->m_descripteurWebcam );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}
	}

	// Parametrer buffers
	for( i = 0; i < requeteBuffer.count; i++ )
	{
		// Vider le buffer
		memset( &infoBuffer,
			0,
			sizeof( struct v4l2_buffer ) );

		// Parametrer
		infoBuffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		infoBuffer.memory = V4L2_MEMORY_MMAP;
		infoBuffer.index = i;

		// Envoyer requete
		if( !NLib_Module_Webcam_xioctl( out->m_descripteurWebcam,
			VIDIOC_QBUF,
			&infoBuffer ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_V4L2 );

			// Liberer
			for( i = 0; i < out->m_nombreBuffer; i++ )
				v4l2_munmap( out->m_buffer[ i ].m_data,
					out->m_buffer[ i ].m_taille );

			// Fermer le descripteur
			v4l2_close( out->m_descripteurWebcam );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}
	}

	// Definir le type d'operation
	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	// Lancer le flux
	if( !NLib_Module_Webcam_xioctl( out->m_descripteurWebcam,
		VIDIOC_STREAMON,
		&type ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_V4L2 );

		// Liberer
		for( i = 0; i < out->m_nombreBuffer; i++ )
			v4l2_munmap( out->m_buffer[ i ].m_data,
				out->m_buffer[ i ].m_taille );

		// Fermer le descripteur
		v4l2_close( out->m_descripteurWebcam );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire la camera
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Webcam_NCamera_Detruire( NCamera **this )
{
	// Type de buffer
	enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	// Iterateur
	unsigned int i = 0;

	// Demander la fin de la lecture du flux
	NLib_Module_Webcam_xioctl( (*this)->m_descripteurWebcam,
		VIDIOC_STREAMOFF,
		&type );

	// Liberer les buffers
	for( ; i < (*this)->m_nombreBuffer; i++ )
		v4l2_munmap( (*this)->m_buffer[ i ].m_data,
			(*this)->m_buffer[ i ].m_taille );

	// Fermer le decripteur webcam
	v4l2_close( (*this)->m_descripteurWebcam );

	// Liberer
	NFREE( (*this)->m_buffer );
	NFREE( (*this) );
}

/**
 * Lire une image sur la webcam
 *
 * @param this
 * 		Cette instance
 *
 * @return l'image
 */
__ALLOC NImageCamera *NLib_Module_Webcam_NCamera_Capturer( NCamera *this )
{
	// Selecteur de flux
	fd_set selecteurFlux;

	// Timeout selecteur
	struct timeval timeoutSelecteur;

	// Code retour de la selection du flux
	int retourSelecteur;

	// Proprietes buffer
	struct v4l2_buffer infoBuffer = { 0, };

	// Sortie
	__OUTPUT NImageCamera *out;

	// Attendre que le flux soit disponible
	do
	{
		// Descripteur a zero
		FD_ZERO( &selecteurFlux );
		FD_SET( this->m_descripteurWebcam,
			&selecteurFlux );

		// Definir le timeout
		timeoutSelecteur.tv_sec = 1;
		timeoutSelecteur.tv_usec = 0;

		// Verifier si le flux est libre
		retourSelecteur = select( this->m_descripteurWebcam + 1,
			&selecteurFlux,
			NULL,
			NULL,
			&timeoutSelecteur );
	} while( ( retourSelecteur == -1
		&& ( errno == EINTR ) ) );

	// Verifier si la selection a reussi
	if( retourSelecteur == -1 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_TIMEOUT );

		// Quitter
		return NULL;
	}

	// Definir les proprietes de capture
	infoBuffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	infoBuffer.memory = V4L2_MEMORY_MMAP;

	// Envoyer la requete
	NLib_Module_Webcam_xioctl( this->m_descripteurWebcam,
		VIDIOC_DQBUF,
		&infoBuffer );

	// Creer l'image
	if( !( out = NLib_Module_Webcam_NImageCamera_Construire( this->m_buffer[ infoBuffer.index ].m_data,
		infoBuffer.bytesused,
		this->m_tailleCapture.x,
		this->m_tailleCapture.y,
		this->m_tailleFormat ) ) )
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

	// Fin de lecture
	NLib_Module_Webcam_xioctl( this->m_descripteurWebcam,
		VIDIOC_QBUF,
		&infoBuffer );

	// OK
	return out;
}

/**
 * Obtenir resolution capture
 *
 * @param this
 *		Cette instance
 *
 * @return la resolution de la capture
 */
const NUPoint *NLib_Module_Webcam_NCamera_ObtenirResolution( const NCamera *this )
{
	return &this->m_tailleCapture;
}

/**
 * Obtenir nombre d'octets par pixel
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre d'octets par pixel
 */
NU32 NLib_Module_Webcam_NCamera_ObtenirNombreOctetParPixel( const NCamera *this )
{
	return this->m_tailleFormat;
}

#endif // NLIB_MODULE_WEBCAM

