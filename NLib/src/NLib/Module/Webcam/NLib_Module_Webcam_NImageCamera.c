#include "../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_WEBCAM

// -----------------------------------------
// struct NLib::Module::Webcam::NImageCamera
// -----------------------------------------

/**
 * Construire une image
 *
 * @param data
 *		Les donnees de l'image
 * @param taille
 *		La taille des donnees
 * @param w
 *		La taille horizontale de l'image
 * @param h
 *		La taille verticale de l'image
 * @param nombreOctetParPixel
 *		Nombre d'octets par pixel
 *
 * @return l'instance de l'image creee
 */
__ALLOC NImageCamera *NLib_Module_Webcam_NImageCamera_Construire( const char *data,
	NU32 taille,
	NU32 w,
	NU32 h,
	NU32 nombreOctetParPixel )
{
	// Sortie
	__OUTPUT NImageCamera *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NImageCamera ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer la place pour les donnees
	if( !( out->m_data = calloc( taille,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier donnees
	memcpy( out->m_data,
		data,
		taille );

	// Enregistrer
		// Taille
			out->m_taille = taille;
		// Dimension
			NDEFINIR_POSITION( out->m_dimension,
				w,
				h );
		// Bytes per pixel
			out->m_nombreOctetParPixel = nombreOctetParPixel;

	// OK
	return out;
}

/**
 * Detruire une image
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Webcam_NImageCamera_Detruire( NImageCamera **this )
{
	// Liberer
	NFREE( (*this)->m_data );
	NFREE( *this );
}

/**
 * Enregistrer une image au format PCM
 *
 * @param this
 * 		Cette instance
 * @param lien
 * 		Le lien d'enregistrement de l'image
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Webcam_NImageCamera_Enregistrer( const NImageCamera *this,
	const char *lien )
{
	// Fichier sortie
	FILE *sortie;

	// Ouvrir la sortie
	if( !( sortie = fopen( lien,
		"w+" ) ) )
	{
		// Notifier
		printf( "Impossible d'ouvrir la sortie.\n" );

		// Quitter
		return NFALSE;
	}

	// Inscrire l'header
	fprintf( sortie,
		"P6\n%d %d 255\n",
		this->m_dimension.x,
		this->m_dimension.y );

	// Inscrire image
	fwrite( this->m_data,
		this->m_taille,
		1,
		sortie );

	// Fermer fichier
	fclose( sortie );

	// OK
	return NTRUE;
}

/**
 * Obtenir taille en octets
 *
 * @param this
 *		Cette instance
 *
 * @return la taille en octets
 */
NU32 NLib_Module_Webcam_NImageCamera_ObtenirTaille( const NImageCamera *this )
{
	return this->m_taille;
}

/**
 * Obtenir resolution
 *
 * @param this
 *		Cette instance
 *
 * @return la resolution
 */
const NUPoint *NLib_Module_Webcam_NImageCamera_ObtenirDimension( const NImageCamera *this )
{
	return &this->m_dimension;
}

/**
 * Obtenir octets par pixels
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre d'octets par pixel
 */
NU32 NLib_Module_Webcam_NImageCamera_ObtenirOctetParPixel( const NImageCamera *this )
{
	return this->m_nombreOctetParPixel;
}

/**
 * Obtenir data
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees
 */
const char *NLib_Module_Webcam_NImageCamera_ObtenirData( const NImageCamera *this )
{
	return this->m_data;
}

#endif // NLIB_MODULE_WEBCAM
