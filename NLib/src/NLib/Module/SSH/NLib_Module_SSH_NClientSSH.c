#include "../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_SSH
// ------------------------------------
// struct NLib::Module::SSH::NClientSSH
// ------------------------------------

/**
 * Attendre que la socket soit prete (exec) (private)
 *
 * @param this
 * 		Cette instance
 * @param timeout
 * 		Le timeout (ms)
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE NS32 NLib_Module_SSH_NClientSSH_AttendreExec( NClientSSH *this,
	NU32 timeout )
{
	// Le timeout
	struct timeval timeoutSelection;

	// Descripteur selection
	fd_set fd;

	// Selection ecriture
	fd_set *writefd = NULL;

	// Selection lecture
	fd_set *readfd = NULL;

	// Direction de l'attente
	NS32 directionAttente;

	// Definir timeout
	timeoutSelection.tv_sec = timeout / 1000;
	timeoutSelection.tv_usec = ( timeout % 1000 ) * 1000;

	// Descripteur a zero
	FD_ZERO( &fd );

	// Initialiser le descripteur
	FD_SET( this->m_socket,
		&fd );

	// Detecter dans quelle direction attendre
	directionAttente = libssh2_session_block_directions( this->m_sessionSSH );

	// On attend en lecture?
	if( directionAttente & LIBSSH2_SESSION_BLOCK_INBOUND )
		// Enregistrer
		readfd = &fd;

	// On attend en ecriture
	if( directionAttente & LIBSSH2_SESSION_BLOCK_OUTBOUND )
		// Enregistrer
		writefd = &fd;

	// Attendre
	return select( this->m_socket + 1,
		readfd,
		writefd,
		NULL,
		&timeoutSelection );
}

/**
 * Creer processus (private)
 *
 * @param this
 * 		Cette instance
 * @param commande
 * 		La commande a executer
 *
 * @return si le shell a pu etre cree
 */
__PRIVATE NBOOL NLib_Module_SSH_NClientSSH_CreerProcessus( NClientSSH *this,
	const char *commande )
{
	// Code
	NS32 code;

	// Preparer
	switch( this->m_mode )
	{
		case NMODE_SSH_EXEC:
			// Commande a executer?
			if( commande == NULL )
				// Ignorer
				break;

		case NMODE_SSH_RAW:
			// Obtenir un canal
			while( !( this->m_shell = libssh2_channel_open_session( this->m_sessionSSH ) )
				&& libssh2_session_last_error( this->m_sessionSSH,
					NULL,
					NULL,
					0 ) == LIBSSH2_ERROR_EAGAIN )
				NLib_Temps_Attendre( 1 );

			// Verifier
			if( this->m_shell == NULL )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SSH_CANT_GET_SHELL );

				// Quitter
				return NFALSE;
			}

			// Demander un pseudo terminal
			while( ( code = libssh2_channel_request_pty( this->m_shell,
					"vanilla" ) ) == LIBSSH2_ERROR_EAGAIN )
				NLib_Temps_Attendre( 1 );

			// Verifer
			if( code != LIBSSH2_ERROR_NONE )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SSH_CANT_GET_SHELL );

				// Fermer canal
				libssh2_channel_free( this->m_shell );
				NDISSOCIER_ADRESSE( this->m_shell );

				// Quitter
				return NFALSE;
			}
			break;

		default:
			break;
	}

	switch( this->m_mode )
	{
		case NMODE_SSH_RAW:
			// Obtenir un shell
			if( libssh2_channel_shell( this->m_shell ) != 0 )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SSH_CANT_GET_SHELL );

				// Fermer canal
				libssh2_channel_close( this->m_shell );

				// Quitter
				return NFALSE;
			}

			// Mode non bloquant
			libssh2_channel_set_blocking( this->m_shell,
				0 );
			break;

		case NMODE_SSH_EXEC:
			// Verifier commande
			if( commande == NULL )
				// Ignorer
				break;

			// Executer commande
			while( ( code = libssh2_channel_exec( this->m_shell,
				commande ) ) == LIBSSH2_ERROR_EAGAIN )
				NLib_Module_SSH_NClientSSH_AttendreExec( this,
					NLIB_MODULE_SSH_TIMEOUT_EXEC );

			// Verifier l'execution
			if( code != 0 )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SSH_CANT_GET_SHELL );

				// Detruire canal
				libssh2_channel_free( this->m_shell );

				// Oublier canal
				NDISSOCIER_ADRESSE( this->m_shell );

				// Quitter
				return NFALSE;
			}

			// Mode non bloquant
			libssh2_channel_set_blocking( this->m_shell,
				0 );
			break;

		default:
			break;
	}

	// OK
	return NTRUE;
}

/**
 * Construire client SSH (protected)
 *
 * @param domaine
 * 		Le nom de domaine
 * @param port
 * 		Le port
 * @param authentification
 * 		Les informations d'authentification
 * @param mode
 * 		Le mode ssh
 *
 * @return le client
 */
__ALLOC __PROTECTED NClientSSH *NLib_Module_SSH_NClientSSH_ConstruireInterne( const char *domaine,
	NU32 port,
	__WILLBEOWNED NMethodeAuthentificationSSH *authenfication,
	NTypeClientSSH typeClientSSH,
	NModeSSH mode )
{
	// Sortie
	__OUTPUT NClientSSH *out;

	// Adresse
	SOCKADDR_IN adresse;

	// Resolution
	NListeResolution *resolution;

	// IP
	const NDetailIP *ip;

	// Resultat
	NBOOL resultat;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NClientSSH ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire authentification
		NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( &authenfication );

		// Quitter
		return NULL;
	}

	// Enregistrer domaine
	if( !( out->m_domaine = NLib_Chaine_Dupliquer( domaine ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire authentification
		NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( &authenfication );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_port = port;
	out->m_authentification = authenfication;
	out->m_mode = mode;
	out->m_type = typeClientSSH;

	// Resoudre le nom de domaine
	if( !( resolution = NLib_Module_Reseau_Resolution_NListeResolution_Construire( domaine,
		"22" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DNS_RESOLVE_FAILED );

		// Detruire authentification
		NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( &authenfication );

		// Liberer
		NFREE( out->m_domaine );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Recuperer une IPv4
	if( !( ip = NLib_Module_Reseau_Resolution_NListeResolution_ObtenirIPV4( resolution ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DNS_RESOLVE_FAILED );

		// Detruire authentification
		NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( &authenfication );

		// Liberer
		NLib_Module_Reseau_Resolution_NListeResolution_Detruire( &resolution );
		NFREE( out->m_domaine );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Dupliquer ip
	if( !( out->m_ip = NLib_Chaine_Dupliquer( NLib_Module_Reseau_IP_NDetailIP_ObtenirAdresse( ip ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DNS_RESOLVE_FAILED );

		// Detruire authentification
		NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( &authenfication );

		// Liberer
		NLib_Module_Reseau_Resolution_NListeResolution_Detruire( &resolution );
		NFREE( out->m_domaine );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Detruire resolution
	NLib_Module_Reseau_Resolution_NListeResolution_Detruire( &resolution );

	// Configurer adresse
	adresse.sin_family = AF_INET;
	adresse.sin_port = htons( (NU16)port );
	adresse.sin_addr.FIN_ADRESSE = inet_addr( out->m_ip );

	// Creer socket
	if( ( out->m_socket = (SOCKET)socket( AF_INET,
		SOCK_STREAM,
		0 ) ) == INVALID_SOCKET )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET );

		// Detruire authentification
		NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( &authenfication );

		// Liberer
		NFREE( out->m_ip );
		NFREE( out->m_domaine );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Se connecter
	if( connect( out->m_socket,
		(struct sockaddr*)&adresse,
		sizeof( SOCKADDR_IN ) ) != 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_CONNECTION_FAILED );

		// Fermer socket
		closesocket( out->m_socket );

		// Detruire authentification
		NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( &authenfication );

		// Liberer
		NFREE( out->m_ip );
		NFREE( out->m_domaine );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer session ssh
	out->m_sessionSSH = libssh2_session_init( );

	// On veut une session bloquante
	libssh2_session_set_blocking( out->m_sessionSSH,
		NTRUE );

	// Dire bonjour
	if( libssh2_session_handshake( out->m_sessionSSH,
		out->m_socket ) != 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_CONNECTION_FAILED );

		// Detruire session
		libssh2_session_free( out->m_sessionSSH );

		// Fermer socket
		closesocket( out->m_socket );

		// Detruire authentification
		NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( &authenfication );

		// Liberer
		NFREE( out->m_ip );
		NFREE( out->m_domaine );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// S'authentifier
	switch( NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirType( authenfication ) )
	{
		case NTYPE_METHODE_AUTHENTIFICATION_SSH_PASSWORD:
			// S'authentifier gr�ce au mot de passe
			resultat = (NBOOL)( ( libssh2_userauth_password( out->m_sessionSSH,
				NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirUtilisateur( out->m_authentification ),
				NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirPassword( out->m_authentification ) ) ) == 0 );
			break;
		case NTYPE_METHODE_AUTHENTIFICATION_SSH_RSA_KEY:
			resultat = (NBOOL)( ( libssh2_userauth_publickey_frommemory( out->m_sessionSSH,
				NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirUtilisateur( out->m_authentification ),
				strlen( NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirUtilisateur( out->m_authentification ) ),
				NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirClefPublique( out->m_authentification ),
				strlen( NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirClefPublique( out->m_authentification ) ),
				NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirClefPrivee( out->m_authentification ),
				strlen( NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirClefPrivee( out->m_authentification ) ),
				"" ) ) == 0 );
			break;

		default:
			resultat = NFALSE;
			break;
	}

	// Verifier resultat tentative login
	if( !resultat )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_AUTH_FAILED );

		// Detruire session
		libssh2_session_free( out->m_sessionSSH );

		// Fermer socket
		closesocket( out->m_socket );

		// Detruire authentification
		NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( &authenfication );

		// Liberer
		NFREE( out->m_ip );
		NFREE( out->m_domaine );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer sous objet ssh
	switch( typeClientSSH )
	{
		default:
		case NTYPE_CLIENT_SSH_SSH:
			// Creer shell
			if( !NLib_Module_SSH_NClientSSH_CreerProcessus( out,
				NFALSE ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SSH_CANT_GET_SHELL );

				// Detruire session
				libssh2_session_free( out->m_sessionSSH );

				// Fermer socket
				closesocket( out->m_socket );

				// Detruire authentification
				NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( &authenfication );

				// Liberer
				NFREE( out->m_ip );
				NFREE( out->m_domaine );
				NFREE( out );

				// Quitter
				return NULL;
			}
			break;

		case NTYPE_CLIENT_SSH_SFTP:
			// On veut une session bloquante
			libssh2_session_set_blocking( out->m_sessionSSH,
				NTRUE );
			break;
	}

	// OK
	return out;
}

/**
 * Construire client SSH
 *
 * @param domaine
 * 		Le nom de domaine
 * @param port
 * 		Le port
 * @param authentification
 * 		Les informations d'authentification
 * @param mode
 * 		Le mode ssh souhaite
 *
 * @return le client
 */
__ALLOC NClientSSH *NLib_Module_SSH_NClientSSH_Construire( const char *domaine,
	NU32 port,
	__WILLBEOWNED NMethodeAuthentificationSSH *authentification,
	NModeSSH mode )
{
	// Construire client classique SSH
	return NLib_Module_SSH_NClientSSH_ConstruireInterne( domaine,
		port,
		authentification,
		NTYPE_CLIENT_SSH_SSH,
		mode );
}

/**
 * Detruire client
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_SSH_NClientSSH_Detruire( NClientSSH **this )
{
	switch( (*this)->m_type )
	{
		case NTYPE_CLIENT_SSH_SSH:
			// Fermer shell
			if( ( *this )->m_shell != NULL )
			{
				libssh2_channel_free( (*this)->m_shell );
				NDISSOCIER_ADRESSE( (*this)->m_shell );
			}
			break;

		default:
			break;
	}

	// Detruire session ssh
	libssh2_session_disconnect( (*this)->m_sessionSSH,
		"NLib_Module_SSH_NClientSSH_Detruire( )" );
	libssh2_session_free( (*this)->m_sessionSSH );

	// Detruire authentification
	NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( &(*this)->m_authentification );

	// Fermer socket
	closesocket( (*this)->m_socket );

	// Liberer
	NFREE( (*this)->m_ip );
	NFREE( (*this)->m_domaine );
	NFREE( (*this) );
}

/**
 * Attendre que le flux soit pret en entree (privee)
 *
 * @param this
 * 		Cette instance
 * @param timeout
 * 		Le temps avant timeout (ms)
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE NBOOL NLib_Module_SSH_NClientSSH_AttendreFluxPretEntree( NClientSSH *this,
	NU32 timeout )
{
	// Descripteur poll
	struct pollfd pfds;

	// Preparer l'attente sur la socket
	pfds.fd = this->m_socket;
	pfds.events = POLLIN;
	pfds.revents = 0;

	// Attendre que la socket soit prete
	if( poll( &pfds,
		1,
		timeout <= 0 ?
			-1
			: (NS32)timeout ) <= 0 )
		// Erreur
		return NFALSE;

	// Il y a des donnees?
	return (NBOOL)( pfds.revents & POLLIN );
}

/**
 * Attendre que le flux soit pret en sortie (privee)
 *
 * @param this
 * 		Cette instance
 * @param timeout
 * 		Le temps avant timeout (ms)
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE NBOOL NLib_Module_SSH_NClientSSH_AttendreFluxPretSortie( NClientSSH *this,
	NU32 timeout )
{
	// Descripteur poll
	struct pollfd pfds;

	// Preparer l'attente sur la socket
	pfds.fd = this->m_socket;
	pfds.events = POLLOUT;
	pfds.revents = 0;

	// Attendre que la socket soit prete
	if( poll( &pfds,
		1,
		timeout <= 0 ?
			-1
			: (NS32)timeout ) <= 0 )
		// Erreur
		return NFALSE;

	// Il y a des donnees?
	return (NBOOL)( pfds.revents & POLLIN );
}

/**
 * Lire flux en entree raw (private)
 *
 * @param this
 * 		Cette instance
 * @param timeout
 * 		Le timeout (ms) (0 = pas de timeout)
 *
 * @return les donnees lues
 */
__PRIVATE __ALLOC char *NLib_Module_SSH_NClientSSH_LireFluxRaw( NClientSSH *this,
	NU32 timeout )
{
	// Code retour
	ssize_t code = 0;

	// Taille sortie
	NU32 tailleSortie = 0;

	// Sortie
	__OUTPUT char *out = NULL;

	// Buffer reception
	char buffer[ 4096 ];

	// Verifier le canal
	if( this->m_shell == NULL )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quitter
		return NULL;
	}

	// Il y a des donnees a lire?
	while( NLib_Module_SSH_NClientSSH_AttendreFluxPretEntree( this,
		timeout ) )
		// Lire les donnees
		do
		{
			// Vider le buffer
			memset( buffer,
				0,
				4096 );

			// Lire les donnees
			code = libssh2_channel_read( this->m_shell,
				buffer,
				4096 );

			// On a lu quelque chose?
			if( code > 0 )
			{
				// Ajouter les donnees
				NLib_Memoire_AjouterData2( &out,
					tailleSortie,
					buffer );

				// Incrementer la taille
				tailleSortie += code;
			}
		} while( code > 0 );

	// Verifier le code retour
	if( tailleSortie == 0
		|| ( code < 0
			&& code != LIBSSH2_ERROR_EAGAIN ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SOCKET_RECV );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Lire flux en entree exec (private)
 *
 * @param this
 * 		Cette instance
 *
 * @return les donnees lues
 */
__PRIVATE __ALLOC char *NLib_Module_SSH_NClientSSH_LireFluxExec( NClientSSH *this )
{
	// Code
	NS32 code;

	// Buffer
	char buffer[ 4096 ];

	// Sortie
	char *sortie = NULL;

	// Taille sortie
	NU32 tailleSortie = 0;

	// Verifier le canal
	if( !this->m_shell )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quitter
		return NULL;
	}

	do
	{
		// Vider buffer
		memset( buffer,
			0,
			4096 );

		// Lire donnees
		if( ( code = (NS32)libssh2_channel_read( this->m_shell,
			buffer,
			4096 ) ) > 0 )
		{
			// Ajouter a la sortie
			NLib_Memoire_AjouterData( &sortie,
				tailleSortie,
				buffer,
				(NU32)code );

			// Incrementer taille
			tailleSortie += code;
		}
		else
			if( code < 0
				&& code != LIBSSH2_ERROR_EAGAIN )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SOCKET_RECV );

				// On ne lit plus
				this->m_estExecutionEnCours = NFALSE;

				// Liberer
				NFREE( sortie );

				// Quitter
				return NULL;
			}
	} while( code > 0 );

	// Encore des choses a lire?
	if( code == LIBSSH2_ERROR_EAGAIN )
		// La commande n'a pas fini de s'executer
		this->m_estExecutionEnCours = NTRUE;
	else
		// L'execution est maintenant terminee
		this->m_estExecutionEnCours = NFALSE;

	// On a une sortie?
	if( sortie != NULL )
		// Ajouter \0
		NLib_Memoire_AjouterData( &sortie,
			tailleSortie,
			"\0",
			1 );

	// OK
	return sortie;
}

/**
 * Lire flux en entree
 *
 * @param this
 * 		Cette instance
 * @param timeout
 * 		Le timeout (ms) (0 = pas de timeout) (ignore en mode exec)
 *
 * @return les donnees lues
 */
__ALLOC char *NLib_Module_SSH_NClientSSH_LireFlux( NClientSSH *this,
	NU32 timeout )
{
	switch( this->m_mode )
	{
		case NMODE_SSH_RAW:
			return NLib_Module_SSH_NClientSSH_LireFluxRaw( this,
				timeout );
		case NMODE_SSH_EXEC:
			return NLib_Module_SSH_NClientSSH_LireFluxExec( this );

		default:
			return NULL;
	}
}

/**
 * Flush flux entree
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_SSH_NClientSSH_FlushEntree( NClientSSH *this )
{
	// Buffer
	char *buffer;

	// Lire flux
	while( ( buffer = NLib_Module_SSH_NClientSSH_LireFlux( this,
		NLIB_MODULE_SSH_TEMPS_AVANT_TIMEOUT_FLUSH  ) ) != NULL )
		// Ignorer
		NFREE( buffer );
}

/**
 * Executer une commande brute
 *
 * @param this
 * 		Cette instance
 * @param commande
 * 		La commande a executer
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE NBOOL NLib_Module_SSH_NClientSSH_ExecuterRaw( NClientSSH *this,
	const char *commande )
{
	// Code retour
	ssize_t code = 0;

	// Taille commande
	NU32 tailleCommande;

	// Taille envoyee
	int tailleEnvoye;

	// Attendre de pouvoir ecrire
	NLib_Module_SSH_NClientSSH_AttendreFluxPretSortie( this,
		0 );

	// Calculer taille commande
	tailleCommande = (NU32)strlen( commande );

	// Initialiser a zero
	tailleEnvoye = 0;

	do
	{
		// Inscrire dans le flux
		code = libssh2_channel_write( this->m_shell,
			commande,
			tailleCommande );

		// Il ne s'agit pas d'une erreur?
		if( code >= 0 )
			// Ajouter a la taille totale
			tailleEnvoye += code;
	} while( code > 0
		&& tailleEnvoye < tailleCommande );

	// Verifier l'ecriture
	if( code < 0
		&& code != LIBSSH2_ERROR_EAGAIN )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_SEND );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Executer une commande exec
 *
 * @param this
 * 		Cette instance
 * @param commande
 * 		La commande a executer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_SSH_NClientSSH_ExecuterExec( NClientSSH *this,
	const char *commande )
{
	// Buffer
	char *buffer;

	// Taille commande
	NU32 tailleCommande;

	// Sortie
	__OUTPUT NBOOL sortie;

	// Verifier commande en cours (si on veut skipper ca, flush le flux)
	if( this->m_estExecutionEnCours )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PROCESS_STILL_EXIST );

		// Quitter
		return NFALSE;
	}

	// Verifier etat
	if( this->m_shell != NULL )
		// Detruire canal
		libssh2_channel_free( this->m_shell );


	// Calculer taille commande
	tailleCommande = (NU32)strlen( commande );

	// Allouer memoire
	if( !( buffer = calloc( tailleCommande + 2,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Copier
	memcpy( buffer,
		commande,
		tailleCommande );

	// Nouvelle ligne
	if( buffer[ tailleCommande ] != '\n' )
	{
		buffer[ tailleCommande ] = '\n';
		buffer[ tailleCommande + 1 ] = '\0';
	}

	// Executer
	sortie = NLib_Module_SSH_NClientSSH_CreerProcessus( this,
		buffer );

	// On lance la commande
	if( sortie >= 0 )
		// Enregistrer le lancement
		this->m_estExecutionEnCours = NTRUE;

	// Liberer
	NFREE( buffer );

	// OK?
	return sortie;
}
/**
 * Executer une commande
 *
 * @param this
 * 		Cette instance
 * @param commande
 * 		La commande a executer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_SSH_NClientSSH_Executer( NClientSSH *this,
	const char *commande )
{
	switch( this->m_mode )
	{
		case NMODE_SSH_RAW:
			return NLib_Module_SSH_NClientSSH_ExecuterRaw( this,
				commande );
		case NMODE_SSH_EXEC:
			return NLib_Module_SSH_NClientSSH_ExecuterExec( this,
				commande );

		default:
			return NFALSE;
	}
}

/**
 * L'execution est terminee? (valable pour exec, pour shell, retourne toujours vrai)
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'execution de la commande est terminee
 */
NBOOL NLib_Module_SSH_NClientSSH_EstExecutionTermine( const NClientSSH *this )
{
	switch( this->m_mode )
	{
		case NMODE_SSH_EXEC:
			return (NBOOL)!this->m_estExecutionEnCours;

		default:
			return NTRUE;
	}
}

/**
 * Obtenir session SSH (protected)
 *
 * @param this
 * 		Cette instance
 *
 * @return la session
 */
__PROTECTED LIBSSH2_SESSION *NLib_Module_SSH_NClientSSH_ObtenirSession( const NClientSSH *this )
{
	return this->m_sessionSSH;
}
#endif // NLIB_MODULE_SSH

