#include "../../../../include/NLib/NLib.h"
#include "../../../../include/NLib/Module/SSH/NLib_Module_SSH_NMethodeAuthentificationSSH.h"

// -----------------------------------------------------
// struct NLib::Module::SSH::NMethodeAuthentificationSSH
// -----------------------------------------------------

#ifdef NLIB_MODULE_SSH
/**
 * Construire authentification mot de passe
 *
 * @param utilisateur
 * 		L'utilisateur
 * @param motDePasse
 * 		Le mot de passe
 *
 * @return l'instance
 */
__ALLOC NMethodeAuthentificationSSH *NLib_Module_SSH_NMethodeAuthentificationSSH_Construire( const char *utilisateur,
	const char *motDePasse )
{
	// Sortie
	__OUTPUT NMethodeAuthentificationSSH *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NMethodeAuthentificationSSH ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Dupliquer password
	if( !( out->m_data.m_password = NLib_Chaine_Dupliquer( motDePasse ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Dupliquer utilisateur
	if( !( out->m_utilisateur = NLib_Chaine_Dupliquer( utilisateur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out->m_data.m_password );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer type
	out->m_type = NTYPE_METHODE_AUTHENTIFICATION_SSH_PASSWORD;

	// OK
	return out;
}

/**
 * Construire authentification clef publique/privee
 *
 * @param utilisateur
 * 		L'utilisateur
 * @param clefPublique
 * 		La clef publique
 * @param estClefPubliqueFichier
 * 		Doit-on chercher la clef dans le fichier %clefPublique%?
 * @param clefPrivee
 * 		La clef privee
 * @param estClefPriveeFichier
 * 		Doit-on chercher la clef dans le fichier %clefPrivee%?
 *
 * @return l'instance
 */
__ALLOC NMethodeAuthentificationSSH *NLib_Module_SSH_NMethodeAuthentificationSSH_Construire2( const char *utilisateur,
	__MUSTNOTBENULL const char *clefPublique,
	NBOOL estClefPubliqueFichier,
	__MUSTNOTBENULL const char *clefPrivee,
	NBOOL estClefPriveeFichier )
{
	// Sortie
	__OUTPUT NMethodeAuthentificationSSH *out;

	// Fichier binaire
	NFichierBinaire *fichier;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NMethodeAuthentificationSSH ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Clef privee dans fichier?
	if( estClefPriveeFichier )
	{
		// Ouvrir fichier
		if( !( fichier = NLib_Fichier_NFichierBinaire_ConstruireLecture( clefPrivee ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Recuperer clef privee
		if( !( out->m_data.m_clef.m_clefPrivee = NLib_Fichier_NFichierBinaire_LireTout( fichier ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

			// Liberer
			NLib_Fichier_NFichierBinaire_Detruire( &fichier );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Fermer fichier
		NLib_Fichier_NFichierBinaire_Detruire( &fichier );
	}
	// Clef privee brute?
	else
		// Dupliquer clef privee
		if( !( out->m_data.m_clef.m_clefPrivee  = NLib_Chaine_Dupliquer( clefPrivee ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Clef publique dans fichier?
	if( estClefPubliqueFichier )
	{
		// Ouvrir fichier
		if( !( fichier = NLib_Fichier_NFichierBinaire_ConstruireLecture( clefPublique ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			NFREE( out->m_data.m_clef.m_clefPrivee );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Recuperer clef publique
		if( !( out->m_data.m_clef.m_clefPublique = NLib_Fichier_NFichierBinaire_LireTout( fichier ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

			// Liberer
			NLib_Fichier_NFichierBinaire_Detruire( &fichier );
			NFREE( out->m_data.m_clef.m_clefPrivee );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Fermer fichier
		NLib_Fichier_NFichierBinaire_Detruire( &fichier );
	}
	// Clef publique brute?
	else
		// Dupliquer clef publique
		if( !( out->m_data.m_clef.m_clefPublique  = NLib_Chaine_Dupliquer( clefPublique ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			NFREE( out->m_data.m_clef.m_clefPrivee );
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Dupliquer utilisateur
	if( !( out->m_utilisateur = NLib_Chaine_Dupliquer( utilisateur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out->m_data.m_clef.m_clefPrivee );
		NFREE( out->m_data.m_clef.m_clefPublique );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer type
	out->m_type = NTYPE_METHODE_AUTHENTIFICATION_SSH_RSA_KEY;

	// OK
	return out;
}

/**
 * Detruire instance
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( NMethodeAuthentificationSSH **this )
{
	// Detruire data
	switch( (*this)->m_type )
	{
		case NTYPE_METHODE_AUTHENTIFICATION_SSH_PASSWORD:
			NFREE( (*this)->m_data.m_password );
			break;
		case NTYPE_METHODE_AUTHENTIFICATION_SSH_RSA_KEY:
			NFREE( (*this)->m_data.m_clef.m_clefPrivee );
			NFREE( (*this)->m_data.m_clef.m_clefPublique );
			break;

		default:
			break;
	}

	// Liberer
	NFREE( (*this)->m_utilisateur );
	NFREE( (*this) );
}

/**
 * Obtenir type d'authentification
 *
 * @param this
 * 		Cette instance
 *
 * @return le type d'authentification
 */
NTypeMethodeAuthentificationSSH NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirType( const NMethodeAuthentificationSSH *this )
{
	return this->m_type;
}

/**
 * Obtenir le mot de passe (si type == NTYPE_METHODE_AUTHENTIFICATION_SSH_PASSWORD)
 *
 * @param this
 * 		Cette instance
 *
 * @return le mot de passe
 */
const char *NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirPassword( const NMethodeAuthentificationSSH *this )
{
	return this->m_data.m_password;
}

/**
 * Obtenir clef privee (si type == NTYPE_METHODE_AUTHENTIFICATION_SSH_RSA_KEY)
 *
 * @param this
 * 		Cette instance
 *
 * @return la clef privee
 */
const char *NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirClefPrivee( const NMethodeAuthentificationSSH *this )
{
	return this->m_data.m_clef.m_clefPrivee;
}

/**
 * Obtenir clef publique (si type == NTYPE_METHODE_AUTHENTIFICATION_SSH_RSA_KEY)
 *
 * @param this
 * 		Cette instance
 *
 * @return la clef privee
 */
const char *NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirClefPublique( const NMethodeAuthentificationSSH *this )
{
	return this->m_data.m_clef.m_clefPublique;
}

/**
 * Obtenir utilisateur
 *
 * @param this
 * 		Cette instance
 *
 * @return l'utilisateur
 */
const char *NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirUtilisateur( const NMethodeAuthentificationSSH *this )
{
	return this->m_utilisateur;
}
#endif // NLIB_MODULE_SSH

