#include "../../../../include/NLib/NLib.h"

// ---------------------------
// namespace NLib::Module::SSH
// ---------------------------

#ifdef NLIB_MODULE_SSH
/**
 * Initialiser libssh
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_SSH_Initialiser( void )
{
	// Code retour
	NS32 codeRetour;

	// Initialiser
	if( ( codeRetour = libssh2_init( 0 ) ) != 0 )
	{
		// Notifier
		NOTIFIER_ERREUR_UTILISATEUR( NERREUR_INIT_FAILED,
			__FUNCTION__,
			(NU32)codeRetour );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Detruire libssh
 */
void NLib_Module_SSH_Detruire( void )
{
	libssh2_exit( );
}

/**
 * Download file via SFTP
 *
 * @param hostname
 * 		The remote hostname
 * @param port
 * 		The remote port
 * @param authenticationDetail
 * 		The authentication details
 * @param remoteLocation
 * 		The remote location
 * @param localDestination
 * 		The local file destination
 *
 * @return if the download succeeded
 */
NBOOL NLib_Module_SSH_DownloadFileSFTP( const char *hostname,
	NU32 port,
	NMethodeAuthentificationSSH *authenticationDetail,
	const char *remoteLocation,
	const char *localDestination )
{
	// SFTP client
	NClientSFTP *sftpClient;

	// Build SFTP client
	if( !( sftpClient = NLib_Module_SSH_NClientSFTP_Construire( hostname,
		port,
		authenticationDetail ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Download file
	if( !NLib_Module_SSH_NClientSFTP_RecevoirFichier( sftpClient,
		remoteLocation,
		localDestination,
		NTRUE ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SFTP_CANT_OPEN_FILE );

		// Destroy SFTP client
		NLib_Module_SSH_NClientSFTP_Detruire( &sftpClient );

		// Quit
		return NFALSE;
	}

	// Destroy SFTP client
	NLib_Module_SSH_NClientSFTP_Detruire( &sftpClient );

	// OK
	return NTRUE;
}
#endif // NLIB_MODULE_SSH

