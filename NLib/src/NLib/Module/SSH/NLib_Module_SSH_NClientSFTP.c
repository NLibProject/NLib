#include "../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_SSH

// -------------------------------------
// struct NLib::Module::SSH::NClientSFTP
// -------------------------------------

/**
 * Construire client SSH (private)
 *
 * @param domaine
 * 		Le nom de domaine
 * @param port
 * 		Le port
 * @param authentification
 * 		Les informations d'authentification
 * @param mode
 * 		Le mode ssh
 *
 * @return le client
 */
__ALLOC __PROTECTED NClientSSH *NLib_Module_SSH_NClientSSH_ConstruireInterne( const char *domaine,
	NU32 port,
	__WILLBEOWNED NMethodeAuthentificationSSH *authenfication,
	NTypeClientSSH typeClientSSH,
	NModeSSH mode );

/**
 * Obtenir session SSH (protected)
 *
 * @param this
 * 		Cette instance
 *
 * @return la session
 */
__PROTECTED LIBSSH2_SESSION *NLib_Module_SSH_NClientSSH_ObtenirSession( const NClientSSH* );

/**
 * Construire client
 *
 * @param domaine
 * 		Le nom de domaine auquel se connecter
 * @param port
 * 		Le port auquel se connecter
 * @param authentification
 * 		Les informations d'authentification
 *
 * @return l'instance du client SFTP
 */
__ALLOC NClientSFTP *NLib_Module_SSH_NClientSFTP_Construire( const char *domaine,
	NU32 port,
	__WILLBEOWNED NMethodeAuthentificationSSH *authentification )
{
	// Sortie
	__OUTPUT NClientSFTP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NClientSFTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire
		NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( &authentification );

		// Quitter
		return NULL;
	}

	// Construire client
	if( !( out->m_client = NLib_Module_SSH_NClientSSH_ConstruireInterne( domaine,
		port,
		authentification,
		NTYPE_CLIENT_SSH_SFTP,
		NMODES_SSH ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Obtenir session SSH
	out->m_sessionSSH = NLib_Module_SSH_NClientSSH_ObtenirSession( out->m_client );

	// Creer session SFTP
	if( !( out->m_sessionSFTP = libssh2_sftp_init( out->m_sessionSSH ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SFTP_CANT_GET_SESSION );

		// Detruire client
		NLib_Module_SSH_NClientSSH_Detruire( &out->m_client );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire client
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_SSH_NClientSFTP_Detruire( NClientSFTP **this )
{
	// Fermer session SFTP
	libssh2_sftp_shutdown( (*this)->m_sessionSFTP );

	// Detruire client SSH
	NLib_Module_SSH_NClientSSH_Detruire( &(*this)->m_client );

	// Liberer
	NFREE( (*this) );
}

/**
 * Creer repertoire
 *
 * @param this
 * 		Cette instance
 * @param repertoire
 * 		Le repertoire a creer
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_SSH_NClientSFTP_CreerRepertoire( NClientSFTP *this,
	const char *repertoire )
{
	// Creer
	return (NBOOL)( libssh2_sftp_mkdir( this->m_sessionSFTP,
		repertoire,
		// Droits utilisateur courant
		LIBSSH2_SFTP_S_IRWXU
		// Droits groupe
		| LIBSSH2_SFTP_S_IRGRP | LIBSSH2_SFTP_S_IXGRP
		// Droits autres
		| LIBSSH2_SFTP_S_IROTH | LIBSSH2_SFTP_S_IXOTH ) == LIBSSH2_ERROR_NONE );
}

/**
 * Supprimer repertoire
 *
 * @param this
 * 		Cette instance
 * @param repertoire
 * 		Le repertoire a supprimer
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_SSH_NClientSFTP_SupprimerRepertoire( NClientSFTP *this,
	const char *repertoire )
{
	// Supprimer
	return (NBOOL)( libssh2_sftp_rmdir( this->m_sessionSFTP,
		repertoire ) == LIBSSH2_ERROR_NONE );
}

/**
 * Renommer fichier
 *
 * @param this
 * 		Cette instance
 * @param ancienNom
 * 		Ancien nom fichier
 * @param nouveauNom
 * 		Le nouveau nom fichier
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_SSH_NClientSFTP_Renommer( NClientSFTP *this,
	const char *ancienNom,
	const char *nouveauNom )
{
	// Renommer
	return (NBOOL)( libssh2_sftp_rename( this->m_sessionSFTP,
		ancienNom,
		nouveauNom ) == LIBSSH2_ERROR_NONE );
}

/**
 * Envoyer fichier disque
 *
 * @param this
 * 		Cette instance
 * @param lienFichier
 * 		Le lien vers le fichier local
 * @param destination
 * 		La destination sur le serveur distant
 * @param estAutoriseEcrase
 * 		On autorise a ecraser si le fichier est deja present?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_SSH_NClientSFTP_EnvoyerFichier( NClientSFTP *this,
	const char *lienFichier,
	const char *destination,
	NBOOL estAutoriseEcrase )
{
	// Fichier distant
	LIBSSH2_SFTP_HANDLE *fichierDistant;

	// Fichier
	NFichierBinaire *fichierLocal;

#define TAILLE_BUFFER		4096

	// Buffer
	char buffer[ TAILLE_BUFFER ];

	// Taille lue
	NU32 tailleLueFichierLocal = 0;

	// Taille a lire
	NU32 tailleALire;

	// Ouvrir fichier local
	if( !( fichierLocal = NLib_Fichier_NFichierBinaire_ConstruireLecture( lienFichier ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return NFALSE;
	}

	// Ouvrir fichier distant
	if( !( fichierDistant = libssh2_sftp_open( this->m_sessionSFTP,
		destination,
		(NU32)( LIBSSH2_FXF_WRITE | LIBSSH2_FXF_CREAT | ( estAutoriseEcrase ?
			LIBSSH2_FXF_TRUNC
			: LIBSSH2_FXF_EXCL ) ),
		LIBSSH2_SFTP_S_IRGRP | LIBSSH2_SFTP_S_IRUSR | LIBSSH2_SFTP_S_IROTH | LIBSSH2_SFTP_S_IWUSR ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SFTP_CANT_OPEN_FILE );

		// Fermer fichier
		NLib_Fichier_NFichierBinaire_Detruire( &fichierLocal );

		// Quitter
		return NFALSE;
	}

	// Ecrire
	do
	{
		// Calculer la taille a lire dans le fichier local
		tailleALire = (NU32)( ( NLib_Fichier_NFichierBinaire_ObtenirTaille( fichierLocal ) - tailleLueFichierLocal ) >= TAILLE_BUFFER ?
			TAILLE_BUFFER
			: ( NLib_Fichier_NFichierBinaire_ObtenirTaille( fichierLocal ) - tailleLueFichierLocal ) );

		// Lire
		if( !NLib_Fichier_NFichierBinaire_Lire2( fichierLocal,
			buffer,
			tailleALire ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

			// Fermer fichiers
			libssh2_sftp_close( fichierDistant );
			NLib_Fichier_NFichierBinaire_Detruire( &fichierLocal );

			// Quitter
			return NFALSE;
		}

		// Envoyer
		if( libssh2_sftp_write( fichierDistant,
			buffer,
			tailleALire ) < 0 )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_SOCKET_SEND );

			// Fermer fichiers
			libssh2_sftp_close( fichierDistant );
			NLib_Fichier_NFichierBinaire_Detruire( &fichierLocal );

			// Quitter
			return NFALSE;
		}

		// Incrementer taille lue
		tailleLueFichierLocal += tailleALire;
	} while( !NLib_Fichier_NFichierBinaire_EstEOF( fichierLocal ) );

	// Fermer fichiers
	NLib_Fichier_NFichierBinaire_Detruire( &fichierLocal );
	libssh2_sftp_close( fichierDistant );

#undef TAILLE_BUFFER

	// OK
	return NTRUE;
}

/**
 * Envoyer fichier memoire
 *
 * @param this
 * 		Cette instance
 * @param fichier
 * 		Le fichier local
 * @param tailleFichier
 * 		La taille du fichier local
 * @param destination
 * 		La destination sur le serveur distant
 * @param estAutoriseEcrase
 * 		On autorise a ecraser si le fichier est deja present?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_SSH_NClientSFTP_EnvoyerFichier2( NClientSFTP *this,
	const char *fichier,
	NU64 tailleFichier,
	const char *destination,
	NBOOL estAutoriseEcrase )
{
	// Fichier distant
	LIBSSH2_SFTP_HANDLE *fichierDistant;

#define TAILLE_BUFFER		4096

	// Taille lue
	NU32 tailleLueFichierLocal = 0;

	// Taille a lire
	NU64 tailleALire;

	// Ouvrir fichier distant
	if( !( fichierDistant = libssh2_sftp_open( this->m_sessionSFTP,
		destination,
		(NU32)( LIBSSH2_FXF_WRITE | LIBSSH2_FXF_CREAT | ( estAutoriseEcrase ?
			LIBSSH2_FXF_TRUNC
			: LIBSSH2_FXF_EXCL ) ),
		LIBSSH2_SFTP_S_IRGRP | LIBSSH2_SFTP_S_IRUSR | LIBSSH2_SFTP_S_IROTH | LIBSSH2_SFTP_S_IWUSR ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SFTP_CANT_OPEN_FILE );

		// Quitter
		return NFALSE;
	}

	// Ecrire
	do
	{
		// Calculer la taille a lire dans le fichier local
		tailleALire = ( tailleFichier - tailleLueFichierLocal ) >= TAILLE_BUFFER ?
			TAILLE_BUFFER
			: ( tailleFichier - tailleLueFichierLocal );

		// Envoyer
		if( libssh2_sftp_write( fichierDistant,
			fichier + tailleLueFichierLocal,
			tailleALire ) < 0 )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_SOCKET_SEND );

			// Fermer fichiers
			libssh2_sftp_close( fichierDistant );

			// Quitter
			return NFALSE;
		}

		// Incrementer taille lue
		tailleLueFichierLocal += tailleALire;
	} while( tailleLueFichierLocal < tailleFichier );

	// Fermer fichiers
	libssh2_sftp_close( fichierDistant );

#undef TAILLE_BUFFER

	// OK
	return NTRUE;
}

/**
 * Recevoir un fichier en le mettant sur le disque
 *
 * @param this
 * 		Cette instance
 * @param lienFichier
 * 		Le lien vers le fichier sur le serveur distant
 * @param destination
 * 		Le lien vers la destination sur le disque local
 * @param estAutoriseEcrase
 * 		On est autorise a ecraser le fichier si il est deja present?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_SSH_NClientSFTP_RecevoirFichier( NClientSFTP *this,
	const char *lienFichier,
	const char *destination,
	NBOOL estAutoriseEcrase )
{
	// Fichier distant
	LIBSSH2_SFTP_HANDLE *fichierDistant;

	// Fichier local
	NFichierBinaire *fichierLocal;

#define TAILLE_BUFFER		4096

	// Code
	NS64 code;

	// Buffer
	char buffer[ TAILLE_BUFFER ];

	// Ouvrir fichier distant
	if( !( fichierDistant = libssh2_sftp_open( this->m_sessionSFTP,
		lienFichier,
		LIBSSH2_FXF_READ,
		0 ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SFTP_CANT_OPEN_FILE );

		// Quitter
		return NFALSE;
	}

	// Ouvrir fichier local
	if( !( fichierLocal = NLib_Fichier_NFichierBinaire_ConstruireEcriture( destination,
		estAutoriseEcrase ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OVERRIDE );

		// Fermer fichier distant
		libssh2_sftp_close( fichierDistant );

		// Quitter
		return NFALSE;
	}

	do
	{
		// Lire buffer
		if( ( code = libssh2_sftp_read( fichierDistant,
			buffer,
			TAILLE_BUFFER ) ) > 0 )
			// Ajouter dans fichier
			if( !NLib_Fichier_NFichierBinaire_Ecrire( fichierLocal,
				buffer,
				(NU32)code ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_CANT_WRITE );

				// Fermer fichier local
				NLib_Fichier_NFichierBinaire_Detruire( &fichierLocal );

				// Fermer fichier distant
				libssh2_sftp_close( fichierDistant );

				// Quitter
				return NFALSE;
			}
	} while( code > 0 );

	// Analyser retour
	if( code < 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_RECV );

		// Fermer fichier local
		NLib_Fichier_NFichierBinaire_Detruire( &fichierLocal );

		// Fermer fichier distant
		libssh2_sftp_close( fichierDistant );

		// Quitter
		return NFALSE;
	}

	// Fermer fichier local
	NLib_Fichier_NFichierBinaire_Detruire( &fichierLocal );

	// Fermer fichier distant
	libssh2_sftp_close( fichierDistant );

#undef TAILLE_BUFFER

	// OK
	return NTRUE;
}

/**
 * Recevoir un fichier en le mettant en memoire
 *
 * @param this
 * 		Cette instance
 * @param lienFichier
 * 		Le lien vers le fichier sur le serveur distant
 *
 * @return le fichier
 */
__ALLOC NData *NLib_Module_SSH_NClientSFTP_RecevoirFichier2( NClientSFTP *this,
	const char *lienFichier )
{
	// Fichier distant
	LIBSSH2_SFTP_HANDLE *fichierDistant;

#define TAILLE_BUFFER		4096

	// Code
	NS64 code;

	// Buffer
	char buffer[ TAILLE_BUFFER ];

	// Sortie
	__OUTPUT NData *sortie;

	// Ouvrir fichier distant
	if( !( fichierDistant = libssh2_sftp_open( this->m_sessionSFTP,
		lienFichier,
		LIBSSH2_FXF_READ,
		0 ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SFTP_CANT_OPEN_FILE );

		// Quitter
		return NULL;
	}

	// Construire sortie
	if( !( sortie = NLib_Memoire_NData_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Fermer fichier distant
		libssh2_sftp_close( fichierDistant );

		// Quitter
		return NULL;
	}

	do
	{
		// Lire buffer
		if( ( code = libssh2_sftp_read( fichierDistant,
			buffer,
			TAILLE_BUFFER ) ) > 0 )
			// Ajouter dans sortie
			if( !NLib_Memoire_NData_AjouterData( sortie,
				buffer,
				(NU32)code ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_COPY );

				// Detruire sortie
				NLib_Memoire_NData_Detruire( &sortie );

				// Fermer fichier distant
				libssh2_sftp_close( fichierDistant );

				// Quitter
				return NULL;
			}
	} while( code > 0 );

	// Analyser retour
	if( code < 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_RECV );

		// Detruire sortie
		NLib_Memoire_NData_Detruire( &sortie );

		// Fermer fichier distant
		libssh2_sftp_close( fichierDistant );

		// Quitter
		return NULL;
	}

	// Fermer fichier distant
	libssh2_sftp_close( fichierDistant );

#undef TAILLE_BUFFER

	// OK
	return sortie;
}

/**
 * Lister les fichiers du repertoire distant
 *
 * @param this
 * 		Cette instance
 * @param repertoire
 * 		Le repertoire distant dont il faut lister les fichiers
 *
 * @return la liste des fichiers (NListe<char*>)
 */
__ALLOC NListe *NLib_Module_SSH_NClientSFTP_ListerFichierRepertoire( NClientSFTP *this,
	const char *repertoire )
{
	// Handle repertoire
	LIBSSH2_SFTP_HANDLE *handle;

	// Sortie
	__OUTPUT NListe *sortie;

	// Taille nom fichier
	NS32 tailleNomFichier;

	// Nom fichier
	char nomFichier[ PATH_MAX + 1 ];

	// Copie nom fichier
	char *copieNomFichier;

	// Ouvrir repertoire
	if( !( handle = libssh2_sftp_opendir( this->m_sessionSFTP,
		repertoire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SFTP_CANT_OPEN_FILE );

		// Quitter
		return NULL;
	}

	// Construire sortie
	if( !( sortie = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NLib_Memoire_Liberer ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Fermer handle
		libssh2_sftp_closedir( handle );

		// Quitter
		return NULL;
	}

	// Lire
	do
	{
		// Lire element repertoire
		if( ( tailleNomFichier = libssh2_sftp_readdir( handle,
			nomFichier,
			PATH_MAX,
			NULL ) ) > 0 )
		{
			// Dupliquer nom
			if( !( copieNomFichier = NLib_Chaine_Dupliquer( nomFichier ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_COPY );

				// Fermer repertoire
				libssh2_sftp_closedir( handle );

				// Detruire sortie
				NLib_Memoire_NListe_Detruire( &sortie );

				// Quitter
				return NULL;
			}

			// Ajouter le nom
			if( !( NLib_Memoire_NListe_Ajouter( sortie,
				copieNomFichier ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Fermer repertoire
				libssh2_sftp_closedir( handle );

				// Detruire sortie
				NLib_Memoire_NListe_Detruire( &sortie );

				// Quitter
				return NULL;
			}
		}
	} while( tailleNomFichier > 0 );

	// Fermer repertoire
	libssh2_sftp_closedir( handle );

	// OK
	return sortie;
}

/**
 * Est ce que ce fichier distant est un fichier?
 *
 * @param this
 * 		Cette instance
 * @param fichierDistant
 * 		Le fichier distant
 *
 * @return si le fichier distant est un fichier ou NERREUR si une erreur a eu lieu
 */
NBOOL NLib_Module_SSH_NClientSFTP_EstFichier( NClientSFTP *this,
	const char *fichierDistant )
{
	// Details fichier
	LIBSSH2_SFTP_ATTRIBUTES detailFichier;

	// Recuperer informations fichier
	libssh2_sftp_stat( this->m_sessionSFTP,
		fichierDistant,
		&detailFichier );

	// On a recu des informations sur les permissions
	if( detailFichier.flags & LIBSSH2_SFTP_ATTR_PERMISSIONS )
		return (NBOOL)( !S_ISDIR( detailFichier.permissions ) );

	// Erreur
	return NERREUR;
}

/**
 * Telecharger tous les fichiers d'un repertoire vers un repertoire
 *
 * @param this
 * 		Cette instance
 * @param repertoireDistant
 * 		Le repertoire d'ou telecharger tous les fichiers
 * @param repertoireLocal
 * 		Le repertoire local ou stocker tous les fichiers
 * @param estAutoriseEcrase
 * 		On a le droit d'ecraser le fichier s'il existe deja?
 * @param estRecursif
 * 		La copie est recursive?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_SSH_NClientSFTP_TelechargerTousFichierRepertoire( NClientSFTP *this,
	const char *repertoireDistant,
	const char *repertoireLocal,
	NBOOL estAutoriseEcrase,
	NBOOL estRecursif )
{
	// Lien distant
	char lienDistant[ PATH_MAX + 1 ];

	// Lien local
	char lienLocal[ PATH_MAX + 1 ];

	// Liste fichiers
	NListe *listeFichier;

	// Nom fichier
	const char *nomFichier;

	// Iterateur
	NU32 i;

	// Lister fichiers
	if( !( listeFichier = NLib_Module_SSH_NClientSFTP_ListerFichierRepertoire( this,
		repertoireDistant ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SFTP_CANT_OPEN_FILE );

		// Quitter
		return NFALSE;
	}

	// Creer repertoire local
	NLib_Module_Repertoire_CreerRepertoire( repertoireLocal );

	// Parcourir fichiers distants
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( listeFichier ); i++ )
	{
		// Obtenir nom
		if( !( nomFichier = NLib_Memoire_NListe_ObtenirElementDepuisIndex( listeFichier,
			i ) )
			|| NLib_Chaine_Comparer( nomFichier,
				".",
				NTRUE,
				0 )
			|| NLib_Chaine_Comparer( nomFichier,
				"..",
				NTRUE,
				0 ) )
			// Ignorer erreur
			continue;

		// Creer lien distant
		snprintf( lienDistant,
			PATH_MAX + 1,
			"%s/%s",
			repertoireDistant,
			nomFichier );

		// Creer lien local
		snprintf( lienLocal,
			PATH_MAX + 1,
			"%s/%s",
			repertoireLocal,
			nomFichier );

		// Si c'est un fichier
		if( NLib_Module_SSH_NClientSFTP_EstFichier( this,
			lienDistant ) )
		{
			// Creer
			if( !NLib_Module_SSH_NClientSFTP_RecevoirFichier( this,
				lienDistant,
				lienLocal,
				estAutoriseEcrase ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SFTP_CANT_OPEN_FILE );

				// Detruire liste fichiers
				NLib_Memoire_NListe_Detruire( &listeFichier );

				// Quitter
				return NFALSE;
			}
		}
		else
			if( estRecursif )
				// Repeter
				if( !NLib_Module_SSH_NClientSFTP_TelechargerTousFichierRepertoire( this,
					lienDistant,
					lienLocal,
					estAutoriseEcrase,
					estRecursif ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_SFTP_CANT_OPEN_FILE );

					// Detruire liste fichiers
					NLib_Memoire_NListe_Detruire( &listeFichier );

					// Quitter
					return NFALSE;
				}
	}

	// Detruire liste fichiers
	NLib_Memoire_NListe_Detruire( &listeFichier );

	// OK
	return NTRUE;
}
#endif // NLIB_MODULE_SSH


