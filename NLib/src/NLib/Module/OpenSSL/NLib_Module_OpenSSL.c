#include "../../../../include/NLib/NLib.h"

// -------------------------------
// namespace NLib::Module::OpenSSL
// -------------------------------

#ifdef NLIB_MODULE_OPENSSL

/**
 * Initialiser
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_OpenSSL_Initialiser( void )
{
	// Initialiser
	SSL_load_error_strings( );
	ERR_load_BIO_strings( );
	ERR_load_crypto_strings( );
	OpenSSL_add_all_algorithms( );
	OpenSSL_add_all_ciphers( );
	OpenSSL_add_all_digests( );

	// Initialiser SSL
	return SSL_library_init( ) >= 0;
}

/**
 * Detruire
 */
void NLib_Module_OpenSSL_Detruire( void )
{

}

#endif // NLIB_MODULE_OPENSSL

