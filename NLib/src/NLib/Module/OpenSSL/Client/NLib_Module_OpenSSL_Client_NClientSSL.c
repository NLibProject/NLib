#include "../../../../../include/NLib/NLib.h"

// ------------------------------------------------
// struct NLib::Module::OpenSSL::Client::NClientSSL
// ------------------------------------------------

#if defined( NLIB_MODULE_RESEAU ) \
	&& defined( NLIB_MODULE_OPENSSL )
/** private **/
/* Callback reception packet */
__CALLBACK NBOOL NLib_Module_OpenSSL_Client_NClientSSL_CallbackReceptionPacketInterne( const NPacket *packet,
	void *data )
{
	// Referencer
	NREFERENCER( packet );
	NREFERENCER( data );

	// OK
	return NTRUE;
}

/* Callback deconnexion */
__CALLBACK NBOOL NLib_Module_OpenSSL_Client_NClientSSL_CallbackDeconnexion( void *data )
{
	// Referencer
	NREFERENCER( data );

	// OK
	return NTRUE;
}

/** public **/
/* Construire */
__ALLOC NClientSSL *NLib_Module_OpenSSL_Client_NClientSSL_Construire( const NDetailIP *ip,
	NU32 port,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NPacket*,
		void* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( void* ),
	void *argumentCallback,
	NU32 tempsAvantTimeout )
{
	// Sortie
	__OUTPUT NClientSSL *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NClientSSL ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire le cache packet
	if( !( out->m_cachePacket = NLib_Module_Reseau_Packet_NCachePacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Configurer la socket
	out->m_addr.sin_family = AF_INET;
	out->m_addr.sin_port = htons( (NU16)port );
	out->m_addr.sin_addr.FIN_ADRESSE = inet_addr( NLib_Module_Reseau_IP_NDetailIP_ObtenirAdresse( ip ) );

	// Enregistrer
	out->m_callbackDeconnexion = ( callbackDeconnexion != NULL ) ?
		callbackDeconnexion
		: NLib_Module_OpenSSL_Client_NClientSSL_CallbackDeconnexion;
	out->m_callbackReceptionPacket = ( callbackReceptionPacket != NULL ) ?
		callbackReceptionPacket
		: NLib_Module_OpenSSL_Client_NClientSSL_CallbackReceptionPacketInterne;
	out->m_argumentCallback = argumentCallback;
	out->m_tempsAvantTimeout = tempsAvantTimeout;

	// Zero
	out->m_estConnecte = NFALSE;
	out->m_socket = INVALID_SOCKET;
	out->m_estErreur = NFALSE;

	// Lancer les threads
		// Ca tourne
			out->m_estThreadEnCours = NTRUE;
		// Emission
			if( !( out->m_threadEmission = CreateThread( NULL,
				0,
				(LPTHREAD_START_ROUTINE)NLib_Module_OpenSSL_Client_Thread_Emission,
				out,
				0,
				NULL ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_THREAD );

				// Detruire le cache packets
				NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

				// Quitter
				return NULL;
			}
		// Reception
			if( !( out->m_threadReception = CreateThread( NULL,
				0,
				(LPTHREAD_START_ROUTINE)NLib_Module_OpenSSL_Client_Thread_Reception,
				out,
				0,
				NULL ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_THREAD );

				// Detruire le cache packets
				NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

				// Quitter
				return NULL;
			}

	// OK
	return out;
}

/* Detruire */
void NLib_Module_OpenSSL_Client_NClientSSL_Detruire( NClientSSL **this )
{
	// Deconnecter
	NLib_Module_OpenSSL_Client_NClientSSL_Deconnecter( (*this) );

	// Arreter les threads
	(*this)->m_estThreadEnCours = NFALSE;

	// Attendre la fin des threads
	while( WaitForSingleObject( (*this)->m_threadEmission,
			INFINITE )
		|| WaitForSingleObject( (*this)->m_threadReception,
			INFINITE ) )
		NLib_Temps_Attendre( 1 );

	// Detruire le cache packets
	NLib_Module_Reseau_Packet_NCachePacket_Detruire( &(*this)->m_cachePacket );

	// Liberer
	NFREE( (*this) );
}

/* Connecter */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_Connecter( NClientSSL *this )
{
	// Timeout
#ifdef IS_WINDOWS
	NU32 timeout;
#else // IS_WINDOWS
	struct timeval timeout
#endif // !IS_WINDOWS

	// Verifier etat
	if( NLib_Module_OpenSSL_Client_NClientSSL_EstConnecte( this ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Desactiver erreur
	this->m_estErreur = NFALSE;

	// Creer la socket
	if( ( this->m_socket = socket( AF_INET,
		SOCK_STREAM,
		0 ) ) == INVALID_SOCKET )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET );

		// Quitter
		return NFALSE;
	}

	// Si on a un timeout
	if( this->m_tempsAvantTimeout > 0 )
	{
#ifdef IS_WINDOWS
		// Definir le timeout
		setsockopt( this->m_socket,
			SOL_SOCKET,
			SO_RCVTIMEO,
			(char*)&this->m_tempsAvantTimeout,
			sizeof( NU32 ) );
#else // IS_WINDOWS
		// Definir le timeout
		timeout.tv_sec = this->m_tempsAvantTimeout / 1000;
		timeout.tv_usec = ( this->m_tempsAvantTimeout % 1000 ) * 1000;

		// Definir le timeout
		setsockopt( this->m_socket,
			SOL_SOCKET,
			SO_RCVTIMEO,
			&timeout,
			sizeof( struct timeval ) );
#endif // !IS_WINDOWS
	}

	// Connecter
	if( connect( this->m_socket,
		(struct sockaddr*)&this->m_addr,
		sizeof( SOCKADDR_IN ) ) == INVALID_SOCKET )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_CONNECTION_FAILED );

		// Fermer la socket
		closesocket( this->m_socket );

		// Quitter
		return NFALSE;
	}

	// Creer un nouveau contexte OpenSSL
	if( ( this->m_contexte = SSL_CTX_new( SSLv23_client_method( ) ) ) == NULL )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Fermer la socket
		closesocket( this->m_socket );

		// Quitter
		return NFALSE;
	}

	// Creer l'etat SSL
	if( ( this->m_etatSSL = SSL_new( this->m_contexte ) ) == NULL )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Fermer la socket
		closesocket( this->m_socket );

		// Detruire le contexte OpenSSL
		SSL_CTX_free( this->m_contexte );

		// Quitter
		return NFALSE;
	}

	// Attacher la socket
	SSL_set_fd( this->m_etatSSL,
		this->m_socket );

	// Se connecter en SSL
	if(	SSL_connect( this->m_etatSSL ) != 1 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Fermer la socket
		closesocket( this->m_socket );

		// Detruire l'etat SSL
		SSL_free( this->m_etatSSL );

		// Detruire le contexte OpenSSL
		SSL_CTX_free( this->m_contexte );

		// Quitter
		return NFALSE;
	}

	// Supprimer le timeout
#ifdef IS_WINDOWS
	timeout = 0;
	setsockopt( this->m_socket,
		SOL_SOCKET,
		SO_RCVTIMEO,
		(char*)&timeout,
		sizeof( NU32 ) );
#else // IS_WINDOWS
	memset( &timeout,
		0,
		sizeof( struct timeval ) );
	setsockopt( this->m_socket,
		SOL_SOCKET,
		SO_RCVTIMEO,
		&timeout,
		sizeof( struct timeval ) );
#endif // !IS_WINDOWS

	// OK
	return this->m_estConnecte = NTRUE;
}

/* Deconnecter */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_Deconnecter( NClientSSL *this )
{
	// Verifier connexion
	if( !NLib_Module_OpenSSL_Client_NClientSSL_EstConnecte( this ) )
		return NFALSE;

	// Callback deconnexion
	this->m_callbackDeconnexion( this->m_argumentCallback );

	// Vider le cache packet
	NLib_Module_Reseau_Packet_NCachePacket_Vider( this->m_cachePacket );

	// On est deconnecte
	this->m_estConnecte = NFALSE;

	// Demander la fin d'execution de SSL
	if( SSL_shutdown( this->m_etatSSL ) <= 0 )
		SSL_shutdown( this->m_etatSSL );

	// Detruire l'etat SSL
	SSL_free( this->m_etatSSL );

	// Detruire la socket
	closesocket( this->m_socket );

	// Oublier la socket
	this->m_socket = INVALID_SOCKET;

	// Detruire le contexte OpenSSL
	SSL_CTX_free( this->m_contexte );

	// OK
	return NTRUE;
}

/* Activer erreur */
void NLib_Module_OpenSSL_Client_NClientSSL_ActiverErreur( NClientSSL *this )
{
	this->m_estErreur = NTRUE;
}

/* Ajouter un packet */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_AjouterPacket( NClientSSL *this,
	__WILLBEOWNED NPacket *packet )
{
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacket( this->m_cachePacket,
		packet );
}

NBOOL NLib_Module_OpenSSL_Client_NClientSSL_AjouterPacketCopie( NClientSSL *this,
	const NPacket *packet )
{
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacketCopie( this->m_cachePacket,
		packet );
}

/* Est connecte? */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_EstConnecte( const NClientSSL *this )
{
	return this->m_estConnecte;
}

/* Est erreur? */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_EstErreur( const NClientSSL *this )
{
	return this->m_estErreur;
}

/* Obtenir cache packet */
NCachePacket *NLib_Module_OpenSSL_Client_NClientSSL_ObtenirCachePacket( const NClientSSL *this )
{
	return this->m_cachePacket;
}

/* Obtenir SOCKET */
SOCKET NLib_Module_OpenSSL_Client_NClientSSL_ObtenirSocket( const NClientSSL *this )
{
	return this->m_socket;
}

/* Obtenir etat SSL */
SSL *NLib_Module_OpenSSL_Client_NClientSSL_ObtenirEtatSSL( const NClientSSL *this )
{
	return this->m_etatSSL;
}

/* Obtenir est thread en cours */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_EstThreadEnCours( const NClientSSL *this )
{
	return this->m_estThreadEnCours;
}

/* Obtenir l'argument pour le callback */
void *NLib_Module_OpenSSL_Client_NClientSSL_ObtenirArgumentCallback( const NClientSSL *this )
{
	return this->m_argumentCallback;
}

/* Est timeout autorise? */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_EstTimeoutAutorise( const NClientSSL *this )
{
	return this->m_tempsAvantTimeout > 0;
}

/* Obtenir delai avant timeout */
NU32 NLib_Module_OpenSSL_Client_NClientSSL_ObtenirTempsAvantTimeout( const NClientSSL *this )
{
	return this->m_tempsAvantTimeout;
}
#endif // NLIB_MODULE_RESEAU && NLIB_MODULE_OPENSSL

