#include "../../../../../../include/NLib/NLib.h"

// -----------------------------------------------
// namespace NLib::Module::OpenSSL::Client::Thread
// -----------------------------------------------

#if defined( NLIB_MODULE_RESEAU ) && defined( NLIB_MODULE_OPENSSL )
/* Emission */
void NLib_Module_OpenSSL_Client_Thread_Emission( NClientSSL *client )
{
	// Packet
	NPacket *packet;

	do
	{
		// Delais
		NLib_Temps_Attendre( 1 );

		// Si le client est vivant
		if( NLib_Module_OpenSSL_Client_NClientSSL_EstConnecte( client )
			&& !NLib_Module_OpenSSL_Client_NClientSSL_EstErreur( client ) )
		{
			// Si il y a quelque chose e ecrire dans le flux
			if( NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( NLib_Module_OpenSSL_Client_NClientSSL_ObtenirCachePacket( client ) ) > 0 )
				// Si on peut ecrire dans le flux
				if( NLib_Module_Reseau_Socket_Selection_EstEcritureDisponible( NLib_Module_OpenSSL_Client_NClientSSL_ObtenirSocket( client ) ) )
				{
					// Recuperer le packet
					if( !( packet = NLib_Module_Reseau_Packet_NCachePacket_ObtenirPacket( NLib_Module_OpenSSL_Client_NClientSSL_ObtenirCachePacket( client ) ) ) )
						continue;

					// Ecrire dans le flux
					if( !NLib_Module_OpenSSL_PacketIO_EnvoyerPacket( packet,
						NLib_Module_OpenSSL_Client_NClientSSL_ObtenirEtatSSL( client ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_SEND,
							__FUNCTION__,
							errno );

						// Tuer le client
						NLib_Module_OpenSSL_Client_NClientSSL_ActiverErreur( client );
					}

					// Liberer le packet
					NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

					// Supprimer le packet du cache
					NLib_Module_Reseau_Packet_NCachePacket_SupprimerPacket( NLib_Module_OpenSSL_Client_NClientSSL_ObtenirCachePacket( client ) );
				}
		}
	} while( NLib_Module_OpenSSL_Client_NClientSSL_EstThreadEnCours( client ) );
}

/* Reception */
void NLib_Module_OpenSSL_Client_Thread_Reception( NClientSSL *client )
{
	// Packet
	NPacket *packet;

	do
	{
		// Delais
		NLib_Temps_Attendre( 1 );

		// Si le client est connecte
		if( NLib_Module_OpenSSL_Client_NClientSSL_EstConnecte( client )
			&& !NLib_Module_OpenSSL_Client_NClientSSL_EstErreur( client ) )
		{
			// Si on peut lire dans le flux
			if( NLib_Module_Reseau_Socket_Selection_EstLectureDisponible( NLib_Module_OpenSSL_Client_NClientSSL_ObtenirSocket( client ) ) )
			{
				// Lire dans le flux
				if( !( packet = NLib_Module_OpenSSL_PacketIO_RecevoirPacket( NLib_Module_OpenSSL_Client_NClientSSL_ObtenirEtatSSL( client ),
					NLib_Module_OpenSSL_Client_NClientSSL_EstTimeoutAutorise( client ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_RECV,
						__FUNCTION__,
						errno );

					// Tuer le client
					NLib_Module_OpenSSL_Client_NClientSSL_ActiverErreur( client );
				}
				// Transmettre au callback
				else
					if( !client->m_callbackReceptionPacket( packet,
						NLib_Module_OpenSSL_Client_NClientSSL_ObtenirArgumentCallback( client ) ) )
						NLib_Module_OpenSSL_Client_NClientSSL_ActiverErreur( client );

				// Liberer le packet
				NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );
			}
		}
	} while( NLib_Module_OpenSSL_Client_NClientSSL_EstThreadEnCours( client ) );
}

#endif // NLIB_MODULE_RESEAU && NLIB_MODULE_OPENSSL

