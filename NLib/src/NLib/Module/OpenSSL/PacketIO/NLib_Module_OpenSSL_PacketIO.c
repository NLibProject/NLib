#include "../../../../../include/NLib/NLib.h"

// -----------------------------------------
// namespace NLib::Module::OpenSSL::PacketIO
// -----------------------------------------

#if defined( NLIB_MODULE_OPENSSL ) && defined( NLIB_MODULE_RESEAU )
// Recevoir un packet
__ALLOC NPacket *NLib_Module_OpenSSL_PacketIO_RecevoirPacket( SSL *ssl,
	NBOOL estTimeoutAutorise )
{
	// Code retour recv
	NS32 code;

	// Buffer
	char buffer[ NBUFFER_PACKET ];

	// Packet
	__OUTPUT NPacket *packet;

	// Construire le packet
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Lire
	do
	{
		// Vider buffer
		memset( buffer,
			0,
			NBUFFER_PACKET );

		// Lire
		if( ( code = SSL_read( ssl,
			buffer,
			NBUFFER_PACKET ) ) == INVALID_SOCKET
			&& ( estTimeoutAutorise ?
					( WSAGetLastError( ) != WSAETIMEDOUT )
					: NTRUE ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Liberer
			NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

			// Quitter
			return NULL;
		}
		
		// Copier
		if( code > 0 )
		{
			// Ajouter donnees
			if( !NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				buffer,
				(NU32)code ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_MEMORY );

				// Liberer
				NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

				// Quitter
				return NULL;
			}
		}
		else
			break;

		// Attendre
		NLib_Temps_Attendre( 1 );
	} while( code > 0 );

	// Verifier
	if( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) == 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_RECV );

		// Detruire le packet
		NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

		// Quitter
		return NULL;
	}

	// OK
	return packet;
}

// Envoyer un packet
NBOOL NLib_Module_OpenSSL_PacketIO_EnvoyerPacket( const NPacket *packet,
	SSL *etatSSL )
{
	// Taille a envoyer
	NU32 tailleAEnvoyer;

	// Taille envoyee
	NU32 tailleEnvoyee = 0;
	
	// Code send
	NS32 code;

	// Taille du packet
	NU32 taillePacket;

	// Recuperer la taille packet
	taillePacket = NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet );

	// Envoyer
	while( tailleEnvoyee < taillePacket )
	{
		// Calculer la taille a envoyer
		tailleAEnvoyer = ( ( taillePacket - tailleEnvoyee >= NBUFFER_PACKET ) ?
			NBUFFER_PACKET
			: taillePacket - tailleEnvoyee );

		// Envoyer
		if( ( code = SSL_write( etatSSL,
			NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ) + tailleEnvoyee,
			tailleAEnvoyer ) ) == INVALID_SOCKET
			|| !code )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Quitter
			return NFALSE;
		}

		// Ajouter
		tailleEnvoyee += code;
	}

	// OK
	return NTRUE;
}
#endif // NLIB_MODULE_OPENSSL && NLIB_MODULE_RESEAU

