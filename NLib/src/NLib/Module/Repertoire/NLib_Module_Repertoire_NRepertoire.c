#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------
// NLib::Module::Repertoire::NRepertoire
// -------------------------------------

#ifdef NLIB_MODULE_REPERTOIRE

/* Vider (private) */
NBOOL NLib_Module_Repertoire_NRepertoire_ViderInterne( NRepertoire *this )
{
	// Iterateur
	NU32 i = 0;

	// Verifier etat
	if( !this->m_estListe )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Liberer
		// 2D
			for( ; i < this->m_nbElements; i++ )
				NLib_Module_Repertoire_Element_NElementRepertoire_Detruire( &this->m_elements[ i ] );
		// 1D
			NFREE( this->m_elements );

	// Remettre a zero
	this->m_nbElements = 0;

	// OK
	return !( this->m_estListe = NFALSE );
}

/* Construire l'objet */
__ALLOC NRepertoire *NLib_Module_Repertoire_NRepertoire_Construire( void )
{
	// Sortie
	__OUTPUT NRepertoire *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NRepertoire ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Zero
	memset( out,
		0,
		sizeof( NRepertoire ) );

	// OK
	return out;
}

/* Detruire l'objet */
void NLib_Module_Repertoire_NRepertoire_Detruire( NRepertoire **this )
{
	// Iterateur
	NU32 i = 0;

	// Liberer elements
	for( ; i < (*this)->m_nbElements; i++ )
		NLib_Module_Repertoire_Element_NElementRepertoire_Detruire( &(*this)->m_elements[ i ] );

	// Detruire
	NFREE( (*this)->m_elements );

	// Liberer
	NFREE( *this );
}

/* Lister les fichiers */
NBOOL NLib_Module_Repertoire_NRepertoire_Lister( NRepertoire *this,
	const char *filtre,
	NAttributRepertoire attributs )
{
#ifdef IS_WINDOWS
	// Fichier
		// Donnees
			FIND_DATA donneesFichier;
		// Handle
			NS32 handleFichier;
#else // IS_WINDOWS
	// Fichier
	struct dirent *donneesFichier;

	// Repertoire
	DIR *repertoire;
#endif // !IS_WINDOWS

	// Iterateur
	NU32 i = 0,
		j;

	// Verifier
		// Parametre
			if( !filtre )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

				// Quitter
				return NFALSE;
			}

	// Liberer les anciens fichiers
	NLib_Module_Repertoire_NRepertoire_ViderInterne( this );

	// Obtenir le nombre de fichiers
	if( !( this->m_nbElements = NLib_Module_Repertoire_ObtenirNombreElements( filtre,
		attributs ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_NO_FILE_REPERTORY );

		// Quitter
		return NFALSE;
	}

	// Allouer la memoire
		// 1D
			if( !( this->m_elements = calloc( this->m_nbElements,
				sizeof( NElementRepertoire* ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Quitter
				return NFALSE;
			}

#ifdef IS_WINDOWS
	// Initialiser listage
	if( ( handleFichier = FINDFIRST( filtre,
		&donneesFichier ) ) == NLIB_ERREUR_RECHERCHE_REPERTOIRE )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_GET_ITEM_REPERTORY_FAILED );

		// Liberer
		NFREE( this->m_elements );

		// Quitter
		return NFALSE;
	}

	// Lister les fichiers
	if( attributs != NATTRIBUT_REPERTOIRE_NORMAL )
	{
		do
		{
			// Verifier attributs
			if( donneesFichier.attrib & attributs )
			{
				// Construire element
				if( !( this->m_elements[ i ] = NLib_Module_Repertoire_Element_NElementRepertoire_Construire( donneesFichier.name,
					donneesFichier.size,
					(NAttributRepertoire)donneesFichier.attrib,
					donneesFichier.time_create,
					donneesFichier.time_write,
					donneesFichier.time_access ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Liberer
						// 2D
							for( j = 0; j < i; j++ )
								NLib_Module_Repertoire_Element_NElementRepertoire_Detruire( &this->m_elements[ j ] );
						// 1D
							NFREE( this->m_elements );

					// Fermer listage
					FINDCLOSE( handleFichier );

					// Quitter
					return NFALSE;
				}

				// Fichier suivant
				i++;
			}
		} while( !FINDNEXT( handleFichier,
			&donneesFichier )
			&& i < this->m_nbElements );
	}
	else
		do
		{
			// Construire element
			if( !( this->m_elements[ i ] = NLib_Module_Repertoire_Element_NElementRepertoire_Construire( donneesFichier.name,
				donneesFichier.size,
				(NAttributRepertoire)donneesFichier.attrib,
				donneesFichier.time_create,
				donneesFichier.time_write,
				donneesFichier.time_access ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Liberer
					// 2D
						for( j = 0; j < i; j++ )
							NLib_Module_Repertoire_Element_NElementRepertoire_Detruire( &this->m_elements[ j ] );
					// 1D
						NFREE( this->m_elements );

				// Fermer listage
				FINDCLOSE( handleFichier );

				// Quitter
				return NFALSE;
			}

			// Fichier suivant
			i++;
		} while( !FINDNEXT( handleFichier,
			&donneesFichier )
			&& i < this->m_nbElements );

	// Verifier
	if( this->m_nbElements != i )
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_GET_ITEM_REPERTORY_FAILED );

	// Fermer listage
	FINDCLOSE( handleFichier );
#else // IS_WINDOWS
	// Ouvrir le repertoire
    if( !( repertoire = opendir( "." ) ) )
    {
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// Liberer
		NFREE( this->m_elements );

		// Quitter
		return NFALSE;
    }

    // Lister les fichiers
    while( ( donneesFichier = readdir( repertoire ) ) != NULL
		&& i < this->m_nbElements )
	{
		// Verifier suivant regex && attributs
		if( NLib_Chaine_EstCorrespondRegex( donneesFichier->d_name,
			filtre )
			&& donneesFichier->d_type & attributs )
		{
			// Construire element
			if( !( this->m_elements[ i ] = NLib_Module_Repertoire_Element_NElementRepertoire_Construire( donneesFichier->d_name,
				donneesFichier->d_reclen,
				(NAttributRepertoire)donneesFichier->d_type,
				0,
				0,
				0 ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Liberer
					// 2D
						for( j = 0; j < i; j++ )
							NLib_Module_Repertoire_Element_NElementRepertoire_Detruire( &this->m_elements[ j ] );
					// 1D
						NFREE( this->m_elements );

				// Fermer repertoire
				closedir( repertoire );

				// Quitter
				return NFALSE;
			}

			// Fichier suivant
			i++;
		}
	}

    // Fermer le repertoire
    closedir( repertoire );
#endif // !IS_WINDOWS

	// OK
	return this->m_estListe = NTRUE;
}

/* Obtenir nombre fichiers */
NU32 NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( const NRepertoire *this )
{
	return this->m_nbElements;
}

/* Obtenir fichier */
const NElementRepertoire *NLib_Module_Repertoire_NRepertoire_ObtenirFichier( const NRepertoire *this,
	NU32 index )
{
	// Verifier
		// Etat
			if( !this->m_estListe )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

				// Quitter
				return NULL;
			}
		// Parametre
			if( index >= this->m_nbElements )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

				// Quitter
				return NULL;
			}

	// OK
	return this->m_elements[ index ];
}

/* Est fichier existe? */
NBOOL NLib_Module_Repertoire_NRepertoire_EstFichierExiste( const NRepertoire *this,
	const char *nom )
{
	// Iterateur
	NU32 i = 0;

	// Chercher
	for( ; i < this->m_nbElements; i++ )
		if( NLib_Chaine_Comparer( NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirNom( this->m_elements[ i ] ),
			nom,
			NFALSE,
			0 ) )
			// Trouve
			return NTRUE;

	// Introuvable
	return NFALSE;
}

#endif // NLIB_MODULE_REPERTOIRE

