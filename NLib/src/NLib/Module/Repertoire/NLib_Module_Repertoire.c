#define NLIB_MODULE_REPERTOIRE_INTERNE
#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------
// namespace NLib::Module::Repertoire
// ----------------------------------

#ifdef NLIB_MODULE_REPERTOIRE

/* Initialiser */
NBOOL NLib_Module_Repertoire_Initialiser( void )
{
	// Allouer la memoire
	if( !( m_repertoireInitial = NLib_Module_Repertoire_ObtenirCourant( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_GET_CURRENT_DIRECTORY_FAILED );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Detruire */
void NLib_Module_Repertoire_Detruire( void )
{
	// Liberer
	NFREE( m_repertoireInitial );
}

/**
 * Creer un repertoire
 *
 * @param rep
 *		Le repertoire a creer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Repertoire_CreerRepertoire( const char *rep )
{
#ifdef IS_WINDOWS
	return !_mkdir( rep );
#else // IS_WINDOWS
	return !mkdir( rep,
		0755 );
#endif // !IS_WINDOWS
}

/* Changer repertoire */
NBOOL NLib_Module_Repertoire_Changer( const char *repertoire )
{
	return (NBOOL)( CHDIR( repertoire ) != NLIB_ERREUR_REPERTOIRE );
}

/* Restaurer repertoire initial */
NBOOL NLib_Module_Repertoire_RestaurerInitial( void )
{
	return NLib_Module_Repertoire_Changer( m_repertoireInitial );
}

/* Obtenir repertoire courant */
__ALLOC char *NLib_Module_Repertoire_ObtenirCourant( void )
{
	return GETCWD( NULL,
		0 );
}

/* Obtenir repertoire initial */
const char *NLib_Module_Repertoire_ObtenirInitial( void )
{
	return m_repertoireInitial;
}

/* Obtenir chemin absolu element */
__ALLOC char *NLib_Module_Repertoire_ObtenirCheminAbsolu( const char *e )
{
#ifdef IS_WINDOWS
	return _fullpath( NULL,
		e,
		0 );
#else // IS_WINDOWS
	// Sortie
	char *out;

	// Allouer la memoire
	if( !( out = calloc( PATH_MAX + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Recuperer chemin
	if( !realpath( e,
		out ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
#endif // !IS_WINDOWS
}

/* Compter elements repertoire */
NU32 NLib_Module_Repertoire_ObtenirNombreElements( const char *filtre,
	NAttributRepertoire attribut )
{
	// Sortie
	__OUTPUT NU32 out = 0;

#ifdef IS_WINDOWS
	// Element
	FIND_DATA element;

	// Handle element
	NS32 handle;

	// Recuperer le premier fichier
	if( ( handle = FINDFIRST( filtre,
		&element ) ) == NLIB_ERREUR_RECHERCHE_REPERTOIRE )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_GET_ITEM_REPERTORY_FAILED );

		// Quitter
		return 0;
	}

	// Compter
	if( attribut != NATTRIBUT_REPERTOIRE_NORMAL )
		do
		{
			if( element.attrib & attribut )
				out++;
		} while( !FINDNEXT( handle,
			&element ) );
	else
		do
		{
			out++;
		} while( !FINDNEXT( handle,
			&element ) );

	// Fermer recherche
	FINDCLOSE( handle );
#else // IS_WINDOWS
    // Repertoire
    DIR *repertoire;

    // Entree repertoire
    struct dirent *entree;

    // Ouvrir le repertoire
    if( !( repertoire = opendir( "." ) ) )
    {
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// Quitter
		return 0;
    }

    // Lire les entrees
    while( ( entree = readdir( repertoire ) ) != NULL )
    {
		// Verifier que le nom correspondent au regex
		if( NLib_Chaine_EstCorrespondRegex( entree->d_name,
			filtre )
			&& entree->d_type & attribut )
			out++;
	}

    // Fermer le repertoire
    closedir( repertoire );
#endif // !IS_WINDOWS

	// OK
	return out;
}

/**
 * Il s'agit d'un repertoire?
 *
 * @param lien
 * 		Le lien vers le fichier
 *
 * @return si il s'agit d'un repertoire
 */
NBOOL NLib_Module_Repertoire_EstRepertoire( const char *lien )
{
	// Stat fichier
	struct stat s;

	// Obtenir stats fichier
	if( stat( lien,
		&s ) != 0 )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_FILE_NOT_FOUND );

		// Quitter
		return NFALSE;
	}

	// OK
	return (NBOOL)S_ISDIR( s.st_mode );
}
#endif // NLIB_MODULE_REPERTOIRE

