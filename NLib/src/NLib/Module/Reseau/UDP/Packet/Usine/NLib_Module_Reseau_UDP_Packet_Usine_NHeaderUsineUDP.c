#include "../../../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU

// ----------------------------------------------------------------
// struct NLib::Module::Reseau::UDP::Packet::Usine::NHeaderUsineUDP
// ----------------------------------------------------------------

/**
 * Construire l'header
 *
 * @param data
 *		Les donnees
 * @param tailleData
 *		La taille des donnees
 *
 * @return l'instance
 */
__ALLOC NHeaderUsineUDP *NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_Construire( const char *data,
	NU32 tailleData )
{
	// Sortie
	__OUTPUT NHeaderUsineUDP *out;

	// Curseur
	NU32 curseur = 0;

	// Verifier parametre
	if( tailleData <= NTAILLE_HEADER_USINE_UDP )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NHeaderUsineUDP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Extraire
		// ID trame
			out->m_identifiant = *( (NU64*)( data + curseur ) );
			curseur += sizeof( NU64 );
		// Nombre total de trames
			out->m_nombreTrameTotal = *( (NU32*)( data + curseur ) );
			curseur += sizeof( NU32 );
		// Taille totale des donnees
			out->m_tailleTotaleData = *( (NU32*)( data + curseur ) );
			curseur += sizeof( NU32 );
		// Taille des donnees dans la trame
			out->m_tailleDataTrame = *( (NU32*)( data + curseur ) );
			curseur += sizeof( NU32 );
		// Adresse initiale des donnees
			out->m_adresseInitialeData = *( (NU32*)( data + curseur ) );
			curseur += sizeof( NU32 );
		// Checksum
			out->m_checksum = *( (NU32*)( data + curseur ) );

	// OK
	return out;
}

/**
 * Detruire header
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_Detruire( NHeaderUsineUDP **this )
{
	// Liberer
	NFREE( (*this) );
}

/**
 * Obtenir identifiant
 *
 * @param this
 *		Cette instance
 *
 * @return l'identifiant
 */
NU64 NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirIdentifiant( const NHeaderUsineUDP *this )
{
	return this->m_identifiant;
}

/**
 * Obtenir nombre trame total
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de trame total
 */
NU32 NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirNombreTrameTotal( const NHeaderUsineUDP *this )
{
	return this->m_nombreTrameTotal;
}

/**
 * Obtenir la taille totale des donnees
 *
 * @param this
 *		Cette instance
 *
 * @return la taille totale des donnees
 */
NU32 NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirTailleTotaleData( const NHeaderUsineUDP *this )
{
	return this->m_tailleTotaleData;
}

/**
 * Obtenir la taille des donnees dans la trame
 *
 * @param this
 *		Cette instance
 *
 * @return la taille des donnees dans la trame
 */
NU32 NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirTailleDataTrame( const NHeaderUsineUDP *this )
{
	return this->m_tailleDataTrame;
}

/**
 * Obtenir l'adresse initiale des donnees
 *
 * @param this
 *		Cette instance
 *
 * @return l'adresse initiale des donnees
 */
NU32 NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirAdresseInitialeData( const NHeaderUsineUDP *this )
{
	return this->m_adresseInitialeData;
}

/**
 * Obtenir le checksum
 *
 * @param this
 *		Cette instance
 *
 * @return le checksum
 */
NU32 NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirChecksum( const NHeaderUsineUDP *this )
{
	return this->m_checksum;
}

#endif // NLIB_MODULE_RESEAU

