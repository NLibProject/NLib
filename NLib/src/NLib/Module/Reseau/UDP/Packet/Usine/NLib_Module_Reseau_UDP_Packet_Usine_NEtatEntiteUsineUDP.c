#include "../../../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU

// --------------------------------------------------------------------
// struct NLib::Module::Reseau::UDP::Packet::Usine::NEtatEntiteUsineUDP
// --------------------------------------------------------------------

/**
 * Construire etat
 *
 * @param identifiant
 *		L'identifiant de l'etat
 * @param nombreTrameTotal
 *		Le nombre de trame total attendu
 * @param tailleData
 *		La taille totale des donnees
 * @param adresseExpediteur
 *		L'adresse de l'expediteur
 * @param portExpediteur
 *		Le port de l'expediteur
 *
 * @return l'instance construite
 */
__ALLOC NEtatEntiteUsineUDP *NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_Construire( NU64 identifiant,
	NU32 nombreFrameTotal,
	NU32 tailleData,
	const NDetailIP *adresseExpediteur,
	NU32 portExpediteur )
{
	// Sortie
	__OUTPUT NEtatEntiteUsineUDP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NEtatEntiteUsineUDP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_identifiant = identifiant;
	out->m_portExpediteur = portExpediteur;
	out->m_nombreTrameAttendue = nombreFrameTotal;
	out->m_tailleData = tailleData;

	// Allouer data
	if( !( out->m_data = calloc( out->m_tailleData,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier adresse expediteur
	if( !( out->m_adresseExpediteur = NLib_Module_Reseau_IP_NDetailIP_Construire2( adresseExpediteur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out->m_data );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer timestamp
	out->m_timestamp = NLib_Temps_ObtenirTick( );

	// OK
	return out;
}

/**
 * Detruire etat
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_Detruire( NEtatEntiteUsineUDP **this )
{
	// Detruire adresse expediteur
	NLib_Module_Reseau_IP_NDetailIP_Detruire( &(*this)->m_adresseExpediteur );

	// Liberer
	NFREE( (*this)->m_data );
	NFREE( *this );
}

/**
 * Obtenir identifiant
 *
 * @param this
 *		Cette instance
 *
 * @return l'identifiant
 */
NU64 NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_ObtenirIdentifiant( const NEtatEntiteUsineUDP *this )
{
	return this->m_identifiant;
}

/**
 * Recevoir une trame
 *
 * @param this
 *		Cette instance
 * @param data
 *		Les donnees du packet
 * @param header
 *		L'header du packet
 */
void NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_RecevoirTrame( NEtatEntiteUsineUDP *this,
	const char *data,
	const NHeaderUsineUDP *header )
{
	// Copier les donnees
    memcpy( this->m_data + NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirAdresseInitialeData( header ),
		data,
		NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirTailleDataTrame( header ) );

	// Incrementer le nombre de trames reçues
	this->m_nombreTrameRecue++;

	// Mettre a jour le timestamp
	this->m_timestamp = NLib_Temps_ObtenirTick( );
}

/**
 * A reçu toutes les trames?
 *
 * @param this
 *		Cette instance
 *
 * @return si toutes les trames ont ete reçues
 */
NBOOL NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_EstReceptionTerminee( const NEtatEntiteUsineUDP *this )
{
    return this->m_nombreTrameRecue >= this->m_nombreTrameAttendue;
}

/**
 * Creer le packet
 *
 * @param this
 *		Cette instance
 *
 * @return le packet reconstitue
 */
__ALLOC NPacketUDP *NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_CreerPacket( const NEtatEntiteUsineUDP *this )
{
	// Detail adresse
	NDetailIP *adresse;

	// Construire une copie de l'adresse
	if( !( adresse = NLib_Module_Reseau_IP_NDetailIP_Construire2( this->m_adresseExpediteur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// OK
	return NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire( this->m_data,
		this->m_tailleData,
		adresse,
		this->m_portExpediteur );

}

/**
 * Obtenir timestamp dernier update
 *
 * @param this
 *		Cette instance
 *
 * @return le timestamp du dernier update
 */
NU32 NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_ObtenirTimestampDernierUpdate( const NEtatEntiteUsineUDP *this )
{
    return this->m_timestamp;
}

#endif // NLIB_MODULE_RESEAU
