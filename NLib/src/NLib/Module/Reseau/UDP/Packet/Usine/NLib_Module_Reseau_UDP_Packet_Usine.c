#include "../../../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU

// --------------------------------------------------
// namespace NLib::Module::Reseau::UDP::Packet::Usine
// --------------------------------------------------

/**
 * Creer le buffer pour l'envoi (privee)
 *
 * @param buffer
 *		Le buffer a remplir
 * @param tailleBuffer
 *		La taille du buffer
 * @param tailleAEnvoyer
 *		(Sortie) Taille a envoyer qui sera calculee ici
 * @param tailleHeader
 *		La taille d'un header construit par l'usine
 * @param data
 *		Les donnees a envoyer
 * @param tailleData
 *		La taille des donnees a envoyer
 * @param tailleEnvoyee
 *		La taille envoyee jusqu'ici
 * @param identifiantUnique
 *		L'identifiant du packet
 */
void NLib_Module_Reseau_UDP_Packet_Usine_CreerBufferEnvoiInterne( __OUTPUT char *buffer,
	NU32 tailleBuffer,
	__OUTPUT NU32 *tailleAEnvoyer,
	NU32 tailleHeader,
	const char *data,
	NU32 tailleData,
	NU32 tailleEnvoyee,
	NU64 identifiantUnique )
{
	// Curseur
	NU32 curseur = 0;

	// Checksum
	NU32 checksum = 0;

	// Taille maximum packet
	const NU32 tailleMaximumData = tailleBuffer - tailleHeader;

	// Nombre de trame total
    const NU32 nombreTrame = ( tailleData / tailleMaximumData ) + 1;

	// Vider le buffer
	memset( buffer,
		0,
		tailleBuffer );

	// Calculer la taille a envoyer
	*tailleAEnvoyer = ( ( tailleData - tailleEnvoyee >= tailleMaximumData ) ?
		tailleMaximumData
		: tailleData - tailleEnvoyee );

	// Remplir l'header
		// ID trame
			*( (NU64*)( buffer + curseur ) ) = identifiantUnique;
			curseur += sizeof( NU64 );
		// Nombre de trames a recevoir
			*( (NU32*)( buffer + curseur ) ) = nombreTrame;
			curseur += sizeof( NU32 );
		// Taille totale data
			*( (NU32*)( buffer + curseur ) ) = tailleData;
			curseur += sizeof( NU32 );
		// Taille de la trame contenu dans le buffer
			*( (NU32*)( buffer + curseur ) ) = *tailleAEnvoyer;
			curseur += sizeof( NU32 );
		// Debut de l'adresse où inscrire cette trame
			*( (NU32*)( buffer + curseur ) ) = tailleEnvoyee;
			curseur += sizeof( NU32 );

	// Inscrire les donnees
    memcpy( buffer + tailleHeader,
		data + tailleEnvoyee,
		*tailleAEnvoyer );

	// Calculer le checksum
		// Header
			checksum = NLib_Memoire_CalculerCRC( buffer,
				tailleHeader - sizeof( NU32 ) );
		// Donnees
			checksum += NLib_Memoire_CalculerCRC( buffer + tailleHeader,
				*tailleAEnvoyer );

	// Inscrire le checksum
	*( (NU32*)( buffer + curseur ) ) = checksum;

	// Ajouter taille de l'header a la taille a envoyer
	*tailleAEnvoyer += NTAILLE_HEADER_USINE_UDP;
}

/**
 * Envoyer packet UDP
 *
 * @param packet
 *		Le packet UDP a envoyer
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Reseau_UDP_Packet_Usine_EnvoyerPacketUDP( const NPacketUDP *packet,
	SOCKET socket )
{
	// Taille a envoyer
	NU32 tailleAEnvoyer;

	// Taille envoyee
	NU32 tailleEnvoyee = 0;

	// Code send
	NS32 code;

	// Taille des donnees
	NU32 tailleData;

	// Donnees
	const char *data;

	// Identifiant packet
	NU64 identifiantPacket;

	// Buffer
	char buffer[ NBUFFER_PACKET_UDP ];

	// Recuperer informations packet
	tailleData = NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirPacket( packet ) );
	data = NLib_Module_Reseau_Packet_NPacket_ObtenirData( NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirPacket( packet ) );

	// Creer un identifiant
	identifiantPacket = ( ( (NU64)rand( ) ) << 32 ) | rand( );

	// Envoyer
	while( tailleEnvoyee < tailleData )
	{
		// Attendre que le reseau soit pret
		if( NLib_Module_Reseau_Socket_Selection_EstEcritureDisponible( socket ) )
		{
			// Creer le buffer
			NLib_Module_Reseau_UDP_Packet_Usine_CreerBufferEnvoiInterne( buffer,
				NBUFFER_PACKET_UDP,
				&tailleAEnvoyer,
				NTAILLE_HEADER_USINE_UDP,
				data,
				tailleData,
				tailleEnvoyee,
				identifiantPacket );

			// Envoyer
			if( ( code = sendto( socket,
				buffer,
				tailleAEnvoyer,
				0,
				(struct sockaddr*)NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirAdressage( packet ),
				sizeof( SOCKADDR_IN ) ) ) <= 0 )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

				// Quitter
				return NFALSE;
			}

			// Ajouter
			if( code > NTAILLE_HEADER_USINE_UDP )
				tailleEnvoyee += ( code - NTAILLE_HEADER_USINE_UDP );
		}

		// Attendre
		NLib_Temps_Attendre( 1 );
	}

	// OK
	return NTRUE;
}

/**
 * Verifier l'integrite du packet
 *
 * @param header
 *		L'header parse
 * @param data
 *		Les donnees
 * @param tailleData
 *		La taille des donnees
 *
 * @return si le packet est integre
 */
NBOOL NLib_Module_Reseau_UDP_Packet_Usine_EstPacketIntegre( const NHeaderUsineUDP *this,
	const char *data,
	NU32 tailleData )
{
	// Checksum
	NU32 checksum;

    // Calculer checksum packet
		// Header
			checksum = NLib_Memoire_CalculerCRC( data,
				NTAILLE_HEADER_USINE_UDP - sizeof( NU32 ) );
		// Donnees
			checksum += NLib_Memoire_CalculerCRC( data + NTAILLE_HEADER_USINE_UDP,
				NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirTailleDataTrame( this ) );

	// Verifier checksum
	return NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirChecksum( this ) == checksum;
}

#endif // NLIB_MODULE_RESEAU

