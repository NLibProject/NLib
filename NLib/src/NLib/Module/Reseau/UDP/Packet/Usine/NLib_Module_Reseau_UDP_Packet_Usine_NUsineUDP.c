#include "../../../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU

// ----------------------------------------------------------
// struct NLib::Module::Reseau::UDP::Packet::Usine::NUsineUDP
// ----------------------------------------------------------

/**
 * Construire usine
 *
 * @param callbackReceptionPacket
 *		Le callback de reception
 * @param tempsAvantTimeout
 *		Le temps avant lequel si aucune trame n'est reçue
 *		le packet est transmis au callback
 * @param clientUDP
 *		L'instance du client
 *
 * @return l'instance construite
 */
__ALLOC NUsineUDP *NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_Construire( NBOOL ( *callbackReceptionPacket )( const NPacketUDP*,
		const void *clientUDP ),
	NU32 tempsAvantTimeout,
	const NClientUDP *clientUDP )
{
	// Sortie
	__OUTPUT NUsineUDP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NUsineUDP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_callbackReceptionPacket = callbackReceptionPacket;
	out->m_tempsAvantTimeout = tempsAvantTimeout;
	out->m_clientUDP = clientUDP;

	// Creer liste etats
	if( !( out->m_etats = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire l'usine
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_Detruire( NUsineUDP **this )
{
	// Detruire etats
	NLib_Memoire_NListe_Detruire( &(*this)->m_etats );

	// Liberer
	NFREE( (*this) );
}

/**
 * Obtenir index trame depuis son ID (privee)
 *
 * @param this
 *		Cette instance
 * @param identifiant
 *		L'identifiant recherche
 *
 * @return l'index de la trame ou NERREUR
 */
__MUSTBEPROTECTED NU32 NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_ObtenirIndexTrameDepuisID( NUsineUDP *this,
	NU64 identifiant )
{
	// Sortie
	__OUTPUT NU32 i = 0;

	// Chercher
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_etats ); i++ )
		// Verifier
		if( NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_ObtenirIdentifiant( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_etats,
			i ) ) == identifiant )
			// Trouve
			return i;

	// Introuvable
	return NERREUR;
}

/**
 * Obtenir trame par son ID (privee)
 *
 * @param this
 *		Cette instance
 * @param id
 *		L'identifiant recherche
 *
 * @return la trame si elle existe sinon NULL
 */
__MUSTBEPROTECTED NEtatEntiteUsineUDP *NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_ObtenirTrameDepuisID( NUsineUDP *this,
	NU64 identifiant )
{
	// Resultat recherche
	NU32 index;

	// Chercher
	if( ( index = NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_ObtenirIndexTrameDepuisID( this,
		identifiant ) ) == NERREUR )
		return NULL;

	// OK
	return (NEtatEntiteUsineUDP*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_etats,
		index );
}

/**
 * Ajouter un etat
 *
 * @param this
 *		Cette instance
 * @param etat
 *		L'etat a ajouter
 *
 * @return si l'operation s'est bien deroulee
 */
__MUSTBEPROTECTED NBOOL NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_AjouterEtat( NUsineUDP *this,
	__WILLBEOWNED NEtatEntiteUsineUDP *etat )
{
	return NLib_Memoire_NListe_Ajouter( this->m_etats,
		etat );
}

/**
 * Supprimer un etat depuis son index
 *
 * @param this
 *		Cette instance
 * @param index
 * 		L'index de l'etat a supprimer
 *
 * @return si l'operation s'est bien passee
 */
__MUSTBEPROTECTED NBOOL NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_SupprimerEtatDepuisIndex( NUsineUDP *this,
	NU32 index )
{
	return NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_etats,
		index );
}

/**
 * Supprimer un etat
 *
 * @param this
 *		Cette instance
 * @param etat
 * 		L'etat a supprimer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_SupprimerEtat( NUsineUDP *this,
	const NEtatEntiteUsineUDP *etat )
{
    return NLib_Memoire_NListe_SupprimerDepuisAdresse( this->m_etats,
		(NEtatEntiteUsineUDP*)etat );
}

/**
 * Recevoir trame
 *
 * @param this
 *		Cette instance
 * @param packet
 *		Le packet reçu
 *
 * @return si l'operation a reussi
 */
__WILLLOCK __WILLUNLOCK NBOOL NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_RecevoirTrame( NUsineUDP *this,
	const NPacketUDP *packet )
{
	// Etat trame
	NEtatEntiteUsineUDP *etat;

	// Header
	NHeaderUsineUDP *header;

	// Donnees
	NU32 tailleData;
	char *data;

	// Recuperer informations packet
	tailleData = NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirPacket( packet ) );
	data = (char*)NLib_Module_Reseau_Packet_NPacket_ObtenirData( NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirPacket( packet ) );

	// Extraire les informations de l'header
    if( !( header = NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_Construire( data,
		tailleData ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Verifier l'integrite du packet
	if( !NLib_Module_Reseau_UDP_Packet_Usine_EstPacketIntegre( header,
		data,
		tailleData ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CRC );

		// Detruire header
		NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_Detruire( &header );

		// Quitter
		return NFALSE;
	}

	// Proteger etats
	NLib_Memoire_NListe_ActiverProtection( this->m_etats );

	// Obtenir etat
	if( !( etat = NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_ObtenirTrameDepuisID( this,
		NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirIdentifiant( header ) ) ) )
	{
		// Creer l'etat
		if( !( etat = NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_Construire( NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirIdentifiant( header ),
			NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirNombreTrameTotal( header ),
			NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirTailleTotaleData( header ),
			NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirDetailIP( packet ),
			NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirPort( packet ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Unlock le mutex
			NLib_Memoire_NListe_DesactiverProtection( this->m_etats );

			// Detruire header
			NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_Detruire( &header );

			// Quitter
			return NFALSE;
		}

		// Ajouter l'etat
		if( !NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_AjouterEtat( this,
			etat ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Unlock le mutex
			NLib_Memoire_NListe_DesactiverProtection( this->m_etats );

			// Detruire l'etat
			NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_Detruire( &etat );

			// Detruire header
			NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_Detruire( &header );

			// Quitter
			return NFALSE;
		}
	}

	// Recevoir la trame
	NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_RecevoirTrame( etat,
		data + NTAILLE_HEADER_USINE_UDP,
		header );

	// Unlock le mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_etats );

	// Detruire l'header
	NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_Detruire( &header );

	// OK
	return NTRUE;
}

/**
 * Mettre a jour l'usine
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation a reussi
 */
__WILLLOCK __WILLUNLOCK NBOOL NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_Update( NUsineUDP *this )
{
	// Iterateur
	NU32 i = 0;

	// Packet UDP
	NPacketUDP *packet;

	// Lock le mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_etats );

	// Verifier l'etat des packets
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_etats ); i++ )
		// Si le packet est pret/timeout
		if( NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_EstReceptionTerminee( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_etats,
			i ) )
			|| NLib_Temps_ObtenirTick( ) - NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_ObtenirTimestampDernierUpdate( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_etats,
				i ) ) >= this->m_tempsAvantTimeout )
		{
			// Construire le packet
			if( !( packet = NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_CreerPacket( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_etats,
				i ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Continuer
				continue;
			}

			// Envoyer le packet au callback
			if( !this->m_callbackReceptionPacket( packet,
				this->m_clientUDP ) )
				NLib_Module_Reseau_UDP_Client_NClientUDP_ActiverErreur( (NClientUDP*)this->m_clientUDP );

			// Detruire le packet
			NLib_Module_Reseau_UDP_Packet_NPacketUDP_Detruire( &packet );

			// Supprimer le packet
			NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_SupprimerEtatDepuisIndex( this,
				i );
		}

	// Unlock le mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_etats );

	// OK
	return NTRUE;
}

#endif // NLIB_MODULE_RESEAU

