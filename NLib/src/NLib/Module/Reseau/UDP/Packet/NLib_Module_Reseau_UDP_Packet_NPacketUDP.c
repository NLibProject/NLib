#include "../../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU

// ----------------------------------------------------
// struct NLib::Module::Reseau::UDP::Packet::NPacketUDP
// ----------------------------------------------------

/**
 * Construire un packet
 *
 * @param data
 *		Les donnees
 * @param taille
 *		La taille des donnees
 * @param adressage
 *		L'adressage
 *
 * @return l'instance
 */
__ALLOC NPacketUDP *NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire( const char *data,
	NU32 taille,
	__WILLBEOWNED NDetailIP *adressage,
	NU32 port )
{
	// Sortie
	__OUTPUT NPacketUDP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NPacketUDP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire le detail IP
		NLib_Module_Reseau_IP_NDetailIP_Detruire( &adressage );

		// Quitter
		return NULL;
	}

	// Construire le packet
	if( !( out->m_packet = NLib_Module_Reseau_Packet_NPacket_Construire3( data,
		taille ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire le detail IP
		NLib_Module_Reseau_IP_NDetailIP_Detruire( &adressage );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_detailIP = adressage;

	// Creer le contexte
	out->m_contexteAdressage.sin_family = PF_INET;
	out->m_contexteAdressage.sin_port = htons( (NU16)port );
	out->m_contexteAdressage.sin_addr.FIN_ADRESSE = inet_addr( NLib_Module_Reseau_IP_NDetailIP_ObtenirAdresse( adressage ) );

	// OK
	return out;
}

/**
 * Constuire un packet
 *
 * @param data
 *		Les donnees
 * @param taille
 *		La taille des donnees
 * @param contexteAdressage
 *		Le contexte d'adressage
 *
 * @return l'instance
 */
__ALLOC NPacketUDP *NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire2( const char *data,
	NU32 taille,
	const SOCKADDR_IN *contexteAdressage )
{
	// Sortie
	__OUTPUT NPacketUDP *out;

	// IP
	char ip[ INET_ADDRSTRLEN ] = { 0, };

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NPacketUDP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire le packet
	if( !( out->m_packet = NLib_Module_Reseau_Packet_NPacket_Construire3( data,
		taille ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Obtenir adresse
	inet_ntop( AF_INET,
		(void*)contexteAdressage,
		ip,
		INET_ADDRSTRLEN * sizeof( char ) );

	// Enregistrer
	if( !( out->m_detailIP = NLib_Module_Reseau_IP_NDetailIP_Construire( NPROTOCOLE_IP_IPV4,
		ip ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NLib_Module_Reseau_Packet_NPacket_Detruire( &out->m_packet );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer le contexte
	out->m_contexteAdressage = *contexteAdressage;

	// OK
	return out;
}

/**
 * Construire un packet
 *
 * @param packet
 *		Le packet
 * @param ip
 *		Le detail concernant le destinataire
 * @param port
 *		Le port a considerer
 *
 * @return l'instance
 */
__ALLOC NPacketUDP *NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire3( __WILLBEOWNED NPacket *packet,
	__WILLBEOWNED NDetailIP *ip,
	NU32 port )
{
	// Sortie
	__OUTPUT NPacketUDP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NPacketUDP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire
		NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );
		NLib_Module_Reseau_IP_NDetailIP_Detruire( &ip );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_detailIP = ip;
	out->m_packet = packet;

	// Creer le contexte
	out->m_contexteAdressage.sin_family = PF_INET;
	out->m_contexteAdressage.sin_port = htons( (NU16)port );
	out->m_contexteAdressage.sin_addr.FIN_ADRESSE = inet_addr( NLib_Module_Reseau_IP_NDetailIP_ObtenirAdresse( ip ) );

	// OK
	return out;
}

/**
 * Construire un packet
 *
 * @param src
 *		La source
 *
 * @return l'instance
 */
__ALLOC NPacketUDP *NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire4( const NPacketUDP *src )
{
	// Sortie
	__OUTPUT NPacketUDP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NPacketUDP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( &out->m_contexteAdressage,
		&src->m_contexteAdressage,
		sizeof( SOCKADDR_IN ) );

	// Allouer IP
	if( !( out->m_detailIP = NLib_Module_Reseau_IP_NDetailIP_Construire2( src->m_detailIP ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier le packet
	if( !( out->m_packet = NLib_Module_Reseau_Packet_NPacket_Construire2( src->m_packet ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire detail IP
		NLib_Module_Reseau_IP_NDetailIP_Detruire( &out->m_detailIP );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire l'instance
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_UDP_Packet_NPacketUDP_Detruire( NPacketUDP **this )
{
	// Detruire
	NLib_Module_Reseau_IP_NDetailIP_Detruire( &(*this)->m_detailIP );
	NLib_Module_Reseau_Packet_NPacket_Detruire( &(*this)->m_packet );

	// Liberer
	NFREE( *this );
}

/**
 * Obtenir le packet
 *
 * @param this
 *		Cette instance
 *
 * @return le packet
 */
const NPacket *NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirPacket( const NPacketUDP *this )
{
	return this->m_packet;
}

/**
 * Obtenir detail IP
 *
 * @param this
 *		Cette instance
 *
 * @return le detail IP
 */
const NDetailIP *NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirDetailIP( const NPacketUDP *this )
{
	return this->m_detailIP;
}

/**
 * Obtenir port
 *
 * @param this
 * 		Cette instance
 *
 * @return le port
 */
NU32 NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirPort( const NPacketUDP *this )
{
    return ntohs( this->m_contexteAdressage.sin_port );
}

/**
 * Obtenir adressage
 *
 * @param this
 *		Cette instance
 *
 * @return l'adressage
 */
const SOCKADDR_IN *NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirAdressage( const NPacketUDP *this )
{
	return &this->m_contexteAdressage;
}

#endif // NLIB_MODULE_RESEAU

