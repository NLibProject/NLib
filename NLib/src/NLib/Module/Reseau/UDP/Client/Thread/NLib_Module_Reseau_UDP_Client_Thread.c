#include "../../../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU
// ---------------------------------------------------
// namespace NLib::Module::Reseau::UDP::Client::Thread
// ---------------------------------------------------

/**
 * Thread reception
 *
 * @param client
 *		Le client
 *
 * @return OK?
 */
NBOOL NLib_Module_Reseau_UDP_Client_Thread_Reception( NClientUDP *client )
{
	// Code retour
	NBOOL codeRetour = NTRUE;

	// Packet
	NPacketUDP *packet;

	// Usine UDP
	NUsineUDP *usine = NULL;

	// Usine active?
	if( NLib_Module_Reseau_UDP_Client_NClientUDP_EstUsineActive( client ) )
		// Recuperer l'usine
		if( !( usine = (NUsineUDP*)NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirUsine( client ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_INIT_UNCOMPLETE );

			// Quitter
			return NFALSE;
		}

	do
	{
		// Si le client est connecte
		if( !NLib_Module_Reseau_UDP_Client_NClientUDP_EstErreur( client ) )
		{
			// Si on peut lire dans le flux
			if( NLib_Module_Reseau_Socket_Selection_EstLectureDisponible( NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirSocket( client ) ) )
			{
				// Lire dans le flux
				if( !( packet = NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketUDP( NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirSocket( client ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_RECV,
						__FUNCTION__,
						errno );

					// Tuer le client
					NLib_Module_Reseau_UDP_Client_NClientUDP_ActiverErreur( client );
				}
				// Traiter le packet reeu
				else
				{
					// Si on utilise l'usine
					if( NLib_Module_Reseau_UDP_Client_NClientUDP_EstUsineActive( client ) )
					{
						// Transmettre le packet a l'usine
						if( !NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_RecevoirTrame( usine,
							packet ) )
							// Activer le flag erreur
							NLib_Module_Reseau_UDP_Client_NClientUDP_ActiverErreur( client );
					}
					// Pas d'usine
					else
						// Transmettre le packet au callback
						if( !client->m_callbackReceptionPacket( packet,
							client ) )
							// Activer le flag erreur
							NLib_Module_Reseau_UDP_Client_NClientUDP_ActiverErreur( client );
				}

				// Liberer le packet
				NLib_Module_Reseau_UDP_Packet_NPacketUDP_Detruire( &packet );
			}
		}

		// Si l'usine est active
		if( NLib_Module_Reseau_UDP_Client_NClientUDP_EstUsineActive( client ) )
			// Mettre a jour l'usine
			NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_Update( usine );

		// Attendre
		NLib_Temps_Attendre( 1 );
	} while( NLib_Module_Reseau_UDP_Client_NClientUDP_EstEnCours( client ) );

	// OK?
	return codeRetour;
}

/**
 * Thread emission
 *
 * @param client
 *		Le client
 *
 * @return OK?
 */
NBOOL NLib_Module_Reseau_UDP_Client_Thread_Emission( NClientUDP *client )
{
	// Code retour
	NBOOL codeRetour = NTRUE;

	// Packet UDP
	NPacketUDP *packet;

	do
	{
		// Si il y a quelque chose a ecrire dans le flux
		if( !NLib_Module_Reseau_UDP_Client_NClientUDP_EstErreur( client ) )
		{
			if( NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirCachePacket( client ) ) > 0 )
			{
				// Recuperer le packet
				if( !( packet = (NPacketUDP*)NLib_Module_Reseau_Packet_NCachePacket_ObtenirPacket( NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirCachePacket( client ) ) ) )
					continue;

				// Ecrire dans le flux
				if( !NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketUDP( packet,
					NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirSocket( client ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_SEND,
						__FUNCTION__,
						errno );

					// Tuer le client
					NLib_Module_Reseau_UDP_Client_NClientUDP_ActiverErreur( client );
				}

				// Detruire le packet
				NLib_Module_Reseau_UDP_Packet_NPacketUDP_Detruire( &packet );

				// Supprimer le packet du cache
				NLib_Module_Reseau_Packet_NCachePacket_SupprimerPacket( NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirCachePacket( client ) );
			}
		}

		// Attendre
		NLib_Temps_Attendre( 1 );
	} while( NLib_Module_Reseau_UDP_Client_NClientUDP_EstEnCours( client ) );

	// OK?
	return codeRetour;
}

#endif // NLIB_MODULE_RESEAU

