#include "../../../../../../include/NLib/NLib.h"

// ----------------------------------------------------
// struct NLib::Module::Reseau::UDP::Client::NClientUDP
// ----------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

/**
 * Construire le client (privee)
 *
 * @param port
 *		Le port d'ecoute
 * @param callbackReception
 *		Le callback de reception
 * @param tempsAvantTimeoutReception
 *		Temps avant le timeout pour la reception
 *
 * @return l'instance client UDP
 */
__ALLOC NClientUDP *NLib_Module_Reseau_UDP_Client_NClientUDP_ConstruireInterne( NU32 port,
	NBOOL ( *callbackReception )( const NPacketUDP*,
		const NClientUDP* ),
	NBOOL estModeReceptionActive,
	NBOOL estUtiliseUsine,
	NU32 tempsAvantTimeoutReception )
{
	// Sortie
	__OUTPUT NClientUDP *out;

	// Contexte d'adressage
	SOCKADDR_IN contexteAdressage;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NClientUDP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_estUtiliseUsine = estUtiliseUsine;
	out->m_estModeReceptionActive = estModeReceptionActive;
	out->m_callbackReceptionPacket = ( NBOOL ( ___cdecl * )( const NPacketUDP*,
		const void * ) )callbackReception;

	// Creer le cache packets
	if( !( out->m_cachePacket = NLib_Module_Reseau_Packet_NCachePacket_Construire( NTYPE_CACHE_PACKET_UDP ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer la socket
	if( ( out->m_socket = socket( PF_INET,
		SOCK_DGRAM,
		0 ) ) == INVALID_SOCKET )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET );

		// Detruire le cache packets
		NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer l'adressage
	contexteAdressage.sin_family = PF_INET;
	contexteAdressage.sin_port = htons( (NU16)port );
	contexteAdressage.sin_addr.FIN_ADRESSE = htonl( INADDR_ANY );

	// Mode reception actif?
	if( estModeReceptionActive )
	{
		// Bind la socket
		if( ( bind( out->m_socket,
			(struct sockaddr*)&contexteAdressage,
			sizeof( SOCKADDR_IN ) ) ) == INVALID_SOCKET )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_SOCKET_BIND );

			// Fermer la socket
			closesocket( out->m_socket );

			// Detruire le cache packets
			NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Creer l'usine
		if( estUtiliseUsine )
			if( !( out->m_usine = NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_Construire( ( NBOOL ( * )( const struct NPacketUDP*,
					const void* ) )callbackReception,
				tempsAvantTimeoutReception,
				out ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SOCKET_BIND );

				// Fermer la socket
				closesocket( out->m_socket );

				// Detruire le cache packets
				NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

				// Liberer
				NFREE( out );

				// Quitter
				return NULL;
			}
	}

	// Lancer le client
	out->m_estEnCours = NTRUE;
	out->m_estErreur = NFALSE;

	// Lancer les threads
		// Emission
			if( !( out->m_threadEmission = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl *)( void* ))NLib_Module_Reseau_UDP_Client_Thread_Emission,
				out,
				&out->m_estEnCours ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_THREAD );

				// Mode reception?
				if( out->m_estModeReceptionActive )
					// Detruire l'usine
					NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_Detruire( &out->m_usine );

				// Fermer socket
				closesocket( out->m_socket );

				// Detruire le cache packets
				NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

				// Liberer
				NFREE( out );

				// Quitter
				return NULL;
			}
		// Mode reception actif?
			if( estModeReceptionActive )
				// Creer le thread reception
				if( !( out->m_threadReception = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl *)( void* ))NLib_Module_Reseau_UDP_Client_Thread_Reception,
					out,
					&out->m_estEnCours ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_THREAD );

					// Arreter le thread emission
					NLib_Thread_NThread_Detruire( &out->m_threadEmission );

					// Mode reception?
					if( out->m_estModeReceptionActive )
						// Detruire l'usine
						NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_Detruire( &out->m_usine );

					// Fermer socket
					closesocket( out->m_socket );

					// Detruire le cache packets
					NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

					// Liberer
					NFREE( out );

					// Quitter
					return NULL;
				}

	// OK
	return out;
}

/**
 * Construire le client (mode emission/reception)
 *
 * @param port
 *		Le port d'ecoute
 * @param callbackReception
 *		Le callback de reception
 * @param timeoutReception
 *		Le timeout en reception
 *
 * @return l'instance
 */
__ALLOC NClientUDP *NLib_Module_Reseau_UDP_Client_NClientUDP_Construire( NU32 port,
	NBOOL ( *callbackReception )( const NPacketUDP*,
		const NClientUDP* ),
		NBOOL estUtiliseUsine,
		NU32 timeoutReception )
{
	// Construire le client
	return NLib_Module_Reseau_UDP_Client_NClientUDP_ConstruireInterne( port,
		callbackReception,
		NTRUE,
		estUtiliseUsine,
		timeoutReception );
}

/**
 * Construire le client (uniquement en mode envoi)
 *
 * @return l'instance du client
 */
__ALLOC NClientUDP *NLib_Module_Reseau_UDP_Client_NClientUDP_Construire2( void )
{
	// Construire le client
	return NLib_Module_Reseau_UDP_Client_NClientUDP_ConstruireInterne( 0,
		NULL,
		NFALSE,
		NFALSE,
		0 );
}

/**
 * Detruire le client
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_UDP_Client_NClientUDP_Detruire( NClientUDP **this )
{
	// Mode reception desactive?
	if( (*this)->m_estModeReceptionActive )
	{
		// Detruire thread reception
		NLib_Thread_NThread_Detruire( &(*this)->m_threadReception );

		// Detruire l'usine
		if( (*this)->m_estUtiliseUsine )
			NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_Detruire( &(*this)->m_usine );
	}

	// Detruire thread emission
	NLib_Thread_NThread_Detruire( &(*this)->m_threadEmission );

	// Detruire le cache packets
	NLib_Module_Reseau_Packet_NCachePacket_Detruire( &(*this)->m_cachePacket );

	// Fermer la socket
	closesocket( (*this)->m_socket );

	// Liberer
	NFREE( (*this) );
}

/**
 * Activer erreur
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_UDP_Client_NClientUDP_ActiverErreur( NClientUDP *this )
{
	this->m_estErreur = NTRUE;
}

/**
 * Est client en cours?
 *
 * @param this
 *		Cette instance
 *
 * @return si le client est en cours
 */
NBOOL NLib_Module_Reseau_UDP_Client_NClientUDP_EstEnCours( const NClientUDP *this )
{
	return this->m_estEnCours;
}

/**
 * Est erreur?
 *
 * @param this
 *		Cette instance
 *
 * @return si il y a une erreur
 */
NBOOL NLib_Module_Reseau_UDP_Client_NClientUDP_EstErreur( const NClientUDP *this )
{
	return this->m_estErreur;
}

/**
 * Obtenir le cache packets
 *
 * @param this
 *		Cette instance
 *
 * @return le cache packets
 */
NCachePacket *NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirCachePacket( const NClientUDP *this )
{
	return this->m_cachePacket;
}

/**
 * Obtenir la socket
 *
 * @param this
 *		Cette instance
 *
 * @return la socket
 */
SOCKET NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirSocket( const NClientUDP *this )
{
	return this->m_socket;
}

/**
 * Definir les donnees client
 *
 * @param this
 *		Cette instance
 * @param donnee
 *		Les donnees a definir
 */
void NLib_Module_Reseau_UDP_Client_NClientUDP_DefinirDonneeClient( NClientUDP *this,
	void *donnee )
{
	this->m_donneeClient = donnee;
}

/**
 * Obtenir les donnees client
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees client
 */
void *NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirDonneeClient( const NClientUDP *this )
{
	return this->m_donneeClient;
}

/**
 * Ajouter un packet
 *
 * @param this
 *		Cette instance
 * @param packet
 *		Le packet a ajouter
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Reseau_UDP_Client_NClientUDP_AjouterPacket( NClientUDP *this,
	__WILLBEOWNED NPacketUDP *packet )
{
	// Ajouter le packet
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacket( this->m_cachePacket,
		(NPacket*)packet );
}

/**
 * Ajouter un packet
 *
 * @param this
 *		Cette instance
 * @param packet
 *		Le packet a ajouter
 * @param ip
 *		Le detail concernant le destinataire
 * @param port
 *		Le port sur lequel envoyer
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Reseau_UDP_Client_NClientUDP_AjouterPacket2( NClientUDP *this,
	__WILLBEOWNED NPacket *packet,
	__WILLBEOWNED NDetailIP *ip,
	NU32 port )
{
	// Packet a ajouter
	NPacketUDP *packetUDP;

	// Creer le packet
	if( !( packetUDP = NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire3( packet,
		ip,
		port ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Ajouter le packet
	return NLib_Module_Reseau_UDP_Client_NClientUDP_AjouterPacket( this,
		packetUDP );
}

/**
 * Ajouter un packet
 *
 * @param this
 *		Cette instance
 * @param packet
 *		Le packet a ajouter
 * @param ip
 *		L'ip du destinataire
 * @param protocole
 *		IPv4 ou IPv6
 * @param port
 *		Le port de destination
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Reseau_UDP_Client_NClientUDP_AjouterPacket3( NClientUDP *this,
	__WILLBEOWNED NPacket *packet,
	const char *ip,
	NProtocoleIP protocole,
	NU32 port )
{
	// Detail IP
	NDetailIP *detailIP;

	// Creer detail IP
	if( !( detailIP = NLib_Module_Reseau_IP_NDetailIP_Construire( protocole,
		ip ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer packet
		NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

		// Quitter
		return NFALSE;
	}

	// Ajouter
	return NLib_Module_Reseau_UDP_Client_NClientUDP_AjouterPacket2( this,
		packet,
		detailIP,
		port );
}

/**
 * Obtenir usine UDP
 *
 * @param this
 *		Cette instance
 *
 * @return l'instance de l'usine
 */
const NUsineUDP *NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirUsine( const NClientUDP *this )
{
	return this->m_usine;
}

/**
 * Est usine active?
 *
 * @param this
 *		Cette instance
 *
 * @return si l'usine est active
 */
NBOOL NLib_Module_Reseau_UDP_Client_NClientUDP_EstUsineActive( const NClientUDP *this )
{
	return this->m_estUtiliseUsine;
}

#endif // NLIB_MODULE_RESEAU

