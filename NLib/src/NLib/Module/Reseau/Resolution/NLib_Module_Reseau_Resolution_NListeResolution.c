#include "../../../../../include/NLib/NLib.h"

// ---------------------------------------------------------
// struct NLib::Module::Reseau::Resolution::NListeResolution
// ---------------------------------------------------------

#ifdef NLIB_MODULE_RESEAU
/**
 * Construire l'instance
 *
 * @param domaine
 *		Le domaine a resoudre
 * @param service
 *		Le service a resoudre (port)
 *
 * @return l'instance
 */
__ALLOC NListeResolution *NLib_Module_Reseau_Resolution_NListeResolution_Construire( const char *domaine,
	const char *service )
{
	// Sortie
	__OUTPUT NListeResolution *out;

	// Detail sur l'hote
	struct addrinfo *detailHote;

	// Indice pour trouver l'adresse
	struct addrinfo indice;

	// Temporaire
	struct addrinfo *tmp;

	// Detail ip
	struct sockaddr_in  *ipv4;
#ifdef IS_WINDOWS
	LPSOCKADDR ipv6;
#else // IS_WINDOWS
	struct sockaddr *ipv6;
#endif // !IS_WINDOWS

	// Buffer ipv6
#define NTAILLE_BUFFER_IPV6				46
#ifdef IS_WINDOWS
	DWORD tailleBufferIPV6 = NTAILLE_BUFFER_IPV6;
#endif // IS_WINDOWS
	char bufferIPV6[ NTAILLE_BUFFER_IPV6 ];

	// Donnees a creer
	char *ip;
	NProtocoleIP protocole;

	// Iterateurs
	NU32 i = 0,
		j;

	// Parametrer indice
		// Zero
			memset( &indice,
				0,
				sizeof( struct addrinfo ) );
		// Parametrer
			indice.ai_family = AF_UNSPEC;
			indice.ai_socktype = SOCK_STREAM;
			indice.ai_protocol = IPPROTO_TCP;

	// Resoudre l'hote
	if( getaddrinfo( domaine,
		service,
		&indice,
		&detailHote ) != 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DNS_RESOLVE_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer la sortie
	if( !( out = calloc( 1,
		sizeof( NListeResolution ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		freeaddrinfo( detailHote );

		// Quitter
		return NULL;
	}

	// Compter le nombre d'ips
	tmp = detailHote;
	while( tmp != NULL )
	{
		// Passer au detail suivant
		tmp = tmp->ai_next;

		// Incrementer le nombre d'ips
		out->m_nombreIP++;
	}

	// Allouer la memoire
		// IP
			if( !( out->m_ip = calloc( out->m_nombreIP,
				sizeof( NDetailIP* ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Liberer
				freeaddrinfo( detailHote );
				NFREE( out );

				// Quitter
				return NULL;
			}
		// Domaine
			if( !( out->m_domaine = calloc( strlen( domaine ) + 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Liberer
				freeaddrinfo( detailHote );
				NFREE( out->m_ip );
				NFREE( out );

				// Quit
				return NULL;
			}

	// Enregistrer domaine
	memcpy( out->m_domaine,
		domaine,
		strlen( domaine ) );

	// Lire les ips
	tmp = detailHote;
	while( tmp != NULL )
	{
		// Traiter
		switch( tmp->ai_family )
		{
			case AF_INET:
				// Recuperer le contexte d'adressage
				ipv4 = (struct sockaddr_in*)tmp->ai_addr;

				// Recuperer l'ip
				ip = inet_ntoa( ipv4->sin_addr );

				// Enregistrer ipv4
				protocole = NPROTOCOLE_IP_IPV4;
				break;
			case AF_INET6:
				// Recuperer le contexte d'adressage
				ipv6 =
#ifdef IS_WINDOWS
					(LPSOCKADDR)tmp->ai_addr;
#else // IS_WINDOWS
					tmp->ai_addr;
#endif // !IS_WINDOWS

#ifdef IS_WINDOWS
				// Fixer taille buffer
				tailleBufferIPV6 = NTAILLE_BUFFER_IPV6;

				// Convertir contexte ipv6 en texte
				if( WSAAddressToStringA( ipv6,
					(DWORD)tmp->ai_addrlen,
					NULL,
					bufferIPV6,
					&tailleBufferIPV6 ) != 0 )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_DNS_RESOLVE_FAILED );

					// Liberer
					for( j = 0; j < i; j++ )
						NLib_Module_Reseau_IP_NDetailIP_Detruire( &out->m_ip[ j ] );
					NFREE( out->m_ip );
					NFREE( out->m_domaine );
					NFREE( out );
					freeaddrinfo( detailHote );

					// Quitter
					return NULL;
				}
#else // IS_WINDOWS
				// Convertir contexte ipv6 en texte
				inet_ntop( AF_INET6,
					ipv6,
					bufferIPV6,
					sizeof( struct sockaddr ) );
#endif // !IS_WINDOWS

				// Enregistrer
				ip = bufferIPV6;
				protocole = NPROTOCOLE_IP_IPV6;
				break;

			default:
				// Notifier
				NOTIFIER_ERREUR( NERREUR_DNS_RESOLVE_FAILED );

				// Liberer
				for( j = 0; j < i; j++ )
					NLib_Module_Reseau_IP_NDetailIP_Detruire( &out->m_ip[ j ] );
				NFREE( out->m_ip );
				NFREE( out->m_domaine );
				NFREE( out );
				freeaddrinfo( detailHote );

				// Quitter
				return NULL;
		}

		// Construire ip
		if( !( out->m_ip[ i ] = NLib_Module_Reseau_IP_NDetailIP_Construire( protocole,
			ip ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			for( j = 0; j < i; j++ )
				NLib_Module_Reseau_IP_NDetailIP_Detruire( &out->m_ip[ j ] );
			NFREE( out->m_ip );
			NFREE( out->m_domaine );
			NFREE( out );
			freeaddrinfo( detailHote );

			// Quitter
			return NULL;
		}

		// Passer a la prochaine IP
		tmp = tmp->ai_next;

		// Incrementer
		i++;
	}

	// Liberer
	freeaddrinfo( detailHote );

	// OK
	return out;
}

/**
 * Detruire l'instance
 *
 * @param this
 *		This instance
 */
void NLib_Module_Reseau_Resolution_NListeResolution_Detruire( NListeResolution **this )
{
	// Iterateur
	NU32 i = 0;

	// Liberer
	for( ; i < (*this)->m_nombreIP; i++ )
		NLib_Module_Reseau_IP_NDetailIP_Detruire( &(*this)->m_ip[ i ] );
	NFREE( (*this)->m_ip );
	NFREE( (*this)->m_domaine );
	NFREE( *(this) );
}

/**
 * Obtenir le nombre d'ips pour le domaine
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre d'ips
 */
NU32 NLib_Module_Reseau_Resolution_NListeResolution_ObtenirNombre( const NListeResolution *this )
{
	return this->m_nombreIP;
}

/**
 * Obtenir detail ip
 *
 * @param this
 *		Cette instance
 * @param index
 *		The ip index
 *
 * @return the detail
 */
const NDetailIP *NLib_Module_Reseau_Resolution_NListeResolution_ObtenirIP( const NListeResolution *this,
	NU32 index )
{
	return this->m_ip[ index ];
}

/**
 * Obtenir ipv4
 *
 * @param this
 *		Cette instance
 *
 * @return une ip au format v4
 */
const NDetailIP *NLib_Module_Reseau_Resolution_NListeResolution_ObtenirIPV4( const NListeResolution *this )
{
	// Iterateur
	NU32 i = 0;

	// Chercher ipv4
	for( ; i < this->m_nombreIP; i++ )
		// Verifier
		if( NLib_Module_Reseau_IP_NDetailIP_ObtenirProtocole( this->m_ip[ i ] ) == NPROTOCOLE_IP_IPV4 )
			return this->m_ip[ i ];

	// Introuvable
	return NULL;
}

/**
 * Obtenir le domaine ayant ete resolu
 *
 * @param this
 *		Cette instance
 *
 * @return le nom de domaine
 */
const char *NLib_Module_Reseau_Resolution_NListeResolution_ObtenirDomaine( const NListeResolution *this )
{
	return this->m_domaine;
}

#endif // NLIB_MODULE_RESEAU

