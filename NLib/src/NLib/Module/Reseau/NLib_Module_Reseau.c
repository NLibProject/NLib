#define RESEAU_INTERNE
#include "../../../../include/NLib/NLib.h"

/**
 *	Module reseau
 *
 *	Sous windows, a l'initialisation du module, appel
 *	de WSAStartup
 *
 *	@author SOARES Lucas
 */

#ifdef NLIB_MODULE_RESEAU
// ------------------------------
// namespace NLib::Module::Reseau
// ------------------------------

/**
 * Initialiser le reseau
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Reseau_Initialiser( void )
{
#ifdef IS_WINDOWS
	// Initialiser reseau
	if( WSAStartup( MAKEWORD( 2, 2 ),
		&m_contexteWSA ) != EXIT_SUCCESS )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_WINSOCK );

		// Quitter
		return NFALSE;
	}
#endif // IS_WINDOWS

#ifndef IS_WINDOWS
	// Ignorer signal SIGPIPE
	signal( SIGPIPE,
		SIG_IGN );
#endif // !IS_WINDOWS

	// OK
	return NTRUE;
}

/**
 * Detruire le reseau
 */
void NLib_Module_Reseau_Detruire( void )
{
	// Sous windows
#ifdef IS_WINDOWS
	WSACleanup( );
#endif // IS_WINDOWS
}

/**
 * Calculer le nombre d'ip dans une plage cidr donnee
 *
 * @param cidr
 * 		Le cidr
 *
 * @return le nombre d'ip
 */
NU32 NLib_Module_Reseau_CalculerNombreIPDansCIDR( NU32 cidr )
{
	// Inclus l'adresse de broadcast et reseau
	return NLib_Math_CalculerPuissance( 2,
		( 32 - cidr ) );
}

#endif // NLIB_MODULE_RESEAU

