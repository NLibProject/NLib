#include "../../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU

#ifdef NLIB_MODULE_RESEAU_BLUETOOTH

// -------------------------------------------------
// namespace NLib::Module::Reseau::Client::Bluetooth
// -------------------------------------------------

/**
 * Obtenir adresse mac
 *
 * @param adresse
 *		L'adresse
 *
 * @return l'adresse mac
 */
__ALLOC char *NLib_Module_Reseau_Client_Bluetooth_TraduireAdresseMac( const struct sockaddr_rc *adresse )
{
	// Sortie
	__OUTPUT char *sortie;

	// Allouer la memoire
	if( !( sortie = calloc( NTAILLE_ADRESSE_MAC + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Traduire
    ba2str( &adresse->rc_bdaddr,
		sortie );

	// OK
	return sortie;
}

/**
 * Obtenir le canal
 *
 * @param adresse
 *		L'adresse
 *
 * @return le canal
 */
NU32 NLib_Module_Reseau_Client_Bluetooth_TraduireCanal( const struct sockaddr_rc *adresse )
{
    return (NU32)adresse->rc_channel;
}

#endif // NLIB_MODULE_RESEAU_BLUETOOTH

#endif // NLIB_MODULE_RESEAU
