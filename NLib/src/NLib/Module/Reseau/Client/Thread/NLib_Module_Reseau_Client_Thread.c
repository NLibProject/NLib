#include "../../../../../../include/NLib/NLib.h"

/*
 *	@author SOARES Lucas
 */

// ----------------------------------------------
// namespace NLib::Module::Reseau::Client::Thread
// ----------------------------------------------

#ifdef NLIB_MODULE_RESEAU

/* Emission */
NBOOL NLib_Module_Reseau_Client_Thread_Emission( NClient *client )
{
	// Packet
	NPacket *packet;

	do
	{
		// Delais
		NLib_Temps_Attendre( 16 );

		// Si le client est vivant
		if( NLib_Module_Reseau_Client_NClient_EstConnecte( client )
			&& !NLib_Module_Reseau_Client_NClient_EstErreur( client ) )
		{
			// Si il y a quelque chose a ecrire dans le flux
			if( NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( NLib_Module_Reseau_Client_NClient_ObtenirCachePacket( client ) ) > 0 )
			{
				// Si on peut ecrire dans le flux
				if( NLib_Module_Reseau_Socket_Selection_EstEcritureDisponible( NLib_Module_Reseau_Client_NClient_ObtenirSocket( client ) ) )
				{
					// Recuperer le packet
					if( !( packet = NLib_Module_Reseau_Packet_NCachePacket_ObtenirPacket( NLib_Module_Reseau_Client_NClient_ObtenirCachePacket( client ) ) ) )
						continue;

					// Ecrire dans le flux
					if( !client->m_callbackMethodeEmission( packet,
						NLib_Module_Reseau_Client_NClient_ObtenirSocket( client ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_SEND,
							__FUNCTION__,
						(NU32)errno );

						// Tuer le client
						NLib_Module_Reseau_Client_NClient_ActiverErreur( client );
					}

					// Liberer le packet
					switch( NLib_Module_Reseau_Client_NClient_ObtenirTypeCachePacket( client ) )
					{
						default:
						case NTYPE_CACHE_PACKET_TCP:
							NLib_Module_Reseau_Packet_NPacket_Detruire( (NPacket**)&packet );
							break;
						case NTYPE_CACHE_PACKET_PERSONNALISE:
							NLib_Module_Reseau_Packet_NPacketPersonnalise_Detruire( (NPacketPersonnalise**)&packet );
							break;
					}

					// Supprimer le packet du cache
					NLib_Module_Reseau_Packet_NCachePacket_SupprimerPacket( NLib_Module_Reseau_Client_NClient_ObtenirCachePacket( client ) );
				}
			}
		}
	} while( NLib_Module_Reseau_Client_NClient_EstThreadEnCours( client ) );

	// OK
	return NTRUE;
}

/* Reception */
NBOOL NLib_Module_Reseau_Client_Thread_Reception( NClient *client )
{
	// Packet
	NPacket *packet;

	do
	{
		// Delais
		NLib_Temps_Attendre( 16 );

		// Si le client est connecte
		if( NLib_Module_Reseau_Client_NClient_EstConnecte( client )
			&& !NLib_Module_Reseau_Client_NClient_EstErreur( client ) )
		{
			// Si on peut lire dans le flux
			if( NLib_Module_Reseau_Socket_Selection_EstLectureDisponible( NLib_Module_Reseau_Client_NClient_ObtenirSocket( client ) ) )
			{
				// Lire dans le flux
				if( !( packet = client->m_callbackMethodeReception( NLib_Module_Reseau_Client_NClient_ObtenirSocket( client ),
					NLib_Module_Reseau_Client_NClient_EstTimeoutAutorise( client ) ) ) )
				{
					// Notifier
					NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_SOCKET_RECV,
						__FUNCTION__,
					(NU32)errno );

					// Tuer le client
					NLib_Module_Reseau_Client_NClient_ActiverErreur( client );
				}
				// Transmettre au callback
				else
				{
					// On a un callback?
					if( client->m_callbackReceptionPacket != NULL )
						// Appeler callback reception
						if( !client->m_callbackReceptionPacket( packet,
							client ) )
							NLib_Module_Reseau_Client_NClient_ActiverErreur( client );

					// Liberer le packet
					NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );
				}
			}
		}
	} while( NLib_Module_Reseau_Client_NClient_EstThreadEnCours( client ) );

	// OK
	return NTRUE;
}

#endif // NLIB_MODULE_RESEAU

