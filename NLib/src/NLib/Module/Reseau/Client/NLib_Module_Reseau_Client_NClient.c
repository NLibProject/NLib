#include "../../../../../include/NLib/NLib.h"

/**
 *	Definition d'un client auto-gere quant-a la reception
 *	et l'emission de packets vers le serveur
 *
 *	@author SOARES Lucas
 */

#ifdef NLIB_MODULE_RESEAU
// --------------------------------------------
// struct NLib::Module::Reseau::Client::NClient
// --------------------------------------------

/**
 * Callback reception packet par defaut (prive)
 *
 * @param packet
 * 		Le packet recu
 * @param client
 * 		Le client ayant recu le message
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE __CALLBACK NBOOL NLib_Module_Reseau_Client_NClient_CallbackReceptionPacketInterne( const NPacket *packet,
	const NClient *client )
{
	// Referencer
	NREFERENCER( packet );
	NREFERENCER( client );

	// OK
	return NTRUE;
}

/**
 * Callback deconnexion par defaut
 *
 * @param client
 * 		Le client qui s'est deconnecte
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NLib_Module_Reseau_Client_NClient_CallbackDeconnexion( const NClient *client )
{
	// Referencer
	NREFERENCER( client );

	// OK
	return NTRUE;
}

/**
 * Construire client
 *
 * @param adresse
 *		L'adresse IPV4/MAC a laquelle se connecter
 * @param portOuCanal
 *		Le port/canal auquel se connecter
 * @param callbackReceptionPacket
 *		Le callback en cas de reception de packet
 * @param callbackDeconnexion
 *		Le callback en cas de deconnexion
 * @param dataUtilisateur
 *		Les donnees utilisateur
 * @param tempsAvantTimeout
 *		Le temps avant d'etre timeout pour reception (ignore si = 0)
 * @param type
 *		Le type de client
 * @param methodeTransmission
 *		La methode de transmission souhaitee
 *
 * @return l'instance du client
 */
__ALLOC NClient *NLib_Module_Reseau_Client_NClient_Construire( const char *adresse,
	NU32 portOuCanal,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NPacket*,
		const NClient* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( const NClient* ),
	void *dataUtilisateur,
	NU32 tempsAvantTimeout,
	NTypeClient type,
	NMethodeTransmissionPacketIO methodeTransmission )
{
	// Methode reception
	__CALLBACK NPacket *( ___cdecl *callbackMethodeReception )( SOCKET socket,
		NBOOL estTimeoutAutorise );

	// Methode emission
	__CALLBACK NBOOL ( ___cdecl *callbackMethodeEmission )( const void *packet,
		SOCKET socket );

	// Definir la methode
	switch( methodeTransmission )
	{
		case NMETHODE_TRANSMISSION_PACKETIO_RAW:
			callbackMethodeReception = NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketRaw;
			callbackMethodeEmission = NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketRaw;
			break;

		case NMETHODE_TRANSMISSION_PACKETIO_NPROJECT:
			callbackMethodeReception = NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketNProject;
			callbackMethodeEmission = NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketNProject;
			break;

		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

			// Quitter
			return NULL;
	}

	// Construire le client
	return NLib_Module_Reseau_Client_NClient_Construire2( adresse,
		portOuCanal,
		callbackReceptionPacket,
		callbackDeconnexion,
		dataUtilisateur,
		tempsAvantTimeout,
		type,
		NTYPE_CACHE_PACKET_TCP,
		callbackMethodeReception,
		callbackMethodeEmission );
}

/**
 * Construire client avec methode transmission personnalisee
 *
 * @param adresse
 *		L'adresse IPV4/MAC a laquelle se connecter
 * @param portOuCanal
 *		Le port/canal auquel se connecter
 * @param callbackReceptionPacket
 *		Le callback en cas de reception de packet
 * @param callbackDeconnexion
 *		Le callback en cas de deconnexion
 * @param dataUtilisateur
 *		Les donnees utilisateur
 * @param tempsAvantTimeout
 *		Le temps avant d'etre timeout pour reception (ignore si = 0)
 * @param type
 *		Le type de client
 * @param typePacketEnvoye
 * 		Le type de packet envoye
 * @param callbackMethodeReception
 *		La methode de transmission pour recevoir
 * @param callbackMethodeEmission
 *		La methode de transmission pour emettre
 *
 * @return l'instance du client
 */
__ALLOC NClient *NLib_Module_Reseau_Client_NClient_Construire2( const char *adresse,
	NU32 portOuCanal,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NPacket*,
		const NClient* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( const NClient* ),
	void *dataUtilisateur,
	NU32 tempsAvantTimeout,
	NTypeClient type,
	NTypeCachePacket typePacketEnvoye,
	__CALLBACK __ALLOC NPacket *( ___cdecl *callbackMethodeReception )( SOCKET socket,
		NBOOL estTimeoutAutorise ),
	__CALLBACK NBOOL ( ___cdecl *callbackMethodeEmission )( const void *packet,
		SOCKET socket ) )
{
	// Sortie
	__OUTPUT NClient *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire le cache packet
	if( !( out->m_cachePacket = NLib_Module_Reseau_Packet_NCachePacket_Construire( typePacketEnvoye ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Configurer la socket
	switch( type )
	{
		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

			// Detruire le cache packet
			NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;

		case NTYPE_CLIENT_TCP:
			out->m_adresse.tcp.sin_family = AF_INET;
			out->m_adresse.tcp.sin_port = htons( (NU16)portOuCanal );
			out->m_adresse.tcp.sin_addr.FIN_ADRESSE = inet_addr( adresse );
			break;
#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
		case NTYPE_CLIENT_BLUETOOTH:
			out->m_adresse.bluetooth.rc_family = AF_BLUETOOTH;
			out->m_adresse.bluetooth.rc_channel = (NU8)portOuCanal;
			str2ba( adresse,
				&out->m_adresse.bluetooth.rc_bdaddr );
			break;
#endif // NLIB_MODULE_RESEAU_BLUETOOTH
	}

	// Enregistrer
	out->m_callbackDeconnexion = ( callbackDeconnexion != NULL ) ?
		callbackDeconnexion
		: NLib_Module_Reseau_Client_NClient_CallbackDeconnexion;
	out->m_callbackReceptionPacket = ( callbackReceptionPacket != NULL ) ?
		callbackReceptionPacket
		: NLib_Module_Reseau_Client_NClient_CallbackReceptionPacketInterne;
	out->m_dataUtilisateur = dataUtilisateur;
	out->m_tempsAvantTimeout = tempsAvantTimeout;
	out->m_type = type;
	out->m_callbackMethodeReception = callbackMethodeReception;
	out->m_callbackMethodeEmission = callbackMethodeEmission;

	// Zero
	out->m_estConnecte = NFALSE;
	out->m_socket = INVALID_SOCKET;
	out->m_estErreur = NFALSE;

	// Lancer les threads
		// Ca tourne
			out->m_estThreadEnCours = NTRUE;
		// Emission
			if( !( out->m_threadEmission = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NLib_Module_Reseau_Client_Thread_Emission,
				out,
				&out->m_estThreadEnCours ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_THREAD );

				// Detruire le cache packets
				NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

				// Quitter
				return NULL;
			}
		// Reception
			if( !( out->m_threadReception = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NLib_Module_Reseau_Client_Thread_Reception,
				out,
				&out->m_estThreadEnCours ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_THREAD );

				// Detruire le thread emission
				NLib_Thread_NThread_Detruire( &out->m_threadEmission );

				// Detruire le cache packets
				NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

				// Quitter
				return NULL;
			}

	// OK
	return out;
}

/**
 * Detruire le client
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Client_NClient_Detruire( NClient **this )
{
	// Deconnecter
	NLib_Module_Reseau_Client_NClient_Deconnecter( (*this) );

	// Arreter les threads
    NLib_Thread_NThread_Detruire( &(*this)->m_threadEmission );
    NLib_Thread_NThread_Detruire( &(*this)->m_threadReception );

	// Detruire le cache packets
	NLib_Module_Reseau_Packet_NCachePacket_Detruire( &(*this)->m_cachePacket );

	// Liberer
	NFREE( (*this) );
}

/**
 * Connecter
 *
 * @param this
 * 		Cette instance
 *
 * @return si la connexion a reussi
 */
NBOOL NLib_Module_Reseau_Client_NClient_Connecter( NClient *this )
{
	return NLib_Module_Reseau_Client_NClient_Connecter2( this,
		0 );
}

/**
 * Connecter avec timeout a la connexion
 *
 * @param this
 * 		Cette instance
 * @param timeout
 * 		Le timeout a la connexion
 *
 * @return si la connexion a reussi
 */
NBOOL NLib_Module_Reseau_Client_NClient_Connecter2( NClient *this,
	NU32 timeout )
{
#ifndef IS_WINDOWS
	struct timeval timeoutADefinir;
#endif // !IS_WINDOWS

	// Domaine socket
	NU32 domaineSocket;

	// Descripteur socket
	fd_set descripteurSocket;

	// Protocole
	NU32 protocole;

	// Taille du contexte d'adressage
    NU32 tailleContexteAdressage = 0;

	// Adresse
	void *adresse = NULL;

	// Flag socket
	NS32 flagSocket;

	// Erreur socket
	NS32 erreurSocket;
	socklen_t tailleErreurSocket = sizeof( NS32 );

	// Verifier etat
	if( NLib_Module_Reseau_Client_NClient_EstConnecte( this ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Desactiver erreur
	this->m_estErreur = NFALSE;

	// Parametrer
	switch( this->m_type )
	{
		case NTYPE_CLIENT_TCP:
			domaineSocket = AF_INET;
			tailleContexteAdressage = sizeof( SOCKADDR_IN );
			adresse = &this->m_adresse.tcp;
			protocole = 0;
			break;
#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
		case NTYPE_CLIENT_BLUETOOTH:
			domaineSocket = AF_BLUETOOTH;
			tailleContexteAdressage = sizeof( struct sockaddr_rc );
			adresse = &this->m_adresse.bluetooth;
			protocole = BTPROTO_RFCOMM;
			break;
#endif // NLIB_MODULE_RESEAU_BLUETOOTH

		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Quitter
			return NFALSE;
	}

	// Creer la socket
	if( ( this->m_socket = (SOCKET)socket( domaineSocket,
		SOCK_STREAM,
		protocole ) ) == INVALID_SOCKET )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET );

		// Quitter
		return NFALSE;
	}

	// Timeout connexion?
	if( timeout > 0 )
	{
		// Obtenir flags
		flagSocket = fcntl( this->m_socket,
			F_GETFL,
			0 );

		// Ajouter le flag non bloquant
		flagSocket |= O_NONBLOCK;

		// Desactiver mode non bloquant socket
		fcntl( this->m_socket,
			F_SETFL,
			flagSocket );
	}

	// Connecter
	if( connect( this->m_socket,
		(struct sockaddr*)adresse,
		tailleContexteAdressage ) == INVALID_SOCKET
		&& ( timeout > 0 ?
			 (NBOOL)( errno != EINPROGRESS )
			 : NTRUE ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SOCKET_CONNECTION_FAILED );

		// Fermer la socket
		closesocket( this->m_socket );

		// Quitter
		return NFALSE;
	}

	// On a un timeout a la connexion?
	if( timeout > 0 )
	{
		// Initialiser descripteur socket
		FD_ZERO( &descripteurSocket );
		FD_SET( this->m_socket,
			&descripteurSocket );

		// Definir timeout
		timeoutADefinir.tv_sec = timeout / 1000;
		timeoutADefinir.tv_usec = ( timeout % 1000 ) * 1000;

		// Attendre que la socket soit disponible en ecriture (voulant dire que la connexion a reussi)
		if( select( this->m_socket + 1,
			NULL,
			&descripteurSocket,
			NULL,
			&timeoutADefinir ) == 1 )
		{
			getsockopt( this->m_socket,
				SOL_SOCKET,
				SO_ERROR,
				&erreurSocket,
				&tailleErreurSocket );

			if( erreurSocket != 0 )
			{
				// Notifier
				NOTIFIER_AVERTISSEMENT( NERREUR_SOCKET_CONNECTION_TIMEOUT );

				// Fermer la socket
				closesocket( this->m_socket );

				// Quitter
				return NFALSE;
			}
		}
		else
		{
			// Notifier
			NOTIFIER_AVERTISSEMENT( NERREUR_SOCKET_CONNECTION_TIMEOUT );

			// Fermer la socket
			closesocket( this->m_socket );

			// Quitter
			return NFALSE;
		}

		// Obtenir liste flags
		flagSocket = fcntl( this->m_socket,
			F_GETFL,
			0 );

		// Supprimer le flag non bloquant
		flagSocket ^= O_NONBLOCK;

		// Desactiver mode non bloquant socket
		fcntl( this->m_socket,
			F_SETFL,
			flagSocket );
	}

	// Si on a un timeout a la reception
	if( this->m_tempsAvantTimeout > 0 )
	{
#ifdef IS_WINDOWS
		// Definir le timeout
		setsockopt( this->m_socket,
			SOL_SOCKET,
			SO_RCVTIMEO,
			(char*)&this->m_tempsAvantTimeout,
			sizeof( NU32 ) );
#else // IS_WINDOWS
		// Definir le timeout
		timeoutADefinir.tv_sec = this->m_tempsAvantTimeout / 1000;
		timeoutADefinir.tv_usec = ( this->m_tempsAvantTimeout % 1000 ) * 1000;

		// Definir le timeout
		setsockopt( this->m_socket,
			SOL_SOCKET,
			SO_RCVTIMEO,
			&timeoutADefinir,
			sizeof( struct timeval ) );
#endif // !IS_WINDOWS
	}

	// OK
	return this->m_estConnecte = NTRUE;
}

/**
 * Se deconnecter
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Client_NClient_Deconnecter( NClient *this )
{
	// Verifier connexion
	if( !NLib_Module_Reseau_Client_NClient_EstConnecte( this ) )
		return NFALSE;

	// Callback deconnexion
	this->m_callbackDeconnexion( this );

	// Vider le cache packet
	NLib_Module_Reseau_Packet_NCachePacket_Vider( this->m_cachePacket );

	// On est deconnecte
	this->m_estConnecte = NFALSE;

	// Detruire la socket
	closesocket( this->m_socket );

	// Oublier la socket
	this->m_socket = INVALID_SOCKET;

	// OK
	return NTRUE;
}

/**
 * Activer flag erreur
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Client_NClient_ActiverErreur( NClient *this )
{
	this->m_estErreur = NTRUE;
}

/**
 * Ajouter un packet a envoyer
 *
 * @param packet
 * 		Le packet a envoyer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Client_NClient_AjouterPacket( NClient *this,
	__WILLBEOWNED void *packet )
{
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacket( this->m_cachePacket,
		packet );
}

/**
 * Ajouter un packet qui sera une copie du packet passe
 *
 * @param packet
 * 		Le packet a ajouter apres duplication
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Client_NClient_AjouterPacketCopie( NClient *this,
	const void *packet )
{
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacketCopie( this->m_cachePacket,
		packet );
}

/**
 * Est ce que le client est connecte?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le client est connecte
 */
NBOOL NLib_Module_Reseau_Client_NClient_EstConnecte( const NClient *this )
{
	return this->m_estConnecte;
}

/**
 * Est ce qu'il y a une erreur?
 *
 * @param this
 * 		Cette instance
 *
 * @return si il y a eu une erreur
 */
NBOOL NLib_Module_Reseau_Client_NClient_EstErreur( const NClient *this )
{
	return this->m_estErreur;
}

/**
 * Obtenir la liste des packets
 *
 * @param this
 * 		Cette instance
 *
 * @return la liste des packet
 */
NCachePacket *NLib_Module_Reseau_Client_NClient_ObtenirCachePacket( const NClient *this )
{
	return this->m_cachePacket;
}

/**
 * Obtenir la socket
 *
 * @param this
 * 		Cette instance
 *
 * @return la socket
 */
SOCKET NLib_Module_Reseau_Client_NClient_ObtenirSocket( const NClient *this )
{
	return this->m_socket;
}

/**
 * Le client est en cours?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le client est en cours
 */
NBOOL NLib_Module_Reseau_Client_NClient_EstThreadEnCours( const NClient *this )
{
	return this->m_estThreadEnCours;
}

/**
 * Obtenir les donnees utilisateur
 *
 * @param this
 * 		Cette instance
 *
 * @return les donnees utilisateur
 */
void *NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( const NClient *this )
{
	return this->m_dataUtilisateur;
}

/**
 * On a un timeout defini pour la reception?
 *
 * @param this
 * 		Cette instance
 *
 * @return si un timeout est defini
 */
NBOOL NLib_Module_Reseau_Client_NClient_EstTimeoutAutorise( const NClient *this )
{
	return (NBOOL)( this->m_tempsAvantTimeout > 0 );
}

/**
 * Obtenir le timeout reception
 *
 * @param this
 * 		Cette instance
 *
 * @return le timeout reception
 */
NU32 NLib_Module_Reseau_Client_NClient_ObtenirTempsAvantTimeout( const NClient *this )
{
	return this->m_tempsAvantTimeout;
}

/**
 * Obtenir l'adresse
 *
 * @param this
 *		Cette instance
 *
 * @return l'adresse
 */
const void *NLib_Module_Reseau_Client_NClient_ObtenirAdresse( const NClient *this )
{
	// Suivant le type de client
	switch( this->m_type )
	{
		case NTYPE_CLIENT_TCP:
			return &this->m_adresse.tcp;
#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
		case NTYPE_SERVEUR_BLUETOOTH:
			return &this->m_adresse.bluetooth;
#endif // NLIB_MODULE_RESEAU_BLUETOOTH

		default:
			return NULL;
	}
}

/**
 * Obtenir le type de cache packet
 *
 * @param this
 * 		Cette instance
 *
 * @return le type de cache packet
 */
NTypeCachePacket NLib_Module_Reseau_Client_NClient_ObtenirTypeCachePacket( const NClient *this )
{
	return NLib_Module_Reseau_Packet_NCachePacket_ObtenirTypeCache( this->m_cachePacket );
}
#endif // NLIB_MODULE_RESEAU

