#include "../../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ------------------------------------------------
// namespace NLib::Module::Reseau::Packet::PacketIO
// ------------------------------------------------

#ifdef NLIB_MODULE_RESEAU
/* PacketIO */
// Creer base buffer (privee) [HEADER/CRC32]
NBOOL NLib_Module_Reseau_Packet_PacketIO_CreerBaseBufferInterne( __OUTPUT char buffer[ NBUFFER_PACKET ],
	NU32 *curseurBuffer,
	const char *data,
	NU32 tailleData,
	NU32 curseurData,
	NEtapePacketIO etape )
{
	// Taille a copier dans le buffer
	NU32 tailleACopierBuffer;

	// Calculer taille a copier dans le buffer
	switch( etape )
	{
		case NETAPE_PACKET_IO_BUFFER_HEADER:
			tailleACopierBuffer = ( tailleData - curseurData >= ( NBUFFER_PACKET - NTAILLE_HEADER_BUFFER - sizeof( NU32 ) - sizeof( NU32 )) ) ?
				( NBUFFER_PACKET - NTAILLE_HEADER_BUFFER - sizeof( NU32 ) - sizeof( NU32 ) )
				: ( tailleData - curseurData );
			break;

		default:
			tailleACopierBuffer = ( tailleData - curseurData >= ( NBUFFER_PACKET - NTAILLE_HEADER_BUFFER - sizeof( NU32 ) ) ) ?
				( NBUFFER_PACKET - NTAILLE_HEADER_BUFFER - sizeof( NU32 ) )
				: ( tailleData - curseurData );
			break;
	}

	// Copier
	memcpy( buffer,
		NHEADER_BUFFER,
		NTAILLE_HEADER_BUFFER );

	// Incrementer curseur
	*curseurBuffer += NTAILLE_HEADER_BUFFER;

	// Ajouter crc
	*((NU32*)&buffer[ *curseurBuffer ]) = NLib_Memoire_CalculerCRC( data + curseurData,
		tailleACopierBuffer );

	// Incrementer
	*curseurBuffer += sizeof( NU32 );

	// OK
	return NTRUE;
}

// Creer un buffer (privee)
NBOOL NLib_Module_Reseau_Packet_PacketIO_CreerBufferInterne( __OUTPUT char buffer[ NBUFFER_PACKET ],
	const char *data,
	NU32 tailleData,
	NU32 *curseurData,
	NEtapePacketIO etape )
{
	// Curseur buffer
	NU32 curseurBuffer = 0;

	// Taille a ajouter
	NU32 tailleAAjouter;

	// Creer base header
	NLib_Module_Reseau_Packet_PacketIO_CreerBaseBufferInterne( buffer,
		&curseurBuffer,
		data,
		tailleData,
		*curseurData,
		etape );

	// Ajouter taille si au debut
	switch( etape )
	{
		case NETAPE_PACKET_IO_BUFFER_HEADER:
			// Ajouter taille
			*((NU32*)&buffer[ curseurBuffer ]) = tailleData;

			// Incrementer curseur
			curseurBuffer += sizeof( NU32 );
			break;

		default:
			break;
	}

	// Calculer taille a ajouter
	tailleAAjouter = ( tailleData - *curseurData ) >= ( NBUFFER_PACKET - curseurBuffer ) ?
		NBUFFER_PACKET - curseurBuffer
		: ( tailleData - *curseurData );

	// Ajouter donnees
	memcpy( buffer + curseurBuffer,
		data + *curseurData,
		tailleAAjouter );

	// Incrementer le curseur
	*curseurData += tailleAAjouter;

	// Ajouter selon etape
	switch( etape )
	{
		case NETAPE_PACKET_IO_BUFFER_HEADER:
			// Recalculer CRC (ajout taille propre e l'header)
			*((NU32*)&buffer[ NTAILLE_HEADER_BUFFER ]) = NLib_Memoire_CalculerCRC( buffer + NTAILLE_HEADER_BUFFER + sizeof( NU32 ),
				NBUFFER_PACKET - NTAILLE_HEADER_BUFFER - sizeof( NU32 ) );
			break;

		default:
			break;
	}

	// OK
	return NTRUE;
}

// Recevoir un buffer (privee)
NBOOL NLib_Module_Reseau_Packet_PacketIO_RecevoirBufferInterne( __OUTPUT char buffer[ NBUFFER_PACKET ],
	SOCKET socket )
{
	// Taille recue
	NU64 tailleLue = 0;

	// Taille a lire
	NU64 tailleALire;

	// Code recv
	ssize_t code;

	// Lire
	while( tailleLue < NBUFFER_PACKET )
	{
		// Calculer la taille a lire
		tailleALire = NBUFFER_PACKET - tailleLue;

		// Lire flux
		if( ( code = recv( socket,
				buffer + tailleLue,
				tailleALire,
				0 ) ) == INVALID_SOCKET
			|| !code )
		{
			// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_SOCKET_RECV,
				__FUNCTION__,
			(NU32)errno );

			// Quitter
			return NFALSE;
		}

		// Incrementer la taille lue
		tailleLue += (NU32)code;
	}

	// OK
	return NTRUE;
}

// Interpreter un buffer (privee)
NBOOL NLib_Module_Reseau_Packet_PacketIO_InterpreterBufferInterne( __OUTPUT char **out,
	NU32 *tailleCouranteOut,
	NU32 *tailleTotalePacket,
	const char buffer[ NBUFFER_PACKET ],
	NEtapePacketIO etape )
{
	// Curseur buffer
	NU32 curseurBuffer = 0;

	// CRC32
	NU32 CRC32;

	// Header
	char header[ NTAILLE_HEADER_BUFFER + 1 ];

	// Lire l'header
	memcpy( header,
		buffer + curseurBuffer,
		NTAILLE_HEADER_BUFFER );

	// Incrementer le curseur
	curseurBuffer += NTAILLE_HEADER_BUFFER;

	// Verifier l'header
	if( memcmp( header,
		NHEADER_BUFFER,
		NTAILLE_HEADER_BUFFER ) != 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Lire le CRC32
	CRC32 = *( (NU32*)( buffer + curseurBuffer ) );

	// Incrementer curseur
	curseurBuffer += sizeof( NU32 );

	// Verifier CRC donnees
	if( NLib_Memoire_CalculerCRC( buffer + curseurBuffer,
		NBUFFER_PACKET - curseurBuffer ) != CRC32 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CRC );

		// Quitter
		return NFALSE;
	}

	// En fonction de l'etape
	switch( etape )
	{
		case NETAPE_PACKET_IO_BUFFER_HEADER:
			// Lire la taille
			*tailleTotalePacket = *((NU32*)( buffer + curseurBuffer ));

			// Incrementer le curseur
			curseurBuffer += sizeof( NU32 );
			break;

		default:
			break;
	}

	// Lire donnees
	if( !NLib_Memoire_AjouterData( out,
		*tailleCouranteOut,
		buffer + curseurBuffer,
		NBUFFER_PACKET - curseurBuffer ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MEMORY );

		// Quitter
		return NFALSE;
	}

	// Incrementer taille
	*tailleCouranteOut += NBUFFER_PACKET - curseurBuffer;

	// OK
	return NTRUE;
}

// Recevoir un packet (privee)
NBOOL NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketInterne( __OUTPUT char **data,
	__OUTPUT NU32 *taille,
	SOCKET socket )
{
	// Etape
	NEtapePacketIO etape = NETAPE_PACKET_IO_BUFFER_HEADER;

	// Buffer
	char buffer[ NBUFFER_PACKET ];

	// Taille totale du packet
	NU32 tailleTotalePacket = 0;

	// Lire
	do
	{
		// Vider buffer
		memset( buffer,
			0,
			NBUFFER_PACKET );

		// Lire buffer
		if( !NLib_Module_Reseau_Packet_PacketIO_RecevoirBufferInterne( buffer,
			socket ) )
		{
			// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_SOCKET_RECV,
				__FUNCTION__,
			(NU32)errno );

			// Liberer
			NFREE( *data );

			// Quitter
			return NFALSE;
		}

		// Interpreter le packet
		if( !NLib_Module_Reseau_Packet_PacketIO_InterpreterBufferInterne( data,
			taille,
			&tailleTotalePacket,
			buffer,
			etape ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Liberer
			NFREE( *data );

			// Quitter
			return NFALSE;
		}

		// Gerer etape
		switch( etape )
		{
			case NETAPE_PACKET_IO_BUFFER_HEADER:
				etape = NETAPE_PACKET_IO_BUFFER_CONTENU;
				break;

			default:
				break;
		}
	} while( *taille < tailleTotalePacket );

	// OK
	return NTRUE;
}

// Envoyer un buffer (privee)
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerBufferInterne( const char buffer[ NBUFFER_PACKET ],
	SOCKET socket )
{
	// Taille envoyee
	NU64 tailleEnvoyee = 0;

	// Code send
	ssize_t code;

	// Envoyer le buffer
	while( tailleEnvoyee < NBUFFER_PACKET )
	{
		// Envoyer
		if( ( code = send( socket,
				buffer + tailleEnvoyee,
				NBUFFER_PACKET - tailleEnvoyee,
				0 ) ) == INVALID_SOCKET
			|| !code )
		{
			// Notifier
			NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_SEND,
				__FUNCTION__,
			(NU32)errno );

			// Quitter
			return NFALSE;
		}

		// Incrementer la taille
		tailleEnvoyee += (NU32)code;
	}

	// OK
	return NTRUE;
}

// Envoyer un packet (privee)
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketInterne( const char *data,
	NU32 taille,
	SOCKET socket )
{
	// Curseur data
	NU32 curseurData = 0;

	// Etape
	NEtapePacketIO etape = NETAPE_PACKET_IO_BUFFER_HEADER;

	// Buffer
	char buffer[ NBUFFER_PACKET ];

	// Envoyer
	while( curseurData < taille )
	{
		// Vider le buffer
		memset( buffer,
			0,
			NBUFFER_PACKET );

		// Creer le buffer
		NLib_Module_Reseau_Packet_PacketIO_CreerBufferInterne( buffer,
			data,
			taille,
			&curseurData,
			etape );

		// Envoyer
		if( !NLib_Module_Reseau_Packet_PacketIO_EnvoyerBufferInterne( buffer,
			socket ) )
		{
			// Notifier
			NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_SEND,
				__FUNCTION__,
				(NU32)errno );

			// Quitter
			return NFALSE;
		}

		// Etape suivante?
		switch( etape )
		{
			case NETAPE_PACKET_IO_BUFFER_HEADER:
				etape = NETAPE_PACKET_IO_BUFFER_CONTENU;
				break;

			default:
				break;
		}
	}

	// OK
	return NTRUE;
}

/**
 * Recevoir packet methode NProject
 *
 * @param socket
 *		La socket sur laquelle on va lire
 * @param unused
 *		Inutilise
 *
 * @return le packet recu
 */
__ALLOC NPacket *NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketNProject( SOCKET socket,
	NBOOL unused )
{
	// Donnees
	char *data = NULL;

	// Taille
	NU32 taille = 0;

	// Output
	__OUTPUT NPacket *out;

	// Referencer parametre
	NREFERENCER( unused );

	// Recevoir
	if( !NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketInterne( &data,
		&taille,
		socket ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_SOCKET_RECV,
			__FUNCTION__,
			(NU32)errno );

		// Quitter
		return NULL;
	}

	// Construire le packet
	if( !( out = NLib_Module_Reseau_Packet_NPacket_Construire3( data,
			taille ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( data );

		// Quitter
		return NULL;
	}

	// Liberer
	NFREE( data );

	// OK
	return out;
}

/**
 * Envoyer packet methode NProject
 *
 * @param packet
 *		Le packet a envoyer
 * @param socket
 *		La socket sur laquelle envoyer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketNProject( const void *packet,
	SOCKET socket )
{
	// Envoyer
	return NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketInterne( NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ),
		socket );
}

/**
 * Recevoir packet methode Raw (+timeout)
 *
 * @param socket
 *		La socket sur laquelle on va lire
 * @param estTimeoutAutorise
 *		On autorise le timeout?
 *
 * @return le packet recu
 */
__ALLOC NPacket *NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketRaw( SOCKET socket,
	NBOOL estTimeoutAutorise )
{
	// Code retour recv
	ssize_t code;

	// Buffer
	char buffer[ NBUFFER_PACKET ];

	// Packet
	__OUTPUT NPacket *packet;

	// Construire le packet
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Lire
	do
	{
		// Vider buffer
		memset( buffer,
			0,
			NBUFFER_PACKET );

		// Lire
		if( ( code = recv( socket,
			buffer,
			NBUFFER_PACKET,
			0 ) ) == INVALID_SOCKET
			&& ( estTimeoutAutorise ?
#ifdef IS_WINDOWS
					( WSAGetLastError( ) != WSAETIMEDOUT )
#else // IS_WINDOWS
				 	(NBOOL)( errno != EWOULDBLOCK && errno != EAGAIN )
#endif // !IS_WINDOWS
					: NTRUE ) )
		{
			// Notifier
			NOTIFIER_AVERTISSEMENT( NERREUR_SOCKET_RECV );

			// Liberer
			NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

			// Quitter
			return NULL;
		}

		// Copier
		if( code > 0 )
		{
			// Ajouter donnees
			if( !NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				buffer,
				(NU32)code ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_MEMORY );

				// Liberer
				NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

				// Quitter
				return NULL;
			}
		}
		else
			break;

		// Attendre
		NLib_Temps_Attendre( 1 );
	} while( code > 0 );

	// Verifier
	if( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) == 0 )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SOCKET_RECV );

		// Detruire le packet
		NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

		// Quitter
		return NULL;
	}

	// OK
	return packet;
}

/**
 * Envoyer data methode raw
 *
 * @param data
 * 		Les donnees
 * @param tailleData
 * 		La taille des donnees
 * @param socket
 * 		La socket sur laquelle envoyer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerDataRaw( const void *data,
	NU32 tailleData,
	SOCKET socket )
{
	// Taille a envoyer
	NU32 tailleAEnvoyer;

	// Taille envoyee
	NU32 tailleEnvoyee = 0;

	// Code send
	ssize_t code;

	// Envoyer
	while( tailleEnvoyee < tailleData )
	{
		// Calculer la taille a envoyer
		tailleAEnvoyer = ( ( tailleData - tailleEnvoyee >= NBUFFER_PACKET ) ?
			NBUFFER_PACKET
			: tailleData - tailleEnvoyee );

		// Envoyer
		if( ( code = send( socket,
			data + tailleEnvoyee,
			tailleAEnvoyer,
			0 ) ) <= 0 )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_SOCKET_SEND );

			// Quitter
			return NFALSE;
		}

		// Ajouter
		tailleEnvoyee += code;
	}

	// OK
	return NTRUE;
}

/**
 * Envoyer packet methode Raw
 *
 * @param packet
 *		Le packet a envoyer
 * @param socket
 *		La socket sur laquelle envoyer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketRaw( const void *packet,
	SOCKET socket )
{
	// Envoyer
	return NLib_Module_Reseau_Packet_PacketIO_EnvoyerDataRaw( NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ),
		socket );
}

/**
 * Envoyer fichier raw
 *
 * @param fichier
 * 		Le fichier binaire
 * @param socket
 * 		La socket sur laquelle envoyer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerFichierRaw( NFichierBinaire *fichier,
	SOCKET socket )
{
	// Taille contenu
	NU64 tailleFichier;

	// Taille envoyee
	NU64 tailleEnvoyee = 0;

	// Taille a envoyer
	NU32 tailleAEnvoyer;

	// Buffer
	char buffer[ NPACKETIO_TAILLE_BUFFER_FICHIER ];

	// Recuperer la taille fichier
	tailleFichier = NLib_Fichier_NFichierBinaire_ObtenirTaille( fichier );

	// Se placer au debut du fichier
	NLib_Fichier_NFichierBinaire_DefinirPositionDebut( fichier );

	// Envoyer
	while( tailleEnvoyee < tailleFichier )
	{
		// Vider le buffer
		memset( buffer,
			0,
			NPACKETIO_TAILLE_BUFFER_FICHIER );

		// Calculer la taille a envoyer
		tailleAEnvoyer = (NU32)( ( tailleFichier - tailleEnvoyee ) >= NPACKETIO_TAILLE_BUFFER_FICHIER ?
			NPACKETIO_TAILLE_BUFFER_FICHIER
			: ( tailleFichier - tailleEnvoyee ) );

		// Recuperer dans le fichier
		if( tailleAEnvoyer != fread( buffer,
			sizeof( char ),
			tailleAEnvoyer,
			NLib_Fichier_NFichierBinaire_ObtenirFichier( fichier ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

			// Quitter
			return NFALSE;
		}

		// Envoyer
		if( !NLib_Module_Reseau_Packet_PacketIO_EnvoyerDataRaw( buffer,
			tailleAEnvoyer,
			socket ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_SOCKET_SEND );

			// Quitter
			return NFALSE;
		}

		// Incrementer la taille envoyee
		tailleEnvoyee += tailleAEnvoyer;
	}

	// Restaurer position fichier
	NLib_Fichier_NFichierBinaire_DefinirPositionDebut( fichier );

	// OK
	return NTRUE;
}

/**
 * Recevoir un packet protocole UDP
 *
 * @param socket
 *		La socket sur laquelle recevoir
 *
 * @return le packet recu
 */
__ALLOC struct NPacketUDP *NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketUDP( SOCKET socket )
{
	// Sortie
	__ALLOC NPacketUDP *out;

	// Contexte
	SOCKADDR_IN contexte;

	// Taille du contexte d'adressage
#ifdef IS_WINDOWS
	NS32 tailleContexte;
#else // IS_WINDOWS
	socklen_t tailleContexte;
#endif // !IS_WINDOWS

	// Buffer
	char buffer[ NBUFFER_PACKET_UDP ];

	// Taille lue
	ssize_t taille;

	// Enregistrer taille contexte
	tailleContexte = sizeof( SOCKADDR_IN );

	// Lire
	if( ( taille = recvfrom( socket,
		buffer,
		NBUFFER_PACKET_UDP,
		0,
		(struct sockaddr *)&contexte,
		&tailleContexte ) ) <= 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_RECV );

		// Quitter
		return NULL;
	}

	// Construire le packet
	if( !( out = NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire2( buffer,
		(NU32)taille,
		&contexte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Envoyer un packet protocole UDP
 *
 * @param packet
 *		Le packet a envoyer
 * @param socket
 *		La socket sur laquelle envoyer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketUDP( const struct NPacketUDP *packet,
	SOCKET socket )
{
	return NLib_Module_Reseau_UDP_Packet_Usine_EnvoyerPacketUDP( packet,
		socket );
}

#endif // NLIB_MODULE_RESEAU

