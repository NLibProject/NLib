#include "../../../../../include/NLib/NLib.h"
#include "../../../../../include/NLib/Module/Reseau/Packet/NLib_Module_Reseau_Packet.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------------------
// struct NLib::Module::Reseau::Packet::NCachePacket
// -------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

/* Construire cache packet */
__ALLOC NCachePacket *NLib_Module_Reseau_Packet_NCachePacket_Construire( NTypeCachePacket type )
{
	// Output
	__OUTPUT NCachePacket *out;

	// Allouer la mamoire
	if( !( out = calloc( 1,
		sizeof( NCachePacket ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_type = type;

	// Dafinir les callbacks
	switch( out->m_type )
	{
		case NTYPE_CACHE_PACKET_TCP:
			out->m_callbackCopie = (void *( ___cdecl * )( const void* ))NLib_Module_Reseau_Packet_NPacket_Construire2;
			out->m_callbackDestruction = (void ( ___cdecl * )( void* ))NLib_Module_Reseau_Packet_NPacket_Detruire;
			break;

		case NTYPE_CACHE_PACKET_UDP:
			out->m_callbackCopie = (void *( ___cdecl * )( const void* ))NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire4;
			out->m_callbackDestruction = (void ( ___cdecl * )( void* ))NLib_Module_Reseau_UDP_Packet_NPacketUDP_Detruire;
			break;

		case NTYPE_CACHE_PACKET_PERSONNALISE:
			out->m_callbackCopie = (void *( ___cdecl * )( const void* ))NLib_Module_Reseau_Packet_NPacketPersonnalise_Construire3;
			out->m_callbackDestruction = (void ( ___cdecl * )( void* ))NLib_Module_Reseau_Packet_NPacketPersonnalise_Detruire;
			break;

		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
	}

	// Construire liste packets
	if( !( out->m_packet = NLib_Memoire_NListe_Construire( out->m_callbackDestruction ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MUTEX );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

// Datruire cache packet
void NLib_Module_Reseau_Packet_NCachePacket_Detruire( NCachePacket **this )
{
	// Supprimer tous les packets
	NLib_Module_Reseau_Packet_NCachePacket_Vider( *this );

	// Datruire liste
	NLib_Memoire_NListe_Detruire( &(*this)->m_packet );

	// Liberer la mamoire
	NFREE( (*this) );
}

__MUSTBEPROTECTED void NLib_Module_Reseau_Packet_NCachePacket_Vider( NCachePacket *this )
{
	while( NLib_Module_Reseau_Packet_NCachePacket_SupprimerPacket( this ) )
		NLib_Temps_Attendre( 1 );
}

// Obtenir le nombre de packets
__WILLLOCK __WILLUNLOCK NU32 NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( const NCachePacket *this )
{
	// Rasultat
	__OUTPUT NU32 out;

	// Lock le mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_packet );

	// Copier le rasultat
	out = NLib_Memoire_NListe_ObtenirNombre( this->m_packet );

	// Unlock le mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_packet );

	// OK
	return out;
}

// Obtenir le packet a envoyer
__WILLLOCK __WILLUNLOCK __ALLOC void *NLib_Module_Reseau_Packet_NCachePacket_ObtenirPacket( const NCachePacket *this )
{
	// NPacket
	__OUTPUT void *packet;

	// Lock le mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_packet );

	// Varifier le contenu du tableau de packet
	if( NLib_Memoire_NListe_ObtenirNombre( this->m_packet ) == 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Unlock mutex
		NLib_Memoire_NListe_DesactiverProtection( this->m_packet );

		// Quitter
		return NULL;
	}

	// Copier le packet
	if( !( packet = this->m_callbackCopie( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_packet,
		0 ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Unlock le mutex
		NLib_Memoire_NListe_DesactiverProtection( this->m_packet );

		// Quitter
		return NULL;
	}

	// Unlock le mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_packet );

	// OK
	return packet;
}

// Est packet(s) dans le cache?
NBOOL NLib_Module_Reseau_Packet_NCachePacket_EstPacketsDansCache( const NCachePacket *this )
{
	return (NBOOL)( NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( this ) > 0 );
}

// Ajouter un packet
__WILLLOCK __WILLUNLOCK NBOOL NLib_Module_Reseau_Packet_NCachePacket_AjouterPacket( NCachePacket *this,
	__WILLBEOWNED void *packet )
{
	// Rasultat
	__OUTPUT NBOOL resultat;

	// Lock le mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_packet );

	// Ajouter packet
	resultat = NLib_Memoire_NListe_Ajouter( this->m_packet,
		packet );

	// Release le mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_packet );

	// OK
	return resultat;
}

NBOOL NLib_Module_Reseau_Packet_NCachePacket_AjouterPacketCopie( NCachePacket *this,
	const void *packet )
{
	// Packet a ajouter
	void *copiePacket;

	// Copier packet suivant methode packet
	if( !( copiePacket = this->m_callbackCopie( packet ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Exacuter l'ajout
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacket( this,
		copiePacket );
}

// Supprimer le packet
__WILLLOCK __WILLUNLOCK NBOOL NLib_Module_Reseau_Packet_NCachePacket_SupprimerPacket( NCachePacket *this )
{
	// Rasultat
	__OUTPUT NBOOL resultat;

	// Lock mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_packet );

	// Supprimer packet 0
	resultat = NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_packet,
		0 );

	// Release mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_packet );

	// OK
	return resultat;
}

/**
 * Obtenir le type de cache
 *
 * @param this
 *		Cette instance
 *
 * @return le type de cache
 */
NTypeCachePacket NLib_Module_Reseau_Packet_NCachePacket_ObtenirTypeCache( const NCachePacket *this )
{
	return this->m_type;
}

#endif // NLIB_MODULE_RESEAU

