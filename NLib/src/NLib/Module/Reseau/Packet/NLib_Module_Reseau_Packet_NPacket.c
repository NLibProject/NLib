#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// --------------------------------------------
// struct NLib::Module::Reseau::Packet::NPacket
// --------------------------------------------

#ifdef NLIB_MODULE_RESEAU
// Construire le packet
__ALLOC NPacket *NLib_Module_Reseau_Packet_NPacket_Construire( void )
{
	// Output
	__OUTPUT NPacket *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NPacket ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

__ALLOC NPacket *NLib_Module_Reseau_Packet_NPacket_Construire2( const NPacket *src )
{
	// Output
	__OUTPUT NPacket *out;

	// Construire le packet
	if( !( out = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Copier la taille
	out->m_taille = src->m_taille;

	// Allouer la memoire
	if( !( out->m_data = calloc( out->m_taille,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer la memoire
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier les donnees
	memcpy( out->m_data,
		src->m_data,
		src->m_taille * sizeof( char ) );

	// OK
	return out;
}

__ALLOC NPacket *NLib_Module_Reseau_Packet_NPacket_Construire3( const char *data,
	NU32 taille )
{
	// Sortie
	__OUTPUT NPacket *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NPacket ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer la memoire donnees
	if( !( out->m_data = calloc( taille,
		sizeof( NS8 ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier
	out->m_taille = taille;
	memcpy( out->m_data,
		data,
		out->m_taille * sizeof( NS8 ) );

	// OK
	return out;
}

// Detruire le packet
void NLib_Module_Reseau_Packet_NPacket_Detruire( NPacket **this )
{
	// Liberer les donnees
	NFREE( (*this)->m_data );

	// Liberer la memoire
	NFREE( (*this) );
}

// Obtenir la taille
NU32 NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( const NPacket *this )
{
	return this->m_taille;
}

// Obtenir les donnees
const char *NLib_Module_Reseau_Packet_NPacket_ObtenirData( const NPacket *this )
{
	return this->m_data;
}

// Ajout donnees
NBOOL NLib_Module_Reseau_Packet_NPacket_AjouterData( NPacket *this,
	const char *data,
	NU32 taille )
{
	// Ajouter donnees
	if( !NLib_Memoire_AjouterData( &this->m_data,
		this->m_taille,
		data,
		taille ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MEMORY );

		// Quitter
		return NFALSE;
	}

	// Augmenter la taille
	this->m_taille += taille;

	// OK
	return NTRUE;
}

#endif // NLIB_MODULE_RESEAU

