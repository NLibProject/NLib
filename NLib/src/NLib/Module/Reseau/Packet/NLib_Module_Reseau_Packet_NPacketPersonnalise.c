#include "../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU

// --------------------------------------------------------
// struct NLib::Module::Reseau::Packet::NPacketPersonnalise
// --------------------------------------------------------

/**
 * Construire un packet avec des donnees utilisateur copiables/destructibles
 *
 * @param callbackCopie
 *		Le callback utilise pour copier les donnees
 * @param callbackDestruction
 * 		Le callback de destruction des donnees
 * @param data
 *		Les donnees utilisateur
 *
 * @return l'instance
 */
__ALLOC NPacketPersonnalise *NLib_Module_Reseau_Packet_NPacketPersonnalise_Construire( void *( ___cdecl *callbackCopie )( const void* ),
	void ( ___cdecl *callbackDestruction )( void* ),
	__WILLBEOWNED void *data )
{
	// Sortie
	__OUTPUT NPacketPersonnalise *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NPacketPersonnalise ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire data utilisateur
		callbackDestruction( &data );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_callbackCopie = callbackCopie;
	out->m_callbackDestruction = callbackDestruction;
	out->m_data = data;

	// Creer le packet
	if( !( out->m_packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire data utilisateur
		callbackDestruction( &data );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Modifiable
	out->m_estDataConstante = NFALSE;

	// OK
	return out;
}

/**
 * Construire un packet avec des donnees utilisateur constantes (ni copie ni destruction)
 *
 * @param data
 *		Les donnees utilisateur
 *
 * @return l'instance
 */
__ALLOC NPacketPersonnalise *NLib_Module_Reseau_Packet_NPacketPersonnalise_Construire2( void *data )
{
	// Sortie
	__OUTPUT NPacketPersonnalise *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NPacketPersonnalise ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_data = data;

	// Creer le packet
	if( !( out->m_packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Constant
	out->m_estDataConstante = NTRUE;

	// OK
	return out;
}

/**
 * Construire a partir d'un packet
 *
 * @param packet
 *		Le packet a copier
 *
 * @return la nouvelle instance
 */
__ALLOC NPacketPersonnalise *NLib_Module_Reseau_Packet_NPacketPersonnalise_Construire3( const NPacketPersonnalise *packet )
{
	// Sortie
	__OUTPUT NPacketPersonnalise *out;

    // Allouer la memoire
    if( !( out = calloc( 1,
		sizeof( NPacketPersonnalise ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier le packet
	if( !( out->m_packet = NLib_Module_Reseau_Packet_NPacket_Construire2( packet->m_packet ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Si il y a des donnees personnalisees
	if( !packet->m_estDataConstante )
		// Copier les donnees
		if( !( out->m_data = packet->m_callbackCopie( packet->m_data ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire packet
			NLib_Module_Reseau_Packet_NPacket_Detruire( &out->m_packet );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Enregistrer
	out->m_callbackCopie = packet->m_callbackCopie;
	out->m_callbackDestruction = packet->m_callbackDestruction;
	out->m_estDataConstante = packet->m_estDataConstante;

	// OK
	return out;
}

/**
 * Detruire un packet
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_Packet_NPacketPersonnalise_Detruire( NPacketPersonnalise **this )
{
	// Un packet est explicitement defini?
	if( (*this)->m_packet != NULL )
		// Detruire le packet
		NLib_Module_Reseau_Packet_NPacket_Detruire( &(*this)->m_packet );

	// Donnees destructibles
	if( !(*this)->m_estDataConstante )
		// Il y a des donnes?
		if( (*this)->m_data != NULL )
			// Detruire
			(*this)->m_callbackDestruction( &(*this)->m_data );

	// Liberer
	NFREE( *this );
}

/**
 * Obtenir le packet
 *
 * @param this
 *		Cette instance
 *
 * @return le packet
 */
const NPacket *NLib_Module_Reseau_Packet_NPacketPersonnalise_ObtenirPacket( const NPacketPersonnalise *this )
{
    return this->m_packet;
}

/**
 * Ajouter des donnees
 *
 * @param this
 *		Cette instance
 * @param data
 *		Les donnees a ajouter
 * @param tailleData
 *		La taille des donnees a ajouter
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Packet_NPacketPersonnalise_AjouterData( NPacketPersonnalise *this,
	const char *data,
	NU32 tailleData )
{
	return NLib_Module_Reseau_Packet_NPacket_AjouterData( this->m_packet,
		data,
		tailleData );
}

/**
 * Obtenir donnees
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees utilisateur
 */
void *NLib_Module_Reseau_Packet_NPacketPersonnalise_ObtenirDataPersonnalise( const NPacketPersonnalise *this )
{
	return this->m_data;
}

#endif // NLIB_MODULE_RESEAU

