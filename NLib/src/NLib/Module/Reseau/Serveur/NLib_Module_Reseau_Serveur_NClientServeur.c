#include "../../../../../include/NLib/NLib.h"

/**
 *	@author SOARES Lucas
 */

// ----------------------------------------------------
// struct NLib::Module::Reseau::Serveur::NClientServeur
// ----------------------------------------------------

#ifdef NLIB_MODULE_RESEAU
/**
 * Construire un client serveur
 *
 * @param socketServeur
 *		La socket du serveur
 * @param callbackReception
 *		Le callback appele a la reception d'un packet
 * @param estConnexionAutorisee
 *		La connexion est-elle autorisee actuellement?
 * @param tempsAvantTimeoutReception
 *		Le temps avant timeout reception
 * @param tempsAvantTimeoutEmission
 * 		Le timeout emission
 * @param type
 *		Le type de serveur
 * @param typePacketEmis
 * 		Le type de packet utilise a l'emission
 * @param callbackMethodeReception
 *		La methode de reception
 * @param callbackMethodeEmission
 *		La methode d'emission
 *
 * @return l'instance du client
 */
__ALLOC NClientServeur *NLib_Module_Reseau_Serveur_NClientServeur_Construire( SOCKET socketServeur,
	__CALLBACK NBOOL ( *callbackReception )( const NClientServeur*,
		const NPacket* ),
	const NBOOL *estConnexionAutorisee,
	NU32 tempsAvantTimeoutReception,
	NU32 tempsAvantTimeoutEmission,
	NTypeServeur type,
	NTypeCachePacket typePacketEmis,
	__CALLBACK __ALLOC NPacket *( ___cdecl *callbackMethodeReception )( SOCKET socket,
		NBOOL estTimeoutAutorise ),
	__CALLBACK NBOOL ( ___cdecl *callbackMethodeEmission )( const void *packet,
		SOCKET socket ) )
{
	// Sortie
	__OUTPUT NClientServeur *out;

	// Socket client
	SOCKET socketClient;

	// Adressage socket client
	SOCKADDR_IN addrSocketClientTCP;
#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
	struct sockaddr_rc addrSocketClientBluetooth;
#endif // NLIB_MODULE_RESEAU_BLUETOOTH

	// Taille du contexte d'adressage
#ifdef IS_WINDOWS
	NS32 tailleContexteAdressage;
#else // IS_WINDOWS
	socklen_t tailleContexteAdressage;
	struct timeval timeout;
#endif // !IS_WINDOWS

	// Definir taille contexte adressage
	switch( type )
	{
		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

			// Quitter
			return NULL;

		case NTYPE_SERVEUR_TCP:
			// Taille adressage TCP
			tailleContexteAdressage = sizeof( SOCKADDR_IN );

			// Accepter la requete
			if( ( socketClient = (SOCKET)accept( socketServeur,
				(struct sockaddr*)&addrSocketClientTCP,
				&tailleContexteAdressage ) ) == INVALID_SOCKET )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SOCKET_ACCEPT_FAILED );

				// Quitter
				return NULL;
			}
			break;

#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
		case NTYPE_SERVEUR_BLUETOOTH:
			// Taille adressage bluetooth
			tailleContexteAdressage = sizeof( struct sockaddr_rc );

			// Accepter client
			if( ( socketClient = accept( socketServeur,
				(struct sockaddr*)&addrSocketClientBluetooth,
				&tailleContexteAdressage ) ) == INVALID_SOCKET )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SOCKET_ACCEPT_FAILED );

				// Quitter
				return NULL;
			}
			break;
#endif // NLIB_MODULE_RESEAU_BLUETOOTH
	}

	// Allouer le client
	if( !( out = calloc( 1,
		sizeof( NClientServeur ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Fermer la socket
		closesocket( socketClient );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_socket = socketClient;
	out->m_type = type;
	out->m_typePacketEmis = typePacketEmis;
	out->m_callbackReception = callbackReception;
	out->m_tempsAvantTimeoutReception = tempsAvantTimeoutReception;
	out->m_callbackMethodeReception = callbackMethodeReception;
	out->m_callbackMethodeEmission = callbackMethodeEmission;

	// Enregistrer adresse
	switch( type )
	{
		case NTYPE_SERVEUR_TCP:
			memcpy( &out->m_contexteAdressage.tcp,
				&addrSocketClientTCP,
				sizeof( SOCKADDR_IN ) );
			break;
#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
		case NTYPE_SERVEUR_BLUETOOTH:
			memcpy( &out->m_contexteAdressage.bluetooth,
				&addrSocketClientBluetooth,
				sizeof( struct sockaddr_rc ) );
			break;
#endif // NLIB_MODULE_RESEAU_BLUETOOTH

		default:
			break;
	}

	// Verifier si la connexion est autorisee
	if( !(*estConnexionAutorisee) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SERVER_CLOSED );

		// Fermer la socket
		closesocket( out->m_socket );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Si on a un timeout reception
	if( tempsAvantTimeoutReception > 0 )
	{
#ifdef IS_WINDOWS
		// Definir le timeout
		setsockopt( out->m_socket,
			SOL_SOCKET,
			SO_RCVTIMEO,
			(char*)&tempsAvantTimeout,
			sizeof( NU32 ) );
#else // IS_WINDOWS
		// Definir le timeout
		timeout.tv_sec = tempsAvantTimeoutReception / 1000;
		timeout.tv_usec = ( tempsAvantTimeoutReception % 1000 ) * 1000;

		// Definir le timeout
		setsockopt( out->m_socket,
			SOL_SOCKET,
			SO_RCVTIMEO,
			&timeout,
			sizeof( struct timeval ) );
#endif // !IS_WINDOWS
	}

	// Si on a un timeout emission
	if( tempsAvantTimeoutReception > 0 )
	{
#ifdef IS_WINDOWS
		// Definir le timeout
		setsockopt( out->m_socket,
			SOL_SOCKET,
			SO_RCVTIMEO,
			(char*)&tempsAvantTimeout,
			sizeof( NU32 ) );
#else // IS_WINDOWS
		// Definir le timeout
		timeout.tv_sec = tempsAvantTimeoutEmission / 1000;
		timeout.tv_usec = ( tempsAvantTimeoutEmission % 1000 ) * 1000;

		// Definir le timeout
		setsockopt( out->m_socket,
			SOL_SOCKET,
			SO_SNDTIMEO,
			&timeout,
			sizeof( struct timeval ) );
#endif // !IS_WINDOWS
	}

	// Obtenir l'adresse
	switch( type )
	{
		case NTYPE_SERVEUR_TCP:
			out->m_adresse = NLib_Module_Reseau_IP_ObtenirAdresseIP( out->m_contexteAdressage.tcp );
			break;

#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
		case NTYPE_SERVEUR_BLUETOOTH:
			out->m_adresse = NLib_Module_Reseau_Serveur_Bluetooth_ObtenirAdresseMAC( out->m_contexteAdressage.bluetooth );
			break;
#endif // NLIB_MODULE_RESEAU_BLUETOOTH

		default:
			break;
	}

	// Verifier qu'on ait bien obtenu l'adresse
	if( !out->m_adresse )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Fermer la socket
		closesocket( out->m_socket );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer le cache packet
	if( !( out->m_cachePacket = NLib_Module_Reseau_Packet_NCachePacket_Construire( typePacketEmis ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Fermer la socket
		closesocket( out->m_socket );

		// Liberer
		NFREE( out->m_adresse );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire le ping
	if( !( out->m_ping = NLib_Module_Reseau_NPing_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Fermer la socket
		closesocket( out->m_socket );

		// Liberer
		NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

		NFREE( out->m_adresse );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Zero
	out->m_estMort = NFALSE;
	out->m_estEnCours = NTRUE;

	// Creer les threads
		// Emission
			if( !( out->m_threadEmission = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl *)( void* ))NLib_Module_Reseau_Serveur_Thread_Client_ThreadEmission,
				out,
				&out->m_estEnCours ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Detruire le ping
				NLib_Module_Reseau_NPing_Detruire( &out->m_ping );

				// Fermer la socket
				closesocket( out->m_socket );

				// Liberer
				NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

				NFREE( out->m_adresse );
				NFREE( out );

				// Quitter
				return NULL;
			}
		// Reception
			if( !( out->m_threadReception = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl *)( void* ))NLib_Module_Reseau_Serveur_Thread_Client_ThreadReception,
				out,
				&out->m_estEnCours ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Arreter le thread
				NLib_Thread_NThread_Detruire( &out->m_threadEmission );

				// Detruire le ping
				NLib_Module_Reseau_NPing_Detruire( &out->m_ping );

				// Fermer la socket
				closesocket( out->m_socket );

				// Liberer
				NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

				NFREE( out->m_adresse );
				NFREE( out );

				// Quitter
				return NULL;
			}

	// OK
	return out;
}

// Detruire le client
void NLib_Module_Reseau_Serveur_NClientServeur_Detruire( NClientServeur **this )
{
	// Detruire les threads
    NLib_Thread_NThread_Detruire( &(*this)->m_threadEmission );
    NLib_Thread_NThread_Detruire( &(*this)->m_threadReception );

	// Fermer la socket
	closesocket( (*this)->m_socket );

	// Detruire ping
	NLib_Module_Reseau_NPing_Detruire( &(*this)->m_ping );

	// Liberer socket
	NFREE( (*this)->m_adresse );

	// Detruire le cache packet
	NLib_Module_Reseau_Packet_NCachePacket_Detruire( &(*this)->m_cachePacket );

	// Liberer la memoire
	NFREE( (*this) );
}

// Est mort?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstMort( const NClientServeur *this )
{
	return this->m_estMort;
}

// Est en cours?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstEnCours( const NClientServeur *this )
{
	return this->m_estEnCours;
}

// Est packet(s) dans le cache?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstPacketsDansCache( const NClientServeur *this )
{
	return NLib_Module_Reseau_Packet_NCachePacket_EstPacketsDansCache( this->m_cachePacket );
}

// Tuer un client
void NLib_Module_Reseau_Serveur_NClientServeur_Tuer( NClientServeur *this )
{
	this->m_estMort = NTRUE;
}

// Definir identifiant
void NLib_Module_Reseau_Serveur_NClientServeur_DefinirIdentifiantUnique( NClientServeur *this,
	NU32 identifiant )
{
	this->m_identifiantUnique = identifiant;
}

// Obtenir l'identifiant
NU32 NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( const NClientServeur *this )
{
	return this->m_identifiantUnique;
}

// Obtenir SOCKET
SOCKET NLib_Module_Reseau_Serveur_NClientServeur_ObtenirSocket( const NClientServeur *this )
{
	return this->m_socket;
}

// Obtenir cache packet
NCachePacket *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirCachePacket( const NClientServeur *this )
{
	return this->m_cachePacket;
}

/**
 * Obtenir adresse
 *
 * @param this
 *		Cette instance
 *
 * @return l'adresse
 */
const char *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( const NClientServeur *this )
{
	return this->m_adresse;
}

// Donner un handle de donnee au client
void NLib_Module_Reseau_Serveur_NClientServeur_DefinirDonneeExterne( NClientServeur *this,
	char *data )
{
	this->m_donneeExterne = data;
}

// Obtenir les donnees externes enregistrees pour ce client
char *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( const NClientServeur *this )
{
	return this->m_donneeExterne;
}

// Obtenir ping
const NPing *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirPing( const NClientServeur *this )
{
	return this->m_ping;
}

// Ajouter un packet
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( NClientServeur *this,
	__WILLBEOWNED void *packet )
{
	// Ajouter le packet au cache
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacket( this->m_cachePacket,
		packet );
}

NBOOL NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacketCopie( NClientServeur *this,
	const void *packet )
{
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacketCopie( this->m_cachePacket,
		packet );
}

/**
 * On a un timeout en reception?
 *
 * @param this
 * 		Cette instance
 *
 * @return si on a un timeout reception
 */
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstTimeoutAutoriseReception( const NClientServeur *this )
{
	return (NBOOL)( this->m_tempsAvantTimeoutReception > 0 );
}

/**
 * Obtenir le timeout reception
 *
 * @param this
 * 		Cette instance
 *
 * @return le timeout reception
 */
NU32 NLib_Module_Reseau_Serveur_NClientServeur_ObtenirTempsAvantTimeoutReception( const NClientServeur *this )
{
	return this->m_tempsAvantTimeoutReception;
}

/**
 * On a un timeout en emission?
 *
 * @param this
 * 		Cette instance
 *
 * @return si on a un timeout emission
 */
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstTimeoutAutoriseEmission( const NClientServeur *this )
{
	return (NBOOL)( this->m_tempsAvantTimeoutEmission > 0 );
}

/**
 * Obtenir le timeout emission
 *
 * @param this
 * 		Cette instance
 *
 * @return le timeout emission
 */
NU32 NLib_Module_Reseau_Serveur_NClientServeur_ObtenirTempsAvantTimeoutEmission( const NClientServeur *this )
{
	return this->m_tempsAvantTimeoutEmission;
}

#endif // NLIB_MODULE_RESEAU

