#include "../../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU

#ifdef NLIB_MODULE_RESEAU_BLUETOOTH

// --------------------------------------------------
// namespace NLib::Module::Reseau::Serveur::Bluetooth
// --------------------------------------------------

/**
 * Creer socket
 *
 * @param canal
 *		Le canal d'ecoute
 *
 * @return la socket
 */
SOCKET NLib_Module_Reseau_Serveur_Bluetooth_CreerSocket( NU32 canal )
{
	// Adressage
	struct sockaddr_rc adressage;

	// Socket
	__OUTPUT SOCKET sock;

	// Adresser
	adressage.rc_channel = (NU32)canal;
	adressage.rc_bdaddr = *BDADDR_ANY;
	adressage.rc_family = AF_BLUETOOTH;

	// Creer la socket
	if( ( sock = socket( AF_BLUETOOTH,
		AF_INET,
		BTPROTO_RFCOMM ) ) == INVALID_SOCKET )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET );

		// Quitter
		return INVALID_SOCKET;
	}

	// Bind la socket
	if( bind( sock,
		(struct sockaddr*)&adressage,
		sizeof( struct sockaddr_rc ) ) == INVALID_SOCKET )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_BIND );

		// Fermer la socket
		closesocket( sock );

		// Quitter
		return INVALID_SOCKET;
	}

	// OK
	return sock;
}

/**
 * Obtenir l'adresse MAC a partir de l'adressage
 *
 * @param adressage
 *		Le contexte d'adressage
 *
 * @return l'adresse MAC
 */
__ALLOC char *NLib_Module_Reseau_Serveur_Bluetooth_ObtenirAdresseMAC( struct sockaddr_rc adressage )
{
	// Sortie
	__OUTPUT char *out;

	// Allouer la memoire
	if( !( out = calloc( NTAILLE_ADRESSE_MAC + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Convertir
	ba2str( &adressage.rc_bdaddr,
		out );

	// OK
	return out;
}

#endif // NLIB_MODULE_RESEAU_BLUETOOTH

#endif // NLIB_MODULE_RESEAU

