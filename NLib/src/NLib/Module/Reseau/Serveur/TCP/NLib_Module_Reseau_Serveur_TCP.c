#include "../../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU
// --------------------------------------------
// namespace NLib::Module::Reseau::Serveur::TCP
// --------------------------------------------

/**
 * Creer la socket
 *
 * @param port
 *		Le port d'ecoute
 * @param listeningIP
 * 		The listening IP (can be NULL for listening on all interfaces)
 *
 * @return la socket
 */
SOCKET NLib_Module_Reseau_Serveur_TCP_CreerSocket( NU32 port,
	const char *listeningIP )
{
	// Adressage
	SOCKADDR_IN adressage;

	// Sortie
	__OUTPUT SOCKET sock;

	// Parametrer l'adressage
	adressage.sin_family = AF_INET;
	adressage.sin_port = htons( (NU16)port );

	// Ecouter sur toutes les interfaces?
	if( listeningIP == NULL )
		adressage.sin_addr.FIN_ADRESSE = htonl( INADDR_ANY );
	// Ecouter sur une seule interface?
	else
		adressage.sin_addr.FIN_ADRESSE = inet_addr( listeningIP );

	// Creer la socket
	if( ( sock = (SOCKET)socket( AF_INET,
		SOCK_STREAM,
		0 ) ) == INVALID_SOCKET )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET );

		// Quitter
		return SOCKET_ERROR;
	}

	// Bind la socket
	if( bind( sock,
		(const struct sockaddr*)&adressage,
		sizeof( SOCKADDR_IN ) ) == SOCKET_ERROR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_BIND );

		// Fermer la socket
		closesocket( sock );

		// Quitter
		return SOCKET_ERROR;
	}

	// OK
	return sock;
}

#endif // !NLIB_MODULE_RESEAU

