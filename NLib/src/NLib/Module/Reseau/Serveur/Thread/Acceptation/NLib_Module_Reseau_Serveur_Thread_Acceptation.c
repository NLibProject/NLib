#include "../../../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ------------------------------------------------------------
// namespace NLib::Module::Reseau::Serveur::Thread::Acceptation
// ------------------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// Thread de gestion des acceptation clients serveur
NBOOL NLib_Module_Reseau_Serveur_Thread_Acceptation_Thread( NServeur *serveur )
{
	// Boucle d'acceptation
	do
	{
		// Accepter client
		NLib_Module_Reseau_Serveur_NServeur_AccepterClient( serveur );

		// Delais
		NLib_Temps_Attendre( 1 );
	} while( NLib_Module_Reseau_Serveur_NServeur_EstEnCours( serveur ) );

	// OK
	return NTRUE;
}

#endif // NLIB_MODULE_RESEAU

