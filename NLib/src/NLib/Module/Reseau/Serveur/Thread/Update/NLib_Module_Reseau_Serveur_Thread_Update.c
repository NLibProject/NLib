#include "../../../../../../../include/NLib/NLib.h"

// -------------------------------------------------------
// namespace NLib::Module::Reseau::Serveur::Thread::Update
// -------------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

/**
 * Thread de nettoyage
 *
 * @param this
 *		Cette instance du serveur
 *
 * @return si l'operation s'est bien passe
 */
__THREAD NBOOL NLib_Module_Reseau_Serveur_Thread_Update_Thread( NServeur *this )
{
	// Nombre de clients nettoyes
	NU32 clientsNettoyes;

#define TAILLE_BUFFER_TITRE_CONSOLE		2048

	// Buffer pour le titre de la console
	char buffer[ TAILLE_BUFFER_TITRE_CONSOLE ];

	do
	{
		// Vider buffer
		memset( buffer,
			0,
			TAILLE_BUFFER_TITRE_CONSOLE );

		/* Nettoyage */
		clientsNettoyes = NLib_Module_Reseau_Serveur_NServeur_NettoyerClients( this );

		// Si il y a eu du nettoyage
		if( clientsNettoyes > 0 )
		{
			// Creer le message
			sprintf( buffer,
				"[SERVEUR] %d client%s nettoye%s.\n",
					clientsNettoyes,
					( clientsNettoyes > 1 ) ? "s" : "",
					( clientsNettoyes > 1 ) ? "s" : "" );

			// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_VERBOSE,
				buffer,
				0 );
		}

		// Attendre
		NLib_Temps_Attendre( 200 );
	} while( this->m_estEnCours );

	// OK
	return NTRUE;
}

#endif // NLIB_MODULE_RESEAU

