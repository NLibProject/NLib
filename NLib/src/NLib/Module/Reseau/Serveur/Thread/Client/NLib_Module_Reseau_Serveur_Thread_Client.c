#include "../../../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------------------------
// namespace NLib::Module::Reseau::Serveur::Thread::Client
// -------------------------------------------------------

#ifdef NLIB_MODULE_RESEAU
/**
 * Le thread d'emission
 *
 * @param client
 * 		Le client
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Serveur_Thread_Client_ThreadEmission( NClientServeur *client )
{
	// Packet
	void *packet;

	// Thread
	do
	{
		// Attendre
		NLib_Temps_Attendre( 16 );

		// Si le client est vivant
		if( !NLib_Module_Reseau_Serveur_NClientServeur_EstMort( client ) )
		{
			// Si il y a quelque chose a inscrire dans le flux
			if( NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirCachePacket( client ) ) > 0 )
				// Si on peut incrire dans le flux
				if( NLib_Module_Reseau_Socket_Selection_EstEcritureDisponible( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirSocket( client ) ) )
				{
					// Recuperer le packet
					if( !( packet = NLib_Module_Reseau_Packet_NCachePacket_ObtenirPacket( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirCachePacket( client ) ) ) )
						continue;

					// Ecrire dans le flux
					if( !client->m_callbackMethodeEmission( packet,
						NLib_Module_Reseau_Serveur_NClientServeur_ObtenirSocket( client ) ) )
					{
						// Notifier
						NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_SOCKET_SEND,
							__FUNCTION__,
							(NU32)errno );

						// Tuer le client
						NLib_Module_Reseau_Serveur_NClientServeur_Tuer( client );
					}

					// Liberer la copie du packet
					switch( NLib_Module_Reseau_Packet_NCachePacket_ObtenirTypeCache( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirCachePacket( client ) ) )
					{
						default:
						case NTYPE_CACHE_PACKET_TCP:
							NLib_Module_Reseau_Packet_NPacket_Detruire( (NPacket**)&packet );
							break;
						case NTYPE_CACHE_PACKET_PERSONNALISE:
							NLib_Module_Reseau_Packet_NPacketPersonnalise_Detruire( (NPacketPersonnalise**)&packet );
							break;
					}

					// Supprimer le packet du cache
					NLib_Module_Reseau_Packet_NCachePacket_SupprimerPacket( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirCachePacket( client ) );
				}
		}
	} while( NLib_Module_Reseau_Serveur_NClientServeur_EstEnCours( client ) );

	// OK
	return NTRUE;
}

/**
 * Le thread de reception
 *
 * @param client
 * 		Le client
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Serveur_Thread_Client_ThreadReception( NClientServeur *client )
{
	// Packet
	NPacket *packet = NULL;

	// Thread
	do
	{
		// Delais
		NLib_Temps_Attendre( 16 );

		// Si le client est vivant
		if( !NLib_Module_Reseau_Serveur_NClientServeur_EstMort( client ) )
		{
			// Si on peut lire dans le flux
			if( NLib_Module_Reseau_Socket_Selection_EstLectureDisponible( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirSocket( client ) ) )
			{
				// Lire dans le flux
				if( !( packet = client->m_callbackMethodeReception( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirSocket( client ),
					NLib_Module_Reseau_Serveur_NClientServeur_EstTimeoutAutoriseReception( client ) ) ) )
				{
					// Notifier
					NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_SOCKET_RECV,
						__FUNCTION__,
						(NU32)errno );

					// Tuer le client
					NLib_Module_Reseau_Serveur_NClientServeur_Tuer( client );
				}
				// Transmettre au callback
				else
				{
					// Si on a un callback de reception
					if( client->m_callbackReception != NULL )
						// Passer au callback de reception
						if( !client->m_callbackReception( client,
							packet ) )
							NLib_Module_Reseau_Serveur_NClientServeur_Tuer( client );

					// Liberer le packet
					NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );
				}
			}
		}
	} while( NLib_Module_Reseau_Serveur_NClientServeur_EstEnCours( client ) );

	// OK
	return NTRUE;
}

#endif // NLIB_MODULE_RESEAU

