#include "../../../../../include/NLib/NLib.h"

/**
 *	Serveur auto-gere pour l'emission et la reception de
 *	packets, ainsi que pour la connexion/deconnexion de
 *	clients.
 *
 *	Communication avec une gestion exterieure via les ca
 *	llbacks:
 *		callbackReceptionPacket: Transmet le packet recu
 *		ainsi que le client ayant recu ce packet
 *		callbackConnexion: Transmet le client qui vient
 *		de se connecter
 *		callbackDeconnexion: Transmet le client qui vien
 *		t de se deconnecter
 *
 *	ATTENTION:
 *	Par defaut, la connexion au serveur est INTERDITE (ap
 *	peler NLib_Module_Reseau_Serveur_NServeur_AutoriserCo
 *	nnexion( ) pour pouvoir se connecter)
 *
 *	@author SOARES Lucas
 */

// ----------------------------------------------
// struct NLib::Module::Reseau::Serveur::NServeur
// ----------------------------------------------

#ifdef NLIB_MODULE_RESEAU
/**
 * Callback reception packet par default (private)
 *
 * @param c
 * 		Le client
 * @param p
 * 		Le packet
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE __CALLBACK NBOOL NLib_Module_Reseau_Serveur_NServeur_CallbackReceptionPacketDefaut( const NClientServeur *client,
	const NPacket *packet )
{
	// Referencer
	NREFERENCER( client );
	NREFERENCER( packet );

	// OK
	return NTRUE;
}

/**
 * Callback connexion par default (private)
 *
 * @param c
 * 		Le client
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE __CALLBACK NBOOL NLib_Module_Reseau_Serveur_NServeur_CallbackConnexionDefaut( const NClientServeur *client )
{
	// Referencer
	NREFERENCER( client );

	// OK
	return NTRUE;
}

/**
 * Callback deconnexion par default (private)
 *
 * @param client
 * 		Le client
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE __CALLBACK NBOOL NLib_Module_Reseau_Serveur_NServeur_CallbackDeconnexionDefaut( const NClientServeur *client )
{
	// Referencer
	NREFERENCER( client );

	// OK
	return NTRUE;
}

/**
 * Construire serveur
 *
 * @param portOuCanal
 *		Le port d'ecoute
 * @param interfaceEcoute
 * 		L'adresse IP de l'interface qui doit ecouter (NULL pour que toutes les interfaces ecoutent)
 * @param callbackReceptionPacket
 *		Le callback de reception
 * @param callbackConnexion
 *		Le callback de connexion client
 * @param callbackDeconnexion
 *		Le callback appele a la deconnexion d'un client
 * @param donneePourClient
 *		Les donnees passees au client
 * @param tempsAvantTimeoutReception
 *		Le temps avant d'etre timeout a la reception (si != 0)
 * @param tempsAvantTimeoutEmission
 * 		Le timeout emission
 * @param type
 *		Le type de serveur
 * @param methodeTransmissionPacketIO
 *		La methode de transmission utilisee
 *
 * @return l'instance du serveur
 */
__ALLOC NServeur *NLib_Module_Reseau_Serveur_NServeur_Construire( NU32 portOuCanal,
	const char *interfaceEcoute,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NClientServeur*,
	const NPacket* ),
	__CALLBACK NBOOL ( *callbackConnexion )( const NClientServeur* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( const NClientServeur* ),
	void *donneePourClient,
	NU32 tempsAvantTimeoutReception,
	NU32 tempsAvantTimeoutEmission,
	NTypeServeur type,
	NMethodeTransmissionPacketIO methodeTransmission )
{
	// Methode reception
	__CALLBACK NPacket *( ___cdecl *callbackMethodeReception )( SOCKET socket,
		NBOOL estTimeoutAutorise );

	// Methode emission
	__CALLBACK NBOOL ( ___cdecl *callbackMethodeEmission )( const void *packet,
		SOCKET socket );

	// Definir la methode
	switch( methodeTransmission )
	{
		case NMETHODE_TRANSMISSION_PACKETIO_RAW:
			callbackMethodeReception = NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketRaw;
			callbackMethodeEmission = NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketRaw;
			break;

		case NMETHODE_TRANSMISSION_PACKETIO_NPROJECT:
			callbackMethodeReception = NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketNProject;
			callbackMethodeEmission = NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketNProject;
			break;

		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

			// Quitter
			return NULL;
	}

	// Construire le serveur
	return NLib_Module_Reseau_Serveur_NServeur_Construire2( portOuCanal,
		interfaceEcoute,
		callbackReceptionPacket,
		callbackConnexion,
		callbackDeconnexion,
		donneePourClient,
		tempsAvantTimeoutReception,
		tempsAvantTimeoutEmission,
		type,
		NTYPE_CACHE_PACKET_TCP,
		callbackMethodeReception,
		callbackMethodeEmission );
}

/**
 * Construire serveur avec methode de transfert personnalisee
 *
 * @param portOuCanal
 *		Le port/canal d'ecoute
 * @param interfaceEcoute
 * 		L'adresse IP de l'interface qui doit ecouter (NULL pour que toutes les interfaces ecoutent)
 * @param callbackReceptionPacket
 *		Le callback de reception
 * @param callbackConnexion
 *		Le callback de connexion client
 * @param callbackDeconnexion
 *		Le callback appele a la deconnexion d'un client
 * @param donneePourClient
 *		Les donnees passees au client
 * @param tempsAvantTimeoutReception
 *		Le temps avant d'etre timeout a la reception (si != 0)
 * @param tempsAvantTimeoutEmission
 * 		Timeout emission
 * @param type
 *		Le type de serveur
 * @param typePacketEmis
 * 		Le type de packet emis
 * @param callbackMethodeReception
 *		La methode de reception
 * @param callbackMethodeEmission
 *		La methode d'emission
 *
 * @return l'instance du serveur
 */
__ALLOC NServeur *NLib_Module_Reseau_Serveur_NServeur_Construire2( NU32 portOuCanal,
	const char *interfaceEcoute,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NClientServeur*,
	const NPacket* ),
	__CALLBACK NBOOL ( *callbackConnexion )( const NClientServeur* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( const NClientServeur* ),
	void *donneePourClient,
	NU32 tempsAvantTimeoutReception,
	NU32 tempsAvantTimeoutEmission,
	NTypeServeur type,
	NTypeCachePacket typePacketEmis,
	__CALLBACK __ALLOC NPacket *( ___cdecl *callbackMethodeReception )( SOCKET socket,
		NBOOL estTimeoutAutorise ),
	__CALLBACK NBOOL ( ___cdecl *callbackMethodeEmission )( const void *packet,
		SOCKET socket ) )
{
		// NServeur
	__OUTPUT NServeur *out;

	// Buffer
	char buffer[ 256 ] = { 0, };

	// Creer le serveur
	if( !( out = calloc( 1,
		sizeof( NServeur ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Creer les clients
	if( !( out->m_clients = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NLib_Module_Reseau_Serveur_NClientServeur_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer la socket
	switch( type )
	{
		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

			// Detruire clients
			NLib_Memoire_NListe_Detruire( &out->m_clients );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;

		case NTYPE_SERVEUR_TCP:
			// Socket TCP
			out->m_socket = NLib_Module_Reseau_Serveur_TCP_CreerSocket( portOuCanal,
				interfaceEcoute );
			break;

#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
		case NTYPE_SERVEUR_BLUETOOTH:
			// Socket bluetooth
			out->m_socket = NLib_Module_Reseau_Serveur_Bluetooth_CreerSocket( portOuCanal );
			break;
#endif // NLIB_MODULE_RESEAU_BLUETOOTH
	}

	// Verifier la creation de la socket
	if( out->m_socket == SOCKET_ERROR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET );

		// Detruire clients
		NLib_Memoire_NListe_Detruire( &out->m_clients );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_callbackReceptionPacket = ( callbackReceptionPacket != NULL ) ?
		callbackReceptionPacket
		: NLib_Module_Reseau_Serveur_NServeur_CallbackReceptionPacketDefaut;
	out->m_callbackConnexion = ( callbackConnexion != NULL ) ?
		callbackConnexion
		: NLib_Module_Reseau_Serveur_NServeur_CallbackConnexionDefaut;
	out->m_callbackDeconnexion = ( callbackDeconnexion != NULL ) ?
		callbackDeconnexion
		: NLib_Module_Reseau_Serveur_NServeur_CallbackDeconnexionDefaut;
	out->m_callbackMethodeReception = callbackMethodeReception;
	out->m_callbackMethodeEmission = callbackMethodeEmission;
	out->m_donneePourClient = donneePourClient;
	out->m_tempsAvantTimeoutReception = tempsAvantTimeoutReception;
	out->m_tempsAvantTimeoutEmission = tempsAvantTimeoutEmission;
	out->m_type = type;
	out->m_typePacketEmis = typePacketEmis;

	// Ecouter les clients
	if( listen( out->m_socket,
		5 ) == SOCKET_ERROR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_LISTEN );

		// Fermer la socket
		closesocket( out->m_socket );

		// Detruire clients
		NLib_Memoire_NListe_Detruire( &out->m_clients );

		// Liberer la memoire
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Zero
	out->m_nombreMaximumClient = NMAXIMUM_CLIENTS_SIMULTANE_NSERVEUR;

	// Demarrer le serveur
	out->m_estEnCours = NTRUE;

	// Creer le thread d'update clients
	if( !( out->m_threadUpdateClient = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NLib_Module_Reseau_Serveur_Thread_Update_Thread,
		out,
		&out->m_estEnCours ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_THREAD );

		// Arreter le serveur
		out->m_estEnCours = NFALSE;

		// Fermer la socket
		closesocket( out->m_socket );

		// Detruire clients
		NLib_Memoire_NListe_Detruire( &out->m_clients );

		// Liberer la memoire
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer le thread d'acceptation
	if( !( out->m_threadAcceptationClient = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl *)( void* ))NLib_Module_Reseau_Serveur_Thread_Acceptation_Thread,
		out,
		NULL ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_THREAD );

		// Tuer thread update
		NLib_Thread_NThread_Detruire( &out->m_threadUpdateClient );

		// Fermer la socket
		closesocket( out->m_socket );

		// Detruire clients
		NLib_Memoire_NListe_Detruire( &out->m_clients );

		// Liberer la memoire
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Notifier
		// Creer le message
			switch( type )
			{
				case NTYPE_SERVEUR_TCP:
					sprintf( buffer,
						"[SERVEUR TCP] Ecoute sur le port %d.",
						portOuCanal );
					break;
#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
				case NTYPE_SERVEUR_BLUETOOTH:
					sprintf( buffer,
						"[SERVEUR Bluetooth] Ecoute sur le canal %d.",
						portOuCanal );
					break;
#endif // NLIB_MODULE_RESEAU_BLUETOOTH

				default:
					break;
			}
		// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_VERBOSE,
				buffer,
				0 );

	// OK
	return out;
}

/**
 * Detruire le serveur
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Serveur_NServeur_Detruire( NServeur **this )
{
	// Interdire la connexion
	NLib_Module_Reseau_Serveur_NServeur_InterdireConnexion( *this );

	// Tuer le thread d'acceptation clients
	NLib_Thread_NThread_Detruire( &(*this)->m_threadAcceptationClient );

	// Tuer tous les clients
	NLib_Module_Reseau_Serveur_NServeur_TuerTousClients( *this );

	// Attendre qu'il n'y ait plus de clients
	while( NLib_Module_Reseau_Serveur_NServeur_ObtenirNombreClients( *this ) > 0 )
		// Attendre
		NLib_Temps_Attendre( 1 );

	// Stopper le thread d'update
	NLib_Thread_NThread_Detruire( &(*this)->m_threadUpdateClient );

	// Fermer la socket
	closesocket( (*this)->m_socket );

	// Detruire clients
	NLib_Memoire_NListe_Detruire( &(*this)->m_clients );

	// Liberer le conteneur
	NFREE( *this );
}

/**
 * Le serveur est en cours d'execution?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le serveur est en cours d'execution
 */
NBOOL NLib_Module_Reseau_Serveur_NServeur_EstEnCours( const NServeur *this )
{
	return this->m_estEnCours;
}

/**
 * Obtenir le nombre de clients connectes actuellement
 *
 * @return le nombre de clients
 */
__WILLLOCK __WILLUNLOCK NU32 NLib_Module_Reseau_Serveur_NServeur_ObtenirNombreClients( const NServeur *this )
{
	// Sortie
	__OUTPUT NU32 nombreClient;

	// Lock le mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_clients );

	// Obtenir nombre
	nombreClient = NLib_Memoire_NListe_ObtenirNombre( this->m_clients );

	// Release mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_clients );

	// OK
	return nombreClient;
}

/**
 * Obtenir l'index du client avec cet identifiant (privee, mutex doit etre lock)
 *
 * @param this
 * 		Cette instance
 * @param identifiant
 * 		L'identifiant du client
 *
 * @return l'index du client ou NERREUR si introuvable
 */
__PRIVATE __MUSTBEPROTECTED NU32 NLib_Module_Reseau_Serveur_NServeur_TrouverIndexDepuisIdentifiant( const NServeur *this,
	NU32 identifiant )
{
	// Sortie
	__OUTPUT NU32 out = 0;

	// Chercher
	for( ; out < NLib_Memoire_NListe_ObtenirNombre( this->m_clients ); out++ )
		// Meme identifiant?
		if( NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clients,
			out ) ) == identifiant )
			// OK
			return out;

	// Introuvable
	return NERREUR;
}

/**
 * Trouver un identifiant unique libre (privee, mutex doit etre lock)
 *
 * @param this
 * 		Cette instance
 *
 * @return l'identifiant libre
 */
__PRIVATE __MUSTBEPROTECTED NU32 NLib_Module_Reseau_Serveur_NServeur_TrouverIdentifiantInterne( const NServeur *this )
{
	// Identifiant
	__OUTPUT NU32 identifiant;

	// Chercher un identifiant libre
	do
	{
		// Choisir un identifiant
		identifiant = (NU32)NLib_Temps_ObtenirNombreAleatoire( ) % LIMITE_IDENTIFIANT_CLIENT_SERVEUR;
	} while( NLib_Module_Reseau_Serveur_NServeur_TrouverIndexDepuisIdentifiant( this,
		identifiant ) != NERREUR );

	// OK
	return identifiant;
}

/**
 * Ajouter un client (private)
 *
 * @param this
 * 		Cette instance
 * @param nouveauClient
 * 		Le client a ajouter
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE __WILLLOCK __WILLUNLOCK NBOOL NLib_Module_Reseau_Serveur_NServeur_AjouterClient( NServeur *this,
	const NClientServeur *nouveauClient )
{
	// Buffer
	char buffer[ 256 ];

	// Lock le mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_clients );

	// Verifier si la connexion est autorisee
	if( !this->m_estConnexionAutorisee
		|| NLib_Memoire_NListe_ObtenirNombre( this->m_clients ) > this->m_nombreMaximumClient )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Unlock le mutex
		NLib_Memoire_NListe_DesactiverProtection( this->m_clients );

		// Quitter
		return NFALSE;
	}

	// Trouver un identifiant libre
	NLib_Module_Reseau_Serveur_NClientServeur_DefinirIdentifiantUnique( (NClientServeur*)nouveauClient,
		NLib_Module_Reseau_Serveur_NServeur_TrouverIdentifiantInterne( this ) );

	// Associer les donnees devant etre transmises
	NLib_Module_Reseau_Serveur_NClientServeur_DefinirDonneeExterne( (NClientServeur*)nouveauClient,
		this->m_donneePourClient );

	// Executer callback
	if( !this->m_callbackConnexion( nouveauClient ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Unlock mutex
		NLib_Memoire_NListe_DesactiverProtection( this->m_clients );

		// Quitter
		return NFALSE;
	}

	// Ajouter le client
    if( !NLib_Memoire_NListe_Ajouter( this->m_clients,
		(NClientServeur*)nouveauClient ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Unlock mutex
		NLib_Memoire_NListe_DesactiverProtection( this->m_clients );

		// Quitter
		return NFALSE;
	}

	// Notifier
	if( this->m_estLogConsole )
	{
		// Creer le message
		snprintf( buffer,
			256,
			"[%s]\t[%d] se connecte.",
			NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( nouveauClient ),
			NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( nouveauClient ) );

		// Notifier
		NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
			buffer,
			0 );
	}

	// Unlock le mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_clients );

	// OK
	return NTRUE;
}

/**
 * Accepter un client
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Serveur_NServeur_AccepterClient( NServeur *this )
{
	// Client a ajouter
	NClientServeur *nouveauClient;

	// Recuperer un nouveau client
	if( !( nouveauClient = NLib_Module_Reseau_Serveur_NClientServeur_Construire( this->m_socket,
		this->m_callbackReceptionPacket,
		&this->m_estConnexionAutorisee,
		this->m_tempsAvantTimeoutReception,
		this->m_tempsAvantTimeoutEmission,
		this->m_type,
		this->m_typePacketEmis,
		this->m_callbackMethodeReception,
		this->m_callbackMethodeEmission ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_ACCEPT_FAILED );

		// Quitter
		return NFALSE;
	}

	// Ajouter le client
	if( !NLib_Module_Reseau_Serveur_NServeur_AjouterClient( this,
		nouveauClient ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Liberer le client
		NLib_Module_Reseau_Serveur_NClientServeur_Detruire( &nouveauClient );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Supprimer un client (private, mutex doit etre lock)
 *
 * @param this
 * 		Cette instance
 * @param index
 * 		L'index du client
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NLib_Module_Reseau_Serveur_NServeur_SupprimerClient( NServeur *this,
	NU32 index )
{
	// Buffer
	char buffer[ 256 ] = { 0, };

	// Client
	NClientServeur *client;

	// Obtenir client
	if( !( client = (NClientServeur*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clients,
		index ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Callback deconnexion
	this->m_callbackDeconnexion( client );

	// Notifier
	if( this->m_estLogConsole )
	{
		// Creer le message
		sprintf( buffer,
			"[%s]\t[%d] se deconnecte",
			NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ),
			NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

		// Notifier
		NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
			buffer,
			0 );
	}

	// Supprimer
	return NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_clients,
		index );
}

/**
 * Nettoyer les clients morts
 *
 * @return le nombre de clients nettoyes
 */
__WILLLOCK __WILLUNLOCK NU32 NLib_Module_Reseau_Serveur_NServeur_NettoyerClients( NServeur *this )
{
	// Iterateur
	NU32 i = 0;

	// Nombre de clients nettoyes
	__OUTPUT NU32 nombreClientNettoye = 0;

	// Lock le mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_clients );

	// Chercher les clients morts
	while( i < NLib_Memoire_NListe_ObtenirNombre( this->m_clients ) )
		// Le client est mort?
		if( NLib_Module_Reseau_Serveur_NClientServeur_EstMort( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clients,
			i ) ) )
		{
			// Supprimer le client
			NLib_Module_Reseau_Serveur_NServeur_SupprimerClient( this,
				i );

			// Incrementer le nombre de clients nettoyes
			nombreClientNettoye++;

			// Recommencer la recherche pour ne rien rater
			i = ( i > 0 ) ?
				i - 1
				: 0;
		}
		else
			i++;

	// Unlock mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_clients );

	// OK
	return nombreClientNettoye;
}

/**
 * Tuer tous les clients connectes (attendre un passage du nettoyeur pour leur elimination)
 *
 * @param this
 * 		Cette instance
 */
__WILLLOCK __WILLUNLOCK void NLib_Module_Reseau_Serveur_NServeur_TuerTousClients( NServeur *this )
{
	// Iterateur
	NU32 i = 0;

	// Lock le mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_clients );

	// Tuer tous les clients
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_clients ); i++ )
		NLib_Module_Reseau_Serveur_NClientServeur_Tuer( (NClientServeur*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_clients,
			i ) );

	// Release mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_clients );
}

/**
 * Changer l'etat d'autorisation de connexion (privee)
 *
 * @param this
 * 		Cette instance
 * @param etat
 * 		L'etat d'autorisation
 */
__PRIVATE __WILLLOCK __WILLUNLOCK void NLib_Module_Reseau_Serveur_NServeur_ChangerEtatConnexion( NServeur *this,
	NBOOL etat )
{
	// Lock le mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_clients );

	// Changer l'etat
	this->m_estConnexionAutorisee = etat;

	// Release le mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_clients );
}

/**
 * Autoriser la connexion
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Serveur_NServeur_AutoriserConnexion( NServeur *this )
{
	// Autoriser
	NLib_Module_Reseau_Serveur_NServeur_ChangerEtatConnexion( this,
		NTRUE );
}

/**
 * Interdire la connexion
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Serveur_NServeur_InterdireConnexion( NServeur *this )
{
	// Interdire
	NLib_Module_Reseau_Serveur_NServeur_ChangerEtatConnexion( this,
		NFALSE );
}

/**
 * Activer log console
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Serveur_NServeur_ActiverLogConsole( NServeur *this )
{
	// Activer
	this->m_estLogConsole = NTRUE;
}

/**
 * Desactiver log console
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Serveur_NServeur_DesactiverLogConsole( NServeur *this )
{
	// Desactiver
	this->m_estLogConsole = NFALSE;
}

/**
 * Obtenir le nombre maximum de clients
 *
 * @param this
 * 		Cette instance
 *
 * @return le nombre maximum de clients autorises
 */
NU32 NLib_Module_Reseau_Serveur_NServeur_ObtenirNombreMaximumClients( const NServeur *this )
{
	return this->m_nombreMaximumClient;
}

/**
 * Definir le nombre maximum de clients
 *
 * @param this
 * 		Cette instance
 * @param nombreMaximumClient
 * 		Le nombre maximum de clients
 */
void NLib_Module_Reseau_Serveur_NServeur_DefinirNombreMaximumClients( NServeur *this,
	NU32 nombreMaximumClient )
{
	// Enregistrer
	this->m_nombreMaximumClient = nombreMaximumClient;
}

#endif // NLIB_MODULE_RESEAU

