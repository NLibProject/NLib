#include "../../../../../include/NLib/NLib.h"

// ------------------------------------------
// struct NLib::Module::Reseau::IP::NDetailIP
// ------------------------------------------

#ifdef NLIB_MODULE_RESEAU
/**
 * Construire instance
 *
 * @param protocole
 *		Le protocole
 * @param adresse
 *		L'adresse ip (v4/v6)
 *
 * @return l'instance
 */
__ALLOC NDetailIP *NLib_Module_Reseau_IP_NDetailIP_Construire( NProtocoleIP protocole,
	const char *adresse )
{
	// Sortie
	__OUTPUT NDetailIP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NDetailIP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer l'adresse
	if( !( out->m_adresse = calloc( strlen( adresse ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_protocole = protocole;
	memcpy( out->m_adresse,
		adresse,
		strlen( adresse ) );

	// OK
	return out;
}

/**
 * Construire l'instance
 *
 * @param src
 *		La source
 *
 * @return l'instance
 */
__ALLOC NDetailIP *NLib_Module_Reseau_IP_NDetailIP_Construire2( const NDetailIP *src )
{
	// Sortie
	__OUTPUT NDetailIP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NDetailIP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer adresse
	if( !( out->m_adresse = calloc( strlen( src->m_adresse ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out->m_adresse,
		src->m_adresse,
		strlen( src->m_adresse ) );
	out->m_protocole = src->m_protocole;
	out->m_cidr = src->m_cidr;

	// OK
	return out;
}

/**
 * Construire l'instance
 *
 * @param protocole
 * 		Le protocole
 * @param adresse
 * 		L'adresse ip (v4/v6)
 * @param cidr
 * 		Le cidr
 *
 * @return l'instance
 */
__ALLOC NDetailIP *NLib_Module_Reseau_IP_NDetailIP_Construire3( NProtocoleIP protocole,
	const char *adresse,
	NU32 cidr )
{
	// Sortie
	__OUTPUT NDetailIP *out;

	// Construire l'instance
	if( !( out = NLib_Module_Reseau_IP_NDetailIP_Construire( protocole,
		adresse ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_cidr = cidr;

	// OK
	return out;
}

/**
 * Detruire l'instance
 *
 * @param this
 *		L'instance a detruire
 */
void NLib_Module_Reseau_IP_NDetailIP_Detruire( NDetailIP **this )
{
	// Liberer
	NFREE( (*this)->m_adresse );
	NFREE( (*this) );
}

/**
 * Obtenir protocole
 *
 * @param this
 *		Cette instance
 *
 * @return Le protocole
 */
NProtocoleIP NLib_Module_Reseau_IP_NDetailIP_ObtenirProtocole( const NDetailIP *this )
{
	return this->m_protocole;
}

/**
 * Obtenir adresse
 *
 * @param this
 *		Cette instance
 *
 * @return l'adresse
 */
const char *NLib_Module_Reseau_IP_NDetailIP_ObtenirAdresse( const NDetailIP *this )
{
	return this->m_adresse;
}

/**
 * Obtenir CIDR
 *
 * @param this
 * 		Cette instance
 *
 * @return le cidr
 */
NU32 NLib_Module_Reseau_IP_NDetailIP_ObtenirCIDR( const NDetailIP *this )
{
	return this->m_cidr;
}
#endif // NLIB_MODULE_RESEAU

