#include "../../../../../include/NLib/NLib.h"

// ----------------------------------
// namespace NLib::Module::Reseau::IP
// ----------------------------------

#ifdef NLIB_MODULE_RESEAU
/**
 * Divide ip in %composentCount% components (The left one is at 0 index)
 *
 * @param ip
 * 		The ip
 * @param ipProtocol
 * 		The ip protocol
 * @param output
 * 		The output array
 *
 * @return if operation succedeed
 */
NBOOL NLib_Module_Reseau_IP_DivideIP( const char *ip,
	NProtocoleIP ipProtocol,
	__OUTPUT NU8 *output )
{
	// Cursor
	NU32 cursor = 0;

	// Iterator
	NU32 i = 0;

	// Buffer
	char *buffer = NULL;

	// Nombre
	NU32 nombre;

	// Composent count
	NU32 composentCount = (NU32)( ipProtocol == NPROTOCOLE_IP_IPV4 ?
		4
		: 6 );

	// Divide
	for( i = ( composentCount - 1 ); (NS32)i >= 0; i-- )
	{
		// Read
		if( !( buffer = NLib_Chaine_LireJusqua( ip,
			(char)( ipProtocol == NPROTOCOLE_IP_IPV4 ?
				'.'
				: ':' ),
			&cursor,
			NFALSE ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Quit
			return NFALSE;
		}

		// Parse
		if( ( nombre = (NU8)strtol( buffer,
			NULL,
			ipProtocol == NPROTOCOLE_IP_IPV4 ?
				10
				: 16 ) ) > 0xFF )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Free
			NFREE( buffer );

			// Quit
			return NFALSE;
		}

		// Save
		output[ i ] = (NU8)nombre;

		// Free
		NFREE( buffer );
	}

	// OK
	return NTRUE;
}

/**
 * Divide ip in %composentCount% components (The left one is at 0 index)
 *
 * @param ip
 * 		The integer composed ip
 * @param ipProtocol
 * 		The ip protocol
 * @param output
 * 		The output array
 */
void NLib_Module_Reseau_IP_DivideIP2( NU64 ip,
	NProtocoleIP ipProtocol,
	__OUTPUT NU8 *output )
{
	// Iterator
	NU32 i = 0;

	// Composent count
	NU32 composentCount = ( ipProtocol == NPROTOCOLE_IP_IPV4 ?
		4
		: 6 );

	// Divide
	for( ; i < composentCount; i++ )
		output[ i ] = (NU8)( ( ip >> ( ( ( composentCount - 1 ) - i ) * 8 ) ) & 0x000000FF );
}

/**
 * C'est une adresse IPv4?
 *
 * @param ip
 * 		L'adresse ip
 *
 * @return si il s'agit d'une adresse IPv4
 */
NBOOL NLib_Module_Reseau_IP_EstAdresseIPv4( const char *ip )
{
	// IP
	NU8 cutIP[ 4 ];

	// Try to divide
	return NLib_Module_Reseau_IP_DivideIP( ip,
		NPROTOCOLE_IP_IPV4,
		cutIP );
}

/**
 * Obtenir IP
 *
 * @param adressage
 *		L'adresse
 *
 * @return l'ip
 */
__ALLOC char *NLib_Module_Reseau_IP_ObtenirAdresseIP( SOCKADDR_IN adressage )
{
	// Output
	__OUTPUT char *out;

	// Allocate memory
	if( !( out = calloc( INET_ADDRSTRLEN + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Obtenir adresse
	inet_ntop( AF_INET,
		(void*)&adressage.sin_addr,
		out,
		INET_ADDRSTRLEN * sizeof( char ) );

	// OK
	return out;
}

#endif // NLIB_MODULE_RESEAU

