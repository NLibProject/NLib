#include "../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU
// -----------------------------------------
// namespace NLib::Module::Reseau::Interface
// -----------------------------------------

/**
 * Trouver toutes les interfaces reseaux
 *
 * @return la liste des interfaces reseaux (NListe<NInterfaceReseau*>)
 */
__ALLOC NListe *NLib_Module_Reseau_Interface_TrouverTouteInterface( void )
{
	// Sortie
	__OUTPUT NListe *sortie;

	// Adresse/CIDR
	char *adresseTexte;
	char *cidrTexte;

	// CIDR
	NU32 cidrBrute;
	NU32 cidr;

	// Iterateurs
	NU32 i,
		j;

	// Buffer
	char buffer[ 512 ];
	NU32 curseurBuffer;

	// Liste interfaces
	struct ifaddrs *listeInterface,
		*curseur;

	// Interface
	NInterfaceReseau *interface;

	// Construire liste
	if( !( sortie = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NLib_Module_Reseau_Interface_NInterfaceReseau_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

#ifdef IS_WINDOWS
	NOTIFIER_ERREUR( NERREUR_UNIMPLEMENTED );
	// GetAdaptersInfo
	// GetAdaptersAddresses
#else
	// Recuperer les interfaces
	if( getifaddrs( &listeInterface ) != 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Liberer
		NLib_Memoire_NListe_Detruire( &sortie );

		// Quitter
		return NULL;
	}

	// Lire les interfaces
	curseur = listeInterface;
	while( curseur != NULL )
	{
		if( curseur->ifa_addr != NULL
			&& curseur->ifa_netmask != NULL
			&& curseur->ifa_addr->sa_family == AF_INET )
		{
			// Recuperer adresse
			adresseTexte = NLib_Module_Reseau_IP_ObtenirAdresseIP( *(struct sockaddr_in*)curseur->ifa_addr );
			cidrTexte = NLib_Module_Reseau_IP_ObtenirAdresseIP( *(struct sockaddr_in*)curseur->ifa_netmask );

			// Transformer cidr en entier
			NLib_Module_Reseau_IP_DivideIP( cidrTexte,
				NPROTOCOLE_IP_IPV4,
				(NU8*)&cidrBrute );

			// Compter nombre de bits a 1 (on considere que le cidr fournit par getifaddrs est fiable, donc que des 1 consecutifs...)
			cidr = NLib_Memoire_CompterNombreBit( cidrBrute,
				sizeof( NU32 ) * 8,
				1 );

			// Construire interface
			if( !( interface = NLib_Module_Reseau_Interface_NInterfaceReseau_Construire( curseur->ifa_name,
				adresseTexte,
				cidr,
				NULL ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Liberer
				NFREE( adresseTexte );
				NFREE( cidrTexte );

				// Detruire liste
				NLib_Memoire_NListe_Detruire( &sortie );

				// Liberer les interfaces
				freeifaddrs( listeInterface );

				// Quitter
				return NULL;
			}

			// Ajouter
			NLib_Memoire_NListe_Ajouter( sortie,
				interface );

			// Liberer
			NFREE( adresseTexte );
			NFREE( cidrTexte );
		}

		// Passer a l'interface suivante
		curseur = curseur->ifa_next;
	}

	// Chercher les adresses mac
	curseur = listeInterface;
	while( curseur != NULL )
	{
		// Adresse mac?
		if( curseur->ifa_addr != NULL
			&& curseur->ifa_addr->sa_family == AF_PACKET )
		{
			// Mettre a zero
			curseurBuffer = 0;
			memset( buffer,
				0,
				512 );

			// Lire adresse
			for( i = 0; i < 6; i++ )
				// Concatener
				curseurBuffer += sprintf( buffer + curseurBuffer,
					"%02X%s",
					( (struct sockaddr_ll*)curseur->ifa_addr )->sll_addr[ i ],
					i < 5 ?
						":"
						: "" );

			// Enregistrer si l'interface est connue
			for( j = 0; j < NLib_Memoire_NListe_ObtenirNombre( sortie ); j++ )
				// Obtenir interface
				if( ( interface = (NInterfaceReseau*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( sortie,
					j ) ) != NULL )
					// Comparer
					if( NLib_Chaine_Comparer( NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirNom( interface ),
						curseur->ifa_name,
						NTRUE,
						0 ) )
						// Enregistrer
						NLib_Module_Reseau_Interface_NInterfaceReseau_DefinirAdresseMAC( interface,
							buffer );
		}

		// Passer a l'interface suivante
		curseur = curseur->ifa_next;
	}

	// Liberer les interfaces
	freeifaddrs( listeInterface );
#endif

	// OK
	return sortie;
}

#endif // NLIB_MODULE_RESEAU
