#include "../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU

// --------------------------------------------------------
// struct NLib::Module::Reseau::Interface::NInterfaceReseau
// --------------------------------------------------------

/**
 * Construire une interface reseau
 *
 * @param nom
 * 		Le nom de l'interface
 * @param adresse
 * 		L'adresse de l'interface
 * @param cidr
 * 		Le cidr de l'adresse
 * @param adresseMAC
 * 		L'adresse mac
 *
 * @return l'instance de l'interface
 */
__ALLOC NInterfaceReseau *NLib_Module_Reseau_Interface_NInterfaceReseau_Construire( const char *nom,
	const char *adresse,
	NU32 cidr,
	const char *adresseMAC )
{
	// Sortie
	__OUTPUT NInterfaceReseau *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NInterfaceReseau ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Dupliquer
	if( !( out->m_nom = NLib_Chaine_Dupliquer( nom ) )
		|| !( out->m_adresse = NLib_Chaine_Dupliquer( adresse ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Liberer
		NFREE( out->m_nom );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// On a une adresse mac?
	if( adresseMAC != NULL )
		// Dupliquer adresse mac
		if( !( out->m_adresseMAC = NLib_Chaine_Dupliquer( adresseMAC ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_COPY );

			// Free
			NFREE( out->m_adresse );
			NFREE( out->m_nom );
			NFREE( out );

			// Quit
			return NULL;
		}

	// Enregistrer
	out->m_cidr = cidr;

	// OK
	return out;
}

/**
 * Dupliquer une interface reseau
 *
 * @param src
 * 		L'interface a dupliquer
 *
 * @return l'instance de l'interface
 */
__ALLOC NInterfaceReseau *NLib_Module_Reseau_Interface_NInterfaceReseau_Construire2( const NInterfaceReseau *src )
{
	return NLib_Module_Reseau_Interface_NInterfaceReseau_Construire( src->m_nom,
		src->m_adresse,
		src->m_cidr,
		src->m_adresseMAC );
}

/**
 * Detruire l'interface
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Interface_NInterfaceReseau_Detruire( NInterfaceReseau **this )
{
	// Liberer
	NFREE( (*this)->m_adresseMAC );
	NFREE( (*this)->m_adresse );
	NFREE( (*this)->m_nom );
	NFREE( (*this) );
}

/**
 * Obtenir nom
 *
 * @param this
 * 		Cette instance
 *
 * @return le nom de l'interface
 */
const char *NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirNom( const NInterfaceReseau *this )
{
	return this->m_nom;
}

/**
 * Obtenir adresse
 *
 * @param this
 * 		Cette instance
 *
 * @return l'adresse de l'interface
 */
const char *NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirAdresse( const NInterfaceReseau *this )
{
	return this->m_adresse;
}

/**
 * Obtenir le CIDR de l'adresse
 *
 * @param this
 * 		Cette instance
 *
 * @return le CIDR de l'adresse
 */
NU32 NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirCIDR( const NInterfaceReseau *this )
{
	return this->m_cidr;
}

/**
 * Definir adresse mac
 *
 * @param this
 * 		Cette instance
 * @param adresseMAC
 * 		L'adresse mac a definir
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Interface_NInterfaceReseau_DefinirAdresseMAC( NInterfaceReseau *this,
	const char *adresseMAC )
{
	// Liberer ancienne adresse
	NFREE( this->m_adresseMAC );

	// Dupliquer
	if( !( this->m_adresseMAC = NLib_Chaine_Dupliquer( adresseMAC ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Obtenir adresse mac
 *
 * @param this
 * 		Cette instance
 *
 * @return l'adresse MAC ou NULL si non definie
 */
const char *NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirAdresseMAC( const NInterfaceReseau *this )
{
	return this->m_adresseMAC;
}

#endif // NLIB_MODULE_RESEAU

