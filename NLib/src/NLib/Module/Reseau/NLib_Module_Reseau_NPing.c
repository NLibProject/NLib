#include "../../../../include/NLib/NLib.h"

// ----------------------------------
// struct NLib::Module::Reseau::NPing
// ----------------------------------

#ifdef NLIB_MODULE_RESEAU

/* Construire */
__ALLOC NPing *NLib_Module_Reseau_NPing_Construire( void )
{
	// Sortie
	__OUTPUT NPing *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NPing ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Zero
	NLib_Module_Reseau_NPing_MettreAZero( out );

	// OK
	return out;
}

/* Detruire */
void NLib_Module_Reseau_NPing_Detruire( NPing **this )
{
	NFREE( (*this) );
}

/* Obtenir duree depuis dernier ping */
NU32 NLib_Module_Reseau_NPing_ObtenirDureeDepuisDernierPing( const NPing *this )
{
	return NLib_Temps_ObtenirTick( ) - this->m_tempsDerniereDemande;
}

/* Effectuer requete */
NBOOL NLib_Module_Reseau_NPing_EffectuerRequete( NPing *this )
{
	// Si on a deja une requete en cours...
	if( this->m_estAttenteReponse )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Obtenir le temps de la demande
	this->m_tempsDerniereDemande = NLib_Temps_ObtenirTick( );

	// OK
	return this->m_estAttenteReponse = NTRUE;
}

/* Reponse a la requete */
NBOOL NLib_Module_Reseau_NPing_RecevoirReponseRequete( NPing *this )
{
	// Si on attend bien un ping
	if( this->m_estAttenteReponse )
	{
		// Enregistrer le ping
		this->m_ping = NLib_Temps_ObtenirTick( ) - this->m_tempsDerniereDemande;

		// OK
		return !( this->m_estAttenteReponse = NFALSE );
	}
	// On attendait rien du tout...
	else
		return NFALSE;
}

/* Est requete timeout? */
NBOOL NLib_Module_Reseau_NPing_EstTimeout( const NPing *this )
{
	return ( this->m_estAttenteReponse
		&& ( NLib_Temps_ObtenirTick( ) - this->m_tempsDerniereDemande ) >= NDUREE_AVANT_TIMEOUT_PING );
}

/* Est doit effectuer une nouvelle requete de ping? */
NBOOL NLib_Module_Reseau_NPing_EstDoitEffectuerRequete( const NPing *this )
{
	return ( !this->m_estAttenteReponse
		&& ( NLib_Temps_ObtenirTick( ) - this->m_tempsDerniereDemande ) >= NDELAI_ENTRE_REQUETE_PING );
}

/* Obtenir ping */
NU32 NLib_Module_Reseau_NPing_ObtenirPing( const NPing *this )
{
	return this->m_ping;
}

/* Remettre a zero */
void NLib_Module_Reseau_NPing_MettreAZero( NPing *this )
{
	// Zero
	this->m_ping = 0;
	this->m_tempsDerniereDemande = NLib_Temps_ObtenirTick( );
	this->m_estAttenteReponse = NFALSE;
}

#endif // NLIB_MODULE_RESEAU

