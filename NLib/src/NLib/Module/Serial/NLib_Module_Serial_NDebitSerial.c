#define NLIB_MODULE_SERIAL_NDEBITSERIAL_INTERNE
#include "../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_SERIAL

// --------------------------------------
// enum NLib::Module_Serial::NDebitSerial
// --------------------------------------

/**
 * Obtenir valeur debit
 *
 * @param debit
 * 		La constante de debit
 *
 * @return la valeur associee
 */
speed_t NLib_Module_Serial_NDebitSerial_ObtenirValeurDebit( NDebitSerial debit )
{
	return NDebitSerialValeur[ debit ];
}
#endif // NLIB_MODULE_SERIAL
