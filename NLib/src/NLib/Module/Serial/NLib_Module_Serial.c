#include "../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_SERIAL

// ------------------------------
// namespace NLib::Module::Serial
// ------------------------------

// https://stackoverflow.com/questions/6947413/how-to-open-read-and-write-from-serial-port-in-c?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

/**
 * Definir attributs port serie
 *
 * @param descripteurFichier
 * 		Descripteur port serie
 * @param debit
 * 		Le debit
 * @param parity
 * 		On utilise un bit de parite?
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Serial_DefinirAttribut( NS32 descripteurFichier,
	NDebitSerial debit,
	NBOOL parity )
{
	// Informations tty
	struct termios tty;

	// Vider informations tty
	memset( &tty,
		0,
		sizeof( tty ) );

	// Obtenir etat actuel tty
	if( tcgetattr( descripteurFichier,
		&tty ) != 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SERIAL );

		// Quitter
		return NFALSE;
	}

	// Definir debit sortie
	cfsetospeed( &tty,
		NLib_Module_Serial_NDebitSerial_ObtenirValeurDebit( debit ) );

	// Definir debit entree
	cfsetispeed( &tty,
		NLib_Module_Serial_NDebitSerial_ObtenirValeurDebit( debit ) );

	// Caractere codes sur 8 bits
	tty.c_cflag = ( tty.c_cflag & ~CSIZE ) | CS8;

	// Desactiver IGNBRK
	tty.c_iflag &= ~IGNBRK;

	// Pas de caractere de signalement, pas d'echo
	tty.c_lflag = 0;

	// Desactiver mode canonique (LF doit etre recu avant de pouvoir parler
	tty.c_oflag = 0;

	// Definir proprietes timeout par defaut
	tty.c_cc[ VMIN ]  = 0;
	tty.c_cc[ VTIME ] = 5;

	// Desactiver controles xon/xoff
	tty.c_iflag &= ~( IXON | IXOFF | IXANY );

	// Ignorer controles modem
	tty.c_cflag |= ( CLOCAL | CREAD );

	// Desactiver la parite
	tty.c_cflag &= ~( PARENB | PARODD );
	tty.c_cflag |= parity;
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CRTSCTS;

	// Re-appliquer les parametres au tty
	if( tcsetattr( descripteurFichier,
		TCSANOW,
		&tty ) != 0 )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SERIAL );

		// Quit
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Definir si un port serial bloque
 *
 * @param descripteurFichier
 * 		Le descripteur du port serie
 * @param estBloquant
 * 		On veut que le port soit bloquant?
 * @param timeout
 * 		Timeout si bloquant (10^-1 seconde [0.%timeout%]) (Max=255, defaut=5)
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Serial_DefinirBloquant( NS32 descripteurFichier,
	NS32 estBloquant,
	NU8 timeout )
{
	// Informations tty
	struct termios tty;

	// Vider informations tty
	memset( &tty,
		0,
		sizeof( tty ) );

	// Obtenir l'etat actuel du tty
	if( tcgetattr( descripteurFichier,
		&tty) != 0 )
	{
		NOTIFIER_ERREUR( NERREUR_SERIAL );

		// Quit
		return NFALSE;
	}

	// Definir si bloquant
	tty.c_cc[ VMIN ] = (NU8)( estBloquant ?
		NTRUE
		: NFALSE );

	// Definir timeout
	tty.c_cc[ VTIME ] = timeout;

	// Appliquer les modifications
	if( tcsetattr( descripteurFichier,
		TCSANOW,
		&tty ) != 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SERIAL );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

#endif // NLIB_MODULE_SERIAL
