#include "../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_SERIAL

// ------------------------------------------
// struct NLib::Module::Serial::NLiaisonSerie
// ------------------------------------------

/**
 * Construire liaison serie
 *
 * @param nomTTY
 * 		Le nom du tty a ouvrir (exemple: ttyUSB0)
 * @param debit
 * 		Le debit de la liaison serie
 *
 * @return l'instance de la liaison serie
 */
__ALLOC NLiaisonSerie *NLib_Module_Serial_NLiaisonSerie_Construire( const char *nomTTY,
	NDebitSerial debit )
{
	// Sortie
	__OUTPUT NLiaisonSerie *out;

	// Chemin tty
	char cheminTTY[ 128 ];

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NLiaisonSerie ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer debit
	out->m_debit = debit;

	// Creer le chemin
	snprintf( cheminTTY,
		128,
		"/dev/%s",
		nomTTY );

	// Tenter d'ouvrir le tty
	if( ( out->m_descripteur = open( cheminTTY,
		O_RDWR | O_NOCTTY | O_SYNC ) ) <= 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Definir proprietes
	NLib_Module_Serial_DefinirAttribut( out->m_descripteur,
		debit,
		NFALSE );
	NLib_Module_Serial_DefinirBloquant( out->m_descripteur,
		NFALSE,
		5 );

	// OK
	return out;
}

/**
 * Detruire liaison
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Serial_NLiaisonSerie_Detruire( NLiaisonSerie **this )
{
	// Fermer liaison
	close( (*this)->m_descripteur );

	// Liberer
	NFREE( (*this) );
}

/**
 * Lire
 *
 * @param this
 * 		Cette instance
 *
 * @return ce qui a ete lu
 */
__ALLOC NData *NLib_Module_Serial_NLiaisonSerie_LireNonBloquant( NLiaisonSerie *this )
{
	// Taille lue
	ssize_t tailleLue = 0;

	// Sortie
	__OUTPUT NData *data;

#define TAILLE_BUFFER		256

	// Buffer
	char buffer[ TAILLE_BUFFER ];

	// Creer sortie
	if( !( data = NLib_Memoire_NData_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Lire
	while( ( tailleLue = read( this->m_descripteur,
		buffer,
		TAILLE_BUFFER ) ) > 0 )
		// Ajouter a la sortie
		if( !NLib_Memoire_NData_AjouterData( data,
			buffer,
			(NU32)tailleLue ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_COPY );

			// Detruire data
			NLib_Memoire_NData_Detruire( &data );

			// Quit
			return NULL;
		}

#undef TAILLE_BUFFER

	// OK
	return data;
}

/**
 * Lire bloquant
 *
 * @param this
 * 		Cette instance
 * @param tailleALire
 * 		La taille a lire
 *
 * @return ce qui a ete lu
 */
__ALLOC NData *NLib_Module_Serial_NLiaisonSerie_LireBloquant( NLiaisonSerie *this,
	NU32 tailleALire )
{
	// Taille lue
	ssize_t tailleLue = 0;

	// Taille a lire courant
	NU32 tailleALireCourant;

	// Sortie
	__OUTPUT NData *data;

#define TAILLE_BUFFER		256

	// Buffer
	char buffer[ TAILLE_BUFFER ];

	// Creer sortie
	if( !( data = NLib_Memoire_NData_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Lire
	while( NLib_Memoire_NData_ObtenirTailleData( data ) < tailleALire )
	{
		// Calculer taille a lire
		tailleALireCourant = ( tailleALire - NLib_Memoire_NData_ObtenirTailleData( data ) ) >= TAILLE_BUFFER ?
			TAILLE_BUFFER
			: ( tailleALire - NLib_Memoire_NData_ObtenirTailleData( data ) );

		// Lire
		if( read( this->m_descripteur,
			buffer,
			tailleALireCourant ) <= 0 )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SERIAL );

			// Detruire
			NLib_Memoire_NData_Detruire( &data );

			// Quitter
			return NULL;
		}

		// Ajouter a la sortie
		if( !NLib_Memoire_NData_AjouterData( data,
			buffer,
			(NU32)tailleLue ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_COPY );

			// Detruire data
			NLib_Memoire_NData_Detruire( &data );

			// Quitter
			return NULL;
		}
	}

#undef TAILLE_BUFFER

	// OK
	return data;
}

/**
 * Ecrire
 *
 * @param this
 * 		Cette instance
 * @param data
 * 		Les donnees a ecrire
 * @param tailleData
 * 		La tailles des donnees a ecrire
 *
 * @return si l'operation s'est bien passee
 */
 NBOOL NLib_Module_Serial_NLiaisonSerie_Ecrire( NLiaisonSerie *this,
	const char *data,
	NU32 tailleData )
{
	// Taille ecrite
	NU32 tailleEcrite = 0;

	// Taille a ecrire
	NU32 tailleAEcrire;

	// Taille ecrite courant
	ssize_t tailleEcriteCourant;

#define TAILLE_BUFFER			256

	// Ecrire
	while( tailleEcrite < tailleData )
	{
		// Calculer taille a ecrire
		tailleAEcrire = tailleData - tailleEcrite >= TAILLE_BUFFER ?
			TAILLE_BUFFER
			: ( tailleData - tailleEcrite );

		// Ecrire
		if( ( tailleEcriteCourant = write( this->m_descripteur,
			data + tailleEcrite,
			tailleAEcrire ) ) < 0 )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_SERIAL );

			// Quit
			return NFALSE;
		}

		// Incrementer taille ecrite
		tailleEcrite += tailleEcriteCourant;
	}
}
#endif // NLIB_MODULE_SERIAL
