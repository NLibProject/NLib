#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ------------------------------------
// namespace NLib::Module::SDL::Surface
// ------------------------------------

#ifdef NLIB_MODULE_SDL
/* Creer une surface */
__ALLOC SDL_Surface *NLib_Module_SDL_Surface_Creer( NU32 w,
	NU32 h,
	NBOOL filtre )
{
	// Surface en sortie
	__OUTPUT SDL_Surface *s = NULL;

	// Avec filtre
	if( filtre )
	{
		// Creer la surface
		if( !( s =
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
			SDL_CreateRGBSurface( SDL_RLEACCEL,
				w,
				h,
				32,
				0xFF000000,
				0x00FF0000,
				0x0000FF00,
				0x000000FF )
#else // SDL_BYTEORDER == SDL_BIG_ENDIAN
			SDL_CreateRGBSurface( SDL_RLEACCEL,
				w,
				h,
				32,
				0x000000FF,
				0x0000FF00,
				0x00FF0000,
				0xFF000000 )
#endif // SDL_BYTEORDER != SDL_BIG_ENDIAN
			) )
			return NULL;

		// Definir le mode alpha
		if( SDL_SetSurfaceBlendMode( s,
			SDL_BLENDMODE_BLEND ) == NSDL_ERREUR )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_SDL );

			// Liberer
			NLIB_NETTOYER_SURFACE( s );

			// Quitter
			return NULL;
		}

		// OK
		return s;
	}
	// Sans filtre
	else
		// Creer la surface
		return SDL_CreateRGBSurface( SDL_RLEACCEL,
			w,
			h,
			32,
			0x00000000,
			0x00000000,
			0x00000000,
			0x00000000 );
}

/* Copier une surface */
// Vers une surface deja existante
NBOOL NLib_Module_SDL_Surface_Copier( const SDL_Surface *src,
	const SDL_Rect *zoneSrc,
	SDL_Surface *dst,
	const SDL_Rect *zoneDst )
{
	// Etat
	SDL_Rect etat = { 0, 0 };

	// Les tailles de copie sont logiques?
	if( zoneDst->x + zoneDst->w > dst->w
		|| zoneDst->y + zoneDst->h > dst->h )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Est ce qu'on a bien le meme format de pixel?
	if( src->format->BytesPerPixel != dst->format->BytesPerPixel )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Locker les surfaces
	if( SDL_LockSurface( (SDL_Surface*)src ) == NSDL_ERREUR
		|| SDL_LockSurface( dst ) == NSDL_ERREUR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Quitter
		return NFALSE;
	}

	// Copier les pixels
	for( etat.x = 0; etat.x < zoneDst->w; etat.x++ )
		for( etat.y = 0; etat.y < zoneDst->h; etat.y++ )
			NLib_Module_SDL_Surface_Putpixel2( zoneDst->x + etat.x,
				zoneDst->y + etat.y,
				dst,
				NLib_Module_SDL_Surface_Getpixel2( zoneSrc->x + etat.x,
					zoneSrc->y + etat.y,
					src ) );

	// Unlocker les surfaces
	SDL_LockSurface( (SDL_Surface*)src );
	SDL_LockSurface( dst );

	// OK
	return NTRUE;
}

// Vers une surface creee pour l'occasion
__ALLOC SDL_Surface *NLib_Module_SDL_Surface_Copier2( const SDL_Surface *src,
	const SDL_Rect *zone )
{
	// Etat actuel copie
	NUPoint p;

	// Limite de la copie
	NUPoint limite;

	// Sortie
	__OUTPUT SDL_Surface *out;

	// Verifier
		// Parametres
		if( !src
			|| zone ?
				( zone->x + zone->w > src->w || zone->y + zone->h > src->h ) 
				: NFALSE )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

			// Quitter
			return NULL;
		}

	// Creer la surface
	if( !( out = NLib_Module_SDL_Surface_Creer( zone ?
			zone->w
			: src->w,
		zone ?
			zone->h
			: src->h,
		( src->format->Amask != 0 )
			? NTRUE
			: NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Definir la limite
	NDEFINIR_POSITION( limite,
		zone ?
			( zone->x + zone->w )
			: src->w,
		zone ?
			( zone->y + zone->h )
			: src->h );

	// Lock surfaces
	if( SDL_LockSurface( (SDL_Surface*)src ) == NSDL_ERREUR
		|| SDL_LockSurface( out ) == NSDL_ERREUR )
	{
		// Unlock
		SDL_UnlockSurface( (SDL_Surface *)src );
		SDL_UnlockSurface( out );

		// Liberer
		NLIB_NETTOYER_SURFACE( out );

		// Quitter
		return NULL;
	}


	// Copier
	for( p.x = zone ? zone->x : 0; p.x < limite.x; p.x++ )
		for( p.y = zone ? zone->y : 0; p.y < limite.y; p.y++ )
			NLib_Module_SDL_Surface_Putpixel2( zone ?
					p.x - zone->x
					: p.x,
				zone ?
					p.y - zone->y
					: p.y,
				out,
				NLib_Module_SDL_Surface_Getpixel2( p.x,
					p.y,
					(SDL_Surface*)src ) );

	// Unlock surface
	SDL_UnlockSurface( (SDL_Surface *)src );
	SDL_UnlockSurface( out );

	// OK
	return out;
}

/* Put pixel */
NBOOL NLib_Module_SDL_Surface_Putpixel( NU32 x,
	NU32 y,
	NU8 **pixels,
	NU32 pitch,
	NU32 bpp,
	NU32 pixel )
{
	// Pixel que l'on considere
	NU8 *p = *pixels + y * pitch + x * bpp;

	// Put
	switch( bpp )
	{
		case 4:
			*(NU32*)p = pixel;
			break;
		case 3:
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
			p[ 0 ] = (pixel >> 16) & 0xff;
			p[ 1 ] = (pixel >> 8) & 0xff;
			p[ 2 ] = pixel & 0xff;
#else // SDL_BYTEORDER == SDL_BIG_ENDIAN
			p[ 0 ] = pixel & 0xff;
			p[ 1 ] = (pixel >> 8) & 0xff;
			p[ 2 ] = (pixel >> 16) & 0xff;
#endif // SDL_BYTEORDER != SDL_BIG_ENDIAN
			break;
		case 1:
			*p = (NU8)pixel;
			break;
		case 2:
			*(NU16*)p = (NU16)pixel;
			break;

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

NBOOL NLib_Module_SDL_Surface_Putpixel2( NU32 x,
	NU32 y,
	SDL_Surface *s,
	NU32 pixel )
{
	// Verifier
	if( !s
		|| x >= (NU32)s->w
		|| y >= (NU32)s->h )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Executer
	return NLib_Module_SDL_Surface_Putpixel( x,
		y,
		(NU8**)&s->pixels,
		s->pitch,
		s->format->BytesPerPixel,
		pixel );
}

/* Get pixel */
NU32 NLib_Module_SDL_Surface_Getpixel( NU32 x,
	NU32 y,
	const NU8 **pixels,
	NU32 pitch,
	NU32 bpp )
{
	// Pixel que l'on considere
	const NU8 *p = *pixels + y * pitch + x * bpp;

	switch( bpp )
	{
		case 1:
			return *p;
		case 2:
			return *(NU16*)p;
		case 3:
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
			return p[ 0 ] << 16 | p[ 1 ] << 8 | p[ 2 ];
#else // SDL_BYTEORDER == SDL_BIG_ENDIAN
			return p[ 0 ] | p[ 1 ] << 8 | p[ 2 ] << 16;
#endif // SDL_BYTEORDER != SDL_BIG_ENDIAN
		case 4:
			return *(NU32*)p;

		default:
			return 0;
	}
}

NU32 NLib_Module_SDL_Surface_Getpixel2( NU32 x,
	NU32 y,
	const SDL_Surface *s )
{
	// Verifier
	if( !s
		|| x >= (NU32)s->w
		|| y >= (NU32)s->h )
		return 0;

	// Executer
	return NLib_Module_SDL_Surface_Getpixel( x,
		y,
		(const NU8**)&s->pixels,
		s->pitch,
		s->format->BytesPerPixel );
}

/* Inverser */
__ALLOC SDL_Surface *NLib_Module_SDL_Surface_Inverser( const SDL_Surface *entree, 
	NBOOL verticalement,
	NBOOL horizontalement )
{
	// Surface en sortie
	__OUTPUT SDL_Surface *sortie = NULL;

	// Position
	NUPoint p = { 0, };

	// Verifier
		// Parametres
		if( !entree
			|| ( !verticalement
				&& !horizontalement ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

			// Quitter
			return NULL;
		}

	// Creer surface
	if( !( sortie = SDL_CreateRGBSurface( SDL_RLEACCEL,
		entree->w,
		entree->h,
		32,
		entree->format->Rmask,
		entree->format->Gmask,
		entree->format->Bmask,
		entree->format->Amask ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Quitter
		return NULL;
	}

	// Lock les surfaces
		// Entree
			if( SDL_MUSTLOCK( entree ) )
				if( SDL_LockSurface( (SDL_Surface*)entree ) == NSDL_ERREUR )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_SDL );

					// Liberer
					NLIB_NETTOYER_SURFACE( sortie );

					// Quitter
					return NULL;
				}
		// Sortie
			if( SDL_MUSTLOCK( sortie ) )
				if( SDL_LockSurface( sortie ) == NSDL_ERREUR )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_SDL );

					// Liberer
					NLIB_NETTOYER_SURFACE( sortie );

					// Unlock
					SDL_UnlockSurface( (SDL_Surface*)entree );

					// Quitter
					return NULL;
				}

	// Inverser surface
	for( p.x = 0; (NS32)p.x < entree->w; p.x++ )
		for( p.y = 0; (NS32)p.y < entree->h; p.y++ )
			NLib_Module_SDL_Surface_Putpixel2( horizontalement ?
					( entree->w - p.x - 1 )
					: p.x,
				verticalement ?
					( entree->h - p.y - 1 )
					: p.y,
				sortie,
				NLib_Module_SDL_Surface_Getpixel2( 
					p.x,
					p.y,
					(SDL_Surface*)entree ) );

	// Unlock
	SDL_UnlockSurface( (SDL_Surface*)entree );
	SDL_UnlockSurface( sortie );

	// OK
	return sortie;
}

#endif // NLIB_MODULE_SDL

