#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------------
// struct NLib::Module::SDL::Surface::NTileset
// -------------------------------------------

#ifdef NLIB_MODULE_SDL
#ifdef NLIB_MODULE_SDL_IMAGE
/* Construire */
__ALLOC NTileset *NLib_Module_SDL_Surface_NTileset_Construire( const char *lien,
	NUPoint tailleCase,
	const NFenetre *fenetre )
{
	// Sortie
	__OUTPUT NTileset *out;

	// Surface tileset
	SDL_Surface *surface,
		*caseTileset;

	// Zone tileset
	SDL_Rect zone;

	// Iterateur
	NU32 x,
		y;
	NU32 i,
		j;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NTileset ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Charger le tileset
	if( !( surface = NLib_Module_SDL_Image_Charger( lien ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL_IMAGE );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Verifier
	if( (NU32)surface->w % tailleCase.x != 0
		|| (NU32)surface->h % tailleCase.y != 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Fermer surface
		NLIB_NETTOYER_SURFACE( surface );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Calculer le nombre de cases
	if( !( out->m_nombreCase.x = (NU32)surface->w / tailleCase.x )
		|| !( out->m_nombreCase.y = (NU32)surface->h / tailleCase.y ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Fermer surface
		NLIB_NETTOYER_SURFACE( surface );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier la taille case
	NDEFINIR_POSITION( out->m_tailleCase,
		tailleCase.x,
		tailleCase.y );

	// Allouer la memoire
		// Allouer 1ere dimension
			if( !( out->m_cases = calloc( out->m_nombreCase.x,
				sizeof( NSurface** ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Fermer surface
				NLIB_NETTOYER_SURFACE( surface );

				// Liberer
				NFREE( out );

				// Quitter
				return NULL;
			}
		// Allouer la seconde dimension
			for( x = 0; x < out->m_nombreCase.x; x++ )
				if( !( out->m_cases[ x ] = calloc( out->m_nombreCase.y,
					sizeof( NSurface* ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Liberer surfaces
						// 2D
							for( i = 0; i < x; i++ )
								NFREE( out->m_cases[ i ] );
						// 1D
							NFREE( out->m_cases );
						
					// Fermer surface
					NLIB_NETTOYER_SURFACE( surface );

					// Liberer
					NFREE( out );

					// Quitter
					return NULL;
				}

#define NERREUR_CREATION_TILESET( erreur ) \
	{ \
		/* Notifier */ \
		NOTIFIER_ERREUR( erreur ); \
	 \
		/* Liberer surfaces */ \
			/* 2D */ \
				for( i = 0; i < x; i++ ) \
				{ \
					/* Surfaces */ \
					for( j = 0; j < y; j++ ) \
						NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_cases[ x ][ y ] ); \
 \
					NFREE( out->m_cases[ i ] ); \
				} \
			/* 1D */ \
				NFREE( out->m_cases ); \
	 \
		/* Fermer surface */ \
		NLIB_NETTOYER_SURFACE( surface ); \
	 \
		/* Liberer */ \
		NFREE( out ); \
	 \
		/* Quitter */ \
		return NULL; \
	} 

	// Copier les surfaces
	for( x = 0; x < out->m_nombreCase.x; x++ )
		for( y = 0; y < out->m_nombreCase.y; y++ )
		{
			// Definir la taille
			NDEFINIR_TAILLE( zone,
				(NS32)out->m_tailleCase.x,
				(NS32)out->m_tailleCase.y );

			// Calculer position actuelle
			NDEFINIR_POSITION( zone,
				(NS32)( x * out->m_tailleCase.x ),
				(NS32)( y * out->m_tailleCase.y ) );

			// Recuperer la petite case
			if( !( caseTileset = NLib_Module_SDL_Surface_Copier2( surface,
				&zone ) ) )
				NERREUR_CREATION_TILESET( NERREUR_SDL );

			// Creer la surface
			if( !( out->m_cases[ x ][ y ] = NLib_Module_SDL_Surface_NSurface_Construire3( fenetre,
				caseTileset ) ) )
				NERREUR_CREATION_TILESET( NERREUR_SDL );

			// Fermer la case
			NLIB_NETTOYER_SURFACE( caseTileset );
		}

#undef NERREUR_CREATION_TILESET

	// Nettoyer la surface
	NLIB_NETTOYER_SURFACE( surface );

	// OK
	return out;
}

/* Detruire */
void NLib_Module_SDL_Surface_NTileset_Detruire( NTileset **this )
{
	// Iterateur
	NU32 x,
		y;

	// Liberer cases
		// 2D
			for( x = 0; x < (*this)->m_nombreCase.x; x++ )
			{
				// Surfaces
				for( y = 0; y < (*this)->m_nombreCase.y; y++ )
					// Detruire surface
					NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_cases[ x ][ y ] );

				// 2D
				NFREE( (*this)->m_cases[ x ] );
			}
		// 1D
			NFREE( (*this)->m_cases );

	// Liberer
	NFREE( *this );
}

/* Obtenir case */
const NSurface *NLib_Module_SDL_Surface_NTileset_ObtenirCase( const NTileset *this,
	NUPoint position )
{
	// Verifier
	if( position.x >= this->m_nombreCase.x
		|| position.y >= this->m_nombreCase.y )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// OK
	return this->m_cases[ position.x ][ position.y ];
}

/* Obtenir le nombre de case */
NUPoint NLib_Module_SDL_Surface_NTileset_ObtenirNombreCase( const NTileset *this )
{
	return this->m_nombreCase;
}

/* Obtenir la taille d'une case */
NUPoint NLib_Module_SDL_Surface_NTileset_ObtenirTailleCase( const NTileset *this )
{
	return this->m_tailleCase;
}

#endif // NLIB_MODULE_SDL_IMAGE
#endif // NLIB_MODULE_SDL

