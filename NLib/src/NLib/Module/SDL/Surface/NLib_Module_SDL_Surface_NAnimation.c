#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ---------------------------------------------
// struct NLib::Module::SDL::Surface::NAnimation
// ---------------------------------------------

#ifdef NLIB_MODULE_SDL
#ifdef NLIB_MODULE_SDL_IMAGE
/* Construire */
__ALLOC NAnimation *NLib_Module_SDL_Surface_NAnimation_Construire( const char *lien,
	NUPoint tailleFrame,
	NU32 delaiEntreFrame,
	const NFenetre *fenetre )
{
	return NLib_Module_SDL_Surface_NAnimation_Construire2( lien,
		tailleFrame,
		delaiEntreFrame,
		fenetre,
		NULL,
		0 );
}

__ALLOC NAnimation *NLib_Module_SDL_Surface_NAnimation_Construire2( const char *lien,
	NUPoint tailleFrame,
	NU32 delaiEntreFrame,
	const NFenetre *fenetre,
	const NU32 *ordreEtapeAnimation,
	NU32 nombreEtapeAnimation )
{
	// Sortie
	__OUTPUT NAnimation *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NAnimation ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire le tileset
	if( !( out->m_animation = NLib_Module_SDL_Surface_NTileset_Construire( lien,
		tailleFrame,
		fenetre ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire l'etat
	if( !( out->m_etat = NLib_Temps_Animation_NEtatAnimation_Construire( delaiEntreFrame,
		NLib_Module_SDL_Surface_NTileset_ObtenirNombreCase( out->m_animation ).x,
		nombreEtapeAnimation,
		ordreEtapeAnimation ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire tileset
		NLib_Module_SDL_Surface_NTileset_Detruire( &out->m_animation );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/* Detruire */
void NLib_Module_SDL_Surface_NAnimation_Detruire( NAnimation **this )
{
	// Detruire tileset
	NLib_Module_SDL_Surface_NTileset_Detruire( &(*this)->m_animation );

	// Detruire etat
	NLib_Temps_Animation_NEtatAnimation_Detruire( &(*this)->m_etat );

	// Liberer
	NFREE( *this );
}

/* Update */
void NLib_Module_SDL_Surface_NAnimation_Update( NAnimation *this )
{
	NLib_Temps_Animation_NEtatAnimation_Update( this->m_etat );
}

/* Afficher */
NBOOL NLib_Module_SDL_Surface_NAnimation_Afficher( const NAnimation *this,
	NU32 animation )
{
	return NLib_Module_SDL_Surface_NAnimation_Afficher2( this,
		animation,
		NLIB_MODULE_SDL_SURFACE_NANIMATION_NIVEAU_ZOOM_DEFAUT );
}

NBOOL NLib_Module_SDL_Surface_NAnimation_Afficher2( const NAnimation *this,
	NU32 animation,
	NU32 zoom )
{
	return NLib_Module_SDL_Surface_NAnimation_Afficher3( this,
		animation,
		zoom,
		NLib_Temps_Animation_NEtatAnimation_ObtenirFrame( this->m_etat ) );
}

NBOOL NLib_Module_SDL_Surface_NAnimation_Afficher3( const NAnimation *this,
	NU32 animation,
	NU32 zoom,
	NU32 frame )
{
	// Surface
	NSurface *surface;

	// Position de la case
	NUPoint positionCase;

	// Definir la position
	NDEFINIR_POSITION( positionCase,
		frame,
		animation );

	// Obtenir la surface
	if( !( surface = (NSurface*)NLib_Module_SDL_Surface_NTileset_ObtenirCase( this->m_animation,
		positionCase ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Definir la position
	NLib_Module_SDL_Surface_NSurface_DefinirPosition( surface,
		this->m_position.x,
		this->m_position.y );
	
	// Definir le zoom
	NLib_Module_SDL_Surface_NSurface_DefinirZoom( surface,
		zoom );

	// Afficher
	NLib_Module_SDL_Surface_NSurface_Afficher( surface );

	// OK
	return NTRUE;
}

NBOOL NLib_Module_SDL_Surface_NAnimation_Afficher4( const NAnimation *this,
	NU32 animation,
	NU32 frame )
{
	return NLib_Module_SDL_Surface_NAnimation_Afficher3( this,
		animation,
		NLIB_MODULE_SDL_SURFACE_NANIMATION_NIVEAU_ZOOM_DEFAUT,
		frame );
}

/* Definir la position */
void NLib_Module_SDL_Surface_NAnimation_DefinirPosition( NAnimation *this,
	NSPoint position )
{
	NDEFINIR_POSITION( this->m_position,
		position.x,
		position.y );
}

/* Obtenir la taille */
NUPoint NLib_Module_SDL_Surface_NAnimation_ObtenirTaille( const NAnimation *this )
{
	return NLib_Module_SDL_Surface_NTileset_ObtenirTailleCase( this->m_animation );
}

/* Obtenir le nombre d'animations */
NU32 NLib_Module_SDL_Surface_NAnimation_ObtenirNombreAnimation( const NAnimation *this )
{
	return NLib_Module_SDL_Surface_NTileset_ObtenirNombreCase( this->m_animation ).y;
}

/* Obtenir le nombre de frames */
NU32 NLib_Module_SDL_Surface_NAnimation_ObtenirNombreFrame( const NAnimation *this )
{
	return NLib_Temps_Animation_NEtatAnimation_ObtenirNombreFrame( this->m_etat );
}

/* Obtenir la position */
NSPoint NLib_Module_SDL_Surface_NAnimation_ObtenirPosition( const NAnimation *this )
{
	return this->m_position;
}

/* Obtenir la frame */
NU32 NLib_Module_SDL_Surface_NAnimation_ObtenirFrame( const NAnimation *this )
{
	return NLib_Temps_Animation_NEtatAnimation_ObtenirFrame( this->m_etat );
}

/* Obtenir l'etat */
const NEtatAnimation *NLib_Module_SDL_Surface_NAnimation_ObtenirEtat( const NAnimation *this )
{
	return this->m_etat;
}

#endif // NLIB_MODULE_SDL_IMAGE
#endif // NLIB_MODULE_SDL

