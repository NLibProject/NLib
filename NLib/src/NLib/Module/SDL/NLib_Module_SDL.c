#define NLIB_MODULE_SDL_INTERNE
#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ---------------------------
// namespace NLib::Module::SDL
// ---------------------------

#ifdef NLIB_MODULE_SDL
/* Initialiser la SDL */
NBOOL NLib_Module_SDL_Initialiser( void )
{
	// Initialiser la SDL
	return (NBOOL)( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_GAMECONTROLLER ) != NSDL_ERREUR );
}

/* Detruire la SDL */
void NLib_Module_SDL_Detruire( void )
{
	// Liberer curseur
	if( m_curseur != NULL )
	{
		// Detruire
		SDL_FreeCursor( m_curseur );

		// Oublier
		NDISSOCIER_ADRESSE( m_curseur );
	}

	// Detruire la SDL
	SDL_Quit( );
}

/* Changer curseur */
void NLib_Module_SDL_DefinirCurseur( const SDL_Surface *surface,
	NS32 x,
	NS32 y )
{
	// Liberer l'ancien curseur
	if( m_curseur != NULL )
	{
		// Detruire
		SDL_FreeCursor( m_curseur );

		// Oublier
		NDISSOCIER_ADRESSE( m_curseur );
	}
	else
		if( m_curseurDefaut == NULL )
			m_curseurDefaut = SDL_GetCursor( );

	// Creer
	if( !( m_curseur = SDL_CreateColorCursor( (SDL_Surface*)surface,
		x,
		y ) ) )
		return;

	// Definir
	SDL_SetCursor( m_curseur );
}

void NLib_Module_SDL_DefinirCurseur2( const SDL_Surface *surface )
{
	NLib_Module_SDL_DefinirCurseur( surface,
		0,
		0 );
}

/* Restaurer curseur */
void NLib_Module_SDL_RestaurerCurseur( void )
{
	// Verifier
	if( !m_curseurDefaut )
		return;

	// Restaurer
	SDL_SetCursor( m_curseurDefaut );
}

/**
 * Obtenir la resolution actuelle
 *
 * @return la resolution actuelle
 */
NUPoint NLib_Module_SDL_ObtenirResolutionActuelle( void )
{
	// Display mode
	SDL_DisplayMode display;

	// Sortie
	__OUTPUT NUPoint out;

	// Lire les donnees
	SDL_GetCurrentDisplayMode( 0,
		&display );

	// Copier
	NDEFINIR_POSITION( out,
		display.w,
		display.h );

	// OK
	return out;
}

#endif // NLIB_MODULE_SDL

