#include "../../../../include/NLib/NLib.h"
#include "../../../../include/NLib/Module/SDL/NLib_Module_SDL_NFenetre.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------
// struct NLib::Module::SDL::NFenetre
// ----------------------------------

#ifdef NLIB_MODULE_SDL
/* Construire la fenetre */
__ALLOC NFenetre *NLib_Module_SDL_NFenetre_Construire( const char *titre,
	NUPoint resolution,
	NBOOL estPleinEcran )
{
	return NLib_Module_SDL_NFenetre_Construire2( titre,
		resolution,
		estPleinEcran,
		NTRUE );
}

__ALLOC NFenetre *NLib_Module_SDL_NFenetre_Construire2( const char *titre,
	NUPoint resolution,
	NBOOL estPleinEcran,
	NBOOL estVisible ) {
	// position
	NSPoint position = { SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED };

	// build window
	return NLib_Module_SDL_NFenetre_Construire3( titre,
		resolution,
		estPleinEcran,
		estVisible,
		position );
}

__ALLOC NFenetre *NLib_Module_SDL_NFenetre_Construire3( const char *titre,
	NUPoint resolution,
	NBOOL estPleinEcran,
	NBOOL estVisible,
	NSPoint position ) {
	// Sortie
	__OUTPUT NFenetre *out;

	// Index renderer
	NU32 indexRenderer;

	// Nombre renderer
	NS32 nombreRenderer;

	// Renderer informations
	SDL_RendererInfo informationsRenderer;

	// Renderer prefere present? (NLIB_MODULE_SDL_RENDERER)
	NBOOL estRendererPresent = NFALSE;

	// Construire la fenetre
	if( !( out = calloc( 1,
		sizeof( NFenetre ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Creer la fenetre
	if( !( out->m_fenetre = SDL_CreateWindow( titre,
		position.x,
		position.y,
		resolution.x,
		resolution.y,
		( estVisible ? SDL_WINDOW_SHOWN : SDL_WINDOW_HIDDEN )
		| ( estPleinEcran ? SDL_WINDOW_FULLSCREEN : 0 ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Quitter
		return NULL;
	}

	// Verifier qu'il y ait bien des renderers de disponible
	if( ( nombreRenderer = SDL_GetNumRenderDrivers( ) ) <= 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Liberer
		SDL_DestroyWindow( out->m_fenetre );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Chercher le renderer qui nous interesse
	for( indexRenderer = 0; indexRenderer < (NU32)nombreRenderer; indexRenderer++ )
	{
		// Obtenir les informations
		if( SDL_GetRenderDriverInfo( indexRenderer,
			&informationsRenderer ) == NSDL_ERREUR )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_SDL );

			// Liberer
			SDL_DestroyWindow( out->m_fenetre );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Verifier le nom du renderer
		if( !strcmp( informationsRenderer.name,
			NLIB_MODULE_SDL_RENDERER ) )
		{
			// On a trouve notre renderer
			estRendererPresent = NTRUE;

			// Sortie
			break;
		}
	}

	// Verifier
	if( !estRendererPresent )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Liberer
		SDL_DestroyWindow( out->m_fenetre );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer le renderer
	if( !( out->m_renderer = SDL_CreateRenderer( out->m_fenetre,
		indexRenderer,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Liberer
		SDL_DestroyWindow( out->m_fenetre );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Definir la methode alpha du renderer
	SDL_SetRenderDrawBlendMode( out->m_renderer,
		SDL_BLENDMODE_BLEND );

	// Enregistrer
	out->m_resolution = resolution;

	// OK
	return out;
}

/* Detruire la fenetre */
void NLib_Module_SDL_NFenetre_Detruire( NFenetre **this )
{
	// Fermer la fenetre
	SDL_DestroyWindow( (*this)->m_fenetre );

	// Liberer
	NFREE( *this );
}

/* Obtenir la fenetre */
SDL_Window *NLib_Module_SDL_NFenetre_ObtenirFenetre( const NFenetre *this )
{
	return this->m_fenetre;
}

/* Obtenir le renderer */
SDL_Renderer *NLib_Module_SDL_NFenetre_ObtenirRenderer( const NFenetre *this )
{
	return this->m_renderer;
}

/* Obtenir la resolution */
const NUPoint *NLib_Module_SDL_NFenetre_ObtenirResolution( const NFenetre *this )
{
	return &this->m_resolution;
}

/* Obtenir l'evenement */
SDL_Event *NLib_Module_SDL_NFenetre_ObtenirEvenement( const NFenetre *this )
{
	return &((NFenetre*)this)->m_evenement;
}

/* Nettoyer la fenetre */
void NLib_Module_SDL_NFenetre_Nettoyer( NFenetre *this )
{
	NLib_Module_SDL_NFenetre_Nettoyer2( this,
		0,
		0,
		0,
		0xFF );
}

void NLib_Module_SDL_NFenetre_Nettoyer2( NFenetre *this,
	NU8 r,
	NU8 g,
	NU8 b,
	NU8 a )
{
	// Definir la couleur
	SDL_SetRenderDrawColor( this->m_renderer,
		r,
		g,
		b,
		a );

	// Nettoyer
	SDL_RenderClear( this->m_renderer );
}

/* Dessiner un point */
void NLib_Module_SDL_NFenetre_DessinerPoint( NFenetre *this,
	NSPoint p,
	NCouleur couleur )
{
	// Definir la couleur
	SDL_SetRenderDrawColor( this->m_renderer,
		couleur.r,
		couleur.g,
		couleur.b,
		couleur.a );

	// Dessiner
	SDL_RenderDrawPoint( this->m_renderer,
		p.x,
		p.y );
}

/* Dessiner des points */
void NLib_Module_SDL_NFenetre_DessinerPoints( NFenetre *this,
	NSPoint *p,
	NU32 nombrePoint,
	NCouleur couleur )
{
	// Definir la couleur
	SDL_SetRenderDrawColor( this->m_renderer,
		couleur.r,
		couleur.g,
		couleur.b,
		couleur.a );

	// Dessiner
	SDL_RenderDrawPoints( this->m_renderer,
		(SDL_Point*)p,
		nombrePoint );
}

/* Dessiner une ligne */
void NLib_Module_SDL_NFenetre_DessinerLigne( NFenetre *this,
	NSPoint p1,
	NSPoint p2,
	NCouleur couleur )
{
	// Definir la couleur
	SDL_SetRenderDrawColor( this->m_renderer,
		couleur.r,
		couleur.g,
		couleur.b,
		couleur.a );

	// Dessiner
	SDL_RenderDrawLine( this->m_renderer,
		p1.x,
		p1.y,
		p2.x,
		p2.y );
}

/* Dessiner un rectangle */
void NLib_Module_SDL_NFenetre_DessinerRectangle( NFenetre *this,
	NSRect rectangle,
	NU32 epaisseur,
	NCouleur couleurContour,
	NCouleur couleurFond )
{
	// Rectangle
	SDL_Rect r[ NDIRECTIONS + 1 ];

	// Definir rectangle bordures
		// Haut
			NDEFINIR_POSITION( r[ NHAUT ],
				rectangle.m_position.x,
				rectangle.m_position.y );

			NDEFINIR_TAILLE( r[ NHAUT ],
				rectangle.m_taille.x,
				epaisseur );
		// Bas
			NDEFINIR_POSITION( r[ NBAS ],
				rectangle.m_position.x,
				rectangle.m_position.y + rectangle.m_taille.y );

			NDEFINIR_TAILLE( r[ NBAS ],
				rectangle.m_taille.x + epaisseur, // Correction
				epaisseur );
		// Gauche
			NDEFINIR_POSITION( r[ NGAUCHE ],
				rectangle.m_position.x,
				rectangle.m_position.y );

			NDEFINIR_TAILLE( r[ NGAUCHE ],
				epaisseur,
				rectangle.m_taille.y );
		// Droite
			NDEFINIR_POSITION( r[ NDROITE ],
				rectangle.m_position.x + rectangle.m_taille.x,
				rectangle.m_position.y );

			NDEFINIR_TAILLE( r[ NDROITE ],
				epaisseur,
				rectangle.m_taille.y );
		// Centre
			NDEFINIR_POSITION( r[ NDIRECTIONS ],
				rectangle.m_position.x + epaisseur,
				rectangle.m_position.y + epaisseur );

			NDEFINIR_TAILLE( r[ NDIRECTIONS ],
				rectangle.m_taille.x - epaisseur,
				rectangle.m_taille.y - epaisseur );

	// Dessiner
	NLib_Module_SDL_NFenetre_DessinerRectangle2( this,
		r,
		&couleurContour,
		&couleurFond );
}

void NLib_Module_SDL_NFenetre_DessinerRectangle2( NFenetre *this,
	const SDL_Rect coordonnees[ NDIRECTIONS + 1 ],
	const NCouleur *couleurContour,
	const NCouleur *couleurFond )
{
	// Definir la couleur du dessin
	SDL_SetRenderDrawColor( NLib_Module_SDL_NFenetre_ObtenirRenderer( this ),
		couleurContour->r,
		couleurContour->g,
		couleurContour->b,
		couleurContour->a );

	// Dessiner rectangles
	SDL_RenderFillRects( NLib_Module_SDL_NFenetre_ObtenirRenderer( this ),
		coordonnees,
		NDIRECTIONS );

	// Si il y a un fond
	if( couleurFond->a != 0 )
	{
		// Definir la couleur du dessin
		SDL_SetRenderDrawColor( NLib_Module_SDL_NFenetre_ObtenirRenderer( this ),
			couleurFond->r,
			couleurFond->g,
			couleurFond->b,
			couleurFond->a );

		// Dessiner le rectangle
		SDL_RenderFillRect( NLib_Module_SDL_NFenetre_ObtenirRenderer( this ),
			&coordonnees[ NDIRECTIONS ] );
	}
}

/* Actualiser le renderer */
void NLib_Module_SDL_NFenetre_Update( NFenetre *this )
{
	SDL_RenderPresent( this->m_renderer );
}

/* Definir un clip rect */
void NLib_Module_SDL_NFenetre_DefinirClipRect( NFenetre *this,
	NSRect rect )
{
	// Rect
	SDL_Rect clipRect;

	// Copier
		// Position
			NDEFINIR_POSITION( clipRect,
				rect.m_position.x,
				rect.m_position.y );
		// Taille
			NDEFINIR_TAILLE( clipRect,
				rect.m_taille.x,
				rect.m_taille.y );

	// Definir le clip rect
	SDL_RenderSetClipRect( this->m_renderer,
		&clipRect );
}

/* Supprimer le clip rect */
void NLib_Module_SDL_NFenetre_SupprimerClipRect( NFenetre *this )
{
	// Supprimer
	SDL_RenderSetClipRect( this->m_renderer,
		NULL );
}

/* Afficher fenetre (SDL_WINDOW_SHOWN) */
void NLib_Module_SDL_NFenetre_AfficherFenetre( NFenetre *this )
{
	// Afficher
	SDL_ShowWindow( this->m_fenetre );

	// Donner le focus
	SDL_RaiseWindow( this->m_fenetre );
}

/* Cache fenetre (SDL_WINDOW_HIDDEN) */
void NLib_Module_SDL_NFenetre_CacherFenetre( NFenetre *this )
{
	SDL_HideWindow( this->m_fenetre );
}

/**
 * La fenetre est affichee?
 *
 * @param this
 * 		Cette instance
 *
 * @return si la fenetre est affichee
 */
NBOOL NLib_Module_SDL_NFenetre_EstAffiche( const NFenetre *this )
{
	return (NBOOL)( ( SDL_GetWindowFlags( this->m_fenetre ) & SDL_WINDOW_SHOWN ) != 0 );
}

#endif // NLIB_MODULE_SDL

