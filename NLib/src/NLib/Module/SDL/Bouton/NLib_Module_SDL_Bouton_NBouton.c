#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -----------------------------------------
// struct NLib::Module::SDL::Bouton::NBouton
// -----------------------------------------

#ifdef NLIB_MODULE_SDL
/* Construire l'objet */
__ALLOC NBouton *NLib_Module_SDL_Bouton_NBouton_Construire( NSPoint position,
	NUPoint taille,
	NCouleur couleur,
	NCouleur couleurFond,
	const NFenetre *fenetre,
	NU32 epaisseur )
{
	// Sortie
	__OUTPUT NBouton *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NBouton ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire cadre
	if( !( out->m_cadre = NLib_Module_SDL_NCadre_Construire( position,
		taille,
		couleur,
		couleurFond,
		fenetre,
		epaisseur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/* Detruire l'objet */
void NLib_Module_SDL_Bouton_NBouton_Detruire( NBouton **this )
{
	// Detruire cadre
	NLib_Module_SDL_NCadre_Detruire( &(*this)->m_cadre );

	// Liberer
	NFREE( *this );
}

/* Mettre a jour */
void NLib_Module_SDL_Bouton_NBouton_Update( NBouton *this,
	const NSPoint *positionSouris,
	const SDL_Event *evt )
{
	// Le curseur est sur le bouton
	if( NLib_Module_SDL_NCadre_EstEnColisionAvec3( this->m_cadre,
		*positionSouris ) )
	{
		// Pression
		if( this->m_etat != NETAT_BOUTON_PRESSE )
		{
			if( evt->type == SDL_MOUSEBUTTONDOWN
				|| evt->type == SDL_FINGERDOWN )
			{
				// Le bouton est presse
				this->m_etat = NETAT_BOUTON_PRESSE;

				// On enregistre le bouton
				this->m_bouton = evt->button.button;
			}
			else
				this->m_etat = NETAT_BOUTON_SURVOLE;
		}

		// Relache
		if( evt->type == SDL_MOUSEBUTTONUP )
			this->m_etat = NETAT_BOUTON_SURVOLE;
	}
	// Le curseur n'est pas sur le bouton
	else
	{
		// Relache en dehors du cadre
		if( evt->type == SDL_MOUSEBUTTONUP )
			this->m_etat = NETAT_BOUTON_REPOS;
		// Sors simplement du cadre
		else if( this->m_etat == NETAT_BOUTON_SURVOLE )
			this->m_etat = NETAT_BOUTON_REPOS;
	}
}

/* Definir position */
void NLib_Module_SDL_Bouton_NBouton_DefinirPosition( NBouton *this,
	NSPoint position )
{
	NLib_Module_SDL_NCadre_DefinirPosition( this->m_cadre,
		position.x,
		position.y );
}

/* Definir taille */
void NLib_Module_SDL_Bouton_NBouton_DefinirTaille( NBouton *this,
	NUPoint taille )
{
	NLib_Module_SDL_NCadre_DefinirTaille( this->m_cadre,
		taille.x,
		taille.y );
}

/* Definir couleur */
void NLib_Module_SDL_Bouton_NBouton_DefinirCouleur( NBouton *this,
	NCouleur couleur )
{
	NLib_Module_SDL_NCadre_DefinirCouleur( this->m_cadre,
		couleur.r,
		couleur.g,
		couleur.b,
		couleur.a );
}

/* Definir couleur fond */
void NLib_Module_SDL_Bouton_NBouton_DefinirCouleurFond( NBouton *this,
	NCouleur couleur )
{
	NLib_Module_SDL_NCadre_DefinirCouleurFond( this->m_cadre,
		couleur.r,
		couleur.g,
		couleur.b,
		couleur.a );
}

/* Definir epaisseur */
void NLib_Module_SDL_Bouton_NBouton_DefinirEpaisseur( NBouton *this,
	NU32 epaisseur )
{
	NLib_Module_SDL_NCadre_DefinirEpaisseur( this->m_cadre,
		epaisseur );
}

/* Obtenir position */
NSPoint NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( const NBouton *this )
{
	return *NLib_Module_SDL_NCadre_ObtenirPosition( this->m_cadre );
}

/* Obtenir taille */
NUPoint NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( const NBouton *this )
{
	return *NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadre );
}

/* Obtenir couleur */
NCouleur NLib_Module_SDL_Bouton_NBouton_ObtenirCouleur( const NBouton *this )
{
	return *NLib_Module_SDL_NCadre_ObtenirCouleur( this->m_cadre );
}

/* Obtenir couleur fond */
NCouleur NLib_Module_SDL_Bouton_NBouton_ObtenirCouleurFond( const NBouton *this )
{
	return *NLib_Module_SDL_NCadre_ObtenirCouleurFond( this->m_cadre );
}

/* Obtenir epaisseur */
NU32 NLib_Module_SDL_Bouton_NBouton_ObtenirEpaisseur( const NBouton *this )
{
	return NLib_Module_SDL_NCadre_ObtenirEpaisseur( this->m_cadre );
}

/* Obtenir bouton d'activation */
NU32 NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( const NBouton *this )
{
	return this->m_bouton;
}

/* Obtenir etat */
NEtatBouton NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( const NBouton *this )
{
	return this->m_etat;
}

/* Remettre a zero */
void NLib_Module_SDL_Bouton_NBouton_RemettreEtatAZero( NBouton *this )
{
	// A zero
	this->m_etat = NETAT_BOUTON_REPOS;
}

/* Dessiner */
void NLib_Module_SDL_Bouton_NBouton_Dessiner( const NBouton *this )
{
	// Dessiner
	NLib_Module_SDL_NCadre_Dessiner( this->m_cadre );
}

/* Deplacer */
void NLib_Module_SDL_Bouton_NBouton_Deplacer( NBouton *this,
	NSPoint positionSouris,
	NBOOL continuerDeplacement )
{
	NLib_Module_SDL_NCadre_Deplacer( this->m_cadre,
		positionSouris,
		continuerDeplacement );
}

/**
 * Est visibile?
 *
 * @param this
 *		Cette instance
 *
 * @return si le bouton est visible
 */
NBOOL NLib_Module_SDL_Bouton_NBouton_EstVisible( const NBouton *this )
{
	return NLib_Module_SDL_NCadre_EstVisible( this->m_cadre );
}

/**
 * Definir si le bouton est visible
 *
 * @param this
 *		Cette instance
 * @param estVisible
 *		Visible?
 */
void NLib_Module_SDL_Bouton_NBouton_DefinirEstVisible( NBouton *this,
	NBOOL estVisible )
{
	// Definir
	NLib_Module_SDL_NCadre_DefinirEstVisible( this->m_cadre,
		estVisible );
}

#endif // NLIB_MODULE_SDL

