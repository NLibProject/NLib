#define NLIB_MODULE_SDL_SAISIE_NSAISIESDL_INTERNE
#include "../../../../../../NLib/include/NLib/NLib.h"

/**
 *	@author SOARES Lucas
 */

// --------------------------------------------
// struct NLib::Module::SDL::Saisie::NSaisieSDL
// --------------------------------------------

#ifdef NLIB_MODULE_SDL
/* Supprimer les cadres (privee) */
void NLib_Module_SDL_Saisie_NSaisieSDL_SupprimerCadresInterne( NSaisieSDL *this )
{
	// Iterateur
	NU32 i = 0;

	// Cadres
		// 2D
			for( ; i < this->m_nbCadres; i++ )
				NLib_Module_SDL_NCadre_Detruire( &this->m_cadre[ i ] );
		// 1D
			NFREE( this->m_cadre );

	// Mettre a zero
	this->m_nbCadres = 0;
	this->m_idCadreClique = NSAISIE_SDL_ID_CADRE_AUCUN;
}

/* Effacer caractere */
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_EffacerCaractereInterne( NSaisieSDL *this )
{
	// Taille actuelle
	NS32 tailleActuelle = 0;

	// Chaine temporaire
	char *strTemp = NULL;

	// Verifier
	if( !this->m_texte
		|| !( tailleActuelle = strlen( this->m_texte ) ) )
		return NFALSE;

	// Copier
		// Allouer la memoire
			if( !( strTemp = (char*)calloc( tailleActuelle + 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Quitter
				return NFALSE;
			}
		// Copier
			memcpy( strTemp,
				this->m_texte,
				tailleActuelle );

	// Suppression
	if( this->m_tailleMaximale )
	{
		// Si curseur dans chaine (pas au point initial)
		if( this->m_curseur > 0
			&& this->m_curseur - 1 + tailleActuelle - this->m_curseur < this->m_tailleMaximale )
		{
			// Copier la chaine
			memcpy( this->m_texte + this->m_curseur - 1,
				strTemp + this->m_curseur,
				tailleActuelle - this->m_curseur );

			// Caractere de fin
			this->m_texte[ tailleActuelle - 1 ] = '\0';
		}
	}
	else
	{
		// Suppression totale de la chaine
		if( tailleActuelle <= 1 )
		{
			// Liberer
			NFREE( this->m_texte );
		}
		// Il restera un caractere ou plus
		else if( this->m_curseur > 0 )
		{
			// Reallouer -1 case
			if( !NLib_Memoire_ReallouerMemoire( &this->m_texte,
				(NU32)( tailleActuelle + 1 ),
				(NU32)tailleActuelle ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Quitter
				return NFALSE;
			}

			// Copier
				// Apres curseur
					memcpy( this->m_texte + this->m_curseur - 1,
						strTemp + this->m_curseur,
						tailleActuelle - this->m_curseur );
				// Clore chaine
					this->m_texte[ tailleActuelle - 1 ] = '\0';
		}
	}

	// Liberer
	NFREE( strTemp );

	// Decrementer curseur
	if( this->m_curseur > 0 )
		this->m_curseur--;

	// OK
	return NTRUE;
}

/* Vider le texte */
void NLib_Module_SDL_Saisie_NSaisieSDL_ViderChaine( NSaisieSDL *this )
{
	// Mettre a zero
	memset( this->m_texte,
		0,
		strlen( this->m_texte ) );

	// Curseur a zero
	this->m_curseur = 0;
}

/* Mise a jour */
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_MiseAJourInterne( NSaisieSDL *this,
	char caractere,
	char **chaine,
	NU32 tailleActuelle,
	NU32 tailleMaximale )
{
	// Chaine temporaire
	char *temp = NULL;

	// Verifier
		// Parametres
			if( caractere == '\0'
				|| !chaine
				|| ( tailleActuelle >= ( ( tailleMaximale ) >= 0 ? ( tailleMaximale - 1 ) : 0 )
					&& tailleMaximale ) )
				return NFALSE;

	// Reallouer la bonne taille si necessaire
	if( !tailleMaximale )
	{
		// Allouer
		if( !( temp = calloc( tailleActuelle + 2,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Quitter
			return NFALSE;
		}

		// Ajouter le caractere
			// Copier jusqu'au curseur
				memcpy( temp,
					*chaine,
					this->m_curseur * sizeof( char ) );
			// Ajouter caractere
				temp[ this->m_curseur ] = caractere;
			// Copier depuis curseur jusqu'a la fin
				memcpy( temp + this->m_curseur,
					*chaine + this->m_curseur + 1,
					( (NS32)tailleActuelle - (NS32)this->m_curseur - 1 ) >= 0 ?
						( (NS32)tailleActuelle - (NS32)this->m_curseur - 1 )
						: 0 );

		// Liberer
		NFREE( *chaine );

		// Stocker nouvelle adresse
		*chaine = temp;

		// Incrementer curseur
		this->m_curseur++;
	}
	else
	{
		// Copier ancienne chaine
			// Allouer la memoire
				if( !( temp = (char*)calloc( tailleMaximale + 1,
					sizeof( char ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Quitter
					return NFALSE;
				}
			// Copier
				memcpy( temp,
					*chaine,
					tailleMaximale * sizeof( char ) );

		// Ajouter le caractere
			// Copier jusqu'au curseur
				memcpy( temp,
					*chaine,
					this->m_curseur * sizeof( char ) );
			// Ajouter caractere
				(*chaine)[ this->m_curseur ] = caractere;
			// Copier depuis curseur jusqu'a la fin
				memcpy( temp + this->m_curseur,
					*chaine + this->m_curseur + 1,
					(NS32)tailleMaximale - (NS32)this->m_curseur + 1 );

		// Liberer
		NFREE( temp );

		// Incrementer curseur
		if( this->m_curseur < tailleMaximale )
			this->m_curseur++;
	}

	// OK
	return NTRUE;
}

// Verification cadres
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_VerifierCadreInterne( NSaisieSDL *this,
	SDL_Event *e,
	NSPoint *p )
{
	// Iterateur
	NU32 i = 0;

	// Clic?
	if( e->button.button == SDL_BUTTON_LEFT )
	{
		// Si cadre(s) defini(s)
		if( this->m_cadre )
		{
			// Cadre englobant tout
			if( this->m_cadre == NSAISIE_SDL_CADRE_TOUT )
			{
				// Noter
				this->m_idCadreClique = NSAISIE_SDL_ID_CADRE_TOUT;

				// Cadre clique!
				return NTRUE;
			}
			else
			{
				// Chercher un cadre OK
				for( i = 0; i < this->m_nbCadres; i++ )
					// Colision?
					if( NLib_Math_Geometrie_EstColision2( *NLib_Module_SDL_NCadre_ObtenirPosition( this->m_cadre[ i ] ),
						*NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadre[ i ] ),
						*p ) )
					{
						// Noter
						this->m_idCadreClique = i;

						// Cadre clique!
						return NTRUE;
					}

				// Rien n'a ete trouve
				return NFALSE;
			}
		}
		else
		// Aucun cadre
			return NFALSE;
	}
	// Pas de clic
	else
		return NFALSE;
}

/* Construire l'objet */
__ALLOC NSaisieSDL *NLib_Module_SDL_Saisie_NSaisieSDL_Construire( NModeSaisieSDL mode,
	NU32 tailleMax,
	SDL_Keycode toucheArret,
	__CALLBACK void ( ___cdecl *callbackGestion )( void* ),
	__CALLBACK void ( ___cdecl *callbackActualisation )( void* ),
	__CALLBACK NBOOL ( ___cdecl *callbackUpdateChaine )( void* ),
	void *argumentCallbackGestion,
	void *argumentCallbackActualisation,
	void *argumentCallbackUpdateChaine,
	const char *chaineInitiale )
{
	// Sortie
	__OUTPUT NSaisieSDL *out;

	// Verifier parametres
	if( !( mode & NMODE_SAISIE_SDL_TEXTE_TOTAL ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NSaisieSDL ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
		// Mode
			out->m_mode = mode;
		// Touche d'arret
			out->m_touche = toucheArret;
		// Taille maximale
			out->m_tailleMaximale = tailleMax;
		// Callback
			out->m_callbackGestion = callbackGestion;
			out->m_callbackActualisation = callbackActualisation;
			out->m_callbackUpdateChaine = callbackUpdateChaine;
		// Arguments callback
			out->m_argumentCallbackGestion = argumentCallbackGestion;
			out->m_argumentCallbackActualisation = argumentCallbackActualisation;
			out->m_argumentCallbackUpdateChaine = argumentCallbackUpdateChaine;

	// Allouer si necessaire
	if( out->m_tailleMaximale )
		// Allouer
		if( !( out->m_texte = calloc( out->m_tailleMaximale + 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Quitter
			return NULL;
		}

	// Definir texte
	NLib_Module_SDL_Saisie_NSaisieSDL_DefinirTexte( out,
		chaineInitiale );

	// OK
	return out;
}

/* Detruire l'objet */
void NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( NSaisieSDL **this )
{
	// Liberer
		// Texte
			NFREE( (*this)->m_texte );
		// Cadres
			NLib_Module_SDL_Saisie_NSaisieSDL_SupprimerCadresInterne( *this );

	NFREE( *this );
}

/* Saisie */
NRetourSaisieSDL NLib_Module_SDL_Saisie_NSaisieSDL_Saisir( NSaisieSDL *this,
	SDL_Event *e, // Evenement["Communiquer l'evenement pour eviter la boucle infinie de repetitions de saisie( )"]
	NBOOL *ptrContinuer, // Handle etat boucle si externe
	NBOOL autoriseTab, // Autoriser "tabulation"?
	char **callbackChaine ) // Stocke en temps reel le texte saisi
{
	// Etat si non fourni en entree
	NBOOL continuer = NTRUE;

	// Resultat evenement (si touche pressee, NTRUE)
	NBOOL resultat = NFALSE;

	// Position souris
	NSPoint pSouris;

	// Caractere presse
	char caractere = 0;

	// Verrouillage accent
	NAccentSaisieSDL accent = NACCENT_SAISIE_SDL_AUCUN;

	// Modificateurs touche
	NBOOL majuscule = NFALSE;
	NBOOL altgr = NFALSE;

	// Initialiser
		// En cours...
			this->m_estEnCours = ptrContinuer ?
				ptrContinuer
				: &continuer;
		// Curseur
			this->m_curseur = this->m_texte ?
				strlen( this->m_texte )
				: 0;

	// Saisie...
	do
	{
		// Resultat traitement a zero (pour appel callback maj)
		resultat = NFALSE;

		// Lire les events
		while( SDL_PollEvent( e )
			&& *this->m_estEnCours )
		{
			// Caractere
			caractere = 0;

			// Verrouillage majuscule?
			majuscule = ( SDL_GetModState( ) & KMOD_CAPS ) ? NTRUE : NFALSE;
			majuscule = ( SDL_GetKeyboardState( NULL )[ SDL_SCANCODE_LSHIFT ] || SDL_GetKeyboardState( NULL )[ SDL_SCANCODE_RSHIFT ] ) ?
				!majuscule
				: majuscule;

			// Lecture
			switch( e->type )
			{
				case SDL_WINDOWEVENT:
					switch( e->window.event )
					{
						case SDL_WINDOWEVENT_CLOSE:
							// Oublier
							NDISSOCIER_ADRESSE( this->m_estEnCours );

							// On sort
							return NRETOUR_SAISIE_SDL_QUITTER;

						default:
							break;
					}
					break;
				case SDL_MOUSEMOTION:
					// Mettre a jour position souris
					NDEFINIR_POSITION( pSouris,
						e->motion.x,
						e->motion.y );
					break;
				case SDL_MOUSEBUTTONUP:
					if( NLib_Module_SDL_Saisie_NSaisieSDL_VerifierCadreInterne( this,
						e,
						&pSouris ) )
					{
						// Plus en cours
						*this->m_estEnCours = NFALSE;

						// Oublier
						NDISSOCIER_ADRESSE( this->m_estEnCours );

						// Sortie via clic
						return NRETOUR_SAISIE_SDL_CLIC_BOUTON;
					}
					break;
				case SDL_KEYUP:
					// Touches de modificateurs
						// Shift
							switch( e->key.keysym.sym )
							{
								case SDLK_LSHIFT:
								case SDLK_RSHIFT:
									majuscule = (NBOOL)!majuscule;
									break;
								case SDLK_RALT:
									altgr = NFALSE;
									break;

								default:
									break;
							}
					break;
				case SDL_KEYDOWN:
					// Touches de modificateurs
					switch( e->key.keysym.sym )
					{
						case SDLK_LSHIFT:
						case SDLK_RSHIFT:
							majuscule = (NBOOL)!majuscule;
							break;
						case SDLK_CAPSLOCK:
							majuscule = (NBOOL)!majuscule;
							break;
						case SDLK_RALT:
							altgr = NTRUE;
							break;
						case SDLK_TAB:
							if( autoriseTab )
							{
								// Oublier
								NDISSOCIER_ADRESSE( this->m_estEnCours );

								// Suivant!
								return NRETOUR_SAISIE_SDL_TAB;
							}
							break;

						case SDLK_BACKSPACE:
							if( NLib_Module_SDL_Saisie_NSaisieSDL_EffacerCaractereInterne( this ) )
								resultat = NTRUE;
							break;

						default:
							break;
					}

					// Lettres
					if( this->m_mode & NMODE_SAISIE_SDL_LETTRES )
					{
						switch( e->key.keysym.sym )
						{
							case SDLK_a:
								NSAISIE_CARACTERE( 'a', 'A', majuscule, caractere );
								break;
							case SDLK_b:
								NSAISIE_CARACTERE( 'b', 'B', majuscule, caractere );
								break;
							case SDLK_c:
								NSAISIE_CARACTERE( 'c', 'C', majuscule, caractere );
								break;
							case SDLK_d:
								NSAISIE_CARACTERE( 'd', 'D', majuscule, caractere );
								break;
							case SDLK_e:
								if( !altgr )
									NSAISIE_CARACTERE( 'e', 'E', majuscule, caractere );
								break;
							case SDLK_f:
								NSAISIE_CARACTERE( 'f', 'F', majuscule, caractere );
								break;
							case SDLK_g:
								NSAISIE_CARACTERE( 'g', 'G', majuscule, caractere );
								break;
							case SDLK_h:
								NSAISIE_CARACTERE( 'h', 'H', majuscule, caractere );
								break;
							case SDLK_i:
								NSAISIE_CARACTERE( 'i', 'I', majuscule, caractere );
								break;
							case SDLK_j:
								NSAISIE_CARACTERE( 'j', 'J', majuscule, caractere );
								break;
							case SDLK_k:
								NSAISIE_CARACTERE( 'k', 'K', majuscule, caractere );
								break;
							case SDLK_l:
								NSAISIE_CARACTERE( 'l', 'L', majuscule, caractere );
								break;
							case SDLK_m:
								NSAISIE_CARACTERE( 'm', 'M', majuscule, caractere );
								break;
							case SDLK_n:
								NSAISIE_CARACTERE( 'n', 'N', majuscule, caractere );
								break;
							case SDLK_o:
								NSAISIE_CARACTERE( 'o', 'O', majuscule, caractere );
								break;
							case SDLK_p:
								NSAISIE_CARACTERE( 'p', 'P', majuscule, caractere );
								break;
							case SDLK_q:
								NSAISIE_CARACTERE( 'q', 'Q', majuscule, caractere );
								break;
							case SDLK_r:
								NSAISIE_CARACTERE( 'r', 'R', majuscule, caractere );
								break;
							case SDLK_s:
								NSAISIE_CARACTERE( 's', 'S', majuscule, caractere );
								break;
							case SDLK_t:
								NSAISIE_CARACTERE( 't', 'T', majuscule, caractere );
								break;
							case SDLK_u:
								NSAISIE_CARACTERE( 'u', 'U', majuscule, caractere );
								break;
							case SDLK_v:
								NSAISIE_CARACTERE( 'v', 'V', majuscule, caractere );
								break;
							case SDLK_w:
								NSAISIE_CARACTERE( 'w', 'W', majuscule, caractere );
								break;
							case SDLK_x:
								NSAISIE_CARACTERE( 'x', 'X', majuscule, caractere );
								break;
							case SDLK_y:
								NSAISIE_CARACTERE( 'y', 'Y', majuscule, caractere );
								break;
							case SDLK_z:
								NSAISIE_CARACTERE( 'z', 'Z', majuscule, caractere );
								break;

							default:
								break;
						}
					}

					// Chiffres
					if( this->m_mode & NMODE_SAISIE_SDL_CHIFFRES )
					{
						switch( e->key.keysym.sym )
						{
							// Clavier
							case SDLK_0:
								NSAISIE_CARACTERE( '\0', '0', majuscule, caractere );
								break;
							case SDLK_1:
								NSAISIE_CARACTERE( '\0', '1', majuscule, caractere );
								break;
							case SDLK_2:
								NSAISIE_CARACTERE( '\0', '2', majuscule, caractere );
								break;
							case SDLK_3:
								NSAISIE_CARACTERE( '\0', '3', majuscule, caractere );
								break;
							case SDLK_4:
								NSAISIE_CARACTERE( '\0', '4', majuscule, caractere );
								break;
							case SDLK_5:
								NSAISIE_CARACTERE( '\0', '5', majuscule, caractere );
								break;
							case SDLK_6:
								NSAISIE_CARACTERE( '\0', '6', majuscule, caractere );
								break;
							case SDLK_7:
								NSAISIE_CARACTERE( '\0', '7', majuscule, caractere );
								break;
							case SDLK_8:
								NSAISIE_CARACTERE( '\0', '8', majuscule, caractere );
								break;
							case SDLK_9:
								NSAISIE_CARACTERE( '\0', '9', majuscule, caractere );
								break;

							// Keypad
							case SDLK_KP_0:
								NSAISIE_CARACTERE( '0', '0', majuscule, caractere );
								break;
							case SDLK_KP_1:
								NSAISIE_CARACTERE( '1', '1', majuscule, caractere );
								break;
							case SDLK_KP_2:
								NSAISIE_CARACTERE( '2', '2', majuscule, caractere );
								break;
							case SDLK_KP_3:
								NSAISIE_CARACTERE( '3', '3', majuscule, caractere );
								break;
							case SDLK_KP_4:
								NSAISIE_CARACTERE( '4', '4', majuscule, caractere );
								break;
							case SDLK_KP_5:
								NSAISIE_CARACTERE( '5', '5', majuscule, caractere );
								break;
							case SDLK_KP_6:
								NSAISIE_CARACTERE( '6', '6', majuscule, caractere );
								break;
							case SDLK_KP_7:
								NSAISIE_CARACTERE( '7', '7', majuscule, caractere );
								break;
							case SDLK_KP_8:
								NSAISIE_CARACTERE( '8', '8', majuscule, caractere );
								break;
							case SDLK_KP_9:
								NSAISIE_CARACTERE( '9', '9', majuscule, caractere );
								break;
							case SDLK_KP_PERIOD:
								NSAISIE_CARACTERE( '.', '.', majuscule, caractere );
								break;

							default:
								break;
						}
					}

					// Espaces
					if( this->m_mode & NMODE_SAISIE_SDL_ESPACES )
					{
						switch( e->key.keysym.sym )
						{
							case SDLK_SPACE:
								NSAISIE_CARACTERE( ' ', ' ', majuscule, caractere );
								break;
							case SDLK_TAB:
								if( !autoriseTab )
									NSAISIE_CARACTERE( '\t', '\t', majuscule, caractere );
								break;

							default:
								break;
						}
					}

					// Caracteres speciaux
					if( this->m_mode & NMODE_SAISIE_SDL_CARACTERES_SPECIAUX )
					{
						switch( e->key.keysym.sym )
						{
							case SDLK_0:
								NSAISIE_CARACTERE_VERIFICATION( (char)133, 64, altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_1:
								NSAISIE_CARACTERE_VERIFICATION( 38, '?', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_2:
								if( !majuscule
									|| altgr )
								{
									// Gerer les accents...
									if( altgr )
									{
										if( accent == NACCENT_SAISIE_SDL_TILDE )
										{
											// Changer caractere
											caractere = '~';

											// Annuler l'accent
											accent = NACCENT_SAISIE_SDL_AUCUN;
										}
										else
											accent = NACCENT_SAISIE_SDL_TILDE;
									}
									else
										caractere = (char)130;
								}
								break;
							case SDLK_3:
								NSAISIE_CARACTERE_VERIFICATION( '"', '#', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_4:
								NSAISIE_CARACTERE_VERIFICATION( '\'', '{', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_5:
								NSAISIE_CARACTERE_VERIFICATION( '(', '[', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_6:
								NSAISIE_CARACTERE_VERIFICATION( '-', '|', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_7:
								if( !majuscule
									|| altgr )
								{
									if( altgr )
									{
										if( accent == NACCENT_SAISIE_SDL_GRAVE )
										{
											// Changer caractere
											caractere = (char)96;

											// Annuler accent
											accent = NACCENT_SAISIE_SDL_AUCUN;
										}
										else
											accent = NACCENT_SAISIE_SDL_GRAVE;
									}
									else
										caractere = (char)138;
								}
								break;
							case SDLK_8:
								NSAISIE_CARACTERE_VERIFICATION( '_', '\\', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_9:
								NSAISIE_CARACTERE_VERIFICATION( (char)249, (char)94, altgr, caractere, ( !majuscule || altgr ) );
								break;

							case SDLK_e:
								NSAISIE_CARACTERE( '\0', 128, altgr, caractere );
								break;

							case SDLK_DOLLAR:
								NSAISIE_CARACTERE( NSAISIE_CARACTERE( (char)36, (char)156, altgr, caractere ), NSAISIE_CARACTERE( (char)36, (char)'?', altgr, caractere ), majuscule, caractere );
								break;
							case 249: // SDLK_PERCENT
								NSAISIE_CARACTERE( (char)151, (char)37, majuscule, caractere );
								break;
							case SDLK_CARET:
								if( !majuscule )
								{
									if( accent == NACCENT_SAISIE_SDL_CIRCONFLEXE )
									{
										// Changer caractere
										caractere = 94;

										// Annuler accent
										accent = NACCENT_SAISIE_SDL_AUCUN;
									}
									else
										accent = NACCENT_SAISIE_SDL_CIRCONFLEXE;
								}
								else
								{
									if( accent == NACCENT_SAISIE_SDL_TREMA )
									{
										// Changer caractere
										caractere = (char)249;

										// Annuler accent
										accent = NACCENT_SAISIE_SDL_AUCUN;
									}
									else
										accent = NACCENT_SAISIE_SDL_TREMA;
								}
								break;
							case SDLK_ASTERISK:
								NSAISIE_CARACTERE( '*', 230, majuscule, caractere );
								break;
							case SDLK_COMMA:
								NSAISIE_CARACTERE( ',', '?', majuscule, caractere );
								break;
							case SDLK_SEMICOLON:
								NSAISIE_CARACTERE( ';', '.', majuscule, caractere );
								break;
							case SDLK_COLON:
								NSAISIE_CARACTERE( ':', '/', majuscule, caractere );
								break;
							case SDLK_EXCLAIM:
								NSAISIE_CARACTERE( '!', 245, majuscule, caractere );
								break;
							case SDLK_LESS:
								NSAISIE_CARACTERE( '<', '>', majuscule, caractere );
								break;
							case SDLK_EQUALS:
								NSAISIE_CARACTERE( '=', '+', majuscule, caractere );
								break;
							case SDLK_RIGHTPAREN:
								NSAISIE_CARACTERE( NSAISIE_CARACTERE( 41, 93, altgr, caractere ), NSAISIE_CARACTERE( 248, 93, altgr, caractere ), majuscule, caractere );
								break;

							case SDLK_KP_PLUS:
								caractere = '+';
								break;
							case SDLK_KP_MINUS:
								caractere = '-';
								break;
							case SDLK_KP_MULTIPLY:
								caractere = '*';
								break;
							case SDLK_KP_DIVIDE:
								caractere = '/';
								break;

							default:
								break;
						}
					}

					// Gerer les accents
					switch( accent )
					{
						case NACCENT_SAISIE_SDL_AIGU:
							break;
						case NACCENT_SAISIE_SDL_GRAVE:
							switch( caractere )
							{
								case 'a':
									NSAISIE_CARACTERE_AVEC_ACCENT( 133, caractere, accent );
									break;
								case 'e':
									NSAISIE_CARACTERE_AVEC_ACCENT( 138, caractere, accent );
									break;
								case 'i':
									NSAISIE_CARACTERE_AVEC_ACCENT( 141, caractere, accent );
									break;
								case 'o':
									NSAISIE_CARACTERE_AVEC_ACCENT( 149, caractere, accent );
									break;
								case 'u':
									NSAISIE_CARACTERE_AVEC_ACCENT( 151, caractere, accent );
									break;

								case 'A':
									NSAISIE_CARACTERE_AVEC_ACCENT( 183, caractere, accent );
									break;
								case 'E':
									NSAISIE_CARACTERE_AVEC_ACCENT( 212, caractere, accent );
									break;
								case 'I':
									NSAISIE_CARACTERE_AVEC_ACCENT( 222, caractere, accent );
									break;
								case 'O':
									NSAISIE_CARACTERE_AVEC_ACCENT( 227, caractere, accent );
									break;
								case 'U':
									NSAISIE_CARACTERE_AVEC_ACCENT( 235, caractere, accent );
									break;

								default:
									break;
							}
							break;
						case NACCENT_SAISIE_SDL_CIRCONFLEXE:
							switch( caractere )
							{
								case 'a':
									NSAISIE_CARACTERE_AVEC_ACCENT( 131, caractere, accent );
									break;
								case 'e':
									NSAISIE_CARACTERE_AVEC_ACCENT( 136, caractere, accent );
									break;
								case 'i':
									NSAISIE_CARACTERE_AVEC_ACCENT( 140, caractere, accent );
									break;
								case 'o':
									NSAISIE_CARACTERE_AVEC_ACCENT( 147, caractere, accent );
									break;
								case 'u':
									NSAISIE_CARACTERE_AVEC_ACCENT( 150, caractere, accent );
									break;

								case 'A':
									NSAISIE_CARACTERE_AVEC_ACCENT( 182, caractere, accent );
									break;
								case 'E':
									NSAISIE_CARACTERE_AVEC_ACCENT( 210, caractere, accent );
									break;
								case 'I':
									NSAISIE_CARACTERE_AVEC_ACCENT( 215, caractere, accent );
									break;
								case 'O':
									NSAISIE_CARACTERE_AVEC_ACCENT( 226, caractere, accent );
									break;
								case 'U':
									NSAISIE_CARACTERE_AVEC_ACCENT( 234, caractere, accent );
									break;

								default:
									break;
							}
							break;
						case NACCENT_SAISIE_SDL_TREMA:
							switch( caractere )
							{
								case 'a':
									NSAISIE_CARACTERE_AVEC_ACCENT( 132, caractere, accent );
									break;
								case 'e':
									NSAISIE_CARACTERE_AVEC_ACCENT( 137, caractere, accent );
									break;
								case 'i':
									NSAISIE_CARACTERE_AVEC_ACCENT( 139, caractere, accent );
									break;
								case 'o':
									NSAISIE_CARACTERE_AVEC_ACCENT( 148, caractere, accent );
									break;
								case 'u':
									NSAISIE_CARACTERE_AVEC_ACCENT( 129, caractere, accent );
									break;
								case 'y':
									NSAISIE_CARACTERE_AVEC_ACCENT( 152, caractere, accent );
									break;

								case 'A':
									NSAISIE_CARACTERE_AVEC_ACCENT( 142, caractere, accent );
									break;
								case 'E':
									NSAISIE_CARACTERE_AVEC_ACCENT( 211, caractere, accent );
									break;
								case 'I':
									NSAISIE_CARACTERE_AVEC_ACCENT( 216, caractere, accent );
									break;
								case 'O':
									NSAISIE_CARACTERE_AVEC_ACCENT( 153, caractere, accent );
									break;
								case 'U':
									NSAISIE_CARACTERE_AVEC_ACCENT( 154, caractere, accent );
									break;

								default:
									break;
							}
							break;
						case NACCENT_SAISIE_SDL_TILDE:
							switch( caractere )
							{
								case 'a':
									NSAISIE_CARACTERE_AVEC_ACCENT( 198, caractere, accent );
									break;
								case 'n':
									NSAISIE_CARACTERE_AVEC_ACCENT( 164, caractere, accent );
									break;
								case 'o':
									NSAISIE_CARACTERE_AVEC_ACCENT( 228, caractere, accent );
									break;

								case 'A':
									NSAISIE_CARACTERE_AVEC_ACCENT( 199, caractere, accent );
									break;
								case 'N':
									NSAISIE_CARACTERE_AVEC_ACCENT( 165, caractere, accent );
									break;
								case 'O':
									NSAISIE_CARACTERE_AVEC_ACCENT( 229, caractere, accent );
									break;

								default:
									break;
							}
							break;

						default:
							break;
					} // switch( accent )

					// Verifier la pression de la touche d'arret
					if( e->key.keysym.sym == this->m_touche )
						// Plus en cours
						*this->m_estEnCours = NFALSE;

					// Verifier pression chap
					if( e->key.keysym.sym == SDLK_ESCAPE )
					{
						// Oublier
						NDISSOCIER_ADRESSE( this->m_estEnCours );

						// Echap
						return NRETOUR_SAISIE_SDL_ECHAP;
					}

					// Mettre a jour la chaine
					if( NLib_Module_SDL_Saisie_NSaisieSDL_MiseAJourInterne( this,
						caractere,
						&this->m_texte,
						this->m_texte ?
							strlen( this->m_texte )
							: 0,
						this->m_tailleMaximale ) )
						resultat = NTRUE;
					break;
			} // switch
		} // while

		// Callback chaine
		if( callbackChaine
			&& this->m_texte )
		{
			// Liberer ce qu'il y avait avant
			NFREE( *callbackChaine );

			// Allouer la memoire ncessaire
			if( !( *callbackChaine = (char*)calloc( strlen( this->m_texte ) + 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Retenter...
				continue;
			}

			// Copier
			memcpy( *callbackChaine,
				this->m_texte,
				strlen( this->m_texte ) * sizeof( char ) );
		}
		else if( !this->m_texte )
			NFREE( *callbackChaine );

		// Appels callback
			// Gestion
				if( this->m_callbackGestion )
					this->m_callbackGestion( this->m_argumentCallbackGestion );
			// Actualisation
				if( this->m_callbackActualisation )
					this->m_callbackActualisation( this->m_argumentCallbackActualisation );
			// Mise a jour chaine!
				if( this->m_callbackUpdateChaine )
					if( resultat )
						this->m_callbackUpdateChaine( this->m_argumentCallbackUpdateChaine );
	} while( *this->m_estEnCours );

	// Oublier continuer
	NDISSOCIER_ADRESSE( this->m_estEnCours );

	// OK
	return NRETOUR_SAISIE_SDL_OK;
}

/* Definir */
// Cadres
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_DefinirCadres( NSaisieSDL *this,
	const NCadre **cadres,
	NU32 nb )
{
	// Itrateurs
	NU32 i, j;

	// Verifier
		// Paramtres
			if( !cadres
				|| !nb )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

				// Quitter
				return NFALSE;
			}

	// Supprimer les anciens cadres
	NLib_Module_SDL_Saisie_NSaisieSDL_SupprimerCadresInterne( this );

	// Sauvegarder nombre cadre(s)
	this->m_nbCadres = nb;

	// Allouer la memoire
	if( !( this->m_cadre = (NCadre**)calloc( this->m_nbCadres,
		sizeof( NCadre* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Crer les cadres
	for( i = 0; i < this->m_nbCadres; i++ )
		if( !( this->m_cadre[ i ] = NLib_Module_SDL_NCadre_Construire2( cadres[ i ] ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
				// 2D
					for( j = 0; j < i; j++ )
						NLib_Module_SDL_NCadre_Detruire( &this->m_cadre[ j ] );
				// 1D
					NFREE( this->m_cadre );

			// Quitter
			return NFALSE;
		}

	// OK
	return NTRUE;
}

// Touche d'arrt
void NLib_Module_SDL_Saisie_NSaisieSDL_DefinirToucheArret( NSaisieSDL *this,
	SDL_Keycode touche )
{
	this->m_touche = touche;
}

// Curseur
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_DefinirCurseur( NSaisieSDL *this,
	NU32 curseur )
{
	// Verifier
		// Paramtre
			if( ( curseur >= this->m_tailleMaximale
					&& this->m_tailleMaximale )
				|| curseur >= strlen( this->m_texte ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

				// Quitter
				return NFALSE;
			}

	// Enregistrer
	this->m_curseur = curseur;

	// OK
	return NTRUE;
}

/* Definir texte */
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_DefinirTexte( NSaisieSDL *this,
	const char *texte )
{
	// Taille texte
	NU32 tailleTexte;

	// Verifier
	if( !texte )
		return NFALSE;

	// Calculer taille texte
	tailleTexte = strlen( texte );

	if( this->m_tailleMaximale != 0 )
		memcpy( this->m_texte,
			texte,
			( tailleTexte >= this->m_tailleMaximale ) ?
			( ( this->m_tailleMaximale - 1 ) >= 0 ?
				this->m_tailleMaximale - 1
				: 0 )
			: tailleTexte );
	else
	{
		// Liberer ancien texte
		NFREE( this->m_texte );

		// Allouer de quoi stocker le texte
		if( !( this->m_texte = calloc( tailleTexte + 1,
				sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Quitter
			return NFALSE;
		}

		// Copier
		memcpy( this->m_texte,
			texte,
			tailleTexte );
	}

	// Placer le curseur
	this->m_curseur = ( this->m_texte != NULL )
		? tailleTexte
		: 0;

	// OK
	return NTRUE;
}

/* Obtenir texte */
const char *NLib_Module_SDL_NSaisieSDL_ObtenirTexte( const NSaisieSDL *this )
{
	return this->m_texte;
}

/* Obtenir cadre cliqu */
NU32 NLib_Module_SDL_Saisie_NSaisieSDL_ObtenirCadreClique( const NSaisieSDL *this )
{
	return this->m_idCadreClique;
}

/* Obtenir texte */
const char *NLib_Module_SDL_Saisie_NSaisieSDL_ObtenirTexte( const NSaisieSDL *this )
{
	return this->m_texte;
}

/* Est en cours? */
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_EstEnCours( const NSaisieSDL *this )
{
	return ( this->m_estEnCours != NULL ) ?
		*this->m_estEnCours
		: NFALSE;
}

#endif // NLIB_MODULE_SDL

