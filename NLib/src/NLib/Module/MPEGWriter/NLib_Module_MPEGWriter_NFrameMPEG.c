#include "../../../../include/NLib/NLib.h"

// -------------------------------------------
// struct NLib::Module::MPEGWriter::NFrameMPEG
// -------------------------------------------

#ifdef NLIB_MODULE_MPEGWRITER

/**
 * Construire la frame
 *
 * @param data
 *		Les donnees
 * @param taille
 *		La taille
 *
 * @return l'instance de la frame
 */
__ALLOC NFrameMPEG *NLib_Module_MPEGWriter_NFrameMPEG_Construire( __WILLBEOWNED char *data,
	NU32 taille )
{
	// Sortie
	__OUTPUT NFrameMPEG *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NFrameMPEG ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( data );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_data = data;
	out->m_taille = taille;

	// OK
	return out;
}

/**
 * Construire la frame a partir d'un fichier mpeg
 *
 * @param fichier
 *		Le fichier source
 * @param taille
 *		La taille de la frame a lire
 *
 * @return l'instance de la frame
 */
__ALLOC NFrameMPEG *NLib_Module_MPEGWriter_NFrameMPEG_Construire2( FILE *fichier,
	NU64 taille )
{
	// Sortie
	__OUTPUT NFrameMPEG *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NFrameMPEG ) ) )
		|| !( out->m_data = calloc( taille,
			sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

    // Se placer a la position
    fseek( fichier,
		(NS64)taille * (NS32)~0,
		SEEK_END );

	// Lire la frame
	fread( out->m_data,
		sizeof( char ),
		taille,
		fichier );

	// Enregistrer
	out->m_taille = taille;

	// OK
	return out;
}

/**
 * Detruire la frame
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_MPEGWriter_NFrameMPEG_Detruire( NFrameMPEG **this )
{
	// Liberer
	NFREE( (*this)->m_data );
	NFREE( (*this) );
}

/**
 * Obtenir les donnees
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees
 */
const char *NLib_Module_MPEGWriter_NFrameMPEG_ObtenirData( const NFrameMPEG *this )
{
	return this->m_data;
}

/**
 * Obtenir la taille
 *
 * @param this
 *		Cette instance
 *
 * @return la taille
 */
NU64 NLib_Module_MPEGWriter_NFrameMPEG_ObtenirTaille( const NFrameMPEG *this )
{
	return this->m_taille;
}

#endif // NLIB_MODULE_MPEGWRITER

