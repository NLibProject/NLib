#include "../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_MPEGWRITER

// ----------------------------------
// namespace NLib::Module::MPEGWriter
// ----------------------------------

// Huffman tables
static const unsigned char s_jo_HTDC_Y[9][2] = {{4,3}, {0,2}, {1,2}, {5,3}, {6,3}, {14,4}, {30,5}, {62,6}, {126,7}};
static const unsigned char s_jo_HTDC_C[9][2] = {{0,2}, {1,2}, {2,2}, {6,3}, {14,4}, {30,5}, {62,6}, {126,7}, {254,8}};
static const unsigned char s_jo_HTAC[32][40][2] = {
{{6,3},{8,5},{10,6},{12,8},{76,9},{66,9},{20,11},{58,13},{48,13},{38,13},{32,13},{52,14},{50,14},{48,14},{46,14},{62,15},{62,15},{58,15},{56,15},{54,15},{52,15},{50,15},{48,15},{46,15},{44,15},{42,15},{40,15},{38,15},{36,15},{34,15},{32,15},{48,16},{46,16},{44,16},{42,16},{40,16},{38,16},{36,16},{34,16},{32,16},},
{{6,4},{12,7},{74,9},{24,11},{54,13},{44,14},{42,14},{62,16},{60,16},{58,16},{56,16},{54,16},{52,16},{50,16},{38,17},{36,17},{34,17},{32,17}},
{{10,5},{8,8},{22,11},{40,13},{40,14}},
{{14,6},{72,9},{56,13},{38,14}},
{{12,6},{30,11},{36,13}},  {{14,7},{18,11},{36,14}},  {{10,7},{60,13},{40,17}},
{{8,7},{42,13}},  {{14,8},{34,13}},  {{10,8},{34,14}},  {{78,9},{32,14}},  {{70,9},{52,17}},  {{68,9},{50,17}},  {{64,9},{48,17}},  {{28,11},{46,17}},  {{26,11},{44,17}},  {{16,11},{42,17}},
{{62,13}}, {{52,13}}, {{50,13}}, {{46,13}}, {{44,13}}, {{62,14}}, {{60,14}}, {{58,14}}, {{56,14}}, {{54,14}}, {{62,17}}, {{60,17}}, {{58,17}}, {{56,17}}, {{54,17}},
};
static const float s_jo_quantTbl[64] = {
	0.015625f,0.005632f,0.005035f,0.004832f,0.004808f,0.005892f,0.007964f,0.013325f,
	0.005632f,0.004061f,0.003135f,0.003193f,0.003338f,0.003955f,0.004898f,0.008828f,
	0.005035f,0.003135f,0.002816f,0.003013f,0.003299f,0.003581f,0.005199f,0.009125f,
	0.004832f,0.003484f,0.003129f,0.003348f,0.003666f,0.003979f,0.005309f,0.009632f,
	0.005682f,0.003466f,0.003543f,0.003666f,0.003906f,0.004546f,0.005774f,0.009439f,
	0.006119f,0.004248f,0.004199f,0.004228f,0.004546f,0.005062f,0.006124f,0.009942f,
	0.008883f,0.006167f,0.006096f,0.005777f,0.006078f,0.006391f,0.007621f,0.012133f,
	0.016780f,0.011263f,0.009907f,0.010139f,0.009849f,0.010297f,0.012133f,0.019785f,
};
static const unsigned char s_jo_ZigZag[] = { 0,1,5,6,14,15,27,28,2,4,7,13,16,26,29,42,3,8,12,17,25,30,41,43,9,11,18,24,31,40,44,53,10,19,23,32,39,45,52,54,20,22,33,38,46,51,55,60,21,34,37,47,50,56,59,61,35,36,48,49,57,58,62,63 };

typedef struct {
	FILE *fp;
	int buf, cnt;
} jo_bits_t;

typedef struct {
    char *out;
    int buf, cnt;
    NU32 taille;
} n_bits_t;

static void jo_writeBits(jo_bits_t *b, int value, int count) {
	NU8 c;
	b->cnt += count;
	b->buf |= value << (24 - b->cnt);
	while(b->cnt >= 8) {
		c = (b->buf >> 16) & 255;
		putc(c, b->fp);
		b->buf <<= 8;
		b->cnt -= 8;
	}
}

static void n_writeBits(n_bits_t *b, int value, int count) {
	NU8 c;
	b->cnt += count;
	b->buf |= value << (24 - b->cnt);
	while(b->cnt >= 8) {
		c = (b->buf >> 16) & 255;
		NLib_Memoire_AjouterData( &b->out,
			b->taille++,
			(char*)&c,
			sizeof( NU8 ) );
		b->buf <<= 8;
		b->cnt -= 8;
	}
}

static void jo_DCT(float *d0, float *d1, float *d2, float *d3, float *d4, float *d5, float *d6, float *d7)
{
	float z1;
	float z5;
	float z2;
	float z4;
	float z3;
	float z11;
	float z13;

	float tmp0 = *d0 + *d7;
	float tmp7 = *d0 - *d7;
	float tmp1 = *d1 + *d6;
	float tmp6 = *d1 - *d6;
	float tmp2 = *d2 + *d5;
	float tmp5 = *d2 - *d5;
	float tmp3 = *d3 + *d4;
	float tmp4 = *d3 - *d4;

	// Even part
	float tmp10 = tmp0 + tmp3;	// phase 2
	float tmp13 = tmp0 - tmp3;
	float tmp11 = tmp1 + tmp2;
	float tmp12 = tmp1 - tmp2;

	*d0 = tmp10 + tmp11; 		// phase 3
	*d4 = tmp10 - tmp11;

	z1 = (tmp12 + tmp13) * 0.707106781f; // c4
	*d2 = tmp13 + z1; 		// phase 5
	*d6 = tmp13 - z1;

	// Odd part
	tmp10 = tmp4 + tmp5; 		// phase 2
	tmp11 = tmp5 + tmp6;
	tmp12 = tmp6 + tmp7;

	// The rotator is modified from fig 4-8 to avoid extra negations.
	z5 = (tmp10 - tmp12) * 0.382683433f; // c6
	z2 = tmp10 * 0.541196100f + z5; // c2-c6
	z4 = tmp12 * 1.306562965f + z5; // c2+c6
	z3 = tmp11 * 0.707106781f; // c4

	z11 = tmp7 + z3;		// phase 5
	z13 = tmp7 - z3;

	*d5 = z13 + z2;			// phase 6
	*d3 = z13 - z2;
	*d1 = z11 + z4;
	*d7 = z11 - z4;
}

static int jo_processDU(jo_bits_t *bits, float A[64], const unsigned char htdc[9][2], int DC) {
	NS32 Q[64];
	float v;
	NS32 aDC;
	NS32 size;
	NS32 tempval;
	NS32 dataOff;
	NS32 i;
	NS32 endpos;
	NS32 run = 0;
	NS32 AC;
	NS32 aAC;
	NS32 code = 0;

	for(dataOff=0; dataOff<64; dataOff+=8) {
		jo_DCT(&A[dataOff], &A[dataOff+1], &A[dataOff+2], &A[dataOff+3], &A[dataOff+4], &A[dataOff+5], &A[dataOff+6], &A[dataOff+7]);
	}
	for(dataOff=0; dataOff<8; ++dataOff) {
		jo_DCT(&A[dataOff], &A[dataOff+8], &A[dataOff+16], &A[dataOff+24], &A[dataOff+32], &A[dataOff+40], &A[dataOff+48], &A[dataOff+56]);
	}
	for(i=0; i<64; ++i) {
		v = A[i]*s_jo_quantTbl[i];
		Q[s_jo_ZigZag[i]] = (int)(v < 0 ? ceilf(v - 0.5f) : floorf(v + 0.5f));
	}

	DC = Q[0] - DC;
	aDC = DC < 0 ? -DC : DC;
	size = 0;
	tempval = aDC;
	while(tempval) {
		size++;
		tempval >>= 1;
	}
	jo_writeBits(bits, htdc[size][0], htdc[size][1]);
	if(DC < 0) aDC ^= (1 << size) - 1;
	jo_writeBits(bits, aDC, size);

	endpos = 63;
	for(; (endpos>0)&&(Q[endpos]==0); --endpos) { /* do nothing */ }
	for(i = 1; i <= endpos;) {
		run = 0;
		while (Q[i]==0 && i<endpos) {
			++run;
			++i;
		}
		AC = Q[i++];
		aAC = AC < 0 ? -AC : AC;
		code = 0, size = 0;
		if (run<32 && aAC<=40) {
			code = s_jo_HTAC[run][aAC-1][0];
			size = s_jo_HTAC[run][aAC-1][1];
			if (AC < 0) code += 1;
		}
		if(!size) {
			jo_writeBits(bits, 1, 6);
			jo_writeBits(bits, run, 6);
			if (AC < -127) {
				jo_writeBits(bits, 128, 8);
			} else if(AC > 127) {
				jo_writeBits(bits, 0, 8);
			}
			code = AC&255;
			size = 8;
		}
		jo_writeBits(bits, code, size);
	}
	jo_writeBits(bits, 2, 2);

	return Q[0];
}

static int n_processDU(n_bits_t *bits, float A[64], const unsigned char htdc[9][2], int DC) {
	NS32 Q[64];
	float v;
	NS32 aDC;
	NS32 size;
	NS32 tempval;
	NS32 dataOff;
	NS32 i;
	NS32 endpos;
	NS32 run = 0;
	NS32 AC;
	NS32 aAC;
	NS32 code = 0;

	for(dataOff=0; dataOff<64; dataOff+=8) {
		jo_DCT(&A[dataOff], &A[dataOff+1], &A[dataOff+2], &A[dataOff+3], &A[dataOff+4], &A[dataOff+5], &A[dataOff+6], &A[dataOff+7]);
	}
	for(dataOff=0; dataOff<8; ++dataOff) {
		jo_DCT(&A[dataOff], &A[dataOff+8], &A[dataOff+16], &A[dataOff+24], &A[dataOff+32], &A[dataOff+40], &A[dataOff+48], &A[dataOff+56]);
	}
	for(i=0; i<64; ++i) {
		v = A[i]*s_jo_quantTbl[i];
		Q[s_jo_ZigZag[i]] = (int)(v < 0 ? ceilf(v - 0.5f) : floorf(v + 0.5f));
	}

	DC = Q[0] - DC;
	aDC = DC < 0 ? -DC : DC;
	size = 0;
	tempval = aDC;
	while(tempval) {
		size++;
		tempval >>= 1;
	}
	n_writeBits(bits, htdc[size][0], htdc[size][1]);
	if(DC < 0) aDC ^= (1 << size) - 1;
	n_writeBits(bits, aDC, size);

	endpos = 63;
	for(; (endpos>0)&&(Q[endpos]==0); --endpos) { /* do nothing */ }
	for(i = 1; i <= endpos;) {
		run = 0;
		while (Q[i]==0 && i<endpos) {
			++run;
			++i;
		}
		AC = Q[i++];
		aAC = AC < 0 ? -AC : AC;
		code = 0, size = 0;
		if (run<32 && aAC<=40) {
			code = s_jo_HTAC[run][aAC-1][0];
			size = s_jo_HTAC[run][aAC-1][1];
			if (AC < 0) code += 1;
		}
		if(!size) {
			n_writeBits(bits, 1, 6);
			n_writeBits(bits, run, 6);
			if (AC < -127) {
				n_writeBits(bits, 128, 8);
			} else if(AC > 127) {
				n_writeBits(bits, 0, 8);
			}
			code = AC&255;
			size = 8;
		}
		n_writeBits(bits, code, size);
	}
	n_writeBits(bits, 2, 2);

	return Q[0];
}

/**
 * Inscrire une frame
 *
 * @param fp
 * 		Le descripteur de fichier (wb)
 * @param rgb
 * 		La frame a inscrire
 * @param width
 * 		La taille horizontale de la frame
 * @param height
 * 		La taille verticale de la frame
 * @param fps
 * 		Le nombre de frame par seconde
 */
void NLib_Module_MPEGWriter_EcrireFrame( FILE *fp,
	const NU8 *rgb,
	NU32 width,
	NU32 height,
	NU32 fps )
{
	NS32 lastDCY = 128,
		lastDCCR = 128,
		lastDCCB = 128;
	NS32 vblock,
		hblock;
	float Y[256], CBx[256], CRx[256];
	const unsigned char *c;
	float r, g, b;
	NS32 i,
		j;
	NS32 k1,
		k2;
	float block[64];
	NS32 x,
		y;
	float CB[64], CR[64];

	jo_bits_t bits = { fp };

	// Sequence Header
	fwrite("\x00\x00\x01\xB3", 4, 1, fp);
	// 12 bits for width, height
	putc((width>>4)&0xFF, fp);
	putc(((width&0xF)<<4) | ((height>>8) & 0xF), fp);
	putc(height & 0xFF, fp);
	// aspect ratio, framerate
	if(fps <= 24) putc(0x12, fp);
	else if(fps <= 25) putc(0x13, fp);
	else if(fps <= 30) putc(0x15, fp);
	else if(fps <= 50) putc(0x16, fp);
	else putc(0x18, fp); // 60fps
	fwrite("\xFF\xFF\xE0\xA0", 4, 1, fp);

	fwrite("\x00\x00\x01\xB8\x80\x08\x00\x40", 8, 1, fp); // GOP header
	fwrite("\x00\x00\x01\x00\x00\x0C\x00\x00", 8, 1, fp); // PIC header
	fwrite("\x00\x00\x01\x01", 4, 1, fp); // Slice header
	jo_writeBits(&bits, 0x10, 6);

	for (vblock=0; vblock<(height+15)/16; vblock++) {
		for (hblock=0; hblock<(width+15)/16; hblock++) {
			jo_writeBits(&bits, 3, 2);

			for (i=0; i<256; ++i) {
				y = vblock*16+(i/16);
				x = hblock*16+(i&15);
				x = x >= width ? width-1 : x;
				y = y >= height ? height-1 : y;
				c = rgb + y*width*3+x*3;
				r = c[0];
				g = c[1];
				b = c[2];
                Y[i] = (0.299f*r + 0.587f*g + 0.114f*b) * (219.f/255) + 16;
                CBx[i] = (-0.299f*r - 0.587f*g + 0.886f*b) * (224.f/255) + 128;
                CRx[i] = (0.701f*r - 0.587f*g - 0.114f*b) * (224.f/255) + 128;
			}

			// Downsample Cb,Cr (420 format)
			for (i=0; i<64; ++i) {
				j =(i&7)*2 + (i&56)*4;
				CB[i] = (CBx[j] + CBx[j+1] + CBx[j+16] + CBx[j+17]) * 0.25f;
				CR[i] = (CRx[j] + CRx[j+1] + CRx[j+16] + CRx[j+17]) * 0.25f;
			}

			for (k1=0; k1<2; ++k1) {
				for (k2=0; k2<2; ++k2) {
					for (i=0; i<64; i+=8) {
						j = (i&7)+(i&56)*2 + k1*8*16 + k2*8;
						memcpy(block+i, Y+j, 8*sizeof(Y[0]));
					}
					lastDCY = jo_processDU(&bits, block, s_jo_HTDC_Y, lastDCY);
				}
			}
			lastDCCB = jo_processDU(&bits, CB, s_jo_HTDC_C, lastDCCB);
			lastDCCR = jo_processDU(&bits, CR, s_jo_HTDC_C, lastDCCR);
		}
	}
	jo_writeBits(&bits, 0, 7);
	fwrite("\x00\x00\x01\xb7", 4, 1, fp); // End of Sequence
}

/**
 * Creer une frame purement en memoire
 *
 * @param rgb
 * 		La frame a inscrire
 * @param width
 * 		La taille horizontale de la frame
 * @param height
 * 		La taille verticale de la frame
 * @param fps
 * 		Le nombre de frame par seconde
 *
 * @return la frame creee
 */
__ALLOC NFrameMPEG *NLib_Module_MPEGWriter_CreerFrame( const NU8 *rgb,
	NU32 width,
	NU32 height,
	NU32 fps )
{
	NS32 lastDCY = 128,
		lastDCCR = 128,
		lastDCCB = 128;
	NS32 vblock,
		hblock;
	float Y[256], CBx[256], CRx[256];
	const unsigned char *c;
	float r, g, b;
	NS32 i,
		j;
	NS32 k1,
		k2;
	float block[64];
	NS32 x,
		y;
	float CB[64], CR[64];

	NU8 tmp;

	n_bits_t bits;

	// Sortie
	__OUTPUT NFrameMPEG *frame;

	// Zero
	memset( &bits,
		0,
		sizeof( n_bits_t ) );

	// Sequence Header
	NLib_Memoire_AjouterData( &bits.out,
		bits.taille,
		"\x00\x00\x01\xB3",
		4 );
	bits.taille += 4;

	// 12 bits for width, height
	tmp = (width>>4)&0xFF;
	NLib_Memoire_AjouterData( &bits.out,
		bits.taille,
		(char*)&tmp,
		sizeof( NU8 ) );
	bits.taille++;
	tmp = ((width&0xF)<<4) | ((height>>8) & 0xF);
	NLib_Memoire_AjouterData( &bits.out,
		bits.taille,
		(char*)&tmp,
		sizeof( NU8 ) );
	bits.taille++;
	tmp = height & 0xFF;
	NLib_Memoire_AjouterData( &bits.out,
		bits.taille,
		(char*)&tmp,
		sizeof( NU8 ) );
	bits.taille++;

	// aspect ratio, framerate
	if(fps <= 24) tmp = 0x12;
	else if(fps <= 25) tmp = 0x13;
	else if(fps <= 30) tmp = 0x15;
	else if(fps <= 50) tmp = 0x16;
	else tmp = 0x18; // 60fps

	NLib_Memoire_AjouterData( &bits.out,
		bits.taille,
		(char*)&tmp,
		sizeof( NU8 ) );
	bits.taille++;

	NLib_Memoire_AjouterData( &bits.out,
		bits.taille,
		"\xFF\xFF\xE0\xA0",
		4 );
	bits.taille += 4;

	NLib_Memoire_AjouterData( &bits.out,
		bits.taille,
		"\x00\x00\x01\xB8\x80\x08\x00\x40",
		8 );
	bits.taille += 8;
	NLib_Memoire_AjouterData( &bits.out,
		bits.taille,
		"\x00\x00\x01\x00\x00\x0C\x00\x00",
		8 );
	bits.taille += 8;
	NLib_Memoire_AjouterData( &bits.out,
		bits.taille,
		"\x00\x00\x01\x01",
		4 );
	bits.taille += 4;

	n_writeBits(&bits, 0x10, 6);

	for (vblock=0; vblock<(height+15)/16; vblock++) {
		for (hblock=0; hblock<(width+15)/16; hblock++) {
			n_writeBits(&bits, 3, 2);

			for (i=0; i<256; ++i) {
				y = vblock*16+(i/16);
				x = hblock*16+(i&15);
				x = x >= width ? width-1 : x;
				y = y >= height ? height-1 : y;
				c = rgb + y*width*3+x*3;
				r = c[0];
				g = c[1];
				b = c[2];
                Y[i] = (0.299f*r + 0.587f*g + 0.114f*b) * (219.f/255) + 16;
                CBx[i] = (-0.299f*r - 0.587f*g + 0.886f*b) * (224.f/255) + 128;
                CRx[i] = (0.701f*r - 0.587f*g - 0.114f*b) * (224.f/255) + 128;
			}

			// Downsample Cb,Cr (420 format)
			for (i=0; i<64; ++i) {
				j =(i&7)*2 + (i&56)*4;
				CB[i] = (CBx[j] + CBx[j+1] + CBx[j+16] + CBx[j+17]) * 0.25f;
				CR[i] = (CRx[j] + CRx[j+1] + CRx[j+16] + CRx[j+17]) * 0.25f;
			}

			for (k1=0; k1<2; ++k1) {
				for (k2=0; k2<2; ++k2) {
					for (i=0; i<64; i+=8) {
						j = (i&7)+(i&56)*2 + k1*8*16 + k2*8;
						memcpy(block+i, Y+j, 8*sizeof(Y[0]));
					}
					lastDCY = n_processDU(&bits, block, s_jo_HTDC_Y, lastDCY);
				}
			}
			lastDCCB = n_processDU(&bits, CB, s_jo_HTDC_C, lastDCCB);
			lastDCCR = n_processDU(&bits, CR, s_jo_HTDC_C, lastDCCR);
		}
	}
	n_writeBits(&bits, 0, 7);

	NLib_Memoire_AjouterData( &bits.out,
		bits.taille,
		"\x00\x00\x01\xb7",
		4 );
	bits.taille += 4;

	// Construire la frame
	if( !( frame = NLib_Module_MPEGWriter_NFrameMPEG_Construire( bits.out,
		bits.taille ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( bits.out );

		// Quitter
		return NULL;
	}

	// OK
	return frame;
}

/**
 * Creer une frame en memoire a l'aide d'un buffer
 *
 * @param rgb
 * 		La frame a inscrire
 * @param width
 * 		La taille horizontale de la frame
 * @param height
 * 		La taille verticale de la frame
 * @param fps
 * 		Le nombre de frame par seconde
 *
 * @return la frame creee
 */
__ALLOC NFrameMPEG *NLib_Module_MPEGWriter_CreerFrame2( const NU8 *rgb,
	NU32 width,
	NU32 height,
	NU32 fps )
{
	// Sortie
	__OUTPUT NFrameMPEG *frame;

	// Fichier
	FILE *fichier;

	// Donnee
	char *data;

	// Nom fichier
	char nomFichier[ 16 ] = { '.', 0, };

	// Taille fichier
	NU32 tailleFichier;

	// Iterateur
	NU32 i;

	// Ouvrir un fichier buffer aleatoire
	do
	{
		// Generer un nom aleatoire
		for( i = 1; i < 15; i++ )
			nomFichier[ i ] = 'A' + rand( )%( 'Z' - 'A' );

		// Verifier si le fichier existe
		if( NLib_Fichier_Operation_EstExiste( nomFichier ) )
			continue;

		// Ouvrir
		fichier = fopen( nomFichier,
			"wb+" );
	} while( !fichier );

	// Creer la frame dans le fichier
	NLib_Module_MPEGWriter_EcrireFrame( fichier,
		rgb,
		width,
		height,
		fps );

	// Obtenir la taille
	tailleFichier = NLib_Fichier_Operation_ObtenirTaille( fichier );

	// Allouer la memoire
	if( !( data = calloc( tailleFichier,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Fermer fichier
		fclose( fichier );

		// Detruire le fichier
		NLib_Fichier_Operation_SupprimerFichier( nomFichier );

		// Quitter
		return NULL;
	}

	// Se placer au debut
	fseek( fichier,
		SEEK_SET,
		0 );

	// Copier
	fread( data,
		sizeof( char ),
		tailleFichier,
		fichier );

	// Fermer le fichier
	fclose( fichier );

	// Detruire le fichier
	NLib_Fichier_Operation_SupprimerFichier( nomFichier );

	// Creer sortie
	if( !( frame = NLib_Module_MPEGWriter_NFrameMPEG_Construire( data,
		tailleFichier ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// OK
	return frame;
}

/**
 * Generer rgbx depuis rgb
 *
 * @param rgb
 *		Le tableau de pixels au format RGB
 * @param x
 *		La taille x
 * @param y
 *		La taille y
 *
 * @return le tableau de pixels au format RGBX
 */
__ALLOC NU8 *NLib_Module_MPEGWriter_ConvertirRGBVersRGBX( const NU8 *rgb,
	NU32 x,
	NU32 y )
{
	// Sortie
	__OUTPUT NU8 *rgbx;

	// Iterateur
	NU32 i,
		curseur = 0;

	// Allouer la memoire
	if( !( rgbx = calloc( x * y * 4,
		sizeof( NU8 ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	for( i = 0; i < x * y * 3; i += 3 )
	{
		// Copier
		memcpy( rgbx + curseur,
			rgb + i,
			3 * sizeof( char ) );

		// Avancer curseur
		curseur += 4;
	}

	// OK
	return rgbx;
}

#endif // NLIB_MODULE_MPEGWRITER

