#include "../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_MPEGWRITER

// --------------------------------------------
// struct NLib::Module::MPEGWriter::NMPEGWriter
// --------------------------------------------

/**
 * Construire le writer
 *
 * @param nom
 *		Le nom du fichier dans lequel ecrire
 * @param estEcraserContenu
 *		On ecrase le contenu precedent du fichier?
 *
 * @return l'instance du writer
 */
__ALLOC NMPEGWriter *NLib_Module_MPEGWriter_NMPEGWriter_Construire( const char *nom,
	NBOOL estEcraserContenu )
{
	// Sortie
	__OUTPUT NMPEGWriter *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NMPEGWriter ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Vider le contenu?
	if( estEcraserContenu )
		// Vider le contenu
		if( ( out->m_fichier = fopen( nom,
			"wb+" ) ) != NULL )
			fclose( out->m_fichier );

	// Ouvrir le fichier
	if( !( out->m_fichier = fopen( nom,
		"ab+" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out->m_nom = calloc( strlen( nom ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Fermer fichier
		fclose( out->m_fichier );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out->m_nom,
		nom,
		strlen( nom ) );

	// OK
	return out;
}

/**
 * Detruire
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_MPEGWriter_NMPEGWriter_Detruire( NMPEGWriter **this )
{
	// Fermer le fichier
	fclose( (*this)->m_fichier );

	// Liberer
	NFREE( (*this)->m_nom );
	NFREE( *this );
}

/**
 * Enregistrer position precedent la lecture
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_MPEGWriter_NMPEGWriter_EnregistrerPositionPrecedente( NMPEGWriter *this )
{
	// Se placer a la fin du fichier pour la lecture
	fseek( this->m_fichier,
			0,
			SEEK_END );

	// Enregistrer derniere position
	this->m_dernierePositionAvantEcriture = ftell( this->m_fichier );
}

/**
 * Inscrire une frame
 *
 * @param this
 *		Cette instance
 * @param rgb
 * 		La frame a inscrire
 * @param x
 * 		La taille horizontale de la frame
 * @param y
 * 		La taille verticale de la frame
 * @param fps
 * 		Le nombre de frame par seconde
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_MPEGWriter_NMPEGWriter_Inscrire( NMPEGWriter *this,
	const NU8 *rgb,
	NU32 x,
	NU32 y,
	NU32 fps )
{
	// Enregistrer position precedent la lecture
	NLib_Module_MPEGWriter_NMPEGWriter_EnregistrerPositionPrecedente( this );

	// Inscrire
	NLib_Module_MPEGWriter_EcrireFrame( this->m_fichier,
		rgb,
		x,
		y,
		fps );

	// Se placer a la fin du fichier pour la lecture
    fseek( this->m_fichier,
		0,
		SEEK_END );

	// OK
	return NTRUE;
}

/**
 * Inscrire une frame
 *
 * @param this
 *		Cette instance
 * @param frame
 *		La frame a inscrire
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_MPEGWriter_NMPEGWriter_Inscrire2( NMPEGWriter *this,
	const NFrameMPEG *frame )
{
	// Resultat
	__OUTPUT NBOOL resultat;

	// Enregistrer derniere position
	NLib_Module_MPEGWriter_NMPEGWriter_EnregistrerPositionPrecedente( this );

	// Inscrire
	resultat = fwrite( frame->m_data,
		sizeof( char ),
		frame->m_taille,
		this->m_fichier ) == frame->m_taille;

	// Se placer a la fin du fichier pour la lecture
    fseek( this->m_fichier,
		0,
		SEEK_END );

	// OK
	return resultat;
}

/**
 * Obtenir la taille du fichier (octets)
 *
 * @param this
 *		Cette instance
 *
 * @return la taille du fichier de capture
 */
NU64 NLib_Module_MPEGWriter_NMPEGWriter_ObtenirTaille( const NMPEGWriter *this )
{
	return ftell( this->m_fichier );
}

/**
 * Obtenir derniere frame inscrite
 *
 * @param this
 *		Cette instance
 *
 * @return la derniere frame inscrite
 */
__ALLOC NFrameMPEG *NLib_Module_MPEGWriter_NMPEGWriter_ObtenirDerniereFrame( const NMPEGWriter *this )
{
	// Taille frame
	NU64 tailleFrame;

	// Calculer la taille
	if( (NS64)( tailleFrame = ( ftell( this->m_fichier ) - this->m_dernierePositionAvantEcriture ) ) <= 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INCORRECT_SIZE );

		// Quitter
		return NULL;
	}

	// Lire frame
	return NLib_Module_MPEGWriter_NFrameMPEG_Construire2( this->m_fichier,
		tailleFrame );
}

/**
 * Obtenir instance fichier
 *
 * @param this
 *		Cette instance
 *
 * @return le fichier
 */
FILE *NLib_Module_MPEGWriter_NMPEGWriter_ObtenirFichier( const NMPEGWriter *this )
{
    return this->m_fichier;
}

/**
 * Vider le contenu du fichier
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_MPEGWriter_NMPEGWriter_ViderContenu( NMPEGWriter *this )
{
	// Fermer le fichier
	fclose( this->m_fichier );

	// Vider le fichier
	if( ( this->m_fichier = fopen( this->m_nom,
		"wb+" ) ) != NULL )
		fclose( this->m_fichier );

	// Rouvrir le fichier
	return ( this->m_fichier = fopen( this->m_nom,
		"ab+" ) ) != NULL;
}

#endif // NLIB_MODULE_MPEGWRITER

