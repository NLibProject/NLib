#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------
// type NBoolean
// -------------

/**
 * Parse boolean keyword
 *
 * @param keyword
 * 		The keyword
 *
 * @return the boolean value
 */
NBOOL NLib_Type_NBOOL_Parse( const char *keyword )
{
	// Check
	return NLib_Chaine_Comparer( keyword,
		NTRUE_KEYWORD,
		NFALSE,
		0 ) ?
			NTRUE
			: NFALSE;
}

/**
 * Get boolean keyword
 *
 * @param value
 * 		The boolean value
 *
 * @return the boolean keyword
 */
const char *NLib_Type_NBOOL_GetKeyword( NBOOL value )
{
	return value ?
	   NTRUE_KEYWORD
	   : NFALSE_KEYWORD;
}

