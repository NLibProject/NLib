#include "../../../include/NLib/NLib.h"

/**
 *	Gestion des caracteres
 *
 *	@author SOARES Lucas
 */

// -------------------------
// namespace NLib::Caractere
// -------------------------

/**
 * Est un chiffre?
 * 
 * @param caractere
 * 		Le caractere
 * 
 * @return si c'est un chiffre
 */
NBOOL NLib_Caractere_EstUnChiffre( char caractere )
{
	return (NBOOL)( caractere >= '0'
		&& caractere <= '9' );
}

/**
 * Est un chiffre au format hexadecimal?
 * 
 * @param caractere
 * 		Le caractere
 * 
 * @return si c'est un chiffre au format hexadecimal
 */
NBOOL NLib_Caractere_EstUnChiffreHexadecimal( char caractere )
{
	return (NBOOL)( ( caractere >= 'A'
				&& caractere <= 'F' )
			|| ( caractere >= 'a'
				&& caractere <= 'f' ) );
}

/**
 * Est une lettre?
 *
 * @param caractere
 * 		Le caractere
 *
 * @return si c'est une lettre
 */
NBOOL NLib_Caractere_EstUneLettre( char caractere )
{
	return (NBOOL)( ( caractere >= 'A'
				&& caractere <= 'Z' )
			|| ( caractere >= 'a'
					&& caractere <= 'z' ) );
}

/**
 * Est un separateur
 *
 * @param caractere
 * 		Le caractere
 *
 * @return si c'est un separateur
 */
NBOOL NLib_Caractere_EstUnSeparateur( char caractere )
{
	switch( caractere )
	{
		case ' ':
		case '\n':
		case '\r':
		case '\t':
			return NTRUE;

		default:
			return NFALSE;
	}
}

/**
 * Convertir caractere vers entier
 *
 * @param caractere
 * 		Le caractere
 *
 * @return la valeur decimale
 */
NU32 NLib_Caractere_ConvertirDecimal( char caractere )
{
	if( NLib_Caractere_EstUnChiffre( caractere ) )
		return (NU32)( caractere - '0' );
	else
		if( NLib_Caractere_EstUnChiffreHexadecimal( caractere ) )
			switch( caractere )
			{
				case 'a':
				case 'A':
					return 0x0A;
				case 'b':
				case 'B':
					return 0x0B;
				case 'c':
				case 'C':
					return 0x0C;
				case 'd':
				case 'D':
					return 0x0D;
				case 'e':
				case 'E':
					return 0x0E;
				case 'f':
				case 'F':
					return 0x0F;

				default:
					return 0;
			}

	// Pas un chiffre
	return NERREUR;
}

/**
 * Convertir un nombre en caractere entre 0 et F
 *
 * @param nombre
 * 		Le nombre a convertir
 *
 * @return le caractere hexadecimal ou -1 en cas d'erreur
 */
char NLib_Caractere_ConvertirHexadecimal( NU8 nombre )
{
	// Verifier
	if( nombre > 15 )
		return -1;

	// Convertir caractere chiffre
	if( nombre < 10 )
		return (char)'0' + (char)nombre;

	// Convertir caractere hexadecimal
	return (char)'A' + (char)( nombre - 10 );
}

/**
 * Est un caractere acceptable dans HTML
 *
 * @param caractere
 * 		Le caractere
 *
 * @return si le caractere est viable dans un fichier HTML
 */
NBOOL NLib_Caractere_EstUnCaractereViableHTMLHref( char caractere )
{
	// Le caractere est tolere?
	if( NLib_Caractere_EstUnChiffre( caractere )
		|| NLib_Caractere_EstUneLettre( caractere ) )
		return NTRUE;

	// Analyser le caractere
	switch( caractere )
	{
		case '/':
		case '\\':
		case '.':
		case '_':
		case '-':
			return NTRUE;

		default:
			return NFALSE;
	}
}

/**
 * Est un caractere acceptable dans un nom de domaine?
 *
 * @param caractere
 *		Le caractere a analyser
 *
 * @return si le caractere est accepte
 */
NBOOL NLib_Caractere_EstUnCaractereViableNomDomaine( char caractere )
{
	// Le caractere est viable?
	if( NLib_Caractere_EstUnChiffre( caractere )
		|| NLib_Caractere_EstUneLettre( caractere ) )
		return NTRUE;

	// Analyser caractere
	switch( caractere )
	{
		case '.':
		case '-':
		case ':':
			return NTRUE;

		default:
			return NFALSE;
	}
}

/**
 * Convertir en minuscule
 *
 * @param caractere
 *		Le caractere a convertir
 *
 * @return le resultat de la conversion
 */
char NLib_Caractere_ConvertirMinuscule( char caractere )
{
	// Lettre majuscule classique?
	if( caractere >= 'A'
		&& caractere <= 'Z' )
		// Convertir
		return caractere + (char)'a' - (char)'A';

	// Accents?
	switch( caractere )
	{
		case (char)192:
			return (char)224;
		case (char)193:
			return (char)225;
		case (char)194:
			return (char)226;
		case (char)195:
			return (char)227;
		case (char)196:
			return (char)228;
		case (char)197:
			return (char)229;
		case (char)198:
			return (char)230;
		case (char)199:
			return (char)231;
		case (char)200:
			return (char)232;
		case (char)201:
			return (char)233;
		case (char)202:
			return (char)234;
		case (char)203:
			return (char)235;
		case (char)204:
			return (char)236;
		case (char)205:
			return (char)237;
		case (char)206:
			return (char)238;
		case (char)207:
			return (char)239;
		case (char)209:
			return (char)241;
		case (char)210:
			return (char)242;
		case (char)211:
			return (char)243;
		case (char)212:
			return (char)244;
		case (char)213:
			return (char)245;
		case (char)214:
			return (char)246;
		case (char)217:
			return (char)249;
		case (char)218:
			return (char)250;
		case (char)219:
			return (char)251;
		case (char)220:
			return (char)252;
		case (char)221:
			return (char)253;

		default:
			return caractere;
	}
}

/**
 * Convertir en majuscule
 *
 * @param caractere
 *		Le caractere a convertir
 *
 * @return le resultat de la conversion
 */
char NLib_Caractere_ConvertirMajuscule( char caractere )
{
	// Lettre minuscule?
	if( caractere >= 'a'
		&& caractere <= 'z' )
		// Convertir
		return caractere - ( (char)'a' - (char)'A' );

	// Accents?
	switch( caractere )
	{
		case (char)224:
			return (char)192;
		case (char)225:
			return (char)193;
		case (char)226:
			return (char)194;
		case (char)227:
			return (char)195;
		case (char)228:
			return (char)196;
		case (char)229:
			return (char)197;
		case (char)230:
			return (char)198;
		case (char)231:
			return (char)199;
		case (char)232:
			return (char)200;
		case (char)233:
			return (char)201;
		case (char)234:
			return (char)202;
		case (char)235:
			return (char)203;
		case (char)236:
			return (char)204;
		case (char)237:
			return (char)205;
		case (char)238:
			return (char)206;
		case (char)239:
			return (char)207;
		case (char)240:
			return (char)208;
		case (char)241:
			return (char)209;
		case (char)242:
			return (char)210;
		case (char)243:
			return (char)211;
		case (char)244:
			return (char)212;
		case (char)245:
			return (char)213;
		case (char)249:
			return (char)217;
		case (char)250:
			return (char)218;
		case (char)251:
			return (char)219;
		case (char)252:
			return (char)220;
		case (char)253:
			return (char)221;

		default:
			return caractere;
	}
}

/**
 * Convertir une valeur vers base64
 *
 * @param valeur
 * 		La valeur a convertir
 * @param estEncoderPourURL
 * 		On veut encoder pour une URL?
 *
 * @return le caractere en base64 associe ou 0xFF si cette valeur ne veut rien dire
 */
char NLib_Caractere_ConvertirValeurVersBase64( NU8 valeur,
	NBOOL estEncoderPourURL )
{
	// Filtrer valeur
	valeur &= 0x0000003F;

	/**
	 * 0 000000 A           17 010001 R           34 100010 i           51 110011 z
	 * 1 000001 B           18 010010 S           35 100011 j           52 110100 0
	 * 2 000010 C           19 010011 T           36 100100 k           53 110101 1
	 * 3 000011 D           20 010100 U           37 100101 l           54 110110 2
	 * 4 000100 E           21 010101 V           38 100110 m           55 110111 3
	 * 5 000101 F           22 010110 W           39 100111 n           56 111000 4
	 * 6 000110 G           23 010111 X           40 101000 o           57 111001 5
	 * 7 000111 H           24 011000 Y           41 101001 p           58 111010 6
	 * 8 001000 I           25 011001 Z           42 101010 q           59 111011 7
	 * 9 001001 J           26 011010 a           43 101011 r           60 111100 8
	 *10 001010 K           27 011011 b           44 101100 s           61 111101 9
	 *11 001011 L           28 011100 c           45 101101 t           62 111110 +
	 *12 001100 M           29 011101 d           46 101110 u           63 111111 /
	 *13 001101 N           30 011110 e           47 101111 v
	 *14 001110 O           31 011111 f           48 110000 w        (complement) =
	 *15 001111 P           32 100000 g           49 110001 x
	 *16 010000 Q           33 100001 h           50 110010 y
 */

	// Determiner
	if( valeur >= 0
		&& valeur <= 25 )
		return (char)( 'A' + valeur );
	else if( valeur >= 26
		&& valeur <= 51 )
		return (char)( 'a' + ( valeur - 26 ) );
	else if( valeur >= 52
		&& valeur <= 61 )
		return (char)( '0' + ( valeur - 52 ) );

	// Caracteres speciaux
	switch( valeur )
	{
		case 62:
			return (char)( estEncoderPourURL ?
			   '-'
			   : '+' );
		case 63:
			return (char)( estEncoderPourURL ?
				'_'
				: '/' );

		default:
			return (char)0xFF;
	}
}

/**
 * Convertir caractere base64 vers valeur
 *
 * @param valeur
 * 		Le caractere base64
 *
 * @return le caractere normal sur 8 bits
 */
NU8 NLib_Caractere_ConvertirValeurDepuisBase64( char valeur )
{
	// Entre A et Z
	if( valeur >= 'A'
		&& valeur <= 'Z' )
		return (NU8)( valeur - 'A' );

	// Entre a et z
	if( valeur >= 'a'
		&& valeur <= 'z' )
		return (NU8)( valeur - 'a' + 26 );

	// Entre 0 et 9
	if( valeur >= '0'
		&& valeur <= '9' )
		return (NU8)( valeur - '0' + 52 );

	// Cas particuliers
	switch( valeur )
	{
		case '+':
		case '-':
			return 62;
		case '/':
		case '_':
			return 63;

		default:
			return '\0';
	}
}

