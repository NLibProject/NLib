#ifndef NLIB_TYPE_RECT_NSRECT_PROTECT
#define NLIB_TYPE_RECT_NSRECT_PROTECT

/*
	Un rectangle a la position signee

	@author SOARES Lucas
*/

// -------------------------------
// struct NLib::Type::Rect::NSRect
// -------------------------------

typedef struct
{
	// Position
	NSPoint m_position;

	// Taille
	NUPoint m_taille;
} NSRect;


#endif // !NLIB_TYPE_RECT_NSRECT_PROTECT

