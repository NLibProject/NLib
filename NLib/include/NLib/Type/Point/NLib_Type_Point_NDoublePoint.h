#ifndef NLIB_TYPE_POINT_NDOUBLEPOINT_PROTECT
#define NLIB_TYPE_POINT_NDOUBLEPOINT_PROTECT

// --------------------------------------
// struct NLib::Type::Point::NDoublePoint
// --------------------------------------

typedef struct NDoublePoint
{
	double x,
		y;
} NDoublePoint;

#endif // !NLIB_TYPE_POINT_NDOUBLEPOINT_PROTECT

