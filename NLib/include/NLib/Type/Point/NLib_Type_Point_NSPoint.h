#ifndef NLIB_TYPE_POINT_NSPOINT_PROTECT
#define NLIB_TYPE_POINT_NSPOINT_PROTECT

/*
	Un point signe

	@author SOARES Lucas
*/

// ---------------------------------
// struct NLib::Type::Point::NSPoint
// ---------------------------------

typedef struct
{
	// Coordonnee x
	NS32 x;

	// Coordonnee y
	NS32 y;
} NSPoint;

#endif // !NLIB_TYPE_POINT_NSPOINT_PROTECT

