#ifndef NLIB_TYPE_POINT_NUPOINT_PROTECT
#define NLIB_TYPE_POINT_NUPOINT_PROTECT

/*
	Un point non signe

	@author SOARES Lucas
*/

// ---------------------------------
// struct NLib::Type::Point::NUPoint
// ---------------------------------

typedef struct
{
	// Coordonnee x
	NU32 x;

	// Coordonnee y
	NU32 y;
} NUPoint;

#endif // !NLIB_TYPE_POINT_NUPOINT_PROTECT

