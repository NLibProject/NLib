#ifndef NLIB_TYPE_NBOOL
#define NLIB_TYPE_NBOOL

/*
	Type booleen (NBOOL/NTRUE/NFALSE)

	@author SOARES Lucas
*/

// NBOOL type
typedef unsigned int NBOOL;

// Boolean constant
#define NFALSE	(NBOOL)0
#define NTRUE	(NBOOL)1

// Boolean keywords
#define NFALSE_KEYWORD		"false"
#define NTRUE_KEYWORD		"true"

/**
 * Parse boolean keyword
 *
 * @param keyword
 * 		The keyword
 *
 * @return the boolean value
 */
NBOOL NLib_Type_NBOOL_Parse( const char *keyword );

/**
 * Get boolean keyword
 *
 * @param value
 * 		The boolean value
 *
 * @return the boolean keyword
 */
const char *NLib_Type_NBOOL_GetKeyword( NBOOL value );

#endif // !NLIB_TYPE_NBOOL

