#ifndef NLIB_TEMPS_PROTECT
#define NLIB_TEMPS_PROTECT

/*
	Gestion du temps

	@author SOARES Lucas
*/

// ---------------------
// namespace NLib::Temps
// ---------------------

#ifndef IS_WINDOWS
#include <time.h>
#include <unistd.h>
#endif // !IS_WINDOWS

// namespace NLib::Temps::Animation
#include "Animation/NLib_Temps_Animation.h"

// struct NLib::Temps::NFramerate
#include "NLib_Temps_NFramerate.h"

// Nombre d'iterations avant le calcul du framerate
#define NLIB_TEMPS_NFRAMERATE_NOMBRE_ITERATION_AVANT_CALCUL		50

/* Obtenir le nombre de ms depuis le lancement */
NU32 NLib_Temps_ObtenirTick( void );
NU64 NLib_Temps_ObtenirTick64( void );

/* Attendre (ms) */
void NLib_Temps_Attendre( NU32 );

/* Obtenir nombre aleatoire */
NU32 NLib_Temps_ObtenirNombreAleatoire( void );

/**
 * Obtenir timestamp
 *
 * @return le timestamp en secondes
 */
NU64 NLib_Temps_ObtenirTimestamp( void );

/**
 * Obtenir date formattee JJ/MM/AAAA, hh:mm:ss
 *
 * @return la date du jour
 */
__ALLOC char *NLib_Temps_ObtenirDateFormate( void );

/**
 * Obtenir date formattee JJ/MM/AAAA, hh:mm:ss
 *
 * @param timestamp
 * 		Le timestamp a partir duquel calculer la date
 *
 * @return la date du jour
 */
__ALLOC char *NLib_Temps_ObtenirDateFormate2( NU64 timestamp );

/**
 * Obtenir heure formatee hh:mm:ss
 *
 * @param nombreSeconde
 * 		Le timestamp depuis lequel calculer
 *
 * @return la chaine formatee correspondant au temps
 */
__ALLOC char *NLib_Temps_ObtenirHeureFormate( NU64 nombreSeconde );

#endif // !NLIB_TEMPS_PROTECT

