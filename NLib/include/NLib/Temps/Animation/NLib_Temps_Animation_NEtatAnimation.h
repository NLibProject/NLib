#ifndef NLIB_TEMPS_ANIMATION_NETATANIMATION_PROTECT
#define NLIB_TEMPS_ANIMATION_NETATANIMATION_PROTECT

// ---------------------------------------------
// struct NLib::Temps::Animation::NEtatAnimation
// ---------------------------------------------

typedef struct
{
	// Etat
		// Frame actuelle
			NU32 m_frame;
		// Etape animation
			NU32 m_etapeAnimation;
		// Dernier update
			NU32 m_dernierUpdate;

	// Consigne pour l'animation
		// Delai entre les frame
			NU32 m_delaiFrame;
		// Nombre frames
			NU32 m_nombreFrame;
		// Nombre d'etape d'animation
			NU32 m_nombreEtapeAnimation;
		// Ordre
			NU32 *m_ordreAnimation;
} NEtatAnimation;

/* Construire */
__ALLOC NEtatAnimation *NLib_Temps_Animation_NEtatAnimation_Construire( NU32 delaiFrame,
	NU32 nombreFrame,
	NU32 nombreEtapeAnimation,
	const NU32 *ordreAnimation );
__ALLOC NEtatAnimation *NLib_Temps_Animation_NEtatAnimation_Construire2( const NEtatAnimation* );

/* Detruire */
void NLib_Temps_Animation_NEtatAnimation_Detruire( NEtatAnimation** );

/* Update */
void NLib_Temps_Animation_NEtatAnimation_Update( NEtatAnimation* );

/* Obtenir frame */
NU32 NLib_Temps_Animation_NEtatAnimation_ObtenirFrame( const NEtatAnimation* );

/* Obtenir nombre frames */
NU32 NLib_Temps_Animation_NEtatAnimation_ObtenirNombreFrame( const NEtatAnimation* );

/* Obtenir delai entre frames */
NU32 NLib_Temps_Animation_NEtatAnimation_ObtenirDelaiFrame( const NEtatAnimation* );

/* Obtenir nombre etapes animation */
NU32 NLib_Temps_Animation_NEtatAnimation_ObtenirNombreEtapeAnimation( const NEtatAnimation* );

/* Obtenir ordre animation */
const NU32 *NLib_Temps_Animation_NEtatAnimation_ObtenirOrdreAnimation( const NEtatAnimation* );

/* Est a la fin de l'animation? */
NBOOL NLib_Temps_Animation_NEtatAnimation_EstFinAnimation( const NEtatAnimation* );

/* Remettre a zero */
void NLib_Temps_Animation_NEtatAnimation_RemettreAZero( NEtatAnimation* );

#endif // !NLIB_TEMPS_ANIMATION_NETATANIMATION_PROTECT

