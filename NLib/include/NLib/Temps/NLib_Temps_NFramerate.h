#ifndef NLIB_TEMPS_NFRAMERATE_PROTECT
#define NLIB_TEMPS_NFRAMERATE_PROTECT

// ------------------------------
// struct NLib::Temps::NFramerate
// ------------------------------

typedef struct NFramerate
{
	// Framerate souhaite
	NU32 m_framerateSouhaite;

	// Temps d'attente entre deux frames
	NU32 m_tempsAttenteEntreDeuxFrame;

	// Temps debut fonction
	NU32 m_tempsDebut;

	// Total temps entre iteration
	NU32 m_totalTempsEntreSuiteIteration;

	// Nombre d'iterations
	NU32 m_nombreIteration;

	// Framerate actuel
	float m_framerateActuel;
} NFramerate;

/**
 * Construire le framerate
 *
 * @param framerateSouhaite
 *		Le framerate souhaite
 *
 * @return le framerate
 */
__ALLOC NFramerate *NLib_Temps_NFramerate_Construire( NU32 framerateSouhaite );

/**
 * Detruire le framerate
 *
 * @param this
 *		Cette instance
 */
void NLib_Temps_NFramerate_Detruire( NFramerate** );

/**
 * Obtenir le framerate actuel
 *
 * @param this
 *		Cette instance
 *
 * @return le framerate actuel
 */
NU32 NLib_Temps_NFramerate_ObtenirFramerateActuel( const NFramerate* );

/**
 * Debuter fonction
 *
 * @param this
 *		Cette instance
 */
void NLib_Temps_NFramerate_DebuterFonction( NFramerate* );

/**
 * Terminer fonction
 *
 * @param this
 *		Cette instance
 */
void NLib_Temps_NFramerate_TerminerFonction( NFramerate* );

#endif // !NLIB_TEMPS_NFRAMERATE_PROTECT

