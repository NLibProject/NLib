#ifndef NLIB_CHAINE_PROTECT
#define NLIB_CHAINE_PROTECT

/**
 *	Gestion des chaines de caracteres
 *
 *	@author SOARES Lucas
 */

// ----------------------
// namespace NLib::Chaine
// ----------------------

// Expression reguliere
#ifndef IS_WINDOWS
#include <regex.h>
#endif // !IS_WINDOWS

/**
 * Est a la fin du fichier?
 *
 * @param source
 * 		La chaine source
 * @param curseur
 * 		Le curseur dans la chaine
 *
 * @return si EOF
 */
NBOOL NLib_Chaine_EstEOF( const char *source,
	NU32 curseur );

/**
 * Est a la fin du fichier?
 *
 * @param curseur
 * 		Le curseur dans le fichier
 * @param taille
 * 		La taille du fichier
 *
 * @return si EOF
 */
NBOOL NLib_Chaine_EstEOF2( NU32 curseur,
	NU32 taille );

/**
 * Est un nombre entier?
 *
 * @param chaine
 *		La chaine a analyser
 * @param base
 * 		La base consideree (10 ou 16?)
 *
 * @return si c'est un nombre entier
 */
NBOOL NLib_Chaine_EstUnNombreEntier( const char *chaine,
	NU32 base );

/**
 * Est un nombre reel?
 *
 * @param chaine
 *		La chaine a analyser
 *
 * @return si c'est un nombre reel
 */
NBOOL NLib_Chaine_EstUnNombreReel( const char *chaine );

/**
 * Placer le curseur au caractere
 *
 * @param source
 * 		La chaine a considerer
 * @param caractere
 * 		Le caractere ou se placer
 * @param positionActuelle
 * 		La position actuelle du curseur
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Chaine_PlacerAuCaractere( const char *source,
	char caractere,
	NU32 *positionActuelle );

/**
 * Se placer a la fin de la chaine
 *
 * @param source
 * 		La chaine a considerer
 * @param chaineRecherche
 * 		La chaine a laquelle se placer
 * @param positionActuelle
 * 		La position actuelle dans la chaine source
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Chaine_PlacerALaChaine( const char *source,
	const char *chaineRecherche,
	NU32 *positionActuelle );

/**
 * Lire une chaine entre deux separateurs
 *
 * @param source
 * 		La chaine source
 * @param caractereDelimiteurGauche
 * 		Le delimitateur gauche
 * @param caractereDelimiteurDroit
 * 		Le delimitateur droit
 * @param nombreCaractereArret
 * 		Le nombre de caractere dans c2
 * @param positionActuelle
 * 		La position dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position a la fin de la lecture
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Chaine_LireEntre( const char *source,
	char caractereDelimiteurGauche,
	const char *caractereDelimiteurDroit,
	NU32 nombreCaractereArret,
	NU32 *positionActuelle,
	NBOOL estConserverPosition );

/**
 * Lire entre deux caractere
 *
 * @param source
 * 		La chaine source
 * @param c
 * 		Le delimitateur gauche/droit
 * @param positionActuelle
 * 		La position actuelle
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Chaine_LireEntre2( const char *source,
	char c,
	NU32 *positionActuelle,
	NBOOL estConserverPosition );

/**
 * Lire a partir de la position actuelle jusqu'au caractere
 *
 * @param source
 * 		La chaine source
 * @param c
 * 		Le caractere ou s'arreter
 * @param positionActuelle
 * 		La position actuelle dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Chaine_LireJusqua( const char *source,
	char c,
	NU32 *positionActuelle,
	NBOOL estConserverPosition );

/**
 * Lire a partir de la position actuelle jusqu'aux caracteres presents dans c
 *
 * @param source
 * 		La chaine source
 * @param listeCaractere
 * 		Le tableau de caracteres ou s'arreter
 * @param nombreCaractereArret
 * 		Le nombre de caractere dans c
 * @param positionActuelle
 * 		La position actuelle dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Chaine_LireJusqua2( const char *source,
	const char *listeCaractere,
	NU32 nombreCaractereArret,
	NU32 *positionActuelle,
	NBOOL estConserverPosition );

/**
 * Placer curseur avant prochaine lettre (en ignorant tous les separateurs espace/\t/\n...
 *
 * @param source
 * 		La chaine source
 * @param curseur
 * 		Le curseur
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Chaine_PlacerCurseurProchainCaractereNonSeparateur( const char *source,
	NU32 *curseur );

/**
 * Obtenir le prochain caractere
 *
 * @param source
 * 		La chaine source
 * @param positionActuelle
 * 		La position actuelle dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture?
 * @param estInclureSeparateur
 * 		Doit-on considerer /\t/\n/' '/...
 *
 * @return le prochain caractere
 */
char NLib_Chaine_ObtenirProchainCaractere2( const char *source,
	NU32 *positionActuelle,
	NBOOL estConserverPosition,
	NBOOL estInclureSeparateur );

/**
 * Obtenir le prochain caractere
 *
 * @param source
 * 		La chaine source
 * @param positionActuelle
 * 		La position actuelle dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position apres lecture?
 *
 * @return le prochain caractere
 */
char NLib_Chaine_ObtenirProchainCaractere( const char *source,
	NU32 *positionActuelle,
	NBOOL estConserverPosition );

/**
 * Lire un nombre non signe
 *
 * @param chaine
 * 		La chaine source
 * @param curseur
 * 		Le curseur dans la chaine
 * @param estConserverPosition
 * 		Restaurer position apres lecture?
 *
 * @return le nombre lu
 */
NU32 NLib_Chaine_LireNombreNonSigne( const char *chaine,
	NU32 *curseur,
	NBOOL estConserverPosition );

/**
 * Lire un nombre signe
 *
 * @param chaine
 * 		La chaine source
 * @param curseur
 * 		Le curseur dans la chaine
 * @param estConserverPosition
 * 		Restaurer position apres lecture?
 *
 * @return le nombre lu
 */
NU32 NLib_Chaine_LireNombreSigne( const char *chaine,
	NU32 *curseur,
	NBOOL estConserverPosition );

/**
 * Est ce que la chaine est vide? (Que des separateurs/taille nulle?)
 *
 * @param source
 * 		La chaine source
 *
 * @return si la chaine est vide
 */
NBOOL NLib_Chaine_EstVide( const char* );

/**
 * Comparaison chaine
 *
 * @param chaine1
 *		La premiere chaine
 * @param chaine2
 *		La deuxieme chaine
 * @param estCasseAConsiderer
 *		Considere-t-on la casse?
 * @param taille
 *		La taille a considerer (si nulle, les tailles sont comparees)
 *
 * @return si les chaines sont identiques
 */
NBOOL NLib_Chaine_Comparer( const char *chaine1,
	const char *chaine2,
	NBOOL estCasseAConsiderer,
	NU32 taille );

/**
 * La chaine contient le caractere?
 *
 * @param chaine
 *		La chaine
 * @param caractere
 *		Le caractere
 *
 * @return si la chaine contient le caractere
 */
NBOOL NLib_Chaine_EstChaineContient( const char *chaine,
	char caractere );

/**
 * La chaine contient la chaine?
 *
 * @param chaine
 * 		La chaine dans laquelle chercher
 * @param motif
 * 		Le motif a rechercher dans la chaine
 * @param estConsidereCasse
 * 		Considere-t-on la casse de la chaine lors de la recherche?
 *
 * @return si la chaine contient le motif
 */
NBOOL NLib_Chaine_EstChaineContient2( const char *chaine,
	const char *motif,
	NBOOL estConsidereCasse );

/**
 * Est chaine continent lettres?
 *
 * @param chaine
 * 		La chaine a analyser
 *
 * @return si la chaine contient des lettres
 */
NBOOL NLib_Chaine_EstChaineContientLettre( const char *chaine );

/**
 * Remplacer un caractere par un autre
 *
 * @param chaine
 *		La chaine
 * @param source
 *		Source
 * @param destination
 *		Destination
 */
void NLib_Chaine_Remplacer( char *chaine,
	char source,
	char destination );

#ifndef IS_WINDOWS
/**
 * Comparer une chaine avec une regex (Et retourne si l'expression correspond)
 *
 * @param expression
 *		L'expression a analyser
 * @param regex
 * 		La regex a laquelle comparer l'expression
 *
 * @return si l'expression correspond a la regex
 */
NBOOL NLib_Chaine_EstCorrespondRegex( const char *expression,
	const char *regex );
#endif // !IS_WINDOWS

/**
 * Compter le nombre d'occurences d'un caractere
 *
 * @param chaine
 *		La chaine
 * @param caractere
 *		Le caractere
 *
 * @return le nombre d'occurence(s) de c dans chaine
 */
NU32 NLib_Chaine_CompterNombreOccurence( const char *chaine,
	char caractere );

/**
 * Compter le nombre de caracteres au debut de la chaine
 *
 * @param chaine
 * 		La chaine
 * @param caractere
 *		Le caractere a compter
 *
 * @return le nombre de caracteres
 */
NU32 NLib_Chaine_CompterNombreCaractereDebut( const char *chaine,
	char caractere );

/**
 * Dupliquer une chaine
 *
 * @param source
 * 		La chaine a dupliquer
 *
 * @return la chaine dupliquee
 */
__ALLOC char *NLib_Chaine_Dupliquer( const char *source );

/**
 * Dupliquer une chaine de facon securisee (en fixant la taille utile)
 *
 * @param source
 * 		La chaine a dupliquer
 * @param taille
 * 		La taille de la chaine a dupliquer (sans \0)
 *
 * @return la copie de la chaine
 */
__ALLOC char *NLib_Chaine_DupliquerSecurite( const char *source,
	NU32 taille );

/**
 * Lire une chaine depuis stdin
 *
 * @param tailleMaximale
 * 		La taille maximale a lire (0 = s'arrete a \n)
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Chaine_LireStdin( NU32 tailleMaximale );

/**
 * Lire une chaine depuis stdin dans un buffer
 *
 * @param buffer
 * 		Le buffer
 * @param tailleBuffer
 * 		La taille du buffer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Chaine_LireStdin2( char *buffer,
	NU32 tailleBuffer );

/**
 * Supprimer caractere
 *
 * @param source
 * 		La chaine
 * @param caractere
 * 		Le caractere a supprimer
 *
 * @return le nombre de caractere(s) supprime(s)
 */
NU32 NLib_Chaine_SupprimerCaractere( char *source,
	char caractere );

/**
 * Supprimer caractere au debut
 *
 * @param source
 * 		La chaine
 * @param caractere
 * 		Le caractere a supprimer
 *
 * @return le nombre de caractere initiaux supprimes
 */
NU32 NLib_Chaine_SupprimerCaractereDebut( char *source,
	char caractere );

/**
 * Mettre la chaine en minuscule
 *
 * @param source
 * 		La chaine
 */
void NLib_Chaine_MettreEnMinuscule( char *source );

/**
 * Mettre la chaine en majuscule
 *
 * @param source
 * 		La chaine
 */
void NLib_Chaine_MettreEnMajuscule( char *source );

/**
 * Convertir en base64
 *
 * @param texte
 * 		Le texte
 * @param taille
 * 		La taille du texte
 *
 * @return le texte en base64
 */
__ALLOC char *NLib_Chaine_ConvertirBase64( const char *texte,
	NU32 taille );

/**
 * Convertir en base64
 *
 * @param texte
 * 		Le texte
 * @param taille
 * 		La taille du texte
 * @param estEncoderPourURL
 * 		On veut encoder pour mettre dans une URL? (Et donc remplacer + par - et / par _)
 *
 * @return le texte en base64
 */
__ALLOC char *NLib_Chaine_ConvertirBase64_2( const char *texte,
	NU32 taille,
	NBOOL estEncoderPourURL );

/**
 * Decoder base64
 *
 * @param texte
 * 		Le message en base64
 * @param taille
 * 		La taille du message
 *
 * @return le message decode
 */
__ALLOC char *NLib_Chaine_DecoderBase64( const char *texte,
	NU32 taille );

/**
 * Lire ligne
 *
 * @param texte
 * 		Le texte
 * @param curseur
 * 		Le curseur dans le texte
 * @param estConserverPosition
 * 		Doit-on conserver la position?
 *
 * @return la ligne lue
 */
__ALLOC char *NLib_Chaine_LireLigne( const char *texte,
	NU32 *curseur,
	NBOOL estConserverPosition );

/**
 * Lire prochaine ligne sans commentaire
 *
 * @param texte
 * 		Le texte
 * @param curseur
 * 		Le curseur dans le texte
 * @param estConserverPosition
 * 		Doit-on conserver la position?
 * @param caractereCommentaire
 * 		Le caractere utilise pour signaler un commentaire
 *
 * @return la ligne lue
 */
__ALLOC char *NLib_Chaine_LireProchaineLigneSansCommentaire( const char *texte,
	NU32 *curseur,
	NBOOL estConserverPosition,
	char caractereCommentaire );

/**
 * Decouper suivant un caractere (c1.c2.c3 avec . donne [0]="c1", [1]="c2", [2]="c3")
 *
 * @param texte
 * 		Le texte a decouper
 * @param caractere
 * 		Le caractere suivant lequel decouper
 *
 * @return la liste des mots
 */
__ALLOC NListe *NLib_Chaine_Decouper( const char *texte,
	char caractere );

/**
 * La chaine commence par ca?
 *
 * @param texte
 * 		Le texte
 * @param debut
 * 		Le texte par lequel doit commencer la chaine
 *
 * @return si le texte commence par %debut%
 */
NBOOL NLib_Chaine_EstChaineCommencePar( const char *texte,
	const char *debut );

#endif // !NLIB_CHAINE_PROTECT

