#ifndef NLIB_MEMOIRE_NDATA_PROTECT
#define NLIB_MEMOIRE_NDATA_PROTECT

// ---------------------------
// struct NLib::Memoire::NData
// ---------------------------

typedef struct NData
{
	// Donnees
	char *m_data;

	// Taille des donnees
	NU32 m_tailleData;
} NData;

/**
 * Construire la data
 *
 * @return une donnee vide
 */
__ALLOC NData *NLib_Memoire_NData_Construire( void );

/**
 * Detruire la data
 *
 * @param this
 * 		Cette instance
 */
void NLib_Memoire_NData_Detruire( NData** );

/**
 * Ajouter data
 *
 * @param this
 * 		Cette instance
 * @param data
 * 		La donnee a ajouter
 * @param tailleData
 * 		La taille de la donnee
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Memoire_NData_AjouterData( NData*,
	const char *data,
	NU32 tailleData );

/**
 * Obtenir les donnees
 *
 * @param this
 * 		Cette instance
 *
 * @return les donnees
 */
const char *NLib_Memoire_NData_ObtenirData( const NData* );

/**
 * Obtenir la taille des donnees
 *
 * @param this
 * 		Cette instance
 *
 * @return la taille des donnees
 */
NU32 NLib_Memoire_NData_ObtenirTailleData( const NData* );

#endif // !NLIB_MEMOIRE_NDATA_PROTECT

