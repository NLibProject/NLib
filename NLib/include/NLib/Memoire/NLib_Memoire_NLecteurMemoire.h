#ifndef NLIB_MEMOIRE_NLECTEURMEMOIRE_PROTECT
#define NLIB_MEMOIRE_NLECTEURMEMOIRE_PROTECT

// -------------------------------------
// struct NLib::Memoire::NLecteurMemoire
// -------------------------------------

typedef struct NLecteurMemoire
{
	// Donnees
	const char *m_data;

	// Taille donnees
	NU32 m_tailleData;

	// Curseur
	NU32 m_curseur;
} NLecteurMemoire;

/**
 * Construire le lecteur
 *
 * @param data
 *		Les donnees
 * @param tailleData
 *		La taille des donnees
 *
 * @return l'instance du lecteur
 */
__ALLOC NLecteurMemoire *NLib_Memoire_NLecteurMemoire_Construire( const char *data,
	NU32 tailleData );

/**
 * Detruire le lecteur
 *
 * @param this
 *		Cette instance
 */
void NLib_Memoire_NLecteurMemoire_Detruire( NLecteurMemoire** );

/**
 * Positionner curseur
 *
 * @param this
 *		Cette instance
 * @param curseur
 *		La nouvelle position
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Memoire_NLecteurMemoire_DefinirCurseur( NLecteurMemoire*,
	NU32 curseur );

/**
 * Obtenir la position du curseur
 *
 * @param this
 *		Cette instance
 *
 * @return la position du curseur
 */
NU32 NLib_Memoire_NLecteurMemoire_ObtenirCurseur( const NLecteurMemoire* );

/**
 * Obtenir donnees
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees
 */
const char *NLib_Memoire_NLecteurMemoire_ObtenirData( const NLecteurMemoire* );

/**
 * Obtenir taille donnees
 *
 * @param this
 *		Cette instance
 *
 * @return la taille des donnees
 */
NU32 NLib_Memoire_NLecteurMemoire_ObtenirTailleData( const NLecteurMemoire* );

/**
 * Lire une chaine de caractere
 *
 * @param this
 *		Cette instance
 * @param taille
 *		La taille a lire
 *
 * @return la chaine de caractere
 */
const char *NLib_Memoire_NLecteurMemoire_Lire( NLecteurMemoire*,
	NU32 taille );

/**
 * Lire un entier non signe
 *
 * @param this
 *		Cette instance
 *
 * @return un entier non signe
 */
NU32 NLib_Memoire_NLecteurMemoire_Lire2( NLecteurMemoire* );

/**
 * Lire un entier signe
 *
 * @param this
 *		Cette instance
 *
 * @return un entier signe
 */
NS32 NLib_Memoire_NLecteurMemoire_Lire3( NLecteurMemoire* );

/**
 * Lire une copie
 *
 * @param this
 *		Cette instance
 * @param taille
 *		La taille a lire
 * @param estChaine
 *		C'est une chaine?
 *
 * @return une copie de ce qui est lu
 */
__ALLOC char *NLib_Memoire_NLecteurMemoire_LireCopieValeur( NLecteurMemoire*,
	NU32 taille,
	NBOOL estChaine );

#endif // !NLIB_MEMOIRE_NLECTEURMEMOIRE_PROTECT

