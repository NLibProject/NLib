#ifndef NLIB_MEMOIRE_NLISTE_PROTECT
#define NLIB_MEMOIRE_NLISTE_PROTECT

// ----------------------------
// struct NLib::Memoire::NListe
// ----------------------------

typedef struct NListe
{
	// Callback de suppression
	void ( ___cdecl *m_callbackSuppression )( void* );

	// Nombre d'elements
	NU32 m_nombre;

	// Elements
	char **m_element;

	// Mutex
	NMutex *m_mutex;
} NListe;

/**
 * Construire la liste
 *
 * @param callbackSuppression
 *		Callback de suppression de l'element
 *
 * @return l'instance de la liste
 */
__ALLOC NListe *NLib_Memoire_NListe_Construire( void ( ___cdecl *callbackSuppression )( void* ) );

/**
 * Detruire la liste
 *
 * @param this
 *		Cette instance
 */
void NLib_Memoire_NListe_Detruire( NListe** );

/**
 * Ajouter un element
 *
 * @param this
 * 		Cette instance
 * @param element
 *		L'element a ajouter
 *
 * @return si l'operation s'est bien passee
 */
__MUSTBEPROTECTED NBOOL NLib_Memoire_NListe_Ajouter( NListe*,
	__WILLBEOWNED void *element );

/**
 * Supprimer un element depuis son adresse
 *
 * @param this
 *		Cette instance
 * @param element
 *		L'adresse de l'element a supprimer
 *
 * @return si l'operation s'est bien passee
 */
__MUSTBEPROTECTED NBOOL NLib_Memoire_NListe_SupprimerDepuisAdresse( NListe*,
	void *element );

/**
 * Supprimer un element depuis son index
 *
 * @param this
 *		Cette instance
 * @param element
 *		L'index de l'element a supprimer
 *
 * @return si l'operation s'est bien passee
 */
__MUSTBEPROTECTED NBOOL NLib_Memoire_NListe_SupprimerDepuisIndex( NListe*,
	NU32 element );

/**
 * Supprimer un element depuis son index
 *
 * @param this
 * 		Cette instance
 * @param element
 * 		L'index de l'element a supprimer
 * @param estDoitLibererElement
 * 		On liberer l'element qu'on supprime?
 *
 * @return si l'operation a reussi
 */
__MUSTBEPROTECTED NBOOL NLib_Memoire_NListe_SupprimerDepuisIndex2( NListe*,
	NU32 element,
	NBOOL estDoitLibererElement );

/**
 * Inverser deux evenements
 *
 * @param this
 *	Cette instance
 * @param e1
 *	L'index de l'element 1
 * @param e2
 *	L'index de l'element 2
 *
 * @return si l'operation s'est bien passee
 */
__MUSTBEPROTECTED NBOOL NLib_Memoire_NListe_Inverser( NListe*,
	NU32 e1,
	NU32 e2 );

/**
 * Obtenir index depuis adresse
 *
 * @param this
 *		Cette instance
 * @param element
 *		L'adresse de l'element dont on cherche l'index
 *
 * @return l'index de l'element
 */
__MUSTBEPROTECTED NU32 NLib_Memoire_NListe_ObtenirIndexDepuisAdresse( const NListe*,
	void *element );

/**
 * Obtenir element depuis index
 *
 * @param this
 * 		Cette instance
 * @param element
 *		L'index de l'element a obtenir
 *
 * @return l'element
 */
__MUSTBEPROTECTED const void *NLib_Memoire_NListe_ObtenirElementDepuisIndex( const NListe*,
	NU32 element );

/**
 * Obtenir le nombre d'elements
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre d'elements
 */
__MUSTBEPROTECTED NU32 NLib_Memoire_NListe_ObtenirNombre( const NListe* );

/**
 * Proteger
 *
 * @param this
 *		Cette instance
 */
__WILLLOCK void NLib_Memoire_NListe_ActiverProtection( NListe* );

/**
 * Ne plus proteger
 *
 * @param this
 *		Cette instance
 */
__WILLUNLOCK void NLib_Memoire_NListe_DesactiverProtection( NListe* );

#endif // !NLIB_MEMOIRE_NLISTE_PROTECT

