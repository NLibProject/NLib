#ifndef NLIB_MEMOIRE_PROTECT
#define NLIB_MEMOIRE_PROTECT

/*
	Gestion de la memoire

	@author SOARES Lucas
*/

// -----------------------
// namespace NLib::Memoire
// -----------------------

// struct NLib::Memoire::NListe
#include "NLib_Memoire_NListe.h"

// struct NLib::Memoire::NLecteurMemoire
#include "NLib_Memoire_NLecteurMemoire.h"

// struct NLib::Memoire::NData
#include "NLib_Memoire_NData.h"

/**
 * Reallouer de la memoire
 *
 * @param src
 * 		La memoire a reallouer
 * @param tailleSrc
 * 		La taille de la memoire initiale
 * @param tailleFinale
 * 		La taille finale souhaitee
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Memoire_ReallouerMemoire( char **src,
	NU32 tailleSrc,
	NU32 tailleFinale );

/* Ajout donnees */
NBOOL NLib_Memoire_AjouterData( char **mem,
	NU32 tailleMem,
	const char *memAjout,
	NU32 tailleMemAjout );

/**
 * Ajout donnees texte
 *
 * @param mem
 * 		L'adresse du pointeur
 * @param tailleMem
 * 		La taille actuelle de l'espace memoire
 * @param texte
 * 		Le texte a ajouter
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Memoire_AjouterData2( char **mem,
	NU32 tailleMem,
	const char *texte );

/* Calculer CRC32 */
NU32 NLib_Memoire_CalculerCRC( const char *data,
	NU32 taille );

/* Swap */
NBOOL NLib_Memoire_Swap( void *d1,
	void *d2,
	NU32 taille );
void NLib_Memoire_SwapEntier( NU32 *d1,
	NU32 *d2 );

/**
 * Afficher la memoire
 *
 * @param mem
 *		La memoire a afficher
 * @param taille
 *		La taille a afficher
 */
void NLib_Memoire_Afficher( const void*,
	NU32 taille );

/**
 * Afficher en hexa
 *
 * @param data
 *		Les donnees
 * @param tailleData
 *		La taille des donnees
 */
void NLib_Memoire_Afficher2( const char *data,
	NU32 tailleData );

/**
 * Liberer un simple element a une dimension
 *
 * @param e
 * 		L'element a liberer
 */
void NLib_Memoire_Liberer( char **e );

/**
 * Afficher entier en binaire
 *
 * @param entier
 * 		L'entier a afficher
 * @param taille
 * 		La taille de l'entier
 * @param tailleGroupe
 * 		Le nombre d'octets par groupe d'octets (4 par exemple)
 */
void NLib_Memoire_AfficherBinaire( void ( *callbackAffichage )( const char * ),
	NU32 entier,
	NU32 taille,
	NU32 tailleGroupe );

/**
 * Compter le nombre de bits à %bitACompter%
 *
 * @param entier
 * 		L'entier a afficher
 * @param taille
 * 		La taille de l'entier
 * @param bitACompter
 * 		0 ou 1 suivant ce qu'on veut compter
 *
 * @return le nombre de bit à %bitACompter%
 */
NU32 NLib_Memoire_CompterNombreBit( NU32 entier,
	NU32 taille,
	NU32 bitACompter );

/**
 * Inverser un tableau de NU8
 *
 * @param tableau
 * 		Le tableau de NU8
 * @param taille
 * 		La taille du tableau
 */
void NLib_Memoire_Inverser( NU8 *tableau,
	NU32 taille );

#endif // !NLIB_MEMOIRE_PROTECT

