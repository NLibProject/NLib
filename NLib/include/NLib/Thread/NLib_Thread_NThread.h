#ifndef NLIB_THREAD_NTHREAD_PROTECT
#define NLIB_THREAD_NTHREAD_PROTECT

// ----------------------------
// struct NLib::Thread::NThread
// ----------------------------

typedef struct NThread
{
#ifdef IS_WINDOWS
	HANDLE m_handle;
#else // IS_WINDOWS
	pthread_t m_handle;
#endif // !IS_WINDOWS

	// Parametre
	void *m_parametre;

	// Handle condition arret
	NBOOL *m_estContinuer;
} NThread;

/**
 * Construit et lance un thread
 *
 * @param fonction
 *		La fonction du thread
 * @param param
 *		Le parametre a passer au thread
 *
 * @return l'instance du thread
 */
__ALLOC NThread *NLib_Thread_NThread_Construire( NBOOL ( *fonction )( void* ),
	void *param,
	NBOOL *conditionArret );

/**
 * Detruire un thread
 *
 * @param this
 *		Cette instance
 */
void NLib_Thread_NThread_Detruire( NThread** );

/**
 * Obtenir parametre thread
 *
 * @param this
 * 		Cette instance
 *
 * @return le parametre
 */
void *NLib_Thread_NThread_ObtenirParametre( const NThread* );

#endif // !NLIB_THREAD_NTHREAD_PROTECT

