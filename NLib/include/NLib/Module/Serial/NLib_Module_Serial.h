#ifndef NLIB_MODULE_SERIAL_PROTECT
#define NLIB_MODULE_SERIAL_PROTECT

#ifdef NLIB_MODULE_SERIAL

// ------------------------------
// namespace NLib::Module::Serial
// ------------------------------

#ifdef IS_WINDOWS
#error Not implemented for windows...
#endif // IS_WINDOWS

// Serial headers
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

// enum NLib::Module_Serial::NDebitSerial
#include "NLib_Module_Serial_NDebitSerial.h"

// struct NLib::Module::Serial::NLiaisonSerie
#include "NLib_Module_Serial_NLiaisonSerie.h"

/**
 * Definir attributs port serie
 *
 * @param descripteurFichier
 * 		Descripteur port serie
 * @param debit
 * 		Le debit
 * @param parity
 * 		On utilise un bit de parite?
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Serial_DefinirAttribut( NS32 descripteurFichier,
	NDebitSerial debit,
	NBOOL parity );

/**
 * Definir si un port serial bloque
 *
 * @param descripteurFichier
 * 		Le descripteur du port serie
 * @param estBloquant
 * 		On veut que le port soit bloquant?
 * @param timeout
 * 		Timeout si bloquant (10^-1 seconde [0.%timeout%]) (Max=255, defaut=5)
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Serial_DefinirBloquant( NS32 descripteurFichier,
	NS32 estBloquant,
	NU8 timeout );
#endif // NLIB_MODULE_SERIAL

#endif // !NLIB_MODULE_SERIAL_PROTECT
