#ifndef NLib_Module_Serial_NLiaisonSerie_PROTECT
#define NLib_Module_Serial_NLiaisonSerie_PROTECT

#ifdef NLIB_MODULE_SERIAL

// ------------------------------------------
// struct NLib::Module::Serial::NLiaisonSerie
// ------------------------------------------

typedef struct NLiaisonSerie
{
	// Descripteur fichier
	NS32 m_descripteur;

	// Debit
	NDebitSerial m_debit;

	// Est erreur?
	NBOOL m_estErreur;
} NLiaisonSerie;

/**
 * Construire liaison serie
 *
 * @param nomTTY
 * 		Le nom du tty a ouvrir (exemple: ttyUSB0)
 * @param debit
 * 		Le debit de la liaison serie
 *
 * @return l'instance de la liaison serie
 */
__ALLOC NLiaisonSerie *NLib_Module_Serial_NLiaisonSerie_Construire( const char *nomTTY,
	NDebitSerial debit );

/**
 * Detruire liaison
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Serial_NLiaisonSerie_Detruire( NLiaisonSerie** );

/**
 * Lire
 *
 * @param this
 * 		Cette instance
 *
 * @return ce qui a ete lu
 */
__ALLOC NData *NLib_Module_Serial_NLiaisonSerie_LireNonBloquant( NLiaisonSerie * );

/**
 * Lire bloquant
 *
 * @param this
 * 		Cette instance
 * @param tailleALire
 * 		La taille a lire
 *
 * @return ce qui a ete lu
 */
__ALLOC NData *NLib_Module_Serial_NLiaisonSerie_LireBloquant( NLiaisonSerie*,
	NU32 tailleALire );

/**
 * Ecrire
 *
 * @param this
 * 		Cette instance
 * @param data
 * 		Les donnees a ecrire
 * @param tailleData
 * 		La tailles des donnees a ecrire
 *
 * @return si l'operation s'est bien passee
 */
 NBOOL NLib_Module_Serial_NLiaisonSerie_Ecrire( NLiaisonSerie*,
	const char *data,
	NU32 tailleData );
#endif // NLIB_MODULE_SERIAL

#endif // !NLib_Module_Serial_NLiaisonSerie_PROTECT
