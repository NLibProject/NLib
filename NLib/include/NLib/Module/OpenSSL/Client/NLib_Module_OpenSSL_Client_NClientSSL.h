#ifndef NLIB_MODULE_OPENSSL_CLIENT_NCLIENTSSL_PROTECT
#define NLIB_MODULE_OPENSSL_CLIENT_NCLIENTSSL_PROTECT

// ------------------------------------------------
// struct NLib::Module::OpenSSL::Client::NClientSSL
// ------------------------------------------------

#if defined( NLIB_MODULE_RESEAU ) && defined( NLIB_MODULE_OPENSSL )
typedef struct NClientSSL
{
	// SOCKET
	SOCKET m_socket;

	// Cache packet
	NCachePacket *m_cachePacket;

	// Adressage client
	SOCKADDR_IN m_addr;

	// Est connecte?
	NBOOL m_estConnecte;

	// Est threads en cours?
	NBOOL m_estThreadEnCours;

	// Est erreur?
	NBOOL m_estErreur;

	// Threads
		// Emission
			HANDLE m_threadEmission;
		// Reception
			HANDLE m_threadReception;

	// Callbacks
		// Argument
			void *m_argumentCallback;
		// Reception packet
			__CALLBACK NBOOL ( *m_callbackReceptionPacket )( const NPacket*,
				void* );
		// Deconnexion client
			__CALLBACK NBOOL ( *m_callbackDeconnexion )( void* );

	// Temps avant timeout (ignore si 0)
	NU32 m_tempsAvantTimeout;

	// Contexte OpenSSL
	SSL_CTX *m_contexte;

	// Etat SSL
	SSL *m_etatSSL;
} NClientSSL;

/* Construire */
__ALLOC NClientSSL *NLib_Module_OpenSSL_Client_NClientSSL_Construire( const NDetailIP *ip,
	NU32 port,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NPacket*,
		void* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( void* ),
	void *argumentCallbacks,
	NU32 tempsAvantTimeout );

/* Detruire */
void NLib_Module_OpenSSL_Client_NClientSSL_Detruire( NClientSSL** );

/* Connecter */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_Connecter( NClientSSL* );

/* Deconnecter */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_Deconnecter( NClientSSL* );

/* Activer erreur */
void NLib_Module_OpenSSL_Client_NClientSSL_ActiverErreur( NClientSSL* );

/* Ajouter un packet */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_AjouterPacket( NClientSSL*,
	__WILLBEOWNED NPacket* );
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_AjouterPacketCopie( NClientSSL*,
	const NPacket* );

/* Est connecte? */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_EstConnecte( const NClientSSL* );

/* Est erreur? */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_EstErreur( const NClientSSL* );

/* Obtenir cache packet */
NCachePacket *NLib_Module_OpenSSL_Client_NClientSSL_ObtenirCachePacket( const NClientSSL* );

/* Obtenir SOCKET */
SOCKET NLib_Module_OpenSSL_Client_NClientSSL_ObtenirSocket( const NClientSSL* );

/* Obtenir etat SSL */
SSL *NLib_Module_OpenSSL_Client_NClientSSL_ObtenirEtatSSL( const NClientSSL* );

/* Obtenir est thread en cours */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_EstThreadEnCours( const NClientSSL* );

/* Obtenir l'argument pour le callback */
void *NLib_Module_OpenSSL_Client_NClientSSL_ObtenirArgumentCallback( const NClientSSL* );

/* Est timeout autorise? */
NBOOL NLib_Module_OpenSSL_Client_NClientSSL_EstTimeoutAutorise( const NClientSSL* );

/* Obtenir delai avant timeout */
NU32 NLib_Module_OpenSSL_Client_NClientSSL_ObtenirTimeout( const NClientSSL* );

#endif // NLIB_MODULE_RESEAU && NLIB_MODULE_OPENSSL

#endif // !NLIB_MODULE_OPENSSL_CLIENT_NCLIENTSSL_PROTECT

