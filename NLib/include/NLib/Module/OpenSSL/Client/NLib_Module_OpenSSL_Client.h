#ifndef NLIB_MODULE_OPENSSL_CLIENT_PROTECT
#define NLIB_MODULE_OPENSSL_CLIENT_PROTECT

// ---------------------------------------
// namespace NLib::Module::OpenSSL::Client
// ---------------------------------------

#if defined( NLIB_MODULE_RESEAU ) && defined( NLIB_MODULE_OPENSSL )
// struct NLib::Module::OpenSSL::Client:NClientSSL
#include "NLib_Module_OpenSSL_Client_NClientSSL.h"

// namespace NLib::Module::OpenSSL::Client::Thread
#include "Thread/NLib_Module_OpenSSL_Client_Thread.h"
#endif // NLIB_MODULE_RESEAU && NLIB_MODULE_OPENSSL

#endif // !NLIB_MODULE_OPENSSL_CLIENT_PROTECT

