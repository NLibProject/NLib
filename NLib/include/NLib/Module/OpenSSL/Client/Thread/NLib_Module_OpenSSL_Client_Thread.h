#ifndef NLIB_MODULE_OPENSSL_CLIENT_THREAD_PROTECT
#define NLIB_MODULE_OPENSSL_CLIENT_THREAD_PROTECT

// -----------------------------------------------
// namespace NLib::Module::OpenSSL::Client::Thread
// -----------------------------------------------

#if defined( NLIB_MODULE_RESEAU ) && defined( NLIB_MODULE_OPENSSL )
/* Emission */
void NLib_Module_OpenSSL_Client_Thread_Emission( NClientSSL* );

/* Reception */
void NLib_Module_OpenSSL_Client_Thread_Reception( NClientSSL* );
#endif // NLIB_MODULE_RESEAU && NLIB_MODULE_OPENSSL

#endif // !NLIB_MODULE_OPENSSL_CLIENT_THREAD_PROTECT

