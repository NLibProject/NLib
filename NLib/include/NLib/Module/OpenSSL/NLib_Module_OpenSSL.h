#ifndef NLIB_MODULE_OPENSSL_PROTECT
#define NLIB_MODULE_OPENSSL_PROTECT

// -------------------------------
// namespace NLib::Module::OpenSSL
// -------------------------------

#ifdef NLIB_MODULE_OPENSSL
// OpenSSL headers
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

// namespace NLib::Module::OpenSSL::PacketIO
#include "PacketIO/NLib_Module_OpenSSL_PacketIO.h"

// namespace NLib::Module::OpenSSL::Client
#include "Client/NLib_Module_OpenSSL_Client.h"

/**
 * Initialiser
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_OpenSSL_Initialiser( void );

/**
 * Detruire
 */
void NLib_Module_OpenSSL_Detruire( void );
#endif // NLIB_MODULE_OPENSSL

#endif // !NLIB_MODULE_OPENSSL_PROTECT

