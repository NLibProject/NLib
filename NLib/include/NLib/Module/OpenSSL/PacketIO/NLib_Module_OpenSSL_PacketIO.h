#ifndef NLIB_MODULE_OPENSSL_PACKETIO_PROTECT
#define NLIB_MODULE_OPENSSL_PACKETIO_PROTECT

// -----------------------------------------
// namespace NLib::Module::OpenSSL::PacketIO
// -----------------------------------------

#if defined( NLIB_MODULE_OPENSSL ) && defined( NLIB_MODULE_RESEAU )
// Recevoir un packet
__ALLOC NPacket *NLib_Module_OpenSSL_PacketIO_RecevoirPacket( SSL*,
	NBOOL estTimeoutAutorise );

// Envoyer un packet
NBOOL NLib_Module_OpenSSL_PacketIO_EnvoyerPacket( const NPacket*,
	SSL* );
#endif // NLIB_MODULE_OPENSSL && NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_OPENSSL_PACKETIO_PROTECT

