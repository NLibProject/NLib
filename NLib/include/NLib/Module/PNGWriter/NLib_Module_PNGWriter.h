#ifndef NLIB_MODULE_PNGWRITER_PROTECT
#define NLIB_MODULE_PNGWRITER_PROTECT

#ifdef NLIB_MODULE_PNGWRITER

// ---------------------------------
// namespace NLib::Module::PNGWriter
// ---------------------------------

// libpng
#include <png.h>

/**
 * Source:
 *		https://www.lemoda.net/c/write-png/
 */

 /**
 * Ecrire tableau de pixels vers PNG
 *
 * @param pixels
 *		Le tableau brut en entree
 * @param w
 *		La taille taille horizontale
 * @param h
 *		La taille verticale
 * @param bpp
 *		Nombre de byte par pixel
 * @param path
 *		Le chemin de sauvegarde
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_PNGWriter_Inscrire( const char *pixels,
	NU32 w,
	NU32 y,
	NU32 bpp,
	const char *path );

#endif // NLIB_MODULE_PNGWRITER

#endif // !NLIB_MODULE_PNGWRITER_PROTECT

