#ifndef NLIB_MODULE_REPERTOIRE_PROTECT
#define NLIB_MODULE_REPERTOIRE_PROTECT

/*
	Module repertoire

	@author SOARES Lucas
*/

// ----------------------------------
// namespace NLib::Module::Repertoire
// ----------------------------------

#ifdef NLIB_MODULE_REPERTOIRE

// Repertoires
#ifdef IS_WINDOWS
#include <direct.h>
#include <io.h>
#else // IS_WINDOWS
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#endif // !IS_WINDOWS

// namespace NLib::Module::Repertoire::Element
#include "Element/NLib_Module_Repertoire_Element.h"

// struct NLib::Module::Repertoire::NRepertoire
#include "NLib_Module_Repertoire_NRepertoire.h"

/* Codes erreur */
static const NS32 NLIB_ERREUR_REPERTOIRE = -1;
static const NS32 NLIB_ERREUR_RECHERCHE_REPERTOIRE = -1L;

/* Initialiser */
NBOOL NLib_Module_Repertoire_Initialiser( void );

/* Detruire */
void NLib_Module_Repertoire_Detruire( void );

/**
 * Creer un repertoire
 *
 * @param rep
 *		Le repertoire a creer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Repertoire_CreerRepertoire( const char *rep );

/* Changer repertoire courant */
NBOOL NLib_Module_Repertoire_Changer( const char* );

// Restaurer repertoire
NBOOL NLib_Module_Repertoire_RestaurerInitial( void );

/* Obtenir repertoire courant */
__ALLOC char *NLib_Module_Repertoire_ObtenirCourant( void );

/* Obtenir repertoire initial */
const char *NLib_Module_Repertoire_ObtenirInitial( void );

/* Obtention chemin absolu element */
__ALLOC char *NLib_Module_Repertoire_ObtenirCheminAbsolu( const char* );

/* Compter elements */
NU32 NLib_Module_Repertoire_ObtenirNombreElements( const char *filtre /* = "*" */,
	NAttributRepertoire attribut );

/**
 * Il s'agit d'un repertoire?
 *
 * @param lien
 * 		Le lien vers le fichier
 *
 * @return si il s'agit d'un repertoire
 */
NBOOL NLib_Module_Repertoire_EstRepertoire( const char *lien );

// Repertoire initial
#ifdef NLIB_MODULE_REPERTOIRE_INTERNE
static char *m_repertoireInitial = NULL;
#endif // NLIB_MODULE_REPERTOIRE_INTERNE

/* Fonctions/types dependants de l'OS */
#ifdef IS_WINDOWS
// Parcours
#define CHDIR		_chdir
#define GETCWD		_getcwd

// Recherche
#define FINDFIRST	_findfirst
#define FINDNEXT	_findnext
#define FINDCLOSE	_findclose
#define FIND_DATA	struct _finddata64i32_t
#else // IS_WINDOW
// Parcours
#define CHDIR		chdir
#define GETCWD		getcwd
#endif // !IS_WINDOWS

#endif // NLIB_MODULE_REPERTOIRE

// Path max
#ifndef IS_WINDOWS
#define MAX_PATH	PATH_MAX
#endif // !IS_WINDOWS

#endif // !NLIB_MODULE_REPERTOIRE_PROTECT

