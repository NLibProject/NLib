#ifndef NLIB_MODULE_REPERTOIRE_ELEMENT_NATTRIBUTREPERTOIRE_PROTECT
#define NLIB_MODULE_REPERTOIRE_ELEMENT_NATTRIBUTREPERTOIRE_PROTECT

/*
	Les differents attibuts d'un fichier dans un repertoire

	@author SOARES Lucas
*/

// -----------------------------------------------------------
// enum NLib::Module::Repertoire::Element::NAttributRepertoire
// -----------------------------------------------------------

#ifdef NLIB_MODULE_REPERTOIRE

// enum NLib::Module::Repertoire::Element::NAttributRepertoire
typedef enum
{
#ifdef IS_WINDOWS
	NATTRIBUT_REPERTOIRE_NORMAL			= _A_NORMAL,	/* Normal file - No read/write restrictions */
	NATTRIBUT_REPERTOIRE_LECTURE_SEULE	= _A_RDONLY,	/* Read only file */
	NATTRIBUT_REPERTOIRE_CACHE			= _A_HIDDEN,	/* Hidden file */
	NATTRIBUT_REPERTOIRE_SYSTEME		= _A_SYSTEM,	/* System file */
	NATTRIBUT_REPERTOIRE				= _A_SUBDIR,	/* Subdirectory */
	NATTRIBUT_REPERTOIRE_ARCHIVE		= _A_ARCH		/* Archive file */
#else // IS_WINDOWS
	NATTRIBUT_REPERTOIRE_NORMAL			= DT_REG,
	NATTRIBUT_REPERTOIRE				= DT_DIR,
	NATTRIBUT_REPERTOIRE_DEVICE			= DT_CHR | DT_BLK
#endif // !IS_WINDOWS
} NAttributRepertoire;

// Verifier la taille
nassert( sizeof( NAttributRepertoire ) == sizeof( NU32 ),
	"NLib::Module::Repertoire::Element::NAttributRepertoire" );

#endif // NLIB_MODULE_REPERTOIRE

#endif // !NLIB_MODULE_REPERTOIRE_ELEMENT_NATTRIBUTREPERTOIRE_PROTECT

