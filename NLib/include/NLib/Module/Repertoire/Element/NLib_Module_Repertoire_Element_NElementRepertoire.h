#ifndef NLIB_MODULE_REPERTOIRE_ELEMENT_NELEMENTREPERTOIRE_PROTECT
#define NLIB_MODULE_REPERTOIRE_ELEMENT_NELEMENTREPERTOIRE_PROTECT

/*
	Un element de l'objet de NRepertoire

	@author SOARES Lucas
*/

// ------------------------------------------------------------
// struct NLib::Module::Repertoire::Element::NElementRepertoire
// ------------------------------------------------------------

#ifdef NLIB_MODULE_REPERTOIRE
// struct NLib::Module::Repertoire::Element::NElementRepertoire
typedef struct
{
	// Nom
	char *m_nom;

	// Chemin absolu
	char *m_cheminAbsolu;

	// Taille
	NU32 m_taille;

	// Date
		// Creation
			NU64 m_dateCreation;
		// Modification
			NU64 m_dateModification;
		// Dernier acces
			NU64 m_dateDernierAcces;

	// Attributs
	NAttributRepertoire m_attribut;
} NElementRepertoire;

/* Construire l'element */
__ALLOC NElementRepertoire *NLib_Module_Repertoire_Element_NElementRepertoire_Construire( const char *nom,
	NU32 tailleFichier,
	NAttributRepertoire,
	NU64 dateCreation,
	NU64 dateModification,
	NU64 dateAcces );

/* Detruire element */
void NLib_Module_Repertoire_Element_NElementRepertoire_Detruire( NElementRepertoire** );

/* Obtenir nom */
const char *NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirNom( const NElementRepertoire* );

/* Obtenir chemin absolu vers le fichier */
const char *NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirCheminAbsolu( const NElementRepertoire* );

/* Obtenir taille */
NU32 NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirTaille( const NElementRepertoire* ) ;

/* Obtenir date creation */
NU64 NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirDateCreation( const NElementRepertoire* );

/* Obtenir date modification */
NU64 NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirDateModification( const NElementRepertoire* );

/* Obtenir date acces */
NU64 NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirDateDernierAcces( const NElementRepertoire* );

/* Obtenir attributs */
NAttributRepertoire NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirAttributs( const NElementRepertoire* );

/* Est un repertoire? */
NBOOL NLib_Module_Repertoire_Element_NElementRepertoire_EstRepertoire( const NElementRepertoire* );

/* Est un fichier? */
NBOOL NLib_Module_Repertoire_Element_NElementRepertoire_EstFichier( const NElementRepertoire* );


#endif // NLIB_MODULE_REPERTOIRE

#endif // !NLIB_MODULE_REPERTOIRE_ELEMENT_NELEMENTREPERTOIRE_PROTECT

