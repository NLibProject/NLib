#ifndef NLIB_MODULE_MPEGWRITER_NMPEGWRITER_PROTECT
#define NLIB_MODULE_MPEGWRITER_NMPEGWRITER_PROTECT

#ifdef NLIB_MODULE_MPEGWRITER

// --------------------------------------------
// struct NLib::Module::MPEGWriter::NMPEGWriter
// --------------------------------------------

typedef struct NMPEGWriter
{
	// Fichier
	FILE *m_fichier;

	// Nom fichier
	char *m_nom;

	// Derniere position avant inscription
	NU64 m_dernierePositionAvantEcriture;
} NMPEGWriter;

/**
 * Construire le writer
 *
 * @param nom
 *		Le nom du fichier dans lequel ecrire
 * @param estEcraserContenu
 *		On ecrase le contenu precedent du fichier?
 *
 * @return l'instance du writer
 */
__ALLOC NMPEGWriter *NLib_Module_MPEGWriter_NMPEGWriter_Construire( const char *nom,
	NBOOL estEcraserContenu );

/**
 * Detruire
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_MPEGWriter_NMPEGWriter_Detruire( NMPEGWriter** );

/**
 * Inscrire une frame
 *
 * @param this
 *		Cette instance
 * @param rgb
 * 		La frame a inscrire
 * @param x
 * 		La taille horizontale de la frame
 * @param y
 * 		La taille verticale de la frame
 * @param fps
 * 		Le nombre de frame par seconde
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_MPEGWriter_NMPEGWriter_Inscrire( NMPEGWriter*,
	const NU8 *rgb,
	NU32 x,
	NU32 y,
	NU32 fps );

/**
 * Inscrire une frame
 *
 * @param this
 *		Cette instance
 * @param frame
 *		La frame a inscrire
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_MPEGWriter_NMPEGWriter_Inscrire2( NMPEGWriter*,
	const NFrameMPEG *frame );

/**
 * Obtenir la taille du fichier (octets)
 *
 * @param this
 *		Cette instance
 *
 * @return la taille du fichier de capture
 */
NU64 NLib_Module_MPEGWriter_NMPEGWriter_ObtenirTaille( const NMPEGWriter* );

/**
 * Obtenir derniere frame inscrite
 *
 * @param this
 *		Cette instance
 *
 * @return la derniere frame inscrite
 */
__ALLOC NFrameMPEG *NLib_Module_MPEGWriter_NMPEGWriter_ObtenirDerniereFrame( const NMPEGWriter* );

/**
 * Obtenir instance fichier
 *
 * @param this
 *		Cette instance
 *
 * @return le fichier
 */
FILE *NLib_Module_MPEGWriter_NMPEGWriter_ObtenirFichier( const NMPEGWriter* );

/**
 * Vider le contenu du fichier
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_MPEGWriter_NMPEGWriter_ViderContenu( NMPEGWriter* );

#endif // NLIB_MODULE_MPEGWRITER

#endif // !NLIB_MODULE_MPEGWRITER_NMPEGWRITER_PROTECT

