#ifndef NLIB_MODULE_MPEGWRITER_NFRAMEMPEG_PROTECT
#define NLIB_MODULE_MPEGWRITER_NFRAMEMPEG_PROTECT

// -------------------------------------------
// struct NLib::Module::MPEGWriter::NFrameMPEG
// -------------------------------------------

typedef struct NFrameMPEG
{
    // Donnees
    char *m_data;

    // Taille des donnees
    NU64 m_taille;
} NFrameMPEG;

/**
 * Construire la frame
 *
 * @param data
 *		Les donnees
 * @param taille
 *		La taille
 *
 * @return l'instance de la frame
 */
__ALLOC NFrameMPEG *NLib_Module_MPEGWriter_NFrameMPEG_Construire( __WILLBEOWNED char *data,
	NU32 taille );

/**
 * Construire la frame a partir d'un fichier mpeg
 *
 * @param fichier
 *		Le fichier source
 * @param taille
 *		La taille de la frame a lire
 *
 * @return l'instance de la frame
 */
__ALLOC NFrameMPEG *NLib_Module_MPEGWriter_NFrameMPEG_Construire2( FILE *fichier,
	NU64 taille );

/**
 * Detruire la frame
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_MPEGWriter_NFrameMPEG_Detruire( NFrameMPEG** );

/**
 * Obtenir les donnees
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees
 */
const char *NLib_Module_MPEGWriter_NFrameMPEG_ObtenirData( const NFrameMPEG* );

/**
 * Obtenir la taille
 *
 * @param this
 *		Cette instance
 *
 * @return la taille
 */
NU64 NLib_Module_MPEGWriter_NFrameMPEG_ObtenirTaille( const NFrameMPEG* );

#endif // !NLIB_MODULE_MPEGWRITER_NFRAMEMPEG_PROTECT

