#ifndef NLIB_MODULE_MPEGWRITER_PROTECT
#define NLIB_MODULE_MPEGWRITER_PROTECT

#ifdef NLIB_MODULE_MPEGWRITER

/**
 * Supporte les frequences
 * 24, 25, 30, 50 et 60 fps
 *
 * @author http://jonolick.com/
 */

// ----------------------------------
// namespace NLib::Module::MPEGWriter
// ----------------------------------

// Math
#include <math.h>

// struct NLib::Module::MPEGWriter::NFrameMPEG
#include "NLib_Module_MPEGWriter_NFrameMPEG.h"

// struct NLib::Module::MPEGWriter::NMPEGWriter
#include "NLib_Module_MPEGWriter_NMPEGWriter.h"

/**
 * Inscrire une frame
 *
 * @param fp
 * 		Le descripteur de fichier (wb)
 * @param rgb
 * 		La frame a inscrire
 * @param width
 * 		La taille horizontale de la frame
 * @param height
 * 		La taille verticale de la frame
 * @param fps
 * 		Le nombre de frame par seconde
 */
void NLib_Module_MPEGWriter_EcrireFrame( FILE *fp,
	const NU8 *rgb,
	NU32 width,
	NU32 height,
	NU32 fps );

/**
 * Creer une frame purement en memoire
 *
 * @param rgb
 * 		La frame a inscrire
 * @param width
 * 		La taille horizontale de la frame
 * @param height
 * 		La taille verticale de la frame
 * @param fps
 * 		Le nombre de frame par seconde
 *
 * @return la frame creee
 */
__ALLOC NFrameMPEG *NLib_Module_MPEGWriter_CreerFrame( const NU8 *rgb,
	NU32 width,
	NU32 height,
	NU32 fps );

/**
 * Creer une frame en memoire a l'aide d'un buffer
 *
 * @param rgb
 * 		La frame a inscrire où x est unused
 * @param width
 * 		La taille horizontale de la frame
 * @param height
 * 		La taille verticale de la frame
 * @param fps
 * 		Le nombre de frame par seconde
 *
 * @return la frame creee
 */
__ALLOC NFrameMPEG *NLib_Module_MPEGWriter_CreerFrame2( const NU8 *rgb,
	NU32 width,
	NU32 height,
	NU32 fps );

/**
 * Generer rgbx depuis rgb
 *
 * @param rgb
 *		Le tableau de pixels au format RGB
 * @param x
 *		La taille x
 * @param y
 *		La taille y
 *
 * @return le tableau de pixels au format RGBX
 */
__ALLOC NU8 *NLib_Module_MPEGWriter_ConvertirRGBVersRGBX( const NU8 *rgb,
	NU32 x,
	NU32 y );

#endif // NLIB_MODULE_MPEGWRITER

#endif // !NLIB_MODULE_MPEGWRITER_PROTECT

