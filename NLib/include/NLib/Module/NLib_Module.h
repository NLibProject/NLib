#ifndef NLIB_MODULE_PROTECT
#define NLIB_MODULE_PROTECT

// ----------------------
// namespace NLib::Module
// ----------------------

// namespace NLib::Module::Reseau
#include "Reseau/NLib_Module_Reseau.h"

// namespace NLib::Module::SSH
#include "SSH/NLib_Module_SSH.h"

// namespace NLib::Module::SDL
#include "SDL/NLib_Module_SDL.h"

// namespace NLib::Module::Repertoire
#include "Repertoire/NLib_Module_Repertoire.h"

// namespace NLib::Module::FModex
#include "FModex/NLib_Module_FModex.h"

// namespace NLib::Module::OpenSSL
#include "OpenSSL/NLib_Module_OpenSSL.h"

// namespace NLib::Module::Webcam
#include "Webcam/NLib_Module_Webcam.h"

// namespace NLib::Module::NMPEGWriter
#include "MPEGWriter/NLib_Module_MPEGWriter.h"

// namespace NLib::Module::PNGWriter
#include "PNGWriter/NLib_Module_PNGWriter.h"

// namespace NLib::Module::Serial
#include "Serial/NLib_Module_Serial.h"

// enum NLib::Module::NEtapeModule
#include "NLib_Module_NEtapeModule.h"

/* Initialiser les modules */
NBOOL NLib_Module_Initialiser( void );

/* Detruire les modules */
void NLib_Module_Detruire( void );

#endif // !NLIB_MODULE_PROTECT

