#ifndef NLIB_MODULE_WEBCAM_NIMAGECAMERA_PROTECT
#define NLIB_MODULE_WEBCAM_NIMAGECAMERA_PROTECT

#ifdef NLIB_MODULE_WEBCAM

// -----------------------------------------
// struct NLib::Module::Webcam::NImageCamera
// -----------------------------------------

typedef struct NImageCamera
{
	// Donnees
	void *m_data;

	// Taille
	NU32 m_taille;

	// Dimension
	NUPoint m_dimension;

	// Nombre de pixels
	NU32 m_nombreOctetParPixel;
} NImageCamera;

/**
 * Construire une image
 *
 * @param data
 *		Les donnees de l'image
 * @param taille
 *		La taille des donnees
 * @param w
 *		La taille horizontale de l'image
 * @param h
 *		La taille verticale de l'image
 * @param nombreOctetParPixel
 *		Nombre d'octets par pixel
 *
 * @return l'instance de l'image creee
 */
__ALLOC NImageCamera *NLib_Module_Webcam_NImageCamera_Construire( const char *data,
	NU32 taille,
	NU32 w,
	NU32 h,
	NU32 nombreOctetParPixel );

/**
 * Detruire une image
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Webcam_NImageCamera_Detruire( NImageCamera** );

/**
 * Enregistrer une image au format PCM
 *
 * @param this
 * 		Cette instance
 * @param lien
 * 		Le lien d'enregistrement de l'image
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Webcam_NImageCamera_Enregistrer( const NImageCamera*,
	const char *lien );

/**
 * Obtenir taille en octets
 *
 * @param this
 *		Cette instance
 *
 * @return la taille en octets
 */
NU32 NLib_Module_Webcam_NImageCamera_ObtenirTaille( const NImageCamera* );

/**
 * Obtenir resolution
 *
 * @param this
 *		Cette instance
 *
 * @return la resolution
 */
const NUPoint *NLib_Module_Webcam_NImageCamera_ObtenirDimension( const NImageCamera* );

/**
 * Obtenir octets par pixels
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre d'octets par pixel
 */
NU32 NLib_Module_Webcam_NImageCamera_ObtenirOctetParPixel( const NImageCamera* );

/**
 * Obtenir data
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees
 */
const char *NLib_Module_Webcam_NImageCamera_ObtenirData( const NImageCamera* );

#endif // NLIB_MODULE_WEBCAM_NIMAGECAMERA_PROTECT

#endif // !IMAGE_PROTECT

