#ifndef NLIB_MODULE_WEBCAM_PROTECT
#define NLIB_MODULE_WEBCAM_PROTECT

#ifdef NLIB_MODULE_WEBCAM

// ------------------------------
// namespace NLib::Module::Webcam
// ------------------------------

// struct NLib::Module::Webcam::NImageCamera
#include "NLib_Module_Webcam_NImageCamera.h"

// struct NLib::Module::Webcam::NCamera
#include "NLib_Module_Webcam_NCamera.h"

/**
 * Effectue une requete IOCTL
 *
 * @param handle
 * 		Handle v4l2
 * @param request
 * 		Le contrôle a effectuer
 * @param arg
 * 		L'argument a transmettre
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Webcam_xioctl( int handle,
	int request,
	void *arg );

#endif // NLIB_MODULE_WEBCAM

#endif // !NLIB_MODULE_WEBCAM_PROTECT

