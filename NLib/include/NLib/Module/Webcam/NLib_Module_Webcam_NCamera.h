#ifndef NLIB_MODULE_WEBCAM_NCAMERA_PROTECT
#define NLIB_MODULE_WEBCAM_NCAMERA_PROTECT

#ifdef NLIB_MODULE_WEBCAM
// ------------------------------------
// struct NLib::Module::Webcam::NCamera
// ------------------------------------

// Headers standards
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <libv4l2.h>

// Nombre de buffer
#define NLIB_MODULE_WEBCAM_NOMBRE_BUFFER	4

typedef struct NCamera
{
	// Descripteur de fichier webcam
	NS32 m_descripteurWebcam;

	// Buffers accordes a la lecture d'image
	NImageCamera *m_buffer;

	// Nombre de buffers alloues a la lecture d'image
	unsigned int m_nombreBuffer;

	// Taille de la capture
	NUPoint m_tailleCapture;

	// Taille format
	NU32 m_tailleFormat;
} NCamera;

/**
 * Construire la camera
 *
 * @param dev
 * 		Le device a utiliser
 * @param w
 * 		La resolution horizontale
 * @param h
 * 		La resolution verticale
 *
 * @return l'instance de la camera
 */
 __ALLOC NCamera *NLib_Module_Webcam_NCamera_Construire( const char *dev,
	NU32 w,
	NU32 h );

/**
 * Detruire la camera
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Webcam_NCamera_Detruire( NCamera** );

/**
 * Lire une image sur la webcam
 *
 * @param this
 * 		Cette instance
 *
 * @return l'image
 */
__ALLOC NImageCamera *NLib_Module_Webcam_NCamera_Capturer( NCamera* );

/**
 * Obtenir resolution capture
 *
 * @param this
 *		Cette instance
 *
 * @return la resolution de la capture
 */
const NUPoint *NLib_Module_Webcam_NCamera_ObtenirResolution( const NCamera* );

/**
 * Obtenir nombre d'octets par pixel
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre d'octets par pixel
 */
NU32 NLib_Module_Webcam_NCamera_ObtenirNombreOctetParPixel( const NCamera* );

#endif // NLIB_MODULE_WEBCAM

#endif // !NLIB_MODULE_WEBCAM_NCAMERA_PROTECT

