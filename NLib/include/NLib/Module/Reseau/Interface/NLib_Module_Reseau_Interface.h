#ifndef NLIB_MODULE_RESEAU_INTERFACE_PROTECT
#define NLIB_MODULE_RESEAU_INTERFACE_PROTECT

#ifdef NLIB_MODULE_RESEAU
// -----------------------------------------
// namespace NLib::Module::Reseau::Interface
// -----------------------------------------

#ifdef IS_WINDOWS

#else // IS_WINDOWS
#include <ifaddrs.h>
#include <linux/if_packet.h>
#include <net/ethernet.h> /* the L2 protocols */
#endif // !IS_WINDOWS

// struct NLib::Module::Reseau::Interface::NInterfaceReseau
#include "NLib_Module_Reseau_Interface_NInterfaceReseau.h"

/**
 * Trouver toutes les interfaces reseaux
 *
 * @return la liste des interfaces reseaux (NListe<NInterfaceReseau*>)
 */
__ALLOC NListe *NLib_Module_Reseau_Interface_TrouverTouteInterface( void );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_INTERFACE_PROTECT
