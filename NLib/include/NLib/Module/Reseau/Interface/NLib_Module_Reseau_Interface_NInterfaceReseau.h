#ifndef NLIB_MODULE_RESEAU_INTERFACE_NINTERFACERESEAU_PROTECT
#define NLIB_MODULE_RESEAU_INTERFACE_NINTERFACERESEAU_PROTECT

#ifdef NLIB_MODULE_RESEAU

// --------------------------------------------------------
// struct NLib::Module::Reseau::Interface::NInterfaceReseau
// --------------------------------------------------------

typedef struct NInterfaceReseau
{
	// Nom
	char *m_nom;

	// Adresse IP
	char *m_adresse;

	// CIDR
	NU32 m_cidr;

	// Adresse mac
	char *m_adresseMAC;
} NInterfaceReseau;

/**
 * Construire une interface reseau
 *
 * @param nom
 * 		Le nom de l'interface
 * @param adresse
 * 		L'adresse de l'interface
 * @param cidr
 * 		Le cidr de l'adresse
 * @param adresseMAC
 * 		L'adresse mac
 *
 * @return l'instance de l'interface
 */
__ALLOC NInterfaceReseau *NLib_Module_Reseau_Interface_NInterfaceReseau_Construire( const char *nom,
	const char *adresse,
	NU32 cidr,
	const char *adresseMAC );

/**
 * Dupliquer une interface reseau
 *
 * @param src
 * 		L'interface a dupliquer
 *
 * @return l'instance de l'interface
 */
__ALLOC NInterfaceReseau *NLib_Module_Reseau_Interface_NInterfaceReseau_Construire2( const NInterfaceReseau *src );

/**
 * Detruire l'interface
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Interface_NInterfaceReseau_Detruire( NInterfaceReseau** );

/**
 * Obtenir nom
 *
 * @param this
 * 		Cette instance
 *
 * @return le nom de l'interface
 */
const char *NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirNom( const NInterfaceReseau* );

/**
 * Obtenir adresse
 *
 * @param this
 * 		Cette instance
 *
 * @return l'adresse de l'interface
 */
const char *NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirAdresse( const NInterfaceReseau* );

/**
 * Obtenir le CIDR de l'adresse
 *
 * @param this
 * 		Cette instance
 *
 * @return le CIDR de l'adresse
 */
NU32 NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirCIDR( const NInterfaceReseau* );

/**
 * Definir adresse mac
 *
 * @param this
 * 		Cette instance
 * @param adresseMAC
 * 		L'adresse mac a definir
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Interface_NInterfaceReseau_DefinirAdresseMAC( NInterfaceReseau*,
	const char *adresseMAC );

/**
 * Obtenir adresse mac
 *
 * @param this
 * 		Cette instance
 *
 * @return l'adresse MAC ou NULL si non definie
 */
const char *NLib_Module_Reseau_Interface_NInterfaceReseau_ObtenirAdresseMAC( const NInterfaceReseau* );

#endif // NLIB_MODULE_RESEAU


#endif // !NLIB_MODULE_RESEAU_INTERFACE_NINTERFACERESEAU_PROTECT
