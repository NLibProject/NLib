#ifndef NLIB_MODULE_RESEAU_PROTECT
#define NLIB_MODULE_RESEAU_PROTECT

/**
 *	Module reseau
 *
 *	Sous windows, a l'initialisation du module, appel
 *	de WSAStartup
 *
 *	@author SOARES Lucas
 */

// ------------------------------
// namespace NLib::Module::Reseau
// ------------------------------

#ifdef NLIB_MODULE_RESEAU

// Headers pour windows
#ifdef IS_WINDOWS
#include <WinSock2.h>
#include <WS2tcpip.h>

// Suffixe SOCKADDR_IN
#define FIN_ADRESSE		S_un.S_addr
#else // IS_WINDOWS
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#endif // !NLIB_MODULE_RESEAU_BLUETOOTH

// Definir le type socket
typedef NU32 SOCKET;

// Redefinir SOCKADDR
typedef struct sockaddr_in SOCKADDR_IN;

// Socket invalide
#define INVALID_SOCKET		( (SOCKET)( -1 ) )

// Code retour erreur socket
#define SOCKET_ERROR		( (SOCKET)( -1 ) )

// Fermer la socket
#define closesocket( s ) \
	close( s )

// Suffixe SOCKADDR_IN
#define FIN_ADRESSE		s_addr
#endif // !IS_WINDOWS

// Adresse loopback
#define NLIB_ADRESSE_IP_LOCALE						"127.0.0.1"

// Taille maximale d'une chaine IP (XXX:XXX:XXX:XXX)
#define NLIB_TAILLE_MAXIMALE_CHAINE_IP				15

// Taille maximale d'une chaine IP accompagnee du port (XXX.XXX.XXX.XXX:XXXXX)
#define NLIB_TAILLE_MAXIMALE_CHAINE_IP_PORT			21

// Taille d'une adresse mac [XX:XX:XX:XX:XX:XX]
#define NTAILLE_ADRESSE_MAC							17

// Nombre de separateurs dans une adresse mac
#define NNOMBRE_SEPARATEUR_ADRESSE_MAC				5

// Delai avant d'etre timeout pour le ping
#define NDUREE_AVANT_TIMEOUT_PING					2000

// Delai avant nouvelle demande ping
#define NDELAI_ENTRE_REQUETE_PING					5000

/**
 * Initialiser le reseau
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Reseau_Initialiser( void );

/**
 * Detruire le reseau
 */
void NLib_Module_Reseau_Detruire( void );

/**
 * Calculer le nombre d'ip dans une plage cidr donnee
 *
 * @param cidr
 * 		Le cidr
 *
 * @return le nombre d'ip
 */
NU32 NLib_Module_Reseau_CalculerNombreIPDansCIDR( NU32 cidr );

// Etat reseau
#ifdef RESEAU_INTERNE
#ifdef IS_WINDOWS
// Contexte WSA pour windows uniquement
static WSADATA m_contexteWSA;
#endif // IS_WINDOWS
#endif // RESEAU_INTERNE

// namespace NLib::Module::Reseau::IP
#include "IP/NLib_Module_Reseau_IP.h"

// namespace NLib::Module::Reseau::Resolution
#include "Resolution/NLib_Module_Reseau_Resolution.h"

// struct NLib::Module::Reseau::NPing
#include "NLib_Module_Reseau_NPing.h"

// namespace NLib::Module::Reseau::Packet
#include "Packet/NLib_Module_Reseau_Packet.h"

// namespace NLib::Module::Reseau::Serveur
#include "Serveur/NLib_Module_Reseau_Serveur.h"

// namespace NLib::Module::Reseau::Socket
#include "Socket/NLib_Module_Reseau_Socket.h"

// namespace NLib::Module::Reseau::Client
#include "Client/NLib_Module_Reseau_Client.h"

// namespace NLib::Module::Reseau::UDP
#include "UDP/NLib_Module_Reseau_UDP.h"

// namespace NLib::Module::Reseau::Interface
#include "Interface/NLib_Module_Reseau_Interface.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PROTECT

