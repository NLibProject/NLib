#ifndef NLIB_MODULE_RESEAU_PACKET_NCACHEPACKET_PROTECT
#define NLIB_MODULE_RESEAU_PACKET_NCACHEPACKET_PROTECT

/*
	Definition du cache packet, accueillants les packets
	a transmettre sous la forme d'une file

	@author SOARES Lucas
*/

// -------------------------------------------------
// struct NLib::Module::Reseau::Packet::NCachePacket
// -------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// struct NCachePacket
typedef struct
{
	// Liste packet
	NListe *m_packet;

	// Type de cache
	NTypeCachePacket m_type;

	// Callbacks
		// Copie
			__ALLOC void *( ___cdecl *m_callbackCopie )( const void* );
		// Destruction
			void ( ___cdecl *m_callbackDestruction )( void* );
} NCachePacket;

// Construire cache packet
__ALLOC NCachePacket *NLib_Module_Reseau_Packet_NCachePacket_Construire( NTypeCachePacket );

// Detruire cache packet
void NLib_Module_Reseau_Packet_NCachePacket_Detruire( NCachePacket** );

// Vider cache
void NLib_Module_Reseau_Packet_NCachePacket_Vider( NCachePacket* );

// Obtenir le nombre de packets
NU32 NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( const NCachePacket* );

// Obtenir le packet a envoyer
__ALLOC void *NLib_Module_Reseau_Packet_NCachePacket_ObtenirPacket( const NCachePacket* );

// Est packet(s) dans le cache?
NBOOL NLib_Module_Reseau_Packet_NCachePacket_EstPacketsDansCache( const NCachePacket* );

// Ajouter un packet
NBOOL NLib_Module_Reseau_Packet_NCachePacket_AjouterPacket( NCachePacket*,
	__WILLBEOWNED void* );

// Ajouter une copie de packet
NBOOL NLib_Module_Reseau_Packet_NCachePacket_AjouterPacketCopie( NCachePacket*,
	const void* );

// Supprimer le packet
NBOOL NLib_Module_Reseau_Packet_NCachePacket_SupprimerPacket( NCachePacket* );

/**
 * Obtenir le type de cache
 *
 * @param this
 *		Cette instance
 *
 * @return le type de cache
 */
NTypeCachePacket NLib_Module_Reseau_Packet_NCachePacket_ObtenirTypeCache( const NCachePacket* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PACKET_NCACHEPACKET_PROTECT



