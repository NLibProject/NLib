#ifndef NLIB_MODULE_RESEAU_PACKET_PACKETIO_PROTECT
#define NLIB_MODULE_RESEAU_PACKET_PACKETIO_PROTECT

/**
 *	Gestion de la transmission (recv/send), avec bufferisation
 *	par tranche de NBUFFER_PACKET (version securisee transfert)
 *
 *	Inclusion systematique de NHEADER_BUFFER et du CRC32 pour
 *	les packets CONTENU, et de la taille a envoyer pour les p
 *	ackets HEADER
 *
 *	[Packet header]
 *	"NHDRNLIBNPACKETH"+CRC32(4octets)+TailleDesDonnees(4octets)+Donnees(?octets)
 *
 *	[Packet contenu]
 *	"NHDRNLIBNPACKETH"+CRC32(4octets)+Donnees(?octets)
 *
 *	@author SOARES Lucas
 */

// ------------------------------------------------
// namespace NLib::Module::Reseau::Packet::PacketIO
// ------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// Taille minimale pour le buffer
#define NBUFFER_PACKET_MINIMUM				(NU32)24

// Header buffer
#define NHEADER_BUFFER						"NHDRNLIBNPACKETH"
#define NTAILLE_HEADER_BUFFER				16

// Taille buffer fichier
#define NPACKETIO_TAILLE_BUFFER_FICHIER		65535

// Verifier la taille
nassert( ( NBUFFER_PACKET >= NBUFFER_PACKET_MINIMUM ),
	"La taille du buffer est incorrecte." );

// enum NLib::Module::Reseau::Packet::PacketIO::NEtapePacketIO
#include "NLib_Module_Reseau_Packet_PacketIO_NEtapePacketIO.h"

// enum NLib::Module::Reseau::Packet::PacketIO::NMethodeTransmissionPacketIO
#include "NLib_Module_Reseau_Packet_PacketIO_NMethodeTransmissionPacketIO.h"

/**
 * Recevoir packet methode NProject
 *
 * @param socket
 *		La socket sur laquelle on va lire
 * @param unused
 *		Inutilise
 *
 * @return le packet recu
 */
__ALLOC NPacket *NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketNProject( SOCKET socket,
	NBOOL unused );

/**
 * Envoyer packet methode NProject
 *
 * @param packet
 *		Le packet a envoyer
 * @param socket
 *		La socket sur laquelle envoyer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketNProject( const void *packet,
	SOCKET socket );

/**
 * Recevoir packet methode Raw (+timeout)
 *
 * @param socket
 *		La socket sur laquelle on va lire
 * @param estTimeoutAutorise
 *		On autorise le timeout?
 *
 * @return le packet recu
 */
__ALLOC NPacket *NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketRaw( SOCKET socket,
	NBOOL estTimeoutAutorise );

/**
 * Envoyer data methode raw
 *
 * @param data
 * 		Les donnees
 * @param tailleData
 * 		La taille des donnees
 * @param socket
 * 		La socket sur laquelle envoyer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerDataRaw( const void *data,
	NU32 tailleData,
	SOCKET socket );

/**
 * Envoyer packet methode Raw
 *
 * @param packet
 *		Le packet a envoyer
 * @param socket
 *		La socket sur laquelle envoyer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketRaw( const void *packet,
	SOCKET socket );

/**
 * Envoyer fichier raw
 *
 * @param fichier
 * 		Le fichier binaire
 * @param socket
 * 		La socket sur laquelle envoyer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerFichierRaw( NFichierBinaire *fichier,
	SOCKET socket );

/**
 * Recevoir un packet protocole UDP
 *
 * @param socket
 *		La socket sur laquelle recevoir
 *
 * @return le packet recu
 */
__ALLOC struct NPacketUDP *NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketUDP( SOCKET socket );

/**
 * Envoyer un packet protocole UDP
 *
 * @param packet
 *		Le packet a envoyer
 * @param socket
 *		La socket sur laquelle envoyer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketUDP( const struct NPacketUDP*,
	SOCKET );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PACKET_PACKETIO_PROTECT

