#ifndef NLIB_MODULE_RESEAU_PACKET_PACKETIO_NMETHODETRANSMISSIONPACKETIO_PROTECT
#define NLIB_MODULE_RESEAU_PACKET_PACKETIO_NMETHODETRANSMISSIONPACKETIO_PROTECT

#ifdef NLIB_MODULE_RESEAU

// -------------------------------------------------------------------------
// enum NLib::Module::Reseau::Packet::PacketIO::NMethodeTransmissionPacketIO
// -------------------------------------------------------------------------

typedef enum NMethodeTransmissionPacketIO
{
	NMETHODE_TRANSMISSION_PACKETIO_RAW,

	NMETHODE_TRANSMISSION_PACKETIO_NPROJECT,

	NMETHODES_TRANSMISSION_PACKETIO
} NMethodeTransmissionPacketIO;

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PACKET_PACKETIO_NMETHODETRANSMISSIONPACKETIO_PROTECT

