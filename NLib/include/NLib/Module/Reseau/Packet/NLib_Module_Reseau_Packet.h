#ifndef NLIB_MODULE_RESEAU_PACKET_PROTECT
#define NLIB_MODULE_RESEAU_PACKET_PROTECT

/**
 *	Definition d'un packet
 *
 *	@author SOARES Lucas
 */

// --------------------------------------
// namespace NLib::Module::Reseau::Packet
// --------------------------------------

#ifdef NLIB_MODULE_RESEAU

// struct NLib::Module::Reseau::Packet::NPacket
#include "NLib_Module_Reseau_Packet_NPacket.h"

// struct NLib::Module::Reseau::Packet::NPacketPersonnalise
#include "NLib_Module_Reseau_Packet_NPacketPersonnalise.h"

// enum NLib::Module::Reseau::Packet::NTypeCachePacket
#include "NLib_Module_Reseau_Packet_NTypeCachePacket.h"

// struct NLib::Module::Reseau::Packet::NCachePacket
#include "NLib_Module_Reseau_Packet_NCachePacket.h"

// Taille buffer packet (octets)
#define NBUFFER_PACKET				(NU32)4096

// namespace NLib::Module::Reseau::Packet::PacketIO
#include "PacketIO/NLib_Module_Reseau_Packet_PacketIO.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PACKET_PROTECT

