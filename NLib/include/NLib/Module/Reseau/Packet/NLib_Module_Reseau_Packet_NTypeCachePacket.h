#ifndef NLIB_MODULE_RESEAU_PACKET_NTYPECACHEPACKET_PROTECT
#define NLIB_MODULE_RESEAU_PACKET_NTYPECACHEPACKET_PROTECT

#ifdef NLIB_MODULE_RESEAU
// ---------------------------------------------------
// enum NLib::Module::Reseau::Packet::NTypeCachePacket
// ---------------------------------------------------

typedef enum NTypeCachePacket
{
	NTYPE_CACHE_PACKET_TCP,
	NTYPE_CACHE_PACKET_UDP,

	NTYPE_CACHE_PACKET_PERSONNALISE,

	NTYPES_CACHE_PACKET
} NTypeCachePacket;

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PACKET_NTYPECACHEPACKET_PROTECT

