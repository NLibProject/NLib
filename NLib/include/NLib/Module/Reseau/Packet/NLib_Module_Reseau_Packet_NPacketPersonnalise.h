#ifndef NLIB_MODULE_RESEAU_PACKET_NPACKETPERSONNALISE_PROTECT
#define NLIB_MODULE_RESEAU_PACKET_NPACKETPERSONNALISE_PROTECT

#ifdef NLIB_MODULE_RESEAU

// --------------------------------------------------------
// struct NLib::Module::Reseau::Packet::NPacketPersonnalise
// --------------------------------------------------------

typedef struct NPacketPersonnalise
{
	// Le packet
	NPacket *m_packet;

	// Donnees personnalisees
	void *m_data;

	// Est donnees constantes? (Ne doivent ni etre copiees/Ni detruites)
	NBOOL m_estDataConstante;

	// Callback copie donnees packet
	__ALLOC void *( ___cdecl *m_callbackCopie )( const void* );

	// Callback destruction packet
	void ( ___cdecl *m_callbackDestruction )( void* );
} NPacketPersonnalise;

/**
 * Construire un packet avec des donnees utilisateur copiables/destructibles
 *
 * @param callbackCopie
 *		Le callback utilise pour copier les donnees
 * @param callbackDestruction
 * 		Le callback de destruction des donnees
 * @param data
 *		Les donnees utilisateur
 *
 * @return l'instance
 */
__ALLOC NPacketPersonnalise *NLib_Module_Reseau_Packet_NPacketPersonnalise_Construire( void *( ___cdecl *callbackCopie )( const void* ),
	void ( ___cdecl *callbackDestruction )( void* ),
	__WILLBEOWNED void *data );

/**
 * Construire un packet avec des donnees utilisateur constantes (ni copie ni destruction)
 *
 * @param data
 *		Les donnees utilisateur
 *
 * @return l'instance
 */
__ALLOC NPacketPersonnalise *NLib_Module_Reseau_Packet_NPacketPersonnalise_Construire2( void *data );

/**
 * Construire a partir d'un packet
 *
 * @param packet
 *		Le packet a copier
 *
 * @return la nouvelle instance
 */
__ALLOC NPacketPersonnalise *NLib_Module_Reseau_Packet_NPacketPersonnalise_Construire3( const NPacketPersonnalise* );

/**
 * Detruire un packet
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_Packet_NPacketPersonnalise_Detruire( NPacketPersonnalise** );

/**
 * Obtenir le packet
 *
 * @param this
 *		Cette instance
 *
 * @return le packet
 */
const NPacket *NLib_Module_Reseau_Packet_NPacketPersonnalise_ObtenirPacket( const NPacketPersonnalise* );

/**
 * Ajouter des donnees
 *
 * @param this
 *		Cette instance
 * @param data
 *		Les donnees a ajouter
 * @param tailleData
 *		La taille des donnees a ajouter
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Packet_NPacketPersonnalise_AjouterData( NPacketPersonnalise*,
	const char *data,
	NU32 tailleData );

/**
 * Obtenir donnees
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees utilisateur
 */
void *NLib_Module_Reseau_Packet_NPacketPersonnalise_ObtenirDataPersonnalise( const NPacketPersonnalise* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PACKET_NPACKETPERSONNALISE_PROTECT

