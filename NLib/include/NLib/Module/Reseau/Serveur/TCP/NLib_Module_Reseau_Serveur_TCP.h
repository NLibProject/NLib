#ifndef NLIB_MODULE_RESEAU_SERVEUR_TCP_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_TCP_PROTECT

#ifdef NLIB_MODULE_RESEAU
// --------------------------------------------
// namespace NLib::Module::Reseau::Serveur::TCP
// --------------------------------------------

/**
 * Creer la socket
 *
 * @param port
 *		Le port d'ecoute
 * @param listeningIP
 * 		The listening IP (can be NULL for listening on all interfaces)
 *
 * @return la socket
 */
SOCKET NLib_Module_Reseau_Serveur_TCP_CreerSocket( NU32 port,
	const char *listeningIP );

#endif // !NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_TCP_PROTECT

