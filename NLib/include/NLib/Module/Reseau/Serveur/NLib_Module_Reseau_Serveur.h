#ifndef NLIB_MODULE_RESEAU_SERVEUR_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_PROTECT

/*
	Definition des elements necessaire a un serveur

	@author SOARES Lucas
*/

// ---------------------------------------
// namespace NLib::Module::Reseau::Serveur
// ---------------------------------------

#ifdef NLIB_MODULE_RESEAU

// enum NLib::Module::Reseau::Serveur::NTypeServeur
#include "NLib_Module_Reseau_Serveur_NTypeServeur.h"

// struct NLib::Module::Reseau::Serveur::NClientServeur
#include "NLib_Module_Reseau_Serveur_NClientServeur.h"

// struct NLib::Module::Reseau::Serveur::NServeur
#include "NLib_Module_Reseau_Serveur_NServeur.h"

// namespace NLib::Module::Reseau::Serveur::TCP
#include "TCP/NLib_Module_Reseau_Serveur_TCP.h"

// namespace NLib::Module::Reseau::Serveur::Bluetooth
#include "Bluetooth/NLib_Module_Reseau_Serveur_Bluetooth.h"

// namespace NLib::Module::Reseau::Serveur::Thread
#include "Thread/NLib_Module_Reseau_Serveur_Thread.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_PROTECT

