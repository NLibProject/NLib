#ifndef NLIB_MODULE_RESEAU_SERVEUR_NTYPESERVEUR_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_NTYPESERVEUR_PROTECT

#ifdef NLIB_MODULE_RESEAU

// ------------------------------------------------
// enum NLib::Module::Reseau::Serveur::NTypeServeur
// ------------------------------------------------

typedef enum NTypeServeur
{
	NTYPE_SERVEUR_TCP,
#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
	NTYPE_SERVEUR_BLUETOOTH,
#endif // NLIB_MODULE_RESEAU_BLUETOOTH

	NTYPES_SERVEUR
} NTypeServeur;

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_NTYPESERVEUR_PROTECT

