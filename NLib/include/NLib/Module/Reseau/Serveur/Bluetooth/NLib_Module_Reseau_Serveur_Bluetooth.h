#ifndef NLIB_MODULE_RESEAU_SERVEUR_BLUETOOTH_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_BLUETOOTH_PROTECT

#ifdef NLIB_MODULE_RESEAU

#ifdef NLIB_MODULE_RESEAU_BLUETOOTH

// --------------------------------------------------
// namespace NLib::Module::Reseau::Serveur::Bluetooth
// --------------------------------------------------

/**
 * Creer socket
 *
 * @param canal
 *		Le canal d'ecoute
 *
 * @return la socket
 */
SOCKET NLib_Module_Reseau_Serveur_Bluetooth_CreerSocket( NU32 canal );

/**
 * Obtenir l'adresse MAC a partir de l'adressage
 *
 * @param adressage
 *		Le contexte d'adressage
 *
 * @return l'adresse MAC
 */
__ALLOC char *NLib_Module_Reseau_Serveur_Bluetooth_ObtenirAdresseMAC( struct sockaddr_rc adressage );

#endif // NLIB_MODULE_RESEAU_BLUETOOTH

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_BLUETOOTH_PROTECT

