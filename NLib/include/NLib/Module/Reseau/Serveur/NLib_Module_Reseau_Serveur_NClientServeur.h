#ifndef NLIB_MODULE_RESEAU_SERVEUR_NCLIENTSERVEUR_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_NCLIENTSERVEUR_PROTECT

/*
	Definition d'un client gere par le serveur

	Il est possible d'ajouter un packet gr�ce au NCachePacket
	avec un type choisi (Normal ou personnalise)

	Attribution automatique d'un identifiant unique exploitab
	le pour l'identification externe

	La structure dispose d'un membre m_donneeExterne, utilisa
	ble directement par l'utilisateur au moyen de
	NLib_Module_Reseau_Serveur_NClientServeur_DefinirDonneeExterne
	et
	NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne

	@author SOARES Lucas
*/

// ----------------------------------------------------
// struct NLib::Module::Reseau::Serveur::NClientServeur
// ----------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// Limite pour l'identifiant
#define LIMITE_IDENTIFIANT_CLIENT_SERVEUR		100000

// struct NLib::Module::Reseau::Serveur::NClientServeur
typedef struct NClientServeur
{
	// Socket client
	SOCKET m_socket;

	// Type de client serveur
	NTypeServeur m_type;

	// Type de packet emis
	NTypeCachePacket m_typePacketEmis;

	// Adresse
	union
	{
		SOCKADDR_IN tcp;
#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
		struct sockaddr_rc bluetooth;
#endif // NLIB_MODULE_RESEAU_BLUETOOTH
	} m_contexteAdressage;

	// Identifiant unique
	NU32 m_identifiantUnique;

	// Thread emission
	NThread *m_threadEmission;

	// Thread reception
	NThread *m_threadReception;

	// Cache packets
	NCachePacket *m_cachePacket;

	// Adresse du client
	char *m_adresse;

	// Est mort? (Doit etre deco/supprime)
	NBOOL m_estMort;

	// Callbacks
		// Reception packet
			__CALLBACK NBOOL ( *m_callbackReception )( const struct NClientServeur*,
				const NPacket* );
		// Transfert
			__CALLBACK __ALLOC NPacket *( ___cdecl *m_callbackMethodeReception )( SOCKET socket,
				NBOOL estTimeoutAutorise );
			__CALLBACK NBOOL ( ___cdecl *m_callbackMethodeEmission )( const void *packet,
				SOCKET socket );

	// Donnees client externe
	char *m_donneeExterne;

	// Ping
	NPing *m_ping;

	// Est en cours?
	NBOOL m_estEnCours;

	// Temps avant timeout
	NU32 m_tempsAvantTimeoutReception;
	NU32 m_tempsAvantTimeoutEmission;
} NClientServeur;

/**
 * Construire un client serveur
 *
 * @param socketServeur
 *		La socket du serveur
 * @param callbackReception
 *		Le callback appele a la reception d'un packet
 * @param estConnexionAutorisee
 *		La connexion est-elle autorisee actuellement?
 * @param tempsAvantTimeoutReception
 *		Le temps avant timeout reception
 * @param tempsAvantTimeoutEmission
 * 		Le timeout emission
 * @param type
 *		Le type de serveur
 * @param typePacketEmis
 * 		Le type de packet utilise a l'emission
 * @param callbackMethodeReception
 *		La methode de reception
 * @param callbackMethodeEmission
 *		La methode d'emission
 *
 * @return l'instance du client
 */
__ALLOC NClientServeur *NLib_Module_Reseau_Serveur_NClientServeur_Construire( SOCKET socketServeur,
	__CALLBACK NBOOL ( *callbackReception )( const NClientServeur*,
	const NPacket* ),
	const NBOOL *estConnexionAutorisee,
	NU32 tempsAvantTimeoutReception,
	NU32 tempsAvantTimeoutEmission,
	NTypeServeur type,
	NTypeCachePacket typePacketEmis,
	__CALLBACK __ALLOC NPacket *( ___cdecl *callbackMethodeReception )( SOCKET socket,
	NBOOL estTimeoutAutorise ),
	__CALLBACK NBOOL ( ___cdecl *callbackMethodeEmission )( const void *packet,
	SOCKET socket ) );

// Detruire le client
void NLib_Module_Reseau_Serveur_NClientServeur_Detruire( NClientServeur** );

// Est mort?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstMort( const NClientServeur* );

// Est en cours?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstEnCours( const NClientServeur* );

// Est packet(s) dans le cache?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstPacketsDansCache( const NClientServeur* );

// Tuer le client
void NLib_Module_Reseau_Serveur_NClientServeur_Tuer( NClientServeur* );

// Definir identifiant
void NLib_Module_Reseau_Serveur_NClientServeur_DefinirIdentifiantUnique( NClientServeur*,
	NU32 identifiant );

// Obtenir l'identifiant
NU32 NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( const NClientServeur* );

// Obtenir SOCKET
SOCKET NLib_Module_Reseau_Serveur_NClientServeur_ObtenirSocket( const NClientServeur* );

// Obtenir cache packet
NCachePacket *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirCachePacket( const NClientServeur* );

// Donner un handle de donnee au client
void NLib_Module_Reseau_Serveur_NClientServeur_DefinirDonneeExterne( NClientServeur*,
	char *data );

// Obtenir les donnees externes enregistrees pour ce client
char *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( const NClientServeur* );

// Obtenir ping
const NPing *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirPing( const NClientServeur* );

// Ajouter un packet
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( NClientServeur*,
	__WILLBEOWNED void *packet );
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacketCopie( NClientServeur*,
	const void *packet );

/**
 * Obtenir adresse
 *
 * @param this
 *		Cette instance
 *
 * @return l'adresse
 */
const char *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( const NClientServeur* );

/**
 * On a un timeout en reception?
 *
 * @param this
 * 		Cette instance
 *
 * @return si on a un timeout reception
 */
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstTimeoutAutoriseReception( const NClientServeur* );

/**
 * Obtenir le timeout reception
 *
 * @param this
 * 		Cette instance
 *
 * @return le timeout reception
 */
NU32 NLib_Module_Reseau_Serveur_NClientServeur_ObtenirTempsAvantTimeoutReception( const NClientServeur* );

/**
 * On a un timeout en emission?
 *
 * @param this
 * 		Cette instance
 *
 * @return si on a un timeout emission
 */
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstTimeoutAutoriseEmission( const NClientServeur* );

/**
 * Obtenir le timeout emission
 *
 * @param this
 * 		Cette instance
 *
 * @return le timeout emission
 */
NU32 NLib_Module_Reseau_Serveur_NClientServeur_ObtenirTempsAvantTimeoutEmission( const NClientServeur* );
#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_NCLIENTSERVEUR_PROTECT

