#ifndef NLIB_MODULE_RESEAU_SERVEUR_THREAD_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_THREAD_PROTECT

/*
	Thread pour l'acceptation de client et pour
	l'emission/reception de packet

	@author SOARES Lucas
*/

// -----------------------------------------------
// namespace NLib::Module::Reseau::Serveur::Thread
// -----------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// namespace NLib::Module::Reseau::Serveur::Thread::Acceptation
#include "Acceptation/NLib_Module_Reseau_Serveur_Thread_Acceptation.h"

// namespace NLib::Module::Reseau::Serveur::Thread::Client
#include "Client/NLib_Module_Reseau_Serveur_Thread_Client.h"

// namespace NLib::Module::Reseau::Serveur::Thread::Update
#include "Update/NLib_Module_Reseau_Serveur_Thread_Update.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_THREAD_PROTECT

