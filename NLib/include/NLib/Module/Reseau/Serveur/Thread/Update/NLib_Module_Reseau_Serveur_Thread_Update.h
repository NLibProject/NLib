#ifndef NLIB_MODULE_RESEAU_SERVEUR_THREAD_UPDATE_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_THREAD_UPDATE_PROTECT

// -------------------------------------------------------
// namespace NLib::Module::Reseau::Serveur::Thread::Update
// -------------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

/**
 * Thread de nettoyage
 *
 * @param this
 *		Cette instance du serveur
 *
 * @return si l'operation s'est bien passe
 */
NBOOL NLib_Module_Reseau_Serveur_Thread_Update_Thread( NServeur* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_THREAD_UPDATE_PROTECT

