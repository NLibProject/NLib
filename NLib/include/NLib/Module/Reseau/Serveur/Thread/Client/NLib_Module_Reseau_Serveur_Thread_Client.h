#ifndef NLIB_MODULE_RESEAU_SERVEUR_THREAD_CLIENT_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_THREAD_CLIENT_PROTECT

/*
	Thread emission/reception de packets

	@author SOARES Lucas
*/

// -------------------------------------------------------
// namespace NLib::Module::Reseau::Serveur::Thread::Client
// -------------------------------------------------------

#ifdef NLIB_MODULE_RESEAU
/**
 * Le thread d'emission
 *
 * @param client
 * 		Le client
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Serveur_Thread_Client_ThreadEmission( NClientServeur *client );

/**
 * Le thread de reception
 *
 * @param client
 * 		Le client
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Serveur_Thread_Client_ThreadReception( NClientServeur *client );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_THREAD_CLIENT_PROTECT

