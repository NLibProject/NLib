#ifndef NLIB_MODULE_RESEAU_SERVEUR_NSERVEUR_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_NSERVEUR_PROTECT

#ifdef NLIB_MODULE_RESEAU

/**
 *	Serveur auto-gere pour l'emission et la reception de
 *	packets, ainsi que pour la connexion/deconnexion de
 *	clients.
 *
 *	Communication avec une gestion exterieure via les ca
 *	llbacks:
 *		callbackReceptionPacket: Transmet le packet re�u
 *		ainsi que le client ayant recu ce packet
 *		callbackConnexion: Transmet le client qui vient
 *		de se connecter
 *		callbackDeconnexion: Transmet le client qui vien
 *		t de se deconnecter
 *
 *	ATTENTION:
 *	Par defaut, la connexion au serveur est INTERDITE (ap
 *	peler NLib_Module_Reseau_Serveur_NServeur_AutoriserCo
 *	nnexion( ) pour pouvoir se connecter)
 *
 *	@author SOARES Lucas
 */

// ----------------------------------------------
// struct NLib::Module::Reseau::Serveur::NServeur
// ----------------------------------------------

// Nombre de connexions simulatanees au maximum
#define NMAXIMUM_CLIENTS_SIMULTANE_NSERVEUR				500

// struct NLib::Module::Reseau::Serveur::NServeur
typedef struct
{
	// Socket du serveur
	SOCKET m_socket;

	// Nombre maximum de clients
	NU32 m_nombreMaximumClient;

	// Liste des clients
	NListe *m_clients;

	// Thread d'acceptation client
	NThread *m_threadAcceptationClient;

	// Thread update clients
	NThread *m_threadUpdateClient;

	// Connexion autorisee?
	NBOOL m_estConnexionAutorisee;

	// Donnees a transmettre au NClientServeur lors de sa connexion
	void *m_donneePourClient;

	// Callbacks
		// Reception packet
			__CALLBACK NBOOL ( *m_callbackReceptionPacket )( const NClientServeur*,
				const NPacket *packet );
		// Connexion
			__CALLBACK NBOOL ( *m_callbackConnexion )( const NClientServeur* );
		// Deconnexion
			__CALLBACK NBOOL ( *m_callbackDeconnexion )( const NClientServeur* );
		// Transfert
			__CALLBACK __ALLOC NPacket *( ___cdecl *m_callbackMethodeReception )( SOCKET socket,
				NBOOL estTimeoutAutorise );
			__CALLBACK NBOOL ( ___cdecl *m_callbackMethodeEmission )( const void *packet,
				SOCKET socket );

	// Est en cours
	NBOOL m_estEnCours;

	// Temps avant timeout
	NU32 m_tempsAvantTimeoutReception;
	NU32 m_tempsAvantTimeoutEmission;

	// Type de serveur
	NTypeServeur m_type;

	// Type de packet emis
	NTypeCachePacket m_typePacketEmis;

	// Est log console?
	NBOOL m_estLogConsole;
} NServeur;

/**
 * Construire serveur
 *
 * @param portOuCanal
 *		Le port d'ecoute
 * @param interfaceEcoute
 * 		L'adresse IP de l'interface qui doit ecouter (NULL pour que toutes les interfaces ecoutent)
 * @param callbackReceptionPacket
 *		Le callback de reception
 * @param callbackConnexion
 *		Le callback de connexion client
 * @param callbackDeconnexion
 *		Le callback appele a la deconnexion d'un client
 * @param donneePourClient
 *		Les donnees passees au client
 * @param tempsAvantTimeoutReception
 *		Le temps avant d'etre timeout a la reception (si != 0)
 * @param tempsAvantTimeoutEmission
 * 		Le timeout emission
 * @param type
 *		Le type de serveur
 * @param methodeTransmissionPacketIO
 *		La methode de transmission utilisee
 *
 * @return l'instance du serveur
 */
__ALLOC NServeur *NLib_Module_Reseau_Serveur_NServeur_Construire( NU32 portOuCanal,
	const char *interfaceEcoute,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NClientServeur*,
	const NPacket* ),
	__CALLBACK NBOOL ( *callbackConnexion )( const NClientServeur* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( const NClientServeur* ),
	void *donneePourClient,
	NU32 tempsAvantTimeoutReception,
	NU32 tempsAvantTimeoutEmission,
	NTypeServeur type,
	NMethodeTransmissionPacketIO methodeTransmission );

/**
 * Construire serveur avec methode de transfert personnalisee
 *
 * @param portOuCanal
 *		Le port/canal d'ecoute
 * @param interfaceEcoute
 * 		L'adresse IP de l'interface qui doit ecouter (NULL pour que toutes les interfaces ecoutent)
 * @param callbackReceptionPacket
 *		Le callback de reception
 * @param callbackConnexion
 *		Le callback de connexion client
 * @param callbackDeconnexion
 *		Le callback appele a la deconnexion d'un client
 * @param donneePourClient
 *		Les donnees passees au client
 * @param tempsAvantTimeoutReception
 *		Le temps avant d'etre timeout a la reception (si != 0)
 * @param tempsAvantTimeoutEmission
 * 		Timeout emission
 * @param type
 *		Le type de serveur
 * @param typePacketEmis
 * 		Le type de packet emis
 * @param callbackMethodeReception
 *		La methode de reception
 * @param callbackMethodeEmission
 *		La methode d'emission
 *
 * @return l'instance du serveur
 */
__ALLOC NServeur *NLib_Module_Reseau_Serveur_NServeur_Construire2( NU32 portOuCanal,
	const char *interfaceEcoute,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NClientServeur*,
	const NPacket* ),
	__CALLBACK NBOOL ( *callbackConnexion )( const NClientServeur* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( const NClientServeur* ),
	void *donneePourClient,
	NU32 tempsAvantTimeoutReception,
	NU32 tempsAvantTimeoutEmission,
	NTypeServeur type,
	NTypeCachePacket typePacketEmis,
	__CALLBACK __ALLOC NPacket *( ___cdecl *callbackMethodeReception )( SOCKET socket,
	NBOOL estTimeoutAutorise ),
	__CALLBACK NBOOL ( ___cdecl *callbackMethodeEmission )( const void *packet,
	SOCKET socket ) );

/**
 * Detruire le serveur
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Serveur_NServeur_Detruire( NServeur** );

/**
 * Le serveur est en cours d'execution?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le serveur est en cours d'execution
 */
NBOOL NLib_Module_Reseau_Serveur_NServeur_EstEnCours( const NServeur* );

/**
 * Obtenir le nombre de clients connectes actuellement
 *
 * @return le nombre de clients
 */
__WILLLOCK __WILLUNLOCK NU32 NLib_Module_Reseau_Serveur_NServeur_ObtenirNombreClients( const NServeur* );

/**
 * Accepter un client
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Serveur_NServeur_AccepterClient( NServeur* );

/**
 * Nettoyer les clients morts
 *
 * @return le nombre de clients nettoyes
 */
NU32 NLib_Module_Reseau_Serveur_NServeur_NettoyerClients( NServeur* );

/**
 * Tuer tous les clients connectes (attendre un passage du nettoyeur
 * NLib_Module_Reseau_Serveur_NServeur_NettoyerClients pour leur elimination)
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Serveur_NServeur_TuerTousClients( NServeur* );

/**
 * Autoriser la connexion
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Serveur_NServeur_AutoriserConnexion( NServeur* );

/**
 * Interdire la connexion
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Serveur_NServeur_InterdireConnexion( NServeur* );

/**
 * Activer log console
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Serveur_NServeur_ActiverLogConsole( NServeur* );

/**
 * Desactiver log console
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Serveur_NServeur_DesactiverLogConsole( NServeur* );

/**
 * Obtenir le nombre maximum de clients
 *
 * @param this
 * 		Cette instance
 *
 * @return le nombre maximum de clients autorises
 */
NU32 NLib_Module_Reseau_Serveur_NServeur_ObtenirNombreMaximumClients( const NServeur* );

/**
 * Definir le nombre maximum de clients
 *
 * @param this
 * 		Cette instance
 * @param nombreMaximumClient
 * 		Le nombre maximum de clients
 */
void NLib_Module_Reseau_Serveur_NServeur_DefinirNombreMaximumClients( NServeur*,
	NU32 nombreMaximumClient );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_NSERVEUR_PROTECT

