#ifndef NLIB_MODULE_RESEAU_RESOLUTION_PROTECT
#define NLIB_MODULE_RESEAU_RESOLUTION_PROTECT

// ------------------------------------------
// namespace NLib::Module::Reseau::Resolution
// ------------------------------------------

#ifdef NLIB_MODULE_RESEAU
// struct NLib::Module::Reseau::Resolution::NListeResolution
#include "NLib_Module_Reseau_Resolution_NListeResolution.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_RESOLUTION_PROTECT

