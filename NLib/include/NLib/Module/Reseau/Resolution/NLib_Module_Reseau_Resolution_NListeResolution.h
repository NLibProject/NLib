#ifndef NLIB_MODULE_RESEAU_RESOLUTION_NLISTERESOLUTION_PROTECT
#define NLIB_MODULE_RESEAU_RESOLUTION_NLISTERESOLUTION_PROTECT

// ---------------------------------------------------------
// struct NLib::Module::Reseau::Resolution::NListeResolution
// ---------------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

typedef struct NListeResolution
{
	/**
	 * Les ips resolues
	 */
	NDetailIP **m_ip;

	/**
	 * Le nombre d'ip resolues
	 */
	NU32 m_nombreIP;

	/**
	 * Le domaine a resoudre
	 */
	char *m_domaine;
} NListeResolution;

/**
 * Construire l'instance
 *
 * @param domaine
 *		Le domaine a resoudre
 * @param service
 *		Le service a resoudre (port)
 *
 * @return l'instance
 */
__ALLOC NListeResolution *NLib_Module_Reseau_Resolution_NListeResolution_Construire( const char *domaine,
	const char *service );

/**
 * Detruire l'instance
 *
 * @param this
 *		This instance
 */
void NLib_Module_Reseau_Resolution_NListeResolution_Detruire( NListeResolution** );

/**
 * Obtenir le nombre d'ips pour le domaine
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre d'ips
 */
NU32 NLib_Module_Reseau_Resolution_NListeResolution_ObtenirNombre( const NListeResolution* );

/**
 * Obtenir detail ip
 *
 * @param this
 *		Cette instance
 * @param index
 *		The ip index
 *
 * @return the detail
 */
const NDetailIP *NLib_Module_Reseau_Resolution_NListeResolution_ObtenirIP( const NListeResolution*,
	NU32 index );

/**
 * Obtenir ipv4
 *
 * @param this
 *		Cette instance
 *
 * @return une ip au format v4
 */
const NDetailIP *NLib_Module_Reseau_Resolution_NListeResolution_ObtenirIPV4( const NListeResolution* );

/**
 * Obtenir le domaine ayant ete resolu
 *
 * @param this
 *		Cette instance
 *
 * @return le nom de domaine
 */
const char *NLib_Module_Reseau_Resolution_NListeResolution_ObtenirDomaine( const NListeResolution* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_RESOLUTION_NLISTERESOLUTION_PROTECT

