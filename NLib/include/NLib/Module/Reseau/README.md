NLib Reseau
===========

Introduction
------------

Module reseau de NLib

Avertissement
-------------

Le signal SIGPIPE sous unix est automatiquement ignore.

***Cependant***, en mode debogage, le signal est capture suivant la configuration
du debogueur, donc veiller a le parametrer correctement pour ne pas etre derange
par un client se deconnectant durant l'envoi d'une trame.

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

