#ifndef NLIB_MODULE_RESEAU_SOCKET_SELECTION_PROTECT
#define NLIB_MODULE_RESEAU_SOCKET_SELECTION_PROTECT

/*
	Selection des sockets pour l'ecriture et la lecture

	@author SOARES Lucas
*/

// -------------------------------------------------
// namespace NLib::Module::Reseau::Socket::Selection
// -------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// Selectionner socket ecriture
NBOOL NLib_Module_Reseau_Socket_Selection_EstEcritureDisponible( SOCKET socket );

// Selectionner socket lecture
NBOOL NLib_Module_Reseau_Socket_Selection_EstLectureDisponible( SOCKET socket );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SOCKET_SELECTION_PROTECT

