#ifndef NLIB_MODULE_RESEAU_IP_NPROTOCOLEIP_PROTECT
#define NLIB_MODULE_RESEAU_IP_NPROTOCOLEIP_PROTECT

// -------------------------------------------
// enum NLib::Module::Reseau::IP::NProtocoleIP
// -------------------------------------------

#ifdef NLIB_MODULE_RESEAU

typedef enum NProtocoleIP
{
	NPROTOCOLE_IP_IPV4,
	NPROTOCOLE_IP_IPV6,

	NPROTOCOLES_IP
} NProtocoleIP;

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_IP_NPROTOCOLEIP_PROTECT

