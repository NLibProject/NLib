#ifndef NLIB_MODULE_RESEAU_IP_PROTECT
#define NLIB_MODULE_RESEAU_IP_PROTECT

// ----------------------------------
// namespace NLib::Module::Reseau::IP
// ----------------------------------

#ifdef NLIB_MODULE_RESEAU
// enum NLib::Module::Reseau::IP::NProtocoleIP
#include "NLib_Module_Reseau_IP_NProtocoleIP.h"

// struct NLib::Module::Reseau::IP::NDetailIP
#include "NLib_Module_Reseau_IP_NDetailIP.h"

// Taille maximale adresse IPv4
#define NTAILLE_MAXIMALE_ADRESSE_IPV4			15

// Taille maximale adresse IPv6
#define NTAILLE_MAXIMALE_ADRESSE_IPV6			17

/**
 * Divide ip in %composentCount% components (The left one is at 0 index)
 *
 * @param ip
 * 		The ip
 * @param ipProtocol
 * 		The ip protocol
 * @param output
 * 		The output array
 *
 * @return if operation succedeed
 */
NBOOL NLib_Module_Reseau_IP_DivideIP( const char *ip,
	NProtocoleIP ipProtocol,
	__OUTPUT NU8 *output );

/**
 * Divide ip in %composentCount% components (The left one is at 0 index)
 *
 * @param ip
 * 		The integer composed ip
 * @param ipProtocol
 * 		The ip protocol
 * @param output
 * 		The output array
 */
void NLib_Module_Reseau_IP_DivideIP2( NU64 ip,
	NProtocoleIP ipProtocol,
	__OUTPUT NU8 *output );

/**
 * C'est une adresse IPv4?
 *
 * @param ip
 * 		L'adresse ip
 *
 * @return si il s'agit d'une adresse IPv4
 */
NBOOL NLib_Module_Reseau_IP_EstAdresseIPv4( const char *ip );

/**
 * Obtenir IP
 *
 * @param adressage
 *		L'adresse
 *
 * @return l'ip
 */
__ALLOC char *NLib_Module_Reseau_IP_ObtenirAdresseIP( SOCKADDR_IN adressage );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_IP_PROTECT

