#ifndef NLIB_MODULE_RESEAU_IP_NDETAILIP_PROTECT
#define NLIB_MODULE_RESEAU_IP_NDETAILIP_PROTECT

// ------------------------------------------
// struct NLib::Module::Reseau::IP::NDetailIP
// ------------------------------------------

#ifdef NLIB_MODULE_RESEAU
typedef struct NDetailIP
{
	// Protocole
	NProtocoleIP m_protocole;

	// Adresse
	char *m_adresse;

	// CIDR
	NU32 m_cidr;
} NDetailIP;

/**
 * Construire instance
 *
 * @param protocole
 *		Le protocole
 * @param adresse
 *		L'adresse ip (v4/v6)
 *
 * @return l'instance
 */
__ALLOC NDetailIP *NLib_Module_Reseau_IP_NDetailIP_Construire( NProtocoleIP,
	const char *adresse );

/**
 * Construire l'instance
 *
 * @param src
 *		La source
 *
 * @return l'instance
 */
__ALLOC NDetailIP *NLib_Module_Reseau_IP_NDetailIP_Construire2( const NDetailIP* );

/**
 * Construire l'instance
 *
 * @param protocole
 * 		Le protocole
 * @param adresse
 * 		L'adresse ip (v4/v6)
 * @param cidr
 * 		Le cidr
 *
 * @return l'instance
 */
__ALLOC NDetailIP *NLib_Module_Reseau_IP_NDetailIP_Construire3( NProtocoleIP protocole,
	const char *adresse,
	NU32 cidr );

/**
 * Detruire l'instance
 *
 * @param this
 *		L'instance a detruire
 */
void NLib_Module_Reseau_IP_NDetailIP_Detruire( NDetailIP** );

/**
 * Obtenir protocole
 *
 * @param this
 *		Cette instance
 *
 * @return Le protocole
 */
NProtocoleIP NLib_Module_Reseau_IP_NDetailIP_ObtenirProtocole( const NDetailIP* );

/**
 * Obtenir adresse
 *
 * @param this
 *		Cette instance
 *
 * @return l'adresse
 */
const char *NLib_Module_Reseau_IP_NDetailIP_ObtenirAdresse( const NDetailIP* );

/**
 * Obtenir CIDR
 *
 * @param this
 * 		Cette instance
 *
 * @return le cidr
 */
NU32 NLib_Module_Reseau_IP_NDetailIP_ObtenirCIDR( const NDetailIP* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_IP_NDETAILIP_PROTECT

