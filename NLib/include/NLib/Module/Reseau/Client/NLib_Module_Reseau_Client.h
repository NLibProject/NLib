#ifndef NLIB_MODULE_RESEAU_CLIENT_PROTECT
#define NLIB_MODULE_RESEAU_CLIENT_PROTECT

/**
 *	Definition des elements necessaires a un client NClient
 *
 *	@author SOARES Lucas
 */

// --------------------------------------
// namespace NLib::Module::Reseau::Client
// --------------------------------------

#ifdef NLIB_MODULE_RESEAU

// enum NLib::Module::Reseau::Client::NTypeClient
#include "NLib_Module_Reseau_Client_NTypeClient.h"

// struct NLib::Module::Reseau::Client::NClient
#include "NLib_Module_Reseau_Client_NClient.h"

// namespace NLib::Module::Reseau::Client::Thread
#include "Thread/NLib_Module_Reseau_Client_Thread.h"

// namespace NLib::Module::Reseau::Client::TCP
#include "TCP/NLib_Module_Reseau_Client_TCP.h"

// namespace NLib::Module::Reseau::Client::Bluetooth
#include "Bluetooth/NLib_Module_Reseau_Client_Bluetooth.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_CLIENT_PROTECT

