#ifndef NLIB_MODULE_RESEAU_CLIENT_NTYPECLIENT_PROTECT
#define NLIB_MODULE_RESEAU_CLIENT_NTYPECLIENT_PROTECT

#ifdef NLIB_MODULE_RESEAU

// ----------------------------------------------
// enum NLib::Module::Reseau::Client::NTypeClient
// ----------------------------------------------

typedef enum NTypeClient
{
	NTYPE_CLIENT_TCP,
#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
	NTYPE_CLIENT_BLUETOOTH,
#endif // NLIB_MODULE_RESEAU_BLUETOOTH

	NTYPES_CLIENT
} NTypeClient;

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_CLIENT_NTYPECLIENT_PROTECT

