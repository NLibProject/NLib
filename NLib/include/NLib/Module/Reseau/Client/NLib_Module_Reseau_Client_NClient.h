#ifndef NLIB_MODULE_RESEAU_CLIENT_NCLIENT_PROTECT
#define NLIB_MODULE_RESEAU_CLIENT_NCLIENT_PROTECT

/**
 *	Definition d'un client auto-gere quant-a la reception
 *	et l'emission de packets vers le serveur
 *
 *	@author SOARES Lucas
 */

// --------------------------------------------
// struct NLib::Module::Reseau::Client::NClient
// --------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// struct NLib::Reseau::Client::NClient
typedef struct NClient
{
	// Socket
	SOCKET m_socket;

	// Cache packet
	NCachePacket *m_cachePacket;

	// Adressage client
	union
	{
		SOCKADDR_IN tcp;
#ifdef NLIB_MODULE_RESEAU_BLUETOOTH
		struct sockaddr_rc bluetooth;
#endif // NLIB_MODULE_RESEAU_BLUETOOTH
	} m_adresse;

	// Est connecte?
	NBOOL m_estConnecte;

	// Est threads en cours?
	NBOOL m_estThreadEnCours;

	// Est erreur?
	NBOOL m_estErreur;

	// Type client
	NTypeClient m_type;

	// Threads
		// Emission
			NThread *m_threadEmission;
		// Reception
			NThread *m_threadReception;

	// Callbacks
		// Argument
			void *m_dataUtilisateur;
		// Reception packet
			__CALLBACK NBOOL ( *m_callbackReceptionPacket )( const NPacket*,
				const struct NClient* );
		// Deconnexion client
			__CALLBACK NBOOL ( *m_callbackDeconnexion )( const struct NClient* );
		// Transmission
			__CALLBACK NBOOL ( *m_callbackMethodeEmission )( const void *packet,
				SOCKET socket );
			__CALLBACK __ALLOC NPacket *( *m_callbackMethodeReception )( SOCKET socket,
				NBOOL estTimeoutAutorise );

	// Temps avant timeout (ignore si 0)
	NU32 m_tempsAvantTimeout;
} NClient;

/**
 * Construire client
 *
 * @param adresse
 *		L'adresse IPV4/MAC a laquelle se connecter
 * @param portOuCanal
 *		Le port/canal auquel se connecter
 * @param callbackReceptionPacket
 *		Le callback en cas de reception de packet
 * @param callbackDeconnexion
 *		Le callback en cas de deconnexion
 * @param dataUtilisateur
 *		Les donnees utilisateur
 * @param tempsAvantTimeout
 *		Le temps avant d'etre timeout pour reception (ignore si = 0)
 * @param type
 *		Le type de client
 * @param methodeTransmission
 *		La methode de transmission souhaitee
 *
 * @return l'instance du client
 */
__ALLOC NClient *NLib_Module_Reseau_Client_NClient_Construire( const char *adresse,
	NU32 portOuCanal,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NPacket*,
		const NClient* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( const NClient* ),
	void *dataUtilisateur,
	NU32 tempsAvantTimeout,
	NTypeClient type,
	NMethodeTransmissionPacketIO methodeTransmission );

/**
 * Construire client avec methode transmission personnalisee
 *
 * @param adresse
 *		L'adresse IPV4/MAC a laquelle se connecter
 * @param portOuCanal
 *		Le port/canal auquel se connecter
 * @param callbackReceptionPacket
 *		Le callback en cas de reception de packet
 * @param callbackDeconnexion
 *		Le callback en cas de deconnexion
 * @param dataUtilisateur
 *		Les donnees utilisateur
 * @param tempsAvantTimeout
 *		Le temps avant d'etre timeout pour reception (ignore si = 0)
 * @param type
 *		Le type de client
 * @param typePacketEnvoye
 * 		Le type de packet envoye
 * @param callbackMethodeReception
 *		La methode de transmission pour recevoir
 * @param callbackMethodeEmission
 *		La methode de transmission pour emettre
 *
 * @return l'instance du client
 */
__ALLOC NClient *NLib_Module_Reseau_Client_NClient_Construire2( const char *adresse,
	NU32 portOuCanal,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NPacket*,
	const NClient* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( const NClient* ),
	void *dataUtilisateur,
	NU32 tempsAvantTimeout,
	NTypeClient type,
	NTypeCachePacket typePacketEnvoye,
	__CALLBACK __ALLOC NPacket *( ___cdecl *callbackMethodeReception )( SOCKET socket,
	NBOOL estTimeoutAutorise ),
	__CALLBACK NBOOL ( ___cdecl *callbackMethodeEmission )( const void *packet,
	SOCKET socket ) );

/**
 * Detruire le client
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Client_NClient_Detruire( NClient** );

/**
 * Connecter
 *
 * @param this
 * 		Cette instance
 *
 * @return si la connexion a reussi
 */
NBOOL NLib_Module_Reseau_Client_NClient_Connecter( NClient* );

/**
 * Connecter avec timeout a la connexion
 *
 * @param this
 * 		Cette instance
 * @param timeout
 * 		Le timeout a la connexion
 *
 * @return si la connexion a reussi
 */
NBOOL NLib_Module_Reseau_Client_NClient_Connecter2( NClient*,
	NU32 timeout );

/**
 * Se deconnecter
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Client_NClient_Deconnecter( NClient* );

/**
 * Activer flag erreur
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_Reseau_Client_NClient_ActiverErreur( NClient* );

/**
 * Ajouter un packet a envoyer
 *
 * @param packet
 * 		Le packet a envoyer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Client_NClient_AjouterPacket( NClient*,
	__WILLBEOWNED void *packet );

/**
 * Ajouter un packet qui sera une copie du packet passe
 *
 * @param packet
 * 		Le packet a ajouter apres duplication
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_Reseau_Client_NClient_AjouterPacketCopie( NClient*,
	const void *packet );

/**
 * Est ce que le client est connecte?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le client est connecte
 */
NBOOL NLib_Module_Reseau_Client_NClient_EstConnecte( const NClient* );

/**
 * Est ce qu'il y a une erreur?
 *
 * @param this
 * 		Cette instance
 *
 * @return si il y a eu une erreur
 */
NBOOL NLib_Module_Reseau_Client_NClient_EstErreur( const NClient* );

/**
 * Obtenir la liste des packets
 *
 * @param this
 * 		Cette instance
 *
 * @return la liste des packet
 */
NCachePacket *NLib_Module_Reseau_Client_NClient_ObtenirCachePacket( const NClient* );

/**
 * Obtenir la socket
 *
 * @param this
 * 		Cette instance
 *
 * @return la socket
 */
SOCKET NLib_Module_Reseau_Client_NClient_ObtenirSocket( const NClient* );

/**
 * Le client est en cours?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le client est en cours
 */
NBOOL NLib_Module_Reseau_Client_NClient_EstThreadEnCours( const NClient* );

/**
 * Obtenir les donnees utilisateur
 *
 * @param this
 * 		Cette instance
 *
 * @return les donnees utilisateur
 */
void *NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( const NClient* );

/**
 * On a un timeout defini pour la reception?
 *
 * @param this
 * 		Cette instance
 *
 * @return si un timeout est defini
 */
NBOOL NLib_Module_Reseau_Client_NClient_EstTimeoutAutorise( const NClient* );

/**
 * Obtenir le timeout reception
 *
 * @param this
 * 		Cette instance
 *
 * @return le timeout reception
 */
NU32 NLib_Module_Reseau_Client_NClient_ObtenirTimeout( const NClient* );

/**
 * Obtenir l'adresse
 *
 * @param this
 *		Cette instance
 *
 * @return l'adresse
 */
const void *NLib_Module_Reseau_Client_NClient_ObtenirAdresse( const NClient* );

/**
 * Obtenir le type de cache packet
 *
 * @param this
 * 		Cette instance
 *
 * @return le type de cache packet
 */
NTypeCachePacket NLib_Module_Reseau_Client_NClient_ObtenirTypeCachePacket( const NClient* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_CLIENT_NCLIENT_PROTECT

