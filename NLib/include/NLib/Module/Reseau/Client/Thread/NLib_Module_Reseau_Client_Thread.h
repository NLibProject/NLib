#ifndef NLIB_MODULE_RESEAU_CLIENT_THREAD_PROTECT
#define NLIB_MODULE_RESEAU_CLIENT_THREAD_PROTECT

/*
	Threads pour l'auto-gestion d'un client

	@author SOARES Lucas
*/

// ----------------------------------------------
// namespace NLib::Module::Reseau::Client::Thread
// ----------------------------------------------

#ifdef NLIB_MODULE_RESEAU

/* Emission */
NBOOL NLib_Module_Reseau_Client_Thread_Emission( NClient* );

/* Reception */
NBOOL NLib_Module_Reseau_Client_Thread_Reception( NClient* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_CLIENT_THREAD_PROTECT

