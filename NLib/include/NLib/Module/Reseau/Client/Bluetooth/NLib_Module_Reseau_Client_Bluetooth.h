#ifndef NLIB_MODULE_RESEAU_CLIENT_BLUETOOTH_PROTECT
#define NLIB_MODULE_RESEAU_CLIENT_BLUETOOTH_PROTECT

#ifdef NLIB_MODULE_RESEAU

#ifdef NLIB_MODULE_RESEAU_BLUETOOTH

// -------------------------------------------------
// namespace NLib::Module::Reseau::Client::Bluetooth
// -------------------------------------------------

/**
 * Obtenir adresse mac
 *
 * @param adresse
 *		L'adresse
 *
 * @return l'adresse mac
 */
__ALLOC char *NLib_Module_Reseau_Client_Bluetooth_TraduireAdresseMac( const struct sockaddr_rc *adresse );

/**
 * Obtenir le canal
 *
 * @param adresse
 *		L'adresse
 *
 * @return le canal
 */
NU32 NLib_Module_Reseau_Client_Bluetooth_TraduireCanal( const struct sockaddr_rc *adresse );

#endif // NLIB_MODULE_RESEAU_BLUETOOTH

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_CLIENT_BLUETOOTH_PROTECT

