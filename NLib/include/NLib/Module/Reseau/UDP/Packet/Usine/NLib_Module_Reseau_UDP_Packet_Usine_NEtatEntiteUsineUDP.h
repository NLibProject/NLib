#ifndef NLIB_MODULE_RESEAU_UDP_PACKET_USINE_NETATENTITEUSINEUDP_PROTECT
#define NLIB_MODULE_RESEAU_UDP_PACKET_USINE_NETATENTITEUSINEUDP_PROTECT

#ifdef NLIB_MODULE_RESEAU

// --------------------------------------------------------------------
// struct NLib::Module::Reseau::UDP::Packet::Usine::NEtatEntiteUsineUDP
// --------------------------------------------------------------------

typedef struct NEtatEntiteUsineUDP
{
	// Identifiant
	NU64 m_identifiant;

	// Nombre de trame attendues
	NU32 m_nombreTrameAttendue;

	// Nombre de trames reçues
	NU32 m_nombreTrameRecue;

	// Donnees
	char *m_data;

	// Taille donnees
	NU32 m_tailleData;

	// Adresse expediteur
	NDetailIP *m_adresseExpediteur;
	NU32 m_portExpediteur;

	// Timestamp derniere mise a jour
	NU32 m_timestamp;
} NEtatEntiteUsineUDP;

/**
 * Construire etat
 *
 * @param identifiant
 *		L'identifiant de l'etat
 * @param nombreTrameTotal
 *		Le nombre de trame total attendu
 * @param tailleData
 *		La taille totale des donnees
 * @param adresseExpediteur
 *		L'adresse de l'expediteur
 * @param portExpediteur
 *		Le port de l'expediteur
 *
 * @return l'instance construite
 */
__ALLOC NEtatEntiteUsineUDP *NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_Construire( NU64 identifiant,
	NU32 nombreFrameTotal,
	NU32 tailleData,
	const NDetailIP *adresseExpediteur,
	NU32 portExpediteur );

/**
 * Detruire etat
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_Detruire( NEtatEntiteUsineUDP** );

/**
 * Obtenir identifiant
 *
 * @param this
 *		Cette instance
 *
 * @return l'identifiant
 */
NU64 NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_ObtenirIdentifiant( const NEtatEntiteUsineUDP* );

/**
 * Recevoir une trame
 *
 * @param this
 *		Cette instance
 * @param data
 *		Les donnees du packet
 * @param header
 *		L'header du packet
 */
void NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_RecevoirTrame( NEtatEntiteUsineUDP*,
	const char *data,
	const NHeaderUsineUDP* );

/**
 * A reçu toutes les trames?
 *
 * @param this
 *		Cette instance
 *
 * @return si toutes les trames ont ete reçues
 */
NBOOL NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_EstReceptionTerminee( const NEtatEntiteUsineUDP* );

/**
 * Creer le packet
 *
 * @param this
 *		Cette instance
 *
 * @return le packet reconstitue
 */
__ALLOC NPacketUDP *NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_CreerPacket( const NEtatEntiteUsineUDP* );

/**
 * Obtenir timestamp dernier update
 *
 * @param this
 *		Cette instance
 *
 * @return le timestamp du dernier update
 */
NU32 NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP_ObtenirTimestampDernierUpdate( const NEtatEntiteUsineUDP* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_UDP_PACKET_USINE_NETATENTITEUSINEUDP_PROTECT

