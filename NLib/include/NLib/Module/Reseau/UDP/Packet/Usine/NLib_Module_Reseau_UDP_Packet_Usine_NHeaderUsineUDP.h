#ifndef NLIB_MODULE_RESEAU_UDP_PACKET_USINE_NHEADERUSINEUDP_PROTECT
#define NLIB_MODULE_RESEAU_UDP_PACKET_USINE_NHEADERUSINEUDP_PROTECT

#ifdef NLIB_MODULE_RESEAU

// ----------------------------------------------------------------
// struct NLib::Module::Reseau::UDP::Packet::Usine::NHeaderUsineUDP
// ----------------------------------------------------------------

typedef struct NHeaderUsineUDP
{
	// Identifiant
	NU64 m_identifiant;

	// Nombre de trames a recevoir
	NU32 m_nombreTrameTotal;

	// Taille totale des donnees
	NU32 m_tailleTotaleData;

	// Taille des donnees contenues dans cette trame
	NU32 m_tailleDataTrame;

	// Adresse initiale des donnees de cette trame
	NU32 m_adresseInitialeData;

	// Checksum
	NU32 m_checksum;
} NHeaderUsineUDP;

/**
 * Construire l'header
 *
 * @param data
 *		Les donnees
 * @param tailleData
 *		La taille des donnees
 *
 * @return l'instance
 */
__ALLOC NHeaderUsineUDP *NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_Construire( const char *data,
	NU32 tailleData );

/**
 * Detruire header
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_Detruire( NHeaderUsineUDP** );

/**
 * Obtenir identifiant
 *
 * @param this
 *		Cette instance
 *
 * @return l'identifiant
 */
NU64 NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirIdentifiant( const NHeaderUsineUDP* );

/**
 * Obtenir nombre trame total
 *
 * @param this
 *		Cette instance
 *
 * @return le nombre de trame total
 */
NU32 NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirNombreTrameTotal( const NHeaderUsineUDP* );

/**
 * Obtenir la taille totale des donnees
 *
 * @param this
 *		Cette instance
 *
 * @return la taille totale des donnees
 */
NU32 NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirTailleTotaleData( const NHeaderUsineUDP* );

/**
 * Obtenir la taille des donnees dans la trame
 *
 * @param this
 *		Cette instance
 *
 * @return la taille des donnees dans la trame
 */
NU32 NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirTailleDataTrame( const NHeaderUsineUDP* );

/**
 * Obtenir l'adresse initiale des donnees
 *
 * @param this
 *		Cette instance
 *
 * @return l'adresse initiale des donnees
 */
NU32 NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirAdresseInitialeData( const NHeaderUsineUDP* );

/**
 * Obtenir le checksum
 *
 * @param this
 *		Cette instance
 *
 * @return le checksum
 */
NU32 NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP_ObtenirChecksum( const NHeaderUsineUDP* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_UDP_PACKET_USINE_NHEADERUSINEUDP_PROTECT

