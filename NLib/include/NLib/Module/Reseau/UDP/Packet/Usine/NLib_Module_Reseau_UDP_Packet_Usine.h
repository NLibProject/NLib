#ifndef NLIB_MODULE_RESEAU_UDP_PACKET_USINE_PROTECT
#define NLIB_MODULE_RESEAU_UDP_PACKET_USINE_PROTECT

#ifdef NLIB_MODULE_RESEAU

// --------------------------------------------------
// namespace NLib::Module::Reseau::UDP::Packet::Usine
// --------------------------------------------------

// struct NLib::Module::Reseau::UDP::Packet::Usine::NHeaderUsineUDP
#include "NLib_Module_Reseau_UDP_Packet_Usine_NHeaderUsineUDP.h"

// struct NLib::Module::Reseau::UDP::Packet::Usine::NEtatEntiteUsineUDP
#include "NLib_Module_Reseau_UDP_Packet_Usine_NEtatEntiteUsineUDP.h"

// struct NLib::Module::Reseau::UDP::Packet::Usine::NUsineUDP
#include "NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP.h"

/**
 *		|---------------------------------------------------------------------|
 *		|ID TRAME|NB TRAMES|TAILLE TOTALE||TAILLE TRAME|DEBUT ADRESSE|CHECKSUM|
 *		|---------------------------------------------------------------------|
 *		|  NU64  |  NU32   |	   NU32    ||    NU32     |    NU32     | NU32  |
 *		|---------------------------------------------------------------------|
 *
 *		==> 28 octets
 */

/**
 * Envoyer packet UDP
 *
 * @param packet
 *		Le packet UDP a envoyer
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Reseau_UDP_Packet_Usine_EnvoyerPacketUDP( const NPacketUDP*,
	SOCKET );

/**
 * Verifier l'integrite du packet
 *
 * @param header
 *		L'header parse
 * @param data
 *		Les donnees
 * @param tailleData
 *		La taille des donnees
 *
 * @return si le packet est integre
 */
NBOOL NLib_Module_Reseau_UDP_Packet_Usine_EstPacketIntegre( const NHeaderUsineUDP*,
	const char *data,
	NU32 tailleData );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_UDP_PACKET_USINE_PROTECT

