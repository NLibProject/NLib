#ifndef NLIB_MODULE_RESEAU_UDP_PACKET_USINE_NUSINEUDP_PROTECT
#define NLIB_MODULE_RESEAU_UDP_PACKET_USINE_NUSINEUDP_PROTECT

#ifdef NLIB_MODULE_RESEAU

// ----------------------------------------------------------
// struct NLib::Module::Reseau::UDP::Packet::Usine::NUsineUDP
// ----------------------------------------------------------

typedef struct NUsineUDP
{
	// Etats
	NListe *m_etats;

	// Callback reception packet (une fois que le packet est complete/timeout, on transmet)
	NBOOL ( *m_callbackReceptionPacket )( const NPacketUDP*,
		const void *clientUDP );

	// Temps avant timeout
	NU32 m_tempsAvantTimeout;

	// Handle client UDP
	const struct NClientUDP *m_clientUDP;
} NUsineUDP;

/**
 * Construire usine
 *
 * @param callbackReceptionPacket
 *		Le callback de reception
 * @param tempsAvantTimeout
 *		Le temps avant lequel si aucune trame n'est reçue
 *		le packet est transmis au callback
 * @param clientUDP
 *		Instance client UDP
 *
 * @return l'instance construite
 */
__ALLOC NUsineUDP *NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_Construire( NBOOL ( *callbackReceptionPacket )( const NPacketUDP*,
		const void *clientUDP ),
	NU32 tempsAvantTimeout,
	const struct NClientUDP* );

/**
 * Detruire l'usine
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_Detruire( NUsineUDP** );

/**
 * Recevoir trame
 *
 * @param this
 *		Cette instance
 * @param packet
 *		Le packet reçu
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_RecevoirTrame( NUsineUDP*,
	const NPacketUDP *packet );

/**
 * Mettre a jour l'usine
 *
 * @param this
 *		Cette instance
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Reseau_UDP_Packet_Usine_NUsineUDP_Update( NUsineUDP* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_UDP_PACKET_USINE_NUSINEUDP_PROTECT

