#ifndef NLIB_MODULE_RESEAU_UDP_PACKET_PROTECT
#define NLIB_MODULE_RESEAU_UDP_PACKET_PROTECT

#ifdef NLIB_MODULE_RESEAU

// -------------------------------------------
// namespace NLib::Module::Reseau::UDP::Packet
// -------------------------------------------

// MTU minimum = 576 - ( 20 = IPv4 header ) - ( 8 = UDP Header ) = 548
// On arrondit a la puissance de 2 en dessous
#define NBUFFER_PACKET_UDP			(NU32)5096

// Taille header genere par l'usine UDP
#define NTAILLE_HEADER_USINE_UDP	28

// struct NLib::Module::Reseau::UDP::Packet::NPacketUDP
#include "NLib_Module_Reseau_UDP_Packet_NPacketUDP.h"

// namespace NLib::Module::Reseau::UDP::Packet::Usine
#include "Usine/NLib_Module_Reseau_UDP_Packet_Usine.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_UDP_PACKET_PROTECT

