#ifndef NLIB_MODULE_RESEAU_UDP_PACKET_NPACKETUDP_PROTECT
#define NLIB_MODULE_RESEAU_UDP_PACKET_NPACKETUDP_PROTECT

#ifdef NLIB_MODULE_RESEAU

// ----------------------------------------------------
// struct NLib::Module::Reseau::UDP::Packet::NPacketUDP
// ----------------------------------------------------

typedef struct NPacketUDP
{
	// Packet
	NPacket *m_packet;

	// Details adressage
	NDetailIP *m_detailIP;

	// Contexte d'adressage
	SOCKADDR_IN m_contexteAdressage;
} NPacketUDP;

/**
 * Construire un packet
 *
 * @param data
 *		Les donnees
 * @param taille
 *		La taille des donnees
 * @param adressage
 *		L'adressage
 *
 * @return l'instance
 */
__ALLOC NPacketUDP *NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire( const char*,
	NU32 taille,
	__WILLBEOWNED NDetailIP*,
	NU32 port );

/**
 * Constuire un packet
 *
 * @param data
 *		Les donnees
 * @param taille
 *		La taille des donnees
 * @param contexteAdressage
 *		Le contexte d'adressage
 *
 * @return l'instance
 */
__ALLOC NPacketUDP *NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire2( const char*,
	NU32 taille,
	const SOCKADDR_IN* );

/**
 * Construire un packet
 *
 * @param packet
 *		Le packet
 * @param ip
 *		Le detail concernant le destinataire
 * @param port
 *		Le port a considerer
 *
 * @return l'instance
 */
__ALLOC NPacketUDP *NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire3( __WILLBEOWNED NPacket*,
	__WILLBEOWNED NDetailIP*,
	NU32 port );

/**
 * Construire un packet
 *
 * @param src
 *		La source
 *
 * @return l'instance
 */
__ALLOC NPacketUDP *NLib_Module_Reseau_UDP_Packet_NPacketUDP_Construire4( const NPacketUDP* );

/**
 * Detruire l'instance
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_UDP_Packet_NPacketUDP_Detruire( NPacketUDP** );

/**
 * Obtenir le packet
 *
 * @param this
 *		Cette instance
 *
 * @return le packet
 */
const NPacket *NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirPacket( const NPacketUDP* );

/**
 * Obtenir detail IP
 *
 * @param this
 *		Cette instance
 *
 * @return le detail IP
 */
const NDetailIP *NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirDetailIP( const NPacketUDP* );

/**
 * Obtenir adressage
 *
 * @param this
 *		Cette instance
 *
 * @return l'adressage
 */
const SOCKADDR_IN *NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirAdressage( const NPacketUDP* );

/**
 * Obtenir port
 *
 * @param this
 * 		Cette instance
 *
 * @return le port
 */
NU32 NLib_Module_Reseau_UDP_Packet_NPacketUDP_ObtenirPort( const NPacketUDP* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_UDP_PACKET_NPACKETUDP_PROTECT

