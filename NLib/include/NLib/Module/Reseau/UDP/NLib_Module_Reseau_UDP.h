#ifndef NLIB_MODULE_RESEAU_UDP_PROTECT
#define NLIB_MODULE_RESEAU_UDP_PROTECT

#ifdef NLIB_MODULE_RESEAU
// -----------------------------------
// namespace NLib::Module::Reseau::UDP
// -----------------------------------

// namespace NLib::Module::Reseau::UDP::Packet
#include "Packet/NLib_Module_Reseau_UDP_Packet.h"

// namespace NLib::Module::Reseau::UDP::Client
#include "Client/NLib_Module_Reseau_UDP_Client.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_UDP_PROTECT

