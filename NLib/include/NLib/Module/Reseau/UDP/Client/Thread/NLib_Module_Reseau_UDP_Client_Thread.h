#ifndef NLIB_MODULE_RESEAU_UDP_CLIENT_THREAD_PROTECT
#define NLIB_MODULE_RESEAU_UDP_CLIENT_THREAD_PROTECT

#ifdef NLIB_MODULE_RESEAU

// ---------------------------------------------------
// namespace NLib::Module::Reseau::UDP::Client::Thread
// ---------------------------------------------------

/**
 * Thread reception
 *
 * @param client
 *		Le client
 *
 * @return OK?
 */
NBOOL NLib_Module_Reseau_UDP_Client_Thread_Reception( NClientUDP* );

/**
 * Thread emission
 *
 * @param client
 *		Le client
 *
 * @return OK?
 */
NBOOL NLib_Module_Reseau_UDP_Client_Thread_Emission( NClientUDP* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_UDP_CLIENT_THREAD_PROTECT

