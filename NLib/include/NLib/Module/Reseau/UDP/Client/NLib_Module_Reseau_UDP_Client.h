#ifndef NLIB_MODULE_RESEAU_UDP_CLIENT_PROTECT
#define NLIB_MODULE_RESEAU_UDP_CLIENT_PROTECT

#ifdef NLIB_MODULE_RESEAU

// -------------------------------------------
// namespace NLib::Module::Reseau::UDP::Client
// -------------------------------------------

// struct NLib::Module::Reseau::UDP::Client::NClientUDP
#include "NLib_Module_Reseau_UDP_Client_NClientUDP.h"

// namespace NLib::Module::Reseau::UDP::Client::Thread
#include "Thread/NLib_Module_Reseau_UDP_Client_Thread.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_UDP_CLIENT_PROTECT

