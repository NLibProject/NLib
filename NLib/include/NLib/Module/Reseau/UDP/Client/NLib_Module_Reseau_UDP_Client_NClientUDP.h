#ifndef NLIB_MODULE_RESEAU_UDP_CLIENT_NCLIENTUDP_PROTECT
#define NLIB_MODULE_RESEAU_UDP_CLIENT_NCLIENTUDP_PROTECT

#ifdef NLIB_MODULE_RESEAU

// ----------------------------------------------------
// struct NLib::Module::Reseau::UDP::Client::NClientUDP
// ----------------------------------------------------

typedef struct NClientUDP
{
	// Cache du packet (envoi)
	NCachePacket *m_cachePacket;

	// Usine (reception)
	NUsineUDP *m_usine;

	// SOCKET
	SOCKET m_socket;

	// Est en cours?
	NBOOL m_estEnCours;

	// Est erreur?
	NBOOL m_estErreur;

	// Threads
	NThread *m_threadEmission;
	NThread *m_threadReception;

	// Mode reception active?
	NBOOL m_estModeReceptionActive;

	// Utilise l'usine?
	NBOOL m_estUtiliseUsine;

	// Donnees client (personnalisable)
	void *m_donneeClient;

	// Callback reception
	NBOOL ( *m_callbackReceptionPacket )( const NPacketUDP*,
		const void *clientUDP );
} NClientUDP;

/**
 * Construire le client (mode emission/reception)
 *
 * @param port
 *		Le port d'ecoute
 * @param callbackReception
 *		Le callback de reception
 * @param timeoutReception
 *		Le timeout en reception
 *
 * @return l'instance
 */
__ALLOC NClientUDP *NLib_Module_Reseau_UDP_Client_NClientUDP_Construire( NU32 port,
	NBOOL ( *callbackReception )( const NPacketUDP*,
		const NClientUDP* ),
		NBOOL estUtiliseUsine,
		NU32 timeoutReception );

/**
 * Construire le client (uniquement en mode emission)
 *
 * @return l'instance du client
 */
__ALLOC NClientUDP *NLib_Module_Reseau_UDP_Client_NClientUDP_Construire2( void );

/**
 * Detruire le client
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_UDP_Client_NClientUDP_Detruire( NClientUDP** );

/**
 * Activer erreur
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_Reseau_UDP_Client_NClientUDP_ActiverErreur( NClientUDP* );

/**
 * Est client en cours?
 *
 * @param this
 *		Cette instance
 *
 * @return si le client est en cours
 */
NBOOL NLib_Module_Reseau_UDP_Client_NClientUDP_EstEnCours( const NClientUDP* );

/**
 * Est erreur?
 *
 * @param this
 *		Cette instance
 *
 * @return si il y a une erreur
 */
NBOOL NLib_Module_Reseau_UDP_Client_NClientUDP_EstErreur( const NClientUDP* );

/**
 * Obtenir le cache packets
 *
 * @param this
 *		Cette instance
 *
 * @return le cache packets
 */
NCachePacket *NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirCachePacket( const NClientUDP* );

/**
 * Obtenir la socket
 *
 * @param this
 *		Cette instance
 *
 * @return la socket
 */
SOCKET NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirSocket( const NClientUDP* );

/**
 * Definir les donnees client
 *
 * @param this
 *		Cette instance
 * @param donnee
 *		Les donnees a definir
 */
void NLib_Module_Reseau_UDP_Client_NClientUDP_DefinirDonneeClient( NClientUDP*,
	void *donnee );

/**
 * Obtenir les donnees client
 *
 * @param this
 *		Cette instance
 *
 * @return les donnees client
 */
void *NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirDonneeClient( const NClientUDP* );

/**
 * Ajouter un packet
 *
 * @param this
 *		Cette instance
 * @param packet
 *		Le packet a ajouter
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Reseau_UDP_Client_NClientUDP_AjouterPacket( NClientUDP*,
	__WILLBEOWNED NPacketUDP* );

/**
 * Ajouter un packet
 *
 * @param this
 *		Cette instance
 * @param packet
 *		Le packet a ajouter
 * @param ip
 *		Le detail concernant le destinataire
 * @param port
 * 		Le port de destination
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Reseau_UDP_Client_NClientUDP_AjouterPacket2( NClientUDP*,
	__WILLBEOWNED NPacket*,
	__WILLBEOWNED NDetailIP*,
	NU32 port );

/**
 * Ajouter un packet
 *
 * @param this
 *		Cette instance
 * @param packet
 *		Le packet a ajouter
 * @param ip
 *		L'ip du destinataire
 * @param protocole
 *		IPv4 ou IPv6
 * @param port
 *		Le port de destination
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_Reseau_UDP_Client_NClientUDP_AjouterPacket3( NClientUDP*,
	__WILLBEOWNED NPacket*,
	const char *ip,
	NProtocoleIP,
	NU32 port );

/**
 * Obtenir usine UDP
 *
 * @param this
 *		Cette instance
 *
 * @return l'instance de l'usine
 */
const NUsineUDP *NLib_Module_Reseau_UDP_Client_NClientUDP_ObtenirUsine( const NClientUDP* );

/**
 * Est usine active?
 *
 * @param this
 *		Cette instance
 *
 * @return si l'usine est active
 */
NBOOL NLib_Module_Reseau_UDP_Client_NClientUDP_EstUsineActive( const NClientUDP* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_UDP_CLIENT_NCLIENTUDP_PROTECT

