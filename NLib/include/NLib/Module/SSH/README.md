Module SSH
==========

Introduction
------------

Ce module offre 2 modes de connexion SSH:

- Shell classique
- Execution directe

Ainsi qu'un client SFTP.

Shell classique
---------------

Il s'agit ici d'entrees sorties classiques avec possibilite de saisie textuelle clavier

Execution directe
-----------------

En mode execution directe, on transmet directement une commande
a executer au serveur SSH:

```C
    NLib_Module_SSH_NClientSSH_Executer( );
			
    while( !NLib_Module_SSH_NClientSSH_EstExecutionTermine( ) )
        NLib_Module_SSH_NClientSSH_LireFlux( );
```

Ou si le retour doit etre ignore:
```C
    NLib_Module_SSH_NClientSSH_Executer( );
			
    NLib_Module_SSH_NClientSSH_FlushEntree( );
```

Client sftp
-----------

Le client SFTP est bloquant!

Si besoin d'execution asynchrone, la gestion des threads n'est pas
assuree par ce module.

Auteur
------

SOARES Lucas <lucas.soares.npro@gmail.com>
