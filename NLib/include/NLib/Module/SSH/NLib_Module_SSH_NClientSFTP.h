#ifndef NLIB_MODULE_SSH_NCLIENTSFTP_PROTECT
#define NLIB_MODULE_SSH_NCLIENTSFTP_PROTECT

#ifdef NLIB_MODULE_SSH

// -------------------------------------
// struct NLib::Module::SSH::NClientSFTP
// -------------------------------------

typedef struct NClientSFTP
{
	// Client SSH
	NClientSSH *m_client;

	// Session SSH
	LIBSSH2_SESSION *m_sessionSSH;

	// Session SFTP
	LIBSSH2_SFTP *m_sessionSFTP;
} NClientSFTP;

/**
 * Construire client
 *
 * @param domaine
 * 		Le nom de domaine auquel se connecter
 * @param port
 * 		Le port auquel se connecter
 * @param authentification
 * 		Les informations d'authentification
 *
 * @return l'instance du client SFTP
 */
__ALLOC NClientSFTP *NLib_Module_SSH_NClientSFTP_Construire( const char *domaine,
	NU32 port,
	__WILLBEOWNED NMethodeAuthentificationSSH *authentification );

/**
 * Detruire client
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_SSH_NClientSFTP_Detruire( NClientSFTP** );

/**
 * Creer repertoire
 *
 * @param this
 * 		Cette instance
 * @param repertoire
 * 		Le repertoire a creer sur le serveur distant
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_SSH_NClientSFTP_CreerRepertoire( NClientSFTP*,
	const char *repertoire );

/**
 * Supprimer repertoire
 *
 * @param this
 * 		Cette instance
 * @param repertoire
 * 		Le repertoire a supprimer
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_SSH_NClientSFTP_SupprimerRepertoire( NClientSFTP*,
	const char *repertoire );

/**
 * Renommer fichier
 *
 * @param this
 * 		Cette instance
 * @param ancienNom
 * 		Ancien nom fichier
 * @param nouveauNom
 * 		Le nouveau nom fichier
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_SSH_NClientSFTP_Renommer( NClientSFTP*,
	const char *ancienNom,
	const char *nouveauNom );

/**
 * Envoyer fichier disque
 *
 * @param this
 * 		Cette instance
 * @param lienFichier
 * 		Le lien vers le fichier local
 * @param destination
 * 		La destination sur le serveur distant
 * @param estAutoriseEcrase
 * 		On autorise a ecraser si le fichier est deja present?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_SSH_NClientSFTP_EnvoyerFichier( NClientSFTP*,
	const char *lienFichier,
	const char *destination,
	NBOOL estAutoriseEcrase );

/**
 * Envoyer fichier memoire
 *
 * @param this
 * 		Cette instance
 * @param fichier
 * 		Le fichier local
 * @param tailleFichier
 * 		La taille du fichier local
 * @param destination
 * 		La destination sur le serveur distant
 * @param estAutoriseEcrase
 * 		On autorise a ecraser si le fichier est deja present?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_SSH_NClientSFTP_EnvoyerFichier2( NClientSFTP*,
	const char *fichier,
	NU64 tailleFichier,
	const char *destination,
	NBOOL estAutoriseEcrase );

/**
 * Recevoir un fichier en le mettant sur le disque
 *
 * @param this
 * 		Cette instance
 * @param lienFichier
 * 		Le lien vers le fichier sur le serveur distant
 * @param destination
 * 		Le lien vers la destination sur le disque local
 * @param estAutoriseEcrase
 * 		On est autorise a ecraser le fichier si il est deja present?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_SSH_NClientSFTP_RecevoirFichier( NClientSFTP*,
	const char *lienFichier,
	const char *destination,
	NBOOL estAutoriseEcrase );

/**
 * Recevoir un fichier en le mettant en memoire
 *
 * @param this
 * 		Cette instance
 * @param lienFichier
 * 		Le lien vers le fichier sur le serveur distant
 *
 * @return le fichier
 */
__ALLOC NData *NLib_Module_SSH_NClientSFTP_RecevoirFichier2( NClientSFTP*,
	const char *lienFichier );

/**
 * Lister les fichiers du repertoire distant
 *
 * @param this
 * 		Cette instance
 * @param repertoire
 * 		Le repertoire distant dont il faut lister les fichiers
 *
 * @return la liste des fichiers (NListe<char*>)
 */
__ALLOC NListe *NLib_Module_SSH_NClientSFTP_ListerFichierRepertoire( NClientSFTP*,
	const char *repertoire );

/**
 * Est ce que ce fichier distant est un fichier?
 *
 * @param this
 * 		Cette instance
 * @param fichierDistant
 * 		Le fichier distant
 *
 * @return si le fichier distant est un fichier
 */
NBOOL NLib_Module_SSH_NClientSFTP_EstFichier( NClientSFTP*,
	const char *fichierDistant );

/**
 * Telecharger tous les fichiers d'un repertoire vers un repertoire
 *
 * @param this
 * 		Cette instance
 * @param repertoireDistant
 * 		Le repertoire d'ou telecharger tous les fichiers
 * @param repertoireLocal
 * 		Le repertoire local ou stocker tous les fichiers
 * @param estAutoriseEcrase
 * 		On a le droit d'ecraser le fichier s'il existe deja?
 * @param estRecursif
 * 		La copie est recursive?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Module_SSH_NClientSFTP_TelechargerTousFichierRepertoire( NClientSFTP *this,
	const char *repertoireDistant,
	const char *repertoireLocal,
	NBOOL estAutoriseEcrase,
	NBOOL estRecursif );

#endif // NLIB_MODULE_SSH

#endif // !NLIB_MODULE_SSH_NCLIENTSFTP_PROTECT
