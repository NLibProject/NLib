#ifndef NLIB_MODULE_SSH_NMODESSH_PROTECT
#define NLIB_MODULE_SSH_NMODESSH_PROTECT

#ifdef NLIB_MODULE_SSH

// --------------------------------
// enum NLib::Module::SSH::NModeSSH
// --------------------------------

typedef enum NModeSSH
{
	NMODE_SSH_RAW,
	NMODE_SSH_EXEC,

	NMODES_SSH
} NModeSSH;

#endif // NLIB_MODULE_SSH

#endif // !NLIB_MODULE_SSH_NMODESSH_PROTECT
