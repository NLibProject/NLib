#ifndef NLIB_MODULE_SSH_NTYPECLIENTSSH_PROTECT
#define NLIB_MODULE_SSH_NTYPECLIENTSSH_PROTECT

#ifdef NLIB_MODULE_SSH
// --------------------------------------
// enum NLib::Module::SSH::NTypeClientSSH
// --------------------------------------

typedef enum NTypeClientSSH
{
	NTYPE_CLIENT_SSH_SSH,
	NTYPE_CLIENT_SSH_SFTP,

	NTYPES_CLIENT_SSH
} NTypeClientSSH;

#endif // NLIB_MODULE_SSH

#endif // !NLIB_MODULE_SSH_NTYPECLIENTSSH_PROTECT
