#ifndef NLIB_MODULE_SSH_NTYPEMETHODEAUTHENTIFICATIONSSH_PROTECT
#define NLIB_MODULE_SSH_NTYPEMETHODEAUTHENTIFICATIONSSH_PROTECT

// -------------------------------------------------------
// enum NLib::Module::SSH::NTypeMethodeAuthentificationSSH
// -------------------------------------------------------

#ifdef NLIB_MODULE_SSH
typedef enum NTypeMethodeAuthentificationSSH
{
	NTYPE_METHODE_AUTHENTIFICATION_SSH_PASSWORD,
	NTYPE_METHODE_AUTHENTIFICATION_SSH_RSA_KEY,

	NTYPES_METHODE_AUTHENTIFICATION_SSH
} NTypeMethodeAuthentificationSSH;
#endif // NLIB_MODULE_SSH

#endif // !NLIB_MODULE_SSH_NTYPEMETHODEAUTHENTIFICATIONSSH_PROTECT

