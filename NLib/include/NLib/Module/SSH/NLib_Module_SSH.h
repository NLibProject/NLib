#ifndef NLIB_MODULE_SSH_PROTECT
#define NLIB_MODULE_SSH_PROTECT

// ---------------------------
// namespace NLib::Module::SSH
// ---------------------------

#ifdef NLIB_MODULE_SSH
// poll
#include <poll.h>

// libssh2
#include <libssh2.h>
#include <libssh2_sftp.h>

/**
 * Initialiser libssh
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_SSH_Initialiser( void );

/**
 * Detruire libssh
 */
void NLib_Module_SSH_Detruire( void );

// Timeout flush (ms)
#define NLIB_MODULE_SSH_TEMPS_AVANT_TIMEOUT_FLUSH		300

// Timeout exec (ms)
#define NLIB_MODULE_SSH_TIMEOUT_EXEC					500

// enum NLib::Module::SSH::NTypeMethodeAuthentificationSSH
#include "NLib_Module_SSH_NTypeMethodeAuthentificationSSH.h"

// struct NLib::Module::SSH::NMethodeAuthentificationSSH
#include "NLib_Module_SSH_NMethodeAuthentificationSSH.h"

// enum NLib::Module::SSH::NModeSSH
#include "NLib_Module_SSH_NModeSSH.h"

// enum NLib::Module::SSH::NTypeClientSSH
#include "NLib_Module_SSH_NTypeClientSSH.h"

// struct NLib::Module::SSH::NClientSSH
#include "NLib_Module_SSH_NClientSSH.h"

// struct NLib::Module::SSH::NClientSFTP
#include "NLib_Module_SSH_NClientSFTP.h"

/**
 * Download file via SFTP
 *
 * @param hostname
 * 		The remote hostname
 * @param port
 * 		The remote port
 * @param authenticationDetail
 * 		The authentication details
 * @param remoteLocation
 * 		The remote location
 * @param localDestination
 * 		The local file destination
 *
 * @return if the download succeeded
 */
NBOOL NLib_Module_SSH_DownloadFileSFTP( const char *hostname,
	NU32 port,
	NMethodeAuthentificationSSH *authenticationDetail,
	const char *remoteLocation,
	const char *localDestination );
#endif // NLIB_MODULE_SSH

#endif // !NLIB_MODULE_SSH_PROTECT
