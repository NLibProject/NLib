#ifndef NLIB_MODULE_SSH_NMETHODEAUTHENTIFICATIONSSH_PROTECT
#define NLIB_MODULE_SSH_NMETHODEAUTHENTIFICATIONSSH_PROTECT

// -----------------------------------------------------
// struct NLib::Module::SSH::NMethodeAuthentificationSSH
// -----------------------------------------------------

#ifdef NLIB_MODULE_SSH
typedef struct NMethodeAuthentificationSSH
{
	// Utilisateur
	char *m_utilisateur;

	// Type d'authentification
	NTypeMethodeAuthentificationSSH m_type;

	// Donnees d'authentification (un seul a la fois suivant le type)
	union
	{
		// Mot de passe
		char *m_password;

		// Clef publique/privee
		struct
		{
			// Clef publique
			char *m_clefPublique;

			// Clef privee
			char *m_clefPrivee;
		} m_clef;
	} m_data;
} NMethodeAuthentificationSSH;

/**
 * Construire authentification mot de passe
 *
 * @param utilisateur
 * 		L'utilisateur
 * @param motDePasse
 * 		Le mot de passe
 *
 * @return l'instance
 */
__ALLOC NMethodeAuthentificationSSH* NLib_Module_SSH_NMethodeAuthentificationSSH_Construire( const char *utilisateur,
	const char *motDePasse );

/**
 * Construire authentification clef publique/privee
 *
 * @param utilisateur
 * 		L'utilisateur
 * @param clefPublique
 * 		La clef publique
 * @param estClefPubliqueFichier
 * 		Doit-on chercher la clef dans le fichier %clefPublique%?
 * @param clefPrivee
 * 		La clef privee
 * @param estClefPriveeFichier
 * 		Doit-on chercher la clef dans le fichier %clefPrivee%?
 *
 * @return l'instance
 */
__ALLOC NMethodeAuthentificationSSH *NLib_Module_SSH_NMethodeAuthentificationSSH_Construire2( const char *utilisateur,
	__MUSTNOTBENULL const char *clefPublique,
	NBOOL estClefPubliqueFichier,
	__MUSTNOTBENULL const char *clefPrivee,
	NBOOL estClefPriveeFichier );

/**
 * Detruire instance
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_SSH_NMethodeAuthentificationSSH_Detruire( NMethodeAuthentificationSSH** );

/**
 * Obtenir type d'authentification
 *
 * @param this
 * 		Cette instance
 *
 * @return le type d'authentification
 */
NTypeMethodeAuthentificationSSH NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirType( const NMethodeAuthentificationSSH* );

/**
 * Obtenir le mot de passe (si type == NTYPE_METHODE_AUTHENTIFICATION_SSH_PASSWORD)
 *
 * @param this
 * 		Cette instance
 *
 * @return le mot de passe
 */
const char *NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirPassword( const NMethodeAuthentificationSSH* );

/**
 * Obtenir clef privee (si type == NTYPE_METHODE_AUTHENTIFICATION_SSH_RSA_KEY)
 *
 * @param this
 * 		Cette instance
 *
 * @return la clef privee
 */
const char *NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirClefPrivee( const NMethodeAuthentificationSSH* );

/**
 * Obtenir clef publique (si type == NTYPE_METHODE_AUTHENTIFICATION_SSH_RSA_KEY)
 *
 * @param this
 * 		Cette instance
 *
 * @return la clef privee
 */
const char *NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirClefPublique( const NMethodeAuthentificationSSH* );

/**
 * Obtenir utilisateur
 *
 * @param this
 * 		Cette instance
 *
 * @return l'utilisateur
 */
const char *NLib_Module_SSH_NMethodeAuthentificationSSH_ObtenirUtilisateur( const NMethodeAuthentificationSSH* );
#endif // NLIB_MODULE_SSH

#endif // !NLIB_MODULE_SSH_NMETHODEAUTHENTIFICATIONSSH_PROTECT
