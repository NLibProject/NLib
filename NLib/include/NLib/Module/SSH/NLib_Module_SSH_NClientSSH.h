#ifndef NLIB_MODULE_SSH_NCLIENTSSH_PROTECT
#define NLIB_MODULE_SSH_NCLIENTSSH_PROTECT

// ------------------------------------
// struct NLib::Module::SSH::NClientSSH
// ------------------------------------

#ifdef NLIB_MODULE_SSH
typedef struct NClientSSH
{
	// La socket
	SOCKET m_socket;

	// Type
	NTypeClientSSH m_type;

	// Mode
	NModeSSH m_mode;

	// Le nom de domaine auquel se connecter
	char *m_domaine;

	// IP
	char *m_ip;

	// Le port auquel se connecter
	NU32 m_port;

	// Authentification
	NMethodeAuthentificationSSH *m_authentification;

	// Session SSH
	LIBSSH2_SESSION *m_sessionSSH;

	// Instance shell
	LIBSSH2_CHANNEL *m_shell;

	// L'execution est en cours?
	NBOOL m_estExecutionEnCours;
} NClientSSH;

/**
 * Construire client SSH
 *
 * @param domaine
 * 		Le nom de domaine
 * @param port
 * 		Le port
 * @param authentification
 * 		Les informations d'authentification
 * @param mode
 * 		Le mode ssh souhaite
 *
 * @return le client
 */
__ALLOC NClientSSH *NLib_Module_SSH_NClientSSH_Construire( const char *domaine,
	NU32 port,
	__WILLBEOWNED NMethodeAuthentificationSSH *authentification,
	NModeSSH mode );

/**
 * Detruire client
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_SSH_NClientSSH_Detruire( NClientSSH** );

/**
 * Lire flux en entree
 *
 * @param this
 * 		Cette instance
 * @param timeout
 * 		Le timeout (ms) (0 = pas de timeout)
 *
 * @return les donnees lues
 */
__ALLOC char *NLib_Module_SSH_NClientSSH_LireFlux( NClientSSH*,
	NU32 timeout );

/**
 * Flush flux entree
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_SSH_NClientSSH_FlushEntree( NClientSSH* );

/**
 * Executer une commande
 *
 * @param this
 * 		Cette instance
 * @param commande
 * 		La commande a executer
 *
 * @return si l'operation a reussie
 */
NBOOL NLib_Module_SSH_NClientSSH_Executer( NClientSSH*,
	const char *commande );

/**
 * L'execution est terminee? (valable pour exec, pour shell, retourne toujours vrai)
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'execution de la commande est terminee
 */
NBOOL NLib_Module_SSH_NClientSSH_EstExecutionTermine( const NClientSSH* );

#endif // NLIB_MODULE_SSH

#endif // !NLIB_MODULE_SSH_NCLIENTSSH_PROTECT

