#ifndef NLIB_MODULE_SDL_BOUTON_NBOUTON_PROTECT
#define NLIB_MODULE_SDL_BOUTON_NBOUTON_PROTECT

/*
	Objet bouton permettant une gestion de cadre
	pointe/clique avec la souris, ainsi qu'un de
	placement auto-gere

	@author SOARES Lucas
*/

#ifdef NLIB_MODULE_SDL

// -----------------------------------------
// struct NLib::Module::SDL::Bouton::NBouton
// -----------------------------------------

typedef struct
{
	// Cadre
	NCadre *m_cadre;

	// Etat
	NEtatBouton m_etat;

	// Bouton avec lequel le clic a ete effectue
	NU32 m_bouton;
} NBouton;

/* Construire l'objet */
__ALLOC NBouton *NLib_Module_SDL_Bouton_NBouton_Construire( NSPoint position,
	NUPoint taille,
	NCouleur couleur,
	NCouleur couleurFond,
	const NFenetre *fenetre,
	NU32 epaisseur );

/* Detruire l'objet */
void NLib_Module_SDL_Bouton_NBouton_Detruire( NBouton** );

/* Mettre a jour */
void NLib_Module_SDL_Bouton_NBouton_Update( NBouton*,
	const NSPoint *positionSouris,
	const SDL_Event *evt );

/* Definir position */
void NLib_Module_SDL_Bouton_NBouton_DefinirPosition( NBouton*,
	NSPoint );

/* Definir taille */
void NLib_Module_SDL_Bouton_NBouton_DefinirTaille( NBouton*,
	NUPoint );

/* Definir couleur */
void NLib_Module_SDL_Bouton_NBouton_DefinirCouleur( NBouton*,
	NCouleur );

/* Definir couleur fond */
void NLib_Module_SDL_Bouton_NBouton_DefinirCouleurFond( NBouton*,
	NCouleur );

/* Definir epaisseur */
void NLib_Module_SDL_Bouton_NBouton_DefinirEpaisseur( NBouton*,
	NU32 );

/* Obtenir position */
NSPoint NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( const NBouton* );

/* Obtenir taille */
NUPoint NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( const NBouton* );

/* Obtenir couleur */
NCouleur NLib_Module_SDL_Bouton_NBouton_ObtenirCouleur( const NBouton* );

/* Obtenir couleur fond */
NCouleur NLib_Module_SDL_Bouton_NBouton_ObtenirCouleurFond( const NBouton* );

/* Obtenir epaisseur */
NU32 NLib_Module_SDL_Bouton_NBouton_ObtenirEpaisseur( const NBouton* );

/* Obtenir bouton d'activation */
NU32 NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( const NBouton* );

/* Obtenir etat */
NEtatBouton NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( const NBouton* );

/* Remettre a zero */
void NLib_Module_SDL_Bouton_NBouton_RemettreEtatAZero( NBouton* );

/* Dessiner */
void NLib_Module_SDL_Bouton_NBouton_Dessiner( const NBouton* );

/* Deplacer */
void NLib_Module_SDL_Bouton_NBouton_Deplacer( NBouton*,
	NSPoint positionSouris,
	NBOOL continuerDeplacement );

/**
 * Est visibile?
 *
 * @param this
 *		Cette instance
 *
 * @return si le bouton est visible
 */
NBOOL NLib_Module_SDL_Bouton_NBouton_EstVisible( const NBouton* );

/**
 * Definir si le bouton est visible
 *
 * @param this
 *		Cette instance
 * @param estVisible
 *		Visible?
 */
void NLib_Module_SDL_Bouton_NBouton_DefinirEstVisible( NBouton*,
	NBOOL estVisible );

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_BOUTON_NBOUTON_PROTECT

