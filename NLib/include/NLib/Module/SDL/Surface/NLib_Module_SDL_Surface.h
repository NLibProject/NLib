#ifndef NLIB_MODULE_SDL_SURFACE_PROTECT
#define NLIB_MODULE_SDL_SURFACE_PROTECT

/*
	Ensemble des differents types de surfaces affichables sur
	un objet de type NFenetre

	@author SOARES Lucas
*/

// ------------------------------------
// namespace NLib::Module::SDL::Surface
// ------------------------------------

#ifdef NLIB_MODULE_SDL
// Niveau de zoom par defaut pour les animations
static const NU32 NLIB_MODULE_SDL_SURFACE_NANIMATION_NIVEAU_ZOOM_DEFAUT	= 1;

// struct NLib::Module::SDL::Surface::NSurface
#include "NLib_Module_SDL_Surface_NSurface.h"

// struct NLib::Module::SDL::Surface::NTileset
#include "NLib_Module_SDL_Surface_NTileset.h"

// struct NLib::Module::SDL::Surface::NAnimation
#include "NLib_Module_SDL_Surface_NAnimation.h"

/* Creer une surface */
__ALLOC SDL_Surface *NLib_Module_SDL_Surface_Creer( NU32 w,
	NU32 h,
	NBOOL filtre );

/* Copier une surface */
// Vers une surface deja existante
NBOOL NLib_Module_SDL_Surface_Copier( const SDL_Surface *src,
	const SDL_Rect *zoneSrc,
	SDL_Surface *dst,
	const SDL_Rect *zoneDst );

// Vers une surface creee pour l'occasion
__ALLOC SDL_Surface *NLib_Module_SDL_Surface_Copier2( const SDL_Surface *src,
	const SDL_Rect *zone );

/* Put pixel */
NBOOL NLib_Module_SDL_Surface_Putpixel( NU32 x,
	NU32 y,
	NU8 **pixels,
	NU32 pitch,
	NU32 bpp,
	NU32 pixel );

NBOOL NLib_Module_SDL_Surface_Putpixel2( NU32 x,
	NU32 y,
	SDL_Surface *s,
	NU32 pixel );

/* Get pixel */
NU32 NLib_Module_SDL_Surface_Getpixel( NU32 x,
	NU32 y,
	const NU8 **pixels,
	NU32 pitch,
	NU32 bpp );

NU32 NLib_Module_SDL_Surface_Getpixel2( NU32 x,
	NU32 y,
	const SDL_Surface *s );

/* Inverser */
__ALLOC SDL_Surface *NLib_Module_SDL_Surface_Inverser( const SDL_Surface *entree, 
	NBOOL verticalement,
	NBOOL horizontalement );

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_SURFACE_PROTECT

