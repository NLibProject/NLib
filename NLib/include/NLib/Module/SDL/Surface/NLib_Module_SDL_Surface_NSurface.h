#ifndef NLIB_MODULE_SDL_SURFACE_NSURFACE_PROTECT
#define NLIB_MODULE_SDL_SURFACE_NSURFACE_PROTECT

/*
	Une surface affichage sur un objet de type
	NFenetre. Peut etre cree manuellement, ou
	charge a partir d'une image

	@author SOARES Lucas
*/

// -------------------------------------------
// struct NLib::Module::SDL::Surface::NSurface
// -------------------------------------------

#ifdef NLIB_MODULE_SDL
typedef struct
{
	// Fenetre relative a cette surface
	const NFenetre *m_handleFenetre;

	// Surface
	SDL_Surface *m_surface;

	// Texture
	SDL_Texture *m_texture;

	// Est texture a jour
	NBOOL m_estTextureUpdate;

	// Taille
	NUPoint m_taille;

	// Position
	NSPoint m_position;

	// Zoom
	NS32 m_zoom;
} NSurface;

/* Construire la surface */
__ALLOC NSurface *NLib_Module_SDL_Surface_NSurface_Construire( const NFenetre*,
	NUPoint taille );
#ifdef NLIB_MODULE_SDL_IMAGE
__ALLOC NSurface *NLib_Module_SDL_Surface_NSurface_Construire2( const NFenetre*,
	const char *lien );
#endif // NLIB_MODULE_SDL_IMAGE
__ALLOC NSurface *NLib_Module_SDL_Surface_NSurface_Construire3( const NFenetre*,
	__WILLBEOWNED SDL_Surface* );
__ALLOC NSurface *NLib_Module_SDL_Surface_NSurface_Construire4( const NFenetre*,
	__WILLBEOWNED SDL_Surface*,
	SDL_Rect );

/* Detruire la surface */
void NLib_Module_SDL_Surface_NSurface_Detruire( NSurface** );

/* Demander update texture */
void NLib_Module_SDL_Surface_NSurface_DemanderUpdateTexture( NSurface* );

/* Definir position */
void NLib_Module_SDL_Surface_NSurface_DefinirPosition( NSurface*,
	NS32 x,
	NS32 y );
void NLib_Module_SDL_Surface_NSurface_DefinirPosition2( NSurface*,
	NSPoint );

/**
 * Definir taille
 *
 * @param this
 *		Cette instance
 * @param w
 *		La taille x
 * @param h
 *		La taille y
 */
void NLib_Module_SDL_Surface_NSurface_DefinirTaille( NSurface*,
	NU32 w,
	NU32 h );

/**
 * Restaurer la taille
 *
 * @param this
 *		Cette instance
 */
void NLib_Module_SDL_Surface_NSurface_RestaurerTaille( NSurface* );

/* Definir zoom */
void NLib_Module_SDL_Surface_NSurface_DefinirZoom( NSurface*,
	NS32 zoom );

/* Definir modification couleur */
void NLib_Module_SDL_Surface_NSurface_DefinirModificationCouleur( NSurface*,
	NU8 r,
	NU8 g,
	NU8 b );

/* Definir modification alpha */
void NLib_Module_SDL_Surface_NSurface_DefinirModificationAlpha( NSurface*,
	NU8 a );

/* Afficher la texture */
void NLib_Module_SDL_Surface_NSurface_Afficher( const NSurface* );

/* Obtenir la taille */
const NUPoint *NLib_Module_SDL_Surface_NSurface_ObtenirTaille( const NSurface* );

/* Obtenir le facteur de zoom */
const NS32 NLib_Module_SDL_Surface_NSurface_ObtenirZoom( const NSurface* );

/* Obtenir la position */
const NSPoint *NLib_Module_SDL_Surface_NSurface_ObtenirPosition( const NSurface* );

/* Obtenir la surface */
SDL_Surface *NLib_Module_SDL_Surface_NSurface_ObtenirSurface( const NSurface* );
#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_SURFACE_NSURFACE_PROTECT

