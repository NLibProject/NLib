#ifndef NLIB_MODULE_SDL_SURFACE_NTILESET_PROTECT
#define NLIB_MODULE_SDL_SURFACE_NTILESET_PROTECT

/*
	Un tileset affichable dans un objet de type
	NFenetre. Comporte une grille de case corre
	spondant chacune a une portion du tileset

	@author SOARES Lucas
*/

// -------------------------------------------
// struct NLib::Module::SDL::Surface::NTileset
// -------------------------------------------

#ifdef NLIB_MODULE_SDL
#ifdef NLIB_MODULE_SDL_IMAGE
// struct NLib::Module::SDL::Surface::NTileset
typedef struct
{
	// Surfaces
	NSurface ***m_cases;

	// Taille d'une case
	NUPoint m_tailleCase;

	// Nombre de cases dans la grille
	NUPoint m_nombreCase;
} NTileset;

/* Construire */
__ALLOC NTileset *NLib_Module_SDL_Surface_NTileset_Construire( const char*,
	NUPoint tailleCase,
	const NFenetre* );

/* Detruire */
void NLib_Module_SDL_Surface_NTileset_Detruire( NTileset** );

/* Obtenir case */
const NSurface *NLib_Module_SDL_Surface_NTileset_ObtenirCase( const NTileset*,
	NUPoint );

/* Obtenir le nombre de case */
NUPoint NLib_Module_SDL_Surface_NTileset_ObtenirNombreCase( const NTileset* );

/* Obtenir la taille d'une case */
NUPoint NLib_Module_SDL_Surface_NTileset_ObtenirTailleCase( const NTileset* );

#endif // NLIB_MODULE_SDL_IMAGE
#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_SURFACE_NTILESET_PROTECT

