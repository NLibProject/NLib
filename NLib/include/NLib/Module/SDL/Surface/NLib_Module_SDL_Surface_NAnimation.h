#ifndef NLIB_MODULE_SDL_SURFACE_NANIMATION_PROTECT
#define NLIB_MODULE_SDL_SURFACE_NANIMATION_PROTECT

// ---------------------------------------------
// struct NLib::Module::SDL::Surface::NAnimation
// ---------------------------------------------

#ifdef NLIB_MODULE_SDL
#ifdef NLIB_MODULE_SDL_IMAGE
/*
	Objet permettant l'affichage d'animation sur un objet de type NFenetre

	Un fichier image d'animation doit contenir les frames d'une animation sur
	une seule ligne. Toutes les animations similaires vont dans le meme fichi
	er et sont distinguees a l'obtention par NU32 animation
	Il est possible de specie un ordre de lecture des frames (ex un aller-ret
	our) afin de creer une animation personnalisee

	@author SOARES Lucas
*/

// struct NLib::Module::SDL::Surface::NAnimation
typedef struct
{
	// Tileset
	NTileset *m_animation;

	// Position
	NSPoint m_position;

	// Etat animation
	NEtatAnimation *m_etat;
} NAnimation;

/* Construire */
__ALLOC NAnimation *NLib_Module_SDL_Surface_NAnimation_Construire( const char *lien,
	NUPoint tailleFrame,
	NU32 delaiEntreFrame,
	const NFenetre* );
__ALLOC NAnimation *NLib_Module_SDL_Surface_NAnimation_Construire2( const char *lien,
	NUPoint tailleFrame,
	NU32 delaiEntreFrame,
	const NFenetre*,
	const NU32 *ordreEtapeAnimation,
	NU32 nombreEtapeAnimation );

/* Detruire */
void NLib_Module_SDL_Surface_NAnimation_Detruire( NAnimation** );

/* Update */
void NLib_Module_SDL_Surface_NAnimation_Update( NAnimation* );

/* Afficher */
NBOOL NLib_Module_SDL_Surface_NAnimation_Afficher( const NAnimation*,
	NU32 animation );
NBOOL NLib_Module_SDL_Surface_NAnimation_Afficher2( const NAnimation*,
	NU32 animation,
	NU32 zoom );
NBOOL NLib_Module_SDL_Surface_NAnimation_Afficher3( const NAnimation*,
	NU32 animation,
	NU32 zoom,
	NU32 frame );
NBOOL NLib_Module_SDL_Surface_NAnimation_Afficher4( const NAnimation*,
	NU32 animation,
	NU32 frame );

/* Definir la position */
void NLib_Module_SDL_Surface_NAnimation_DefinirPosition( NAnimation*,
	NSPoint position );

/* Obtenir la taille */
NUPoint NLib_Module_SDL_Surface_NAnimation_ObtenirTaille( const NAnimation* );

/* Obtenir le nombre d'animations */
NU32 NLib_Module_SDL_Surface_NAnimation_ObtenirNombreAnimation( const NAnimation* );

/* Obtenir le nombre de frames */
NU32 NLib_Module_SDL_Surface_NAnimation_ObtenirNombreFrame( const NAnimation* );

/* Obtenir la position */
NSPoint NLib_Module_SDL_Surface_NAnimation_ObtenirPosition( const NAnimation* );

/* Obtenir la frame */
NU32 NLib_Module_SDL_Surface_NAnimation_ObtenirFrame( const NAnimation* );

/* Obtenir l'etat */
const NEtatAnimation *NLib_Module_SDL_Surface_NAnimation_ObtenirEtat( const NAnimation* );

#endif // NLIB_MODULE_SDL_IMAGE
#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_SURFACE_NANIMATION_PROTECT

