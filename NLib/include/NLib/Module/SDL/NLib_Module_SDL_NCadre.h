#ifndef NLIB_MODULE_SDL_NCADRE_PROTECT
#define NLIB_MODULE_SDL_NCADRE_PROTECT

/*
	Cadre applicable sur un objet NFenetre

	@author SOARES Lucas
*/

// --------------------------------
// struct NLib::Module::SDL::NCadre
// --------------------------------

#ifdef NLIB_MODULE_SDL

typedef struct
{
	// Epaisseur
	NU32 m_epaisseur;

	// Origine
	NSPoint m_position;

	// Taille
	NUPoint m_taille;

	// Couleur
	NCouleur m_couleur;

	// Couleur fond
	NCouleur m_couleurFond;

	// Rectangles a tracer
	SDL_Rect m_coordonnee[ NDIRECTIONS + 1 ];

	// Fenetre
	const NFenetre *m_fenetre;

	// Pour deplacement
		// Position initiale
			NSPoint m_decalageDeplacement;
		// Deplacement active?
			NBOOL m_estDeplacementActif;

	// Est visible?
	NBOOL m_estVisible;
} NCadre;

/* Construire l'objet */
__ALLOC NCadre *NLib_Module_SDL_NCadre_Construire( NSPoint position,
	NUPoint taille,
	NCouleur couleur,
	NCouleur couleurFond,
	const NFenetre*,
	NU32 epaisseur );
__ALLOC NCadre *NLib_Module_SDL_NCadre_Construire2( const NCadre *src );
__ALLOC NCadre *NLib_Module_SDL_NCadre_Construire3( NS32 x,
	NS32 y,
	NU32 w,
	NU32 h,
	NCouleur couleur,
	NCouleur couleurFond,
	const NFenetre*,
	NU32 epaisseur );

/* Detruire l'objet */
void NLib_Module_SDL_NCadre_Detruire( NCadre** );

/* Obtenir position */
const NSPoint *NLib_Module_SDL_NCadre_ObtenirPosition( const NCadre* );

/* Obtenir taille */
const NUPoint *NLib_Module_SDL_NCadre_ObtenirTaille( const NCadre* );

/* Obtenir couleur */
const NCouleur *NLib_Module_SDL_NCadre_ObtenirCouleur( const NCadre* );

/* Obtenir couleur fond */
const NCouleur *NLib_Module_SDL_NCadre_ObtenirCouleurFond( const NCadre* );

/* Obtenir epaisseur */
NU32 NLib_Module_SDL_NCadre_ObtenirEpaisseur( const NCadre* );

/* Definir position */
void NLib_Module_SDL_NCadre_DefinirPosition( NCadre*,
	NS32 x,
	NS32 y );

/* Definir taille */
void NLib_Module_SDL_NCadre_DefinirTaille( NCadre*,
	NU32 x,
	NU32 y );

/* Definir couleur */
void NLib_Module_SDL_NCadre_DefinirCouleur( NCadre*,
	NU8 r,
	NU8 g,
	NU8 b,
	NU8 a );

/* Definir couleur fond */
void NLib_Module_SDL_NCadre_DefinirCouleurFond( NCadre*,
	NU8 r,
	NU8 g,
	NU8 b,
	NU8 a );

/* Definir epaisseur */
void NLib_Module_SDL_NCadre_DefinirEpaisseur( NCadre*,
	NU32 );

/* Dessiner */
void NLib_Module_SDL_NCadre_Dessiner( const NCadre* );

/* Est en colision? */
NBOOL NLib_Module_SDL_NCadre_EstEnColisionAvec( const NCadre*,
	const NSRect* );
NBOOL NLib_Module_SDL_NCadre_EstEnColisionAvec2( const NCadre*,
	const NCadre* );
NBOOL NLib_Module_SDL_NCadre_EstEnColisionAvec3( const NCadre*,
	NSPoint );

/* Calculer intersection */
__ALLOC NCadre *NLib_Module_SDL_NCadre_CalculerIntersection( const NCadre*,
	const NCadre* );
__ALLOC NCadre *NLib_Module_SDL_NCadre_CalculerIntersection2( const NCadre*,
	const NSRect* );

/* Deplacer */
void NLib_Module_SDL_NCadre_Deplacer( NCadre*,
	NSPoint positionSouris,
	NBOOL continuerDeplacement );

/**
 * Est visibile?
 *
 * @param this
 *		Cette instance
 *
 * @return si le cadre est visible
 */
NBOOL NLib_Module_SDL_NCadre_EstVisible( const NCadre* );

/**
 * Definir si le cadre est visible
 *
 * @param this
 *		Cette instance
 * @param estVisible
 *		Visible?
 */
void NLib_Module_SDL_NCadre_DefinirEstVisible( NCadre*,
	NBOOL estVisible );

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_NCADRE_PROTECT

