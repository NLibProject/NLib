#ifndef NLIB_MODULE_SDL_SAISIE_NRETOURSAISIESDL_PROTECT
#define NLIB_MODULE_SDL_SAISIE_NRETOURSAISIESDL_PROTECT

/*
	Retour de la saisie utilisateur via NSaisieSDL

	@author SOARES Lucas
*/

// -------------------------------------------------
// enum NLib::Modules::SDL::Saisie::NRetourSaisieSDL
// -------------------------------------------------

#ifdef NLIB_MODULE_SDL
typedef enum
{
	// La saisie s'est bien passee
	NRETOUR_SAISIE_SDL_OK,

	// La saisie demande a quitter
	NRETOUR_SAISIE_SDL_QUITTER,

	// La saisie notifie la pression de la touche tab
	NRETOUR_SAISIE_SDL_TAB,

	// La saisie notifie la pression du bouton signifiant l'arret
	NRETOUR_SAISIE_SDL_CLIC_BOUTON,

	// La saisie notifie la pression de echap
	NRETOUR_SAISIE_SDL_ECHAP,

	NRETOURS_SAISIE_SDL
} NRetourSaisieSDL;

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_SAISIE_NRETOURSAISIESDL_PROTECT

