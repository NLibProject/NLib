#ifndef NLIB_MODULE_SDL_PROTECT
#define NLIB_MODULE_SDL_PROTECT

/*
	Module SDL

	@author SOARES Lucas
*/

// ---------------------------
// namespace NLib::Module::SDL
// ---------------------------

#ifdef NLIB_MODULE_SDL
// SDL Header
#include <SDL2/SDL.h>

// On n'utilise pas le main de la SDL
#undef main

// namespace NLib::Module::SDL::Image
#include "Image/NLib_Module_SDL_Image.h"

// struct NLib::Module::SDL::NFenetre
#include "NLib_Module_SDL_NFenetre.h"

// namespace NLib::Module::SDL::Surface
#include "Surface/NLib_Module_SDL_Surface.h"

// namespace NLib::Module::SDL::TTF
#include "TTF/NLib_Module_SDL_TTF.h"

// struct NLib::Module::SDL::NCadre
#include "NLib_Module_SDL_NCadre.h"

// namespace NLib::Module::Scrolling
#include "Scrolling/NLib_Module_SDL_Scrolling.h"

// namespace NLib::Module::SDL::Bouton
#include "Bouton/NLib_Module_SDL_Bouton.h"

// namespace NLib::Module::SDL::Saisie
#include "Saisie/NLib_Module_SDL_Saisie.h"

/* Initialiser la SDL */
NBOOL NLib_Module_SDL_Initialiser( void );

/* Detruire la SDL */
void NLib_Module_SDL_Detruire( void );

/* Changer curseur */
void NLib_Module_SDL_DefinirCurseur( const SDL_Surface*,
	NS32 x,
	NS32 y );
void NLib_Module_SDL_DefinirCurseur2( const SDL_Surface* );

/* Restaurer curseur */
void NLib_Module_SDL_RestaurerCurseur( void );

/**
 * Obtenir la resolution actuelle
 *
 * @return la resolution actuelle
 */
NUPoint NLib_Module_SDL_ObtenirResolutionActuelle( void );

/* Erreur SDL */
static const NS32 NSDL_ERREUR = (NS32)NERREUR;

#ifdef NLIB_MODULE_SDL_INTERNE
static SDL_Cursor *m_curseur = NULL;
static SDL_Cursor *m_curseurDefaut = NULL;
#endif // NLIB_MODULE_SDL_INTERNE

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_PROTECT

