#ifndef NLIB_MODULE_SDL_TTF_NPOLICE_PROTECT
#define NLIB_MODULE_SDL_TTF_NPOLICE_PROTECT

/*
	Objet NPolice permettant la creation de
	texte affichable dans une NFenetre

	@author SOARES Lucas
*/

// --------------------------------------
// struct NLib::Module::SDL::TTF::NPolice
// --------------------------------------

#ifdef NLIB_MODULE_SDL_TTF
// struct NLib::Module::SDL::TTF::NPolice
typedef struct
{
	// Police
	TTF_Font *m_police;

	// Taille
	NU32 m_taille;

	// Couleur
	NCouleur m_couleur;
} NPolice;

/* Construire la police */
__ALLOC NPolice *NLib_Module_SDL_TTF_NPolice_Construire( const char *lien,
	NU32 taille );
__ALLOC NPolice *NLib_Module_SDL_TTF_NPolice_Construire2( const char *lien,
	NU32 taille,
	NCouleur );

/* Detruire la police */
void NLib_Module_SDL_TTF_NPolice_Detruire( NPolice** );

/* Creer un texte */
__ALLOC NSurface *NLib_Module_SDL_TTF_NPolice_CreerTexte( const NPolice*,
	const NFenetre*,
	const char *texte );

/* Definir la couleur */
void NLib_Module_SDL_TTF_NPolice_DefinirCouleur( NPolice*,
	NU8 r,
	NU8 g,
	NU8 b );

/* Definir le style */
void NLib_Module_SDL_TTF_NPolice_DefinirStyle( NPolice*,
	NStylePolice );

/* Obtenir la taille */
NU32 NLib_Module_SDL_TTF_NPolice_ObtenirTaille( const NPolice* );

/* Calculer taille texte */
NUPoint NLib_Module_SDL_TTF_NPolice_CalculerTailleTexte( const NPolice*,
	const char *texte );

#endif // NLIB_MODULE_SDL_TTF

#endif // !NLIB_MODULE_SDL_TTF_NPOLICE_PROTECT

