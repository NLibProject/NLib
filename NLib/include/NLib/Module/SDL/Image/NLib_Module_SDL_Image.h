#ifndef NLIB_MODULE_SDL_IMAGE_PROTECT
#define NLIB_MODULE_SDL_IMAGE_PROTECT

/*
	Module SDL Image

	@author SOARES Lucas
*/

// ----------------------------------
// namespace NLib::Module::SDL::Image
// ----------------------------------

#ifdef NLIB_MODULE_SDL_IMAGE
// SDL_Image header
#include <SDL2/SDL_image.h>

/* Initialiser SDL Image */
NBOOL NLib_Module_SDL_Image_Initialiser( void );

/* Detruire la SDL Image */
void NLib_Module_SDL_Image_Detruire( void );

/* Charger une image */
__ALLOC SDL_Surface *NLib_Module_SDL_Image_Charger( const char* );

#endif // NLIB_MODULE_SDL_IMAGE

#endif // !NLIB_MODULE_SDL_IMAGE_PROTECT

