#ifndef NLIB_MODULE_FMODEX_NSON_PROTECT
#define NLIB_MODULE_FMODEX_NSON_PROTECT

// ---------------------------------
// struct NLib::Module::FModex::NSon
// ---------------------------------

#ifdef NLIB_MODULE_FMODEX
// struct NLib::Module::FModex::NSon
typedef struct
{
	// Son
	FMOD_SOUND *m_son;

	// Volume (Entre 1 et 100)
	NU32 m_volume;

	// Contexte
	const FMOD_SYSTEM *m_contexte;

	// Canal
	FMOD_CHANNEL *m_canal;
} NSon;

/**
 * Construire le son
 *
 * @param lien
 * 		Le lien vers le fichier audio
 * @param volume
 * 		Le volume (1 - 100)
 *
 * @return l'instance
 */
__ALLOC NSon *NLib_Module_FModex_NSon_Construire( const char*,
	NU32 volume );

/**
 * Detruire l'instance
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_FModex_NSon_Detruire( NSon** );

/**
 * Lancer la lecture
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NSon_Lire( NSon* );

/**
 * Definir le volume
 *
 * @param this
 * 		Cette instance
 * @param volume
 * 		Le volume a definir (1 - 100)
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NSon_DefinirVolume( NSon*,
	NU32 );

/**
 * Est en lecture?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le son est en lecture
 */
NBOOL NLib_Module_FModex_NSon_EstLecture( NSon* );

/**
 * Obtenir position (ms)
 *
 * @param this
 * 		Cette instance
 *
 * @return la position dans le son (ms)
 */
NU32 NLib_Module_FModex_NSon_ObtenirPosition( NSon* );

#endif // NLIB_MODULE_FMODEX

#endif // !NLIB_MODULE_FMODEX_NSON_PROTECT

