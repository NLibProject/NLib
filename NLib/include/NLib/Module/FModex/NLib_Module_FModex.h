#ifndef NLIB_MODULE_FMODEX_PROTECT
#define NLIB_MODULE_FMODEX_PROTECT

/**
 *	Module FModex
 *
 *	@author SOARES Lucas
 */

// ------------------------------
// namespace NLib::Module::FModex
// ------------------------------

#ifdef NLIB_MODULE_FMODEX
// FModex header
#include <FModex/fmod.h>

// Constantes
// Nombre maximum de canaux
#define NLIB_MODULE_FMOD_MAX_CANAUX					64

// Constante repetition infinie
#define NLIB_MODULE_FMODEX_REPETITION_INFINIE		( -1 )

// Fichier DLS pour unix
#define NLIB_MODULE_FMOD_DLS_FILE_UNIX				"gm.dls"

// struct NLib::Module::FModex::NSon
#include "NLib_Module_FModex_NSon.h"

// struct NLib::Module::FModex::NMusique
#include "NLib_Module_FModex_NMusique.h"

/* Initialiser */
NBOOL NLib_Module_FModex_Initialiser( void );

/* Detruire */
void NLib_Module_FModex_Detruire( void );

/* Obtenir le contexte */
FMOD_SYSTEM *NLib_Module_FModex_ObtenirContexte( void );

/* Stopper les sons termines */
void NLib_Module_FModex_Update( void );

#ifdef NLIB_MODULE_FMODEX_INTERNE
static FMOD_SYSTEM *m_contexte = NULL;
#endif // NLIB_MODULE_FMODEX_INTERNE

#endif // NLIB_MODULE_FMODEX

#endif // !NLIB_MODULE_FMODEX_PROTECT

