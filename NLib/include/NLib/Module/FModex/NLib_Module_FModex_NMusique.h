#ifndef NLIB_MODULE_FMODEX_NMUSIQUE_PROTECT
#define NLIB_MODULE_FMODEX_NMUSIQUE_PROTECT

/*
	Une musique geree via FModex

	@author SOARES Lucas
*/

// -------------------------------------
// struct NLib::Module::FModex::NMusique
// -------------------------------------

#ifdef NLIB_MODULE_FMODEX
// struct NLib::Module::FModex::NMusique
typedef struct
{
	// Musique
	FMOD_SOUND *m_musique;

	// Duree musique (ms)
	NU32 m_duree;

	// Volume (entre 1 et 100)
	NU32 m_volume;

	// Est en cours de lecture?
	NBOOL m_estLecture;

	// Canal de lecture
	FMOD_CHANNEL *m_canal;

	// Position a la pause
	NU32 m_positionPause;

	// Contexte FModex
	const FMOD_SYSTEM *m_contexte;
} NMusique;
#endif // NLIB_MODULE_FMODEX

/**
 * Construire musique
 *
 * @param lien
 * 		Le lien vers la musique
 * @param volume
 * 		Le volume initial (0-100)
 * @param estRepete
 * 		La musique se repete?
 *
 * @return
 */
__ALLOC NMusique *NLib_Module_FModex_NMusique_Construire( const char *lien,
	NU32 volume,
	NBOOL estRepete );

/**
 * Detruire musique
 *
 * @param this
 * 		Cette instance
 */
void NLib_Module_FModex_NMusique_Detruire( NMusique** );

/**
 * Reprendre la lecture
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NMusique_Lire( NMusique* );

/**
 * Mettre en pause
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NMusique_Pause( NMusique* );

/**
 * Stopper la lecture
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NMusique_Arreter( NMusique* );

/**
 * Lecture en cours?
 *
 * @param this
 * 		Cette instance
 *
 * @return si la musique est en cours de lecture
 */
NBOOL NLib_Module_FModex_NMusique_EstLecture( const NMusique* );

/**
 * Pause en cours?
 *
 * @param this
 * 		Cette instance
 *
 * @return si la musique est en cours de pause
 */
NBOOL NLib_Module_FModex_NMusique_EstPause( const NMusique* );

/**
 * Definir le volume
 *
 * @param this
 * 		Cette instance
 * @param volume
 * 		Le volume a definir (0-100)
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NMusique_DefinirVolume( NMusique*,
	NU32 volume );

/**
 * Definir position (ms)
 *
 * @param this
 * 		Cette instance
 * @param position
 * 		La position en ms
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Module_FModex_NMusique_DefinirPosition( NMusique*,
	NU32 position );

/**
 * Obtenir position (ms)
 *
 * @param this
 * 		Cette instance
 *
 * @return la position (ms)
 */
NU32 NLib_Module_FModex_NMusique_ObtenirPosition( NMusique* );

/**
 * Obtenir volume (0 - 100)
 *
 * @param this
 * 		Cette instance
 *
 * @return le volume
 */
NU32 NLib_Module_FModex_NMusique_ObtenirVolume( const NMusique* );

/**
 * Obtenir duree musique
 *
 * @param this
 * 		Cette instance
 *
 * @return la duree de la musique
 */
NU32 NLib_Module_FModex_NMusique_ObtenirDuree( const NMusique* );

#endif // !NLIB_MODULE_FMODEX_NMUSIQUE_PROTECT

