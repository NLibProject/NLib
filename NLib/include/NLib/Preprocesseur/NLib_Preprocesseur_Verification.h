#ifndef NLIB_PREPROCESSEUR_VERIFICATION_PROTECT
#define NLIB_PREPROCESSEUR_VERIFICATION_PROTECT

/*
	Verifications pre-processeur

	@author SOARES Lucas
*/

// Verifier modules
// SDL
#if defined( NLIB_MODULE_SDL_TTF ) || defined( NLIB_MODULE_SDL_IMAGE )
#ifndef NLIB_MODULE_SDL
#error Les modules TTF et IMG necessitent la SDL.
#endif // !NLIB_MODULE_SDL
#endif // NLIB_MODULE_TTF || NLIB_MODULE_IMAGE

// Webcam
#ifdef NLIB_MODULE_WEBCAM
#ifdef IS_WINDOWS
#error Ne peut pas utiliser le module Webcam sous windows.
#endif // IS_WINDOWS
#endif // NLIB_MODULE_WEBCAM

#endif // !NLIB_PREPROCESSEUR_VERIFICATION_PROTECT

