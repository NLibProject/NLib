#ifndef NLIB_PREPROCESSEUR_DEFINITIONS_PROTECT
#define NLIB_PREPROCESSEUR_DEFINITIONS_PROTECT

/**
 *	Definitions de preprocesseur
 *
 *	@author SOARES Lucas
 */

// Will alloc memory
#define __ALLOC

// Output
#define __OUTPUT

// Control will be gained by function once passed (once passed, the variable must be considered
// as detached from the calling function, no free from here)
#define __WILLBEOWNED

// Fonction utilisee en tant que callback
#define __CALLBACK

// Fonction threadee
#define __THREAD

// Doit etre libere
#define __MUSTBEFREED

// Le mutex doit etre lock
#define __MUSTBEPROTECTED

// La fonction va lock le mutex
#define __WILLLOCK

// La fonction va unlock
#define __WILLUNLOCK

// L'element est prive
#define __PRIVATE static

// L'element est accessible aux structures derivees
#define __PROTECTED

// Ne doit pas etre NULL
#define __MUSTNOTBENULL

// Peut etre nul
#define __CANBENULL

// Windows?
#if defined( WIN32 ) || defined( _WIN64 )
#define IS_WINDOWS
#endif // WIN32 || _WIN64

// Debogage
#define NLIB_ERREUR_NOTIFICATION_DEBOGUER_LOG

// Pour linux
#ifdef IS_WINDOWS
#define ___cdecl __cdecl
#else // IS_WINDOWS
#define ___cdecl
#endif // !IS_WINDOWS

// Activer module camera
#ifdef NLIB_MODULE_CAMERA
#define NLIB_MODULE_WEBCAM
#endif // NLIB_MODULE_CAMERA

/**
 *	Modules (A definir dans la ligne de preprocesseur du projet)
 *
 *	NLIB_MODULE_RESEAU:
 *		Pour le module reseau, transfert securise via buffer avec entete+checksum
 *			=> NMETHODE_TRANSFERT_SECURISE_PACKETIO_NPROJECT
 *		Active le module reseau incluant
 *			- NCachePacket: Gestionnaire de packets sous forme de file
 *			- NClient: Gestionnaire client avec emission/reception packet
 *				=> Callbacks pour reception packet/deconnexion du serveur
 *				=> NCachePacket
 *			- NServeur: Gestionnaire serveur avec acceptation des clients,
 *					gestion des emissions/receptions de chaque client
 *				=> Callbacks pour reception packet/connexion d'un client/
 *					deconnexion d'un client
 *				=> NClientServeur: Un client du serveur dans le cache.
 *				=> NCachePacket pour chaque NClientServeur
 *
 *		NLIB_MODULE_RESEAU_BLUETOOTH: Active la partie bluetooth du reseau
 *
 *	NLIB_MODULE_SDL
 *		Active le module SDL 2 incluant
 *			- NFenetre: La fenetre a manipuler pour creer l'application
 *				graphique
 *			- NSurface: Une surface standard
 *			- NTileset: Une grille de surfaces
 *			- NAnimation: Une grille animee de surfaces
 *
 *	NLIB_MODULE_SDL_IMAGE
 *		Active le module SDL Image (necessitant le module SDL) incluant
 *			- IMG_Load( )
 *
 *	NLIB_MODULE_SDL_TTF
 *		Active le module SDL TTF (necessitant le module SDL) incluant
 *			- NPolice: Donnees d'une police
 *
 *	NLIB_MODULE_FMODEX
 *		Active le module audio FModex
 *
 *	NLIB_MODULE_REPERTOIRE
 *		Active le module repertoire
 *
 *	NLIB_MODULE_OPENSSL
 *		Active le module openssl (connexion securisee)
 *
 *	NLIB_MODULE_WEBCAM | NLIB_MODULE_CAMERA
 *		Active le module webcam (specifique unix, link avec -l4v2
 */

#endif // !NLIB_PREPROCESSEUR_DEFINITIONS_PROTECT

