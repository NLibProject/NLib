#ifndef NLIB_PREPROCESSEUR_ASSERTION_PROTECT
#define NLIB_PREPROCESSEUR_ASSERTION_PROTECT

/*
	Assertions

	@author SOARES Lucas
*/

// assertion statique
#if defined( WIN32 ) || defined( _WIN64 )
#define nassert( EXP, MSG ) static_assert( EXP, MSG )
#else // WIN32
#define nassert( EXP, MSG ) _Static_assert( EXP, MSG )
#endif // !WIN32

#endif // !NLIB_PREPROCESSEUR_ASSERTION_PROTECT

