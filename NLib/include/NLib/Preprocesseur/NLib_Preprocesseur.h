#ifndef NLIB_PREPROCESSOR_PROTECT
#define NLIB_PREPROCESSOR_PROTECT

/*
	Preprocesseur

	@author SOARES Lucas
*/

// Assertion
#include "NLib_Preprocesseur_Assertion.h"

// Definitions
#include "NLib_Preprocesseur_Definitions.h"

// Maccros
#include "NLib_Preprocesseur_Maccros.h"

// Link
#include "NLib_Preprocesseur_Link.h"

// Verifications
#include "NLib_Preprocesseur_Verification.h"

#endif // !NLIB_PREPROCESSOR_PROTECT

