#ifndef NLIB_PREPROCESSEUR_MACCROS_PROTECT
#define NLIB_PREPROCESSEUR_MACCROS_PROTECT

/*
	Definition de maccros

	@author SOARES Lucas
*/

#define NDISSOCIER_ADRESSE( m ) \
	(m) = NULL

#define NFREE( m ) \
	{ \
		if( (m) != NULL ) \
		{ \
			free( (m) ); \
			NDISSOCIER_ADRESSE( (m) ); \
		} \
	}

#ifdef IS_WINDOWS
#define NREFERENCER( a ) \
	a
#else // IS_WINDOWS
#define NREFERENCER( a )
#endif // !IS_WINDOWS

#define NDEFINIR_POSITION( p, xD, yD ) \
	{ \
		(p).x = xD; \
		(p).y = yD; \
	}

#define NDEFINIR_TAILLE( p, wD, hD ) \
	{ \
		(p).w = wD; \
		(p).h = hD; \
	}

#define NDEFINIR_COULEUR( c, rD, gD, bD ) \
	{ \
		(c).r = rD; \
		(c).g = gD; \
		(c).b = bD; \
	}

#define NDEFINIR_COULEUR_A( c, rD, gD, bD, aD ) \
	{ \
		(c).r = rD; \
		(c).g = gD; \
		(c).b = bD; \
		(c).a = aD; \
	}

#define NLIB_NETTOYER_SURFACE( s ) \
	{ \
		if( (s) != NULL ) \
		{ \
			SDL_FreeSurface( s ); \
			NDISSOCIER_ADRESSE( s ); \
		} \
	}

#define NLIB_NETTOYER_TEXTURE( t ) \
	{ \
		if( (t) != NULL ) \
		{ \
			SDL_DestroyTexture( t ); \
			NDISSOCIER_ADRESSE( t ); \
		} \
	}

#endif // !NLIB_PREPROCESSEUR_MACCROS_PROTECT

