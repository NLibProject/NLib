#ifndef NLIB_MUTEX_PROTECT
#define NLIB_MUTEX_PROTECT

// ---------------------
// namespace NLib::Mutex
// ---------------------

#ifndef IS_WINDOWS
#include <pthread.h>
#endif // !IS_WINDOWS

// struct NLib::Mutex::NMutex
#include "NLib_Mutex_NMutex.h"

#endif // !NLIB_MUTEX_PROTECT

