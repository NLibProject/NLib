#ifndef NLIB_MUTEX_NMUTEX_PROTECT
#define NLIB_MUTEX_NMUTEX_PROTECT

// --------------------------
// struct NLib::Mutex::NMutex
// --------------------------

#ifdef IS_WINDOWS
#include <WinSock2.h>
#endif // IS_WINDOWS

typedef struct
{
#ifdef IS_WINDOWS
	HANDLE m_mutex;
#else // IS_WINDOWS
    pthread_mutex_t m_mutex;
#endif // !IS_WINDOWS
} NMutex;

/* Construire */
__ALLOC NMutex *NLib_Mutex_NMutex_Construire( void );

/* Detruire */
void NLib_Mutex_NMutex_Detruire( NMutex** );

#ifdef NLIB_DEBUG_MUTEX
/* Lock */
NBOOL NLib_Mutex_NMutex_LockDebug( NMutex*,
	const char *fichier,
	NU32 ligne );
#define NLib_Mutex_NMutex_Lock( m ) \
	NLib_Mutex_NMutex_LockDebug( m, \
		__FILE__, \
		__LINE__ )

/* Unlock */
void NLib_Mutex_NMutex_UnlockDebug( NMutex*,
	const char *fichier,
	NU32 ligne );
#define NLib_Mutex_NMutex_Unlock( m ) \
	NLib_Mutex_NMutex_UnlockDebug( m, \
		__FILE__, \
		__LINE__ )
#else // NLIB_DEBUG_MUTEX
/* Lock */
NBOOL NLib_Mutex_NMutex_Lock( NMutex* );

/* Unlock */
void NLib_Mutex_NMutex_Unlock( NMutex* );
#endif // !NLIB_DEBUG_MUTEX

#endif // !NLIB_MUTEX_NMUTEX_PROTECT

