#ifndef NLIB_PROTECT
#define NLIB_PROTECT

/*
	Librairie NLib fournissant un support complet sur tous les
	besoins elementaires avec une gestion:
		- Des caracteres
		- Des chaines
		- Des erreurs
		- De la memoire
		- Des fichiers

	Fournit egalement une partie "module", activable ou desacti
	vable selon le projet via des declarations comportant:
		- Module SDL [GUI]
			=> Module SDL_Image
			=> Module SDL_TTF
		- Module FModex [GUI]
		- Module Repertoire
			=> Liste des fichiers dans les repertoires
		- Module reseau
			=> Packets
			=> Serveur (avec gestion des clients pour envoi/re
			ception packet)
			=> Client (avec gestion envoi/reception packet)

	Linkage automatique des librairies sous windows

	@author SOARES Lucas
*/

// CRT NO WARNINGS
#define _CRT_SECURE_NO_WARNINGS // Windows

// _CRT_RAND_S
#define _CRT_RAND_S	// Activer rand_s

// Standard headers
#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <time.h>

#ifndef IS_WINDOWS
#include <errno.h>
#include <linux/limits.h>
#include <signal.h>
#endif // !IS_WINDOWS

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#endif // _DEBUG

#include <stdlib.h>


// Definition preprocesseur
#include "Preprocesseur/NLib_Preprocesseur.h"

// Warnings en moins
#ifdef IS_WINDOWS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma warning( disable:4706 ) // assignation au sein d'une expression conditionnelle
#pragma warning( disable:4054 ) // 'cast de type' : du pointeur fonction 'void (___cdecl *const )(const NErreur *)' vers le pointeur donnee 'void *'
#endif

// namespace NLib::Type
#include "Type/NLib_Type.h"

// namespace NLib::Erreur
#include "Erreur/NLib_Erreur.h"

// namespace NLib::Caractere
#include "Caractere/NLib_Caractere.h"

// namespace NLib::Mutex
#include "Mutex/NLib_Mutex.h"

// namespace NLib::Memoire
#include "Memoire/NLib_Memoire.h"

// namespace NLib::Temps
#include "Temps/NLib_Temps.h"

// namespace NLib::Fichier
#include "Fichier/NLib_Fichier.h"

// namespace NLib::Chaine
#include "Chaine/NLib_Chaine.h"

// namespace NLib::Math
#include "Math/NLib_Math.h"

// namespace NLib::Thread
#include "Thread/NLib_Thread.h"

// namespace NLib::Module
#include "Module/NLib_Module.h"

// --------------
// namespace NLib
// --------------

/* Initialiser NLib */
NBOOL NLib_Initialiser( void ( *callbackNotificationErreur )( const NErreur* ) );

/* Detruire NLib */
void NLib_Detruire( void );

#ifdef NLIB_INTERNE
static NBOOL m_estInitialise = (NBOOL)0;
#endif // NLIB_INTERNE

#endif // !NLIB_PROTECT

