#ifndef NLIB_FICHIER_NFICHIERBINAIRE_PROTECT
#define NLIB_FICHIER_NFICHIERBINAIRE_PROTECT

// -------------------------------------
// struct NLib::Fichier::NFichierBinaire
// -------------------------------------

typedef struct
{
	// Fichier
	FILE *m_fichier;

	// Est lecture?
	NBOOL m_estLecture;

	// Taille
	NU64 m_taille;
} NFichierBinaire;

/* Construire */
__ALLOC NFichierBinaire *NLib_Fichier_NFichierBinaire_ConstruireEcriture( const char *lien,
	NBOOL estEcraserContenu );
__ALLOC NFichierBinaire *NLib_Fichier_NFichierBinaire_ConstruireLecture( const char *lien );

/* Detruire */
void NLib_Fichier_NFichierBinaire_Detruire( NFichierBinaire** );

/* Est EOF? */
NBOOL NLib_Fichier_NFichierBinaire_EstEOF( const NFichierBinaire* );

/* Obtenir taille */
NU64 NLib_Fichier_NFichierBinaire_ObtenirTaille( const NFichierBinaire* );

/* Est lecture? */
NBOOL NLib_Fichier_NFichierBinaire_EstLecture( const NFichierBinaire* );

/* Est ecriture? */
NBOOL NLib_Fichier_NFichierBinaire_EstEcriture( const NFichierBinaire* );

/* Lire */
__ALLOC char *NLib_Fichier_NFichierBinaire_Lire( NFichierBinaire*,
	NU32 taille );
NBOOL NLib_Fichier_NFichierBinaire_Lire2( NFichierBinaire*,
	__OUTPUT char *sortie,
	NU32 taille );

/* Ecrire */
NBOOL NLib_Fichier_NFichierBinaire_Ecrire( NFichierBinaire*,
	const char *buffer,
	NU32 tailleBuffer );

/**
 * Retourner au debut du fichier
 *
 * @param this
 * 		Cette instance
 */
void NLib_Fichier_NFichierBinaire_DefinirPositionDebut( NFichierBinaire* );

/**
 * Obtenir fichier FILE*
 *
 * @param this
 * 		Cette instance
 *
 * @return le fichier
 */
FILE *NLib_Fichier_NFichierBinaire_ObtenirFichier( NFichierBinaire* );

/**
 * Lire tout le contenu
 *
 * @param this
 * 		Cette instance
 *
 * @return le contenu du fichier
 */
__ALLOC char *NLib_Fichier_NFichierBinaire_LireTout( NFichierBinaire* );

#endif // !NLIB_FICHIER_NFICHIERBINAIRE_PROTECT

