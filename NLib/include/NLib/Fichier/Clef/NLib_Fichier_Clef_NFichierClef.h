#ifndef NLIB_FICHIER_CLEF_NFICHIERCLEF_PROTECT
#define NLIB_FICHIER_CLEF_NFICHIERCLEF_PROTECT

/*
	Expression d'un fichier compose de clefs/valeurs

	L'ordre des clefs dans le tableau transmis corre
	spond a l'ordre de stockage dans cette structure
	Il s'agira donc de donner les memes identifiants
	de clefs pour recuperer les valeurs.

	@author SOARES Lucas
*/

// ----------------------------------------
// struct NLib::Fichier::Clef::NFichierClef
// ----------------------------------------

typedef struct
{
	// Nombre clefs
	NU32 m_nombreClefs;

	// Valeurs
	char **m_valeurs;
} NFichierClef;

/**
 * Construire un ensemble de valeurs associes aux clefs, valeurs recuperees dans le fichier
 *
 * @param lien
 * 		Le lien vers le fichier
 * @param clefs
 * 		La liste des clefs
 * @param nombreClefs
 * 		Le nombre de clefs
 * @param estCorrigerValeur
 * 		Est ce qu'on supprime les espaces au debut de la valeur, et les retour chariot/nouvelle ligne a la fin?
 *
 * @return l'instance du fichier clefs
 */
__ALLOC NFichierClef *NLib_Fichier_Clef_NFichierClef_Construire( const char *lien,
	const char **clefs,
	NU32 nombreClefs,
	NBOOL estCorrigerValeur );

/**
 * Detruire le fichier
 *
 * @param this
 * 		Cette instance
 */
void NLib_Fichier_Clef_NFichierClef_Detruire( NFichierClef** );

/**
 * Obtenir valeur texte
 *
 * @param this
 * 		Cette instance
 * @param clef
 * 		La clef
 *
 * @return la valeur texte
 */
const char *NLib_Fichier_Clef_NFichierClef_ObtenirValeur( const NFichierClef*,
	NU32 clef );

/**
 * Obtenir valeur entiere non signee
 *
 * @param this
 * 		Cette instance
 * @param clef
 * 		La clef
 *
 * @return la valeur entiere non signee ou NERREUR si une erreur a eu lieu
 */
NU32 NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( const NFichierClef*,
	NU32 clef );

/**
 * Obtenir valeur entiere signee
 *
 * @param this
 * 		Cette instance
 * @param clef
 * 		La clef
 *
 * @return la valeur entiere signee
 */
NS32 NLib_Fichier_Clef_NFichierClef_ObtenirValeur3( const NFichierClef*,
	NU32 clef );

/**
 * Obtenir copie valeur texte
 *
 * @param this
 * 		Cette instance
 * @param clef
 * 		La clef
 *
 * @return la copie de la valeur texte
 */
__ALLOC char *NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( const NFichierClef*,
	NU32 clef );

#endif // !NLIB_FICHIER_CLEF_NFICHIERCLEF_PROTECT

