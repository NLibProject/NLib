#ifndef NLIB_FICHIER_OPERATION_PROTECT
#define NLIB_FICHIER_OPERATION_PROTECT

/**
 *	Operation sur les fichiers
 *
 *	@author SOARES Lucas
 */

// ----------------------------------
// namespace NLib::Fichier::Operation
// ----------------------------------

/**
 * Obtenir la taille d'un fichier
 *
 * @param f
 * 		Le fichier
 *
 * @return la taille du fichier
 */
NU64 NLib_Fichier_Operation_ObtenirTaille( FILE* );

/**
 * Obtenir la taille du fichier
 *
 * @param lien
 * 		Le lien vers le fichier
 *
 * @return la taille du fichier
 */
NU64 NLib_Fichier_Operation_ObtenirTaille2( const char* );

/**
 * Calculer le checksum d'un fichier
 *
 * @param fichier
 * 		Le fichier dont on veut le checksum
 *
 * @return le checksum du fichier
 */
NS32 NLib_Fichier_Operation_CalculerChecksum( FILE *fichier );

/**
 * Calculer le checksum d'un fichier
 *
 * @param lien
 * 		Le lien du fichier
 *
 * @return le checksum du fichier
 */
NS32 NLib_Fichier_Operation_CalculerChecksum2( const char *lien );

/**
 * Le fichier est EOF?
 *
 * @param f
 * 		Le fichier
 *
 * @return Est EOF?
 */
NBOOL NLib_Fichier_Operation_EstEOF( FILE* );

/**
 * Le fichier est EOF?
 *
 * @param f
 * 		Le fichier
 * @param taille
 * 		La taille du fichier
 *
 * @return Est EOF?
 */
NBOOL NLib_Fichier_Operation_EstEOF2( FILE*,
	NU64 taille );

/**
 * Le fichier existe?
 *
 * @param lien
 * 		Le lien vers le fichier
 *
 * @return si le fichier existe
 */
NBOOL NLib_Fichier_Operation_EstExiste( const char *lien );

/**
 * Placer le curseur a la prochaine occurence du caractere
 *
 * @param f
 * 		Le fichier
 * @param c
 * 		Le caractere auquel se placer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Fichier_Operation_PlacerCaractere( FILE*,
	char );

/**
 * Se placer a la chaine
 *
 * @param f
 * 		Le fichier
 * @param s
 * 		La chaine a laquelle se placer
 * @param estDoitRetournerAuDebut
 * 		On retourne au debut avant de se placer a cette chaine?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Fichier_Operation_PlacerChaine( FILE*,
	const char*,
	NBOOL estDoitRetournerDebut );

/**
 * Lire chaine entre deux delimiteurs
 *
 * @param f
 * 		Le fichier
 * @param c1
 * 		Le caractere a gauche
 * @param c2
 * 		Le caractere a droite
 * @param estForcerRelectureCaractereFin
 * 		Est ce que si le premier caractere lu a droite est un %c2% au tout debut, on doit lire le caractere suivant? (Defaut: NTRUE)
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Fichier_Operation_LireEntre( FILE *f,
	char c1,
	char c2,
	NBOOL estForcerRelectureCaractereFin );

/**
 * Lire entre deux delimiteurs identiques
 *
 * @param f
 * 		Le fichier
 * @param c
 * 		Le delimiteur gauche/droit
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Fichier_Operation_LireEntre2( FILE*,
	char c );

/**
 * Lire l'integralite du fichier
 *
 * @param f
 * 		Le fichier
 *
 * @return l'integralite du fichier dans une chaine
 */
__ALLOC char *NLib_Fichier_Operation_LireContenu( FILE* );

/**
 * Lire nombre non signe
 *
 * @param f
 * 		Le fichier
 * @param estConserverPosition
 * 		Restaurer la position apres lecture?
 *
 * @return le nombre lu
 */
NU32 NLib_Fichier_Operation_LireNombreNonSigne( FILE*,
	NBOOL estConserverPosition );

/**
 * Lire nombre signe
 *
 * @param f
 * 		Le fichier
 * @param estConserverPosition
 * 		Restaurer la position apres lecture?
 *
 * @return le nombre lu
 */
NU32 NLib_Fichier_Operation_LireNombreSigne( FILE*,
	NBOOL estConserverPosition );

/**
 * Obtenir prochain caractere
 *
 * @param f
 * 		Le fichier
 * @param estConserverSeparateur
 * 		On considere les separateurs comme des caracteres
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture?
 *
 * @return le caractere lu
 */
char NLib_Fichier_Operation_ObtenirProchainCaractere( FILE *f,
	NBOOL estConserverSeparateur,
	NBOOL estConserverPosition );

/**
 * Ecrire data dans fichier en ecrasant le contenu
 *
 * @param lien
 * 		Le lien du fichier dans lequel ecrire
 * @param data
 * 		Les donnees a inscrire
 * @param taille
 * 		La taille des donnees a inscrire
 * @param estEcraserSiExiste
 * 		On ecrase le fichier si il existe?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Fichier_Operation_Ecrire( const char *lien,
	const char *data,
	NU64 taille,
	NBOOL ecraserSiExiste );

/**
 * Determiner l'extension d'un fichier
 *
 * @param nom
 * 		Le nom du fichier
 *
 * @return l'extension du fichier
 */
__ALLOC char *NLib_Fichier_Operation_ObtenirExtension( const char* );

/**
 * Supprimer un fichier
 *
 * @param fichier
 *		Le lien du fichier a supprimer
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_Operation_SupprimerFichier( const char *fichier );

/**
 * Copier fichier (ecrase la destination)
 *
 * @param src
 *		Le fichier source
 * @param dst
 *		Le fichier destination
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Fichier_Operation_CopierFichier( const char *src,
	const char *dst );

/**
 * Calculer hash md5
 *
 * @param fichier
 * 		Le fichier
 * @param hash
 * 		Le hash en sortie (16)
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Fichier_Operation_CalculerHashMD5( FILE *fichier,
	__OUTPUT unsigned char hash[ 16 ] );

/**
 * Calculer hash md5
 *
 * @param lienFichier
 * 		Le chemin vers le fichier
 * @param hash
 * 		Le hash en sortie (16)
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NLib_Fichier_Operation_CalculerHashMD5_2( const char *lienFichier,
	__OUTPUT unsigned char hash[ 16 ] );

#endif // !NLIB_FICHIER_OPERATION_PROTECT

