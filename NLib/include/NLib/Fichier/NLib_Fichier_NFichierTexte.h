#ifndef NLIB_FICHIER_NFICHIERTEXTE_PROTECT
#define NLIB_FICHIER_NFICHIERTEXTE_PROTECT

/*
	Fichier texte, contenant des mots lisibles
	et interpretables par l'humain

	@author SOARES Lucas
*/

// -----------------------------------
// struct NLib::Fichier::NFichierTexte
// -----------------------------------

typedef struct
{
	// Fichier
	FILE *m_fichier;

	// Est lecture?
	NBOOL m_estLecture;

	// Taille
	NU32 m_taille;
} NFichierTexte;

/**
 * Construire fichier texte en ecriture
 *
 * @param lien
 * 		Le lien du fichier
 * @param estEcraserContenu
 * 		Vider le fichier a l'ouverture?
 *
 * @return l'instance du fichier
 */
__ALLOC NFichierTexte *NLib_Fichier_NFichierTexte_ConstruireEcriture( const char *lien,
	NBOOL estEcraserContenu );

/**
 * Construire fichier texte en lecture
 *
 * @param lien
 * 		Le lien du fichier
 *
 * @return l'instance du fichier
 */
__ALLOC NFichierTexte *NLib_Fichier_NFichierTexte_ConstruireLecture( const char *lien );

/**
 * Detruire le fichier
 *
 * @param this
 * 		Cette instance
 */
void NLib_Fichier_NFichierTexte_Detruire( NFichierTexte** );

/**
 * On est a la fin du fichier?
 *
 * @param this
 * 		Cette instance
 *
 * @return si EOF
 */
NBOOL NLib_Fichier_NFichierTexte_EstEOF( const NFichierTexte* );

/**
 * Obtenir la taille du fichier
 *
 * @param this
 * 		Cette instance
 *
 * @return la taille du fichier
 */
NU32 NLib_Fichier_NFichierTexte_ObtenirTaille( const NFichierTexte* );

/**
 * Le fichier est ouvert en lecture?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le fichier est ouvert en lecture
 */
NBOOL NLib_Fichier_NFichierTexte_EstLecture( const NFichierTexte* );

/**
 * Le fichier est ouvert en ecriture?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le fichier est ouvert en ecriture
 */
NBOOL NLib_Fichier_NFichierTexte_EstEcriture( const NFichierTexte* );

/**
 * Lire caractere
 *
 * @param this
 * 		Cette instance
 * @param estConservePosition
 * 		Conserver la position?
 *
 * @return le caractere lu
 */
char NLib_Fichier_NFichierTexte_LireCaractere( NFichierTexte*,
	NBOOL estConservePosition );

/**
 * Lire un nombre dans le fichier
 *
 * @param this
 * 		Cette instance
 * @param estSigne
 * 		Le nombre a lire est signe?
 * @param estConservePosition
 * 		Conserver la position?
 *
 * @return le nombre lu ou NFICHIER_CODE_ERREUR en cas d'erreur
 */
NU32 NLib_Fichier_NFichierTexte_LireNombre( NFichierTexte*,
	NBOOL estSigne,
	NBOOL estConservePosition );

/**
 * Lire un mot
 *
 * @param this
 * 		Cette instance
 * @param estConservePosition
 * 		Conserver la position?
 *
 * @return le mot lu
 */
__ALLOC char *NLib_Fichier_NFichierTexte_LireMot( NFichierTexte*,
	NBOOL estConservePosition );

/**
 * Lire une ligne
 *
 * @param this
 * 		Cette instance
 * @param estConservePosition
 * 		Conserver la position?
 *
 * @return la ligne lue
 */
__ALLOC char *NLib_Fichier_NFichierTexte_LireLigne( NFichierTexte*,
	NBOOL estConservePosition );

/**
 * Chercher et lire prochaine ligne sans commentaire
 *
 * @param this
 * 		Cette instance
 * @param caractereCommentaire
 * 		Le caractere identifiant un commentaire
 * @param estConserverPosition
 * 		Conserver la position?
 *
 * @return la chaine lue ou NULL
 */
__ALLOC char *NLib_Fichier_NFichierTexte_LireProchaineLigneSansCommentaire( NFichierTexte*,
	char caractereCommentaire,
	NBOOL estConserverPosition );

/**
 * Lire un mot encadre de caracteres
 *
 * @param this
 * 		Cette instance
 * @param caractereGauche
 * 		Le delimiteur gauche (\0 pour ignorer)
 * @param caractereDroit
 * 		Le delimiteur droit
 * @param estConserverPosition
 * 		On conserve la position apres la lecture?
 * @param estForcerRelectureCaractereFin
 * 		On force la lecture du premier caractere lu jusqu'a trouver un caractere different de %caractereDroit%? (Defaut: NTRUE)
 *
 * @return ce qui a ete lu ou NULL
 */
__ALLOC char *NLib_Fichier_NFichierTexte_Lire( NFichierTexte*,
	char caractereGauche,
	char caractereDroit,
	NBOOL estConserverPosition,
	NBOOL estForcerRelectureCaractereFin );

/**
 * Lire une chaine entre deux delimiteurs
 *
 * @param this
 * 		Cette instance
 * @param caractereEncadrement
 * 		Le caractere encadrant
 * @param estConserverPosition
 * 		Conserver la position?
 *
 * @return la chaine lue
 */
__ALLOC char *NLib_Fichier_NFichierTexte_Lire2( NFichierTexte*,
	char caractereEncadrement,
	NBOOL estConserverPosition );

/**
 * Ecrire entier non signe dans fichier
 *
 * @param this
 * 		Cette instance
 * @param nombre
 * 		Le nombre a inscrire
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_Ecrire( NFichierTexte *this,
	NU32 nombre );

/**
 * Ecrire entier signe
 *
 * @param this
 * 		Cette instance
 * @param nombre
 * 		Le nombre a inscrire
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_Ecrire2( NFichierTexte*,
	NS32 nombre );

/**
 * Ecrire chaine
 *
 * @param this
 * 		Cette instance
 * @param chaine
 * 		La chaine a inscrire
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_Ecrire3( NFichierTexte*,
	const char *chaine );

/**
 * Ecrire nombre reel
 *
 * @param this
 * 		Cette instance
 * @param nombre
 * 		Le nombre a inscrire
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_Ecrire4( NFichierTexte*,
	float nombre );

/**
 * Ecrire nombre reel
 *
 * @param this
 * 		Cette instance
 * @param nombre
 * 		Le nombre a inscrire
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_Ecrire5( NFichierTexte*,
	double nombre );

/**
 * Ecrire caractere
 *
 * @param this
 * 		Cette instance
 * @param caractere
 * 		Le caractere a inscrire
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_Ecrire6( NFichierTexte*,
	char caractere );

/**
 * Positionner curseur apres le caractere
 *
 * @param this
 * 		Cette instance
 * @param caractere
 * 		Le caractere apres lequel se placer
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_PositionnerProchainCaractere( NFichierTexte*,
	char caractere );

/**
 * Positionner le curseur apres la chaine
 *
 * @param this
 * 		Cette instance
 * @param chaine
 * 		La chaine
 * @param estDoitRetournerDebut
 * 		La recherche part du debut?
 *
 * @return si l'operation a reussi
 */
NBOOL NLib_Fichier_NFichierTexte_PositionnerProchaineChaine( NFichierTexte*,
	const char *chaine,
	NBOOL estDoitRetournerDebut );

/**
 * Obtenir fichier
 *
 * @param this
 * 		Cette instance
 *
 * @return le fichier
 */
FILE *NLib_Fichier_NFichierTexte_ObtenirFichier( NFichierTexte* );

#endif // !NLIB_FICHIER_NFICHIERTEXTE_PROTECT

