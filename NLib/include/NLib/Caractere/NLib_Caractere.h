#ifndef NLIB_CARACTERE_PROTECT
#define NLIB_CARACTERE_PROTECT

/**
 *	Gestion des caracteres
 *
 *	@author SOARES Lucas
 */

// -------------------------
// namespace NLib::Caractere
// -------------------------

/**
 * Est un chiffre?
 *
 * @param caractere
 * 		Le caractere
 *
 * @return si c'est un chiffre
 */
NBOOL NLib_Caractere_EstUnChiffre( char caractere );

/**
 * Est un chiffre au format hexadecimal?
 *
 * @param caractere
 * 		Le caractere
 *
 * @return si c'est un chiffre au format hexadecimal
 */
NBOOL NLib_Caractere_EstUnChiffreHexadecimal( char caractere );

/**
 * Est une lettre?
 *
 * @param caractere
 * 		Le caractere
 *
 * @return si c'est une lettre
 */
NBOOL NLib_Caractere_EstUneLettre( char caractere );

/**
 * Est un separateur
 *
 * @param caractere
 * 		Le caractere
 *
 * @return si c'est un separateur
 */
NBOOL NLib_Caractere_EstUnSeparateur( char caractere );

/**
 * Convertir caractere vers entier
 *
 * @param caractere
 * 		Le caractere
 *
 * @return la valeur decimale
 */
NU32 NLib_Caractere_ConvertirDecimal( char caractere );

/**
 * Convertir un nombre en caractere entre 0 et F
 *
 * @param nombre
 * 		Le nombre a convertir
 *
 * @return le caractere hexadecimal ou -1 en cas d'erreur
 */
char NLib_Caractere_ConvertirHexadecimal( NU8 nombre );

/**
 * Est un caractere acceptable dans HTML
 *
 * @param caractere
 * 		Le caractere
 *
 * @return si le caractere est viable dans un fichier HTML
 */
NBOOL NLib_Caractere_EstUnCaractereViableHTMLHref( char caractere );

/**
 * Est un caractere acceptable dans un nom de domaine?
 *
 * @param caractere
 *		Le caractere a analyser
 *
 * @return si le caractere est accepte
 */
NBOOL NLib_Caractere_EstUnCaractereViableNomDomaine( char caractere );

/**
 * Convertir en minuscule
 *
 * @param caractere
 *		Le caractere a convertir
 *
 * @return le resultat de la conversion
 */
char NLib_Caractere_ConvertirMinuscule( char caractere );

/**
 * Convertir en majuscule
 *
 * @param caractere
 *		Le caractere a convertir
 *
 * @return le resultat de la conversion
 */
char NLib_Caractere_ConvertirMajuscule( char caractere );

/**
 * Convertir une valeur vers base64
 *
 * @param valeur
 * 		La valeur a convertir
 * @param estEncoderPourURL
 * 		On veut encoder pour une URL?
 *
 * @return le caractere en base64 associe ou 0xFF si cette valeur ne veut rien dire
 */
char NLib_Caractere_ConvertirValeurVersBase64( NU8 valeur,
	NBOOL estEncoderPourURL );

/**
 * Convertir caractere base64 vers valeur
 *
 * @param valeur
 * 		Le caractere base64
 *
 * @return le caractere normal sur 8 bits
 */
NU8 NLib_Caractere_ConvertirValeurDepuisBase64( char valeur );

#endif // !NLIB_CARACTERE_PROTECT

