#ifndef NLIB_ERREUR_NCODEERREUR_PROTECT
#define NLIB_ERREUR_NCODEERREUR_PROTECT

/*
	Code d'erreur pour NLib

	@author SOARES Lucas
*/

// ------------------------------
// enum NLib::Erreur::NCodeErreur
// ------------------------------

// Code NErreur
typedef enum
{
	// Pas d'erreur
	NERREUR_AUCUNE,

	// Debogage
	NERREUR_DEBOGAGE,

	// Echec allocation
	NERREUR_ALLOCATION_FAILED,

	// Echec du constructeur
	NERREUR_CONSTRUCTOR_FAILED,

	// Erreur utilisateur (creee manuellement par le programme externe a NLib) --> Utilise code utilisateur
	NERREUR_USER,

	// Erreur verbose
	NERREUR_VERBOSE,

	// NULL pointeur
	NERREUR_NULL_POINTER,

	// Erreur parametre
	NERREUR_PARAMETER_ERROR,

	// Out of memory
	NERREUR_OUT_MEMORY,

	// Echec initialisation
	NERREUR_INIT_FAILED,

	// Timeout
	NERREUR_TIMEOUT,

	// File not found
	NERREUR_FILE_NOT_FOUND,

	// File already exists
	NERREUR_FILE_ALREADY_EXISTS,

	// File cannot be opened
	NERREUR_FILE_CANNOT_BE_OPENED,

	// Ne peut pas remplacer le fichier
	NERREUR_FILE_CANNOT_BE_OVERRIDE,

	// Ne peut pas ecrire dans le fichier
	NERREUR_FILE_CANT_WRITE,

	// Ne peut pas lire dans le fichier
	NERREUR_FILE_CANT_READ,

	// Fichier vide
	NERREUR_FILE_EMPTY,

	// Clef introuvable fichier
	NERREUR_FILE_KEY_NOT_FOUND,

	// Erreur CRC
	NERREUR_CRC,

	// File (non precise)
	NERREUR_FILE,

	// Erreur flux
	NERREUR_STREAM_ERROR,

	// Deja initialise
	NERREUR_ALREADY_INITIALIZED,

	// Initialisation incomplete
	NERREUR_INIT_UNCOMPLETE,

	// SDL
	NERREUR_SDL,

	// SDL_IMG
	NERREUR_SDL_IMAGE,

	// SDL_TTF
	NERREUR_SDL_TTF,

	// WinSock
	NERREUR_WINSOCK,

	// Socket
	NERREUR_SOCKET,

	// Socket surchargee
	NERREUR_SOCKET_OVERLOADED,

	// Connexion socket echouee
	NERREUR_SOCKET_CONNECTION_FAILED,

	// Connexion socket timeout
	NERREUR_SOCKET_CONNECTION_TIMEOUT,

	// Accept socket echouee
	NERREUR_SOCKET_ACCEPT_FAILED,

	// Recv failed
	NERREUR_SOCKET_RECV,

	// Send failed
	NERREUR_SOCKET_SEND,

	// Impossible de se connecter
	NERREUR_UNABLE_TO_CONNECT,

	// PortAudio
	NERREUR_PORTAUDIO,

	// Obtention repertoire
	NERREUR_GET_CURRENT_DIRECTORY_FAILED,

	// Obtention element repertoire
	NERREUR_GET_ITEM_REPERTORY_FAILED,

	// Erreur d'etat objet
	NERREUR_OBJECT_STATE,

	// Pas de fichiers dans le repertoire
	NERREUR_NO_FILE_REPERTORY,

	// Taille incorrecte
	NERREUR_INCORRECT_SIZE,

	// Archive vide
	NERREUR_EMPTY_ARCHIVE,

	// Date incorrecte
	NERREUR_INCORRECT_DATE,

	// Packet inconnu
	NERREUR_UNKNOWN_PACKET,

	// Echec login
	NERREUR_LOGIN_FAILED,

	// Echec logout
	NERREUR_LOGOUT_FAILED,

	// Echec export
	NERREUR_EXPORT_FAILED,

	// Erreur thread
	NERREUR_THREAD,

	// Erreur mutex
	NERREUR_MUTEX,

	// Erreur lock mutex
	NERREUR_LOCK_MUTEX,

	// Erreur memoire
	NERREUR_MEMORY,

	// Serveur ferme
	NERREUR_SERVER_CLOSED,

	// Socket bind
	NERREUR_SOCKET_BIND,

	// Socket listen
	NERREUR_SOCKET_LISTEN,

	// FMod
	NERREUR_FMODEX,

	// Repertoire
	NERREUR_DIRECTORY,

	// Erreur syntaxe
	NERREUR_SYNTAX,

	// La resolution DNS a echoue
	NERREUR_DNS_RESOLVE_FAILED,

	// URL malformee
	NERREUR_MALFORMED_URL,

	// Erreur de protocole
	NERREUR_PROTOCOL,

	// Erreur regex
	NERREUR_REGEX,

	// Erreur v4l2
	NERREUR_V4L2,

	// Erreur png
	NERREUR_PNG,

	// Erreur copie
	NERREUR_COPY,

	// Non implemente
	NERREUR_UNIMPLEMENTED,

	// Echec authentification
	NERREUR_AUTH_FAILED,

	// Impossible d'obtenir un shell
	NERREUR_SSH_CANT_GET_SHELL,

	// Processus en cours
	NERREUR_PROCESS_STILL_EXIST,

	// Impossible de creer session SFTP
	NERREUR_SFTP_CANT_GET_SESSION,

	// Impossible d'ouvrir fichier SFTP
	NERREUR_SFTP_CANT_OPEN_FILE,

	// Impossible de traiter la requete
	NERREUR_CANT_PROCESS_REQUEST,

	// Erreur serial
	NERREUR_SERIAL,

	// Erreur http
	NERREUR_HTTP_SYNTAX,

	// Erreur aucune interface reseau
	NERREUR_NO_NETWORK_INTERFACE,

	// Erreur inconnue
	NERREUR_UNKNOWN,

	// Total...
	NERREURS
} NCodeErreur;

/* Obtention texte erreurs */
const char *NLib_Erreur_NCodeErreur_Traduire( NCodeErreur );

/* Savoir si un code utilise le code utilisateur */
NBOOL NLib_Erreur_NCodeErreur_EstUtilisateurCodeUtilisateur( NCodeErreur );

// Chaine erreurs
#ifdef NLIB_ERREUR_NCODEERREUR_INTERNE
static const char NCodeErreurTexte[ NERREURS ][ 64 ] =
{
	// Pas d'erreur
	"OK",

	// Erreur volontaire pour debogage
	"DBG",

	// Echec allocation
	"Echec allocation",

	// Echec du constructeur
	"Echec constructeur",

	// Erreur utilisateur (creee manuellement par le programme externe a NLib)
	"Erreur utilisateur",

	// Verbose
	"Verbose",

	// NULL pointeur
	"Pointeur NULL",

	// Erreur parametre
	"Parametre(s) incorrect(s)",

	// Out of memory
	"Out of memory",

	// Echec initialisation
	"Echec initialisation",

	// Timeout
	"Temps ecoule",

	// File not found
	"Fichier introuvable",

	// File already exists
	"Fichier existe deja",

	// File cannot be opened
	"Echec ouverture fichier",

	// Ne peut pas remplacer le fichier
	"Fichier protege",

	// Impossible d'ecrire dans le fichier
	"Echec ecriture fichier",

	// Impossible de lire dans le fichier
	"Echec lecture fichier",

	// Fichier vide
	"Fichier vide",

	// Fichier clef introuvable
	"Clef introuvable fichier",

	// Erreur CRC
	"Erreur CRC",

	// File (non precise)
	"Erreur avec un fichier",

	// Erreur avec le flux
	"Erreur flux",

	// Deja initialise
	"Deja initialise",

	// Initialisation incomplete
	"Initialisation incomplete",

	// SDL
	"SDL",

	// SDL Image
	"SDL_Image",

	// SDL TTF
	"SDL_TTF",

	// WinSock
	"Winsock",

	// Socket
	"Socket",

	// Socket surchargee
	"Socket surchargee",

	// Connexion socket echouee
	"Connexion socket echouee",

	// Connexion socket timeout
	"Connexion socket timeout",

	// accept a echoue
	"accept( ) echoue",

	// recv a echoue
	"recv( ) echoue",

	// send a echoue
	"send( ) echoue",

	// Impossible de se connecter
	"Impossible de se connecter",

	// PortAudio
	"PortAudio",

	// Erreur obtention repertoire courant
	"Echec obtention repertoire",

	// Erreur obtention element repertoire
	"Erreur obtention elt rep",

	// Erreur d'etat objet
	"Erreur etat objet",

	// Erreur pas de fichier dans le repertoire
	"Erreur aucun fichier rep",

	// Taille incorrecte
	"Taille incorrecte",

	// Archive vide
	"Archive vide",

	// Date incorrecte
	"Date incorrecte",

	// Packet inconnu
	"Packet inconnu",

	// Echec login
	"Echec login",

	// Echec logout
	"Echec logout",

	// Echec export
	"Echec export",

	// Erreur thread
	"Erreur thread",

	// Erreur mutex
	"Erreur mutex",

	// Erreur lock mutex
	"Erreur lock mutex",

	// Erreur memoire
	"Erreur memoire",

	// Serveur ferme
	"Serveur ferme",

	// Bind socket
	"Erreur bind socket",

	// Listen socket
	"Erreur listen socket",

	// Erreur FModex
	"Erreur FModex",

	// Erreur repertoire
	"Erreur repertoire",

	// Erreur de syntaxe
	"Erreur de syntaxe",

	// Impossible de resoudre le nom de domaine
	"Impossible de resoudre",

	// URL malformee
	"Erreur URL mal formee",

	// Erreur de protocole
	"Erreur de protocole",

	// Erreur regex
	"Erreur regex",

	// Erreur v4l2
	"Erreur v4l2",

	// Erreur PNG lib
	"Erreur PNG",

	// Erreur copie
	"Erreur copie",

	// Non implemente
	"Non implemente",

	// Echec authentification
	"Echec authentification",

	// Impossible d'obtenir shell
	"Impossible obtenir shell",

	// Processus toujours en cours
	"Processus toujours en cours",

	// Impossible d'obtenir session SFTP
	"Impossible d'obtenir session SFTP",

	// Impossible d'ouvrir fichier SFTP
	"Impossible d'ouvrir fichier SFTP",

	// Impossible de traiter la requete
	"Impossible de traiter la requete",

	// Erreur avec liaison serie
	"Erreur liaison serie",

	// Erreur http
	"Erreur HTTP",

	// Erreur aucune interface reseau
	"Erreur pas d'interface reseau",

	// Erreur inconnue
	"Erreur inconnue"
};
#endif // NLIB_ERREUR_NCODEERREUR_INTERNE

#endif // !NLIB_ERREUR_NCODEERREUR_PROTECT

