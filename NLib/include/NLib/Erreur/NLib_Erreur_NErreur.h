#ifndef NLIB_ERREUR_NERREUR_PROTECT
#define NLIB_ERREUR_NERREUR_PROTECT

/*
	Declaration d'une erreur NLib

	@author SOARES Lucas
*/

// ----------------------------
// struct NLib::Erreur::NErreur
// ----------------------------

typedef struct
{
	// Detail erreur
		// Message
		char *m_message;
		// Fichier
		char *m_fichier;
		// Ligne
		NU32 m_ligne;
		// Code d'erreur
		NCodeErreur m_code;
		NU32 m_codeUtilisateur;
		// Temps
		NU64 m_timestamp;

	// Niveau
	NNiveauErreur m_niveau;
} NErreur;

/**
 * Construire erreur
 *
 * @param codeErreur
 * 		Le code d'erreur
 * @param message
 * 		Le message d'erreur
 * @param codeUtilisateur
 * 		Le code utilisateur
 * @param fichier
 * 		Le fichier ou a eu lieu l'erreur
 * @param ligne
 * 		La ligne ou a eu lieu l'erreur
 * @param niveau
 * 		Le niveau de gravite de l'erreur
 *
 * @return l'instance
 */
__ALLOC NErreur *NLib_Erreur_NErreur_Construire( NCodeErreur codeErreur,
	const char *message,
	NU32 codeUtilisateur,
	const char *fichier,
	NU32 ligne,
	NNiveauErreur niveau );

/**
 * Detruire erreur
 *
 * @param this
 * 		Cette instance
 */
void NLib_Erreur_NErreur_Detruire( NErreur** );

/**
 * Obtenir code erreur
 *
 * @param this
 * 		Cette instance
 *
 * @return le code d'erreur
 */
NCodeErreur NLib_Erreur_NErreur_ObtenirCode( const NErreur* );

/**
 * Obtenir code erreur utilisateur
 *
 * @param this
 * 		Cette instance
 *
 * @return le code d'erreur utilisateur
 */
NU32 NLib_Erreur_NErreur_ObtenirCodeUtilisateur( const NErreur* );

/**
 * Obtenir message
 *
 * @param this
 * 		Cette instance
 *
 * @return le message d'erreur
 */
const char *NLib_Erreur_NErreur_ObtenirMessage( const NErreur* );

/**
 * Obtenir ligne
 *
 * @param this
 * 		Cette instance
 *
 * @return la ligne de l'erreur
 */
NU32 NLib_Erreur_NErreur_ObtenirLigne( const NErreur* );

/**
 * Obtenir fichier
 *
 * @param this
 * 		Cette instance
 *
 * @return le fichier
 */
const char *NLib_Erreur_NErreur_ObtenirFichier( const NErreur* );

/**
 * Obtenir niveau erreur
 *
 * @param this
 * 		Cette instance
 *
 * @return le niveau d'erreur
 */
NNiveauErreur NLib_Erreur_NErreur_ObtenirNiveau( const NErreur* );

/**
 * Obtenir timestamp
 *
 * @param this
 * 		Cette instance
 *
 * @return le timestamp de l'erreur
 */
NU64 NLib_Erreur_NErreur_ObtenirTimestamp( const NErreur* );

#endif // !NLIB_ERREUR_NERREUR_PROTECT

