#ifndef NLIB_ERREUR_NNIVEAUERREUR_PROTECT
#define NLIB_ERREUR_NNIVEAUERREUR_PROTECT

/*
	Niveau d'erreur pour la notification a NLib

	@author SOARES Lucas
*/

// --------------------------------
// enum NLib::Erreur::NNiveauErreur
// --------------------------------

typedef enum
{
	NNIVEAU_ERREUR_AVERTISSEMENT,
	NNIVEAU_ERREUR_ERREUR,

	NNIVEAUX_ERREUR
} NNiveauErreur;

/* Traduire niveau */
const char *NLib_Erreur_NNiveauErreur_Traduire( NNiveauErreur );

/* Niveau erreur sous forme de texte */
#ifdef NLIB_ERREUR_NNIVEAUERREUR_INTERNE
static const char NNiveauErreurTexte[ NNIVEAUX_ERREUR ][ 32 ] =
{
	"Avertissement",
	"Erreur"
};
#endif // NLIB_ERREUR_NNIVEAU_INTERNE


#endif // !NLIB_ERREUR_NNIVEAUERREUR_PROTECT

