#ifndef NLIB_MATH_MD5_PROTECT
#define NLIB_MATH_MD5_PROTECT

// -------------------------
// namespace NLib::Math::MD5
// -------------------------

// struct NLib::Math::MD5::NMD5_CTX
#include "NLib_Math_MD5_NMD5_CTX.h"

/**
 * Init MD5 hashing
 *
 * @param ctx
 * 		The current hashing state
 */
void NLib_Math_MD5_Init( NMD5_CTX *ctx );

/**
 * One step in hashing
 *
 * @param ctx
 * 		Current hashing state
 * @param data
 * 		The data to hash
 * @param size
 * 		The data length
 */
void NLib_Math_MD5_Update( NMD5_CTX *ctx,
	const void *data,
	unsigned long size );

/**
 * Finalize the hashing
 *
 * @param result
 * 		The result (must be 16 length)
 * @param ctx
 * 		The current state
 */
void NLib_Math_MD5_Final( __OUTPUT unsigned char result[ 16 ],
	NMD5_CTX *ctx );

#endif // !NLIB_MATH_MD5_PROTECT
