#ifndef NLIB_MATH_MD5_NMD5CTX_PROTECT
#define NLIB_MATH_MD5_NMD5CTX_PROTECT

// --------------------------------
// struct NLib::Math::MD5::NMD5_CTX
// --------------------------------

/*
 * This is an OpenSSL-compatible implementation of the RSA Data Security, Inc.
 * MD5 Message-Digest Algorithm (RFC 1321).
 *
 * Homepage:
 * http://openwall.info/wiki/people/solar/software/public-domain-source-code/md5
 *
 * Author:
 * Alexander Peslyak, better known as Solar Designer <solar at openwall.com>
 *
 * This software was written by Alexander Peslyak in 2001.  No copyright is
 * claimed, and the software is hereby placed in the public domain.
 * In case this attempt to disclaim copyright and place the software in the
 * public domain is deemed null and void, then the software is
 * Copyright (c) 2001 Alexander Peslyak and it is hereby released to the
 * general public under the following terms:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted.
 *
 * There's ABSOLUTELY NO WARRANTY, express or implied.
 *
 * See md5.c for more information.
 */

typedef struct NMD5_CTX
{
	NU32 lo,
		hi;

	NU32 a,
		b,
		c,
		d;

	unsigned char buffer[ 64 ];

	NU32 block[ 16 ];
} NMD5_CTX;

#endif // !NLIB_MATH_MD5_NMD5CTX_PROTECT
