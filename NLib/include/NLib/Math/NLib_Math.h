#ifndef NLIB_MATH_PROTECT
#define NLIB_MATH_PROTECT

/*
	Ensemble de fonctions mathematiques

	@author SOARES Lucas
*/

// --------------------
// namespace NLib::Math
// --------------------

// namespace NLib::Math::Geometrie
#include "Geometrie/NLib_Math_Geometrie.h"

// namespace NLib::Math::Probabilite
#include "Probabilite/NLib_Math_Probabilite.h"

// namespace NLib::Math::MD5
#include "MD5/NLib_Math_MD5.h"

/* Absolue */
NS32 NLib_Math_Abs( NS32 );

/* Minimum */
NS32 NLib_Math_Minimum( NS32 x,
	NS32 y );
NU32 NLib_Math_Minimum2( NU32 x,
	NU32 y );

/* Maximum */
NS32 NLib_Math_Maximum( NS32 x,
	NS32 y );
NU32 NLib_Math_Maximum2( NU32 x,
	NU32 y );

/* Obtenir nombre digit */
NU32 NLib_Math_ObtenirNombreDigit( NU32 );

/**
 * Calculer puissance
 *
 * @param nombre
 * 		Le nombre
 * @param puissance
 * 		La puissance
 *
 * @return nombre^puissance
 */
NU32 NLib_Math_CalculerPuissance( NU32 nombre,
	NU32 puissance );

#endif // !NLIB_MATH_PROTECT

