#ifndef NLIB_MATH_PROBABILITE_NEXPERIENCE_PROTECT
#define NLIB_MATH_PROBABILITE_NEXPERIENCE_PROTECT

// -------------------------------------------
// struct NLib::Math::Probabilite::NExperience
// -------------------------------------------

typedef struct
{
	// Probabilites de chaque evenement
	NU32 *m_probabilite;

	// Nombre d'evenements
	NU32 m_nombreEvenement;
} NExperience;

/* Construire (proba entre 0 et 100, avec total <= 100) */
__ALLOC NExperience *NLib_Math_Probabilite_NExperience_Construire( const NU32 *probabilite,
	NU32 nombreProbabilite );

/* Detruire */
void NLib_Math_Probabilite_NExperience_Detruire( NExperience** );

/* Faire une experience (retourne l'identifiant de l'evenement) */
NU32 NLib_Math_Probabilite_NExperience_Lancer( const NExperience* );

#endif // !NLIB_MATH_PROBABILITE_NEXPERIENCE_PROTECT

